package com.nutiteq;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.opengl.GLSurfaceView;
import android.provider.Settings.Secure;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.Components;
import com.nutiteq.components.Constraints;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MapTile;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.Options;
import com.nutiteq.components.Point3D;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.layers.Layers;
import com.nutiteq.license.DefaultWatermark;
import com.nutiteq.license.ExpiredWatermark;
import com.nutiteq.license.LicenseManager;
import com.nutiteq.license.OSMTextWatermark;
import com.nutiteq.license.OSMWatermark;
import com.nutiteq.license.Watermark;
import com.nutiteq.log.Log;
import com.nutiteq.renderers.MapRenderer;
import com.nutiteq.renderers.MapRenderers;
import com.nutiteq.renderers.utils.DefaultEGLConfigChooser;
import com.nutiteq.ui.MapTouchListener;

/**
 * The main view class for all mapping operations. Allows the user to manipulate the map and access various related
 * components.
 * 
 * Please note that as this class is derived from GLSurfaceView, it behaves differently from most Views.
 * It is strongly recommended to NOT create multiple MapView instances in your activity, as GLSurfaceView is really a
 * separate surface that is created and Z-ordered behind its containing window. GLSurfaceView Z ordering is different
 * from normal view Z ordering.
 * 
 * @author Nutiteq
 * 
 */
@SuppressLint("Override")
public class MapView extends GLSurfaceView {
  /**
   * Base 64 encoded license parameters
   */
  private static final String LICENSE_VALID_UNTIL = "dmFsaWRVbnRpbA=="; // 'validUntil' in Base64
  private static final String LICENSE_WATERMARK = "d2F0ZXJtYXJr"; // 'watermark' in Base64
  private static final String LICENSE_PACKAGE_NAME = "cGFja2FnZU5hbWU="; // 'packageName'
  private static final String LICENSE_DEVICE_ID = "ZGV2aWNlSWQ="; // 'deviceId'
  
  /**
   * Base 64 encoded public key for license manager
   */
  //private static final String LICENSE_MANAGER_PUBLIC_KEY = "MIIBtzCCASwGByqGSM44BAEwggEfAoGBAOHD5Oa9gZ/b6mJKf5xyVUAtEGzTD+ynSSxp/jLwdBiCq1MDKCDFrSJjCCp2V84SGcPzS+YRMXkPp6zAH8/8hXyqQrvocp+FJ0HZ51MCSesKzLDwgHOfat6BKQD/bzxX5LDKj84+1gzMcMA55JioVVG/cFoltKEFtS16zoLbloexAhUA8yRmkSLicTd2RYHuCtpNLOHuyIsCgYEAm+ZQpfx9tVtNh1eYqaWbVO9xNNJbZA2MpLbt4BYmUWtLdNq4I814ES/aWRlWEf2lXmLIdlAXWaRdvJ3Co66o2ZDeHWPqyNqAUpnf+OQeQu3DzuWTm5wx0bcwwZAhas7MyXGY6rc88TY+//H5l/QBG0P0J7a3V1cwACqZ4nQarkgDgYQAAoGAHf2jI4AsnPnYOQBzcrbU15dk7ygreVf4+7NpS0S8e+n/oHwS1lpUdstIx9KyqCrGB7G6maZkhztBu2/5he6/H3GnzCyvFq2iQZ7WVe6fkk86bW2blQki6Adj1dUCiFei/zhDd4wqFldfJaMBDVpXoXmMmoqCCFJ4eacPwZhBHGM=";
  private static final String LICENSE_MANAGER_PUBLIC_KEY = "MIIBtjCCASsGByqGSM44BAEwggEeAoGBAIlFs9OqwdM4lcfhXQVCyYDzPc6rO6sbtlKuEYa/uSzeuk1tvYcGybBwB3mYLYvOh0Qd/65TUO6rYI9xTAHK0HtjGj+XLaNDiP4eEvaVfg2fhX26XDdaAGXfKWKCHKiZ0cWLnBtTap2woVvSt6TLxcxrDnwG9mrL3Lt06rflF4oJAhUAyWhMdWcGzgq37TUJcKauhX7OEHcCgYAsXe5Q9JRA3yQsAGijSifQS8Pth1FfF69dU7VUeYD55VZ5x4UVAQP+wg7K5e6RQgJpaL1R4duVkFRgr3RuTwevv1szi/3ENiIQW4vNhPxc/sN+Y2YdlNnguOhV2LEcYmneX+F5cb2UXQZBBDhVgEtU7c9bxyA6tSwKuC70EqfZSQOBhAACgYAzp7pUQ1XxR79N8NbaB1PUPE7n1bZdFLF1QsK9thjL4Q3dbg6fub3qZfSPL286nZjfD+15oya1ORFKwSplindHNx6vSL+AmNhI4GYdyIasPnLAqCV9rIMTgQ+RfmyWDvSLxSDVqWqA83M6m/FFuWMKWKqMOEueJZTwrr/HNNTk+w==";

  private static boolean instanceCreated = false;
  private static final Object licenseManagerLock = new Object();
  
  private static final int ZOOM_IN_OUT_DURATION = 300;
  
  private Components components;
  private MapRenderers mapRenderers;
  private EGLConfigChooser eglConfigChooser;

  /**
   * Class constructor used for creating MapView from code.
   * 
   * @param context
   *          activity or application context
   * @throws SecurityException if invalid license is used for the current package.
   */
  public MapView(final Context context) {
    super(context);
    synchronized (licenseManagerLock) {
      LicenseManager manager = LicenseManager.getInstance();
      if (manager != null) {
        String packageName = manager.getProperty(LICENSE_PACKAGE_NAME);
        if (!packageName.equals(context.getPackageName())) {
          String packageRegExp = packageName.replace(".", "\\.").replace("*", ".*");
          if (!context.getPackageName().matches(packageRegExp)) {
            throw new SecurityException();
          }
          }
      }
      instanceCreated = true;
    }
  }
  
  /**
   * Class constructor used for creating MapView from the layout xml file.
   * 
   * @param context
   *          activity or application context
   * @param attrs
   *          view attributes
   */
  public MapView(final Context context, final AttributeSet attrs) {
    super(context, attrs);
  }
  
  /**
   * Set custom EGL display configuration chooser.
   * 
   * @param configChooser
   *          custom display configuration chooser.
   */
  @Override
  public void setEGLConfigChooser(EGLConfigChooser eglConfigChooser) {
    this.eglConfigChooser = eglConfigChooser;
    super.setEGLConfigChooser(eglConfigChooser);
  }

  /**
   * Get mapping components attached to the view.
   *  
   * @return instance of components attached to the view.
   */
  public Components getComponents() {
    return components;
  }

  /**
   * Set new mapping components. Should be called before startMapping()!
   * 
   * @param components
   *          instance of components to use.
   */
  public void setComponents(Components components) {
    this.components = components;
    if (components == null) {
      if (mapRenderers != null) {
        mapRenderers.getMapRenderer().setView(null);
      }
      mapRenderers = null;
    } else {
      mapRenderers = components.mapRenderers;
      init();
    }
  }

  /**
   * Starts mapping processes. Should be called onStart()
   */
  public void startMapping() {
    //super.onResume(); // potentially dangerous, stackoverflow has some report of NPE
    components.persistentCache.reopenDb();
    components.layers.onStartMapping();
    mapRenderers.getMapRenderer().onStartMapping();
  }

  /**
   * Stops mapping processes. Should be called onStop()
   */
  public void stopMapping() {
    //super.onPause();
    components.rasterTaskPool.cancelAll();
    components.layers.onStopMapping();
    mapRenderers.getMapRenderer().onStopMapping();
    components.persistentCache.closeDb();
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    List<MapTouchListener> mapTouchListeners = null; 
    if (components != null) {
      mapTouchListeners = components.options.getMapTouchListeners();
    }
    if (mapTouchListeners != null) {
      for (MapTouchListener touchListener : mapTouchListeners) {
        if (touchListener.onTouchEvent(event)) {
          return true;
        }
      }
    }
    if (mapRenderers != null) {
      return mapRenderers.getMapRenderer().onTouchEvent(event);
    }
    return super.onTouchEvent(event);
  }

  /**
   * Gets the Layers component, that can be used for adding and removing map layers.
   * 
   * @return the Layer component
   */
  public Layers getLayers() {
    return components.layers;
  }

  /**
   * Gets the Options component, that can be used for modifying various map parameters.
   * 
   * @return the Option component
   */
  public Options getOptions() {
    return components.options;
  }

  /**
   * Gets the Constraints component, that can be used for specifying various map movement restrictions.
   * 
   * @return the Constraints component
   */
  public Constraints getConstraints() {
    return components.constraints;
  }
  
  /**
   * Set map focus point offset from screen center in pixels. Default is 0, 0. Note that this offset shifts all
   * 2D coordinates, so worldToScreen/screenToWorld methods are affected.
   * 
   * @param x
   *          horizontal offset in pixels
   * @param y
   *          vertical offset in pixels
   */
  public void setFocusPointOffset(float x, float y) {
    mapRenderers.getMapRenderer().setFocusPointOffset(x, y);
  }
  
  /**
   * Calculates the world map position from screen point coordinates.
   * 
   * @param x
   *          the x coordinate of point in screen space
   * @param y
   *          the y coordinate of point in screen space 
   *          
   * @return corresponding map point coordinates in world space (at ground plane, thus z is 0)
   */
  public MapPos screenToWorld(double x, double y) {
    Rect viewRect = getSizeWithFallback();
    Point3D point = mapRenderers.getMapRenderer().screenToWorld(x, y, viewRect.width(), viewRect.height(), false);
    if (point == null) {
      return getCameraPos();
    }
    MapPos mapPos = mapRenderers.getRenderProjection().unproject(point);
    return components.layers.getBaseProjection().fromInternal(mapPos);
  }
  
  /**
   * Calculates the screen coordinates for given map position.
   * 
   * @param x
   *          the x coordinate of the map point
   * @param y
   *          the y coordinate of the map point
   * @param z
   *          the z coordinate of the map point
   *
   * @return corresponding screen point coordinates. Note: returned z coordinate is always 0.
   */
  public MapPos worldToScreen(double x, double y, double z) {
    Rect viewRect = getSizeWithFallback();
    MapPos mapPosInternal = getLayers().getBaseProjection().toInternal(x, y);
    Point3D point = mapRenderers.getRenderProjection().project(new MapPos(mapPosInternal.x, mapPosInternal.y, z));
    return mapRenderers.getMapRenderer().worldToScreen(point.x, point.y, point.z, viewRect.width(), viewRect.height());
  }
  
  /**
   * Calculate world coordinates from map tile and tile local coordinates.  
   *
   * @param mapTile
   *          map tile
   * @param tilePos
   *          tile local coordinates of the point. Tile local coordinates are in 0..1 range.
   *
   * @return corresponding map point coordinates in world space.
   */
  public MapPos mapTileToWorld(MapTile mapTile, MapPos tilePos) {
    MapPos mapPosInternal = mapRenderers.getMapRenderer().mapTileToInternal(mapTile, tilePos);
    return components.layers.getBaseProjection().fromInternal(mapPosInternal);
  }

  /**
   * Get map tile corresponding to world coordinates. Also return local coordinates within the tile.
   * Tile zoom level is calculated based on current view.  
   *
   * @param x
   *          the x coordinate of the map point
   * @param y
   *          the y coordinate of the map point
   * @param tilePos
   *          tile local coordinates of the point. Tile local coordinates are in 0..1 range.
   *
   * @return tile corresponding to given coordinates or null if no such tile exists
   */
  public MapTile worldToMapTile(double x, double y, MutableMapPos tilePos) {
    MapPos mapPosInternal = getLayers().getBaseProjection().toInternal(x, y);
    return mapRenderers.getMapRenderer().internalToMapTile(mapPosInternal.x, mapPosInternal.y, -1, tilePos);
  }

  /**
   * Get map tile corresponding to world coordinates and given zoom level. Also return local coordinates within the tile.
   *
   * @param x
   *          the x coordinate of the map point
   * @param y
   *          the y coordinate of the map point
   * @param zoom
   *          zoom level for the tile to return.
   * @param tilePos
   *          tile local coordinates of the point. Tile local coordinates are in 0..1 range.
   *
   * @return tile corresponding to given coordinates or null if no such tile exists
   */
  public MapTile worldToMapTile(double x, double y, int zoom, MutableMapPos tilePos) {
    MapPos mapPosInternal = getLayers().getBaseProjection().toInternal(x, y);
    return mapRenderers.getMapRenderer().internalToMapTile(mapPosInternal.x, mapPosInternal.y, zoom, tilePos);
  }

  /**
   * Gets the current camera (or eye) position in current base map's projection.
   * 
   * @return the current camera position
   */
  public MapPos getCameraPos() {
    Point3D cameraPos = mapRenderers.getMapRenderer().getCameraPos();
    MapPos cameraPosInternal = mapRenderers.getRenderProjection().unproject(cameraPos);
    return components.layers.getBaseProjection().fromInternal(cameraPosInternal);
  }

  /**
   * Gets the position of the point, that the camera is currently looking at, in current base map's projection.
   * 
   * @return the current camera position
   */
  public MapPos getFocusPoint() {
    Point3D focusPoint = mapRenderers.getMapRenderer().getFocusPoint();
    MapPos focusPointInternal = mapRenderers.getRenderProjection().unproject(focusPoint);
    return components.layers.getBaseProjection().fromInternal(focusPointInternal);
  }

  /**
   * Sets the position of the point that the camera should be looking at, in current base map's projection. The
   * resulting focus point position is capped to world range. The focus point's position can be further constrained by
   * the Constrains.setBounds method.
   * 
   * @param x
   *          the x component of the new focus point
   * @param y
   *          the y component of the new focus point
   */
  public void setFocusPoint(double x, double y) {
    MapPos mapPos = components.layers.getBaseProjection().toInternal(x, y);
    Point3D focusPoint = mapRenderers.getRenderProjection().project(mapPos);
    mapRenderers.getMapRenderer().setFocusPoint(focusPoint.x, focusPoint.y, focusPoint.z, 0, true);
  }

  /**
   * Sets the position of the point that the camera should be looking at, in current base map's projection. The
   * resulting focus point position is capped to world range. The focus point's position can be further constrained by
   * the Constrains.setBounds method.
   * 
   * @param focusPoint
   *          the new focus point
   */
  public void setFocusPoint(MapPos focusPoint) {
    setFocusPoint(focusPoint.x, focusPoint.y);
  }

  /**
   * Animates the the position of the point that the camera should be looking at. The position is given in current base map's projection.
   * If this method is called before the old animation has finished, old animation will be canceled.
   * 
   * @param focusPoint
   *          the new focus point
   * @param duration
   *          duration of animation in milliseconds
   */
  public void setFocusPoint(MapPos focusPoint, int duration) {
    MapPos mapPos = components.layers.getBaseProjection().toInternal(focusPoint);
    Point3D point = mapRenderers.getRenderProjection().project(mapPos);
    mapRenderers.getMapRenderer().setFocusPoint(point.x, point.y, point.z, duration, true);
  }

  /**
   * Gets the current map rotation in degrees. 0 means looking to north, 90 means west, 180 means south and 270 means
   * east. Note: this function is deprecated, use getMapRotation() instead.
   * 
   * @return the current map rotation in degrees
   */
  @Deprecated
  public float getRotation() {
    return getMapRotation();
  }

  /**
   * Gets the current map rotation in degrees. 0 means looking to north, 90 means west, 180 means south and 270 means
   * east.
   * 
   * @return the current map rotation in degrees
   */
  public float getMapRotation() {
    float angle = mapRenderers.getMapRenderer().getRotation();
    return (angle % 360 + 360) % 360;
  }

  /**
   * Sets the new absolute map rotation in degrees. 0 means look to north, 90 means west, 180 means south and 270 means
   * east. Note: this function is deprecated, use setMapRotation(float) instead.
   * 
   * @param angle
   *          the new absolute map rotation in degrees
   */
  @Deprecated
  public void setRotation(float angle) {
    setMapRotation(angle);
  }

  /**
   * Sets the new absolute map rotation in degrees. 0 means look to north, 90 means west, 180 means south and 270 means
   * east.
   * 
   * @param angle
   *          the new absolute map rotation in degrees
   */
  public void setMapRotation(float angle) {
    mapRenderers.getMapRenderer().setRotation(angle, 0);
  }

  /**
   * Sets the rotation of the map relative to the current rotation angle. Positive values rotate clockwise,
   * negative values counterclockwise.
   * 
   * @param deltaAngle
   *          the number of degrees the map should be rotate by
   */
  public void rotate(float deltaAngle) {
    mapRenderers.getMapRenderer().rotate(deltaAngle, 0);
  }

  /**
   * Animates the rotation of the map by the given angle. Positive values rotate clockwise,
   * negative values counterclockwise. If this method is called before the old animation has finished,
   * old animation will be canceled.
   * 
   * @param deltaAngle
   *          the number of degrees the map should be rotate by
   * @param duration
   *          duration of animation in milliseconds
   */
  public void rotate(float deltaAngle, int duration) {
    mapRenderers.getMapRenderer().rotate(deltaAngle, duration);
  }

  /**
   * Gets the current tilt angle in degrees. 0 means looking directly at the horizon, 90 means looking directly down.
   * 
   * @return the current tilt angle in degrees
   */
  public float getTilt() {
    return mapRenderers.getMapRenderer().getTilt();
  }

  /**
   * Sets the new absolute tilt angle in degrees. 0 means look directly at the horizon, 90 means look directly down. The
   * minimum tilt angle is 30 degrees and the maximum is 90 degrees. The tilt angle can be further constrained by the
   * Constraints.setTileRange method. Values exceeding these ranges will be capped.
   * 
   * @param angle
   *          the new absolute tilt angle in degrees
   */
  public void setTilt(float angle) {
    mapRenderers.getMapRenderer().setTilt(angle, 0);
  }

  /**
   * Sets the new tilt angle relative to the current tilt angle. Positive values tilt the camera towards look at the
   * ground, negative values tilt the camera towards looking at the horizon. The new calculated tilt angle will be
   * capped to a range as specified in setTilt.
   * 
   * @param deltaAngle
   *          the number of degrees the camera should be tilted by
   */
  public void tilt(float deltaAngle) {
    mapRenderers.getMapRenderer().tilt(deltaAngle, 0);
  }

  /**
   * Animate the new angle relative to the current tilt angle. Positive values tilt the camera towards look at the
   * ground, negative values tilt the camera towards looking at the horizon. The new calculated tilt angle will be
   * capped to a range as specified in setTilt. If this method is called before the old animation has finished,
   * old animation will be canceled.
   * 
   * @param deltaAngle
   *          the number of degrees the camera should be tilted by
   * @param duration
   *          duration of animation in milliseconds
   */
  public void tilt(float deltaAngle, int duration) {
    mapRenderers.getMapRenderer().tilt(deltaAngle, duration);
  }

  /**
   * Gets the current zoom level. The value returned is never negative, 0 means absolutely zoomed out and all other
   * values describe some level of zoom.
   * 
   * @return the current zoom value
   */
  public float getZoom() {
    return mapRenderers.getMapRenderer().getZoom();
  }

  /**
   * Sets the new absolute zoom value. The minimum zoom value is 0, which means absolutely zoomed out and the maximum
   * zoom value is 24. The zoom value can be further constrained by the Constraints.setZoomRange method. Values
   * exceeding these ranges will be capped.
   * 
   * @param zoom
   *          the new absolute zoom value
   */
  public void setZoom(float zoom) {
    mapRenderers.getMapRenderer().setZoom(zoom, 0);
  }

  /**
   * Sets the new zoom value relative to the current zoom value. Positive values zoom in, negative values zoom out. The
   * new calculated zoom value will be capped to range as specified in setZoom.
   * 
   * @param deltaZoom
   *          the delta zoom value
   */
  public void zoom(float deltaZoom) {
    mapRenderers.getMapRenderer().zoom(deltaZoom, 0);
  }

  /**
   * Animate zoom relative to the current zoom value. Positive values zoom in, negative values zoom out. The
   * new calculated zoom value will be capped to range as specified in setZoom. If this method is called before the old animation has finished,
   * old animation will be canceled. 
   * 
   * @param deltaZoom
   *          the delta zoom value
   * @param duration
   *          duration of animation in milliseconds
   */
  public void zoom(float deltaZoom, int duration) {
    mapRenderers.getMapRenderer().zoom(deltaZoom, duration);
  }

  /**
   * Zooms in by 1. Convenience method for calling zoom(1, 300).
   */
  public void zoomIn() {
    mapRenderers.getMapRenderer().zoom(1, ZOOM_IN_OUT_DURATION);
  }

  /**
   * Zooms out by 1. Convenience method for calling zoom(-1, 300).
   */
  public void zoomOut() {
    mapRenderers.getMapRenderer().zoom(-1, ZOOM_IN_OUT_DURATION);
  }
  
  /**
   * Set map view parameters (focus position, zoom tilt, rotation) so that the specified bounding box becomes fully visible.
   * Note: this method needs view dimensions, which are not available when called from onCreate. Thus incorrect bounding area can be calculated
   * (it is calculated based on screen dimensions in that case)
   * 
   * @param bounds
   *          bounds for the view
   * @param integerZoom
   *          true if closest integer zoom level should be used, false if exact fractional zoom level should be used 
   */
  public void setBoundingBox(Bounds bounds, boolean integerZoom) {
    setBoundingBox(bounds, integerZoom, 0);
  }

  /**
   * Animate the view parameters (focus position, zoom tilt, rotation) so that the specified bounding box becomes fully visible.
   * Note: this method needs view dimensions, which are not available when called from onCreate. Thus incorrect bounding area can be calculated
   * (it is calculated based on screen dimensions in that case)
   * 
   * @param bounds
   *          bounds for the view
   * @param integerZoom
   *          true if closest integer zoom level should be used, false if exact fractional zoom level should be used 
   * @param duration
   *          duration of animation in milliseconds
   */
  public void setBoundingBox(Bounds bounds, boolean integerZoom, int duration) {
    setBoundingBox(bounds, getSizeWithFallback(), integerZoom, true, true, duration);
  }

  /**
   * Animate the view parameters (focus position, tilt, rotation, zoom) so that the specified bounding box becomes fully visible.
   * 
   * @param bounds
   *          map bounds for the view (in base projection coordinates)
   * @param rect
   *          rectangle (in screen coordinates) to fit the map bounds into
   * @param integerZoom
   *          if true, then closest integer zoom level will be used. If false, exact fractional zoom level will be used. 
   * @param resetTilt
   *          if true, view will be untilted. If false, current tilt will be kept. 
   * @param resetRotation
   *          if true, rotation will be reset. If false, current rotation will be kept. 
   * @param duration
   *          duration of animation in milliseconds
   */
  public void setBoundingBox(Bounds bounds, Rect rect, boolean integerZoom, boolean resetTilt, boolean resetRotation, int duration) {
    Rect viewRect = getSizeWithFallback();
    MapPos mapPos0 = components.layers.getBaseProjection().toInternal(bounds.left, bounds.top);
    MapPos mapPos1 = components.layers.getBaseProjection().toInternal(bounds.right, bounds.bottom);
    List<Point3D> points = new ArrayList<Point3D>();
    points.add(mapRenderers.getRenderProjection().project(mapPos0));
    points.add(mapRenderers.getRenderProjection().project(mapPos1));
    Point3D focusPoint = mapRenderers.getRenderProjection().project(new MapPos((mapPos0.x + mapPos1.x) / 2, (mapPos0.y + mapPos1.y) / 2));
    mapRenderers.getMapRenderer().fitToBoundingBox(focusPoint, points, rect, viewRect.width(), viewRect.height(), integerZoom, resetTilt, resetRotation, duration);
  }
  
  /**
   * Get currently selected vector element.
   * 
   * @return selected vector element or null.
   */
  public VectorElement getSelectedVectorElement() {
    return mapRenderers.getMapRenderer().getSelectedVectorElement();
  }

  /**
   * Makes the label of the specified marker visible. Does NOT move the map to make the marker visible on the screen.
   * 
   * @param vectorElement
   *          the element to be selected. Element has to be attached to a layer to be selected. 
   */
  public void selectVectorElement(VectorElement vectorElement) {
    mapRenderers.getMapRenderer().selectVectorElement(vectorElement);
  }

  /**
   * Hides the label of the currently selected marker, if there is one.
   */
  public void deselectVectorElement() {
    mapRenderers.getMapRenderer().deselectVectorElement();
  }

  /**
   * Register SDK license.
   * Note: this is class method and must be called before creating any actual MapView instances!
   *
   * @param license
   *          license string provided for this application 
   * @param context
   *          application context for the license 
   *
   * @return true is license was accepted, false if not.
   */
  public static boolean registerLicense(String license, Context context) {
    synchronized (licenseManagerLock) {
      if (instanceCreated) {
        return false;
      }

      // Try to decrypt and load license
      LicenseManager manager = new LicenseManager();
      try {
        String licenseDecrypted = LicenseManager.decryptLicense(license);
        if (!manager.load(licenseDecrypted, LICENSE_MANAGER_PUBLIC_KEY)) {
          return false;
        }
      } catch (Exception ex) {
        return false;
      }

      // Verify package name
      String packageName = manager.getProperty(LICENSE_PACKAGE_NAME);
      if (packageName == null) {
        return false;
      }
      String realPackageName = context.getPackageName();
      if (!packageName.equals(realPackageName)) {
        String packageRegExp = packageName.replace(".", "\\.").replace("*", ".*");
        if (!realPackageName.matches(packageRegExp)) {
          return false;
        }
      }
      
      // Verify device ids
      String deviceIds = manager.getProperty(LICENSE_DEVICE_ID);
      if (deviceIds != null) {
        boolean match = false;
        String currentDeviceId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        for (String deviceId : deviceIds.split(",")) {
          if (deviceId.trim().equals(currentDeviceId)) { // works also if currentDeviceId is null
            match = true;
            break;
          }
        }
        if (!match) {
          return false;
        }
      }

      // Verify license has not expired
      String validUntil = manager.getProperty(LICENSE_VALID_UNTIL);
      if (validUntil != null) {
        Date currentDate = new Date(System.currentTimeMillis());
        try {
          Date validDate = (new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)).parse(validUntil);
          if (currentDate.after(validDate)) {
            manager.clear();
            manager.setWatermark(new ExpiredWatermark());
            LicenseManager.setInstance(manager);
            return false;
          }
        } catch (Exception ex) {
          return false;
        }
      }

      // Check license 'watermark' property and set default watermark accordingly
      String watermark = manager.getProperty(LICENSE_WATERMARK);
      manager.setWatermark(new DefaultWatermark());
      if (watermark != null) {
        if (watermark.equals("OSM")) {
          manager.setWatermark(new OSMWatermark());
          manager.addWatermark(new OSMTextWatermark());
        }
      }

      // TODO: implement SDK version checks

      // Success
      LicenseManager.setInstance(manager);
      return true;
    }
  }

  /**
   * Set custom watermark replacing default SDK watermark.
   * Note: this method is only available for valid SDK licenses, MapView.registerLicense
   * must be called before this method.
   *
   * @param bitmap
   *          watermark bitmap to use or null. Note: bitmap.getConfig() must return ARGB_8888.
   * @param xPos
   *          watermark relative x position on screen between -1.0f (left) to 1.0f (right)
   * @param yPos
   *          watermark relative y position on screen between -1.0f (bottom) to 1.0f (top)
   * @param scale
   *          watermark relative scale on screen
   * @throws UnsupportedOperationException if the license does not allow watermark customization.
   */
  public static void setWatermark(Bitmap bitmap, float xPos, float yPos, float scale) {
    synchronized (licenseManagerLock) {
      LicenseManager manager = LicenseManager.getInstance();
      if (manager == null) {
        return;
      }
      String watermarkType = manager.getProperty(LICENSE_WATERMARK);
      if (watermarkType == null || !watermarkType.equals("custom")) {
        throw new UnsupportedOperationException();
      }
      Watermark watermark = null;
      if (bitmap != null) {
        watermark = new Watermark(bitmap, xPos, yPos, scale);
      }
      LicenseManager.getInstance().setWatermark(watermark);
    }
  }
  
  private void init() {
    Log.debug("MapView: model: " + android.os.Build.MODEL + ", board: " + android.os.Build.BOARD + ", product: " + android.os.Build.PRODUCT);
    setFocusable(true);
    getHolder().setFormat(PixelFormat.RGBA_8888);
    if (eglConfigChooser == null) {
      setEGLConfigChooser(new DefaultEGLConfigChooser(0));
    }
    MapRenderer renderer = mapRenderers.getMapRenderer();
    renderer.setView(this);
    setRenderer(renderer);
    setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
  }

  private Rect getSizeWithFallback() {
    DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
    int width = getWidth();
    if (width == 0) { // it is possible that layout is not yet done, so use full screen size as a fallback
      width = metrics.widthPixels;
    }
    int height = getHeight();
    if (height == 0) {
      height = metrics.heightPixels;
    }
    
    return new Rect(0, 0, width, height);
  }
}

  