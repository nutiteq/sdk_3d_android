package com.nutiteq.ui;

import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

import com.nutiteq.components.TextureInfo;
import com.nutiteq.utils.Utils;

/**
 * Abstract base class for map element labels.
 */
public abstract class Label {

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void onShow(GLSurfaceView view, float x, float y) {
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void onHide(GLSurfaceView view) {
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public boolean onTouchEvent(MotionEvent ev) {
    return false;
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public boolean isDirty() {
    return false;
  }

  /**
   * This is a rendering hook called by map renderer to draw label into bitmap.
   * 
   * @return drawn bitmap wrapped as texture info. 
   */
  public abstract TextureInfo drawMarkerLabel();
  
  /**
   * Get alpha value for marker rendering.
   * 
   * @return alpha value between 0.0f and 1.0f
   */
  public float getMarkerLabelAlpha() {
    return 1.0f;
  }

  /**
   * Utility method for creating texture info with specified size and embedded bitmap.
   * This bitmap can be used as canvas for drawing the label.
   * 
   * @param width
   *          embedded bitmap width.
   * @param height
   *          embedded bitmap height
   * @return texture info with embedded bitmap of specified size.
   */
  protected TextureInfo getTextureInfo(int width, int height) {
    int realWidth = Utils.upperPow2(width);
    int realHeight = Utils.upperPow2(height);
    float tx = (float) width / realWidth;
    float ty = (float) height / realHeight;

    Bitmap labelBitmap = Bitmap.createBitmap(realWidth, realHeight, Bitmap.Config.ARGB_8888);

    float[] labelTexCoords = new float[] { 0.0f, ty, tx, ty, 0.0f, 0.0f, tx, 0.0f };

    return new TextureInfo(labelBitmap, labelTexCoords, width, height);
  }
}
