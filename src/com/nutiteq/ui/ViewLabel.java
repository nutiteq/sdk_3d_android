package com.nutiteq.ui;

import java.lang.reflect.Method;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import com.nutiteq.components.TextureInfo;
import com.nutiteq.style.LabelStyle;

/**
 * Map label for embedding Android views. 
 */
public class ViewLabel extends Label {
  private static final LabelStyle DEFAULT_STYLE = LabelStyle.builder().build();

  private final String title;
  private final View view;
  private final Handler handler;
  private final LabelStyle style;

  private final int titleWidth;
  private final int titleHeight;

  private int viewWidth;
  private int viewHeight;
  private int viewScrollX = -1;
  private int viewScrollY = -1;

  private Runnable task;
  private Bitmap bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
  private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
  private volatile boolean isDirty = false;

  private boolean handleTouch = false;
  private volatile Matrix transform;

  /**
   * Default constructor.
   * 
   * @param view
   *          Android view to embed into the label.
   * @param handler
   *          Android handler for the view.
   */
  public ViewLabel(View view, Handler handler) {
    this(null, view, handler);
  }

  /**
   * Constructor for creating label with both title and embedded view.
   * 
   * @param title
   *          label title. Title can be null, in that case title will not be displayed.
   * @param view
   *          Android view to embed into the label.
   * @param handler
   *          Android handler for the view.
   */
  public ViewLabel(String title, View view, Handler handler) {
    this(title, view, handler, DEFAULT_STYLE);
  }
  
  /**
   * Constructor for creating label with title, embedded view and style.
   * 
   * @param title
   *          label title. Title can be null, in that case title will not be displayed.
   * @param view
   *          Android view to embed into the label.
   * @param handler
   *          Android handler for the view.
   * @param style
   *          label style.
   */
  public ViewLabel(String title, View view, Handler handler, LabelStyle style) {
    this.style = style;
    this.title = title;
    this.view = view;
    this.handler = handler;

    if (title != null) {
      paint.setTextAlign(style.titleAlign);
      paint.setStyle(Style.FILL);

      paint.setTypeface(style.titleFont);
      paint.setTextSize(style.titleSize);

      Rect titleSize = new Rect();
      paint.getTextBounds(title, 0, title.length(), titleSize);
      titleWidth = titleSize.width();
      titleHeight = titleSize.height();
    } else {
      titleWidth = 0;
      titleHeight = 0;
    }
    
    viewWidth = view.getWidth();
    viewHeight = view.getHeight();
  }

  /**
   * Get title of the label.
   * 
   * @return title of the label.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Get embedded view of the label.
   * 
   * @return embedded view.
   */
  public View getView() {
    return view;
  }
  
  /**
   * Get the touch handling mode for the label.
   * 
   * @return the flag value for handling touch events.
   */
  public boolean isTouchHandlingMode() {
    return handleTouch;
  }

  /**
   * Set the touch handling mode for the label.
   * If the mode is set to true, touch events will be forwarded to the view. In case of webview, this allows browsing inside the label.
   * Otherwise label acts as a normal label and touch events are not forwarded.
   * 
   * @param handleTouch the flag value for handling touch events. Default is false.
   */
  public void setTouchHandlingMode(boolean handleTouch) {
    this.handleTouch = handleTouch;
  }

  @Override
  public TextureInfo drawMarkerLabel() {
    int width = (int) Math.max(titleWidth, viewWidth + 2 * style.edgePadding);
    int height = (int) titleHeight + viewHeight + 2 * style.edgePadding + style.linePadding;

    // Build texture info and canvas
    TextureInfo textureInfo = getTextureInfo(width, height + Math.max(0, style.triangleSize - style.borderSize));
    Canvas canvas = new Canvas(textureInfo.bitmap);

    // Draw background bubble
    paint.setColor(style.borderColor);
    canvas.drawRoundRect(new RectF(0, 0, width, height), style.roundedOutside, style.roundedOutside, paint);
    paint.setColor(style.backgroundColor);
    canvas.drawRoundRect(new RectF(style.borderSize, style.borderSize, width - style.borderSize, height - style.borderSize),
        style.roundedInside, style.roundedInside, paint);
    
    // Draw triangle
    Path triangleOutside = new Path();
    triangleOutside.moveTo(0, 0);
    triangleOutside.lineTo(style.triangleSize, 0);
    triangleOutside.lineTo(style.triangleSize / 2, style.triangleSize);
    triangleOutside.lineTo(0, 0);

    float newBorderSize = (int) (style.borderSize * 1.4);
    Path triangleInside = new Path();
    triangleInside.moveTo(newBorderSize, 0);
    triangleInside.lineTo(style.triangleSize - newBorderSize, 0);
    triangleInside.lineTo(style.triangleSize / 2, style.triangleSize - newBorderSize * 1.5f);
    triangleInside.lineTo(newBorderSize, 0);

    canvas.translate(width / 2 - style.triangleSize / 2, height - style.borderSize);
    paint.setColor(style.borderColor);
    canvas.drawPath(triangleOutside, paint);
    paint.setColor(style.backgroundColor);
    canvas.drawPath(triangleInside, paint);
    canvas.setMatrix(null);

    // Draw text
    if (title != null) {
      paint.setColor(style.titleColor);
      paint.setTypeface(style.titleFont);
      paint.setTextSize(style.titleSize);
      canvas.drawText(title, width / 2, style.edgePadding + titleHeight, paint);
    }
    
    // Draw view
    synchronized (bitmap) {
      isDirty = false;
      canvas.drawBitmap(bitmap, (width - viewWidth) / 2, style.edgePadding + titleHeight, null);
    }
    return textureInfo;
  }
  
  @Override
  public float getMarkerLabelAlpha() {
    return style.alpha;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  @Override
  public void onShow(final GLSurfaceView glView, final float x, final float y) {
    // Attach task that monitors view to handler
    if (task == null) {
      task = new Runnable() {
        public void run() {
          // View.isDirty is available from API11 only, thus use reflection to get access
          boolean redraw = false;
          try {
            Method method = view.getClass().getMethod("isDirty", (Class<?>[]) null);
            Object result = method.invoke(view, (Object[]) null);
            if ((Boolean) result) {
              redraw = true;
            }
          }
          catch (Exception e) {
            redraw = true;
          }

          // Check if view/bitmap sizes are same. Otherwise redraw
          synchronized (bitmap) {
            if (bitmap.getWidth() != view.getWidth() || bitmap.getHeight() != view.getHeight()) {
              bitmap.recycle();
              bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
              viewWidth = view.getWidth();
              viewHeight = view.getHeight();
              redraw = true;
            }
          }

          // Check scroll positions
          if (viewScrollX != view.getScrollX() || viewScrollY != view.getScrollY()) {
            viewScrollX = view.getScrollX();
            viewScrollY = view.getScrollY();
            redraw = true;
          }

          // Draw view to bitmap and mark bitmap as dirty
          if (redraw) {
            synchronized (bitmap) {
              Canvas bitmapCanvas = new Canvas(bitmap);
              bitmapCanvas.translate(-view.getScrollX(), -view.getScrollY());
              view.draw(bitmapCanvas);
              isDirty = true;
            }
            glView.requestRender();
          }

          handler.post(task);
        }
      };
      handler.post(task);
    }

    // Create coordinate transformation for the view
    transform = new Matrix();
    transform.setTranslate(-(x - viewWidth / 2), -(glView.getHeight() - (style.edgePadding + viewHeight + y)));
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  @Override
  public void onHide(GLSurfaceView view) {
    // Remove callback
    if (task != null) {
      handler.removeCallbacks(task);
      task = null;
    }
    transform = null;
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  @Override
  public boolean isDirty() {
    return isDirty;
  }

  /**
   * Not part of public API.
   * NOTE: this method is called from UI thread, NOT from renderer thread!
   * @pad.exclude
   */
  @Override
  public boolean onTouchEvent(MotionEvent ev) {
    if (!handleTouch) {
      return false;
    }

    Matrix transform = this.transform; // make local copy as it can change
    if (transform == null) {
      return false;
    }

    // Transform event coordinates and check that they hit the view
    float[] coords = new float[] { ev.getX(), ev.getY() };
    transform.mapPoints(coords);
    if (coords[0] < 0 || coords[1] < 0 || coords[0] >= viewWidth || coords[1] >= viewHeight) {
      return false;
    }
    
    // Clone event, set new coordinates and dispatch it to the view
    final MotionEvent ev2 = MotionEvent.obtain(ev);
    ev2.setLocation(coords[0], coords[1]);
    handler.post(new Runnable() {
      public void run() {
        view.dispatchTouchEvent(ev2);
        ev2.recycle();
      }
    });
    return true;
  }

}
