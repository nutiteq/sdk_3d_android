package com.nutiteq.ui;

import android.view.MotionEvent;

/**
 * Listener for map touch events. 
 */
public interface MapTouchListener {
  
  /**
   * Handle touch event.
   * 
   * @param event to handle
   * @return true if event was handled and should not be processed further or false if event was not handled by this listener.
   */
  boolean onTouchEvent(MotionEvent event);
}
