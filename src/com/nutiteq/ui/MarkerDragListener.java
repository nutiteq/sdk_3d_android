package com.nutiteq.ui;

import com.nutiteq.components.MapPos;
import com.nutiteq.geometry.DynamicMarker;

/**
 * Listener for dynamic marker drag events.
 */
public interface MarkerDragListener {

  /**
   * Event for marker move event initiation. 
   * 
   * @param marker marker that is being moved.
   */
  void onDragStart(DynamicMarker marker);
  
  /**
   * Event for marker move event.
   * 
   * @param marker marker that is being moved
   * @param pos new position for the marker
   * @return true if the position is accepted and false otherwise
   */
  boolean onDrag(DynamicMarker marker, MapPos pos);
  
  /**
   * Event for finished marker move event.
   * 
   * @param marker marker that was dragged.
   */
  void onDragEnd(DynamicMarker marker);
}
