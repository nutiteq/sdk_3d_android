package com.nutiteq.ui;

import android.graphics.Bitmap;

/**
 * Listener for rendering events.
 */
public interface MapRenderListener {

  /**
   * Called when map has been rendered.
   * 
   * @param mapBitmap rendered map as bitmap.
   */
  void onMapRendered(Bitmap mapBitmap);
}
