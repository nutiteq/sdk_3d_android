package com.nutiteq.ui;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.os.Handler;
import android.os.Message;

import com.nutiteq.components.MutableMapPos;
import com.nutiteq.geometry.VectorElement;

/**
 * Listener for map events (element clicking, map movement, etc).
 * Also provides hooks for custom OpenGL rendering.
 */
public abstract class MapListener {
  private static final int ON_MAP_MOVED = 0;
  private static final int ON_MAP_CLICKED = 1;
  private static final int ON_VECTOR_ELEMENT_CLICKED = 2;
  private static final int ON_LABEL_CLICKED = 3;
  private static final int ON_BACKGROUND_TASKS_STARTED = 4;
  private static final int ON_BACKGROUND_TASKS_FINISHED = 5;
  
  private int backgroundTaskCounter = 0;
  
  private static class VectorElementClickEvent {
    final VectorElement element;
    final double x;
    final double y;

    VectorElementClickEvent(VectorElement element, double x, double y) {
      this.element = element;
      this.x = x;
      this.y = y;
    }
  }
  
  private final Handler handler = new Handler() {
    @Override
    public final void handleMessage(Message msg) {
      switch (msg.what) {
      case ON_MAP_MOVED:
        onMapMoved();
        break;
      case ON_MAP_CLICKED:
        MutableMapPos mapPos = (MutableMapPos) msg.obj;
        onMapClicked(mapPos.x, mapPos.y, msg.arg1 == 0 ? false : true);
        break;
      case ON_VECTOR_ELEMENT_CLICKED:
        VectorElementClickEvent clickEvent = (VectorElementClickEvent) msg.obj;
        onVectorElementClicked(clickEvent.element, clickEvent.x, clickEvent.y, msg.arg1 == 0 ? false : true);
        break;
      case ON_LABEL_CLICKED:
        onLabelClicked((VectorElement) msg.obj, msg.arg1 == 0 ? false : true);
        break;
      case ON_BACKGROUND_TASKS_STARTED:
        onBackgroundTasksStarted();
        break;
      case ON_BACKGROUND_TASKS_FINISHED:
        onBackgroundTasksFinished();
        break;
      }
    }
  };

  /**
   * Gets called when the map is moved. The method is called from the main
   * thread.
   */
  public abstract void onMapMoved();

  /**
   * Gets called when a click is performed on a empty map area.
   * Coordinates are in the base map's projection's coordinate system. The
   * method is called from the main thread.
   * 
   * @param x
   *          x component of the click coordinate
   * @param y
   *          y component of the click coordinate
   * @param longClick
   *          true if a long click was performed, false if a normal click was performed
   */
  public abstract void onMapClicked(double x, double y, boolean longClick);

  /**
   * Gets called when a click is performed on a vector element.
   * Coordinates are in the base map's projection's coordinate system. The
   * method is called from the main thread.
   * 
   * @param vectorElement
   *          vector element on which the normal click was performed on
   * @param x
   *          x component of the click coordinate
   * @param y
   *          y component of the click coordinate
   * @param longClick
   *          true if a long click was performed, false if a normal click was performed
   */
  public abstract void onVectorElementClicked(VectorElement vectorElement, double x, double y, boolean longClick);

  /**
   * Gets called when a click is performed on a vector element's label.
   * The method is called from the main thread.
   * 
   * @param vectorElement
   *          vector element whose label was clicked
   * @param longClick
   *          true if a long click was performed, false if a normal click was performed
   */
  public abstract void onLabelClicked(VectorElement vectorElement, boolean longClick);

  /**
   * Gets called right before a click event is called for this vector
   * element. The purpose of this event is to determine if the label of the
   * vector element should be shown. This method gets called from the renderer
   * thread and only if the vector element actually has a label.
   * 
   * @param vectorElement
   *          the vector element whose label's visibility is being determined
   * @param longClick
   *          true if a long click was performed, false if a normal click was performed   
   * @return true if label should be shown, false if not
   */
  public boolean showLabelOnVectorElementClick(VectorElement vectorElement, boolean longClick) {
    return !longClick;
  }
  
  /**
   * Gets called on OpenGL surface is created (after initialization, or after surface is lost).
   * This method is called from GL renderer thread, not from main thread.
   * 
   * @param gl
   *          gl context for the new surface
   * @param config
   *          EGL configuration for the surface
   */
  public void onSurfaceCreated(GL10 gl, EGLConfig config) {
  }

  /**
   * Gets called on OpenGL surface change (f.e. orientation change).
   * This method is called from GL renderer thread, not from main thread.
   * 
   * @param gl
   *          gl context for the surface
   * @param width
   *          width of the new surface in pixels
   * @param height
   *          height of the new surface in pixels
   */
  public void onSurfaceChanged(GL10 gl, int width, int height) {
  }

  /**
   * Gets called at the start of the rendering process. This method can be used
   * to perform synchronized updates to objects before drawing.
   * This method is called from GL renderer thread, not from main thread.
   * 
   * @param gl
   *          gl context
   */
  public void onDrawFrame(GL10 gl) {
  }

  /**
   * Gets called in the middle of the rendering process, after all 2D elements
   * have been drawn (f.e. raster and geometry layers) and right before any 3D
   * elements (f.e 3d polygon, text, marker and 3d model layers). No special
   * OpenGL state clearing is performed beforehand and all the states are
   * expected to remain the same.
   * This method is called from GL renderer thread, not from main thread.
   * 
   * @param gl
   *          gl context
   * @param zoomPow2
   *          zoom step of the current view
   */
  public void onDrawFrameBefore3D(GL10 gl, float zoomPow2) {
  }

  /**
   * Gets called in the end of the rendering process, after all 3D elements have
   * already been drawn (f.e 3d polygon, text, marker and 3d model layers) and
   * right before any overlay elements (f.e vector element labels). No special
   * OpenGL state clearing is performed beforehand and all the states are
   * expected to remain the same.
   * This method is called from GL renderer thread, not from main thread.
   * 
   * 
   * @param gl
   *          gl context
   * @param zoomPow2
   *          zoom step of the current view
   */
  public void onDrawFrameAfter3D(GL10 gl, float zoomPow2) {
  }
  
  /**
   * Gets called when new background tasks are created that will update visible items in the future.
   * As background tasks may take several seconds to execute, this is a good place to show a progress indicator.
   * This method is called from main thread.
   */
  public void onBackgroundTasksStarted() {
  }

  /**
   * Gets called when all background tasks have stopped and view is already updated or will be updated very shortly afterwards.
   * This is a good place to hide progress indicator if it was displayed in onBackgroundTasksStarted.
   * This method is called from main thread.
   */
  public void onBackgroundTasksFinished() {
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public final void onMapMovedInternal() {
    Message msg = new Message();
    msg.what = ON_MAP_MOVED;
    handler.sendMessage(msg);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public final void onMapClickedInternal(double x, double y, boolean longClick) {
    Message msg = new Message();
    msg.what = ON_MAP_CLICKED;
    msg.obj = new MutableMapPos(x, y);
    msg.arg1 = longClick ? 1 : 0;
    handler.sendMessage(msg);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public final void onVectorElementClickedInternal(VectorElement vectorElement, double x, double y, boolean longClick) {
    Message msg = new Message();
    msg.what = ON_VECTOR_ELEMENT_CLICKED;
    msg.obj = new VectorElementClickEvent(vectorElement, x, y);
    msg.arg1 = longClick ? 1 : 0;
    handler.sendMessage(msg);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public final void onLabelClickedInternal(VectorElement vectorElement, boolean longClick) {
    Message msg = new Message();
    msg.what = ON_LABEL_CLICKED;
    msg.obj = vectorElement;
    msg.arg1 = longClick ? 1 : 0;
    handler.sendMessage(msg);
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized final int getBackgroundTaskCounter() {
    return backgroundTaskCounter;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized final void onBackgroundTaskStartedInternal() {
    if (backgroundTaskCounter++ == 0) {
      Message msg = new Message();
      msg.what = ON_BACKGROUND_TASKS_STARTED;
      handler.sendMessage(msg);
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized final void onBackgroundTaskFinishedInternal() {
    if (--backgroundTaskCounter == 0) {
      Message msg = new Message();
      msg.what = ON_BACKGROUND_TASKS_FINISHED;
      handler.sendMessage(msg);
    }
  }
}
