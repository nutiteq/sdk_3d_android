package com.nutiteq.geometry;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.Point3D;
import com.nutiteq.geometry.Line.LineInternalState;
import com.nutiteq.geometry.Point.PointInternalState;
import com.nutiteq.geometry.Polygon.PolygonInternalState;
import com.nutiteq.style.Style;

/**
 * Geometry class for representing vector tile elements.
 * 
 * @pad.exclude
 */
public class VectorTileGeometry extends Geometry {

  /**
   * Default constructor.
   * 
   * @param internalState
   *          internal representation for the element
   * @param style
   *          style to use for the element
   * @param userData
   *          custom user data
   */
  public VectorTileGeometry(GeometryInternalState internalState, Style style, Object userData) {
    super(null, null, userData);
    if (internalState instanceof PolygonInternalState) {
      PolygonInternalState polygonInternalState = (PolygonInternalState) internalState;
      this.internalState = new PolygonInternalState(polygonInternalState.origin, polygonInternalState.vertices, polygonInternalState.uvs, polygonInternalState.lines, polygonInternalState.points);
    } else if (internalState instanceof LineInternalState) {
      LineInternalState lineInternalState = (LineInternalState) internalState;
      this.internalState = new LineInternalState(lineInternalState.lines, lineInternalState.points);
    } else if (internalState instanceof PointInternalState) {
      PointInternalState pointInternalState = (PointInternalState) internalState;
      this.internalState = new PointInternalState(pointInternalState.points);
    }
    this.internalState.activeStyle = style;
  }
  
  @Override
  public void setActiveStyle(int zoom) {
  }

  @Override
  public void calculateInternalState() {
  }

  @Override
  public Point3D calculateInternalClickPos(Point3D clickPos) {
    return null;
  }

  @Override
  public Envelope getInternalEnvelope() {
    return null;
  }
  
  @Override
  public String toString() {
    return "VectorTileGeometry";
  }
}
