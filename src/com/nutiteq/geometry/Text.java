package com.nutiteq.geometry;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;

import com.nutiteq.components.Color;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.TextureInfo;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.StyleSet;
import com.nutiteq.style.TextStyle;
import com.nutiteq.ui.Label;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.Utils;

/**
 * Text to be shown on the map at specific location.  
 */
public class Text extends BillBoard {
  private static final int PADDING = 2;
  private static final float TO_INTERNAL_SIZE = 0.006f;
  
  private String text;

  /**
   * Default constructor (can be used if single style is sufficient for all zoom levels).
   * 
   * @param mapPos
   *        position of the text element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param text
   *        the text to display on the map.
   * @param textStyle
   *        style to use for displaying the text (font, size, color, etc). 
   * @param userData
   *        custom user data associated with the element.
   */
  public Text(MapPos mapPos, String text, TextStyle textStyle, Object userData) {
    this(mapPos, text, null, new StyleSet<TextStyle>(textStyle), userData);
  }

  /**
   * Constructor for text if text style depends on zoom level.
   * 
   * @param mapPos
   *        position of the text element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param text
   *        the text to display on the map.
   * @param styles
   *        style set to use for displaying the text. 
   * @param userData
   *        custom user data associated with the element.
   */
  public Text(MapPos mapPos, String text, StyleSet<TextStyle> styles, Object userData) {
    this(mapPos, text, null, styles, userData);
  }
  
  /**
   * Constructor for text if text needs label (can be used if single style is sufficient for all zoom levels).
   * 
   * @param mapPos
   *        position of the text element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param text
   *        the text to display on the map.
   * @param label
   *        text label
   * @param textStyle
   *        style to use for displaying the text (font, size, color, etc). 
   * @param userData
   *        custom user data associated with the element.
   */
  public Text(MapPos mapPos, String text, Label label, TextStyle textStyle, Object userData) {
    this(mapPos, text, label, new StyleSet<TextStyle>(textStyle), userData);
  }

  /**
   * Constructor for text if text style depends on zoom level and text needs label.
   * 
   * @param mapPos
   *        position of the text element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param text
   *        the text to display on the map.
   * @param label
   *        text label
   * @param styles
   *        style set to use for displaying the text. 
   * @param userData
   *        custom user data associated with the element.
   */
  public Text(MapPos mapPos, String text, Label label, StyleSet<TextStyle> styles, Object userData) {
    super(label, mapPos, styles, userData);
    this.text = text;
  }
  
  /**
   * Constructor for the case when single style is sufficient for all zoom levels and position is based on another geometric element.
   * 
   * @param baseElement
   *        base element that is used as a base for positioning the text.
   * @param text
   *        the text to display on the map.
   * @param textStyle
   *        style to use for displaying the text (font, size, color, etc). 
   * @param userData
   *        custom user data associated with the element.
   */
  public Text(BaseElement baseElement, String text, TextStyle textStyle, Object userData) {
    this(baseElement, text, new StyleSet<TextStyle>(textStyle), userData);
  }

  /**
   * Constructor for text if text style depends on zoom level and position is based on another geometric element.
   * 
   * @param baseElement
   *        base element that is used as a base for positioning the text.
   * @param text
   *        the text to display on the map.
   * @param styles
   *        style set to use for displaying the text. 
   * @param userData
   *        custom user data associated with the element.
   */
  public Text(BaseElement baseElement, String text, StyleSet<TextStyle> styles, Object userData) {
    super(null, baseElement, styles, userData);
    this.text = text;
  }
  
  /**
   * Get the style set of this element.
   * 
   * @return the style set of the element.
   */
  @SuppressWarnings("unchecked")
  @Override
  public StyleSet<TextStyle> getStyleSet() {
    return (StyleSet<TextStyle>) styleSet;
  }

  /**
   * Change the style for the element.
   * 
   * @param style
   *        the new style for the element.
   */
  public void setStyle(TextStyle style) {
    setStyleSet(new StyleSet<TextStyle>(style));
  }

  /**
   * Change the style set of this element.
   * 
   * @param styles style set for the element.
   */
  public void setStyleSet(StyleSet<TextStyle> styles) {
    if (!styles.equals(this.styleSet)) {
      this.styleSet = styles;
      notifyElementChanged();
    }
  }

  /**
   * Get the text associated with this element.
   * 
   * @return the text value of this element.
   */
  public String getText() {
    return text;
  }
  
  /**
   * Set the text value of this element.
   * 
   * @param text new text.
   */
  public void setText(String text) {
    if (!text.equals(this.text)) {
      this.text = text;
      notifyElementChanged();
    }
  }
  
  @Override
  public synchronized void setActiveStyle(int zoom) {
    TextInternalState internalState = getInternalState();
    if (internalState == null) {
      return;
    }
    TextStyle activeStyle = (TextStyle) internalState.activeStyle;
    TextStyle newActiveStyle = (visible ? (TextStyle) styleSet.getZoomStyle(zoom) : null);
    internalState.zoom = zoom;
    internalState.activeStyle = newActiveStyle;
    if (newActiveStyle == null) {
      internalState.visible = false;
    } else {
      boolean updateTextureInfo = true;
      if (activeStyle != null) {
        updateTextureInfo =
          !newActiveStyle.color.equals(activeStyle.color) ||
          !newActiveStyle.font.equals(activeStyle.font) ||
          newActiveStyle.size != activeStyle.size;
      }
      if (internalState.textInfo == null) {
        updateTextureInfo = true;
      } else if (internalState.textInfo.text != text) {
        updateTextureInfo = true;
      }
      if (updateTextureInfo) {
        internalState.textInfo = new TextInfo(text, newActiveStyle);
      }
      if (newActiveStyle.allowOverlap && baseElement == null) { // if position and visibility is fixed, mark visible
        internalState.visible = true;
      }
    }
  }
  
  @Override
  public synchronized void updateInternalPlacement(Point3D pos, float rotationAngle) {
    RenderProjection renderProjection = getRenderProjection();
    if (renderProjection == null) {
      return;
    }

    double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(pos);
    TextInfo textInfo = null;
    TextInternalState internalState = getInternalState();
    if (internalState != null) {
      if (internalState.textInfo.text.equals(text)) {
        textInfo = internalState.textInfo;
      } else {
        textInfo = new TextInfo(text, (TextStyle) internalState.activeStyle);
      }
    } else {
      textInfo = new TextInfo(text, null);
    }
    setInternalState(new TextInternalState(textInfo, pos, localFrameMatrix, rotationAngle));
  }

  @Override
  public TextInternalState getInternalState() {
    return (TextInternalState) internalState;
  }
  
  @Override
  public void calculateInternalState() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    if (baseElement != null) {
      baseElement.calculateInternalState(projection, renderProjection);
    }

    MapPos mapPosInternal = projection.toInternal(mapPos);
    Point3D pos = renderProjection.project(mapPosInternal);
    double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(pos);

    TextInternalState internalState = getInternalState();
    TextInfo textInfo = new TextInfo(text, internalState != null ? (TextStyle) internalState.activeStyle : null);
    setInternalState(new TextInternalState(textInfo, pos, localFrameMatrix, rotationAngle));
  }
  
  @Override
  public Envelope getInternalEnvelope() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    if (baseElement != null) {
      return baseElement.calculateInternalEnvelope(projection, renderProjection);
    }

    MapPos mapPosInternal = projection.toInternal(mapPos);
    return new Envelope(mapPosInternal.x, mapPosInternal.y);
  }

  @Override
  public String toString() {
    return "Text [mapPos=" + mapPos + ", \"" + text + "\"]";
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class TextInfo {
    public final String text;
    public final TextStyle textStyle;
    public final float textX;
    public final float textY;
    public final float iconX;
    public final float iconY;
    public final float textWidth;
    public final float textHeight;
    public final float textureWidth;
    public final float textureHeight;
    private int[] splitTableCache; // for each column, lower 16-bits: number of pixels to the next (left-to-right) empty column, upper 16-bits: number of pixels to the previous (right-to-left) empty column

    public TextInfo(String text, TextStyle textStyle) {
      this.text = text;
      this.textStyle = textStyle;
      if (textStyle == null) {
        this.textX = 0;
        this.textY = 0;
        this.iconX = 0;
        this.iconY = 0;
        this.textWidth = 0;
        this.textHeight = 0;
        this.textureWidth = 0;
        this.textureHeight = 0;
      } else {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextAlign(Align.LEFT);
        paint.setTypeface(textStyle.font);
        paint.setTextSize(textStyle.size);
        FontMetrics fontMetrics = paint.getFontMetrics();

        this.textWidth = paint.measureText(text) + PADDING * 2;
        this.textHeight = -fontMetrics.ascent + fontMetrics.descent + PADDING * 2;

        float textX = 0;
        float textY = 0;
        float iconX = 0;
        float iconY = 0;
        float realWidth = textWidth;
        float realHeight = textHeight;
        if (textStyle != null && textStyle.backgroundBitmap != null) {
          float widthPadding = textStyle.backgroundBitmapPaddingLeft + textStyle.backgroundBitmapPaddingRight;
          float heightPadding = textStyle.backgroundBitmapPaddingTop + textStyle.backgroundBitmapPaddingBottom;
          
          realWidth = Math.max(textWidth + widthPadding, textStyle.backgroundBitmap.getWidth());
          realHeight = Math.max(textHeight + heightPadding, textStyle.backgroundBitmap.getHeight());
          textX = textStyle.backgroundBitmapPaddingLeft + (realWidth - textWidth - widthPadding) / 2;
          textY = textStyle.backgroundBitmapPaddingTop + (realHeight - textHeight - heightPadding) / 2;
        }
        if (textStyle != null && textStyle.iconBitmap != null) {
          float width = textStyle.iconBitmap.getWidth();
          float height = textStyle.iconBitmap.getHeight();

          iconX = (realWidth * (-textStyle.iconAnchorX + 1) * 0.5f + width * (-textStyle.iconAnchorX - 1) * 0.5f);
          iconY = (realHeight * (textStyle.iconAnchorY + 1) * 0.5f + height * (textStyle.iconAnchorY - 1) * 0.5f);
          if (iconX < 0) {
            realWidth -= iconX;
            textX -= iconX;
            iconX = 0;
          }
          if (iconY < 0) {
            realHeight -= iconY;
            textY -= iconY;
            iconY = 0;
          }
          realWidth = Math.max(realWidth, iconX + width);
          realHeight = Math.max(realHeight, iconY + height);
        }
        this.textX = textX;
        this.textY = textY;
        this.iconX = iconX;
        this.iconY = iconY;
        this.textureWidth = realWidth * TO_INTERNAL_SIZE * Const.UNIT_SIZE;
        this.textureHeight = realHeight * TO_INTERNAL_SIZE * Const.UNIT_SIZE;
      }
    }
    
    public TextureInfo createTextureInfo() {
      float realWidth = textureWidth / (TO_INTERNAL_SIZE * Const.UNIT_SIZE);
      float realHeight = textureHeight / (TO_INTERNAL_SIZE * Const.UNIT_SIZE);
      Bitmap bitmap = Bitmap.createBitmap(Utils.upperPow2((int) realWidth), Utils.upperPow2((int) realHeight), Bitmap.Config.ARGB_8888);
      if (textStyle != null && textStyle.backgroundBitmap != null) {
        float widthPadding = textStyle.backgroundBitmapPaddingLeft + textStyle.backgroundBitmapPaddingRight;
        float heightPadding = textStyle.backgroundBitmapPaddingTop + textStyle.backgroundBitmapPaddingBottom;
        
        int width = (int) Math.max(textWidth + widthPadding, textStyle.backgroundBitmap.getWidth());
        int height = (int) Math.max(textHeight + heightPadding, textStyle.backgroundBitmap.getHeight());
        int x = (int) (textX - (width - textWidth - widthPadding) / 2) - textStyle.backgroundBitmapPaddingLeft; 
        int y = (int) (textY - (height - textHeight - heightPadding) / 2) - textStyle.backgroundBitmapPaddingTop; 

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(textStyle.backgroundBitmap, null, new Rect(x, y, x + width, y + height), paint);
      }
      if (textStyle != null && textStyle.iconBitmap != null) {
        int width = textStyle.iconBitmap.getWidth();
        int height = textStyle.iconBitmap.getHeight();
        int x = (int) iconX;
        int y = (int) iconY;

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(textStyle.iconBitmap, null, new Rect(x, y, x + width, y + height), paint);
      }
      return createTextureInfo(bitmap, textX, textY, 0, 0, realWidth, realHeight);
    }
    
    public TextureInfo createTextureInfo(Bitmap bitmap, float x, float y, float x0, float y0, float width, float height) {
      Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
      paint.setTextAlign(Align.LEFT);
      paint.setTypeface(textStyle.font);
      paint.setTextSize(textStyle.size);
      FontMetrics fontMetrics = paint.getFontMetrics();
      Canvas canvas = new Canvas(bitmap);

      if (textStyle.strokeWidth > 0) {
        paint.setColor(textStyle.strokeColor.colorInt);
        paint.setStyle(android.graphics.Paint.Style.STROKE);
        paint.setStrokeWidth(textStyle.strokeWidth);
        canvas.drawText(text, x, -fontMetrics.ascent + y, paint);
      }

      paint.setColor(textStyle.color.colorInt);
      paint.setStyle(android.graphics.Paint.Style.FILL);
      canvas.drawText(text, x, -fontMetrics.ascent + y, paint);

      int realWidth = Utils.upperPow2(bitmap.getWidth());
      int realHeight = Utils.upperPow2(bitmap.getHeight());
      float tx0 = x0 / realWidth;
      float ty0 = y0 / realHeight;
      float tx1 = (x0 + width) / realWidth;
      float ty1 = (y0 + height) / realHeight;
      float[] texCoords = new float[] { tx0, ty1, tx1, ty1, tx0, ty0, tx1, ty0 };
      return new TextureInfo(bitmap, texCoords, width * TO_INTERNAL_SIZE * Const.UNIT_SIZE, height * TO_INTERNAL_SIZE * Const.UNIT_SIZE);
    }
    
    public synchronized int[] getSplitTable() {
      if (splitTableCache != null) {
        return splitTableCache;
      }

      Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
      paint.setTextAlign(Align.LEFT);
      paint.setTypeface(textStyle.font);
      paint.setTextSize(textStyle.size);
      FontMetrics fontMetrics = paint.getFontMetrics();

      Bitmap bitmap = Bitmap.createBitmap((int) textWidth, (int) textHeight, Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(bitmap);
      paint.setColor(Color.BLACK);
      paint.setStyle(android.graphics.Paint.Style.FILL);
      canvas.drawText(text, 0, -fontMetrics.ascent, paint);

      // Mark all transparent columns
      boolean[] transparent = new boolean[bitmap.getWidth()];
      int[] pixels = new int[bitmap.getHeight()];
      for (int x = 0; x < bitmap.getWidth(); x++) {
        bitmap.getPixels(pixels, 0, 1, x, 0, 1, bitmap.getHeight());
        transparent[x] = true;
        for (int y = 0; y < pixels.length; y++) {
          int a = (pixels[y] >> 24) & 0xff;
          if (a > 32) {
            transparent[x] = false;
            break;
          }
        }
      }
      bitmap.recycle();
      
      // Build split able in one pass: count number of columns to next/previous transparent column
      int l = 0, r = 0;
      int[] splitTable = new int[bitmap.getWidth()];
      for (int x = 0; x < splitTable.length; x++) {
        if (!transparent[splitTable.length - x - 1]) {
          ++r;
          splitTable[splitTable.length - x - 1] = (splitTable[splitTable.length - x - 1] & 0xffff0000) + r;
        } else {
          r = 0;
        }
        if (!transparent[x]) {
          ++l;
          splitTable[x] = (splitTable[x] & 0xffff) + (l << 16);
        } else {
          l = 0;
        }
      }
      
      splitTableCache = splitTable;
      return splitTable;
    }
    
    @Override
    public int hashCode() {
      return text.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
      if (o == null) return false;
      if (this.getClass() != o.getClass()) return false;
      TextInfo other = (TextInfo) o;
      return text.equals(other.text) && textStyle == other.textStyle && textWidth == other.textWidth && textHeight == other.textHeight && textureWidth == other.textureWidth && textureHeight == other.textureHeight;
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class TextInternalState extends BillBoardInternalState {
    public TextInfo textInfo;
    
    public TextInternalState(TextInfo textInfo, Point3D pos, double[] localFrameMatrix, float rotationDeg) {
      super(pos, localFrameMatrix, rotationDeg);
      this.textInfo = textInfo;
    }

    @Override
    public float getTextureWidth() {
      TextInfo textInfo = this.textInfo;
      return textInfo != null ? textInfo.textureWidth : 0;
    }

    @Override
    public float getTextureHeight() {
      TextInfo textInfo = this.textInfo;
      return textInfo != null ? textInfo.textureHeight : 0;
    }
  }

}
