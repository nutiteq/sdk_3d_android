package com.nutiteq.geometry;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.nmlpackage.GLMesh;
import com.nutiteq.nmlpackage.GLModel;
import com.nutiteq.nmlpackage.NMLPackage;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.ModelStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;
import com.nutiteq.utils.Matrix;

/**
 * NML model on the map.
 * 
 * NML models can be created from Collada files directly and placed anywhere on the map or converted from KMZ files.
 * NML models are optimized for fast loading and contain multiple levels-of-detail for faster loading and rendering.
 * Multiple NML models can be batched together to display hundreds or thousands of simple textured objects simultaneously.
 */
public class NMLModel extends VectorElement {
  
  /**
   * Proxy element for composite NML model.
   * 
   * Each NML model can contain multiple actual objects. Each actual object inside NML model is identified by unique id
   * and can be mapped to 'proxy object'. This proxy class represents the actual instances of actual model instances.
   */
  public static class Proxy extends VectorElement {
    private MapPos mapPos;

    /**
     * Default constructor.
     * 
     * @param mapPos
     *        position of the element. Position is defined in the coordinate system of the layer this element is attached to.
     * @param label
     *        the label for the point. Label is shown when the point is selected. Can be null.
     * @param styles
     *        style set for displaying the model. 
     * @param userData
     *        custom user data associated with the element.
     */
    public Proxy(MapPos mapPos, Label label, StyleSet<ModelStyle> styles, Object userData) {
      super(label, styles, userData);
      this.mapPos = mapPos;
    }

    /**
     * Get model position on the map.
     * 
     * @return position on the map.
     */
    public MapPos getMapPos() {
      return mapPos;
    }
    
    @Override
    public ProxyInternalState getInternalState() {
      return (ProxyInternalState) internalState;
    }

    @Override
    public void calculateInternalState() {
      MapPos mapPosInternal = getProjection().toInternal(mapPos);
      Point3D pos = getRenderProjection().project(mapPosInternal);
      
      setInternalState(new ProxyInternalState(pos));
    }
    
    @Override
    public Envelope getInternalEnvelope() {
      MapPos mapPosInternal = getProjection().toInternal(mapPos);
      return new Envelope(mapPosInternal.x, mapPosInternal.y); 
    }

    @Override
    public Point3D calculateInternalClickPos(Point3D clickPos) {
      ProxyInternalState state = getInternalState();
      return state != null ? state.pos : null;
    }
    
    @Override
    public String toString() {
      return "NMLModel.Proxy";
    }

    /**
     * Not part of public API.
     * @pad.exclude
     */
    public static class ProxyInternalState extends InternalState {
      public final Point3D pos;

      ProxyInternalState(Point3D pos) {
        this.pos = pos;
      }
    }
  }
  
  private final long id;
  private final long[] parentIds;
  private final Map<Integer, Proxy> proxyMap;
  private final Map<String, Texture> textureMap;
  private final GLModel glModel;
  
  private MapPos mapPos;
  private Vector3D rotationAxis = new Vector3D(0, 0, 1);
  private float rotationAngle = 0;
  private Vector3D scale = new Vector3D(1, 1, 1);
  private boolean mapScale = true;

  private static long nextUniqueId = -1;
  
  private synchronized static long allocateUniqueId() {
    return nextUniqueId--;
  }

  /**
   * Constructor for single NML model.
   * 
   * @param mapPos
   *          model position on map
   * @param label
   *          model label
   * @param styles
   *          model drawing styles
   * @param nmlModel
   *          model description
   * @param userData
   *          custom user data
   */
  public NMLModel(MapPos mapPos, Label label, StyleSet<ModelStyle> styles, NMLPackage.Model nmlModel, Object userData) {
    super(label, styles, userData);
    this.id = allocateUniqueId();
    this.parentIds = new long[0];
    this.mapPos = new MapPos(mapPos);
    this.proxyMap = null;
    this.textureMap = new HashMap<String, Texture>();
    this.glModel = new GLModel(nmlModel);
    for (int i = 0; i < nmlModel.getTexturesCount(); i++) {
      NMLPackage.Texture nmlTexture = nmlModel.getTextures(i);
      Texture texture = new Texture(allocateUniqueId(), nmlTexture);
      textureMap.put(nmlTexture.getId(), texture);
    }
  }

  /**
   * Main constructor for single NML model.
   * 
   * @param mapPos
   *          model position on map
   * @param label
   *          model label
   * @param styles
   *          model drawing styles
   * @param inputStream
   *          input stream containing model description
   * @param userData
   *          custom user data
   * @throws IOException 
   */
  public NMLModel(MapPos mapPos, Label label, StyleSet<ModelStyle> styles, InputStream inputStream, Object userData) throws IOException {
    super(label, styles, userData);
    NMLPackage.Model nmlModel = NMLPackage.Model.parseFrom(inputStream);
    this.id = allocateUniqueId();
    this.parentIds = new long[0];
    this.mapPos = new MapPos(mapPos);
    this.proxyMap = null;
    this.textureMap = new HashMap<String, Texture>();
    this.glModel = new GLModel(nmlModel);
    for (int i = 0; i < nmlModel.getTexturesCount(); i++) {
      NMLPackage.Texture nmlTexture = nmlModel.getTextures(i);
      Texture texture = new Texture(allocateUniqueId(), nmlTexture);
      textureMap.put(nmlTexture.getId(), texture);
    }
  }

  /**
   * Main constructor for composite models (LOD tree nodes).
   * 
   * @param mapPos
   *          model position on map
   * @param label
   *          model label
   * @param styles
   *          model drawing styles
   * @param nmlModel
   *          model description
   * @param proxyMap
   *          if this model is a composite model (containing multiple subobjects) then proxyMap maps model ids to proxy objects.
   * @param id
   *          model unique id.
   * @param parentIds
   *          list of parent model ids.
   * @param userData
   *          custom user data
   * @pad.exclude
   */
  public NMLModel(MapPos mapPos, Label label, StyleSet<ModelStyle> styles, NMLPackage.Model nmlModel, Map<Integer, Proxy> proxyMap, long id, long[] parentIds, Object userData) {
    super(label, styles, userData);
    this.id = id;
    this.parentIds = parentIds;
    this.mapPos = new MapPos(mapPos);
    this.proxyMap = proxyMap;
    this.textureMap = new HashMap<String, Texture>();
    this.glModel = new GLModel(nmlModel);
    for (int i = 0; i < nmlModel.getTexturesCount(); i++) {
      NMLPackage.Texture nmlTexture = nmlModel.getTextures(i);
      Texture texture = new Texture(allocateUniqueId(), nmlTexture);
      textureMap.put(nmlTexture.getId(), texture);
    }
  }
  
  /**
   * Get model id.
   * 
   * @return model unique id.
   */
  public long getId() {
    return id;
  }

  /**
   * Get parent model ids.
   * 
   * @return array containing parent model ids.
   */
  public long[] getParentIds() {
    return parentIds;
  }

  /**
   * Get the style set of this element.
   * 
   * @return the style set of the element.
   */
  @SuppressWarnings("unchecked")
  @Override
  public StyleSet<ModelStyle> getStyleSet() {
    return (StyleSet<ModelStyle>) styleSet;
  }

  /**
   * Change the style for the element.
   * 
   * @param style
   *        the new style for the element.
   */
  public void setStyle(ModelStyle style) {
    setStyleSet(new StyleSet<ModelStyle>(style));
  }

  /**
   * Change the style set of this element.
   * 
   * @param styles style set for the element.
   */
  public void setStyleSet(StyleSet<ModelStyle> styles) {
    if (!styles.equals(this.styleSet)) {
      this.styleSet = styles;
      notifyElementChanged();
    }
  }

  /**
   * Get model position on the map.
   * 
   * @return position on the map.
   */
  public MapPos getMapPos() {
    return mapPos;
  }
  
  /**
   * Set model position on the map.
   * 
   * @param mapPos new position.
   */
  public void setMapPos(MapPos mapPos) {
    if (!mapPos.equals(this.mapPos)) {
      this.mapPos = mapPos;
      notifyElementChanged();
    }
  }

  /**
   * Get model scale for each axis. 
   * 
   * @return model scale.
   */
  public Vector3D getScale() {
    return scale;
  }
  
  /**
   * Set model scale for each axis. To have uniform scaling, just use the same number for each coordinate.
   * 
   * @param scale new scaling vector.
   */
  public void setScale(Vector3D scale) {
    if (!scale.equals(this.scale)) {
      this.scale = scale;
      notifyElementChanged();
    }
  }
  
  /**
   * Get the rotation axis.
   * 
   * @return rotation axis.
   */
  public Vector3D getRotationAxis() {
    return rotationAxis;
  }
  
  /**
   * Get the rotation angle around rotation axis.
   * 
   * @return rotation angle in degrees.
   */
  public float getRotationAngle() {
    return rotationAngle;
  }
  
  /**
   * Set the rotation axis and angle.
   * 
   * @param axis rotation axis.
   * @param angle rotation angle in degrees.
   * @throws IllegalArgumentException if null vector is given as rotation axis.
   */
  public void setRotation(Vector3D axis, float angle) {
    if (axis.getLength() == 0) {
      throw new IllegalArgumentException("Axis is null vector");
    }
    axis = axis.getNormalized();
    if (!axis.equals(this.rotationAxis) || angle != this.rotationAngle) {
      this.rotationAxis = axis;
      this.rotationAngle = angle;
      notifyElementChanged();
    }
  }
  
  /**
   * Test if model is using map scaling mode.
   * 
   * @return if true, model is scaled to match underlying base map. If false, model scale is constant across all map.
   */
  public boolean isMapScale() {
    return mapScale;
  }

  /**
   * Set scaling mode (map or global).
   * 
   * @param mapScale
   *          if true, model is scaled to match underlying base map. If false, model scale is constant across all map.
   */
  public void setMapScale(boolean mapScale) {
    if (mapScale != this.mapScale) {
      this.mapScale = mapScale;
      notifyElementChanged();
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public Map<Integer, Proxy> getProxyMap() {
    return proxyMap;
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void setMesh(String id, Mesh mesh) {
    glModel.replaceMesh(id, mesh.glMesh);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void setMesh(String id, Mesh mesh, NMLPackage.MeshOp meshOp) {
    GLMesh glMesh = mesh.glMesh;
    if (meshOp != null) {
      glMesh = new GLMesh(glMesh, meshOp);
    }
    glModel.replaceMesh(id, glMesh);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void setTexture(String id, Texture texture) {
    textureMap.put(id, texture);
  }

  @Override
  public void setActiveStyle(int zoom) {
    super.setActiveStyle(zoom);
    if (proxyMap != null) {
      for (Proxy proxy : proxyMap.values()) {
        proxy.setActiveStyle(zoom);
      }
    }
  }
  
  @Override
  public NMLModelInternalState getInternalState() {
    return (NMLModelInternalState) internalState;
  }
  
  @Override
  public void calculateInternalState() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    MapPos mapPosInternal = projection.toInternal(mapPos);
    Point3D pos = renderProjection.project(mapPosInternal);
    double[] worldTransform = calculateWorldTransform(renderProjection, mapPosInternal, scale, rotationAxis, rotationAngle, mapScale);

    setInternalState(new NMLModelInternalState(glModel, textureMap, pos, worldTransform));
  }
  
  @Override
  public Envelope getInternalEnvelope() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    MapPos mapPosInternal = projection.toInternal(mapPos);
    if (renderProjection == null) {
      return new Envelope(mapPosInternal.x, mapPosInternal.y);
    }
    
    double[] worldTransform = calculateWorldTransform(renderProjection, mapPosInternal, scale, rotationAxis, rotationAngle, mapScale);

    float[] minBounds = glModel.getMinBounds();
    float[] maxBounds = glModel.getMaxBounds();
    double[] points = new double[] { minBounds[0], minBounds[1], minBounds[2], 1, maxBounds[0], maxBounds[1], maxBounds[2], 1 };
    double[] transPoints = new double[8];
    Matrix.multiplyMV(transPoints, 0, worldTransform, 0, points, 0);
    Matrix.multiplyMV(transPoints, 4, worldTransform, 0, points, 4);

    // Calculate envelope from bounds
    MutableMapPos mapPosCorner = new MutableMapPos();
    double minX = Double.MAX_VALUE, maxX = -Double.MAX_VALUE;
    double minY = Double.MAX_VALUE, maxY = -Double.MAX_VALUE;
    for (int i = 0; i < 8; i++) {
      double x = (i & 1) == 0 ? transPoints[0] : transPoints[4];
      double y = (i & 2) == 0 ? transPoints[1] : transPoints[5];
      double z = (i & 4) == 0 ? transPoints[2] : transPoints[6];
      
      renderProjection.unproject(x, y, z, mapPosCorner);
      
      minX = Math.min(minX, mapPosCorner.x);
      minY = Math.min(minY, mapPosCorner.y);
      maxX = Math.max(maxX, mapPosCorner.x);
      maxY = Math.max(maxY, mapPosCorner.y);
    }
    return new Envelope(minX, maxX, minY, maxY);
  }
  
  static double[] calculateWorldTransform(RenderProjection renderProjection, MapPos mapPosInternal, Vector3D scale, Vector3D rotationAxis, float rotationAngle, boolean mapScale) {
    // Calculate local transform from scale and rotation
    double[] localTransform = new double[16];
    Matrix.setScaleM(localTransform, scale.x, scale.y, scale.z);
    if (rotationAngle != 0) {
      double[] rotationTransform = new double[16];
      Matrix.setRotationM(rotationTransform, rotationAxis.x, rotationAxis.y, rotationAxis.z, rotationAngle);
      double[] concatTransform = new double[16];
      Matrix.multiplyMM(concatTransform, 0, rotationTransform, 0, localTransform, 0);
      localTransform = concatTransform;
    }
    
    // Calculate world transform. Result depends whether we use map scaling or not.
    double[] globalTransform = renderProjection.getGlobalFrameMatrix(mapPosInternal, mapScale);
    double[] worldTransform = new double[16];
    Matrix.multiplyMM(worldTransform, 0, globalTransform, 0, localTransform, 0);
    return worldTransform;
  }
  
  @Override
  public Point3D calculateInternalClickPos(Point3D clickPos) {
    NMLModelInternalState state = getInternalState();
    return state != null ? state.pos : null;
  }

  @Override
  public String toString() {
    return "NMLModel [id=" + id + "]";
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class Mesh {
    public final long id;
    public final GLMesh glMesh;
    
    public Mesh(long id, NMLPackage.Mesh mesh) {
      this.id = id;
      this.glMesh = new GLMesh(mesh);
    }
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class Texture {
    public final long id;
    public final NMLPackage.Texture nmlTexture;
    
    public Texture(long id, NMLPackage.Texture texture) {
      this.id = id;
      this.nmlTexture = texture;
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class NMLModelInternalState extends InternalState {
    public final GLModel glModel;
    public final Map<String, Texture> textureMap;
    public final Point3D pos;
    public final double[] globalTransformMatrix;
    
    public NMLModelInternalState(GLModel glModel, Map<String, Texture> textureMap, Point3D pos, double[] globalTransformMatrix) {
      this.glModel = glModel;
      this.textureMap = textureMap;
      this.pos = pos;
      this.globalTransformMatrix = globalTransformMatrix;
    }
  }

}
