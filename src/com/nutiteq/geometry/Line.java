package com.nutiteq.geometry;

import java.util.ArrayList;
import java.util.List;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableEnvelope;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.geometry.Point.PointInfo;
import com.nutiteq.geometry.Point.PointInternalState;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.LineStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;
import com.nutiteq.utils.GeomUtils;

/**
 * Geometric line string on the map. 
 */
public class Line extends Geometry {
  private List<MapPos> mapPoses;
  
  /**
   * Default constructor (can be used if single style is sufficient for all zoom levels).
   * 
   * @param mapPoses
   *        list of positions. Positions are defined in the coordinate system of the layer this element is attached to.
   *        the list must contain at least 2 positions, otherwise exception will be thrown. If the list contains N positions,
   *        then N-1 lines will be actually displayed.
   * @param label
   *        the label for the line. Label is shown when the element is selected. Can be null.
   * @param lineStyle
   *        style to use for displaying the line (color, texture, etc). 
   * @param userData
   *        custom user data associated with the element.
   * @throws IllegalArgumentException if mapPoses contains less than 2 vertices
   */
  public Line(List<MapPos> mapPoses, Label label, LineStyle lineStyle, Object userData) {
    this(mapPoses, label, new StyleSet<LineStyle>(lineStyle), userData);
  }

  /**
   * Constructor for the case when style depends on zoom level.
   * 
   * @param mapPoses
   *        list of positions. Positions are defined in the coordinate system of the layer this element is attached to.
   *        the list must contain at least 2 positions, otherwise exception will be thrown. If the list contains N positions,
   *        then N-1 lines will be actually displayed.
   * @param label
   *        the label for the line. Label is shown when the element is selected. Can be null.
   * @param styles
   *        style set defining how to display the line. 
   * @param userData
   *        custom user data associated with the element.
   * @throws IllegalArgumentException if mapPoses contains less than 2 vertices
   */
  public Line(List<MapPos> mapPoses, Label label, StyleSet<LineStyle> styles, Object userData) {
    super(label, styles, userData);
    if (mapPoses.size() < 2) {
      throw new IllegalArgumentException("Line requires at least 2 vertices!");
    }
    this.mapPoses = new ArrayList<MapPos>(mapPoses);
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public StyleSet<LineStyle> getStyleSet() {
    return (StyleSet<LineStyle>) styleSet;
  }

  /**
   * Change the style for the element.
   * 
   * @param style
   *        the new style for the element.
   */
  public void setStyle(LineStyle style) {
    setStyleSet(new StyleSet<LineStyle>(style));
  }

  /**
   * Change the style set of this element.
   * 
   * @param styles style set for the element.
   */
  public void setStyleSet(StyleSet<LineStyle> styles) {
    if (!styles.equals(this.styleSet)) {
      this.styleSet = styles;
      notifyElementChanged();
    }
  }

  /**
   * Get the list of vertex points.
   * 
   * @return list of all vertex points defining the line. The list is unmodifiable. 
   */
  public List<MapPos> getVertexList() {
    return java.util.Collections.unmodifiableList(mapPoses);
  }
  
  /**
   * Change the list of vertex points.
   * 
   * @param mapPoses new list of vertex positions for the line.
   * @throws IllegalArgumentException if mapPoses contains less than 2 vertices
   */
  public void setVertexList(List<MapPos> mapPoses) {
    if (mapPoses.size() < 2) {
      throw new IllegalArgumentException("Line requires at least 2 vertices!");
    }
    if (!mapPoses.equals(this.mapPoses)) {
      this.mapPoses = new ArrayList<MapPos>(mapPoses);
      notifyElementChanged();
    }
  }
  
  @Override
  public LineInternalState getInternalState() {
    return (LineInternalState) internalState;
  }
  
  @Override
  public void calculateInternalState() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    // Check if point style is used
    boolean hasPointStyle = false;
    if (styleSet != null) {
      for (StyleSet.ZoomStyle<?> zoomStyle : styleSet.getZoomStyles()) {
        LineStyle style = (LineStyle) zoomStyle.style;
        if (style != null && style.pointStyle != null) {
          hasPointStyle = true;
        }
      }
    }

    // Calculate line coordinate in internal coordinate space
    List<MapPos> mapPosListInternal = new ArrayList<MapPos>(mapPoses.size() + 1);
    projectVertices(projection, mapPoses, mapPosListInternal);
    
    // Calculate edge data
    ArrayList<EdgeInfo> edges = new ArrayList<EdgeInfo>(mapPoses.size() + 1);
    buildEdges(renderProjection, mapPosListInternal, edges, false);
    ArrayList<PointInfo> points = new ArrayList<PointInfo>(mapPoses.size() + 1);
    if (hasPointStyle) {
      buildPoints(renderProjection, mapPosListInternal, points);
    }

    LineInfo[] lines = new LineInfo[] { new LineInfo(edges.toArray(new EdgeInfo[edges.size()])) };
    setInternalState(new LineInternalState(lines, points.toArray(new PointInfo[points.size()])));
  }
  
  @Override
  public Envelope getInternalEnvelope() {
    List<MapPos> mapPosListInternal = new ArrayList<MapPos>(mapPoses.size() + 1);
    projectVertices(getProjection(), mapPoses, mapPosListInternal);
    return calculateEnvelope(getRenderProjection(), mapPosListInternal, false);
  }
  
  static void projectVertices(Projection projection, List<MapPos> mapPosList, List<MapPos> mapPosListInternal) {
    for (MapPos mapPos : mapPosList) {
      MapPos mapPosInternal = projection.toInternal(mapPos.x, mapPos.y);
      mapPosListInternal.add(mapPosInternal);
    }
  }
  
  static void buildPoints(RenderProjection renderProjection, List<MapPos> mapPosListInternal, List<PointInfo> points) {
    if (renderProjection == null) {
      return;
    }
    for (MapPos mapPosInternal : mapPosListInternal) {
      Point3D point = renderProjection.project(mapPosInternal);
      double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(point);
      points.add(new PointInfo(point, localFrameMatrix));
    }
  }
  
  static void buildEdges(RenderProjection renderProjection, List<MapPos> mapPosListInternal, List<EdgeInfo> edges, boolean closed) {
    if (renderProjection == null) {
      return;
    }
    ArrayList<Point3D> pointList = new ArrayList<Point3D>();
    Point3D prevLinePoint = null;
    if (closed && mapPosListInternal.size() > 0) {
      prevLinePoint = renderProjection.project(mapPosListInternal.get(mapPosListInternal.size() - 1));
    }
    for (MapPos mapPosInternal : mapPosListInternal) {
      Point3D linePoint = renderProjection.project(mapPosInternal);

      if (prevLinePoint != null) {
        pointList.clear();
        renderProjection.tesselateLine(prevLinePoint, linePoint, pointList);

        Point3D prevPoint = null;
        Vector3D prevNormal = null;
        for (int i = 0; i < pointList.size(); i++) {
          Point3D point = pointList.get(i);
          Vector3D normal = renderProjection.getNormal(point);

          if (prevPoint != null) {
            if (!point.equals(prevPoint)) {
              Vector3D edgeNormal = normal;
              if (normal != prevNormal) {
                edgeNormal = new Vector3D(prevNormal.x + normal.x, prevNormal.y + normal.y, prevNormal.z + normal.z).getNormalized();
              }
              edges.add(new EdgeInfo(prevPoint, point, edgeNormal));
            }
          }

          prevPoint = point;
          prevNormal = normal;
        }
      }

      prevLinePoint = linePoint;
    }
  }
  
  static Envelope calculateEnvelope(RenderProjection renderProjection, List<MapPos> mapPosListInternal, boolean closed) {
    MutableEnvelope envInternal = new MutableEnvelope();
    if (renderProjection == null) {
      for (MapPos mapPosInternal : mapPosListInternal) {
        envInternal.add(mapPosInternal.x, mapPosInternal.y);
      }
    } else {
      ArrayList<Point3D> pointList = new ArrayList<Point3D>();
      Point3D prevLinePoint = null;
      if (closed && mapPosListInternal.size() > 0) {
        prevLinePoint = renderProjection.project(mapPosListInternal.get(mapPosListInternal.size() - 1));
      }
      for (MapPos linePosInternal : mapPosListInternal) {
        Point3D linePoint = renderProjection.project(linePosInternal);

        if (prevLinePoint != null) {
          pointList.clear();
          renderProjection.tesselateLine(prevLinePoint, linePoint, pointList);
          for (Point3D point : pointList) {
            MapPos mapPosInternal = renderProjection.unproject(point);
            envInternal.add(mapPosInternal.x, mapPosInternal.y);
          }
        }
      
        prevLinePoint = linePoint;
      }
    }
    return new Envelope(envInternal);
  }
  
  static Point3D calculateMidPoint(LineInfo lineInfo) {
    double lineLength = 0;
    for (EdgeInfo edge : lineInfo.edges) {
      lineLength += edge.length;
    }
    double linePos = 0;
    for (EdgeInfo edge : lineInfo.edges) {
      if (linePos + edge.length >= lineLength * 0.5) {
        double t = lineLength * 0.5 - linePos;
        return new Point3D(edge.x0 + edge.dx_du * t, edge.y0 + edge.dy_du * t, edge.z0 + edge.dz_du * t);
      }
      linePos += edge.length;
    }
    return new Point3D();
  }
  
  @Override
  public Point3D calculateInternalClickPos(Point3D clickPos) {
    LineInternalState state = getInternalState();
    if (state == null) {
      return null;
    }
    if (clickPos == null) {
      if (state.lines.length == 0) {
        return null;
      }
      return calculateMidPoint(state.lines[0]);
    }
    
    Point3D bestPoint = null;
    double bestDistance = Double.MAX_VALUE;
    for (LineInfo lineInfo : state.lines) {
      for (EdgeInfo edgeInfo : lineInfo.edges) {
        Point3D point = GeomUtils.nearestPointOnLine(new Point3D(edgeInfo.x0, edgeInfo.y0, edgeInfo.z0), new Point3D(edgeInfo.x1, edgeInfo.y1, edgeInfo.z1), clickPos);
        double distance = new Vector3D(point, clickPos).getLength();
        if (distance < bestDistance) {
          bestPoint = point;
          bestDistance = distance;
        }
      }
    }
    return bestPoint;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class EdgeInfo {
    public final double x0;
    public final double y0;
    public final double z0;
    public final double x1;
    public final double y1;
    public final double z1;
    public final double length;
    public final Vector3D normal;
    public final float dx_du;
    public final float dy_du;
    public final float dz_du;
    public final float dx_dv;
    public final float dy_dv;
    public final float dz_dv;

    public EdgeInfo(Point3D point0, Point3D point1, Vector3D normal) {
      double tx = point1.x - point0.x;
      double ty = point1.y - point0.y;
      double tz = point1.z - point0.z;
      double bx = ty * normal.z - tz * normal.y;
      double by = tz * normal.x - tx * normal.z;
      double bz = tx * normal.y - ty * normal.x;
      double tlen = Math.sqrt(tx * tx + ty * ty + tz * tz);
      double blen = Math.sqrt(bx * bx + by * by + bz * bz);
      this.x0 = point0.x;
      this.y0 = point0.y;
      this.z0 = point0.z;
      this.x1 = point1.x;
      this.y1 = point1.y;
      this.z1 = point1.z;
      this.length = tlen;
      this.normal = normal;
      this.dx_du = (float) (tx / tlen);
      this.dy_du = (float) (ty / tlen);
      this.dz_du = (float) (tz / tlen);
      this.dx_dv = (float) (bx / blen);
      this.dy_dv = (float) (by / blen);
      this.dz_dv = (float) (bz / blen);
    }
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class LineInfo {
    public final EdgeInfo[] edges;

    public LineInfo(EdgeInfo[] edges) {
      this.edges = edges;
    }
  }
  
  @Override
  public String toString() {
    return "Line [mapPoses=" + mapPoses + "]";
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class LineInternalState extends PointInternalState {
    public final LineInfo[] lines;

    public LineInternalState(LineInfo[] lines, PointInfo[] points) {
      super(points);
      this.lines = lines;
    }
  }

}
