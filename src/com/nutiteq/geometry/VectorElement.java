package com.nutiteq.geometry;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.Point3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.Style;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;
import com.nutiteq.vectordatasources.VectorDataSource;
import com.nutiteq.vectorlayers.VectorLayer;

/**
 * Abstract base class for all vector elements (points, lines, texts, models, etc).
 */
public abstract class VectorElement {
  protected long id = -1;
  protected boolean visible = true;
  protected Label label;
  protected VectorLayer<?> layer = null;
  protected VectorDataSource<?> dataSource = null;
  protected StyleSet<?> styleSet;
  protected volatile InternalState internalState;

  /**
   * User-defined custom data associated with the element.  
   */
  public Object userData;

  /**
   * Default constructor.
   * 
   * @param label
   *        label associated with this element. Can be null.
   * @param styles
   *        style set for this element. Actual style is selected by zoom level of the layer this element is attached to.
   * @param userData
   *        custom user data associated with this element.
   */
  public VectorElement(Label label, StyleSet<?> styles, Object userData) {
    this.label = label;
    this.styleSet = styles;
    this.userData = userData;
  }
  
  /**
   * Get element id.
   * 
   * @return element id.  If no id is previously defined, returns -1.
   */
  public long getId() {
    return id;
  }

  /**
   * Set element id.
   * 
   * @param id new element id
   */
  public void setId(long id) {
    this.id = id;
  }

  /**
   * Get the layer this element is attached to.
   * 
   * @return the layer this element is attached to. Can be null.
   */
  public VectorLayer<?> getLayer() {
    return layer;
  }

  /**
   * Get the data source this element is attached to.
   * 
   * @return the data source this element is attached to. Can be null.
   */
  public VectorDataSource<?> getDataSource() {
    return dataSource;
  }

  /**
   * Get the label of this element.
   * 
   * @return the label associated with this element.
   */
  public Label getLabel() {
    return label;
  }

  /**
   * Set the label of this element.
   * 
   * @param label the new label for the element.
   */
  public void setLabel(Label label) {
    if (label != this.label) {
      this.label = label;
      notifyElementChanged();
    }
  }

  /**
   * Get the style set of this element.
   * 
   * @return the style set of the element.
   */
  public StyleSet<?> getStyleSet() {
    return styleSet;
  }

  /**
   * Get the visibility flag of this element.
   *
   * @return true if element is visible, false if invisible.
   */
  public boolean isVisible() {
    return visible;
  }

  /**
   * Set the visibility flag of this element.
   *
   * @param visible new visibility value for the element.
   */
  public void setVisible(boolean visible) {
    if (visible != this.visible) {
      this.visible = visible;
      notifyElementChanged();
    }
  }

  /**
   * This method is intended for vector layers - when element is added to the layer, it has to be attached to form bidirectional link
   * between layers and elements.
   * 
   * @param layer
   *        the layer to attach this element to.
   */
  public void attachToLayer(VectorLayer<?> layer) {
    if (layer == this.layer) {
      return;
    }
    if (this.layer != null) {
      throw new UnsupportedOperationException("Element already attached to layer");
    }
    this.layer = layer;
    if (layer.getRenderProjection() != null) {
      calculateInternalState();
    }
  }

  /**
   * This method is intended for vector layers.
   * When element is removed from layer, it must be detached to drop the element-layer link.
   */
  public void detachFromLayer() {
    // Reset only layer, keep active style intact to reduce flickering due to asynchronous rendering/element handling
    layer = null;
  }
  
  /**
   * This method is intended for data sources - when element is added to the data source, it has to be attached to form bidirectional link
   * between data sources and elements.
   * 
   * @param dataSource
   *        the data source to attach this element to.
   */
  public void attachToDataSource(VectorDataSource<?> dataSource) {
    if (dataSource == this.dataSource) {
      return;
    }
    if (this.dataSource != null) {
      throw new UnsupportedOperationException("Element already attached to data source");
    }
    this.dataSource = dataSource;
  }
  
  /**
   * This method is intended for data sources.
   * When element is removed from data source, it must be detached to drop the element-data source link.
   */
  public void detachFromDataSource() {
    dataSource = null;
  }

  /**
   * This method is intended for vector layers - it will update element internal state based on zoom level.
   * 
   * @param zoom active zoom level.
   */
  public synchronized void setActiveStyle(int zoom) {
    if (internalState == null) {
      return;
    }
    internalState.zoom = zoom;
    internalState.activeStyle = (visible ? styleSet.getZoomStyle(zoom) : null);
  }

  /**
   * Not part of public API.
   * This method is intended for renderers.
   * @pad.exclude
   */
  public InternalState getInternalState() {
    return internalState;
  }

  /**
   * Not part of public API.
   * This method should be called only inside calculateInternalState - to update existing internal state.
   * 
   * @param state
   *        new internal state for the element.
   * @pad.exclude
   */
  protected void setInternalState(InternalState state) {
    if (internalState != null) {
      state.zoom = internalState.zoom;
      state.activeStyle = internalState.activeStyle;
    }
    internalState = state;
  }
  
  /**
   * Not part of public API.
   * This method should be called only vector layers - if layer internal state (like projection) changes, 
   * this method will recalculate internal state to reflect the change. The best pattern for implementation is to
   * calculate the needed state and then call setInternalState at the end with newly constructed internal state object.
   * 
   * NOTE: this method can be called from multiple threads, thus it should be implemented with this in mind.
   * @pad.exclude
   */
  public abstract void calculateInternalState();
  
  /**
   * Calculate element click position based on world coordinates.
   * This method is called only from renderer thread.
   * 
   * @param clickPos
   *        original click position in world coordinates.
   *        Note: null is a valid input, in that case default position should be returned.
   * @return recalculated click position in world coordinates.
   * @pad.exclude
   */
  public abstract Point3D calculateInternalClickPos(Point3D clickPos);
  
  /**
   * Calculate element envelope in internal coordinates.
   * 
   * @return element envelope in internal coordinates
   * @pad.exclude
   */
  public abstract Envelope getInternalEnvelope();
  
  /**
   * Not part of public API.
   * This is intended for derived classes - when object size or position is changed and internal state should be recalculated.
   * @pad.exclude
   */
  protected void notifyElementChanged() {
    VectorLayer<?> layer = this.layer;
    if (layer != null) {
      layer.onElementChanged(this);
    }
    
    VectorDataSource<?> dataSource = this.dataSource;
    if (dataSource != null) {
      dataSource.onElementChanged(this);
    }
  }
  
  protected Projection getProjection() {
    VectorLayer<?> layer = this.layer;
    if (layer != null) {
      return layer.getProjection();
    }

    VectorDataSource<?> dataSource = this.dataSource;
    if (dataSource != null) {
      return dataSource.getProjection();
    }
    return null;
  }
  
  protected RenderProjection getRenderProjection() {
    VectorLayer<?> layer = this.layer;
    if (layer != null) {
      return layer.getRenderProjection();
    }
    return null;
  }
  
  /**
   * Not part of public API.
   * This state is intended for element rendering.
   * @pad.exclude
   */
  public static class InternalState {
    public int zoom;
    public Style activeStyle;

    public InternalState() {
    }
  }

}
