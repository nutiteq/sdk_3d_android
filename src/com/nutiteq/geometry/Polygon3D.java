package com.nutiteq.geometry;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.geometry.Line.EdgeInfo;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.Polygon3DStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.GeomUtils;

/**
 * Geometric prism (defined by base polygon and height) on the map. Base polygons can be non-convex and may contain holes. 
 */
public class Polygon3D extends VectorElement {
  protected static final int VERTICES_PER_SIDE = 2 * 3;

  /**
   * Constant for scaling height internal units to meters.
   */
  public static final float METERS_TO_HEIGHT = (float) (2 * Const.UNIT_SIZE / (2 * Math.PI * Const.EARTH_RADIUS)); 

  protected List<MapPos> mapPoses;
  protected List<List<MapPos>> mapPosesHoles;
  protected float height;

  /**
   * Default constructor (can be used if single style is sufficient for all zoom levels).
   * 
   * @param mapPoses
   *        vertices of the base polygon. Vertices are defined in the coordinate system of the layer this element is attached to.
   * @param mapPosesHoles
   *        list of hole polygons. Can be null.       
   * @param height
   *        height of the prism. Height is given in internal units, if height in meters is needed, then Polygon3D.METERS_TO_HEIGHT constant can be used.
   * @param label
   *        the label for the polygon. Label is shown when the polygon is selected. Can be null.
   * @param polygonStyle
   *        style for displaying the polygon (color, texture, etc). 
   * @param userData
   *        custom user data associated with the element.
   * @throws IllegalArgumentException if mapPoses contains less than 3 vertices
   */
  public Polygon3D(List<MapPos> mapPoses, List<List<MapPos>> mapPosesHoles, float height, Label label,
      Polygon3DStyle polygonStyle, Object userData) {
    this(mapPoses, mapPosesHoles, height, label, new StyleSet<Polygon3DStyle>(polygonStyle), userData);
  }

  /**
   * Constructor for the case when style depends on zoom level.
   * 
   * @param mapPoses
   *        vertices of the base polygon. Vertices are defined in the coordinate system of the layer this element is attached to.
   * @param mapPosesHoles
   *        list of hole polygons. Can be null.        
   * @param height
   *        height of the prism. Height is given in internal units, if height in meters is needed, then Polygon3D.METERS_TO_HEIGHT constant can be used. 
   * @param label
   *        the label for the polygon. Label is shown when the polygon is selected. Can be null.
   * @param styles
   *        style set defining how to display the polygon. 
   * @param userData
   *        custom user data associated with the element.
   * @throws IllegalArgumentException if mapPoses contains less than 3 vertices
   */
  public Polygon3D(List<MapPos> mapPoses, List<List<MapPos>> mapPosesHoles, float height, Label label,  StyleSet<Polygon3DStyle> styles, Object userData) {
    super(label, styles, userData);
    
    if (mapPoses.size() < 3) {
      throw new IllegalArgumentException("Polygon3D requires at least 3 vertices!");
    }
    this.mapPoses = new ArrayList<MapPos>(mapPoses);
    if (mapPosesHoles != null) {
      List<List<MapPos>> mapPosesHoleList = new LinkedList<List<MapPos>>();
      for (List<MapPos> mapPosesHole : mapPosesHoles) {
        mapPosesHoleList.add(new ArrayList<MapPos>(mapPosesHole));
      }
      this.mapPosesHoles = mapPosesHoleList;
    }
    this.height = height;
  }

  /**
   * Get the style set of this element.
   * 
   * @return the style set of the element.
   */
  @SuppressWarnings("unchecked")
  @Override
  public StyleSet<Polygon3DStyle> getStyleSet() {
    return (StyleSet<Polygon3DStyle>) styleSet;
  }

  /**
   * Change the style for the element.
   * 
   * @param style
   *        the new style for the element.
   */
  public void setStyle(Polygon3DStyle style) {
    setStyleSet(new StyleSet<Polygon3DStyle>(style));
  }

  /**
   * Change the style set of this element.
   * 
   * @param styles style set for the element.
   */
  public void setStyleSet(StyleSet<Polygon3DStyle> styles) {
    if (!styles.equals(this.styleSet)) {
      this.styleSet = styles;
      notifyElementChanged();
    }
  }

  /**
   * Get the list of polygon vertex points.
   * 
   * @return list of all vertex points defining the polygon. The list is unmodifiable. 
   */
  public List<MapPos> getVertexList() {
    return java.util.Collections.unmodifiableList(mapPoses);
  }

  /**
   * Set the polygon vertex points.
   * 
   * @param mapPoses list of polygon vertices.
   * @throws IllegalArgumentException if mapPoses contains less than 3 vertices
   */
  public void setVertexList(List<MapPos> mapPoses) {
    if (mapPoses.size() < 3) {
      throw new IllegalArgumentException("Polygon3D requires at least 3 vertices!");
    }
    if (!mapPoses.equals(this.mapPoses)) {
      this.mapPoses = new ArrayList<MapPos>(mapPoses);
      notifyElementChanged();
    }
  }

  /**
   * Get the list of hole polygons.
   * 
   * @return list of all hole polygons. The list is unmodifiable and can be null in case there are no holes. 
   */
  public List<List<MapPos>> getHolePolygonList() {
    if (mapPosesHoles == null) {
      return null;
    }
    List<List<MapPos>> holePolyList = new ArrayList<List<MapPos>>();
    for (List<MapPos> hole : mapPosesHoles) {
      holePolyList.add(java.util.Collections.unmodifiableList(hole));
    }
    return java.util.Collections.unmodifiableList(holePolyList);
  }
  
  /**
   * Set the list of hole polygons.
   * 
   * @param mapPosesHoles list of hole polygons. Can be null.
   */
  public void setHolePolygonList(List<List<MapPos>> mapPosesHoles) {
    boolean equal = (mapPosesHoles == this.mapPosesHoles);
    if (mapPosesHoles != null) {
      equal = (this.mapPosesHoles != null && mapPosesHoles.equals(this.mapPosesHoles));
    }
    if (!equal) {
      if (mapPosesHoles != null) {
        List<List<MapPos>> mapPosesHoleList = new LinkedList<List<MapPos>>();
        for (List<MapPos> mapPosesHole : mapPosesHoles) {
          mapPosesHoleList.add(new ArrayList<MapPos>(mapPosesHole));
        }
        this.mapPosesHoles = mapPosesHoleList;
      } else {
        this.mapPosesHoles = null;
      }
      notifyElementChanged();
    }
  }

  /**
   * Get the height of the prism. Height is given in internal units, if height in meters is needed, then the result can be divided with Polygon3D.METERS_TO_HEIGHT constant.
   * 
   * @return height of the prism.
   */
  public float getHeight() {
    return height;
  }

  /**
   * Set the height of the prism. Height is given in internal units, if height in meters is used, then the value must be multiplied with Polygon3D.METERS_TO_HEIGHT constant.
   * 
   * @param height new height of the prism.
   */
  public void setHeight(float height) {
    if (height != this.height) {
      this.height = height;
      notifyElementChanged();
    }
  }

  @Override
  public Polygon3DInternalState getInternalState() {
    return (Polygon3DInternalState) internalState;
  }

  @Override
  public void calculateInternalState() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    // Project vertices and calculate edge info for outer ring
    ArrayList<MapPos> mapPosesInternal = new ArrayList<MapPos>();
    Line.projectVertices(projection, mapPoses, mapPosesInternal);
    ArrayList<EdgeInfo> edges = new ArrayList<EdgeInfo>();
    Line.buildEdges(renderProjection, mapPosesInternal, edges, true);
    
    // Project vertices and calculate edge info for inner rings
    List<ArrayList<MapPos>> mapPosesHolesInternal = new ArrayList<ArrayList<MapPos>>();
    if (mapPosesHoles != null) {
      for (List<MapPos> mapPosesHole : mapPosesHoles) {
        ArrayList<MapPos> mapPosHoleListInternal = new ArrayList<MapPos>();
        Line.projectVertices(projection, mapPosesHole, mapPosHoleListInternal);
        Line.buildEdges(renderProjection, mapPosHoleListInternal, edges, true);
        mapPosesHolesInternal.add(mapPosHoleListInternal);
      }
    }
    
    // Triangulate and tesselate roof
    ArrayList<Point3D> roofTriangles = new ArrayList<Point3D>();
    Polygon.triangulateTesselatePolygon(renderProjection, mapPosesInternal, mapPosesHolesInternal, roofTriangles);
    Point3D origin = Polygon.calculateOrigin(roofTriangles);
    
    // Project and tesselate sides
    ArrayList<ArrayList<Point3D>> sidePoints = new ArrayList<ArrayList<Point3D>>();
    ArrayList<Point3D> outerPoints = new ArrayList<Point3D>();
    projectTesselateRing(renderProjection, mapPosesInternal, outerPoints);
    sidePoints.add(outerPoints);
    int sideVerts = outerPoints.size();
    for (List<MapPos> mapPosesHole : mapPosesHolesInternal) {
      ArrayList<Point3D> holePoints = new ArrayList<Point3D>();
      projectTesselateRing(renderProjection, mapPosesHole, holePoints);
      sidePoints.add(holePoints);
      sideVerts += holePoints.size();
    }
    
    // Calculate light direction - base it on polygon origin
    Vector3D lightDir = GeomUtils.transform(Const.LIGHT_DIR, renderProjection.getLocalFrameMatrix(origin));
    
    // Allocate buffers for vertices/colors
    float[] verts = new float[(roofTriangles.size() + sideVerts * VERTICES_PER_SIDE) * 3];
    float[] colors = new float[(roofTriangles.size() + sideVerts * VERTICES_PER_SIDE) * 3];

    // Calculate side vertices/colors
    int sideIndex = roofTriangles.size();
    for (int n = 0; n < sidePoints.size(); n++) {
      List<Point3D> points = sidePoints.get(n);
 
      // Calculate vertex orientation of the polygon
      double signedArea = 0;
      for (int i = 0; i < points.size(); i++) {
        Point3D point0 = points.get(i);
        Point3D point1 = points.get((i + 1) % points.size());
        Point3D point2 = points.get((i + 2) % points.size());

        Vector3D surfaceNormal = renderProjection.getNormal(point1);
        Vector3D sideNormal = Vector3D.crossProduct(new Vector3D(point0, point1), new Vector3D(point1, point2));
        signedArea += Vector3D.dotProduct(surfaceNormal, sideNormal);
      }
      boolean clockwise = (signedArea * (n > 0 ? -1 : 1) < 0);

      // Calculate side vertices
      Point3D firstPoint = null, prevPoint = null;
      for (int tsj = 0; tsj <= points.size(); tsj++) {
        Point3D point;
        if (tsj < points.size()) {
          point = points.get(tsj);
          if (firstPoint == null) {
            firstPoint = point;
          }
        } else {
          point = firstPoint;
        }

        if (prevPoint != null) {
          int index = sideIndex * 3;
          Point3D point0 = clockwise ? prevPoint : point;
          Point3D point1 = clockwise ? point : prevPoint;

          double[] localFrameMatrix0 = renderProjection.getLocalFrameMatrix(point0);
          double dx0_dh = localFrameMatrix0[8];
          double dy0_dh = localFrameMatrix0[9];
          double dz0_dh = localFrameMatrix0[10];
          
          double[] localFrameMatrix1 = renderProjection.getLocalFrameMatrix(point1);
          double dx1_dh = localFrameMatrix1[8];
          double dy1_dh = localFrameMatrix1[9];
          double dz1_dh = localFrameMatrix1[10];
          
          verts[index + 0] = (float) (height * dx0_dh + point0.x - origin.x);
          verts[index + 1] = (float) (height * dy0_dh + point0.y - origin.y);
          verts[index + 2] = (float) (height * dz0_dh + point0.z - origin.z);
          verts[index + 6] = (float) (point0.x - origin.x);
          verts[index + 7] = (float) (point0.y - origin.y);
          verts[index + 8] = (float) (point0.z - origin.z);
          verts[index + 3] = (float) (point1.x - origin.x);
          verts[index + 4] = (float) (point1.y - origin.y);
          verts[index + 5] = (float) (point1.z - origin.z);

          verts[index + 9]  = (float) (height * dx1_dh + point1.x - origin.x);
          verts[index + 10] = (float) (height * dy1_dh + point1.y - origin.y);
          verts[index + 11] = (float) (height * dz1_dh + point1.z - origin.z);
          verts[index + 15] = (float) (height * dx0_dh + point0.x - origin.x);
          verts[index + 16] = (float) (height * dy0_dh + point0.y - origin.y);
          verts[index + 17] = (float) (height * dz0_dh + point0.z - origin.z);
          verts[index + 12] = (float) (point1.x - origin.x);
          verts[index + 13] = (float) (point1.y - origin.y);
          verts[index + 14] = (float) (point1.z - origin.z);

          // Calculate lighting intensity for the wall
          Vector3D normal = Vector3D.crossProduct(new Vector3D(point1.x - point0.x, point1.y - point0.y, point1.z - point0.z), new Vector3D(localFrameMatrix0[8], localFrameMatrix0[9], localFrameMatrix0[10]));
          float intensity = calculateIntensity(normal.getNormalized(), lightDir);
          index = sideIndex * 3;
          for (int tsj2 = 0; tsj2 < 6; tsj2++) {
            int index2 = index + tsj2 * 3;
            colors[index2 + 0] = intensity;
            colors[index2 + 1] = intensity;
            colors[index2 + 2] = intensity;
          }
          
          sideIndex += VERTICES_PER_SIDE;
        }
        prevPoint = point;
      }
    }

    // Calculate roof polygon vertices/colors
    for (int i = 0; i < roofTriangles.size(); i++) {
      int index = i * 3;
      Point3D roofPosWorld = roofTriangles.get(i);

      double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(roofPosWorld);
      double dx_dh = localFrameMatrix[8];
      double dy_dh = localFrameMatrix[9];
      double dz_dh = localFrameMatrix[10];
      
      verts[index + 0] = (float) (height * dx_dh + roofPosWorld.x - origin.x);
      verts[index + 1] = (float) (height * dy_dh + roofPosWorld.y - origin.y);
      verts[index + 2] = (float) (height * dz_dh + roofPosWorld.z - origin.z);

      float intensity = calculateIntensity(new Vector3D(dx_dh, dy_dh, dz_dh), lightDir); 
      colors[index + 0] = intensity;
      colors[index + 1] = intensity;
      colors[index + 2] = intensity;
    }

    setInternalState(new Polygon3DInternalState(origin, verts, colors));
  }
  
  @Override
  public Envelope getInternalEnvelope() {
    List<MapPos> mapPosListInternal = new ArrayList<MapPos>(mapPoses.size() + 1);
    Line.projectVertices(getProjection(), mapPoses, mapPosListInternal);
    return Line.calculateEnvelope(getRenderProjection(), mapPosListInternal, true);
  }
  
  static void projectTesselateRing(RenderProjection renderProjection, List<MapPos> mapPosListInternal, List<Point3D> pointList) {
    if (mapPosListInternal.isEmpty()) {
      return;
    }
    MapPos mapPosInternalPrev = mapPosListInternal.get(mapPosListInternal.size() - 1);
    Point3D prevPoint = renderProjection.project(mapPosInternalPrev);
    for (MapPos mapPosInternal : mapPosListInternal) {
      Point3D point = renderProjection.project(mapPosInternal);
      renderProjection.tesselateLine(prevPoint, point, pointList);
      pointList.remove(pointList.size() - 1);
      prevPoint = point;
    }
  }

  private float calculateIntensity(Vector3D norm, Vector3D lightDir) {
    float intensity = (float) -Vector3D.dotProduct(norm, lightDir) * 0.5f + 0.5f;
    return (1 - Const.AMBIENT_FACTOR) * intensity + Const.AMBIENT_FACTOR;
  } 

  @Override
  public Point3D calculateInternalClickPos(Point3D clickPos) {
    Polygon3DInternalState state = getInternalState();
    if (state == null) {
      return null;
    }
    if (clickPos != null) {
      return clickPos;
    }
    return state.origin;
  }

  @Override
  public String toString() {
    return "Polygon3D [mapPoses=" + mapPoses + ", mapPosesHoles=" + mapPosesHoles + "]";
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class Polygon3DInternalState extends InternalState {
    public final Point3D origin;
    public final float[] vertices;
    public final float[] colors;
    
    public Polygon3DInternalState(Point3D origin, float[] verts, float[] colors) {
      this.origin = origin;
      this.vertices = verts;
      this.colors = colors;
    } 

    @Deprecated
    public Polygon3DInternalState(Point3D origin, float[] verts, float[] colors, Envelope envelope) {
      this(origin, verts, colors);
    } 
  }
  
}
