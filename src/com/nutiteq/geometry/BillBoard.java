package com.nutiteq.geometry;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.geometry.Line.EdgeInfo;
import com.nutiteq.geometry.Point.PointInfo;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.BillBoardStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;

/**
 * Abstract base class for texts and markers.
 * Supports positioning and rotation and automatic element placement. 
 */
public abstract class BillBoard extends VectorElement {

  /**
   * Abstract base element to calculate the placement for the billboard. 
   */
  public static abstract class BaseElement {
    protected abstract void calculateInternalState(Projection projection, RenderProjection renderProjection);
    protected abstract Envelope calculateInternalEnvelope(Projection projection, RenderProjection renderProjection);
  }
  
  /**
   * Point base element. 
   */
  public static class BasePoint extends BaseElement {
    /**
     * Not part of public API.
     * @pad.exclude
     */
    public final MapPos mapPos;

    /**
     * Not part of public API.
     * @pad.exclude
     */
    public PointInfo point;
    
    /**
     * Default constructor.
     * 
     * @param mapPos
     *        coordinates for the base point.
     */
    public BasePoint(MapPos mapPos) {
      this.mapPos = mapPos;
    }
    
    @Override
    protected void calculateInternalState(Projection projection, RenderProjection renderProjection) {
      MapPos mapPosInternal = projection.toInternal(mapPos);
      Point3D pos = renderProjection.project(mapPosInternal);
      double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(pos);

      point = new PointInfo(pos, localFrameMatrix);
    }

    @Override
    protected Envelope calculateInternalEnvelope(Projection projection, RenderProjection renderProjection) {
      MapPos mapPosInternal = projection.toInternal(mapPos);
      return new Envelope(mapPosInternal.x, mapPosInternal.y);
    }
  }

  /**
   * Line base element. 
   */
  public static class BaseLine extends BaseElement {
    /**
     * Not part of public API.
     * @pad.exclude
     */
    public final List<MapPos> mapPoses;

    /**
     * Not part of public API.
     * @pad.exclude
     */
    public List<EdgeInfo> edges;
    
    /**
     * Default constructor.
     * 
     * @param mapPoses
     *        list of coordinates for the base line.
     */
    public BaseLine(List<MapPos> mapPoses) {
      this.mapPoses = new ArrayList<MapPos>(mapPoses);
    }

    @Override
    protected void calculateInternalState(Projection projection, RenderProjection renderProjection) {
      List<MapPos> mapPosListInternal = new ArrayList<MapPos>(mapPoses.size() + 1);
      for (MapPos mapPos : mapPoses) {
        MapPos mapPosInternal = projection.toInternal(mapPos);
        mapPosListInternal.add(mapPosInternal);
      }
      ArrayList<EdgeInfo> edges = new ArrayList<EdgeInfo>(mapPosListInternal.size() + 1);
      Line.buildEdges(renderProjection, mapPosListInternal, edges, false);

      this.edges = edges;
    }

    @Override
    protected Envelope calculateInternalEnvelope(Projection projection, RenderProjection renderProjection) {
      List<MapPos> mapPosListInternal = new ArrayList<MapPos>(mapPoses.size() + 1);
      Line.projectVertices(projection, mapPoses, mapPosListInternal);
      return Line.calculateEnvelope(renderProjection, mapPosListInternal, false);
    }
  }

  /**
   * Polygon base element. 
   */
  public static class BasePolygon extends BaseElement {
    /**
     * Not part of public API.
     * @pad.exclude
     */
    public final List<MapPos> mapPoses;

    /**
     * Not part of public API.
     * @pad.exclude
     */
    public final List<List<MapPos>> mapPosesHoles;

    /**
     * Not part of public API.
     * @pad.exclude
     */
    public PointInfo centerPoint;
    
    /**
     * Default constructor.
     * 
     * @param mapPoses
     *        list of coordinates for the base polygon.
     * @param mapPosesHoles
     *        list of base polygon holes.
     */
    public BasePolygon(List<MapPos> mapPoses, List<List<MapPos>> mapPosesHoles) {
      this.mapPoses = new ArrayList<MapPos>(mapPoses);
      if (mapPosesHoles != null) {
        this.mapPosesHoles = new LinkedList<List<MapPos>>();
        for (List<MapPos> mapPosesHole : mapPosesHoles) {
          this.mapPosesHoles.add(new ArrayList<MapPos>(mapPosesHole));
        }
      } else {
        this.mapPosesHoles = null;
      }
    }

    @Override
    protected void calculateInternalState(Projection projection, RenderProjection renderProjection) {
      double cx = 0, cy = 0, cz = 0;
      for (MapPos mapPos : mapPoses) {
        MapPos mapPosInternal = projection.toInternal(mapPos);
        Point3D pos = renderProjection.project(mapPosInternal);
        cx += pos.x;
        cy += pos.y;
        cz += pos.z;
      }
      MapPos mapPos = renderProjection.unproject(new Point3D(cx / mapPoses.size(), cy / mapPoses.size(), cz / mapPoses.size()));
      Point3D centerPos = renderProjection.project(new MapPos(mapPos.x, mapPos.y, 0));
      double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(centerPos);

      centerPoint = new PointInfo(centerPos, localFrameMatrix);
    }

    @Override
    protected Envelope calculateInternalEnvelope(Projection projection, RenderProjection renderProjection) {
      List<MapPos> mapPosListInternal = new ArrayList<MapPos>(mapPoses.size() + 1);
      Line.projectVertices(projection, mapPoses, mapPosListInternal);
      return Line.calculateEnvelope(renderProjection, mapPosListInternal, false);
    }
  }

  protected BaseElement baseElement;
  protected MapPos mapPos;
  protected float rotationAngle;

  /**
   * Constructor for billboard if its style depends on zoom level.
   * 
   * @param label
   *        label for the element
   * @param mapPos
   *        position of the element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param styles
   *        style set to use for displaying the element. 
   * @param userData
   *        custom user data associated with the element.
   */
  protected BillBoard(Label label, MapPos mapPos, StyleSet<? extends BillBoardStyle> styles, Object userData) {
    super(label, styles, userData);
    this.baseElement = null;
    this.mapPos = mapPos;
  }
  
  /**
   * Constructor for billboard if its style depends on zoom level and position is based on another geometric element.
   * 
   * @param label
   *        label for the element
   * @param baseElement
   *        base element that is used as a base for positioning the element.
   * @param styles
   *        style set to use for displaying the element. 
   * @param userData
   *        custom user data associated with the element.
   */
  protected BillBoard(Label label, BaseElement baseElement, StyleSet<? extends BillBoardStyle> styles, Object userData) {
    super(label, styles, userData);
    this.baseElement = baseElement;
    this.mapPos = new MapPos(Double.NaN, Double.NaN);
  }
  
  /**
   * Get the base element for this element that is used for automatically placing the element.
   *  
   * @return base element of the element.
   */
  public BaseElement getBaseElement() {
    return baseElement;
  }

  /**
   * Set the base element for this element.
   *  
   * @param baseElement
   *         new base element of the element.
   */
  public void setBaseElement(BaseElement baseElement) {
    if (baseElement != this.baseElement) {
      this.baseElement = baseElement;
      notifyElementChanged();
    }
  }

  /**
   * Get the position of the element.
   * 
   * @return position of the element.
   */
  public MapPos getMapPos() {
    return mapPos;
  }

  /**
   * Change the element position on the map.
   * 
   * @param mapPos
   *        the new position for the element.
   */
  public void setMapPos(MapPos mapPos) {
    if (!mapPos.equals(this.mapPos)) {
      this.mapPos = mapPos;
      notifyElementChanged();
    }
  }

  /**
   * Get the current rotation angle.
   * 
   * @return current rotation angle in degrees.
   */
  public float getRotation() {
    return rotationAngle;
  }
  
  /**
   * Set current rotation angle.
   * 
   * @param angle
   *        the new rotation angle.
   */
  public void setRotation(float angle) {
    if (angle != this.rotationAngle) {
      this.rotationAngle = angle;
      notifyElementChanged();
    }
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public abstract void updateInternalPlacement(Point3D pos, float rotationAngle);
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  @Override
  public BillBoardInternalState getInternalState() {
    return (BillBoardInternalState) internalState;
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  protected void setInternalState(BillBoardInternalState state) {
    BillBoardInternalState internalState = getInternalState();
    if (internalState != null) {
      BillBoardStyle activeStyle = (BillBoardStyle) internalState.activeStyle;
      if (activeStyle != null && activeStyle.allowOverlap) {
        state.visible = internalState.visible;
      }
      if (!state.visible && state.pos.equals(internalState.pos) && state.rotationDeg == internalState.rotationDeg) {
        state.visible = internalState.visible;
      }
    }
    super.setInternalState(state);
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  @Override
  public Point3D calculateInternalClickPos(Point3D clickPos) {
    BillBoardInternalState state = getInternalState();
    if (state == null) {
      return null;
    }
    return state.pos;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static abstract class BillBoardInternalState extends InternalState {
    public final Point3D pos;
    public final double[] localFrameMatrix;
    public final float rotationDeg;
    public volatile boolean visible;
    
    public BillBoardInternalState(Point3D pos, double[] localFrameMatrix, float rotationDeg) {
      this.pos = pos;
      this.localFrameMatrix = localFrameMatrix;
      this.rotationDeg = rotationDeg;
    }
    
    public abstract float getTextureWidth();
    public abstract float getTextureHeight();
  }

}
