package com.nutiteq.geometry;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.geometry.Line.EdgeInfo;
import com.nutiteq.geometry.Line.LineInfo;
import com.nutiteq.geometry.Line.LineInternalState;
import com.nutiteq.geometry.Point.PointInfo;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.PolygonStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.PolygonTriangulation;

/**
 * Geometric polygon on the map. Polygons can be non-convex and may contain holes. 
 */
public class Polygon extends Geometry {
  private List<MapPos> mapPoses;
  private List<List<MapPos>> mapPosesHoles;

  /**
   * Default constructor (can be used if single style is sufficient for all zoom levels and polygon does not contain any holes).
   * 
   * @param mapPoses
   *        vertices of the polygon. Vertices are defined in the coordinate system of the layer this element is attached to.
   * @param label
   *        the label for the polygon. Label is shown when the polygon is selected. Can be null.
   * @param polygonStyle
   *        style for displaying the polygon (color, texture, etc). 
   * @param userData
   *        custom user data associated with the element.
   * @throws IllegalArgumentException if mapPoses contains less than 3 vertices
   */
  public Polygon(List<MapPos> mapPoses, Label label, PolygonStyle polygonStyle, Object userData) {
    this(mapPoses, null, label, new StyleSet<PolygonStyle>(polygonStyle), userData);
  }
  
  /**
   * Constructor for the case when style depends on zoom level and polygon does not contain any holes.
   * 
   * @param mapPoses
   *        vertices of the polygon. Vertices are defined in the coordinate system of the layer this element is attached to.
   * @param label
   *        the label for the polygon. Label is shown when the polygon is selected. Can be null.
   * @param styles
   *        style set defining how to display the polygon. 
   * @param userData
   *        custom user data associated with the element.
   * @throws IllegalArgumentException if mapPoses contains less than 3 vertices
   */
  public Polygon(List<MapPos> mapPoses, Label label, StyleSet<PolygonStyle> styles, Object userData) {
    this(mapPoses, null, label, styles, userData);
  }
  
  /**
   * Constructor for the case if single style is sufficient for all zoom levels and polygon contains holes.
   * 
   * @param mapPoses
   *        vertices of the polygon. Vertices are defined in the coordinate system of the layer this element is attached to.
   * @param mapPosesHoles
   *        list of hole polygons. Can be null.        
   * @param label
   *        the label for the polygon. Label is shown when the polygon is selected. Can be null.
   * @param polygonStyle
   *        style for displaying the polygon (color, texture, etc). 
   * @param userData
   *        custom user data associated with the element.
   * @throws IllegalArgumentException if mapPoses contains less than 3 vertices
   */
  public Polygon(List<MapPos> mapPoses, List<List<MapPos>> mapPosesHoles, Label label, PolygonStyle polygonStyle, Object userData) {
    this(mapPoses, mapPosesHoles, label, new StyleSet<PolygonStyle>(polygonStyle), userData);
  }

  /**
   * Constructor for the case when style depends on zoom level and polygon contains holes.
   * 
   * @param mapPoses
   *        vertices of the polygon. Vertices are defined in the coordinate system of the layer this element is attached to.
   * @param mapPosesHoles
   *        list of hole polygons. Can be null.        
   * @param label
   *        the label for the polygon. Label is shown when the polygon is selected. Can be null.
   * @param styles
   *        style set defining how to display the polygon. 
   * @param userData
   *        custom user data associated with the element.
   * @throws IllegalArgumentException if mapPoses contains less than 3 vertices
   */
  public Polygon(List<MapPos> mapPoses, List<List<MapPos>> mapPosesHoles, Label label, StyleSet<PolygonStyle> styles, Object userData) {
    super(label, styles, userData);
    if (mapPoses.size() < 3) {
      throw new IllegalArgumentException("Polygon requires at least 3 vertices!");
    }
    this.mapPoses = new ArrayList<MapPos>(mapPoses);
    if (mapPosesHoles != null) {
      List<List<MapPos>> mapPosesHoleList = new LinkedList<List<MapPos>>();
      for (List<MapPos> mapPosesHole : mapPosesHoles) {
        mapPosesHoleList.add(new ArrayList<MapPos>(mapPosesHole));
      }
      this.mapPosesHoles = mapPosesHoleList;
    }
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public StyleSet<PolygonStyle> getStyleSet() {
    return (StyleSet<PolygonStyle>) styleSet;
  }

  /**
   * Change the style for the element.
   * 
   * @param style
   *        the new style for the element.
   */
  public void setStyle(PolygonStyle style) {
    setStyleSet(new StyleSet<PolygonStyle>(style));
  }

  /**
   * Change the style set of this element.
   * 
   * @param styles style set for the element.
   */
  public void setStyleSet(StyleSet<PolygonStyle> styles) {
    if (!styles.equals(this.styleSet)) {
      this.styleSet = styles;
      notifyElementChanged();
    }
  }

  /**
   * Get the list of polygon vertex points.
   * 
   * @return list of all vertex points defining the polygon. The list is unmodifiable. 
   */
  public List<MapPos> getVertexList() {
    return java.util.Collections.unmodifiableList(mapPoses);
  }

  /**
   * Set the polygon vertex points.
   * 
   * @param mapPoses list of polygon vertices.
   * @throws IllegalArgumentException if mapPoses contains less than 3 vertices
   */
  public void setVertexList(List<MapPos> mapPoses) {
    if (mapPoses.size() < 3) {
      throw new IllegalArgumentException("Polygon requires at least 3 vertices!");
    }
    if (!mapPoses.equals(this.mapPoses)) {
      this.mapPoses = new ArrayList<MapPos>(mapPoses);
      notifyElementChanged();
    }
  }

  /**
   * Get the list of hole polygons.
   * 
   * @return list of all hole polygons. The list is unmodifiable and can be null if there are no holes. 
   */
  public List<List<MapPos>> getHolePolygonList() {
    if (mapPosesHoles == null) {
      return null;
    }
    List<List<MapPos>> holePolyList = new ArrayList<List<MapPos>>();
    for (List<MapPos> hole : mapPosesHoles) {
      holePolyList.add(java.util.Collections.unmodifiableList(hole));
    }
    return java.util.Collections.unmodifiableList(holePolyList);
  }

  /**
   * Set the list of hole polygons.
   * 
   * @param mapPosesHoles list of hole polygons. Can be null.
   */
  public void setHolePolygonList(List<List<MapPos>> mapPosesHoles) {
    boolean equal = (mapPosesHoles == this.mapPosesHoles);
    if (mapPosesHoles != null) {
      equal = (this.mapPosesHoles != null && mapPosesHoles.equals(this.mapPosesHoles));
    }
    if (!equal) {
      if (mapPosesHoles != null) {
        List<List<MapPos>> mapPosesHoleList = new LinkedList<List<MapPos>>();
        for (List<MapPos> mapPosesHole : mapPosesHoles) {
          mapPosesHoleList.add(new ArrayList<MapPos>(mapPosesHole));
        }
        this.mapPosesHoles = mapPosesHoleList;
      } else {
        this.mapPosesHoles = null;
      }
      notifyElementChanged();
    }
  }
  
  @Override
  public void setActiveStyle(int zoom) {
    if (internalState == null) {
      return;
    }
    super.setActiveStyle(zoom);

    // Build uvs, if polygon is patterned
    float[] uvs = null;
    PolygonInternalState internalState = getInternalState(); 
    PolygonStyle polygonStyle = (PolygonStyle) internalState.activeStyle; 
    if (polygonStyle.patternTextureInfo != null) {
      Projection projection = getProjection();
      RenderProjection renderProjection = getRenderProjection();
      RenderProjection.Transform transform = renderProjection.getTransform(projection);

      int pointCount = internalState.vertices.length / 3;
      uvs = new float[pointCount * 2];
      double scaleX = polygonStyle.patternTextureInfo.width * projection.getBounds().getWidth() / Const.UNIT_SIZE;
      double scaleY = polygonStyle.patternTextureInfo.height * projection.getBounds().getHeight() / Const.UNIT_SIZE;
      MutableMapPos mapPos = new MutableMapPos();
      double u0 = 0, v0 = 0;
      for (int i = 0; i < pointCount; i++) {
        transform.unproject(internalState.vertices[i * 3 + 0], internalState.vertices[i * 3 + 1], internalState.vertices[i * 3 + 2], mapPos);
        double u = mapPos.x / scaleX;
        double v = mapPos.y / scaleY;
        if (i == 0) {
          u0 = Math.floor(u);
          v0 = Math.floor(v);
        }
        uvs[i * 2 + 0] = (float) (u - u0);
        uvs[i * 2 + 1] = (float) (v - v0);
      }
    }
    internalState.uvs = uvs;
  }

  @Override
  public PolygonInternalState getInternalState() {
    return (PolygonInternalState) internalState;
  }
  
  @Override
  public void calculateInternalState() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();
    
    // Check if line style is used
    boolean hasLineStyle = false, hasPointStyle = false;
    if (styleSet != null) {
      for (StyleSet.ZoomStyle<?> zoomStyle : styleSet.getZoomStyles()) {
        PolygonStyle style = (PolygonStyle) zoomStyle.style;
        if (style != null && style.lineStyle != null) {
          hasLineStyle = true;
          if (style.lineStyle.pointStyle != null) {
            hasPointStyle = true;
          }
        }
      }
    }

    // Project vertices and calculate edge info for outer ring
    ArrayList<MapPos> mapPosesInternal = new ArrayList<MapPos>(mapPoses.size() + 1);
    Line.projectVertices(projection, mapPoses, mapPosesInternal);
    ArrayList<LineInfo> lines = new ArrayList<LineInfo>();
    if (hasLineStyle) {
      ArrayList<EdgeInfo> edges = new ArrayList<EdgeInfo>();
      Line.buildEdges(renderProjection, mapPosesInternal, edges, true);
      lines.add(new LineInfo(edges.toArray(new EdgeInfo[edges.size()])));
    }
    ArrayList<PointInfo> points = new ArrayList<PointInfo>();
    if (hasPointStyle) {
      Line.buildPoints(renderProjection, mapPosesInternal, points);
    }
    
    // Project vertices and calculate edge info for inner rings
    List<ArrayList<MapPos>> mapPosesHolesInternal = new ArrayList<ArrayList<MapPos>>();
    if (mapPosesHoles != null) {
      for (List<MapPos> mapPosesHole : mapPosesHoles) {
        ArrayList<MapPos> mapPosHoleListInternal = new ArrayList<MapPos>(mapPosesHole.size() + 1);
        Line.projectVertices(projection, mapPosesHole, mapPosHoleListInternal);
        if (hasLineStyle) {
          ArrayList<EdgeInfo> edges = new ArrayList<EdgeInfo>();
          Line.buildEdges(renderProjection, mapPosHoleListInternal, edges, true);
          lines.add(new LineInfo(edges.toArray(new EdgeInfo[edges.size()])));
        }
        if (hasPointStyle) {
          Line.buildPoints(renderProjection, mapPosHoleListInternal, points);
        }
        mapPosesHolesInternal.add(mapPosHoleListInternal);
      }
    }
    
    // Triangulate and tesselate
    ArrayList<Point3D> triangles = new ArrayList<Point3D>();
    triangulateTesselatePolygon(renderProjection, mapPosesInternal, mapPosesHolesInternal, triangles);
    Point3D origin = calculateOrigin(triangles);

    // Create triangle vertex array
    float[] vertices = new float[triangles.size() * 3];
    for (int i = 0; i < triangles.size(); i++) {
      Point3D point = triangles.get(i);
      vertices[i * 3 + 0] = (float) (point.x - origin.x);
      vertices[i * 3 + 1] = (float) (point.y - origin.y);
      vertices[i * 3 + 2] = (float) (point.z - origin.z);
    }
    
    // Optionally create uvs
    setInternalState(new PolygonInternalState(origin, vertices, null, lines.toArray(new LineInfo[lines.size()]), points.toArray(new PointInfo[points.size()])));
  }
  
  @Override
  public Envelope getInternalEnvelope() {
    List<MapPos> mapPosListInternal = new ArrayList<MapPos>(mapPoses.size() + 1);
    Line.projectVertices(getProjection(), mapPoses, mapPosListInternal);
    return Line.calculateEnvelope(getRenderProjection(), mapPosListInternal, true);
  }
  
  static void triangulateTesselatePolygon(RenderProjection renderProjection, ArrayList<MapPos> mapPosesInternal, List<ArrayList<MapPos>> mapPosesHolesInternal, List<Point3D> triangles) {
    // Triangulate
    List<MapPos> trianglesInternal = PolygonTriangulation.triangulate(mapPosesInternal, mapPosesHolesInternal);
    
    // Tesselate
    for (int i = 0; i < trianglesInternal.size(); i += 3) {
      Point3D point0 = renderProjection.project(trianglesInternal.get(i + 0));
      Point3D point1 = renderProjection.project(trianglesInternal.get(i + 1));
      Point3D point2 = renderProjection.project(trianglesInternal.get(i + 2));
      
      // Change triangle ordering if needed
      Vector3D surfaceNormal = renderProjection.getNormal(point1);
      Vector3D triangleNormal = Vector3D.crossProduct(new Vector3D(point0, point1), new Vector3D(point1, point2));
      if (Vector3D.dotProduct(surfaceNormal, triangleNormal) < 0) {
        Point3D temp = point1;
        point1 = point2;
        point2 = temp;
      }
      renderProjection.tesselateTriangle(point0, point1, point2, triangles);
    }    
  }
  
  static Point3D calculateOrigin(List<Point3D> points) {
    double x = 0, y = 0, z = 0;
    for (Point3D point : points) {
      x += point.x;
      y += point.y;
      z += point.z;
    }
    return new Point3D(x / points.size(), y / points.size(), z / points.size());
  }

  @Override
  public Point3D calculateInternalClickPos(Point3D clickPos) {
    PolygonInternalState state = getInternalState();
    if (state == null) {
      return null;
    }
    if (clickPos != null) {
      return clickPos;
    }
    return state.origin;
  }

  @Override
  public String toString() {
    return "Polygon [mapPoses=" + mapPoses + ", mapPosesHoles=" + mapPosesHoles + "]";
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class PolygonInternalState extends LineInternalState {
    public final Point3D origin;
    public final float[] vertices;
    public float[] uvs;

    public PolygonInternalState(Point3D origin, float[] vertices, float[] uvs, LineInfo[] lines, PointInfo[] points) {
      super(lines, points);
      this.origin = origin;
      this.vertices = vertices;
      this.uvs = uvs;
    }
  }
}
