package com.nutiteq.geometry;

import android.view.MotionEvent;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.Components;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.layers.Layer;
import com.nutiteq.renderers.MapRenderer;
import com.nutiteq.style.MarkerStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;
import com.nutiteq.ui.MapTouchListener;
import com.nutiteq.ui.MarkerDragListener;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.Utils;

/**
 * Dynamic marker on the map.
 *  
 * This is similar to normal marker with support for being 'draggable' - user can move the marker on the map.
 */
public class DynamicMarker extends Marker {
  private volatile boolean draggable = false;
  private double dragDeltaX = 0;
  private double dragDeltaY = 0;
  private MarkerDragListener markerDragListener = null;
  private MarkerTouchListener markerTouchListener = null;
  
  private class MarkerTouchListener implements MapTouchListener {
    @Override
    public boolean onTouchEvent(MotionEvent event) {
      if (event.getPointerCount() > 1) {
        return false;
      }
      switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        return true;
      case MotionEvent.ACTION_MOVE:
        onDrag(event.getX(), event.getY());
        return true;
      case MotionEvent.ACTION_CANCEL:
        onDragEnd();
        return true;
      case MotionEvent.ACTION_UP:
        onDrag(event.getX(), event.getY());
        onDragEnd();
        return true;
      }   
      return false;
    }
  }

  /**
   * Default constructor (can be used if single style is sufficient for all zoom levels).
   * 
   * @param mapPos
   *        position of the element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param label
   *        the label for the marker. Label is shown when the marker is selected. Can be null.
   * @param markerStyle
   *        style for displaying the marker (color, texture, etc). 
   * @param userData
   *        custom user data associated with the element.
   */
  public DynamicMarker(MapPos mapPos, Label label, MarkerStyle markerStyle, Object userData) {
    this(mapPos, label, new StyleSet<MarkerStyle>(markerStyle), userData);
  }

  /**
   * Constructor for the case when style depends on zoom level.
   * 
   * @param mapPos
   *        position of the element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param label
   *        the label for the marker. Label is shown when the marker is selected. Can be null.
   * @param styles
   *        style set defining how to display the marker. 
   * @param userData
   *        custom user data associated with the element.
   */
  public DynamicMarker(MapPos mapPos, Label label, StyleSet<MarkerStyle> styles, Object userData) {
    super(mapPos, label, styles, userData);
  }

  /**
   * Check if marker is draggable.
   * 
   * @return true if marker can be dragged, false otherwise.
   */
  public boolean isDraggable() {
    return draggable;
  }
  
  /**
   * Set marker draggable flag.
   * 
   * @param draggable true if marker can be dragged, false otherwise.
   */
  public void setDraggable(boolean draggable) {
    this.draggable = draggable;
  }
  
  /**
   * Get attached marker drag listener.
   * 
   * @return listener instance or null if listener is not set via setDragListener.
   */
  public MarkerDragListener getDragListener() {
    return markerDragListener;
  }
  
  /**
   * Set marker drag listener.
   * 
   * @param listener listener instance. Can be null.
   */
  public void setDragListener(MarkerDragListener listener) {
    this.markerDragListener = listener;
  }
  
  /**
   * Not part of public API.
   * 
   * @pad.exclude
   */
  public boolean onDragStart(float x, float y) {
    if (!draggable) {
      return false;
    }

    Layer layer = getLayer();
    if (layer == null) {
      return false;
    }
    Components components = layer.getComponents();
    if (components == null) {
      return false;
    }
    MapRenderer mapRenderer = components.mapRenderers.getMapRenderer();
    if (mapRenderer == null) {
      return false;
    }
    
    Point3D pos = mapRenderer.getRenderSurface().getRenderProjection().project(layer.getProjection().toInternal(getMapPos()));
    MapPos screenPos = mapRenderer.worldToScreen(pos.x, pos.y, pos.z);
    
    double maxDeltaX = 0, maxDeltaY = 0;
    MarkerInternalState internalState = getInternalState();
    if (internalState != null) {
      MarkerStyle style = (MarkerStyle) internalState.activeStyle;
      if (style != null) {
        maxDeltaX = style.pickingTextureInfo.width  * mapRenderer.getView().getWidth()  / Const.UNIT_SIZE / 16;
        maxDeltaY = style.pickingTextureInfo.height * mapRenderer.getView().getHeight() / Const.UNIT_SIZE / 16;
      }
    }
    if (!(Math.abs(screenPos.x - x) < maxDeltaX && Math.abs(screenPos.y - y) < maxDeltaY)) {
      components.options.removeMapTouchListener(markerTouchListener);
      markerTouchListener = null;
      return false;
    }
    dragDeltaX = screenPos.x - x;
    dragDeltaY = screenPos.y - y;

    if (markerTouchListener != null) {
      components.options.removeMapTouchListener(markerTouchListener);
      markerTouchListener = null;
    }
    markerTouchListener = new MarkerTouchListener();
    components.options.addMapTouchListener(markerTouchListener);

    MarkerDragListener listener = markerDragListener;
    if (listener != null) {
      listener.onDragStart(this);
    }
    return true;
  }
  
  private void onDrag(float x, float y) {
    Layer layer = getLayer();
    if (layer == null) {
      return;
    }
    Components components = layer.getComponents();
    if (components == null) {
      return;
    }
    MapRenderer mapRenderer = components.mapRenderers.getMapRenderer();
    if (mapRenderer == null) {
      return;
    }

    Point3D point = mapRenderer.screenToWorld(x + dragDeltaX, y + dragDeltaY, true);
    MapPos mapPos = layer.getProjection().fromInternal(mapRenderer.getRenderSurface().getRenderProjection().unproject(point));
    
    Bounds bounds = layer.getProjection().getBounds();
    mapPos = new MapPos(Utils.toRange(mapPos.x, bounds.left, bounds.right), Utils.toRange(mapPos.y, bounds.bottom, bounds.top));
    
    MarkerDragListener listener = markerDragListener;
    if (listener != null) {
      if (listener.onDrag(this, mapPos)) {
        setMapPos(mapPos);
      }
    } else {
      setMapPos(mapPos);
    }
  }
  
  private void onDragEnd() {
    Layer layer = getLayer();
    if (layer != null) {
      Components components = layer.getComponents();
      if (components != null) {
        components.options.removeMapTouchListener(markerTouchListener);
        markerTouchListener = null;
      }
    }
    
    MarkerDragListener listener = markerDragListener;
    if (listener != null) {
      listener.onDragEnd(this);
    }
    dragDeltaX = 0;
    dragDeltaY = 0;
  }
}
