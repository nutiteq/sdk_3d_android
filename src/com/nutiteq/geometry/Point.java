package com.nutiteq.geometry;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.PointStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;

/**
 * Geometric point on the map. 
 */
public class Point extends Geometry {
  private MapPos mapPos;

  /**
   * Default constructor (can be used if single style is sufficient for all zoom levels).
   * 
   * @param mapPos
   *        position of the element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param label
   *        the label for the point. Label is shown when the point is selected. Can be null.
   * @param pointStyle
   *        style for displaying the point (color, texture, etc). 
   * @param userData
   *        custom user data associated with the element.
   */
  public Point(MapPos mapPos, Label label, PointStyle pointStyle, Object userData) {
    this(mapPos, label, new StyleSet<PointStyle>(pointStyle), userData);
  }

  /**
   * Constructor for the case when style depends on zoom level.
   * 
   * @param mapPos
   *        position of the element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param label
   *        the label for the point. Label is shown when the point is selected. Can be null.
   * @param styles
   *        style set defining how to display the point. 
   * @param userData
   *        custom user data associated with the element.
   */
  public Point(MapPos mapPos, Label label, StyleSet<PointStyle> styles, Object userData) {
    super(label, styles, userData);
    this.mapPos = mapPos;
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public StyleSet<PointStyle> getStyleSet() {
    return (StyleSet<PointStyle>) styleSet;
  }

  /**
   * Change the style for the element.
   * 
   * @param style
   *        the new style for the element.
   */
  public void setStyle(PointStyle style) {
    setStyleSet(new StyleSet<PointStyle>(style));
  }

  /**
   * Change the style set of this element.
   * 
   * @param styles style set for the element.
   */
  public void setStyleSet(StyleSet<PointStyle> styles) {
    if (!styles.equals(this.styleSet)) {
      this.styleSet = styles;
      notifyElementChanged();
    }
  }

  /**
   * Get the position of the point.
   * 
   * @return position of the point.
   */
  public MapPos getMapPos() {
    return mapPos;
  }

  /**
   * Change the point position on the map.
   * 
   * @param mapPos
   *        the new position for the point.
   */
  public void setMapPos(MapPos mapPos) {
    if (!mapPos.equals(this.mapPos)) {
      this.mapPos = mapPos;
      notifyElementChanged();
    }
  }

  @Override
  public PointInternalState getInternalState() {
    return (PointInternalState) internalState;
  }
  
  @Override
  public void calculateInternalState() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    MapPos mapPosInternal = projection.toInternal(mapPos.x, mapPos.y);
    Point3D pos = renderProjection.project(mapPosInternal);
    double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(pos);
    
    PointInfo[] points = new PointInfo[] { new PointInfo(pos, localFrameMatrix) };
    setInternalState(new PointInternalState(points));
  }
  
  @Override
  public Envelope getInternalEnvelope() {
    MapPos mapPosInternal = getProjection().toInternal(mapPos.x, mapPos.y);
    return new Envelope(mapPosInternal.x, mapPosInternal.y);
  }

  @Override
  public Point3D calculateInternalClickPos(Point3D clickPos) {
    PointInternalState state = getInternalState();
    if (state == null) {
      return null;
    }
    if (clickPos == null) {
      if (state.points.length == 0) {
        return null;
      }
      return new Point3D(state.points[0].x, state.points[0].y, state.points[0].z);
    }

    Point3D bestPoint = null;
    double bestDistance = Double.MAX_VALUE;
    for (PointInfo pointInfo : state.points) {
      Point3D point = new Point3D(pointInfo.x, pointInfo.y, pointInfo.z);
      double distance = new Vector3D(point, clickPos).getLength();
      if (distance < bestDistance) {
        bestPoint = point;
        bestDistance = distance;
      }
    }
    return bestPoint;
  }

  @Override
  public String toString() {
    return "Point [mapPos=" + mapPos + "]";
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class PointInfo {
    public final double x;
    public final double y;
    public final double z;
    public final float dx_du;
    public final float dy_du;
    public final float dz_du;
    public final float dx_dv;
    public final float dy_dv;
    public final float dz_dv;

    public PointInfo(Point3D pos, double[] localFrameMatrix) {
      x = pos.x;
      y = pos.y;
      z = pos.z;
      dx_du = (float) localFrameMatrix[0];
      dy_du = (float) localFrameMatrix[1];
      dz_du = (float) localFrameMatrix[2];
      dx_dv = (float) localFrameMatrix[4];
      dy_dv = (float) localFrameMatrix[5];
      dz_dv = (float) localFrameMatrix[6];
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class PointInternalState extends GeometryInternalState {
    public final PointInfo[] points;

    public PointInternalState(PointInfo[] points) {
      this.points = points;
    }
  }

}
