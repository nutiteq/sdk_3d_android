package com.nutiteq.geometry;

import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;

/**
 * Abstract base class for all geometric vector elements (points, lines, ...). 
 */
public abstract class Geometry extends VectorElement {

  /**
   * Default constructor.
   * 
   * @param label
   *        label associated with this element. Can be null.
   * @param styles
   *        style set for this element. Actual style is selected by zoom level of the layer this element is attached to.
   * @param userData
   *        custom user data associated with this element.
   */
  public Geometry(Label label, StyleSet<?> styles, Object userData) {
    super(label, styles, userData);
  }

  @Override
  public GeometryInternalState getInternalState() {
    return (GeometryInternalState) internalState;
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class GeometryInternalState extends InternalState {
    public GeometryInternalState() {
    }
  }
}
