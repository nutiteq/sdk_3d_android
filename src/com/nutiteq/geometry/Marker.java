package com.nutiteq.geometry;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.MarkerStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.ui.Label;

/**
 * Marker on the map. 
 */
public class Marker extends BillBoard {
  private int displayOrder;

  /**
   * Default constructor (can be used if single style is sufficient for all zoom levels).
   * 
   * @param mapPos
   *        position of the element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param label
   *        the label for the marker. Label is shown when the marker is selected. Can be null.
   * @param markerStyle
   *        style for displaying the marker (color, texture, etc). 
   * @param userData
   *        custom user data associated with the element.
   */
  public Marker(MapPos mapPos, Label label, MarkerStyle markerStyle, Object userData) {
    this(mapPos, label, new StyleSet<MarkerStyle>(markerStyle), userData);
  }

  /**
   * Constructor for the case when style depends on zoom level.
   * 
   * @param mapPos
   *        position of the element. Position is defined in the coordinate system of the layer this element is attached to.
   * @param label
   *        the label for the marker. Label is shown when the marker is selected. Can be null.
   * @param styles
   *        style set defining how to display the marker. 
   * @param userData
   *        custom user data associated with the element.
   */
  public Marker(MapPos mapPos, Label label, StyleSet<MarkerStyle> styles, Object userData) {
    super(label, mapPos, styles, userData);
  }
  
  /**
   * Constructor for the case when single style is sufficient for all zoom levels and position is based on another geometric element.
   * 
   * @param baseElement
   *        base element that is used as a base for positioning the marker.
   * @param markerStyle
   *        style to use for displaying the marker (size, color, etc). 
   * @param userData
   *        custom user data associated with the element.
   */
  public Marker(BaseElement baseElement, MarkerStyle markerStyle, Object userData) {
    this(baseElement, new StyleSet<MarkerStyle>(markerStyle), userData);
  }

  /**
   * Constructor for marker if its style depends on zoom level and position is based on another geometric element.
   * 
   * @param baseElement
   *        base element that is used as a base for positioning the marker.
   * @param styles
   *        style set to use for displaying the marker. 
   * @param userData
   *        custom user data associated with the element.
   */
  public Marker(BaseElement baseElement, StyleSet<MarkerStyle> styles, Object userData) {
    super(null, baseElement, styles, userData);
  }
  
  /**
   * Get the style set of this element.
   * 
   * @return the style set of the element.
   */
  @SuppressWarnings("unchecked")
  @Override
  public StyleSet<MarkerStyle> getStyleSet() {
    return (StyleSet<MarkerStyle>) styleSet;
  }

  /**
   * Change the style for the element.
   * 
   * @param style
   *        the new style for the element.
   */
  public void setStyle(MarkerStyle style) {
    setStyleSet(new StyleSet<MarkerStyle>(style));
  }

  /**
   * Change the style set of this element.
   * 
   * @param styles style set for the element.
   */
  public void setStyleSet(StyleSet<MarkerStyle> styles) {
    if (!styles.equals(this.styleSet)) {
      this.styleSet = styles;
      notifyElementChanged();
    }
  }

  /**
   * Get marker display order. Markers with higher order values are displayed on top of markers with lower order values.
   * 
   * @return marker display order. Default is 0.
   */
  public int getDisplayOrder() {
    return displayOrder;
  }

  /**
   * Change marker display order. Markers with higher order values are displayed on top of markers with lower order values.
   * Note: this really works only when the corresponding layer is set to "2D mode", by calling MarkerLayer.setZOrdering(false). Otherwise the ordering is device-specific.
   * 
   * @param displayOrder
   *        relative marker display order.
   */
  public void setDisplayOrder(int displayOrder) {
    if (displayOrder != this.displayOrder) { 
      this.displayOrder = displayOrder;
      notifyElementChanged();
    }
  }

  @Override
  public synchronized void setActiveStyle(int zoom) {
    MarkerInternalState internalState = getInternalState();
    if (internalState == null) {
      return;
    }
    MarkerStyle newActiveStyle = (visible ? (MarkerStyle) styleSet.getZoomStyle(zoom) : null);
    internalState.zoom = zoom;
    internalState.activeStyle = newActiveStyle;
    if (newActiveStyle == null) {
      internalState.visible = false;
    } else {
      if (newActiveStyle.allowOverlap && baseElement == null) { // if position and visibility is fixed, mark visible
        internalState.visible = true;
      }
    }
  }

  @Override
  public synchronized void updateInternalPlacement(Point3D pos, float rotationAngle) {
    RenderProjection renderProjection = getRenderProjection();
    if (renderProjection == null) {
      return;
    }

    double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(pos);
    setInternalState(new MarkerInternalState(pos, localFrameMatrix, rotationAngle));
  }

  @Override
  public MarkerInternalState getInternalState() {
    return (MarkerInternalState) internalState;
  }
  
  @Override
  public void calculateInternalState() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    if (baseElement != null) {
      baseElement.calculateInternalState(projection, renderProjection);
    }

    MapPos mapPosInternal = projection.toInternal(mapPos);
    Point3D pos = renderProjection.project(mapPosInternal);
    double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(pos);

    setInternalState(new MarkerInternalState(pos, localFrameMatrix, rotationAngle));
  }
  
  @Override
  public Envelope getInternalEnvelope() {
    Projection projection = getProjection();
    RenderProjection renderProjection = getRenderProjection();

    if (baseElement != null) {
      return baseElement.calculateInternalEnvelope(projection, renderProjection);
    }

    MapPos mapPosInternal = projection.toInternal(mapPos);
    return new Envelope(mapPosInternal.x, mapPosInternal.y);
  }

  @Override
  public String toString() {
    return "Marker [mapPos=" + mapPos + "]";
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public static class MarkerInternalState extends BillBoardInternalState {
    public MarkerInternalState(Point3D pos, double[] localFrameMatrix, float rotationDeg) {
      super(pos, localFrameMatrix, rotationDeg);
    }
    
    @Override
    public float getTextureWidth() {
      MarkerStyle style = (MarkerStyle) activeStyle;
      return style != null ? style.textureInfo.width : 0;
    }

    @Override
    public float getTextureHeight() {
      MarkerStyle style = (MarkerStyle) activeStyle;
      return style != null ? style.textureInfo.height : 0;
    }
  }
}
