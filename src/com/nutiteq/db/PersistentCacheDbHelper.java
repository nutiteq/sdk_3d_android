package com.nutiteq.db;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.nutiteq.log.Log;
import com.nutiteq.utils.LongMap;

/**
 * Helper for persistent tile cache.
 * @pad.exclude
 */
public class PersistentCacheDbHelper {
  private static final String CREATE_TABLE_SQL = "CREATE TABLE IF NOT EXISTS persistent_cache "
      + "(tileId INTEGER NOT NULL PRIMARY KEY, compressed BLOB, time INTEGER)";
  private static final String PRAGMA_PAGE_SIZE_SQL = "PRAGMA page_size=4096";
  private static final String LOAD_TILESET_SQL = "SELECT tileId, LENGTH(compressed) FROM persistent_cache ORDER BY time ASC";
  //private static final String ADD_TILE_SQL = "INSERT INTO persistent_cache VALUES (?, ?, ?)";
  //private static final String UPDATE_TILE_TIME_SQL = "UPDATE persistent_cache SET time = ? WHERE tileId = ?";
  private static final String GET_TILE_SQL = "SELECT compressed FROM persistent_cache WHERE tileId = ?";
  private static final String REMOVE_TILE_SQL = "DELETE FROM persistent_cache WHERE tileId = ?";
  private static final String REMOVE_TILE_RANGE_SQL = "DELETE FROM persistent_cache WHERE tileId >= ? AND tileId < ?";
  private static final String CLEAR_DB_SQL = "DELETE FROM persistent_cache";
  
  private final SQLiteDatabase db;

  public PersistentCacheDbHelper(String path) {
    File pathFile = new File(path);
    File parentFile = pathFile.getParentFile();
    if (parentFile != null) {
      parentFile.mkdirs();
    }
    boolean exists = pathFile.exists(); 
    db = SQLiteDatabase.openOrCreateDatabase(path, null);
    if (!exists) {
      try {
        SQLiteStatement statement = db.compileStatement(PRAGMA_PAGE_SIZE_SQL);
        statement.execute();
      } catch (RuntimeException e) {
        Log.debug("PersistentCacheDbHelper: could not set page size!");
      }
    }
    SQLiteStatement statement = db.compileStatement(CREATE_TABLE_SQL);
    statement.execute();
  }
  
  public List<LongMap.Entry<Integer>> loadDb() {
    List<LongMap.Entry<Integer>> tileMap = new ArrayList<LongMap.Entry<Integer>>();
    if (!db.isOpen()){
      return tileMap;
    }
    Cursor cursor = db.rawQuery(LOAD_TILESET_SQL, null);
    if (cursor == null) {
      return null;
    }
    while (cursor.moveToNext()) {
      long tileId = cursor.getLong(0);
      int size = cursor.getInt(1);
      tileMap.add(new LongMap.Entry<Integer>(tileId, size));
    }
    cursor.close();
    return tileMap;
  }
  
  public void closeDb() {
    if (db.isOpen()) {
      db.close();
    }
  }

  public void clear() {
    if (!db.isOpen()){
      return;
    }
    SQLiteStatement statement = db.compileStatement(CLEAR_DB_SQL);
    statement.execute();
  }
 
  public void add(long tileId, byte[] compressed) {
    if (!db.isOpen()){
      return;
    }
    /*
    SQLiteStatement statement = db.compileStatement(ADD_TILE_SQL);
    statement.bindLong(1, tileId);
    statement.bindBlob(2, compressed);
    statement.bindLong(3, System.currentTimeMillis());
    statement.executeInsert();
    */
    ContentValues values = new ContentValues();
    values.put("tileId", tileId);
    values.put("compressed", compressed);
    values.put("time", System.currentTimeMillis());
    db.insert("persistent_cache", null, values);
  }

  public byte[] get(long tileId) {
    if (!db.isOpen()){
      return null;
    }
    Cursor cursor = db.rawQuery(GET_TILE_SQL, new String[] { Long.toString(tileId) });
    if (cursor == null) {
      return null;
    }
    byte[] compressed = null;
    if (cursor.moveToNext()) {
      compressed = cursor.getBlob(0);
    }
    cursor.close();

    // Note: the update is deliberately uncommented - it makes queries roughly 10 times slower!
    /*
    SQLiteStatement statement = db.compileStatement(UPDATE_TILE_TIME_SQL);
    statement.bindLong(1, System.currentTimeMillis());
    statement.bindLong(2, tileId);
    statement.execute();
    */
    return compressed;
  }

  public void remove(long tileId) {
    if (!db.isOpen()){
      return;
    }
    SQLiteStatement statement = db.compileStatement(REMOVE_TILE_SQL);
    statement.bindLong(1, tileId);
    statement.execute();
  }
  
  public void removeRange(long firstTileId, long lastTileId) {
    if (!db.isOpen()){
      return;
    }
    SQLiteStatement statement = db.compileStatement(REMOVE_TILE_RANGE_SQL);
    statement.bindLong(1, firstTileId);
    statement.bindLong(2, lastTileId);
    statement.execute();
  }
  
}
