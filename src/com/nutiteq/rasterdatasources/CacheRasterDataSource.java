package com.nutiteq.rasterdatasources;

import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileBitmap;

/**
 * Data source for caching and fast retrieval of source tiles.
 */
public class CacheRasterDataSource extends AbstractRasterDataSource {
  
  /**
   * Cache store interface.
   * 
   * Actual storage details (file system, memory, database) are left to the implementation,
   * also caching policy decision (Least-Recently-Used, lower zoom prioritizing, etc) is left to the implementation.
   * 
   * Note: access to the store is serialized and does not have to synchronized.
   */
  public interface CacheStore {
    /**
     * Open cache store. Other operations become valid only after opening.
     */
    void open();
    
    /**
     * Close cache store. Other operations except open() are not valid after closing.
     */
    void close();

    /**
     * Get tile bitmap corresponding to given tile.
     * 
     * @param tile
     *          tile coordinates
     * @return tile bitmap or null if the tile does not exist in cache.
     */
    TileBitmap get(MapTile tile);

    /**
     * Clear cache, remove all tiles.
     */
    void clear();
    
    /**
     * Put new tile to the cache.
     * Note: this method may be called even when tile already exists in the cache, in that case existing tile should be replaced.
     * 
     * @param tile
     *          tile coordinates
     * @param tileBitmap
     *          tile bitmap to store
     */
    void put(MapTile tile, TileBitmap tileBitmap);
    
    /**
     * Remove tile from the cache.
     * Note: this method may be called when tile does not exist in the cache, it should not throw exception in this case.
     * 
     * @param tile
     *          tile coordinates
     */
    void remove(MapTile tile);
  }
  
  /**
   * Data source change listener.
   */
  private class DataSourceChangeListener implements OnChangeListener {
    @Override
    public void onTilesChanged() {
      synchronized (CacheRasterDataSource.this) {
        if (cacheStoreOpen) {
          cacheStore.clear();
        }
      }
      notifyTilesChanged();
    }
  }

  private final RasterDataSource dataSource;
  private DataSourceChangeListener dataSourceListener = null;
  private final CacheStore cacheStore;
  private volatile boolean cacheStoreOpen = false;

  /**
   * Default constructor.
   * 
   * @param dataSource
   *          original data source to be cached
   * @param cacheStore
   *          cache store implementation for the cache. Note: the store must be in 'closed' state.
   */
  public CacheRasterDataSource(RasterDataSource dataSource, CacheStore cacheStore) {
    super(dataSource.getProjection(), dataSource.getMinZoom(), dataSource.getMaxZoom());
    this.dataSource = dataSource;
    this.cacheStore = cacheStore;
  }
  
  /**
   * Open cache storage.
   * Note: if this is not done, cache will not return any data.
   */
  public synchronized void open() {
    if (cacheStoreOpen) {
      throw new IllegalStateException("Caches already initialized");
    }
    cacheStore.open();
    cacheStoreOpen = true;
    dataSourceListener = new DataSourceChangeListener();
    dataSource.addOnChangeListener(dataSourceListener);
  }
  
  /**
   * Close cache storage.
   * Note: after cache is closed, original data source is no longer queried.
   */
  public synchronized void close() {
    if (!cacheStoreOpen) {
      return;
    }
    dataSource.removeOnChangeListener(dataSourceListener);
    dataSourceListener = null;
    cacheStoreOpen = false;
    cacheStore.close();
  }
  
  @Override
  public TileBitmap loadTile(MapTile tile) {
    synchronized (this) {
      if (!cacheStoreOpen) {
        return null;
      }
      TileBitmap tileBitmap = cacheStore.get(tile);
      if (tileBitmap != null) {
        return tileBitmap;
      }
    }

    TileBitmap tileBitmap = dataSource.loadTile(tile);
    if (tileBitmap == null) {
      return null;
    }

    synchronized (this) {
      if (cacheStoreOpen) {
        cacheStore.put(tile, tileBitmap);
      }
    }

    return tileBitmap;
  }
}
