package com.nutiteq.rasterdatasources;

import java.util.LinkedList;
import java.util.List;

import com.nutiteq.projections.Projection;

/**
 * Abstract base class for raster datasources.
 * It is recommended to use this as a base class for all raster datasources.
 * 
 * This class provides default implementation for listener registration and other common datasource methods.
 * Subclasses only need to define their loadTile method.
 */
public abstract class AbstractRasterDataSource implements RasterDataSource {
  private final List<OnChangeListener> onChangeListeners = new LinkedList<OnChangeListener>();
  
  protected final Projection projection;
  protected final int minZoom;
  protected final int maxZoom;

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the data source.
   * @param minZoom
   *          minimum zoom level supported by this data source
   * @param maxZoom
   *          maximum zoom level supported by this data source
   */
  protected AbstractRasterDataSource(Projection projection, int minZoom, int maxZoom) {
    this.projection = projection;
    this.minZoom = minZoom;
    this.maxZoom = maxZoom;
  }

  @Override
  public Projection getProjection() {
    return projection;
  }
  
  @Override
  public int getMinZoom() {
    return minZoom;
  }
  
  @Override
  public int getMaxZoom() {
    return maxZoom;
  }
  
  @Override
  public void addOnChangeListener(OnChangeListener listener) {
    synchronized (onChangeListeners) {
      onChangeListeners.add(listener);
    }
  }

  @Override
  public void removeOnChangeListener(OnChangeListener listener) {
    synchronized (onChangeListeners) {
      onChangeListeners.remove(listener);
    }
  }

  /**
   * Notify listeners that tiles have changed.
   * This method will remove tiles from caches and forces tiles to be reloaded from original datasource.
   */
  protected void notifyTilesChanged() {
    synchronized (onChangeListeners) {
      for (OnChangeListener listener : onChangeListeners) {
        listener.onTilesChanged();
      }
    }
  }
}
