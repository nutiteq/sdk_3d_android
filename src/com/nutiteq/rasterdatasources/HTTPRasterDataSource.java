package com.nutiteq.rasterdatasources;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Random;

import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileBitmap;
import com.nutiteq.log.Log;
import com.nutiteq.projections.Projection;
import com.nutiteq.utils.Utils;

/**
 * Base class for online tile data sources that use HTTP connection for transport.
 * This class provides methods for dealing for HTTP headers, timeouts.
 * 
 * The class uses template scheme for tile URL generation. Various tags are replaced within the template based on actual tile being loaded.
 * 
 * For example: http://tile.openstreetmap.org/{zoom}/{x}/{y}.png
 * If the tile being loaded uses zoom level 2 and x, y coordinates of 1,3 respectively,
 * then this class load tile from following URL: http://tile.openstreetmap.org/2/1/3.png
 * 
 * The following tags are supported: s, zoom, x, y, xflipped, yflipped, quadkey
 */
public class HTTPRasterDataSource extends StreamRasterDataSource {
  private static final int DEFAULT_CONNECTION_TIMEOUT = 5 * 1000;
  private static final int DEFAULT_READ_TIMEOUT = 5 * 1000;

  private static final String[] SERVERS = new String[] { "a", "b", "c", "d" };
  private static final Random SERVER_RANDOM = new Random();
  
  protected final String urlTemplate;
  protected int connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;
  protected int readTimeout = DEFAULT_READ_TIMEOUT;
  protected Map<String, String> httpHeaders = null;

  /**
   * Default constructor.
   * 
   * @param projection
   *          the desired projection
   * @param minZoom
   *          minimum zoom level supported by this data source
   * @param maxZoom
   *          maximum zoom level supported by this data source
   * @param urlTemplate
   *          URL template containing tags (for example, "http://openstreetmap.org/{zoom}/{x}/{y}.png")
   */
  public HTTPRasterDataSource(Projection projection, int minZoom, int maxZoom, String urlTemplate) {
    super(projection, minZoom, maxZoom);
    this.urlTemplate = urlTemplate;
  }
  
  /**
   * Add HTTP headers. Useful for referer, basic-auth etc.
   * 
   * @param httpHeaders
   *          map of additional HTTP headers to use when performing REST queries. 
   */
  public void setHttpHeaders(Map<String, String> httpHeaders) {
    this.httpHeaders = httpHeaders;
  }
  
  /**
   * Set connection timeout.
   * 
   * @param timeout
   *          timeout in milliseconds
   */
  public void setConnectionTimeout(int timeout) {
    this.connectionTimeout = timeout;
  }

  /**
   * Set read timeout.
   * 
   * @param timeout
   *          timeout in milliseconds
   */
  public void setReadTimeout(int timeout) {
    this.readTimeout = timeout;
  }

  /**
   * Method for building URL for the tile.
   * URL will be generated according to specified template.
   * This method can be customized by subclasses.
   * 
   * @param tile
   *          tile to generate the URL for.
   * @return URL for the tile.
   */
  protected String buildTileURL(MapTile tile) {
    Map<String, String> tags = buildTileTagMap(tile);
    tags.put("s", SERVERS[SERVER_RANDOM.nextInt(SERVERS.length)]);
    return Utils.replaceTags(urlTemplate, tags);
  }
  
  @Override
  public TileBitmap loadTile(MapTile tile) {
    String url = buildTileURL(tile);

    Log.info(getClass().getName() + ": loading tile " + url);

    HttpURLConnection urlConnection = null;
    try {
      urlConnection = (HttpURLConnection) (new URL(url)).openConnection();
      urlConnection.setConnectTimeout(connectionTimeout);
      urlConnection.setReadTimeout(readTimeout);
      
      if (httpHeaders != null){
        for (Map.Entry<String, String> entry : httpHeaders.entrySet()) {
          urlConnection.addRequestProperty(entry.getKey(), entry.getValue());  
        }
      }
      
      BufferedInputStream inputStream = new BufferedInputStream(urlConnection.getInputStream(), BUFFER_SIZE);
      return readTileBitmap(inputStream);
    } catch (IOException e) {
      Log.error(getClass().getName() + ": failed to load tile. " + e.getMessage());
    } finally {
      if (urlConnection != null) {
        urlConnection.disconnect();
      }
    }
    return null;
  }

}
