package com.nutiteq.rasterdatasources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileBitmap;
import com.nutiteq.log.Log;
import com.nutiteq.projections.Projection;

/**
 * An abstract base class for stream-based raster data sources. Provides useful utility methods for subclasses.
 */
public abstract class StreamRasterDataSource extends AbstractRasterDataSource {
  protected static final int BUFFER_SIZE = 4096;

  /**
   * Default constructor.
   * 
   * @param projection
   *          the desired projection
   * @param minZoom
   *          minimum zoom level supported by this data source
   * @param maxZoom
   *          maximum zoom level supported by this data source
   */
  protected StreamRasterDataSource(Projection projection, int minZoom, int maxZoom) {
    super(projection, minZoom, maxZoom);
  }
  
  /**
   * Create a tag map for given tile. The tag map can be used to construct actual path or URL for the tile.
   * Supports following tags: {zoom}, {x}, {y}, {xflipped}, {yflipped}
   * 
   * @param tile
   *          tile to use for tag map generation
   * @return tag map
   */
  protected Map<String, String> buildTileTagMap(MapTile tile) {
    Map<String, String> tags = new HashMap<String, String>();

    tags.put("zoom", Integer.toString(tile.zoom));
    tags.put("x", Integer.toString(tile.x));
    tags.put("y", Integer.toString(tile.y));
    tags.put("xflipped", Integer.toString((1 << tile.zoom) - 1 - tile.x));
    tags.put("yflipped", Integer.toString((1 << tile.zoom) - 1 - tile.y));

    StringBuffer sb = new StringBuffer();
    for (int i = tile.zoom - 1; i >= 0; i--) {
      sb.append(((tile.y >> i) & 1) * 2 + ((tile.x >> i) & 1));
    }
    tags.put("quadkey", sb.toString());
    return tags;
  }

  /**
   * Read compressed tile bitmap from a given stream.
   * 
   * @param inputStream
   *          stream containing the compressed bitmap data.
   * @return tile bitmap
   */
  protected TileBitmap readTileBitmap(InputStream inputStream) {
    byte[] buffer = new byte[BUFFER_SIZE];
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    try {
      int bytesRead;
      while ((bytesRead = inputStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, bytesRead);
      }
      outputStream.flush();
      return new TileBitmap(outputStream.toByteArray());
    } catch (IOException e) {
      Log.error(getClass().getName() + ": failed to load tile. " + e.getMessage());
    } finally {
      try {
        outputStream.close();
        if (inputStream != null) {
          inputStream.close();
        }
      } catch (IOException e) {
        Log.error(getClass().getName() + ": failed to close the stream. " + e.getMessage());
      }
    }
    return null;
  }

  /**
   * Reload tiles. This method informs listeners that tiles have changed.
   */
  public void reloadTiles() {
    notifyTilesChanged();
  }

}
