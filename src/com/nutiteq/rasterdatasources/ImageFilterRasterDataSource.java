package com.nutiteq.rasterdatasources;

import android.graphics.Bitmap;

import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileBitmap;
import com.nutiteq.projections.Projection;

/**
 * Data source for processing (filtering) source tile images.
 * This can be used to perform simple image manipulations (contrast enhancement, for example) on source tiles.
 */
public class ImageFilterRasterDataSource extends AbstractRasterDataSource {
  
  /**
   * Interface for bitmap filtering
   */
  public interface ImageFilter {
    /**
     * Method for filtering the image. This must be overridden in subclasses.
     * 
     * @param source
     *          source bitmap to filter. This bitmap should not be changed.
     * @return filtered bitmap. Its dimensions should be unchanged from the original image.
     */
    Bitmap filter(Bitmap source);
  }
  
  private final RasterDataSource dataSource;
  private ImageFilter imageFilter;
  
  /**
   * Default constructor. In this case image filter has to be connected at later stage.
   * 
   * @param dataSource
   *          data source to filter
   */
  public ImageFilterRasterDataSource(RasterDataSource dataSource) {
    super(dataSource.getProjection(), dataSource.getMinZoom(), dataSource.getMaxZoom());
    this.dataSource = dataSource;
    this.imageFilter = null;
  }
  
  /**
   * Constructor with explicit image filter.
   * 
   * @param dataSource
   *          data source to filter
   * @param imageFilter
   *          the image filter. Can be null if no processing is required. 
   */
  public ImageFilterRasterDataSource(RasterDataSource dataSource, ImageFilter imageFilter) {
    super(dataSource.getProjection(), dataSource.getMinZoom(), dataSource.getMaxZoom());
    this.dataSource = dataSource;
    this.imageFilter = imageFilter;
  }
  
  /**
   * Get the current image filter.
   * 
   * @return current image filter.
   */
  public ImageFilter getImageFilter() {
    return imageFilter;
  }
  
  /**
   * Set the current image filter.
   * 
   * @param imageFilter
   *          the new image filter. Can be null if no processing is required.
   */
  public void setImageFilter(ImageFilter imageFilter) {
    if (imageFilter != this.imageFilter) {
      this.imageFilter = imageFilter;
      notifyTilesChanged();
    }
  }

  @Override
  public Projection getProjection() {
    return dataSource.getProjection();
  }

  @Override
  public TileBitmap loadTile(MapTile tile) {
    TileBitmap tileBitmap = dataSource.loadTile(tile);
    if (tileBitmap == null) {
      return null;
    }
    if (imageFilter == null) {
      return tileBitmap;
    }
    return new TileBitmap(imageFilter.filter(tileBitmap.getBitmap()));
  }

  @Override
  public void addOnChangeListener(OnChangeListener listener) {
    // We will register listener directly and also register at original data source level: direct registration is required as we allow updating the filter
    super.addOnChangeListener(listener);
    dataSource.addOnChangeListener(listener);
  }

  @Override
  public void removeOnChangeListener(OnChangeListener listener) {
    dataSource.removeOnChangeListener(listener);
    super.removeOnChangeListener(listener);
  }
}
