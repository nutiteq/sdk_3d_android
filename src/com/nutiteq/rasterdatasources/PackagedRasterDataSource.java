package com.nutiteq.rasterdatasources;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Map;

import android.content.Context;
import android.content.res.Resources;

import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileBitmap;
import com.nutiteq.log.Log;
import com.nutiteq.projections.Projection;
import com.nutiteq.utils.Utils;

/**
 * A raster data source that uses the map bundled in the /res/raw/ folder as a source.
 * The request are generated based on resourceTemplate that may contain various tags.
 * 
 * The following tags are supported: zoom, x, y, xflipped, yflipped, quadkey
 *
 * For example if, if resourceTemplate = "t{zoom}_{x}_{y}", then the result (for root tile) will be "/res/raw/t0_0_0.png".
 * File extension of resource files is removed by Android SDK, do not add this to template. 
 */
public class PackagedRasterDataSource extends StreamRasterDataSource {
  protected final Resources resources;
  protected final String resourcePrefix;
  protected final String resourceTemplate;
  
  /**
   * Default constructor.
   * 
   * @param projection
   *          the desired projection
   * @param minZoom
   *          minimum zoom level supported by this data source
   * @param maxZoom
   *          maximum zoom level supported by this data source
   * @param resourceTemplate
   *          Resource template containing tags (for example, "t{zoom}_{x}_{y}.png")
   * @param context
   *          the activity or application context
   */
  public PackagedRasterDataSource(Projection projection, int minZoom, int maxZoom, String resourceTemplate, Context context) {
    super(projection, minZoom, maxZoom);
    this.resources = context.getResources();
    this.resourcePrefix = context.getPackageName() + ":raw/";
    this.resourceTemplate = resourceTemplate;
  }
  
  /**
   * Method for building path for the tile.
   * Path will be generated according to specified template.
   * This method can be customized by subclasses.
   * 
   * @param tile
   *          tile to generate the path for.
   * @return path for the tile.
   */
  protected String buildTilePath(MapTile tile) {
    Map<String, String> tags = buildTileTagMap(tile);
    return resourcePrefix + Utils.replaceTags(resourceTemplate, tags);
  }

  @Override
  public TileBitmap loadTile(MapTile tile) {
    String path = buildTilePath(tile);

    Log.info(getClass().getName() + ": loading tile " + path);

    int resourceId = resources.getIdentifier(path, null, null);
    if (resourceId == 0) {
      Log.error(getClass().getName() + ": resource not found: " + path);
      return null;
    }
    
    InputStream inputStream = new DataInputStream(resources.openRawResource(resourceId));
    return readTileBitmap(inputStream);
  }
}
