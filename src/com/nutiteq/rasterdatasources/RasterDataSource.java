package com.nutiteq.rasterdatasources;

import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileBitmap;
import com.nutiteq.projections.Projection;

/**
 * Interface for all raster tile data sources.
 */
public interface RasterDataSource {
  
  /**
   * Interface for monitoring data source change events.
   */
  public interface OnChangeListener {
    /**
     * Called when tile set has changed and needs to be updated.
     */
    void onTilesChanged();
  }

  /**
   * Get projection for this data source.
   * 
   * @return projection
   */
  Projection getProjection();
  
  /**
   * Get minimum zoom level supported by this data source.
   * 
   * @return minimum zoom level supported (inclusive)
   */
  int getMinZoom();
  
  /**
   * Get maximum zoom level supported by this data source.
   * 
   * @return maximum zoom level supported (inclusive)
   */
  int getMaxZoom();
  
  /**
   * Load the specified raster tile.
   * 
   * @param tile
   *          tile to load.
   * @return raster tile bitmap. If tile is not available, null may be returned.
   */
  TileBitmap loadTile(MapTile tile);
  
  /**
   * Register listener for data source change events.
   * 
   * @param listener
   *          listener for change events.
   */
  void addOnChangeListener(OnChangeListener listener);

  /**
   * Unregister listener for data source change events.
   * 
   * @param listener
   *          previously added listener.
   */
  void removeOnChangeListener(OnChangeListener listener);
}
