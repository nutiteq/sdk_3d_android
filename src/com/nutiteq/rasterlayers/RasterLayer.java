package com.nutiteq.rasterlayers;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.nutiteq.cache.CompressedMemoryCache;
import com.nutiteq.cache.TextureMemoryCache;
import com.nutiteq.components.Color;
import com.nutiteq.components.Components;
import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileBitmap;
import com.nutiteq.layers.TileLayer;
import com.nutiteq.log.Log;
import com.nutiteq.projections.Projection;
import com.nutiteq.rasterdatasources.RasterDataSource;
import com.nutiteq.renderers.components.MapTileDrawData;
import com.nutiteq.renderers.components.MapTileProxy;
import com.nutiteq.renderers.components.MapTileQuadTreeNode;
import com.nutiteq.tasks.FetchTileTask;
import com.nutiteq.utils.Const;

/**
 * Raster layer base class. Should be used together with corresponding data source.
 *
 * Contains methods for controlling caching policy and some visual attributes.
 */
public class RasterLayer extends TileLayer {
  private static final int CACHE_FETCH_TASK_PRIORITY = Integer.MAX_VALUE; // Priority for cache fetch tasks

  /**
   * Task for fetching tiles from persistent cache.
   */
  private class CacheFetchTileTask extends FetchTileTask {
    public CacheFetchTileTask(MapTile tile, Components components) {
      super(tile, components, tileIdOffset);
    }
    
    @Override
    public void run() {
      super.run();
      byte[] compressed = components.persistentCache.get(rasterTileId);
      if (compressed != null) {
        finished(compressed);
        cleanUp();
      } else {
        components.retrievingTiles.remove(rasterTileId);
        if (getComponents() != null) {
          fetchTile(tile);
        }
      }
    }
  }

  /**
   * Task for fetching tiles from data source.
   */
  private class DataSourceFetchTask extends FetchTileTask {
    public DataSourceFetchTask(MapTile tile, Components components) {
      super(tile, components, tileIdOffset);
    }

    @Override
    public void run() {
      super.run();
      try {
        TileBitmap tileBitmap = dataSource.loadTile(tile);
        if (tileBitmap == null) {
          Log.error("DataSourceFetchTask: no tile data");
        } else {
          byte[] data = tileBitmap.getCompressed();
          if (persistentCaching) {
            components.persistentCache.add(rasterTileId, data);
          }
          if (memoryCaching) {
            components.compressedMemoryCache.add(rasterTileId, data);
          }
          components.textureMemoryCache.add(rasterTileId, tileBitmap.getBitmap());
        }
      } catch (Exception e) {
        Log.error("DataSourceFetchTask: failed to fetch tile: " + e.getMessage());
      }
      cleanUp();
    }
  }
  
  /**
   * Listener for data source 'on change' events.
   */
  private class DataSourceChangeListener implements RasterDataSource.OnChangeListener {
    @Override
    public void onTilesChanged() {
      long firstTileId = tileIdOffset;
      long lastTileId = tileIdOffset + Const.TILE_ID_OFFSET;
      Components components = getComponents();
      if (components != null) {
        // Stop all pending tasks
        while (true) {
          Thread.yield();
          
          // Check that there are no pending tasks: we have to wait until last one has finished
          synchronized (components.rasterTaskPool) {
            components.rasterTaskPool.cancelAll();
            if (components.rasterTaskPool.getActiveWorkerCount() > 0) {
              continue;
            }
        
            // Remove tile ranges from caches and recalculate visible tiles
            if (persistentCaching) {
              components.persistentCache.removeRange(firstTileId, lastTileId);
            }
            if (memoryCaching) {
              components.compressedMemoryCache.removeRange(firstTileId, lastTileId);
            }
            components.textureMemoryCache.removeRange(firstTileId, lastTileId);
          }
          updateVisibleTiles();
          break;
        }
      }
    }
  }

  protected final RasterDataSource dataSource;
  protected DataSourceChangeListener dataSourceListener = null;
  @Deprecated
  protected final String location; // TODO: remove in final 2.4

  protected Color tileColor = new Color(Color.WHITE); 
  protected boolean tileFading = true;

  private volatile List<MapTileDrawData> visibleTiles;
  private volatile AtomicLong visibleTilesTimeStamp = new AtomicLong();

  /**
   * Default constructor.
   * 
   * @param dataSource
   *          tile data source for the layer.
   * @param id
   *          unique layer id. Id for the layer must be depend ONLY on the layer source, otherwise tile caching will not work properly. 
   */
  public RasterLayer(RasterDataSource dataSource, int id) {
    super(dataSource.getProjection(), dataSource.getMinZoom(), dataSource.getMaxZoom(), id);
    this.dataSource = dataSource;
    this.location = null;
  }

  /**
   * Default constructor for subclasses.
   * 
   * @param projection
   *          projection for the layer.
   * @param minZoom
   *          minimum zoom level supported by the layer.
   * @param maxZoom
   *          maximum zoom level supported by the layer.
   * @param id
   *          unique layer id. Id for the layer must be depend ONLY on the layer source, otherwise tile caching will not work properly. 
   */
  protected RasterLayer(Projection projection, int minZoom, int maxZoom, int id) {
    super(projection, minZoom, maxZoom, id);
    this.dataSource = null;
    this.location = null;
  }

  /**
   * Default constructor for subclasses. Note: this version is deprecated
   * 
   * @param projection
   *          projection for the layer.
   * @param minZoom
   *          minimum zoom level supported by the layer.
   * @param maxZoom
   *          maximum zoom level supported by the layer.
   * @param id
   *          unique layer id. Id for the layer must be depend ONLY on the layer source, otherwise tile caching will not work properly.
   * @param location
   *          custom location info (DEPRECATED)
   */
  @Deprecated
  protected RasterLayer(Projection projection, int minZoom, int maxZoom, int id, String location) {
    super(projection, minZoom, maxZoom, id);
    this.dataSource = null;
    this.location = location;
  }

  /**
   * Get the data source attached to the layer.
   * 
   * @return layer data source.
   */
  public RasterDataSource getDataSource() {
    return dataSource;
  }
  
  /**
   * Get active tile color. Default is white.
   * 
   * @return current tile color.
   */
  public Color getTileColor() {
    return tileColor;
  }
  
  /**
   * Set tile color. Tile texture will be modulated by this color.
   * This method allows making layer partially transparent, by setting alpha channel appropriately. In this is case it is strongly
   * advisable to turn tile fading off, otherwise visible artifacts may occur.
   * 
   * @param color
   *          the new tile color.
   */
  public void setTileColor(Color color) {
    this.tileColor = color;

    Components components = getComponents();
    if (components != null) {
      components.mapRenderers.getMapRenderer().requestRenderView();
    }
  }

  /**
   * Get tile fading state for this layer. By default
   * fading is enabled for all layers.
   * 
   * @return fading flag value
   */
  public boolean isTileFading() {
    return tileFading;
  }
  
  /**
   * Enable or disable tile fading for this layer. By default
   * fading is enabled for all layers.
   * 
   * @param
   *      tileFading fading flag value
   */
  public void setTileFading(boolean tileFading) {
    this.tileFading = tileFading;
  }
  
  @Override
  public boolean getTilePreloading() {
    Components components = getComponents();
    if (components == null) {
      return false;
    }

    return components.options.isPreloading();
  }
  
  @Override
  public long getVisibleTilesTimeStamp() {
    return visibleTilesTimeStamp.get();
  }

  @Override
  public List<MapTileDrawData> getVisibleTiles() {
    return visibleTiles;
  }

  @Override
  public void setVisibleTiles(List<MapTileDrawData> visibleTiles) {
    this.visibleTiles = visibleTiles;
    this.visibleTilesTimeStamp.incrementAndGet();

    Components components = getComponents();
    if (components != null) {
      components.mapRenderers.getMapRenderer().requestRenderView();
    }
  }

  @Override
  public boolean resolveTile(MapTileQuadTreeNode tile, List<MapTileProxy> proxies) {
    Components components = getComponents();
    if (components == null) {
      return false;
    }
    
    TextureMemoryCache textureMemoryCache = components.textureMemoryCache;
    CompressedMemoryCache compressedMemoryCache = components.compressedMemoryCache;

    // Check the texture memory cache
    long fullTileId = tileIdOffset + tile.id;
    if (textureMemoryCache.get(fullTileId) != 0) {
      proxies.add(new MapTileProxy(fullTileId, tile));
      return true;
    }

    // Find a substitute texture from the parent, only if zoom is below layer max
    MapTileProxy proxy = findParentProxy(textureMemoryCache, tile);
    if (proxy != null) {
      proxies.add(proxy);
    } else {
      // Find a substitute texture from the children
      if (!findChildProxies(textureMemoryCache, tile, proxies)) {
        proxies.add(new MapTileProxy(fullTileId, tile));
      }
    }

    // Check the compressed memory cache
    byte[] compressed = null;
    if (isMemoryCaching()) {
      compressed = compressedMemoryCache.get(fullTileId);
    }
    if (compressed == null) {
      // Check if the texture is already being loaded from cache/downloaded
      if (!components.retrievingTiles.contains(fullTileId)) {
        // If persistent caching is not enabled for the layer or tile is not in the cache, do tile fetch directly.
        if (isPersistentCaching() && components.persistentCache.contains(fullTileId)) {
          components.rasterTaskPool.execute(new CacheFetchTileTask(tile, components), CACHE_FETCH_TASK_PRIORITY);
        } else {
          fetchTile(tile);
        }
      }
    } else {
      // Create a BitmapFactory to specify the options and decode the
      // compressed data to bitmap
      BitmapFactory.Options opts = new BitmapFactory.Options();
      opts.inScaled = false;
      Bitmap bitmap = BitmapFactory.decodeByteArray(compressed, 0, compressed.length, opts);
      if (bitmap == null) {
        // If the compressed image is corrupt, delete it
        Log.error("RasterCullTask: Failed to decode a compressed image.");
        compressedMemoryCache.remove(fullTileId);
      } else {
        // If not corrupt, add to the textureMemoryCache
        textureMemoryCache.add(fullTileId, bitmap);
      }
    }
    return true;
  }

  private MapTileProxy findParentProxy(TextureMemoryCache textureMemoryCache, MapTileQuadTreeNode tile) {
    // Move towards root starting from immediate parent. Try to find parent that exists in texture cache
    float tileX = 0, tileY = 0;
    float tileSize = 1;
    MapTileQuadTreeNode proxyTile = tile;
    while (proxyTile.parent != null) {
      tileX = (tileX + MapTileQuadTreeNode.getChildX(0, proxyTile.nodeType)) * 0.5f;
      tileY = (tileY + MapTileQuadTreeNode.getChildY(0, proxyTile.nodeType)) * 0.5f;
      tileSize = tileSize * 0.5f;
      proxyTile = proxyTile.parent;

      long fullProxyTileId = tileIdOffset + proxyTile.id;
      int texture = textureMemoryCache.getWithoutMod(fullProxyTileId);
      if (texture != 0) {
        return new MapTileProxy(fullProxyTileId, tile, tileX, tileY, tileSize);
      }
    }
    return null;
  }

  private boolean findChildProxies(TextureMemoryCache textureMemoryCache, MapTileQuadTreeNode tile, List<MapTileProxy> proxies) {
    // Add all children that have textures to the draw list.
    // Note: this actually breaks the zoom calculation logic and can cause cracks, though once correct texture is loaded, it will disappear
    int count = proxies.size();
    long fullTileId = tileIdOffset + tile.topLeft.id;
    if (textureMemoryCache.getWithoutMod(fullTileId) != 0) {
      proxies.add(new MapTileProxy(fullTileId, tile.topLeft));
    }

    fullTileId = tileIdOffset + tile.topRight.id;
    if (textureMemoryCache.getWithoutMod(fullTileId) != 0) {
      proxies.add(new MapTileProxy(fullTileId, tile.topRight));
    }

    fullTileId = tileIdOffset + tile.bottomRight.id;
    if (textureMemoryCache.getWithoutMod(fullTileId) != 0) {
      proxies.add(new MapTileProxy(fullTileId, tile.bottomRight));
    }

    fullTileId = tileIdOffset + tile.bottomLeft.id;
    if (textureMemoryCache.getWithoutMod(fullTileId) != 0) {
      proxies.add(new MapTileProxy(fullTileId, tile.bottomLeft));
    }
    return proxies.size() > count;
  }
  
  /**
   * Start appropriate task for fetching the requested tile. 
   * This gets called when a visible tile can't be found in any of the caches and thus must be reloaded from the source.
   * Inheriting classes can either start a new task immediately or wait for the flush command to start all at once.
   * NB! You need to start the task in your implementation, it is not done automatically. If you have online tiles then 
   * usually you calculate your URL (urlString) and the following line is the last one in your implementation:
   *   components.rasterTaskPool.execute(new NetFetchTileTask(tile, components, tileIdOffset, urlString));
   * 
   * @param tile
   *          the requested tile
   */
  public void fetchTile(MapTile tile) {
    if (dataSource == null) {
      throw new IllegalArgumentException("RasterLayer: data source not set!");
    }
    int tileZoom = tile.zoom;
    if (tileZoom < minZoom || tileZoom > maxZoom) {
      return;
    }
    Components components = getComponents();
    if (components == null) {
      return;
    }
    executeFetchTask(new DataSourceFetchTask(tile, components));
  }

  /**
   * Execute fetch task asynchronously. Fetch tasks are queued and executed according to priorities.
   * 
   * @param runnable
   *          fetch task to execute.
   */
  protected void executeFetchTask(Runnable runnable) {
    Components components = getComponents();
    if (components != null) {
      components.rasterTaskPool.execute(runnable, fetchPriority);
    }
  }
  
  /**
   * Not part of public API.
   * 
   * @pad.exclude
   */
  protected void registerDataSourceListener() {
    dataSourceListener = new DataSourceChangeListener();
    dataSource.addOnChangeListener(dataSourceListener);
  }

  /**
   * Not part of public API.
   * 
   * @pad.exclude
   */
  protected void unregisterDataSourceListener() {
    dataSource.removeOnChangeListener(dataSourceListener);
    dataSourceListener = null;
  }

  @Override
  public synchronized void setComponents(Components components) {
    super.setComponents(components);
    if (dataSource != null) {
      if (components != null && dataSourceListener == null) {
        registerDataSourceListener();
      } else if (components == null && dataSourceListener != null) {
        unregisterDataSourceListener();
      }
    }
  }
}
