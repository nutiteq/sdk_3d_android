package com.nutiteq.layers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import com.nutiteq.components.Components;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.projections.EPSG3857;
import com.nutiteq.projections.Projection;
import com.nutiteq.rasterlayers.RasterLayer;
import com.nutiteq.vectorlayers.VectorLayer;

/**
 * Container for all raster and vector layers of the map view.
 * In general layer rendering order corresponds to the layer ordering, with the exception 
 * of Z-ordered layers. NMLModelLayer and Polygon3DLayer are always Z-ordered, MarkerLayer and 
 * TextLayer can be configured to be either non Z-ordered or Z-ordered. GeometryLayer is not Z-ordered.
 */
public class Layers {
  /**
   * Default base projection.
   */
  public static final Projection DEFAULT_BASE_PROJECTION = new EPSG3857();

  private final ReentrantLock readLock = new ReentrantLock();
  
  private Layer baseLayer;
  private final List<Layer> layers = new LinkedList<Layer>();
  
  private volatile List<Layer> allLayers;
  private volatile List<TileLayer> tileLayers;
  private volatile List<RasterLayer> rasterLayers;
  private volatile List<VectorLayer<VectorElement>> vectorLayers;

  private final Components components;

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public Layers(Components components) {
    this.components = components;
  }
  
  /**
   * Gets the list of layers. Note: do not try to add layers directly to the list that is returned - it is unmodifiable.
   * This list does not contain base layer.
   * 
   * @return list of all layers.
   */
  public List<Layer> getLayers() {
    return java.util.Collections.unmodifiableList(layers);
  }

  /**
   * Sets the list of layers. Base layer will be left unchanged.
   * 
   * @param layers
   *          the new list of layers.
   */
  public void setLayers(List<Layer> layers) {
    for (Layer layer : layers) {
      layer.setComponents(components);
    }
    
    List<Layer> oldLayers;
    readLock.lock();
    try {
      oldLayers = new ArrayList<Layer>(this.layers);
      this.layers.clear();
      this.layers.addAll(layers);
      layersChanged();
    }
    finally {
      readLock.unlock();
    }

    for (Layer oldLayer : oldLayers) {
      if (baseLayer != oldLayer && !layers.contains(oldLayer)) { 
        oldLayer.setComponents(null);
      }
    }
    
    for (Layer layer : layers) {
      components.mapRenderers.getMapRenderer().layerChanged(layer);
    }
  }

  /**
   * Add a new layer to the layer stack. Layers get drawn in the order they were added in, with the exception of
   * Z ordered layers.
   * 
   * @param layer
   *          the layer to be added
   */
  public void addLayer(Layer layer) {
    layer.setComponents(components);
    
    readLock.lock();
    try {
      layers.add(layer);
      layersChanged();
    }
    finally {
      readLock.unlock();
    }

    components.mapRenderers.getMapRenderer().layerChanged(layer);
  }

  /**
   * Removes the specified layer from the layer stack. This should generally not be necessary, usually setting the
   * layer's visibility to false using Layer.setVisibility should suffice.
   * 
   * @param layer
   *          the layer to be removed
   */
  public void removeLayer(Layer layer) {
    readLock.lock();
    try {
      layers.remove(layer);
      layersChanged();
    }
    finally {
      readLock.unlock();
    }

    if (baseLayer != layer && !layers.contains(layer)) {
      layer.setComponents(null);
    }

    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Gets the base layer.
   * 
   * @return base layer.
   */
  public Layer getBaseLayer() {
    return baseLayer;
  }

  /**
   * Sets the base layer. The base layer is the first layer that gets drawn, with the exceptions of Z ordered layers.
   * Base layer coordinate system is also used for MapListener and MapView coordinates.
   * 
   * @param layer
   *          the new base layer
   */
  public void setBaseLayer(Layer layer) {
    if (layer != null) {
      layer.setComponents(components);
    }
    
    Layer oldBaseLayer;
    readLock.lock();
    try {
      oldBaseLayer = baseLayer;
      baseLayer = layer;
      layersChanged();
    }
    finally {
      readLock.unlock();
    }
    
    if (oldBaseLayer != null) {
      if (baseLayer != oldBaseLayer && !layers.contains(oldBaseLayer)) {
        oldBaseLayer.setComponents(null);
      }
    }

    components.mapRenderers.getMapRenderer().layerChanged(layer);
    int renderProj = components.options.getRenderProjection(); // renderprojection is potentially derived from baseprojection, so reset renderprojection
    components.mapRenderers.getMapRenderer().setRenderSurface(components.mapRenderers.createRenderSurface(renderProj));
  }
  
  private void layersChanged() {
    allLayers = null;
    tileLayers = null;
    rasterLayers = null;
    vectorLayers = null;
  }
  
  @SuppressWarnings("unchecked")
  private void rebuildLayers() {
    List<Layer> allLayers = new ArrayList<Layer>(layers.size() + 1);
    List<TileLayer> tileLayers = new ArrayList<TileLayer>(layers.size() + 1);
    List<RasterLayer> rasterLayers = new ArrayList<RasterLayer>(layers.size() + 1);
    List<VectorLayer<VectorElement>> vectorLayers = new ArrayList<VectorLayer<VectorElement>>(layers.size() + 1);
    
    if (baseLayer != null) {
      allLayers.add(baseLayer);
      if (baseLayer instanceof TileLayer) {
        tileLayers.add((TileLayer) baseLayer);
      }
      if (baseLayer instanceof RasterLayer) {
        rasterLayers.add((RasterLayer) baseLayer);
      }
      if (baseLayer instanceof VectorLayer) {
        vectorLayers.add((VectorLayer<VectorElement>) baseLayer);
      }
    }
    
    for (Layer layer : layers) {
      allLayers.add(layer);
      if (layer instanceof TileLayer) {
        tileLayers.add((TileLayer) layer);
      }
      if (layer instanceof RasterLayer) {
        rasterLayers.add((RasterLayer) layer);
      }
      if (layer instanceof VectorLayer) {
        vectorLayers.add((VectorLayer<VectorElement>) layer);
      }
    }
    
    this.allLayers = java.util.Collections.unmodifiableList(allLayers);
    this.tileLayers = java.util.Collections.unmodifiableList(tileLayers);
    this.rasterLayers = java.util.Collections.unmodifiableList(rasterLayers);
    this.vectorLayers = java.util.Collections.unmodifiableList(vectorLayers);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public Projection getBaseProjection() {
    Projection projection = DEFAULT_BASE_PROJECTION;
    readLock.lock();
    try {
      if (baseLayer != null) {
        projection = baseLayer.getProjection();
      }
    }
    finally {
      readLock.unlock();
    }
    return projection;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public List<Layer> getAllLayers() {
    List<Layer> allLayers;
    readLock.lock();
    try {
      if (this.allLayers == null) {
        rebuildLayers();
      }
      allLayers = this.allLayers;
    }
    finally {
      readLock.unlock();
    }
    return allLayers;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public List<TileLayer> getTileLayers() {
    List<TileLayer> tileLayers;
    readLock.lock();
    try {
      if (this.tileLayers == null) {
        rebuildLayers();
      }
      tileLayers = this.tileLayers;
    }
    finally {
      readLock.unlock();
    }
    return tileLayers;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public List<RasterLayer> getRasterLayers() {
    List<RasterLayer> rasterLayers;
    readLock.lock();
    try {
      if (this.rasterLayers == null) {
        rebuildLayers();
      }
      rasterLayers = this.rasterLayers;
    }
    finally {
      readLock.unlock();
    }
    return rasterLayers;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public List<VectorLayer<VectorElement>> getVectorLayers() {
    List<VectorLayer<VectorElement>> vectorLayers;
    readLock.lock();
    try {
      if (this.vectorLayers == null) {
        rebuildLayers();
      }
      vectorLayers = this.vectorLayers;
    }
    finally {
      readLock.unlock();
    }
    return vectorLayers;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void onStartMapping() {
    readLock.lock();
    try {
      rebuildLayers();
      for (VectorLayer<VectorElement> vectorLayer : vectorLayers) {
        vectorLayer.onStartMapping();
      }
    }
    finally {
      readLock.unlock();
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void onStopMapping() {
    readLock.lock();
    try {
      rebuildLayers();
      for (VectorLayer<VectorElement> vectorLayer : vectorLayers) {
        vectorLayer.onStopMapping();
      }
    }
    finally {
      readLock.unlock();
    }
  }
}
