package com.nutiteq.layers;

import java.util.List;

import com.nutiteq.components.Components;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderers.components.MapTileDrawData;
import com.nutiteq.renderers.components.MapTileProxy;
import com.nutiteq.renderers.components.MapTileQuadTreeNode;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.Const;

public abstract class TileLayer extends Layer {
  protected final int id;
  protected final long tileIdOffset;

  protected boolean memoryCaching = true;
  protected boolean persistentCaching = false;

  protected final int minZoom;
  protected final int maxZoom;

  protected int fetchPriority = 0;

  protected TileLayer(Projection projection, int minZoom, int maxZoom, int id) {
    super(projection);
    this.minZoom = minZoom;
    this.maxZoom = maxZoom;
    this.id = id;
    this.tileIdOffset = id * Const.TILE_ID_OFFSET;
  }

  /**
   * Return the layer id specified in the constructor.
   *
   * @return layer id.
   */
  public int getId() {
    return id;
  }

  /**
   * Get tile id offset for the layer. This id offset should be added to the tile id before inserting tiles to the cache. 
   * 
   * @return tile id offset for the layer.
   */
  public long getTileIdOffset() {
    return tileIdOffset;
  }

  /**
   * Gets the minimum zoom used by this raster layer.
   * 
   * @return the minimum zoom level.
   */
  public int getMinZoom() {
    return minZoom;
  }

  /**
   * Gets the maximum zoom used by this raster layer.
   * 
   * @return the maximum zoom level.
   */
  public int getMaxZoom() {
    return maxZoom;
  }
  
  /**
   * Check if in-memory caching is enabled for this layer.
   * 
   * @return true if enabled, false if not.
   */
  public boolean isMemoryCaching() {
    return memoryCaching;
  }
  
  /**
   * Enable/disable in-memory caching for this layer.
   * 
   * @param flag
   *          if true, in-memory cache will be used. If false, not. By default in-memory caching is enabled.
   */
  public void setMemoryCaching(boolean flag) {
    this.memoryCaching = flag;
  }
  
  /**
   * Check if persistent caching is enabled for this layer.
   * 
   * @return true if enabled, false if not.
   */
  public boolean isPersistentCaching() {
    return persistentCaching;
  }
  
  /**
   * Enable/disable persistent caching for this layer.
   * 
   * @param flag
   *          if true, persistent cache will be used. If false, not. By default persistent caching is disabled.
   */
  public void setPersistentCaching(boolean flag) {
    this.persistentCaching = flag;
  }
  
  /**
   * Get priority of data retrieval. Default is 0.
   * 
   * @return relative priority value
   */
  public int getFetchPriority() {
    return fetchPriority;
  }
  
  /**
   * Set priority for data retrieval. If this layer downloads data from network,
   * this method allows to prioritize base layer by giving higher priority value to base layer
   * and lower priority values to other layers.
   * 
   * @param priority
   *          relative priority. Default priority is 0.
   */
  public void setFetchPriority(int priority) {
    this.fetchPriority = priority;
  }
  
  public abstract boolean getTilePreloading();
  
  public abstract long getVisibleTilesTimeStamp();
  
  public abstract List<MapTileDrawData> getVisibleTiles();

  public abstract void setVisibleTiles(List<MapTileDrawData> visibleTiles);
  
  public void updateVisibleTiles() {
    Components components = getComponents();
    if (components != null) {
      components.mapRenderers.getMapRenderer().layerChanged(this);
    }
  }

  public abstract boolean resolveTile(MapTileQuadTreeNode tile, List<MapTileProxy> proxies);

  @Override
  public void setRenderProjection(RenderProjection renderProjection) {
    if (renderProjection != this.renderProjection) {
      this.renderProjection = renderProjection;
      setVisibleTiles(null);
      
      updateVisibleTiles();
    }
  }

}
