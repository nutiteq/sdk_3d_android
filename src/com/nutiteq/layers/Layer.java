package com.nutiteq.layers;

import com.nutiteq.components.Components;
import com.nutiteq.components.Range;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;

/**
 * The abstract layer class, inherited by all other layer subclasses. Contains methods for setting and checking the
 * visibility of the layer.
 */
public abstract class Layer {
  protected Projection projection;
  protected RenderProjection renderProjection;

  protected boolean visible = true;
  protected Range visibleZoomRange = new Range(0, Float.POSITIVE_INFINITY);

  protected volatile Components components; // NOTE: components can be null and can be accessed from multiple threads

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the layer.
   */
  protected Layer(Projection projection) {
    this.projection = projection;
  }

  /**
   * Gets the projection used by this layer.
   * 
   * @return the projection used by this layer
   */
  public Projection getProjection() {
    return projection;
  }

  /**
   * Checks the visibility of this layer.
   * 
   * @return true if the layer is visible.
   */
  public boolean isVisible() {
    return visible;
  }

  /**
   * Sets the visibility of this layer.
   * 
   * @param visible
   *          the new visibility state of the layer
   */
  public void setVisible(boolean visible) {
    if (this.visible != visible) {
      this.visible = visible;
      Components components = getComponents();
      if (components != null) {
        components.mapRenderers.getMapRenderer().layerChanged(this);
      }
    }
  }

  /**
   * Get visible zoom range. Current zoom level must be within this range for the layer to be visible.
   * 
   * @return current visible zoom range
   */
  public Range getVisibleZoomRange() {
    return visibleZoomRange;
  }
  
  /**
   * Set visible zoom range. Current zoom level must be within this range for the layer to be visible.
   * This range is half-open, thus layer is visible if range.min <= ZOOMLEVEL < range.max
   * 
   * @param range new visible zoom range
   */
  public void setVisibleZoomRange(Range range) {
    if (!this.visibleZoomRange.equals(range)) {
      visibleZoomRange = range;
      Components components = getComponents();
      if (components != null) {
        components.mapRenderers.getMapRenderer().layerChanged(this);
      }
    }
  }
  
  /**
   * Test if given zoom level is within visible zoom range.
   * 
   * @param zoom zoom level to test
   * @return true if given zoom level is within visible zoom range. Note that the range is half-open.
   */
  public boolean isInVisibleZoomRange(float zoom) {
    return zoom >= visibleZoomRange.min && zoom < visibleZoomRange.max;
  }

  /**
   * Get mapping components attached to this layer.
   * 
   * @return components instance.
   */
  public Components getComponents() {
    return components;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized void setComponents(Components components) {
    this.components = components;
    if (components != null) {
      RenderProjection renderProjection = components.mapRenderers.getMapRenderer().getRenderSurface().getRenderProjection();
      if (renderProjection != this.renderProjection) {
        setRenderProjection(renderProjection);
      }
      components.mapRenderers.getMapRenderer().layerChanged(this);
    }
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public RenderProjection getRenderProjection() {
    return renderProjection;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void setRenderProjection(RenderProjection renderProjection) {
    this.renderProjection = renderProjection;
  }

}
