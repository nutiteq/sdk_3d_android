package com.nutiteq.renderprojections;

import java.util.List;

import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.MutablePoint3D;
import com.nutiteq.components.MutableVector3D;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.utils.Matrix;
import com.nutiteq.utils.Utils;

/**
 * Spherical render projection.
 * @pad.exclude
 */
public class SphericalRenderProjection extends RenderProjection {
  private static final double TESSELATION_DIVIDE_THRESHOLD_PER_DEGREE = 5.0;

  private final double radius;
  private final double scale;

  public SphericalRenderProjection(double radius, double size) {
    this.radius = radius;
    this.scale = size / radius;
  }

  @Override
  public void project(double u, double v, double h, MutablePoint3D pos) {
    double phi = u * (Math.PI / 180);
    double rho = v * (Math.PI / 180);
    double r = (radius + h) * scale;

    pos.x = r * Math.cos(rho) * Math.cos(phi);
    pos.y = r * Math.cos(rho) * Math.sin(phi);
    pos.z = r * Math.sin(rho);
  }

  @Override
  public void unproject(double x, double y, double z, MutableMapPos uvh) {
    double r2 = x * x + y * y + z * z;
    double r = Math.max(Math.abs(x), Math.max(Math.abs(y), Math.max(Math.abs(z), Math.sqrt(r2))));

    double sin_rho = z / r;
    double rho = Math.asin(sin_rho);
    double cos_rho = Math.cos(rho);
    double cos_phi = x / r / cos_rho;
    double sin_phi = y / r / cos_rho;
    double phi = Math.atan2(sin_phi, cos_phi);

    uvh.x = phi * (180 / Math.PI);
    uvh.y = rho * (180 / Math.PI);
    uvh.z = r / scale - radius;
  }

  @Override
  public void setNormal(double x, double y, double z, MutableVector3D normal) {
    double length = Math.sqrt(x * x + y * y + z * z);
    normal.x = x / length;
    normal.y = y / length;
    normal.z = z / length;
  }

  @Override
  public void tesselateLine(Point3D point1, Point3D point2, List<Point3D> pointList) {
    double dist = getDistance(point1, point2);
    double r = radius * scale;
    if (dist < TESSELATION_DIVIDE_THRESHOLD_PER_DEGREE * r * Math.PI / 180) {
      pointList.add(point1);
      pointList.add(point2);
      return;
    }
    Vector3D v = new Vector3D(point1.x + point2.x, point1.y + point2.y, point1.z + point2.z);
    if (v.getLength() == 0) {
      return;
    }
    double c = r / v.getLength();
    Point3D midpoint = new Point3D(v.x * c, v.y * c, v.z * c);
    tesselateLine(point1, midpoint, pointList);
    pointList.remove(pointList.size() - 1);
    tesselateLine(midpoint, point2, pointList);
  }

  @Override
  public void tesselateTriangle(Point3D point1, Point3D point2, Point3D point3, List<Point3D> pointList) {
    double dist12 = getDistance(point1, point2);
    double dist23 = getDistance(point2, point3);
    double dist31 = getDistance(point3, point1);
    double maxDist = Math.max(dist12, Math.max(dist23, dist31));
    double r = radius * scale;
    if (maxDist < TESSELATION_DIVIDE_THRESHOLD_PER_DEGREE * r * Math.PI / 180) {
      pointList.add(point1);
      pointList.add(point2);
      pointList.add(point3);
      return;
    }

    if (dist23 > dist12 && dist23 > dist31) {
      Point3D temp = point1;
      point1 = point2;
      point2 = point3;
      point3 = temp;      
    } else if (dist31 > dist12 && dist31 > dist23) {
      Point3D temp = point3;
      point3 = point2;
      point2 = point1;
      point1 = temp;
    }

    Vector3D v = new Vector3D(point1.x + point2.x, point1.y + point2.y, point1.z + point2.z);
    if (v.getLength() == 0) {
      return;
    }
    double c = r / v.getLength();
    Point3D midpoint = new Point3D(v.x * c, v.y * c, v.z * c);
    tesselateTriangle(point1, midpoint, point3, pointList);
    tesselateTriangle(midpoint, point2, point3, pointList);
  }

  @Override
  public double getDistance(Point3D point1, Point3D point2) {
    Vector3D v1 = new Vector3D(point1.x, point1.y, point1.z).getNormalized();
    Vector3D v2 = new Vector3D(point2.x, point2.y, point2.z).getNormalized();
    double cos = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    return Math.acos(Utils.toRange(cos, -1, 1)) * radius * scale;
  }

  @Override
  public double[] getTranslateMatrix(Point3D point1, Point3D point2, double t) {
    Vector3D v1 = new Vector3D(point1.x, point1.y, point1.z).getNormalized();
    Vector3D v2 = new Vector3D(point2.x, point2.y, point2.z).getNormalized();
    Vector3D n = Vector3D.crossProduct(v1, v2);
    if (n.getLength() < 1.0e-6) {
      // Special case - points are very close together, use linear transformation
      double[] matrix = new double[16];
      Matrix.setTranslateM(matrix, (point2.x - point1.x) * t, (point2.y - point1.y) * t, (point2.z - point1.z) * t);
      return matrix;
    }
    n = n.getNormalized();

    double cos = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    double angle = Math.acos(Utils.toRange(cos, -1, 1));
    double c = Math.cos(angle * t);
    double s = Math.sin(angle * t);

    // Rodrigues formula in matrix notation
    double[] matrix = new double[16];
    matrix[0]  = n.x*n.x + (1 - n.x*n.x) * c;
    matrix[1]  = n.x*n.y * (1 - c) + n.z * s;
    matrix[2]  = n.x*n.z * (1 - c) - n.y * s;
    matrix[4]  = n.y*n.x * (1 - c) - n.z * s;
    matrix[5]  = n.y*n.y + (1 - n.y*n.y) * c;
    matrix[6]  = n.y*n.z * (1 - c) + n.x * s;
    matrix[8]  = n.z*n.x * (1 - c) + n.y * s;
    matrix[9]  = n.z*n.y * (1 - c) - n.x * s;
    matrix[10] = n.z*n.z + (1 - n.z*n.z) * c;
    matrix[15] = 1;

    // Scale
    double scale = Utils.lerp(1, new Vector3D(point2.x, point2.y, point2.z).getLength() / new Vector3D(point1.x, point1.y, point1.z).getLength(), t);
    for (int i = 0; i < 12; i++) {
      matrix[i] *= scale;
    }

    return matrix;
  }

  @Override
  public double[] getGlobalFrameMatrix(MapPos mapPos, boolean mapScale) {
    double phi = mapPos.x * (Math.PI / 180);
    double rho = mapPos.y * (Math.PI / 180);

    double cos_rho = Math.cos(rho);
    double sin_rho = Math.sin(rho);
    double cos_phi = Math.cos(phi);
    double sin_phi = Math.sin(phi);

    // Find world position
    MutablePoint3D worldPos = new MutablePoint3D();
    project(mapPos.x, mapPos.y, mapPos.z, worldPos);

    // Partial derivatives
    double dx_du = -sin_phi * cos_rho;
    double dx_dv = -cos_phi * sin_rho;
    double dx_dh =  cos_phi * cos_rho;

    double dy_du =  cos_phi * cos_rho;
    double dy_dv = -sin_phi * sin_rho;
    double dy_dh =  sin_phi * cos_rho;

    double dz_du =  0;
    double dz_dv =  cos_rho;
    double dz_dh =  sin_rho;

    double c = 1.0 / (4 * Math.PI);
    double du_scale = c / Math.sqrt(dx_du * dx_du + dy_du * dy_du + dz_du * dz_du);
    double dv_scale = c / Math.sqrt(dx_dv * dx_dv + dy_dv * dy_dv + dz_dv * dz_dv);
    double dh_scale = c / Math.sqrt(dx_dh * dx_dh + dy_dh * dy_dh + dz_dh * dz_dh);

    // Jacobian matrix of scaled partial derivatives + translation to world position
    double[] matrix = new double[16];
    matrix[0]  = dx_du * du_scale;
    matrix[1]  = dy_du * du_scale;
    matrix[2]  = dz_du * du_scale;
    matrix[4]  = dx_dv * dv_scale;
    matrix[5]  = dy_dv * dv_scale;
    matrix[6]  = dz_dv * dv_scale;
    matrix[8]  = dx_dh * dh_scale;
    matrix[9]  = dy_dh * dh_scale;
    matrix[10] = dz_dh * dh_scale;
    matrix[12] = worldPos.x;
    matrix[13] = worldPos.y;
    matrix[14] = worldPos.z;
    matrix[15] = 1;

    return matrix;
  }

  @Override
  public double[] getLocalFrameMatrix(Point3D point) {
    double x = point.x;
    double y = point.y;
    double z = point.z;

    double dx_dh = x;
    double dy_dh = y;
    double dz_dh = z;

    double dx_du = -y;
    double dy_du = x;
    double dz_du = 0;

    double dx_dv = -z * x;
    double dy_dv = -z * y;
    double dz_dv = x * x + y * y;

    double du_scale = 1.0 / Math.sqrt(dx_du * dx_du + dy_du * dy_du);
    double dh_scale = 1.0 / Math.sqrt(dx_dh * dx_dh + dy_dh * dy_dh + dz_dh * dz_dh);
    double dv_scale = du_scale * dh_scale;

    double[] matrix = new double[16];
    matrix[0]  = dx_du * du_scale;
    matrix[1]  = dy_du * du_scale;
    matrix[2]  = dz_du * du_scale;
    matrix[4]  = dx_dv * dv_scale;
    matrix[5]  = dy_dv * dv_scale;
    matrix[6]  = dz_dv * dv_scale;
    matrix[8]  = dx_dh * dh_scale;
    matrix[9]  = dy_dh * dh_scale;
    matrix[10] = dz_dh * dh_scale;
    matrix[15] = 1;

    return matrix;
  }

}
