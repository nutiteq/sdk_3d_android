package com.nutiteq.renderprojections;

import java.util.List;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.MutablePoint3D;
import com.nutiteq.components.MutableVector3D;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.utils.Matrix;

/**
 * Flat render projection.
 * 
 * Used for planar maps. Depends on specific projection.
 * @pad.exclude
 */
public class PlanarRenderProjection extends RenderProjection {
  private static final Vector3D NORMAL = new Vector3D(0, 0, 1);
  private static final double[] LOCAL_FRAME_MATRIX = new double[] { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

  /**
   * Null transform implementation. Can be used when projection and base projection are equal.
   */
  protected class NullTransform implements Transform {
    protected NullTransform() {
    }
    
    @Override
    public void project(double x, double y, double z, MutablePoint3D pos) {
      pos.x = (x - originX) * scaleX;
      pos.y = (y - originY) * scaleY;
      pos.z = z * scaleZ;
    }

    @Override
    public void unproject(double x, double y, double z, MutableMapPos mapPos) {
      mapPos.x = x / scaleX + originX;
      mapPos.y = y / scaleY + originY;
      mapPos.z = z / scaleZ;
    }
  }

  private final Projection baseProjection;
  private final double radius;
  private final double size;
  private final double originX;
  private final double originY;
  private final double scaleX;
  private final double scaleY;
  private final double scaleZ;
  private final Transform nullTransform = new NullTransform();

  public PlanarRenderProjection(Projection baseProjection, double radius, double size) {
    this.baseProjection = baseProjection;
    this.radius = radius;
    this.size = size;

    Bounds bounds = baseProjection.getBounds();
    scaleX = (size * 2) / Math.min(bounds.getWidth(), bounds.getHeight());
    scaleY = scaleX;
    scaleZ = (size * 2) / (radius * 2 * Math.PI);
    originX = bounds.left + bounds.getWidth() / 2;
    originY = bounds.bottom + bounds.getHeight() / 2;
  }
  
  public Projection getBaseProjection() {
    return baseProjection;
  }
  
  @Override
  public void project(double u, double v, double h, MutablePoint3D pos) {
    MutableMapPos mapPos = new MutableMapPos();
    baseProjection.fromInternal(u, v, h, mapPos);
    pos.x = (mapPos.x - originX) * scaleX;
    pos.y = (mapPos.y - originY) * scaleY;
    pos.z = mapPos.z * scaleZ;
  }
  
  @Override
  public void unproject(double x, double y, double z, MutableMapPos uvh) {
    x = x / scaleX + originX;
    y = y / scaleY + originY;
    z = z / scaleZ;
    baseProjection.toInternal(x, y, z, uvh);
  }
  
  @Override
  public Vector3D getNormal(Point3D point) {
    return NORMAL;
  }

  @Override
  public void setNormal(double x, double y, double z, MutableVector3D normal) {
    normal.x = NORMAL.x;
    normal.y = NORMAL.y;
    normal.z = NORMAL.z;
  }
  
  @Override
  public void tesselateLine(Point3D point1, Point3D point2, List<Point3D> pointList) {
    pointList.add(point1);
    pointList.add(point2);
  }

  @Override
  public void tesselateTriangle(Point3D point1, Point3D point2, Point3D point3, List<Point3D> pointList) {
    pointList.add(point1);
    pointList.add(point2);
    pointList.add(point3);
  }

  @Override
  public double getDistance(Point3D point1, Point3D point2) {
    return new Vector3D(point1, point2).getLength();
  }
  
  @Override
  public double[] getTranslateMatrix(Point3D point1, Point3D point2, double t) {
    Vector3D delta = new Vector3D(point1, point2);
    double[] matrix = new double[16];
    Matrix.setTranslateM(matrix, delta.x * t, delta.y * t, delta.z * t);
    return matrix;
  }
  
  @Override
  public double[] getGlobalFrameMatrix(MapPos mapPos, boolean mapScale) {
    // Find world position
    MutablePoint3D worldPos = new MutablePoint3D();
    project(mapPos.x, mapPos.y, mapPos.z, worldPos);

    // Modify base projection internal matrix
    double[] matrix;
    if (mapScale) {
      matrix = baseProjection.getInternalFrameMatrix(mapPos.x, mapPos.y, mapPos.z).clone();
    } else {
      matrix = baseProjection.getInternalFrameMatrix(0, 0, 0).clone();
    }
    matrix[0]  *= size / 180;
    matrix[4]  *= size / 180;
    matrix[8]  *= size / 180;
    matrix[12] *= size / 180;

    matrix[1]  *= size / 180;
    matrix[5]  *= size / 180;
    matrix[9]  *= size / 180;
    matrix[13] *= size / 180;

    matrix[2]  *= size / (Math.PI * radius);
    matrix[6]  *= size / (Math.PI * radius);
    matrix[10] *= size / (Math.PI * radius);
    matrix[14] *= size / (Math.PI * radius);

    matrix[12] += worldPos.x;
    matrix[13] += worldPos.y;
    matrix[14] += worldPos.z;

    return matrix;
  }

  @Override
  public double[] getLocalFrameMatrix(Point3D point) {
    return LOCAL_FRAME_MATRIX;
  }
  
  @Override
  public Transform getTransform(Projection projection) {
    if (projection.equals(baseProjection)) {
      return nullTransform; 
    }
    return super.getTransform(projection);
  }

}
