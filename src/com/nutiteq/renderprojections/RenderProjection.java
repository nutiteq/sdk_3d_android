package com.nutiteq.renderprojections;

import java.util.List;

import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.MutablePoint3D;
import com.nutiteq.components.MutableVector3D;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.projections.Projection;

/**
 * Abstract base class for all render projections.
 * 
 * Render projections define surface for maps and provide methods for
 * transforming points between internal and render surface coordinate systems plus various other transformations.
 */
public abstract class RenderProjection {
  
  /**
   * Transformation interface between specified projection and rendering coordinates.
   */
  public interface Transform {
    void project(double x, double y, double z, MutablePoint3D pos);
    void unproject(double x, double y, double z, MutableMapPos mapPos);
  }
  
  /**
   * Default transform implementation.
   */
  protected class DefaultTransform implements Transform {
    private final Projection projection;
    private final MutableMapPos mapPosInternal = new MutableMapPos();
    
    protected DefaultTransform(Projection projection) {
      this.projection = projection;
    }
    
    @Override
    public void project(double x, double y, double z, MutablePoint3D pos) {
      projection.toInternal(x, y, z, mapPosInternal);
      RenderProjection.this.project(mapPosInternal.x, mapPosInternal.y, mapPosInternal.z, pos);
    }

    @Override
    public void unproject(double x, double y, double z, MutableMapPos mapPos) {
      RenderProjection.this.unproject(x, y, z, mapPosInternal);
      projection.fromInternal(mapPosInternal.x, mapPosInternal.y, mapPosInternal.z, mapPos);
    }
  }

  /**
   * Project map point given in internal coordinates to render surface coordinate system.
   * 
   * @param mapPos
   *          map point to project (in internal coordinates)
   * @return projected point
   */
  public Point3D project(MapPos mapPos) {
    MutablePoint3D point = new MutablePoint3D();
    project(mapPos.x, mapPos.y, mapPos.z, point);
    return new Point3D(point);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public abstract void project(double u, double v, double h, MutablePoint3D pos);

  /**
   * Unproject render surface point to internal coordinates.
   * 
   * @param point
   *          point to unproject
   * @return unprojected point on the map in internal coordinates
   */
  public MapPos unproject(Point3D point) {
    MutableMapPos mapPos = new MutableMapPos();
    unproject(point.x, point.y, point.z, mapPos);
    return new MapPos(mapPos);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public abstract void unproject(double x, double y, double z, MutableMapPos uvh);

  /**
   * Get surface normal at the given point.
   * 
   * @param point
   *          point for the normal.
   * @return normal vector (unit length)
   */
  public Vector3D getNormal(Point3D point) {
    MutableVector3D normal = new MutableVector3D();
    setNormal(point.x, point.y, point.z, normal);
    return new Vector3D(normal);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public abstract void setNormal(double x, double y, double z, MutableVector3D normal);

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public abstract void tesselateLine(Point3D point1, Point3D point2, List<Point3D> pointList);

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public abstract void tesselateTriangle(Point3D point1, Point3D point2, Point3D point3, List<Point3D> pointList);

  /**
   * Calculate distance between two points.
   * 
   * @param point1
   *          first point
   * @param point2
   *          second point
   * @return distance between the points
   */
  public abstract double getDistance(Point3D point1, Point3D point2);

  /**
   * Calculate translation matrix from one point to another.
   * 
   * @param point1
   *          first point
   * @param point2
   *          second point
   * @param t
   *          fractional distance between the points for the matrix. if 0, identity matrix is returned, if 1 then matrix that transforms point1 into point2 is returned.
   * @return matrix encoded as 16-element array
   */
  public abstract double[] getTranslateMatrix(Point3D point1, Point3D point2, double t);

  /**
   * Calculate global frame matrix from internal coordinates.
   * 
   * @param mapPos
   *          position on the map in internal coordinates
   * @param mapScale
   *          if true, use underlying map scale. If false, use uniform scaling.
   * @return matrix encoded as 16-element array
   */
  public abstract double[] getGlobalFrameMatrix(MapPos mapPos, boolean mapScale);

  /**
   * Calculate local frame matrix at given point.
   * 
   * @param point
   *          point for the local frame
   * @return matrix encoded as 16-element array. 
   */
  public abstract double[] getLocalFrameMatrix(Point3D point);

  /**
   * Return transformation object for given projection. NOTE: returned transform object is not thread safe!
   * 
   * @param projection
   *          projection for source coordinates
   * @return transformation object for converting between source coordinates and rendering coordinates
   */
  public Transform getTransform(Projection projection) {
    return new DefaultTransform(projection);
  }
}
