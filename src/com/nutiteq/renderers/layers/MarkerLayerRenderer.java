package com.nutiteq.renderers.layers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;

import com.nutiteq.cache.TextureBitmapCache;
import com.nutiteq.components.CameraState;
import com.nutiteq.components.Color;
import com.nutiteq.components.Point3D;
import com.nutiteq.geometry.Marker;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.renderers.components.PickingState;
import com.nutiteq.renderers.utils.BillBoardVertexBuilder;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.MarkerStyle;
import com.nutiteq.utils.ColorUtils;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.utils.Matrix;
import com.nutiteq.vectorlayers.MarkerLayer;

/**
 * Marker layer renderer.
 */
public class MarkerLayerRenderer extends BillBoardLayerRenderer {
  private static final int MAX_MARKERS_PER_DRAW_CALL = 2048; // should generate < 65536 vertices

  private final MarkerLayer layer;
  private final BillBoardVertexBuilder vertexBuilder;
  private final TextureBitmapCache bitmapCache;
  private final MarkerComparator markerComparator = new MarkerComparator();
  private final float[] pickingColor = new float[4];
  private final FloatVertexBuffer elementVertexBuf = new FloatVertexBuffer();
  private final FloatVertexBuffer elementTexCoordBuf = new FloatVertexBuffer();
  private final FloatVertexBuffer elementColorBuf = new FloatVertexBuffer();

  private Marker[] sortedElements = new Marker[0];
  private ArrayList<Marker> unsortedElements = new ArrayList<Marker>();
  private ArrayList<Marker> drawElements = new ArrayList<Marker>();

  /**
   * Marker comparator that respects display order and uses distance from camera plane for equal display order values.
   */
  private class MarkerComparator implements Comparator<Marker> {
    CameraState camera;

    @Override
    public int compare(Marker marker1, Marker marker2) {
      if (marker1.getDisplayOrder() != marker2.getDisplayOrder()) {
        return marker1.getDisplayOrder() < marker2.getDisplayOrder() ? -1 : 1;
      }

      Point3D worldPos1 = marker1.getInternalState().pos;
      Point3D worldPos2 = marker2.getInternalState().pos;
      
      double[] mvMatrix = camera.modelviewMatrix;
      float distance1 = (float) (worldPos1.x * mvMatrix[2] + worldPos1.y * mvMatrix[6] + worldPos1.z * mvMatrix[10] + mvMatrix[14]);
      float distance2 = (float) (worldPos2.x * mvMatrix[2] + worldPos2.y * mvMatrix[6] + worldPos2.z * mvMatrix[10] + mvMatrix[14]);
      return Float.compare(distance1, distance2); // put markers that are further away from camera first
    }
  }

  public MarkerLayerRenderer(MarkerLayer layer, RenderProjection renderProjection, TextureBitmapCache bitmapCache) {
    super(renderProjection);
    this.layer = layer;
    this.vertexBuilder = new BillBoardVertexBuilder(renderProjection); 
    this.bitmapCache = bitmapCache;
  }
  
  @Override
  public void synchronize(GL10 gl) {
  }
  
  @Override
  public boolean isSynchronized() {
    return true;
  }
  
  @Override
  public void dispose(GL10 gl) {
  }

  @Override
  public void draw(GL10 gl, CameraState camera, Pass pass) {
    if ((pass == Pass.OVERLAY) != layer.isZOrdered()) {
      return;
    }
    List<Marker> visibleElements = layer.getVisibleElements();
    if (visibleElements == null || visibleElements.isEmpty()) {
      return;
    }
    sortMarkers(visibleElements, camera);
    vertexBuilder.setCamera(camera);

    gl.glPushMatrix();
    gl.glLoadIdentity();

    Bitmap bitmap = null;
    for (Marker element : sortedElements) {
      MarkerStyle style = (MarkerStyle) element.getInternalState().activeStyle;
      if (style == null) {
        continue;
      }

      boolean draw = style.textureInfo.bitmap != bitmap && bitmap != null;
      if (drawElements.size() > MAX_MARKERS_PER_DRAW_CALL) {
        draw = true;
      }
      if (draw) {
        drawMarkers(gl, drawElements, bitmap, camera, null);
        drawElements.clear();
        bitmap = null;
      }

      drawElements.add(element);
      bitmap = style.textureInfo.bitmap;
    }
    if (bitmap != null) {
      drawMarkers(gl, drawElements, bitmap, camera, null);
      drawElements.clear();
    }
    
    gl.glPopMatrix();
    
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
  }
  
  @Override
  public Point3D calculateElementLabelPos(VectorElement element, CameraState camera, Point3D clickPos) {
    if (element instanceof Marker) {
      Marker marker = (Marker) element;
      MarkerStyle markerStyle = (MarkerStyle) marker.getInternalState().activeStyle;

      double[] elementMVMatrix = new double[16];
      vertexBuilder.setCamera(camera);
      if (!vertexBuilder.calculateElementMVMatrix(marker, markerStyle, marker.getInternalState().pos, marker.getInternalState().localFrameMatrix, 0, markerStyle.textureInfo.width, markerStyle.textureInfo.height, elementMVMatrix)) {
        return null;
      }

      double[] cameraInvMVMatrix = new double[16];
      Matrix.invertM(cameraInvMVMatrix, 0, camera.modelviewMatrix, 0);
      
      double[] vertex = new double[] { -markerStyle.labelAnchorX * 0.5, -markerStyle.labelAnchorY * 0.5, 0, 1, 0, 0, 0, 0 };
      Matrix.multiplyMV(vertex, 4, elementMVMatrix, 0, vertex, 0);
      Matrix.multiplyMV(vertex, 0, cameraInvMVMatrix, 0, vertex, 4);
      return new Point3D(vertex[0], vertex[1], vertex[2]);
    }
    return clickPos;
  }

  @Override
  public void drawPicking(GL10 gl, CameraState camera, PickingState picking, Pass pass) {
    if ((pass == Pass.OVERLAY) != layer.isZOrdered()) {
      return;
    }
    List<Marker> visibleElements = layer.getVisibleElements();
    if (visibleElements == null || visibleElements.isEmpty()) {
      return;
    }
    sortMarkers(visibleElements, camera);
    vertexBuilder.setCamera(camera);

    gl.glPushMatrix();
    gl.glLoadIdentity();

    Bitmap bitmap = null;
    for (Marker element : sortedElements) {
      MarkerStyle style = (MarkerStyle) element.getInternalState().activeStyle;
      if (style == null) {
        continue;
      }
      if (style.pickingTextureInfo == null) {
        continue;
      }

      boolean draw = style.pickingTextureInfo.bitmap != bitmap && bitmap != null;
      if (drawElements.size() > MAX_MARKERS_PER_DRAW_CALL) {
        draw = true;
      }
      if (draw) {
        drawMarkers(gl, drawElements, bitmap, camera, picking);
        drawElements.clear();
        bitmap = null;
      }
    
      drawElements.add(element);
      bitmap = style.pickingTextureInfo.bitmap;
    }
    if (bitmap != null) {
      drawMarkers(gl, drawElements, bitmap, camera, picking);
      drawElements.clear();
    }
    
    gl.glPopMatrix();

    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
  }
  
  private void sortMarkers(List<Marker> elements, CameraState camera) {
    unsortedElements.clear();
    for (Marker element : elements) {
      if (!element.getInternalState().visible) {
        continue;
      }
      unsortedElements.add(element);
    }
    if (sortedElements.length != unsortedElements.size()) {
      sortedElements = new Marker[unsortedElements.size()];
    }
    unsortedElements.toArray(sortedElements);
    if (layer.isZOrdered()) {
      markerComparator.camera = camera;
      java.util.Arrays.sort(sortedElements, markerComparator);
    }
  }
  
  private void drawMarkers(GL10 gl, List<Marker> elements, Bitmap bitmap, CameraState camera, PickingState picking) {
    elementVertexBuf.clear();
    elementTexCoordBuf.clear();
    elementColorBuf.clear();
    elementVertexBuf.reserve(elements.size() * 6 * 3);
    elementTexCoordBuf.reserve(elements.size() * 6 * 2);
    elementColorBuf.reserve(elements.size() * 6 * 4);
    for (Marker marker : elements) {
      MarkerStyle style = (MarkerStyle) marker.getInternalState().activeStyle;
      if (style == null) {
        continue;
      }
      Marker.MarkerInternalState internalState = marker.getInternalState();

      // Build vertices for marker
      int offset = elementVertexBuf.size();
      if (!vertexBuilder.generateElementVertices(marker, style, internalState.pos, internalState.localFrameMatrix, internalState.rotationDeg, elementVertexBuf, (picking != null ? style.pickingTextureInfo : style.textureInfo), elementTexCoordBuf)) {
        continue;
      }

      // Calculate vertex colors
      if (picking != null) {
        int index = picking.bindElement(marker);
        ColorUtils.encodeIntAsFloatColor(index, pickingColor);
        for (int i = offset; i < elementVertexBuf.size(); i += 3) {
          elementColorBuf.add(pickingColor[0], pickingColor[1], pickingColor[2], pickingColor[3]);
        }
      } else {
        Color color = style.color;
        for (int i = offset; i < elementVertexBuf.size(); i += 3) {
          elementColorBuf.add(color.r * color.a, color.g * color.a, color.b * color.a, color.a);
        }
      }
    }

    // Draw elements
    if (picking == null) {
      gl.glEnable(GL10.GL_TEXTURE_2D);
      gl.glBindTexture(GL10.GL_TEXTURE_2D, bitmapCache.getTexture(gl, bitmap));
    } else {
      gl.glEnable(GL10.GL_TEXTURE_2D);
      gl.glBindTexture(GL10.GL_TEXTURE_2D, bitmapCache.getTexture(gl, bitmap));
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_NEAREST);
    }

    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, elementVertexBuf.build(0, elementVertexBuf.size()));
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, elementTexCoordBuf.build(0, elementTexCoordBuf.size()));
    gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glColorPointer(4, GL10.GL_FLOAT, 0, elementColorBuf.build(0, elementColorBuf.size()));
    gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
    gl.glDrawArrays(GL10.GL_TRIANGLES, 0, elementVertexBuf.size() / 3);
  }
}
