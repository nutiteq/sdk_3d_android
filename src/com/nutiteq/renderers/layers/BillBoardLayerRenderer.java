package com.nutiteq.renderers.layers;

import com.nutiteq.renderprojections.RenderProjection;

/**
 * Abstract base class for billboard (markers/text) renderers. 
 */
public abstract class BillBoardLayerRenderer implements VectorLayerRenderer {
  protected final RenderProjection renderProjection;
  
  protected BillBoardLayerRenderer(RenderProjection renderProjection) {
    this.renderProjection = renderProjection;
  }
}
