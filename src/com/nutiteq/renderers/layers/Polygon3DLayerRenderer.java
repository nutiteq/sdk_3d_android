package com.nutiteq.renderers.layers;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Color;
import com.nutiteq.components.Point3D;
import com.nutiteq.geometry.Polygon3D;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.renderers.components.PickingState;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.Style;
import com.nutiteq.utils.ByteVertexBuffer;
import com.nutiteq.utils.ColorUtils;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.utils.GLUtils;
import com.nutiteq.vectorlayers.Polygon3DLayer;

/**
 * Renderer for 3D polygon layers.
 */
public class Polygon3DLayerRenderer implements VectorLayerRenderer {
  private final Polygon3DLayer layer;

  private FloatVertexBuffer polygonVertices = new FloatVertexBuffer();
  private ByteVertexBuffer polygonColors = new ByteVertexBuffer();
  private ByteVertexBuffer pickingColors = new ByteVertexBuffer();
  private byte[] pickingColor = new byte[4];

  public Polygon3DLayerRenderer(Polygon3DLayer layer, RenderProjection renderProjection) {
    this.layer = layer;
  }
  
  @Override
  public void synchronize(GL10 gl) {
  }
  
  @Override
  public boolean isSynchronized() {
    return true;
  }
  
  @Override
  public void dispose(GL10 gl) {
  }

  @Override
  public void draw(GL10 gl, CameraState camera, Pass pass) {
    if (pass != Pass.OVERLAY) {
      return;
    }
    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    
    buildPolygonBuffers(camera, null);

    int maxVertexBufferSize = GLUtils.getMaxVertexBufferSize(gl);
    for (int offset = 0; offset < polygonVertices.size() / 3; offset += maxVertexBufferSize) {
      int count = Math.min(maxVertexBufferSize, polygonVertices.size() / 3 - offset);
      gl.glVertexPointer(3, GL10.GL_FLOAT, 0, polygonVertices.build(offset * 3, count * 3));
      gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
      gl.glColorPointer(4, GL10.GL_UNSIGNED_BYTE, 0, polygonColors.build(offset * 4, count * 4));
      gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
      gl.glDrawArrays(GL10.GL_TRIANGLES, 0, count);
    }
    
    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
  }

  @Override
  public Point3D calculateElementLabelPos(VectorElement element, CameraState camera, Point3D clickPos) {
    return clickPos;
  }

  @Override
  public void drawPicking(GL10 gl, CameraState camera, PickingState picking, Pass pass) {
    if (pass != Pass.OVERLAY) {
      return;
    }
    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

    buildPolygonBuffers(camera, picking);

    int maxVertexBufferSize = GLUtils.getMaxVertexBufferSize(gl);
    for (int offset = 0; offset < polygonVertices.size() / 3; offset += maxVertexBufferSize) {
      int count = Math.min(maxVertexBufferSize, polygonVertices.size() / 3 - offset);
      gl.glVertexPointer(3, GL10.GL_FLOAT, 0, polygonVertices.build(offset * 3, count * 3));
      gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
      gl.glColorPointer(4, GL10.GL_UNSIGNED_BYTE, 0, pickingColors.build(offset * 4, count * 4));
      gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
      gl.glDrawArrays(GL10.GL_TRIANGLES, 0, count);
    }

    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
  }
  
  private void buildPolygonBuffers(CameraState camera, PickingState picking) {
    List<Polygon3D> visibleElements = layer.getVisibleElements();
    if (visibleElements == null) {
      visibleElements = new ArrayList<Polygon3D>();
    }

    Point3D cameraPos = camera.cameraPos;
    polygonVertices.clear();
    polygonColors.clear();
    pickingColors.clear();
    for (Polygon3D polygon3D : visibleElements) {
      Style style = polygon3D.getInternalState().activeStyle;
      if (style == null) {
        continue;
      }

      Point3D origin = polygon3D.getInternalState().origin;
      float dx = (float) (origin.x - cameraPos.x), dy = (float) (origin.y - cameraPos.y), dz = (float) (origin.z - cameraPos.z); 
      float[] vertices = polygon3D.getInternalState().vertices;
      for (int i = 0; i + 3 <= vertices.length; i += 3) {
        polygonVertices.add(vertices[i + 0] + dx, vertices[i + 1] + dy, vertices[i + 2] + dz);
      }

      Color color = style.color;
      float[] colors = polygon3D.getInternalState().colors;
      for (int i = 0; i < colors.length; i += 3) {
        float r = colors[i] * color.a * 255;
        float g = colors[i + 1] * color.a * 255;
        float b = colors[i + 2] * color.a * 255;
        polygonColors.add((byte) (color.r * r), (byte) (color.g * g), (byte) (color.b * b), (byte) (color.a * 255));
      }
      
      if (picking != null) {
        int index = picking.bindElement(polygon3D);
        ColorUtils.encodeIntAsColor(index, pickingColor);
        for (int i = 0; i < colors.length; i += 3) {
          pickingColors.add(pickingColor[0], pickingColor[1], pickingColor[2], pickingColor[3]);
        }
      }
    }
  }
}
