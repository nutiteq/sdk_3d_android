package com.nutiteq.renderers.layers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.cache.TextureInfoCache;
import com.nutiteq.components.CameraState;
import com.nutiteq.components.Color;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.TextureInfo;
import com.nutiteq.geometry.Geometry;
import com.nutiteq.geometry.Line.EdgeInfo;
import com.nutiteq.geometry.Line.LineInfo;
import com.nutiteq.geometry.Line.LineInternalState;
import com.nutiteq.geometry.Point.PointInfo;
import com.nutiteq.geometry.Point.PointInternalState;
import com.nutiteq.geometry.Polygon.PolygonInternalState;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.renderers.components.PickingState;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.LineStyle;
import com.nutiteq.style.PointStyle;
import com.nutiteq.style.PolygonStyle;
import com.nutiteq.style.Style;
import com.nutiteq.utils.ByteVertexBuffer;
import com.nutiteq.utils.ColorUtils;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.utils.GLUtils;
import com.nutiteq.utils.Utils;
import com.nutiteq.vectorlayers.GeometryLayer;

/**
 * Layer renderer for all geometry types (points, lines, polygons).
 */
public class GeometryLayerRenderer implements VectorLayerRenderer {
  private static final float LINE_ENDPOINT_TESSELATION_FACTOR = 0.2f / Const.UNIT_SIZE; // triangles per degree per unit width
  private static final List<Geometry> EMPTY_GEOMETRY_LIST = new ArrayList<Geometry>();
  
  private final GeometryLayer layer;
  private final TextureInfoCache styleCache;
  private final boolean clipLines;
  private long visibleElementsTimeStamp;
  private Map<Style, DrawRecord> drawMap = new HashMap<Style, DrawRecord>();
  private byte[] pickingColor = new byte[4];
  private double[] clipBuffer1 = new double[3];
  private double[] clipBuffer2 = new double[3];

  /**
   * Grouped element record for drawing. Elements are grouped using style.
   */
  private static class DrawRecord {
    ArrayList<Geometry> elements;

    FloatVertexBuffer pointVertices;
    FloatVertexBuffer pointTexCoords;
    Point3D pointOrigin;
    float pointZoomPow2;

    FloatVertexBuffer lineVertices;
    FloatVertexBuffer lineTexCoords;
    Point3D lineOrigin;
    float lineZoomPow2;

    FloatVertexBuffer polygonVertices;
    FloatVertexBuffer polygonTexCoords;
    Point3D polygonOrigin;
    float polygonZoom;

    FloatVertexBuffer pickingVertices;
    ByteVertexBuffer pickingColors;
    
    DrawRecord() {
      elements = new ArrayList<Geometry>();
      pointVertices = new FloatVertexBuffer();
      pointTexCoords = new FloatVertexBuffer();
      lineVertices = new FloatVertexBuffer();
      lineTexCoords = new FloatVertexBuffer();
      polygonVertices = new FloatVertexBuffer();
      polygonTexCoords = null;
      pickingVertices = new FloatVertexBuffer();
      pickingColors = new ByteVertexBuffer();
    }
  }

  public GeometryLayerRenderer(GeometryLayer layer, RenderProjection renderProjection, TextureInfoCache styleCache, boolean clipLines) {
    this.layer = layer;
    this.styleCache = styleCache;
    this.clipLines = clipLines;
  }
  
  @Override
  public void synchronize(GL10 gl) {
    long visibleElementsTimeStamp = layer.getVisibleElementsTimeStamp();
    if (visibleElementsTimeStamp == this.visibleElementsTimeStamp) {
      return;
    }

    List<Geometry> visibleElements = layer.isVisible() ? layer.getVisibleElements() : null;
    if (visibleElements == null) {
      visibleElements = EMPTY_GEOMETRY_LIST;
    }
    
    if (visibleElements.isEmpty() && !drawMap.isEmpty()) {
      drawMap.clear(); // release cache to reduce memory footprint
    }
    
    for (DrawRecord record : drawMap.values()) {
      record.elements.clear();
    }
    
    for (Geometry element : visibleElements) {
      Style style = element.getInternalState().activeStyle;
      if (style == null) {
        continue;
      }
      
      DrawRecord record = drawMap.get(style);
      if (record == null) {
        record = new DrawRecord();
        drawMap.put(style, record);
      }
      record.elements.add(element);
    }

    for (Iterator<Map.Entry<Style, DrawRecord>> it = drawMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<Style, DrawRecord> entry = it.next();
      Style style = entry.getKey();
      DrawRecord record = entry.getValue();
      if (record.elements.isEmpty()) {
        it.remove();
        continue;
      }

      if (style instanceof PolygonStyle) {
        PolygonStyle polygonStyle = (PolygonStyle) style;
        int vertices = 0;
        for (Geometry geometry : record.elements) {
          PolygonInternalState internalState = (PolygonInternalState) geometry.getInternalState(); 
          vertices += internalState.vertices.length / 3;
        }
        record.polygonOrigin = null;
        record.polygonVertices.reserve(vertices * 3);
        style = polygonStyle.lineStyle;
      }
      if (style instanceof LineStyle) {
        LineStyle lineStyle = (LineStyle) style;
        int vertices = 0;
        for (Geometry geometry : record.elements) {
          LineInternalState internalState = (LineInternalState) geometry.getInternalState();
          for (LineInfo lineInfo : internalState.lines) {
            vertices += lineInfo.edges.length * 6;
            if (lineStyle.lineJoinMode != LineStyle.NO_LINEJOIN) {
              vertices += lineInfo.edges.length * 3;
            }
          }
        }
        record.lineOrigin = null;
        record.lineVertices.reserve(vertices * 3);
        record.lineTexCoords.reserve(vertices * 2);
        style = lineStyle.pointStyle;
      }
      if (style instanceof PointStyle) {
        int vertices = 0;
        for (Geometry geometry : record.elements) {
          PointInternalState internalState = (PointInternalState) geometry.getInternalState();
          vertices += internalState.points.length * 6;
        }
        record.pointOrigin = null;
        record.pointVertices.reserve(vertices * 3);
        record.pointTexCoords.reserve(vertices * 2);
      }
    }
    
    this.visibleElementsTimeStamp = visibleElementsTimeStamp;
  }
  
  @Override
  public boolean isSynchronized() {
    return true;
  }
  
  @Override
  public void dispose(GL10 gl) {
    drawMap.clear();
  }
  
  @Override
  public void draw(GL10 gl, CameraState camera, Pass pass) {
    if (pass != Pass.BASE) {
      return;
    }
    if (drawMap.isEmpty()) {
      return;
    }
    
    int maxVertexBufferSize = GLUtils.getMaxVertexBufferSize(gl);
    for (Iterator<Map.Entry<Style, DrawRecord>> it = drawMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<Style, DrawRecord> entry = it.next();
      Style style = entry.getKey();
      DrawRecord record = entry.getValue();

      if (style instanceof PolygonStyle) {
        PolygonStyle polygonStyle = (PolygonStyle) style;
        buildPolygonBuffers(record, record.elements, polygonStyle, camera);

        Color color = polygonStyle.color;
        gl.glPushMatrix();
        gl.glTranslatef((float) (record.polygonOrigin.x - camera.cameraPos.x), (float) (record.polygonOrigin.y - camera.cameraPos.y), (float) (record.polygonOrigin.z - camera.cameraPos.z));
        gl.glColor4f(color.r * color.a, color.g * color.a, color.b * color.a, color.a);
        if (polygonStyle.patternTextureInfo != null) {
          gl.glEnable(GL10.GL_TEXTURE_2D);
          gl.glBindTexture(GL10.GL_TEXTURE_2D, styleCache.getTexture(gl, polygonStyle.patternTextureInfo));
        } else {
          gl.glDisable(GL10.GL_TEXTURE_2D);
        }
        for (int offset = 0; offset < record.polygonVertices.size() / 3; offset += maxVertexBufferSize) {
          int count = Math.min(maxVertexBufferSize, record.polygonVertices.size() / 3 - offset);
          gl.glVertexPointer(3, GL10.GL_FLOAT, 0, record.polygonVertices.build(offset * 3, count * 3));
          gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
          if (record.polygonTexCoords != null) {
            gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, record.polygonTexCoords.build(offset * 2, count * 2));
            gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
          } else {
            gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
          }
          gl.glDrawArrays(GL10.GL_TRIANGLES, 0, count);
        }
        gl.glPopMatrix();
        style = polygonStyle.lineStyle;
      }
      if (style instanceof LineStyle) {
        LineStyle lineStyle = (LineStyle) style;
        buildLineBuffers(record, record.elements, lineStyle, camera);

        Color color = lineStyle.color;
        gl.glPushMatrix();
        gl.glTranslatef((float) (record.lineOrigin.x - camera.cameraPos.x), (float) (record.lineOrigin.y - camera.cameraPos.y), (float) (record.lineOrigin.z - camera.cameraPos.z));
        gl.glColor4f(color.r * color.a, color.g * color.a, color.b * color.a, color.a);
        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, styleCache.getTexture(gl, lineStyle.textureInfo));
        for (int offset = 0; offset < record.lineVertices.size() / 3; offset += maxVertexBufferSize) {
          int count = Math.min(maxVertexBufferSize, record.lineVertices.size() / 3 - offset);
          gl.glVertexPointer(3, GL10.GL_FLOAT, 0, record.lineVertices.build(offset * 3, count * 3));
          gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
          gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, record.lineTexCoords.build(offset * 2, count * 2));
          gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
          gl.glDrawArrays(GL10.GL_TRIANGLES, 0, count);
        }
        gl.glPopMatrix();
        style = lineStyle.pointStyle;
      }
      if (style instanceof PointStyle) {
        PointStyle pointStyle = (PointStyle) style;
        buildPointBuffers(record, record.elements, pointStyle, camera);

        Color color = pointStyle.color;
        gl.glPushMatrix();
        gl.glTranslatef((float) (record.pointOrigin.x - camera.cameraPos.x), (float) (record.pointOrigin.y - camera.cameraPos.y), (float) (record.pointOrigin.z - camera.cameraPos.z));
        gl.glColor4f(color.r * color.a, color.g * color.a, color.b * color.a, color.a);
        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, styleCache.getTexture(gl, pointStyle.textureInfo));
        for (int offset = 0; offset < record.pointVertices.size() / 3; offset += maxVertexBufferSize) {
          int count = Math.min(maxVertexBufferSize, record.pointVertices.size() / 3 - offset);
          gl.glVertexPointer(3, GL10.GL_FLOAT, 0, record.pointVertices.build(offset * 3, count * 3));
          gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
          gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, record.pointTexCoords.build(offset * 2, count * 2));
          gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
          gl.glDrawArrays(GL10.GL_TRIANGLES, 0, count);
        }
        gl.glPopMatrix();
      }
    }

    gl.glColor4f(1, 1, 1, 1);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
  }

  @Override
  public Point3D calculateElementLabelPos(VectorElement element, CameraState camera, Point3D clickPos) {
    return clickPos;
  }
  
  @Override
  public void drawPicking(GL10 gl, CameraState camera, PickingState picking, Pass pass) {
    if (pass != Pass.BASE) {
      return;
    }
    if (drawMap.isEmpty()) {
      return;
    }

    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

    int maxVertexBufferSize = GLUtils.getMaxVertexBufferSize(gl);
    for (Iterator<Map.Entry<Style, DrawRecord>> it = drawMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<Style, DrawRecord> entry = it.next();
      Style style = entry.getKey();
      DrawRecord record = entry.getValue();

      buildBuffersPicking(record, record.elements, style, camera, picking);

      for (int offset = 0; offset < record.pickingVertices.size() / 3; offset += maxVertexBufferSize) {
        int count = Math.min(maxVertexBufferSize, record.pickingVertices.size() / 3 - offset);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, record.pickingVertices.build(offset * 3, count * 3));
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glColorPointer(4, GL10.GL_UNSIGNED_BYTE, 0, record.pickingColors.build(offset * 4, count * 4));
        gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
        gl.glDrawArrays(GL10.GL_TRIANGLES, 0, count);
      }
    }

    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
  }

  private void buildPointBuffers(DrawRecord record, List<Geometry> elements, PointStyle pointStyle, CameraState camera) {
    if (record.pointOrigin != null && record.pointZoomPow2 == camera.zoomPow2) {
      return;
    }
    Point3D cameraPos = camera.cameraPos;
    TextureInfo textureInfo = pointStyle.textureInfo;
    float du = 0.5f * textureInfo.width  / camera.zoomPow2;
    float dv = 0.5f * textureInfo.height / camera.zoomPow2;

    float u00 = textureInfo.getTexU0(), v00 = textureInfo.getTexV0();
    float u10 = textureInfo.getTexU1(), v10 = textureInfo.getTexV0();
    float u01 = textureInfo.getTexU0(), v01 = textureInfo.getTexV1();
    float u11 = textureInfo.getTexU1(), v11 = textureInfo.getTexV1();

    record.pointVertices.clear();
    record.pointTexCoords.clear();
    record.pointOrigin = cameraPos;
    record.pointZoomPow2 = camera.zoomPow2;
    for (Geometry element : elements) {
      PointInfo[] points = ((PointInternalState) element.getInternalState()).points;
      for (PointInfo pointInfo : points) {
        float x0 = (float) (pointInfo.x - cameraPos.x), y0 = (float) (pointInfo.y - cameraPos.y), z0 = (float) (pointInfo.z - cameraPos.z);

        float x00 = -du * pointInfo.dx_du - dv * pointInfo.dx_dv + x0;
        float y00 = -du * pointInfo.dy_du - dv * pointInfo.dy_dv + y0;
        float z00 = -du * pointInfo.dz_du - dv * pointInfo.dz_dv + z0;

        float x01 = -du * pointInfo.dx_du + dv * pointInfo.dx_dv + x0;
        float y01 = -du * pointInfo.dy_du + dv * pointInfo.dy_dv + y0;
        float z01 = -du * pointInfo.dz_du + dv * pointInfo.dz_dv + z0;

        float x10 =  du * pointInfo.dx_du - dv * pointInfo.dx_dv + x0;
        float y10 =  du * pointInfo.dy_du - dv * pointInfo.dy_dv + y0;
        float z10 =  du * pointInfo.dz_du - dv * pointInfo.dz_dv + z0;

        float x11 =  du * pointInfo.dx_du + dv * pointInfo.dx_dv + x0;
        float y11 =  du * pointInfo.dy_du + dv * pointInfo.dy_dv + y0;
        float z11 =  du * pointInfo.dz_du + dv * pointInfo.dz_dv + z0;

        record.pointVertices.add(x00, y00, z00);
        record.pointTexCoords.add(u00, v00);
        record.pointVertices.add(x10, y10, z10);
        record.pointTexCoords.add(u10, v10);
        record.pointVertices.add(x01, y01, z01);
        record.pointTexCoords.add(u01, v01);

        record.pointVertices.add(x10, y10, z10);
        record.pointTexCoords.add(u10, v10);
        record.pointVertices.add(x11, y11, z11);
        record.pointTexCoords.add(u11, v11);
        record.pointVertices.add(x01, y01, z01);
        record.pointTexCoords.add(u01, v01);
      }
    }
  }

  private void buildLineBuffers(DrawRecord record, List<Geometry> elements, LineStyle lineStyle, CameraState camera) {
    Point3D cameraPos = camera.cameraPos;
    TextureInfo textureInfo = lineStyle.textureInfo;
    float sx = 0.5f * textureInfo.width / camera.zoomPow2;
    float sv = textureInfo.getTexSV();
    boolean clipLine = textureInfo.bitmap.getHeight() > 1;
    if (record.lineOrigin != null && record.lineZoomPow2 == camera.zoomPow2 && !(clipLine && clipLines)) {
      return;
    }

    record.lineVertices.clear();
    record.lineTexCoords.clear();
    record.lineOrigin = cameraPos;
    record.lineZoomPow2 = camera.zoomPow2;
    for (Geometry element : elements) {
      LineInfo[] lines = ((LineInternalState) element.getInternalState()).lines;
      for (LineInfo lineInfo : lines) {
        EdgeInfo[] edges = lineInfo.edges;
        if (edges.length == 0) {
          continue;
        }

        float linePos = 0;
        EdgeInfo firstEdgeInfo = edges[0];
        for (int i = 0; i < edges.length; i++) {
          EdgeInfo edgeInfo = edges[i];

          float x0 = (float) (edgeInfo.x0 - cameraPos.x), y0 = (float) (edgeInfo.y0 - cameraPos.y), z0 = (float) (edgeInfo.z0 - cameraPos.z);
          float x1 = (float) (edgeInfo.x1 - cameraPos.x), y1 = (float) (edgeInfo.y1 - cameraPos.y), z1 = (float) (edgeInfo.z1 - cameraPos.z);
          float u0 = textureInfo.getTexU0(), v0 = textureInfo.getTexV0();
          float u1 = textureInfo.getTexU1(), v1 = textureInfo.getTexV1();

          if (clipLine) {
            // If stretching mode (texture with size > 1 repeats along line axis), do manual clipping.
            // This is important for some GPUs with limited texture interpolation precision (Tegra 3, for example)
            double[] l0 = clipBuffer1, l1 = clipBuffer2;
            l0[0] = edgeInfo.x0; l0[1] = edgeInfo.y0; l0[2] = edgeInfo.z0;
            l1[0] = edgeInfo.x1; l1[1] = edgeInfo.y1; l1[2] = edgeInfo.z1;
            linePos += calculateLineLength3D(l1[0] - l0[0], l1[1] - l0[1], l1[2] - l0[2]);
            if (clipLines) {
              if (!camera.frustum.clipLine(l0, l1, sx)) {
                continue;
              }
            }
            x0 = (float) (l0[0] - cameraPos.x); y0 = (float) (l0[1] - cameraPos.y); z0 = (float) (l0[2] - cameraPos.z);
            x1 = (float) (l1[0] - cameraPos.x); y1 = (float) (l1[1] - cameraPos.y); z1 = (float) (l1[2] - cameraPos.z);
            v0 = (linePos + calculateLineLength3D(l0[0] - edgeInfo.x0, l0[1] - edgeInfo.y0, l0[2] - edgeInfo.z0)) * sv / sx / 2;
            v1 = (linePos + calculateLineLength3D(l1[0] - edgeInfo.x0, l1[1] - edgeInfo.y0, l1[2] - edgeInfo.z0)) * sv / sx / 2;
            v1 -= (float) Math.floor(v0);
            v0 -= (float) Math.floor(v0);
          }

          float dv = sx;

          float x00 = -dv * edgeInfo.dx_dv + x0;
          float y00 = -dv * edgeInfo.dy_dv + y0;
          float z00 = -dv * edgeInfo.dz_dv + z0;

          float x01 = -dv * edgeInfo.dx_dv + x1;
          float y01 = -dv * edgeInfo.dy_dv + y1;
          float z01 = -dv * edgeInfo.dz_dv + z1;

          float x10 =  dv * edgeInfo.dx_dv + x0;
          float y10 =  dv * edgeInfo.dy_dv + y0;
          float z10 =  dv * edgeInfo.dz_dv + z0;

          float x11 =  dv * edgeInfo.dx_dv + x1;
          float y11 =  dv * edgeInfo.dy_dv + y1;
          float z11 =  dv * edgeInfo.dz_dv + z1;

          float u00 = u0, v00 = v0;
          float u01 = u0, v01 = v1;
          float u10 = u1, v10 = v0;
          float u11 = u1, v11 = v1;

          record.lineVertices.add(x00, y00, z00);
          record.lineTexCoords.add(u00, v00);
          record.lineVertices.add(x10, y10, z10);
          record.lineTexCoords.add(u10, v10);
          record.lineVertices.add(x01, y01, z01);
          record.lineTexCoords.add(u01, v01);

          record.lineVertices.add(x10, y10, z10);
          record.lineTexCoords.add(u10, v10);
          record.lineVertices.add(x11, y11, z11);
          record.lineTexCoords.add(u11, v11);
          record.lineVertices.add(x01, y01, z01);
          record.lineTexCoords.add(u01, v01);

          if (lineStyle.lineJoinMode == LineStyle.NO_LINEJOIN) {
            continue;
          }

          // Find next edge info.
          // Also detect loops, so that loops can be connected.
          EdgeInfo nextEdgeInfo = null;
          if (i + 1 < edges.length) {
            nextEdgeInfo = edges[i + 1];
          } else if (edgeInfo.x1 == firstEdgeInfo.x0 && edgeInfo.y1 == firstEdgeInfo.y0 && edgeInfo.z1 == firstEdgeInfo.z0) {
            nextEdgeInfo = firstEdgeInfo;
          } else {
            continue;
          }

          // Tesselate the area between current and next endpoint.
          // Note: this tesselation is really a hack - it creates T-vertices and does not handle texture coordinate calculation properly.
          // But it works for solid color non-transparent lines.
          EdgeInfo prevEdgeInfo = edgeInfo;
          if (edgeInfo.dx_du * nextEdgeInfo.dx_dv + edgeInfo.dy_du * nextEdgeInfo.dy_dv + edgeInfo.dz_du * nextEdgeInfo.dz_dv < 0) {
            dv = -dv;
            prevEdgeInfo = nextEdgeInfo;
            nextEdgeInfo = edgeInfo;
          }

          float x1Prev = dv * prevEdgeInfo.dx_dv + x1;
          float y1Prev = dv * prevEdgeInfo.dy_dv + y1;
          float z1Prev = dv * prevEdgeInfo.dz_dv + z1;

          if (lineStyle.lineJoinMode == LineStyle.BEVEL_LINEJOIN) {
            float x1Next = dv * nextEdgeInfo.dx_dv + x1;
            float y1Next = dv * nextEdgeInfo.dy_dv + y1;
            float z1Next = dv * nextEdgeInfo.dz_dv + z1;

            record.lineVertices.add(x1Prev, y1Prev, z1Prev);
            record.lineTexCoords.add(u1, v1);
            record.lineVertices.add(x1Next, y1Next, z1Next);
            record.lineTexCoords.add(u1, v1);
            record.lineVertices.add(x1, y1, z1);
            record.lineTexCoords.add((u0 + u1) / 2, v1);

            continue;
          }

          double deltaDot = prevEdgeInfo.dx_dv * nextEdgeInfo.dx_dv + prevEdgeInfo.dy_dv * nextEdgeInfo.dy_dv + prevEdgeInfo.dz_dv * nextEdgeInfo.dz_dv;  
          float deltaDeg = (float) Math.acos(Utils.toRange(deltaDot, -1, 1)) * Const.RAD_TO_DEG;
          int segments = (int) Math.ceil(deltaDeg * textureInfo.width * LINE_ENDPOINT_TESSELATION_FACTOR);
          for (int j = 0; j < segments; j++) {
            float t = (float) (j + 1) / segments;

            float dx_dv = (nextEdgeInfo.dx_dv * t + prevEdgeInfo.dx_dv * (1 - t));
            float dy_dv = (nextEdgeInfo.dy_dv * t + prevEdgeInfo.dy_dv * (1 - t));
            float dz_dv = (nextEdgeInfo.dz_dv * t + prevEdgeInfo.dz_dv * (1 - t));
            float len = (float) Math.sqrt(dx_dv * dx_dv + dy_dv * dy_dv + dz_dv * dz_dv);

            float x1Next = dv * dx_dv / len + x1;
            float y1Next = dv * dy_dv / len + y1;
            float z1Next = dv * dz_dv / len + z1;

            record.lineVertices.add(x1Prev, y1Prev, z1Prev);
            record.lineTexCoords.add(u1, v1);
            record.lineVertices.add(x1Next, y1Next, z1Next);
            record.lineTexCoords.add(u1, v1);
            record.lineVertices.add(x1, y1, z1);
            record.lineTexCoords.add((u0 + u1) / 2, v1);

            x1Prev = x1Next;
            y1Prev = y1Next;
            z1Prev = z1Next;
          }
        }

        if (lineStyle.lineCapMode == LineStyle.BUTT_LINECAP) {
          continue;
        }

        // Draw line cap. First check that line is not closed.
        EdgeInfo lastEdgeInfo = edges[edges.length - 1];
        if (firstEdgeInfo.x0 == lastEdgeInfo.x1 && firstEdgeInfo.y0 == lastEdgeInfo.y1 && firstEdgeInfo.z0 == lastEdgeInfo.z1) {
          continue;
        }

        if (lineStyle.lineCapMode == LineStyle.SQUARE_LINECAP) {
          buildSquareLineCapBuffers(record, firstEdgeInfo, lineStyle, 1, camera);
          buildSquareLineCapBuffers(record, lastEdgeInfo, lineStyle, -1, camera);
        } else { 
          buildRoundLineCapBuffers(record, firstEdgeInfo, lineStyle, 1, camera);
          buildRoundLineCapBuffers(record, lastEdgeInfo, lineStyle, -1, camera);
        }
      }
    }
  }

  private static float calculateLineLength3D(double dx, double dy, double dz) {
    return (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
  }

  private static void buildSquareLineCapBuffers(DrawRecord record, EdgeInfo edgeInfo, LineStyle lineStyle, float sign, CameraState camera) {
    TextureInfo textureInfo = lineStyle.textureInfo;

    float x0 = (float) ((sign > 0 ? edgeInfo.x0 : edgeInfo.x1) - camera.cameraPos.x);
    float y0 = (float) ((sign > 0 ? edgeInfo.y0 : edgeInfo.y1) - camera.cameraPos.y);
    float z0 = (float) ((sign > 0 ? edgeInfo.z0 : edgeInfo.z1) - camera.cameraPos.z);
    float u0 = textureInfo.getTexU0();
    float u1 = textureInfo.getTexU1();
    float v1 = sign > 0 ? textureInfo.getTexV0() : textureInfo.getTexV1();
    float dv = 0.5f * textureInfo.width / camera.zoomPow2;

    float x1 = -sign * edgeInfo.dx_du * dv + x0;
    float y1 = -sign * edgeInfo.dy_du * dv + y0;
    float z1 = -sign * edgeInfo.dz_du * dv + z0;

    float x00 = sign * edgeInfo.dx_dv * dv + x0;
    float y00 = sign * edgeInfo.dy_dv * dv + y0;
    float z00 = sign * edgeInfo.dz_dv * dv + z0;

    float x01 = sign * edgeInfo.dx_dv * dv + x1;
    float y01 = sign * edgeInfo.dy_dv * dv + y1;
    float z01 = sign * edgeInfo.dz_dv * dv + z1;

    float x10 = -sign * edgeInfo.dx_dv * dv + x0;
    float y10 = -sign * edgeInfo.dy_dv * dv + y0;
    float z10 = -sign * edgeInfo.dz_dv * dv + z0;

    float x11 = -sign * edgeInfo.dx_dv * dv + x1;
    float y11 = -sign * edgeInfo.dy_dv * dv + y1;
    float z11 = -sign * edgeInfo.dz_dv * dv + z1;

    float u00 = u0, v00 = v1;
    float u01 = u0, v01 = v1;
    float u10 = u1, v10 = v1;
    float u11 = u1, v11 = v1;

    record.lineVertices.add(x00, y00, z00);
    record.lineTexCoords.add(u00, v00); // HACK: should use linear texgen
    record.lineVertices.add(x10, y10, z10);
    record.lineTexCoords.add(u10, v10);
    record.lineVertices.add(x01, y01, z01);
    record.lineTexCoords.add(u01, v01);

    record.lineVertices.add(x10, y10, z10);
    record.lineTexCoords.add(u10, v10);
    record.lineVertices.add(x11, y11, z11);
    record.lineTexCoords.add(u11, v11);
    record.lineVertices.add(x01, y01, z01);
    record.lineTexCoords.add(u01, v01);
  }

  private static void buildRoundLineCapBuffers(DrawRecord record, EdgeInfo edgeInfo, LineStyle lineStyle, float sign, CameraState camera) {
    TextureInfo textureInfo = lineStyle.textureInfo;

    float x0 = (float) ((sign > 0 ? edgeInfo.x0 : edgeInfo.x1) - camera.cameraPos.x);
    float y0 = (float) ((sign > 0 ? edgeInfo.y0 : edgeInfo.y1) - camera.cameraPos.y);
    float z0 = (float) ((sign > 0 ? edgeInfo.z0 : edgeInfo.z1) - camera.cameraPos.z);
    float u0 = textureInfo.getTexU0();
    float u1 = textureInfo.getTexU1();
    float v1 = sign > 0 ? textureInfo.getTexV0() : textureInfo.getTexV1();
    float dv = 0.5f * lineStyle.textureInfo.width / camera.zoomPow2;

    float x1Prev = -sign * edgeInfo.dx_dv * dv + x0;
    float y1Prev = -sign * edgeInfo.dy_dv * dv + y0;
    float z1Prev = -sign * edgeInfo.dz_dv * dv + z0;

    int segments = (int) Math.ceil(90 * lineStyle.textureInfo.width * LINE_ENDPOINT_TESSELATION_FACTOR);
    for (int j = 0; j < 2 * segments; j++) {
      float s = 1 - (float) (j + 1) / segments;
      float t = (float) (j + 1) / segments;
      if (t > 1) {
        t = 2 - t;
      }
      
      float dx_dv = -sign * (edgeInfo.dx_du * t + edgeInfo.dx_dv * s);
      float dy_dv = -sign * (edgeInfo.dy_du * t + edgeInfo.dy_dv * s);
      float dz_dv = -sign * (edgeInfo.dz_du * t + edgeInfo.dz_dv * s);
      float len = (float) Math.sqrt(dx_dv * dx_dv + dy_dv * dy_dv + dz_dv * dz_dv);
      
      float x1Next = dv * dx_dv / len + x0;
      float y1Next = dv * dy_dv / len + y0;
      float z1Next = dv * dz_dv / len + z0;

      record.lineVertices.add(x1Prev, y1Prev, z1Prev);
      record.lineTexCoords.add(u1, v1); // HACK: should use linear texgen
      record.lineVertices.add(x1Next, y1Next, z1Next);
      record.lineTexCoords.add(u1, v1);
      record.lineVertices.add(x0, y0, z0);
      record.lineTexCoords.add((u0 + u1) / 2, v1);
 
      x1Prev = x1Next;
      y1Prev = y1Next;
      z1Prev = z1Next;
    }
  }

  private void buildPolygonBuffers(DrawRecord record, List<Geometry> elements, PolygonStyle polyStyle, CameraState camera) {
    if (record.polygonOrigin != null && (int) record.polygonZoom == (int) camera.zoom) {
      return;
    }
    Point3D cameraPos = camera.cameraPos;

    record.polygonVertices.clear();
    if (record.polygonTexCoords != null) {
      record.polygonTexCoords.clear();
    }
    record.polygonOrigin = cameraPos;
    record.polygonZoom = camera.zoom;
    for (Geometry element : elements) {
      PolygonInternalState internalState = (PolygonInternalState) element.getInternalState();
      float intZoomPow2 = (float) Math.pow(2, (int) camera.zoom);
      float x0 = (float) (internalState.origin.x - cameraPos.x), y0 = (float) (internalState.origin.y - cameraPos.y), z0 = (float) (internalState.origin.z - cameraPos.z);
      float[] vertices = internalState.vertices;
      float[] uvs = internalState.uvs;
      if (uvs != null) {
        if (record.polygonTexCoords == null) {
          record.polygonTexCoords = new FloatVertexBuffer();
        }
        record.polygonTexCoords.reserve(record.polygonTexCoords.size() + uvs.length);
      }
      int count = vertices.length / 3;
      for (int i = 0; i < count; i++) {
        record.polygonVertices.add(vertices[i * 3 + 0] + x0, vertices[i * 3 + 1] + y0, vertices[i * 3 + 2] + z0);
        if (uvs != null) {
          record.polygonTexCoords.add(uvs[i * 2 + 0] * intZoomPow2, uvs[i * 2 + 1] * intZoomPow2);
        }
      }
    }
  }
  
  private void buildBuffersPicking(DrawRecord record, List<Geometry> elements, Style style, CameraState camera, PickingState picking) {
    Point3D cameraPos = camera.cameraPos;

    int maxVertices = Math.max(Math.max(record.pointVertices.size(), record.lineVertices.size()), record.polygonVertices.size()) / 3;
    record.pickingVertices.clear();
    record.pickingColors.clear();
    record.pickingVertices.reserve(maxVertices * 3);
    record.pickingColors.reserve(maxVertices * 4);
    for (Geometry element : elements) {
      int count = 0;
      if (style instanceof PolygonStyle) {
        PolygonInternalState internalState = (PolygonInternalState) element.getInternalState(); 

        Point3D origin = internalState.origin;
        float x0 = (float) (origin.x - cameraPos.x), y0 = (float) (origin.y - cameraPos.y), z0 = (float) (origin.z - cameraPos.z);
        float[] vertices = internalState.vertices;
        count = vertices.length / 3;
        for (int i = 0; i < count; i++) {
          record.pickingVertices.add(vertices[i * 3 + 0] + x0, vertices[i * 3 + 1] + y0, vertices[i * 3 + 2] + z0);
        }
      }
      if (style instanceof LineStyle) {
        LineStyle lineStyle = (LineStyle) style;
        LineInternalState internalState = (LineInternalState) element.getInternalState(); 

        float sx = 0.5f * lineStyle.pickingWidth / camera.zoomPow2;
        for (LineInfo lineInfo : internalState.lines) {
          for (EdgeInfo edgeInfo : lineInfo.edges) {
            float x0 = (float) (edgeInfo.x0 - cameraPos.x), y0 = (float) (edgeInfo.y0 - cameraPos.y), z0 = (float) (edgeInfo.z0 - cameraPos.z);
            float x1 = (float) (edgeInfo.x1 - cameraPos.x), y1 = (float) (edgeInfo.y1 - cameraPos.y), z1 = (float) (edgeInfo.z1 - cameraPos.z);

            float dv = sx;

            float x00 = -dv * edgeInfo.dx_dv + x0;
            float y00 = -dv * edgeInfo.dy_dv + y0;
            float z00 = -dv * edgeInfo.dz_dv + z0;

            float x01 = -dv * edgeInfo.dx_dv + x1;
            float y01 = -dv * edgeInfo.dy_dv + y1;
            float z01 = -dv * edgeInfo.dz_dv + z1;

            float x10 =  dv * edgeInfo.dx_dv + x0;
            float y10 =  dv * edgeInfo.dy_dv + y0;
            float z10 =  dv * edgeInfo.dz_dv + z0;

            float x11 =  dv * edgeInfo.dx_dv + x1;
            float y11 =  dv * edgeInfo.dy_dv + y1;
            float z11 =  dv * edgeInfo.dz_dv + z1;

            record.pickingVertices.add(x00, y00, z00);
            record.pickingVertices.add(x10, y10, z10);
            record.pickingVertices.add(x01, y01, z01);

            record.pickingVertices.add(x10, y10, z10);
            record.pickingVertices.add(x11, y11, z11);
            record.pickingVertices.add(x01, y01, z01);
          }

          count += 6 * lineInfo.edges.length;
        }
      }
      if (style instanceof PointStyle) {
        PointStyle pointStyle = (PointStyle) style;
        PointInternalState internalState = (PointInternalState) element.getInternalState(); 
        for (PointInfo pointInfo : internalState.points) {
          float du = 0.5f * pointStyle.pickingSize / camera.zoomPow2;
          float dv = 0.5f * pointStyle.pickingSize / camera.zoomPow2;

          float x0 = (float) (pointInfo.x - cameraPos.x), y0 = (float) (pointInfo.y - cameraPos.y), z0 = (float) (pointInfo.z - cameraPos.z);

          float x00 = -du * pointInfo.dx_du - dv * pointInfo.dx_dv + x0;
          float y00 = -du * pointInfo.dy_du - dv * pointInfo.dy_dv + y0;
          float z00 = -du * pointInfo.dz_du - dv * pointInfo.dz_dv + z0;

          float x01 = -du * pointInfo.dx_du + dv * pointInfo.dx_dv + x0;
          float y01 = -du * pointInfo.dy_du + dv * pointInfo.dy_dv + y0;
          float z01 = -du * pointInfo.dz_du + dv * pointInfo.dz_dv + z0;

          float x10 =  du * pointInfo.dx_du - dv * pointInfo.dx_dv + x0;
          float y10 =  du * pointInfo.dy_du - dv * pointInfo.dy_dv + y0;
          float z10 =  du * pointInfo.dz_du - dv * pointInfo.dz_dv + z0;
        
          float x11 =  du * pointInfo.dx_du + dv * pointInfo.dx_dv + x0;
          float y11 =  du * pointInfo.dy_du + dv * pointInfo.dy_dv + y0;
          float z11 =  du * pointInfo.dz_du + dv * pointInfo.dz_dv + z0;
        
          record.pickingVertices.add(x00, y00, z00);
          record.pickingVertices.add(x10, y10, z10);
          record.pickingVertices.add(x01, y01, z01);

          record.pickingVertices.add(x10, y10, z10);
          record.pickingVertices.add(x11, y11, z11);
          record.pickingVertices.add(x01, y01, z01);
        }

        count = 6 * internalState.points.length;
      }

      int index = picking.bindElement(element);
      ColorUtils.encodeIntAsColor(index, pickingColor);
      for (int i = 0; i < count; i++) {
        record.pickingColors.add(pickingColor[0], pickingColor[1], pickingColor[2], pickingColor[3]);
      }
    }
  }
  
}
