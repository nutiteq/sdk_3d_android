package com.nutiteq.renderers.layers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.cache.TextureMemoryCache;
import com.nutiteq.components.CameraState;
import com.nutiteq.components.Color;
import com.nutiteq.components.Components;
import com.nutiteq.components.MutableVector3D;
import com.nutiteq.components.Options;
import com.nutiteq.components.Point3D;
import com.nutiteq.rasterlayers.RasterLayer;
import com.nutiteq.renderers.components.MapTileDrawData;
import com.nutiteq.renderers.components.MapTileProxy;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.FloatVertexBuffer;

/**
 * Renderer for raster layers.
 */
public class RasterLayerRenderer implements LayerRenderer {
  private static final float TILE_FADE_SPEED = 0.1f;

  private final RasterLayer layer;
  private final RenderProjection renderProjection;
  private final TextureMemoryCache textureMemoryCache;
  private final Options options;
  private float tileFadeAmount;
  private MutableVector3D focusPointNormal = new MutableVector3D();
  private List<MapTileDrawData> newVisibleTiles;
  private List<MapTileDrawData> oldVisibleTiles;
  private long visibleTilesTimeStamp;
  private Set<MapTileProxy> duplicateTileSet = new HashSet<MapTileProxy>();
  private float[] tileVertices = new float[0];
  private FloatVertexBuffer tileVertexBuf = new FloatVertexBuffer();
  private float[] tileColors = new float[0];
  private FloatVertexBuffer tileColorBuf = new FloatVertexBuffer();

  public RasterLayerRenderer(RasterLayer layer, RenderProjection renderProjection, TextureMemoryCache textureMemoryCache, Options options) {
    this.layer = layer;
    this.renderProjection = renderProjection;
    this.textureMemoryCache = textureMemoryCache;
    this.options = options;
  }

  @Override
  public void draw(GL10 gl, CameraState camera, Pass pass) {
    if (pass != Pass.BASE) {
      return;
    }
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
    gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glEnable(GL10.GL_TEXTURE_2D);
    Color tileColor = layer.getTileColor();
    
    // Get world-space normal at focus point
    renderProjection.setNormal(camera.focusPoint.x, camera.focusPoint.y, camera.focusPoint.z, focusPointNormal);

    // Draw old tiles
    if (tileFadeAmount < 1) {
      if (oldVisibleTiles != null) {
        for (MapTileDrawData drawData : oldVisibleTiles) {
          if (!duplicateTileSet.contains(drawData.proxy)) {
            drawTile(gl, drawData, camera, tileColor, 1);
          }
        }
      }
    }

    // Draw new tiles
    if (newVisibleTiles != null) {
      for (MapTileDrawData drawData : newVisibleTiles) {
        float tileAlpha = 1;
        if (!duplicateTileSet.contains(drawData.proxy)) {
          tileAlpha = tileFadeAmount;
        }
        drawTile(gl, drawData, camera, tileColor, tileAlpha);
      }
    }

    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
  }
  
  @Override
  public void synchronize(GL10 gl) {
    long visibleTilesTimeStamp = layer.getVisibleTilesTimeStamp();
    if (visibleTilesTimeStamp != this.visibleTilesTimeStamp) {
      List<MapTileDrawData> visibleTiles = layer.getVisibleTiles();
      tileFadeAmount = newVisibleTiles != null ? 0 : 1;
      oldVisibleTiles = newVisibleTiles;
      newVisibleTiles = visibleTiles;
      duplicateTileSet.clear();
      if (newVisibleTiles != null && oldVisibleTiles != null) {
        HashMap<MapTileProxy, MapTileDrawData> oldDrawDataMap = new HashMap<MapTileProxy, MapTileDrawData>();
        for (MapTileDrawData oldDrawData : oldVisibleTiles) {
          oldDrawDataMap.put(oldDrawData.proxy, oldDrawData);
        }

        for (MapTileDrawData drawData : newVisibleTiles) {
          if (oldDrawDataMap.containsKey(drawData.proxy)) {
            duplicateTileSet.add(drawData.proxy);
          }
        }
      }
      
      this.visibleTilesTimeStamp = visibleTilesTimeStamp;
    }
    
    if (tileFadeAmount < 1) {
      tileFadeAmount += TILE_FADE_SPEED;
    }
    if (tileFadeAmount > 1) {
      tileFadeAmount = 1;
    }
    if (!(options.isTileFading() && layer.isTileFading())) {
      tileFadeAmount = 1;
    }
    if (layer.getTileColor().a < 1) { // tile fading is not support for partially transparent layers
      tileFadeAmount = 1;
    }
    
    Components components = layer.getComponents(); // tile fading is supported only for base layers
    if (components != null) {
      if (components.layers.getBaseLayer() != layer) {
        tileFadeAmount = 1;
      }
    }
  }
  
  @Override
  public boolean isSynchronized() {
    return tileFadeAmount >= 1;
  }
  
  @Override
  public void dispose(GL10 gl) {
  }

  private void drawTile(GL10 gl, MapTileDrawData drawData, CameraState camera, Color tileColor, float tileAlpha) {
    int texture = textureMemoryCache.getWithoutMod(drawData.proxy.fullTileId);
    if (texture == 0) {
      return;
    }
    
    // Test if tile is visible
    if (!drawData.proxy.mapTile.bounds.testIntersection(camera.frustum, camera.cameraPos)) {
      return;
    }
    
    // Convert vertices from world coordinate system to camera local coordinate system
    Point3D cameraPos = camera.cameraPos;
    double[] tileVertsWorld = drawData.tileVerts;
    if (tileVertices.length < tileVertsWorld.length) {
      tileVertices = new float[tileVertsWorld.length];
    }
    float[] tileVertices = this.tileVertices;
    double x0 = cameraPos.x, y0 = cameraPos.y, z0 = cameraPos.z;
    for (int i = 0; i < tileVertsWorld.length; i += 3) {
      tileVertices[i + 0] = (float) (tileVertsWorld[i + 0] - x0);
      tileVertices[i + 1] = (float) (tileVertsWorld[i + 1] - y0);
      tileVertices[i + 2] = (float) (tileVertsWorld[i + 2] - z0);
    }
    tileVertexBuf.attach(tileVertices, tileVertsWorld.length);
    
    // Calculate lighting
    float ca = tileColor.a * tileAlpha;
    float cr = tileColor.r * ca, cg = tileColor.g * ca, cb = tileColor.b * ca;
    float[] tileNormals = drawData.tileNormals; 
    if (tileColors.length < tileNormals.length * 4 / 3) {
      tileColors = new float[tileNormals.length * 4 / 3];
    }
    float[] tileColors = this.tileColors;
    float nx = (float) focusPointNormal.x, ny = (float) focusPointNormal.y, nz = (float) focusPointNormal.z;
    for (int i = 0, j = 0; i < tileNormals.length; i += 3, j += 4) {
      float dot = tileNormals[i + 0] * nx + tileNormals[i + 1] * ny + tileNormals[i + 2] * nz;
      float intensity = Const.AMBIENT_FACTOR + (1 - Const.AMBIENT_FACTOR) * dot;
      tileColors[j + 0] = intensity * cr;
      tileColors[j + 1] = intensity * cg;
      tileColors[j + 2] = intensity * cb;
      tileColors[j + 3] = ca;
    }
    tileColorBuf.attach(tileColors, tileNormals.length * 4 / 3);
    
    // Draw vertex buffers
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, tileVertexBuf.build(0, tileVertexBuf.size()));
    gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, drawData.tileTexCoordBuf);
    gl.glColorPointer(4, GL10.GL_FLOAT, 0, tileColorBuf.build(0, tileColorBuf.size()));

    gl.glBindTexture(GL10.GL_TEXTURE_2D, texture);
    gl.glDrawElements(GL10.GL_TRIANGLES, drawData.indexBuf.capacity(), GL10.GL_UNSIGNED_SHORT, drawData.indexBuf);
  }
  
}
