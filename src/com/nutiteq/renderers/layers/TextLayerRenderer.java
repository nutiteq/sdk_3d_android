package com.nutiteq.renderers.layers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.TextureInfo;
import com.nutiteq.geometry.Text;
import com.nutiteq.geometry.Text.TextInfo;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.renderers.components.PickingState;
import com.nutiteq.renderers.utils.BillBoardVertexBuilder;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.Style;
import com.nutiteq.style.TextStyle;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.utils.GLUtils;
import com.nutiteq.utils.Matrix;
import com.nutiteq.vectorlayers.TextLayer;

/**
 * Renderer for text layers.
 */
public class TextLayerRenderer extends BillBoardLayerRenderer {
  private static final int NEW_TEXTURES_PER_FRAME = 4;
  private static final int TEXT_FADEIN_DELAY = 400; // in ms
  private static final int TEXT_FADEOUT_DELAY = 400; // in ms
  private static final List<Text> EMPTY_TEXT_LIST = new ArrayList<Text>();
  
  private final TextLayer layer;
  private final BillBoardVertexBuilder vertexBuilder;
  private final FloatVertexBuffer elementVertexBuf = new FloatVertexBuffer();
  private final FloatVertexBuffer elementTexCoordBuf = new FloatVertexBuffer();

  private Map<Text, TextRecord> textMap = new HashMap<Text, TextRecord>();
  private boolean layerSynchronized = false;

  /**
   * Grouped element record for drawing. Elements are grouped using style.
   */
  private static class TextRecord {
    final TextInfo textInfo;
    final int texture;
    final TextureInfo textureInfo;
    float alpha;
    boolean visible;
    long lastTimestamp;
    
    TextRecord(TextInfo textInfo, int texture, TextureInfo textureInfo) {
      this.textInfo = textInfo;
      this.texture = texture;
      this.textureInfo = textureInfo;
      this.alpha = 0;
      this.visible = false;
      this.lastTimestamp = System.currentTimeMillis();
    }
  }

  public TextLayerRenderer(TextLayer layer, RenderProjection renderProjection) {
    super(renderProjection);
    this.layer = layer;
    this.vertexBuilder = new BillBoardVertexBuilder(renderProjection);
  }
  
  @Override
  public void synchronize(GL10 gl) {
    List<Text> visibleElements = layer.isVisible() ? layer.getVisibleElements() : null;
    if (visibleElements == null) {
      visibleElements = EMPTY_TEXT_LIST;
    }

    long currentTimestamp = System.currentTimeMillis();
    
    for (Iterator<Map.Entry<Text, TextRecord>> it = textMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<Text, TextRecord> entry = it.next();
      TextRecord textRecord = entry.getValue();
      textRecord.visible = false;
    }

    layerSynchronized = true;
    int textureCreateCount = 0;
    boolean textFading = layer.isTextFading();
    for (Text element : visibleElements) {
      Style style = element.getInternalState().activeStyle;
      if (style == null) {
        continue;
      }
      if (!element.getInternalState().visible) {
        continue;
      }
      
      TextInfo textInfo = element.getInternalState().textInfo;
      if (textInfo == null) {
        continue;
      }
      
      TextRecord textRecord = textMap.get(element);
      float alpha = 0;
      long lastTimestamp = currentTimestamp;
      if (textRecord != null) {
        alpha = textRecord.alpha;
        lastTimestamp = textRecord.lastTimestamp;
        if (!textInfo.equals(textRecord.textInfo)) {
          if (textureCreateCount < NEW_TEXTURES_PER_FRAME) {
            GLUtils.deleteTexture(gl, textRecord.texture);
            textRecord = null;
          }
        }
      }
      if (textRecord == null) {
        if (textureCreateCount >= NEW_TEXTURES_PER_FRAME) {
          layerSynchronized = false;
          continue;
        }
        TextureInfo textureInfo = textInfo.createTextureInfo();
        int texture = GLUtils.buildMipmaps(gl, textureInfo.bitmap);
        gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
        gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
        textRecord = new TextRecord(textInfo, texture, textureInfo);
        textMap.put(element, textRecord);
        textureInfo.bitmap.recycle();
        textureCreateCount++;
      }
      textRecord.visible = true;
      textRecord.alpha = alpha;
      textRecord.lastTimestamp = lastTimestamp;
    }

    for (Iterator<Map.Entry<Text, TextRecord>> it = textMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<Text, TextRecord> entry = it.next();
      TextRecord textRecord = entry.getValue();
      float delta = currentTimestamp - textRecord.lastTimestamp;
      textRecord.lastTimestamp = currentTimestamp;

      if (textRecord.visible) {
        if (textFading) {
          textRecord.alpha += delta / TEXT_FADEIN_DELAY;
          if (textRecord.alpha < 1) {
            layerSynchronized = false;
          } else {
            textRecord.alpha = 1;
          }
        } else {
          textRecord.alpha = 1;
        }
        continue;
      }

      if (textFading) {
        textRecord.alpha -= delta / TEXT_FADEOUT_DELAY;
        if (textRecord.alpha > 0) {
          layerSynchronized = false;
        } else {
          textRecord.alpha = 0;
        }
      } else {
        textRecord.alpha = 0;
      }
      
      if (textRecord.alpha == 0) {
        GLUtils.deleteTexture(gl, textRecord.texture);
        it.remove();
      }
    }
  }
  
  @Override
  public boolean isSynchronized() {
    return layerSynchronized;
  }
  
  @Override
  public void dispose(GL10 gl) {
    for (TextRecord textRecord : textMap.values()) {
      GLUtils.deleteTexture(gl, textRecord.texture);
    }
    textMap.clear();
  }

  @Override
  public void draw(GL10 gl, CameraState camera, Pass pass) {
    if ((pass == Pass.OVERLAY) != layer.isZOrdered()) {
      return;
    }
    if (textMap.isEmpty()) {
      return;
    }
    vertexBuilder.setCamera(camera);

    gl.glColor4f(1, 1, 1, 1);
    gl.glEnable(GL10.GL_TEXTURE_2D);
    gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    
    gl.glPushMatrix();
    gl.glLoadIdentity();

    for (Iterator<Map.Entry<Text, TextRecord>> it = textMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<Text, TextRecord> entry = it.next();
      Text element = entry.getKey();
      TextRecord textRecord = entry.getValue();
      TextInfo textInfo = textRecord.textInfo;
      TextStyle style = textInfo.textStyle;
      if (style == null) {
        continue;
      }
      Text.TextInternalState internalState = element.getInternalState();

      elementVertexBuf.clear();
      elementTexCoordBuf.clear();
      if (!vertexBuilder.generateElementVertices(element, style, internalState.pos, internalState.localFrameMatrix, internalState.rotationDeg, elementVertexBuf, textRecord.textureInfo, elementTexCoordBuf)) {
        continue;
      }

      gl.glBindTexture(GL10.GL_TEXTURE_2D, textRecord.texture);
      gl.glVertexPointer(3, GL10.GL_FLOAT, 0, elementVertexBuf.build(0, elementVertexBuf.size()));
      gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
      gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, elementTexCoordBuf.build(0, elementTexCoordBuf.size()));
      gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
      gl.glColor4f(textRecord.alpha, textRecord.alpha, textRecord.alpha, textRecord.alpha);
      gl.glDrawArrays(GL10.GL_TRIANGLES, 0, elementVertexBuf.size() / 3);
    }
    
    gl.glPopMatrix();
    
    gl.glColor4f(1, 1, 1, 1);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
  }

  @Override
  public Point3D calculateElementLabelPos(VectorElement element, CameraState camera, Point3D clickPos) {
    if (element instanceof Text) {
      Text text = (Text) element;
      TextStyle textStyle = (TextStyle) text.getInternalState().activeStyle;
      Text.TextInfo textInfo = text.getInternalState().textInfo;

      double[] elementMVMatrix = new double[16];
      vertexBuilder.setCamera(camera);
      if (!vertexBuilder.calculateElementMVMatrix(text, textStyle, text.getInternalState().pos, text.getInternalState().localFrameMatrix, 0, textInfo.textureWidth, textInfo.textureHeight, elementMVMatrix)) {
        return null;
      }

      double[] cameraInvMVMatrix = new double[16];
      Matrix.invertM(cameraInvMVMatrix, 0, camera.modelviewMatrix, 0);
      
      double[] vertex = new double[] { -textStyle.labelAnchorX * 0.5, -textStyle.labelAnchorY * 0.5, 0, 1, 0, 0, 0, 0 };
      Matrix.multiplyMV(vertex, 4, elementMVMatrix, 0, vertex, 0);
      Matrix.multiplyMV(vertex, 0, cameraInvMVMatrix, 0, vertex, 4);
      return new Point3D(vertex[0], vertex[1], vertex[2]);
    }
    return clickPos;
  }

  @Override
  public void drawPicking(GL10 gl, CameraState camera, PickingState picking, Pass pass) {
    if ((pass == Pass.OVERLAY) != layer.isZOrdered()) {
      return;
    }

    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

    gl.glPushMatrix();
    gl.glLoadIdentity();

    for (Iterator<Map.Entry<Text, TextRecord>> it = textMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<Text, TextRecord> entry = it.next();
      Text element = entry.getKey();
      TextRecord textRecord = entry.getValue();
      TextInfo textInfo = textRecord.textInfo;
      TextStyle style = textInfo.textStyle;
      if (style == null) {
        continue;
      }
      if (!style.pickingMode) {
        continue;
      }
      Text.TextInternalState internalState = element.getInternalState();

      elementVertexBuf.clear();
      elementTexCoordBuf.clear();
      if (!vertexBuilder.generateElementVertices(element, style, internalState.pos, internalState.localFrameMatrix, internalState.rotationDeg, elementVertexBuf, textRecord.textureInfo, elementTexCoordBuf)) {
        continue;
      }

      gl.glVertexPointer(3, GL10.GL_FLOAT, 0, elementVertexBuf.build(0, elementVertexBuf.size()));
      gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
      picking.bindElement(gl, element);
      gl.glDrawArrays(GL10.GL_TRIANGLES, 0, elementVertexBuf.size() / 3);
    }
    
    gl.glPopMatrix();
  }
}
