package com.nutiteq.renderers.layers;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Point3D;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.renderers.components.PickingState;

/**
 * Vector layer renderer interface.
 */
public interface VectorLayerRenderer extends LayerRenderer {
 
  Point3D calculateElementLabelPos(VectorElement element, CameraState camera, Point3D clickPos);
  
  void drawPicking(GL10 gl, CameraState camera, PickingState picking, Pass pass);
}
