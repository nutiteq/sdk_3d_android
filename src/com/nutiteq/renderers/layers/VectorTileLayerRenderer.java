package com.nutiteq.renderers.layers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.cache.TextureBitmapCache;
import com.nutiteq.cache.TextureInfoCache;
import com.nutiteq.components.CameraState;
import com.nutiteq.components.Point3D;
import com.nutiteq.log.Log;
import com.nutiteq.renderers.components.MapTileDrawData;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.vectorlayers.BillBoardLayer;
import com.nutiteq.vectorlayers.GeometryLayer;
import com.nutiteq.vectorlayers.MarkerLayer;
import com.nutiteq.vectorlayers.TextLayer;
import com.nutiteq.vectorlayers.VectorTileLayer;
import com.nutiteq.vectortile.VectorTile;

/**
 * Renderer for vector tiles
 */
public class VectorTileLayerRenderer implements LayerRenderer {
  private static final List<MapTileDrawData> EMPTY_DRAWDATA_LIST = new ArrayList<MapTileDrawData>();

  private final VectorTileLayer layer;
  private final RenderProjection renderProjection;
  private final TextureInfoCache styleCache;
  private final TextureBitmapCache bitmapCache;

  private List<MapTileDrawData> visibleTiles = null;
  private List<TileRecord> visibleTileRecords = new ArrayList<TileRecord>();
  private Map<MapTileDrawData, TileRecord> tileMap = new HashMap<MapTileDrawData, TileRecord>();
  private Map<MapTileDrawData, TileRecord> oldTileMap = new HashMap<MapTileDrawData, TileRecord>();
  private List<BillBoardLayer<?>> billBoardLayers = new ArrayList<BillBoardLayer<?>>();
  private List<BillBoardLayerRenderer> billBoardLayerRenderers = new ArrayList<BillBoardLayerRenderer>();
  
  private FloatVertexBuffer tileVertices = new FloatVertexBuffer();

  private static class TileRecord {
    final List<GeometryLayerRenderer> geometryLayerRenderers;
    final List<BillBoardLayerRenderer> billBoardLayerRenderers;
    
    TileRecord(VectorTile tile, List<GeometryLayerRenderer> geometryLayerRenderers, List<BillBoardLayerRenderer> billBoardLayerRenderers) {
      this.geometryLayerRenderers = geometryLayerRenderers;
      this.billBoardLayerRenderers = billBoardLayerRenderers;
    }
  }

  public VectorTileLayerRenderer(VectorTileLayer layer, RenderProjection renderProjection, TextureInfoCache styleCache, TextureBitmapCache bitmapCache) {
    this.layer = layer;
    this.renderProjection = renderProjection;
    this.styleCache = styleCache;
    this.bitmapCache = bitmapCache;
  }
  
  @Override
  public boolean isSynchronized() {
    for (BillBoardLayerRenderer renderer : billBoardLayerRenderers) {
      if (!renderer.isSynchronized()) {
        return false;
      }
    }

    for (Iterator<Map.Entry<MapTileDrawData, TileRecord>> it = tileMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<MapTileDrawData, TileRecord> entry = it.next();
      TileRecord record = entry.getValue();
      for (GeometryLayerRenderer renderer : record.geometryLayerRenderers) {
        if (!renderer.isSynchronized()) {
          return false;
        }
      }
      for (BillBoardLayerRenderer renderer : record.billBoardLayerRenderers) {
        if (!renderer.isSynchronized()) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  public void synchronize(GL10 gl) {
    Map<MapTileDrawData, TileRecord> newTileMap = oldTileMap;
    oldTileMap = tileMap;
    tileMap = newTileMap;

    List<BillBoardLayer<?>> newBillBoardLayers = layer.getBillBoardLayers();
    if (newBillBoardLayers != billBoardLayers) {
      for (BillBoardLayerRenderer renderer : billBoardLayerRenderers) {
        renderer.dispose(gl);
      }
      billBoardLayerRenderers.clear();
      for (BillBoardLayer<?> billBoardlayer : newBillBoardLayers) {
        BillBoardLayerRenderer renderer = null;
        if (billBoardlayer instanceof MarkerLayer) {
          renderer = new MarkerLayerRenderer((MarkerLayer) billBoardlayer, renderProjection, bitmapCache);
        } else if (billBoardlayer instanceof TextLayer) {
          renderer = new TextLayerRenderer((TextLayer) billBoardlayer, renderProjection);
        }
        if (renderer != null) {
          billBoardLayerRenderers.add(renderer);
        } else {
          Log.error("VectorTileLayerRenderer: unknown billboard layer type");
        }
      }
      billBoardLayers = newBillBoardLayers;
    }

    for (BillBoardLayerRenderer renderer : billBoardLayerRenderers) {
      renderer.synchronize(gl);
    }

    visibleTiles = layer.getVisibleTiles();
    if (visibleTiles == null) {
      visibleTiles = EMPTY_DRAWDATA_LIST;
    }
    for (MapTileDrawData drawData : visibleTiles) {
      VectorTile tile = layer.getVisibleVectorTile(drawData.proxy.mapTile);
      if (tile == null) {
        continue;
      }

      TileRecord record = oldTileMap.get(drawData);
      if (record == null) {
        List<GeometryLayerRenderer> geometryLayerRenderers = new ArrayList<GeometryLayerRenderer>();
        for (GeometryLayer geometryLayer : tile.getGeometryLayers()) {
          GeometryLayerRenderer renderer = new GeometryLayerRenderer(geometryLayer, renderProjection, styleCache, false);
          geometryLayerRenderers.add(renderer);
        }
        List<BillBoardLayerRenderer> billBoardLayerRenderers = new ArrayList<BillBoardLayerRenderer>();
        record = new TileRecord(tile, geometryLayerRenderers, billBoardLayerRenderers);
      }
      newTileMap.put(drawData, record);
    }

    for (Iterator<Map.Entry<MapTileDrawData, TileRecord>> it = oldTileMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<MapTileDrawData, TileRecord> entry = it.next();
      MapTileDrawData drawData = entry.getKey();
      TileRecord record = entry.getValue();
      if (!newTileMap.containsKey(drawData)) {
        for (GeometryLayerRenderer renderer : record.geometryLayerRenderers) {
          renderer.dispose(gl);
        }
        for (BillBoardLayerRenderer renderer : record.billBoardLayerRenderers) {
          renderer.dispose(gl);
        }
      }
    }

    for (Iterator<Map.Entry<MapTileDrawData, TileRecord>> it = tileMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<MapTileDrawData, TileRecord> entry = it.next();
      TileRecord record = entry.getValue();
      for (GeometryLayerRenderer renderer : record.geometryLayerRenderers) {
        renderer.synchronize(gl);
      }
      for (BillBoardLayerRenderer renderer : record.billBoardLayerRenderers) {
        renderer.synchronize(gl);
      }
    }

    oldTileMap.clear();
  }

  @Override
  public void draw(GL10 gl, CameraState camera, Pass pass) {
    if (pass == Pass.BASE) {
      gl.glEnable(GL10.GL_STENCIL_TEST);
    }

    visibleTileRecords.clear();
    for (MapTileDrawData drawData : visibleTiles) {
      TileRecord record = tileMap.get(drawData);
      if (record == null) {
        continue;
      }
      
      if (!drawData.proxy.mapTile.bounds.testIntersection(camera.frustum, camera.cameraPos)) {
        continue;
      }

      visibleTileRecords.add(record);

      if (pass == Pass.BASE) {
        gl.glClearStencil(255);
        gl.glClear(GL10.GL_STENCIL_BUFFER_BIT);

        gl.glColorMask(false, false, false, false);
        gl.glStencilFunc(GL10.GL_ALWAYS, 0, 0);
        gl.glStencilOp(GL10.GL_REPLACE, GL10.GL_REPLACE, GL10.GL_REPLACE);
        
        drawTileMask(gl, drawData, camera);
 
        gl.glColorMask(true, true, true, true);
      }
      
      int stencilRef = 0; // assume less than 255 geometry layers
      for (GeometryLayerRenderer renderer : record.geometryLayerRenderers) {
        ++stencilRef;
        gl.glStencilFunc(GL10.GL_GREATER, stencilRef, 255);
        gl.glStencilOp(GL10.GL_KEEP, GL10.GL_KEEP, GL10.GL_REPLACE);
        renderer.draw(gl, camera, pass);
      }
    }
    
    if (pass == Pass.BASE) {
      gl.glDisable(GL10.GL_STENCIL_TEST);
    }

    for (TileRecord record : visibleTileRecords) {
      for (BillBoardLayerRenderer renderer : record.billBoardLayerRenderers) {
        renderer.draw(gl, camera, pass);
      }
    }

    for (BillBoardLayerRenderer renderer : billBoardLayerRenderers) {
      renderer.draw(gl, camera, pass);
    }
  }

  @Override
  public void dispose(GL10 gl) {
    for (Iterator<Map.Entry<MapTileDrawData, TileRecord>> it = tileMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<MapTileDrawData, TileRecord> entry = it.next();
      TileRecord record = entry.getValue();
      for (GeometryLayerRenderer renderer : record.geometryLayerRenderers) {
        renderer.dispose(gl);
      }
      for (BillBoardLayerRenderer renderer : record.billBoardLayerRenderers) {
        renderer.dispose(gl);
      }
    }
    tileMap.clear();
    
    for (BillBoardLayerRenderer renderer : billBoardLayerRenderers) {
      renderer.dispose(gl);
    }
    billBoardLayerRenderers.clear();
  }

  private void drawTileMask(GL10 gl, MapTileDrawData drawData, CameraState camera) {
    // Convert vertices from world coordinate system to camera local coordinate system
    tileVertices.clear();
    double[] tileVertsWorld = drawData.tileVerts;
    Point3D cameraPos = camera.cameraPos;
    for (int i = 0; i < tileVertsWorld.length; i += 3) {
      float x = (float) (tileVertsWorld[i + 0] - cameraPos.x), y = (float) (tileVertsWorld[i + 1] - cameraPos.y), z = (float) (tileVertsWorld[i + 2] - cameraPos.z);
      tileVertices.add(x, y, z);
    }
    
    // Draw vertex buffers
    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, tileVertices.build(0, tileVertices.size()));
    gl.glDrawElements(GL10.GL_TRIANGLES, drawData.indexBuf.capacity(), GL10.GL_UNSIGNED_SHORT, drawData.indexBuf);  
    gl.glEnable(GL10.GL_TEXTURE_2D);
  }
}
