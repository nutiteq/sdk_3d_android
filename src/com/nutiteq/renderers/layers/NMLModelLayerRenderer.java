package com.nutiteq.renderers.layers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Point3D;
import com.nutiteq.geometry.NMLModel;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.nmlpackage.GLModel;
import com.nutiteq.nmlpackage.GLSubmesh;
import com.nutiteq.nmlpackage.GLTexture;
import com.nutiteq.renderers.components.PickingState;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.ColorUtils;
import com.nutiteq.utils.LongHashMap;
import com.nutiteq.utils.Matrix;
import com.nutiteq.vectorlayers.NMLModelLayer;

/**
 * Renderer for NML 3D models.
 */
public class NMLModelLayerRenderer implements VectorLayerRenderer {
  private static final int TEXTURE_UPDATE_LIMIT = 1024 * 1024 / 2; // one 1024x1024 texture per frame at most 
  private static final int TEXTURE_UPDATE_COST = 512 * 512 / 2; // additional constant texture update cost (in pixels)

  private static class ModelRecord {
    final NMLModel model;
    boolean used;
    boolean synched;

    ModelRecord(NMLModel model) {
      this.model = model;
      this.used = false;
      this.synched = false;
    }
  }
  
  private static class TextureRecord {
    final GLTexture glTexture;
    boolean used;
    boolean synched;

    TextureRecord(GLTexture texture) {
      this.glTexture = texture;
      this.used = false;
      this.synched = false;
    }
  }

  private final NMLModelLayer layer;
  private final LongHashMap<ModelRecord> modelMap;
  private final LongHashMap<TextureRecord> textureMap;
  private int[] alphaTestFunc = new int[1];
  private float[] alphaTestRef = new float[1];
  private double[] localFrameMatrix = new double[16];
  private float[] glLocalFrameMatrix = new float[16];

  private void markUsed(ModelRecord record) {
    Map<String, NMLModel.Texture> modelTextureMap = record.model.getInternalState().textureMap;
    for (NMLModel.Texture texture : modelTextureMap.values()) {
      TextureRecord textureRecord = textureMap.get(texture.id);
      if (textureRecord != null) {
        textureRecord.used = true;
      }
    }
    record.used = true;
  }
  
  public NMLModelLayerRenderer(NMLModelLayer layer, RenderProjection renderProjection) {
    this.layer = layer;
    this.modelMap = new LongHashMap<ModelRecord>();
    this.textureMap = new LongHashMap<TextureRecord>();
  }
  
  @Override
  public void synchronize(GL10 gl) {
    // Mark all existing models and textures as unused
    for (Iterator<LongHashMap.Entry<ModelRecord>> it = modelMap.entrySetIterator(); it.hasNext(); ) {  
      ModelRecord record = it.next().getValue();
      record.used = false;
    }

    for (Iterator<LongHashMap.Entry<TextureRecord>> it = textureMap.entrySetIterator(); it.hasNext(); ) {  
      TextureRecord record = it.next().getValue();
      record.used = false;
    }
    
    // Sync new models/textures, mark models/textures as used
    List<NMLModel> visibleModels = layer.isVisible() ? layer.getVisibleElements() : null;
    if (visibleModels == null) {
      visibleModels = new ArrayList<NMLModel>();
    }
    for (NMLModel model : visibleModels) {
      ModelRecord modelRecord = modelMap.get(model.getId());
      if (modelRecord == null) {
        modelRecord = new ModelRecord(model);
        modelMap.put(model.getId(), modelRecord);
      }
      GLModel glModel = modelRecord.model.getInternalState().glModel;
      Map<String, NMLModel.Texture> modelTextureMap = modelRecord.model.getInternalState().textureMap;
      for (Map.Entry<String, NMLModel.Texture> entry : modelTextureMap.entrySet()) {
        String textureId = entry.getKey();
        NMLModel.Texture texture = entry.getValue();
        TextureRecord textureRecord = textureMap.get(texture.id);
        if (textureRecord == null) {
          GLTexture glTexture = new GLTexture(texture.nmlTexture);
          textureRecord = new TextureRecord(glTexture);
          textureMap.put(texture.id, textureRecord);
        }
        glModel.replaceTexture(textureId, textureRecord.glTexture);
        textureRecord.used = true;
        if (!textureRecord.synched) {
          modelRecord.synched = false;
        }
      }
      modelRecord.used = true;
    }
    
    // Synchronize textures/models
    int limit = TEXTURE_UPDATE_LIMIT;
    for (Iterator<LongHashMap.Entry<TextureRecord>> it = textureMap.entrySetIterator(); it.hasNext(); ) {
      TextureRecord record = it.next().getValue();
      if (!record.used || record.synched) {
        continue;
      }
      GLTexture glTexture = record.glTexture;
      glTexture.build(gl);
      record.synched = true;
      limit -= glTexture.getTotalTextureSize() + TEXTURE_UPDATE_COST;
      if (limit < 0) {
        break;
      }
    }

    for (Iterator<LongHashMap.Entry<ModelRecord>> it = modelMap.entrySetIterator(); it.hasNext(); ) {  
      ModelRecord modelRecord = it.next().getValue();
      if (!modelRecord.used || modelRecord.synched) {
        continue;
      }
      boolean synched = true;
      Map<String, NMLModel.Texture> modelTextureMap = modelRecord.model.getInternalState().textureMap;
      for (Map.Entry<String, NMLModel.Texture> entry : modelTextureMap.entrySet()) {
        TextureRecord textureRecord = textureMap.get(entry.getValue().id);
        if (textureRecord == null || !textureRecord.synched) {
          synched = false;
          break;
        }
      }
      modelRecord.synched = synched;
    }
    
    // If a model is used but not synched, try to find its first parent that is synched and mark it as used.  
    for (Iterator<LongHashMap.Entry<ModelRecord>> it = modelMap.entrySetIterator(); it.hasNext(); ) {  
      ModelRecord record = it.next().getValue();
      if (record.used && !record.synched) {
        long[] parentIds = record.model.getParentIds();
        for (int i = 0; i < parentIds.length; i++) {
          ModelRecord parent = modelMap.get(parentIds[i]);
          if (parent == null) {
            continue;
          }
          if (parent.synched) {
            markUsed(parent);
            break;
          }
        }
      }
    }

    // If a model is not used but synched then keep it if it has a parent that is used but not synched. Also check that it does not have a closer parent that is synched.  
    for (Iterator<LongHashMap.Entry<ModelRecord>> it = modelMap.entrySetIterator(); it.hasNext(); ) {  
      ModelRecord record = it.next().getValue();
      if (!record.used && record.synched) {
        long[] parentIds = record.model.getParentIds();
        for (int i = 0; i < parentIds.length; i++) {
          ModelRecord parent = modelMap.get(parentIds[i]);
          if (parent == null) {
            continue;
          }
          if (parent.synched) {
            break;
          }
          if (parent.used) {
            markUsed(record);
            break;
          }
        }
      }
    }

    // Dispose all unused models and textures
    for (Iterator<LongHashMap.Entry<ModelRecord>> it = modelMap.entrySetIterator(); it.hasNext(); ) {  
      ModelRecord record = it.next().getValue();
      if (record.used) {
        continue;
      }
      it.remove();
    }

    for (Iterator<LongHashMap.Entry<TextureRecord>> it = textureMap.entrySetIterator(); it.hasNext(); ) {  
      TextureRecord record = it.next().getValue();
      if (record.used) {
        continue;
      }
      record.glTexture.dispose(gl);
      it.remove();
    }
  }

  @Override
  public boolean isSynchronized() {
    for (Iterator<LongHashMap.Entry<ModelRecord>> it = modelMap.entrySetIterator(); it.hasNext(); ) {
      ModelRecord record = it.next().getValue();
      if (record.used && !record.synched) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void dispose(GL10 gl) {
    for (TextureRecord record : textureMap.values()) {
      record.glTexture.dispose(gl);
    }
    modelMap.clear();
    textureMap.clear();
  }

  @Override
  public void draw(GL10 gl, CameraState camera, Pass pass) {
    if (pass != Pass.OVERLAY) {
      return;
    }
    gl.glDisable(GL10.GL_CULL_FACE);
    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    if (gl instanceof GL11) {
      GL11 gl11 = (GL11) gl;
      gl11.glGetIntegerv(GL11.GL_ALPHA_TEST_FUNC, alphaTestFunc, 0);
      gl11.glGetFloatv(GL11.GL_ALPHA_TEST_REF, alphaTestRef, 0);
      gl.glAlphaFunc(GL10.GL_GREATER, layer.getAlphaTestRef());
    }

    Point3D cameraPos = camera.cameraPos;
    for (Iterator<LongHashMap.Entry<ModelRecord>> it = modelMap.entrySetIterator(); it.hasNext(); ) {
      ModelRecord record = it.next().getValue();
      if (!record.synched) {
        continue;
      }
      
      NMLModel.NMLModelInternalState internalState = record.model.getInternalState();
      for (int i = 0; i < 16; i++) {
        localFrameMatrix[i] = internalState.globalTransformMatrix[i];
      }
      localFrameMatrix[12] -= cameraPos.x;
      localFrameMatrix[13] -= cameraPos.y;
      localFrameMatrix[14] -= cameraPos.z;
      double dotZ = localFrameMatrix[8] * (internalState.pos.x - cameraPos.x) + localFrameMatrix[9] * (internalState.pos.y - cameraPos.y) + localFrameMatrix[10] * (internalState.pos.z - cameraPos.z);
      if (dotZ > 0) {
        continue;
      }
      Matrix.doubleToFloatM(glLocalFrameMatrix, 0, localFrameMatrix, 0);
      
      gl.glPushMatrix();
      gl.glMultMatrixf(glLocalFrameMatrix, 0);
      internalState.glModel.draw(gl, GLSubmesh.DrawMode.DRAW_NORMAL);
      gl.glPopMatrix();
    }

    gl.glColor4f(1, 1, 1, 1);
    gl.glEnable(GL10.GL_CULL_FACE);
    if (gl instanceof GL11) {
      gl.glAlphaFunc(alphaTestFunc[0], alphaTestRef[0]);
    }
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
  }

  @Override
  public Point3D calculateElementLabelPos(VectorElement element, CameraState camera, Point3D clickPos) {
    if (element instanceof NMLModel.Proxy) {
      NMLModel.Proxy proxy = (NMLModel.Proxy) element;
      return proxy.getInternalState().pos;
    } else if (element instanceof NMLModel) {
      NMLModel model = (NMLModel) element;
      return model.getInternalState().pos;
    }
    return clickPos;
  }

  @Override
  public void drawPicking(GL10 gl, final CameraState camera, PickingState picking, final Pass pass) {
    if (pass != Pass.OVERLAY) {
      return;
    }
    gl.glDisable(GL10.GL_CULL_FACE);
    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

    Point3D cameraPos = camera.cameraPos;
    for (Iterator<LongHashMap.Entry<ModelRecord>> it = modelMap.entrySetIterator(); it.hasNext(); ) {
      ModelRecord record = it.next().getValue();
      if (!record.synched) {
        continue;
      }

      NMLModel.NMLModelInternalState internalState = record.model.getInternalState();
      for (int i = 0; i < 16; i++) {
        localFrameMatrix[i] = internalState.globalTransformMatrix[i];
      }
      localFrameMatrix[12] -= cameraPos.x;
      localFrameMatrix[13] -= cameraPos.y;
      localFrameMatrix[14] -= cameraPos.z;
      double dotZ = localFrameMatrix[8] * (internalState.pos.x - cameraPos.x) + localFrameMatrix[9] * (internalState.pos.y - cameraPos.y) + localFrameMatrix[10] * (internalState.pos.z - cameraPos.z);
      if (dotZ > 0) {
        continue;
      }
      Matrix.doubleToFloatM(glLocalFrameMatrix, 0, localFrameMatrix, 0);

      gl.glPushMatrix();
      gl.glMultMatrixf(glLocalFrameMatrix, 0);
      final NMLModel model = record.model;
      if (picking != null) {
        picking.bindHandler(gl, new PickingState.Handler() {
          public void draw(GL10 gl) {
            drawPicking(gl, camera, null, pass); // trick, call recursively with no picking state - we will draw vertex ids in this case
          }
          public VectorElement resolve(byte[] b) {
            if (model.getProxyMap() == null) {
              return model;
            }
            int modelId = ColorUtils.decodeIntFromColor(b);
            return model.getProxyMap().get(modelId);
          }
        });
      }
      internalState.glModel.draw(gl, picking != null ? GLSubmesh.DrawMode.DRAW_CONST : GLSubmesh.DrawMode.DRAW_VERTEX_IDS);
      gl.glPopMatrix();
    }

    gl.glColor4f(1, 1, 1, 1);
    gl.glEnable(GL10.GL_CULL_FACE);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
  }
}
