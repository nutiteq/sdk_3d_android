package com.nutiteq.renderers.layers;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.components.CameraState;

/**
 * Base layer renderer interface.
 */
public interface LayerRenderer {
  public enum Pass {
    BASE,
    OVERLAY
  };
  
  boolean isSynchronized();
  
  void synchronize(GL10 gl);
  
  void draw(GL10 gl, CameraState camera, Pass pass);
  
  void dispose(GL10 gl);
}
