package com.nutiteq.renderers.components;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.geometry.VectorElement;
import com.nutiteq.utils.ColorUtils;
import com.nutiteq.utils.IntHashMap;

/**
 * Wrapper class for handling picking (resolving clicked vector elements)
 */
public class PickingState {

  /**
   * Handler for drawing and resolving vector elements based on color. 
   */
  public interface Handler {
    void draw(GL10 gl);
    VectorElement resolve(byte[] color);
  }

  private int pickingIndex = 1;
  private List<VectorElement> pickingList = new ArrayList<VectorElement>();
  private IntHashMap<Handler> pickingHandlers = new IntHashMap<Handler>();

  public void clear(GL10 gl) {
    gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
    pickingIndex = 0;
    pickingList.clear();
    pickingHandlers.clear();
  }

  public int bindElement(VectorElement element) {
    pickingList.add(element);
    return pickingIndex++;
  }

  public int bindElement(GL10 gl, VectorElement element) {
    float[] color = ColorUtils.encodeIntAsFloatColor(pickingIndex);
    gl.glColor4f(color[0], color[1], color[2], 1);
    pickingList.add(element);
    return pickingIndex++;
  }

  public int bindHandler(GL10 gl, Handler handler) {
    float[] color = ColorUtils.encodeIntAsFloatColor(pickingIndex);
    gl.glColor4f(color[0], color[1], color[2], 1);
    pickingList.add(null);
    pickingHandlers.put(pickingIndex, handler);
    return pickingIndex++;
  }

  public VectorElement resolveElement(GL10 gl, int x, int y) {
    // Analyze the color picked from under the user finger
    ByteBuffer buffer = ByteBuffer.allocateDirect(4);
    buffer.order(ByteOrder.nativeOrder());
    gl.glReadPixels(x, y, 1, 1, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, buffer);
    byte b[] = new byte[4];
    buffer.get(b);

    int pixelIndex = ColorUtils.decodeIntFromColor(b);
    if (pixelIndex >= pickingList.size())
      return null;

    // Now there are 2 cases - either resolve directly or resolve indirectly using handler
    Handler handler = pickingHandlers.get(pixelIndex);
    if (handler != null) {
      clear(gl);
      handler.draw(gl);
      buffer.position(0);
      gl.glReadPixels(x, y, 1, 1, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, buffer);
      buffer.get(b);
      return handler.resolve(b);
    }
    return pickingList.get(pixelIndex);
  }
}
