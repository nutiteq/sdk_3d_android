package com.nutiteq.renderers.components;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Map tile with texture coordinates and mesh for rendering.
 */
public class MapTileDrawData {
  public final MapTileProxy proxy;
  public final double[] tileVerts;
  public final float[] tileNormals;
  public final FloatBuffer tileTexCoordBuf;
  public final ShortBuffer indexBuf;

  public MapTileDrawData(MapTileProxy proxy, double[] tileVerts, float[] tileNormals, FloatBuffer tileTexCoordBuf, ShortBuffer indexBuf) {
    this.proxy = proxy;
    this.tileVerts = tileVerts;
    this.tileNormals = tileNormals;
    this.tileTexCoordBuf = tileTexCoordBuf;
    this.indexBuf = indexBuf;
  }
}
