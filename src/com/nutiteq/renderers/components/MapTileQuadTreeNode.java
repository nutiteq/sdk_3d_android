package com.nutiteq.renderers.components;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MapTile;
import com.nutiteq.renderers.utils.MapTileGenerator;

/**
 * A quad tree map tile node containing links to children and parent.
 */
public class MapTileQuadTreeNode extends MapTile {
  public static final int TOP_LEFT = 0;
  public static final int TOP_RIGHT = 1;
  public static final int BOTTOM_RIGHT = 2;
  public static final int BOTTOM_LEFT = 3;

  public final int nodeType;
  public final MapTileGenerator tileGenerator;

  public final double centerX;
  public final double centerY;

  public final double minX;
  public final double maxX;
  public final double minY;
  public final double maxY;

  public int tesselateU;
  public int tesselateV;
  public float distance;
  public MapTileBounds bounds;

  public MapTileQuadTreeNode parent;
  
  public MapTileQuadTreeNode topLeft;
  public MapTileQuadTreeNode topRight;
  public MapTileQuadTreeNode bottomRight;
  public MapTileQuadTreeNode bottomLeft;

  public MapTileQuadTreeNode(MapTileGenerator tileGenerator, MapTileQuadTreeNode parent, int nodeType) {
    super(getChildX(parent.x, nodeType), getChildY(parent.y, nodeType), parent.zoom + 1, parent.id * 4 + nodeType + 1);
    this.nodeType = nodeType;
    this.tileGenerator = tileGenerator;
    this.parent = parent;

    switch (nodeType) {
    case TOP_LEFT:
      minX = parent.minX;
      minY = parent.centerY;
      maxX = parent.centerX;
      maxY = parent.maxY;
      break;
    case TOP_RIGHT:
      minX = parent.centerX;
      minY = parent.centerY;
      maxX = parent.maxX;
      maxY = parent.maxY;
      break;
    case BOTTOM_RIGHT:
      minX = parent.centerX;
      minY = parent.minY;
      maxX = parent.maxX;
      maxY = parent.centerY;
      break;
    default:
      minX = parent.minX;
      minY = parent.minY;
      maxX = parent.centerX;
      maxY = parent.centerY;
      break;
    }
    centerX = (minX + maxX) * 0.5;
    centerY = (minY + maxY) * 0.5;
    
    tesselateU = tesselateV = 1;
  }

  public MapTileQuadTreeNode(MapTileGenerator tileGenerator, MapPos origin, Bounds bounds, int x, int y, int id) {
    super(x, y, 0, id);
    this.nodeType = -1;
    this.tileGenerator = tileGenerator;
    this.parent = null;

    centerX = (bounds.left + bounds.right) * 0.5 + origin.x;
    centerY = (bounds.top + bounds.bottom) * 0.5 + origin.y;
    minX = centerX - bounds.getWidth() * 0.5;
    maxX = centerX + bounds.getWidth() * 0.5;
    minY = centerY - bounds.getHeight() * 0.5;
    maxY = centerY + bounds.getHeight() * 0.5;

    tesselateU = tesselateV = 1;
  }
  
  public MapTileQuadTreeNode getChild(int nodeType) {
    switch (nodeType) {
    case TOP_LEFT:
      return topLeft;
    case TOP_RIGHT:
      return topRight;
    case BOTTOM_RIGHT:
      return bottomRight;
    default:
      return bottomLeft;
    }
  }
  
  public int getChildFromPoint(MapPos point) {
    if (point.x >= centerX) {
      return (point.y < centerY ? BOTTOM_RIGHT : TOP_RIGHT);
    } else {
      return (point.y < centerY ? BOTTOM_LEFT : TOP_LEFT);
    }
  }
  
  public static int getChildX(int x, int nodeType) {
    return x * 2 + (nodeType == TOP_RIGHT || nodeType == BOTTOM_RIGHT ? 1 : 0);
  }

  public static int getChildY(int y, int nodeType) {
    return y * 2 + (nodeType == BOTTOM_RIGHT || nodeType == BOTTOM_LEFT ? 1 : 0);
  }

  @Override
  public int hashCode() {
    return (int) (Double.doubleToLongBits(minX) + Double.doubleToLongBits(minX) * 2 + Double.doubleToLongBits(maxX) * 3 + Double.doubleToLongBits(maxY) * 5);
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MapTileQuadTreeNode other = (MapTileQuadTreeNode) o;
    if (!super.equals(o)) {
      return false;
    }
    if (!tileGenerator.getBaseProjection().equals(other.tileGenerator.getBaseProjection())) {
      return false;
    }
    return minX == other.minX && minY == other.minY && maxX == other.maxX && maxY == other.maxY && tesselateU == other.tesselateU && tesselateV == other.tesselateV;
  }
}
