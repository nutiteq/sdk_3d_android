package com.nutiteq.renderers.components;

/**
 * Proxy for map tile - can be reference to original tile or replacement tile (either parent or child)
 */
public class MapTileProxy {
  public final long fullTileId;
  public final MapTileQuadTreeNode mapTile;
  public final float tileX;
  public final float tileY;
  public final float tileSize;
  
  public MapTileProxy(long fullTileId, MapTileQuadTreeNode mapTile) {
    this.fullTileId = fullTileId;
    this.mapTile = mapTile;
    this.tileX = 0;
    this.tileY = 0;
    this.tileSize = 1;
  }

  public MapTileProxy(long fullTileId, MapTileQuadTreeNode mapTile, float tileX, float tileY, float tileSize) {
    this.fullTileId = fullTileId;
    this.mapTile = mapTile;
    this.tileX = tileX;
    this.tileY = tileY;
    this.tileSize = tileSize;
  }

  @Override
  public int hashCode() {
    return (int) fullTileId + mapTile.hashCode() * 3 + Float.floatToIntBits(tileX) * 5 + Float.floatToIntBits(tileY) * 7 + Float.floatToIntBits(tileSize) * 11;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MapTileProxy other = (MapTileProxy) o;
    return fullTileId == other.fullTileId && mapTile.equals(other.mapTile) && tileX == other.tileX && tileY == other.tileY && tileSize == other.tileSize;
  }
}
