package com.nutiteq.renderers.components;

import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.utils.Frustum;

/**
 * 3D bounding box for map tiles. Contains additional information about tile orientation.
 */
public class MapTileBounds {
  public final double minX;
  public final double minY;
  public final double minZ;
  public final double maxX;
  public final double maxY;
  public final double maxZ;

  public final Point3D center;
  public final Vector3D normal;
  public final float maxNormalSpread; // angle in radians of maximum derivation from normal
  
  public MapTileBounds(double minX, double minY, double minZ, double maxX, double maxY, double maxZ, Point3D center, Vector3D normal, float maxNormalSpread) {
    this.minX = minX;
    this.minY = minY;
    this.minZ = minZ;
    this.maxX = maxX;
    this.maxY = maxY;
    this.maxZ = maxZ;

    this.center = center;
    this.normal = normal;
    this.maxNormalSpread = maxNormalSpread;
  }
  
  public boolean testIntersection(Frustum frustum, Point3D cameraPos) {
    // Test for cuboid intersection
    if (frustum.cuboidIntersects(minX, minY, minZ, maxX, maxY, maxZ) == Frustum.MISS) {
      return false;
    }
    
    // Additional test - test if tile is potentially front-facing
    double dx = center.x - cameraPos.x;
    double dy = center.y - cameraPos.y;
    double dz = center.z - cameraPos.z;
    double len = Math.sqrt(dx * dx + dy * dy + dz * dz);
    double dot = -(dx * normal.x + dy * normal.y + dz * normal.z) / len;
    if (maxNormalSpread < Math.PI / 2) {
      if (dot < Math.cos(Math.PI / 2 + maxNormalSpread)) {
        return false;
      }
    }
    return true;
  }
}
