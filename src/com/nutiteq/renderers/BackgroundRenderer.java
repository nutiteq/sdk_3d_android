package com.nutiteq.renderers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.CameraState;
import com.nutiteq.components.Color;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.Options;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.TextureInfo;
import com.nutiteq.components.Vector3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.renderprojections.PlanarRenderProjection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.utils.GLUtils;
import com.nutiteq.utils.Matrix;
import com.nutiteq.utils.TriangleMesh;

/**
 * Background renderer. Supports sky rendering plus rendering of different background types.
 */
public class BackgroundRenderer {

  // Sky vertices/texture coordinates
  private static final float[] SKY_VERTICES = {
    // North
    -0.5f, 0.5f, -0.5f, 0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f,
    // East
    0.5f, 0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, 0.5f, -0.5f, 0.5f,
    // South
    0.5f, -0.5f, -0.5f, -0.5f, -0.5f, -0.5f, 0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.5f,
    // West
    -0.5f, -0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f, -0.5f, 0.5f, -0.5f, 0.5f, 0.5f };
  private static final float[] SKY_TEXCOORDS = new float[] {
    // North
    0.0f, 1.0f, 0.25f, 1.0f, 0.0f, 0.0f, 0.25f, 0.0f,
    // East
    0.25f, 1.0f, 0.5f, 1.0f, 0.25f, 0.0f, 0.5f, 0.0f,
    // South
    0.5f, 1.0f, 0.75f, 1.0f, 0.5f, 0.0f, 0.75f, 0.0f,
    // West
    0.75f, 1.0f, 1.0f, 1.0f, 0.75f, 0.0f, 1.0f, 0.0f };

  private static final FloatBuffer SKY_VERTEX_BUF;
  private static final FloatBuffer SKY_TEXCOORD_BUF;

  static {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(SKY_VERTICES.length * Float.SIZE / 8);
    byteBuffer.order(ByteOrder.nativeOrder());
    SKY_VERTEX_BUF = byteBuffer.asFloatBuffer();
    SKY_VERTEX_BUF.put(SKY_VERTICES);
    SKY_VERTEX_BUF.position(0);

    byteBuffer = ByteBuffer.allocateDirect(SKY_TEXCOORDS.length * Float.SIZE / 8);
    byteBuffer.order(ByteOrder.nativeOrder());
    SKY_TEXCOORD_BUF = byteBuffer.asFloatBuffer();
    SKY_TEXCOORD_BUF.put(SKY_TEXCOORDS);
    SKY_TEXCOORD_BUF.position(0);    
  }

  private final MapRenderer mapRenderer;
  private final RenderSurface renderSurface;
  private final RenderProjection renderProjection;
  private final Projection baseProjection;
  private final Options options;

  private volatile Bitmap skyBitmap;
  private volatile boolean reloadSkyTexture;
  private int skyTexture;

  private volatile Bitmap backgroundPlaneBitmap;
  private volatile boolean reloadBackgroundPlaneTexture;
  private int backgroundPlaneTexture;
  private TextureInfo backgroundPlaneTextureInfo;

  private volatile Bitmap backgroundPlaneOverlayBitmap;
  private volatile boolean reloadBackgroundPlaneOverlayTexture;
  private int backgroundPlaneOverlayTexture;
  private TextureInfo backgroundPlaneOverlayTextureInfo;

  private volatile Bitmap backgroundImageBitmap;
  private volatile boolean reloadBackgroundImageTexture;
  private int backgroundImageTexture;
  private TextureInfo backgroundImageTextureInfo;

  private TriangleMesh surfaceMesh;
  private float[] surfaceVertices = new float[0];
  private FloatVertexBuffer surfaceVertexBuf = new FloatVertexBuffer();
  private float[] surfaceTexCoords = new float[0];
  private FloatVertexBuffer surfaceTexCoordBuf = new FloatVertexBuffer();
  private float[] surfaceColors = new float[0];
  private FloatVertexBuffer surfaceColorBuf = new FloatVertexBuffer();
  private ShortBuffer surfaceIndexBuf;

  private final float[] orthoProjectionMatrix = new float[16];

  public BackgroundRenderer(MapRenderer mapRenderer, Projection baseProjection, RenderSurface renderSurface, Options options) {
    this.mapRenderer = mapRenderer;
    this.baseProjection = baseProjection;
    this.renderSurface = renderSurface;
    this.renderProjection = renderSurface.getRenderProjection();
    this.options = options;
  }

  public void draw(GL10 gl, CameraState cameraState, int width, int height) {
    if (reloadSkyTexture) {
      reloadSkyTexture = false;
      loadSkyTexture(gl);
    }

    if (reloadBackgroundPlaneTexture) {
      reloadBackgroundPlaneTexture = false;
      loadBackgroundPlaneTexture(gl);
    }

    if (reloadBackgroundPlaneOverlayTexture) {
      reloadBackgroundPlaneOverlayTexture = false;
      loadBackgroundPlaneOverlayTexture(gl);
    }

    if (reloadBackgroundImageTexture) {
      reloadBackgroundImageTexture = false;
      loadBackgroundImageTexture(gl);
    }

    drawBackdropImage(gl, cameraState, width, height);
    if (renderProjection instanceof PlanarRenderProjection) { // HACK: due to numeric precision errors on high zoom levels, use specialized rendering for planar case
      drawSky(gl, cameraState);
      drawBackgroundPlane(gl, cameraState);
      drawBackgroundPlaneOverlay(gl, cameraState);
    } else {
      // Note: sky drawing is not supported now
      drawBackgroundSurface(gl, cameraState);
    }
    drawBackgroundImage(gl, cameraState, width, height);
  }

  public void setSkyBitmap(Bitmap skyBitmap) {
    this.skyBitmap = skyBitmap;
    reloadSkyTexture = true;
  }

  public void setBackgroundPlaneBitmap(Bitmap backgroundPlaneBitmap) {
    this.backgroundPlaneBitmap = backgroundPlaneBitmap;
    reloadBackgroundPlaneTexture = true;
  }

  public void setBackgroundPlaneOverlayBitmap(Bitmap backgroundPlaneOverlayBitmap) {
    this.backgroundPlaneOverlayBitmap = backgroundPlaneOverlayBitmap;
    reloadBackgroundPlaneOverlayTexture = true;
  }

  public void setBackgroundImageBitmap(Bitmap backgroundImageBitmap) {
    this.backgroundImageBitmap = backgroundImageBitmap;
    reloadBackgroundImageTexture = true;
  }

  private void drawSky(GL10 gl, CameraState cameraState) {
    if (options.getSkyDrawMode() != Options.DRAW_BITMAP) {
      return;
    }

    float skyScale = (float) (cameraState.farPlane * 2 / Math.sqrt(3));

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ZERO);

    gl.glEnable(GL10.GL_TEXTURE_2D);
    gl.glBindTexture(GL10.GL_TEXTURE_2D, skyTexture);
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, SKY_VERTEX_BUF);
    gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, SKY_TEXCOORD_BUF);

    gl.glPushMatrix();
    gl.glTranslatef(0, 0, (float) (cameraState.cameraPos.z * options.getSkyOffset() - cameraState.cameraPos.z));
    gl.glScalef(skyScale, skyScale, skyScale);
    gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, SKY_VERTICES.length / 3);
    gl.glPopMatrix();

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
  }

  private void drawBackgroundPlane(GL10 gl, CameraState cameraState) {
    int drawMode = options.getBackgroundPlaneDrawMode();
    if (drawMode == Options.DRAW_NOTHING) {
      return;
    }
    if (drawMode == Options.DRAW_BITMAP && backgroundPlaneTextureInfo == null) {
      return;
    }

    double halfFovTanX = mapRenderer.getAspectRatio() * Const.HALF_FOV_TAN_Y;
    double halfFovCosXY = (float) Math.cos(Math.atan(halfFovTanX)) * Const.HALF_FOV_COS_Y;
    float backgroundScale = (float) (cameraState.farPlane * 2 / halfFovCosXY);

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ZERO);
    gl.glDepthMask(false);

    gl.glPushMatrix();
    gl.glTranslatef(0, 0, (float) -cameraState.cameraPos.z);
    gl.glScalef(backgroundScale, backgroundScale, 0);
    if (drawMode == Options.DRAW_BITMAP) {
      gl.glMatrixMode(GL10.GL_TEXTURE);
      gl.glPushMatrix();
      int intZoomPow2 = (int) Math.pow(2, (int) cameraState.zoom);
      float scaleX = (float) (intZoomPow2 * 0.5 / backgroundPlaneTextureInfo.width);
      float scaleY = (float) (intZoomPow2 * 0.5 / backgroundPlaneTextureInfo.height);
      double translateX = cameraState.cameraPos.x * scaleX;
      double translateY = cameraState.cameraPos.y * scaleY;
      translateX = translateX - Math.floor(translateX); // this is theoretically unneccessary but seems to solve precision issues on Tegra 3 and some other devices
      translateY = translateY - Math.floor(translateY);
      gl.glTranslatef((float) translateX, (float) -translateY, 0);
      gl.glScalef(backgroundScale * scaleX, backgroundScale * scaleY, 1);
      gl.glTranslatef(-0.5f, -0.5f, 0);
      GLUtils.drawTexturedQuad(gl, backgroundPlaneTexture, backgroundPlaneTextureInfo.texCoordBuf);
      gl.glPopMatrix();
      gl.glMatrixMode(GL10.GL_MODELVIEW);
    } else {
      gl.glDisable(GL10.GL_TEXTURE_2D);
      Color bgColor = options.getBackgroundPlaneColor();
      gl.glColor4f(bgColor.r, bgColor.g, bgColor.b, 1.0f);
      gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
      GLUtils.drawQuad(gl);
      gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      gl.glEnable(GL10.GL_TEXTURE_2D);
    }
    gl.glPopMatrix();

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
    gl.glDepthMask(true);
  }

  private void drawBackgroundPlaneOverlay(GL10 gl, CameraState cameraState) {
    int drawMode = options.getBackgroundPlaneOverlayDrawMode();
    Bounds bounds = options.getBackgroundPlaneOverlayBounds();
    if (drawMode == Options.DRAW_NOTHING || bounds == null) {
      return;
    }
    if (drawMode == Options.DRAW_BITMAP && backgroundPlaneOverlayTextureInfo == null) {
      return;
    }

    double minX = Double.MAX_VALUE, maxX = -Double.MAX_VALUE;
    double minY = Double.MAX_VALUE, maxY = -Double.MAX_VALUE;
    for (int i = 0; i < 4; i++) {
      MapPos pos = new MapPos(i % 2 == 0 ? bounds.left : bounds.right, i / 2 == 0 ? bounds.top : bounds.bottom);
      Point3D point = renderProjection.project(baseProjection.toInternal(pos));
      minX = Math.min(minX, point.x); maxX = Math.max(maxX, point.x);
      minY = Math.min(minY, point.y); maxY = Math.max(maxY, point.y);
    }
    Bounds overlayBounds = new Bounds(minX, maxY, maxX, minY);

    float backgroundPosX = (float) ((overlayBounds.left + overlayBounds.right) * 0.5 - cameraState.cameraPos.x);
    float backgroundPosY = (float) ((overlayBounds.top + overlayBounds.bottom) * 0.5 - cameraState.cameraPos.y);
    float backgroundScale = (float) Math.min(overlayBounds.getWidth(), overlayBounds.getHeight());
    
    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ZERO);
    gl.glDepthMask(false);

    gl.glPushMatrix();
    gl.glTranslatef(backgroundPosX, backgroundPosY, (float) -cameraState.cameraPos.z);
    gl.glScalef(backgroundScale, backgroundScale, 0);
    if (drawMode == Options.DRAW_BITMAP) {
      gl.glMatrixMode(GL10.GL_TEXTURE);
      gl.glPushMatrix();
      int intZoomPow2 = (int) Math.pow(2, (int) cameraState.zoom);
      float scaleX = (float) (intZoomPow2 * 0.5 / backgroundPlaneOverlayTextureInfo.width);
      float scaleY = (float) (intZoomPow2 * 0.5 / backgroundPlaneOverlayTextureInfo.height);
      double translateX = (backgroundPosX + cameraState.cameraPos.x) * scaleX;
      double translateY = (backgroundPosY + cameraState.cameraPos.y) * scaleY;
      translateX = translateX - Math.floor(translateX); // this is theoretically unnecessary but seems to solve precision issues on Tegra 3 and some other devices
      translateY = translateY - Math.floor(translateY);
      gl.glTranslatef((float) translateX, (float) -translateY, 0);
      gl.glScalef(backgroundScale * scaleX, backgroundScale * scaleY, 1);
      gl.glTranslatef(-0.5f, -0.5f, 0);
      GLUtils.drawTexturedQuad(gl, backgroundPlaneOverlayTexture, backgroundPlaneOverlayTextureInfo.texCoordBuf);
      gl.glPopMatrix();
      gl.glMatrixMode(GL10.GL_MODELVIEW);
    } else {
      gl.glDisable(GL10.GL_TEXTURE_2D);
      Color bgColor = options.getBackgroundPlaneOverlayColor();
      gl.glColor4f(bgColor.r, bgColor.g, bgColor.b, 1.0f);
      gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
      GLUtils.drawQuad(gl);
      gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      gl.glEnable(GL10.GL_TEXTURE_2D);
    }
    gl.glPopMatrix();

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
    gl.glDepthMask(true);
  }

  private void drawBackgroundSurface(GL10 gl, CameraState cameraState) {
    int drawMode = options.getBackgroundPlaneDrawMode();
    if (drawMode == Options.DRAW_NOTHING) {
      return;
    }
    if (backgroundPlaneTextureInfo == null) {
      return;
    }

    // Calculate background scale and offset data
    double halfFovTanX = mapRenderer.getAspectRatio() * Const.HALF_FOV_TAN_Y;
    double halfFovCosXY = (float) Math.cos(Math.atan(halfFovTanX)) * Const.HALF_FOV_COS_Y;
    float backgroundScale = (float) (Const.UNIT_SIZE / halfFovCosXY);
    
    int intZoomPow2 = (int) Math.pow(2, (int) cameraState.zoom);
    double scaleX = intZoomPow2 * 0.5 / backgroundPlaneTextureInfo.width;
    double scaleY = intZoomPow2 * 0.5 / backgroundPlaneTextureInfo.height;
    MapPos mapPos = renderProjection.unproject(cameraState.cameraPos);
    double translateX = mapPos.x * scaleX - 0.5 * scaleX * backgroundScale;
    double translateY = mapPos.y * scaleY + 0.5 * scaleY * backgroundScale;
    translateX -= Math.floor(translateX); // this is theoretically unnecessary but seems to solve precision issues on Tegra 3 and some other devices
    translateY -= Math.floor(translateY);
    scaleX *= backgroundScale;
    scaleY *= backgroundScale;

    // Get focus point and mesh
    if (surfaceMesh == null) {
      surfaceMesh = renderSurface.getSurfaceTriangles();

      surfaceIndexBuf = ByteBuffer.allocateDirect(surfaceMesh.getTriangleIndices().length * Short.SIZE / 8).order(ByteOrder.nativeOrder()).asShortBuffer();
      surfaceIndexBuf.put(surfaceMesh.getTriangleIndices());
      surfaceIndexBuf.position(0);
    }

    // Create drawing vertex buffers from the mesh
    Point3D cameraPos = cameraState.cameraPos;
    double[] meshVertices = surfaceMesh.getVertices();
    if (surfaceVertices.length < meshVertices.length) {
      surfaceVertices = new float[meshVertices.length];
    }
    double x0 = cameraPos.x, y0 = cameraPos.y, z0 = cameraPos.z;
    for (int i = 0; i < meshVertices.length; i += 3) {
      surfaceVertices[i + 0] = (float) (meshVertices[i + 0] - x0);
      surfaceVertices[i + 1] = (float) (meshVertices[i + 1] - y0);
      surfaceVertices[i + 2] = (float) (meshVertices[i + 2] - z0);
    }
    surfaceVertexBuf.attach(surfaceVertices, meshVertices.length);

    float[] meshTexCoords = surfaceMesh.getTexCoords();
    if (surfaceTexCoords.length < meshTexCoords.length) {
      surfaceTexCoords = new float[meshTexCoords.length];
    }
    for (int i = 0; i < meshTexCoords.length; i += 2) {
      surfaceTexCoords[i + 0] = (float) (meshTexCoords[i + 0] * scaleX + translateX);
      surfaceTexCoords[i + 1] = (float) (meshTexCoords[i + 1] * scaleY - translateY);
    }
    surfaceTexCoordBuf.attach(surfaceTexCoords, meshTexCoords.length);

    Color color = (drawMode == Options.DRAW_BITMAP ? new Color(Color.WHITE) : options.getBackgroundPlaneColor());
    Vector3D focusPointNormal = renderProjection.getNormal(cameraState.focusPoint);
    float[] meshNormals = surfaceMesh.getNormals();
    if (surfaceColors.length < meshNormals.length * 4 / 3) {
      surfaceColors = new float[meshNormals.length * 4 / 3];
    }
    float[] surfaceColors = this.surfaceColors;
    float nx = (float) focusPointNormal.x, ny = (float) focusPointNormal.y, nz = (float) focusPointNormal.z;
    for (int i = 0, j = 0; i < meshNormals.length; i += 3, j += 4) {
      float dot = meshNormals[i + 0] * nx + meshNormals[i + 1] * ny + meshNormals[i + 2] * nz;
      float intensity = Const.AMBIENT_FACTOR + (1 - Const.AMBIENT_FACTOR) * dot;
      surfaceColors[j + 0] = color.r * intensity;
      surfaceColors[j + 1] = color.g * intensity;
      surfaceColors[j + 2] = color.b * intensity;
      surfaceColors[j + 3] = 1;
    }
    surfaceColorBuf.attach(surfaceColors, meshNormals.length * 4 / 3);

    // Draw using generated surface data
    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ZERO);
    gl.glDepthMask(false);

    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, surfaceVertexBuf.build(0, surfaceVertexBuf.size()));
    gl.glColorPointer(4, GL10.GL_FLOAT, 0, surfaceColorBuf.build(0, surfaceColorBuf.size()));
    gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
    if (drawMode == Options.DRAW_BITMAP) {
      gl.glBindTexture(GL10.GL_TEXTURE_2D, backgroundPlaneTexture);
      gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, surfaceTexCoordBuf.build(0, surfaceTexCoordBuf.size()));
      gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    } else {
      gl.glDisable(GL10.GL_TEXTURE_2D);
      gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
    }
    gl.glDrawElements(GL10.GL_TRIANGLES, surfaceIndexBuf.capacity(), GL10.GL_UNSIGNED_SHORT, surfaceIndexBuf);
    gl.glEnable(GL10.GL_TEXTURE_2D);
    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
    gl.glDepthMask(true);
  }

  private void drawBackgroundImage(GL10 gl, CameraState cameraState, int width, int height) {
    if (options.getBackgroundImageDrawMode() != Options.DRAW_BITMAP) {
      return;
    }
    if (backgroundImageTextureInfo == null) {
      return;
    }

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ZERO);
    gl.glDepthMask(false);

    gl.glPushMatrix();
    gl.glTranslatef(0, 0, (float) -cameraState.cameraPos.z);
    gl.glScalef(Const.UNIT_SIZE * 2, Const.UNIT_SIZE * 2, 0);
    GLUtils.drawTexturedQuad(gl, backgroundImageTexture, backgroundImageTextureInfo.texCoordBuf);
    gl.glPopMatrix();

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
    gl.glDepthMask(true);
  }

  private void drawBackdropImage(GL10 gl, CameraState cameraState, int width, int height) {
    if (options.getBackgroundImageDrawMode() != Options.DRAW_BACKDROP_BITMAP) {
      return;
    }
    if (backgroundImageTextureInfo == null) {
      return;
    }

    Matrix.orthoM(orthoProjectionMatrix, 0, width, 0, height, 0.1f, 10);

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ZERO);
    gl.glDepthMask(false);

    gl.glMatrixMode(GL10.GL_PROJECTION);
    gl.glPushMatrix();
    gl.glLoadMatrixf(orthoProjectionMatrix, 0);
    gl.glMatrixMode(GL10.GL_MODELVIEW);
    gl.glPushMatrix();
    gl.glLoadIdentity();
    gl.glScalef(width, height, 1);
    gl.glTranslatef(0.5f, 0.5f, -1);
    GLUtils.drawTexturedQuad(gl, backgroundImageTexture, backgroundImageTextureInfo.texCoordBuf);

    gl.glPopMatrix();
    gl.glMatrixMode(GL10.GL_PROJECTION);
    gl.glPopMatrix();
    gl.glMatrixMode(GL10.GL_MODELVIEW);

    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
    gl.glDepthMask(true);
  }

  private void loadSkyTexture(GL10 gl) {
    GLUtils.deleteTexture(gl, skyTexture);
    if (skyBitmap != null) {
      skyTexture = GLUtils.buildTexture(gl, skyBitmap);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
    } else {
      skyTexture = 0;
    }
  }

  private void loadBackgroundPlaneTexture(GL10 gl) {
    GLUtils.deleteTexture(gl, backgroundPlaneTexture);
    if (backgroundPlaneBitmap != null) {
      backgroundPlaneTextureInfo = TextureInfo.createScaled(backgroundPlaneBitmap, 1);
      backgroundPlaneTexture = GLUtils.buildMipmaps(gl, backgroundPlaneTextureInfo.bitmap);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
    } else {
      backgroundPlaneTextureInfo = null;
      backgroundPlaneTexture = 0;
    }
  }

  private void loadBackgroundPlaneOverlayTexture(GL10 gl) {
    GLUtils.deleteTexture(gl, backgroundPlaneOverlayTexture);
    if (backgroundPlaneOverlayBitmap != null) {
      backgroundPlaneOverlayTextureInfo = TextureInfo.createScaled(backgroundPlaneOverlayBitmap, 1);
      backgroundPlaneOverlayTexture = GLUtils.buildMipmaps(gl, backgroundPlaneOverlayTextureInfo.bitmap);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
    } else {
      backgroundPlaneOverlayTextureInfo = null;
      backgroundPlaneOverlayTexture = 0;
    }
  }

  private void loadBackgroundImageTexture(GL10 gl) {
    GLUtils.deleteTexture(gl, backgroundImageTexture);
    if (backgroundImageBitmap != null) {
      backgroundImageTextureInfo = TextureInfo.createScaled(backgroundImageBitmap, 1);
      backgroundImageTexture = GLUtils.buildMipmaps(gl, backgroundImageTextureInfo.bitmap);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
    } else {
      backgroundImageTextureInfo = null;
      backgroundImageTexture = 0;
    }
  }

}
