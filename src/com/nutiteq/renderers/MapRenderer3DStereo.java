package com.nutiteq.renderers;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import javax.microedition.khronos.opengles.GL10;

import android.view.Surface;
import android.view.SurfaceHolder;

import com.nutiteq.components.Color;
import com.nutiteq.components.Components;
import com.nutiteq.components.Vector3D;
import com.nutiteq.log.Log;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.Matrix;

/**
 * Map renderer interface implementation for stereoscopic displays.
 * Supports LG Real3D and HTC3D displays. 
 */
public class MapRenderer3DStereo extends MapRenderer3D {
  private static final float ASPECT_RATIO = 1; // NOTE: HTC seems to want 1.15f - 1.20f, but this could be bug with specific model
  private static final float FOCAL_LENGTH = 4;
  private static final float EYE_SEPARATION = FOCAL_LENGTH / 100.0f;

  private static final int LEFT_SIDE = 0;
  private static final int RIGHT_SIDE = 1;

  // Various matrices for left/right views
  private final float[] localMVMatrix1 = new float[16];
  private final float[] localMVMatrix2 = new float[16];
  private final float[] projectionMatrix1 = new float[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0 };
  private final float[] projectionMatrix2 = new float[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0 };

  private int matricesChangeCount;
  
  private boolean supported = false;

  public MapRenderer3DStereo(Components components, RenderSurface renderSurface) {
    super(components, renderSurface);
  }

  @Override
  public void onSurfaceChanged(GL10 gl, int width, int height) {
    supported = switchToHTC3D(view.getHolder().getSurface(), width, height);
    if (!supported) {
      supported = switchToReal3D(view.getHolder(), width, height);
    }

    super.onSurfaceChanged(gl, width, height);

    if (supported) {
      aspect = this.width / this.height / ASPECT_RATIO;
    }
  }

  @Override
  public void onDrawFrame(GL10 gl) {
    if (!supported) {
      super.onDrawFrame(gl);
      return;
    }

    redrawPending = false;

    frameStartTime = System.currentTimeMillis();

    rSelectedVectorElement = selectedVectorElement;

    synchronizeLayerRenderers(gl);

    loadTextures(gl);
    
    handleAnimations();

    handleKineticPanning();
    handleKineticRotation();

    detectClick(gl);

    if (matricesChanged) {
      matricesChangeCount = 1;
      handleMatrices(gl, LEFT_SIDE);
      handleMatrices(gl, RIGHT_SIDE);
    }
    setupRenderState(gl);

    updateLabel(gl);

    Color clearColor = options.getClearColor();
    gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
    gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

    draw(gl, LEFT_SIDE);
    draw(gl, RIGHT_SIDE);

    checkGLError(gl);

    lastFrameTime = (int) (System.currentTimeMillis() - frameStartTime);
    if (lastFrameTime > 200) {
      Log.debug("MapRenderer3DStereo.onDrawFrame: Last frame time " + lastFrameTime + "ms");
    }
    
    captureGLView(gl);

    Thread.yield();
  }

  private void draw(GL10 gl, int side) {
    double offset = near / FOCAL_LENGTH * EYE_SEPARATION * options.getStereoModeStrength();
    if (side == LEFT_SIDE) {
      gl.glViewport(0, 0, (int) width / 2, (int) height);
      double top = near * Const.HALF_FOV_TAN_Y;
      double bottom = -top;
      double left = bottom * aspect + 0.5f * offset;
      double right = top * aspect + 0.5f * offset;
      Matrix.frustumReinitializeM(projectionMatrix1, left, right, bottom, top, near, far);
      gl.glMatrixMode(GL10.GL_PROJECTION);
      gl.glLoadMatrixf(projectionMatrix1, 0);
      gl.glMatrixMode(GL10.GL_MODELVIEW);
      gl.glLoadMatrixf(localMVMatrix1, 0);
    } else {
      gl.glViewport((int) width / 2, 0, (int) width / 2, (int) height);
      
      double top = near * Const.HALF_FOV_TAN_Y;
      double bottom = -top;
      double left = bottom * aspect - 0.5f * offset;
      double right = top * aspect - 0.5f * offset;
      
      if (focusPointOffset != null) {
        double dx = 2 * near * Const.HALF_FOV_TAN_Y * focusPointOffset.x / height;
        double dy = 2 * near * Const.HALF_FOV_TAN_Y * focusPointOffset.y / height;

        top += dy;
        bottom += dy;
        left += dx;
        right += dx;
      }
      
      Matrix.frustumReinitializeM(projectionMatrix2, left, right, bottom, top, near, far);
      gl.glMatrixMode(GL10.GL_PROJECTION);
      gl.glLoadMatrixf(projectionMatrix2, 0);
      gl.glMatrixMode(GL10.GL_MODELVIEW);
      gl.glLoadMatrixf(localMVMatrix2, 0);
    }

    setupGLState(gl);
    drawBackground(gl);
    drawDepth(gl);
    drawLayers(gl);
    drawLabel(gl);
    drawWatermark(gl);

    gl.glViewport(0, 0, (int) width, (int) height);
    gl.glMatrixMode(GL10.GL_PROJECTION);
    gl.glLoadMatrixf(projectionMatrix, 0);
    gl.glMatrixMode(GL10.GL_MODELVIEW);
    gl.glLoadMatrixf(localMVMatrix, 0);
  }

  private void handleMatrices(GL10 gl, int side) {
    if (matricesChangeCount > 0) {
      if (matricesChangeCount > 1) {
        matricesChangeCount = 0;
      } else {
        matricesChangeCount++;
      }

      generalLock.lock();
      try {
        double sign = (side == LEFT_SIDE ? -1 : 1);
        double strength = EYE_SEPARATION / 2 * options.getStereoModeStrength();

        Vector3D cameraVec = new Vector3D(cameraPos, focusPoint);
        Vector3D unitCameraOffset = Vector3D.crossProduct(cameraVec, upVector).getNormalized();
        Vector3D cameraOffset = new Vector3D(unitCameraOffset.x * strength, unitCameraOffset.y * strength, unitCameraOffset.z * strength);

        Matrix.setLookAtM(modelviewMatrixDbl,
            cameraPos.x + sign * cameraOffset.x, cameraPos.y + sign * cameraOffset.y, cameraPos.z + sign * cameraOffset.z,
            focusPoint.x + sign * cameraOffset.x, focusPoint.y + sign * cameraOffset.y, focusPoint.z + sign * cameraOffset.z,
            upVector.x, upVector.y, upVector.z);
        Matrix.doubleToFloatM(modelviewMatrix, 0, modelviewMatrixDbl, 0);
        Matrix.copyM(localMVMatrix, 0, modelviewMatrix, 0);
        localMVMatrix[12] = 0;
        localMVMatrix[13] = 0;
        localMVMatrix[14] = 0;
        Matrix.copyM(side == LEFT_SIDE ? localMVMatrix1 : localMVMatrix2, 0, localMVMatrix, 0);
      }
      finally {
        generalLock.unlock();
      }
    }
  }

  // FIXME: this returns true also for HTC One which does not really support 3D!
  public static boolean isHTC3DSupported() {
    // Check for com.htc.view.DisplaySetting.setStereoscopic3DFormat
    ClassLoader classLoader = ClassLoader.getSystemClassLoader();
    Class<?> displaySettingCls;
    try {
      displaySettingCls = classLoader.loadClass("com.htc.view.DisplaySetting");
      displaySettingCls.getMethod("setStereoscopic3DFormat", new Class<?>[] { Surface.class, int.class });
    } catch (ClassNotFoundException e) {
      return false;
    } catch (SecurityException e) {
      return false;
    } catch (NoSuchMethodException e) {
      return false;
    }
    return true;
  }

  public static boolean isReal3DSupported() {
    // Check for com.lge.real3d.Real3D
    ClassLoader classLoader = ClassLoader.getSystemClassLoader();
    try {
      classLoader.loadClass("com.lge.real3d.Real3D");
    } catch (ClassNotFoundException e) {
      return false;
    }
    return true;
  }
  
  private static boolean switchToHTC3D(Surface surface, int width, int height) {
    final int STEREOSCOPIC_3D_FORMAT_OFF = 0x00;
    final int STEREOSCOPIC_3D_FORMAT_SIDE_BY_SIDE = 0x01;

    // Try to call com.htc.view.DisplaySetting.setStereoscopic3DFormat using reflection
    try {
      boolean enable = width > height;
      ClassLoader classLoader = ClassLoader.getSystemClassLoader();
      Class<?> displaySettingCls = classLoader.loadClass("com.htc.view.DisplaySetting");
      Method setStereoscopic3DFormatMethod = displaySettingCls.getMethod("setStereoscopic3DFormat", new Class<?>[] { Surface.class, int.class });
      Object result = setStereoscopic3DFormatMethod.invoke(null, new Object[] { surface, enable ? STEREOSCOPIC_3D_FORMAT_SIDE_BY_SIDE : STEREOSCOPIC_3D_FORMAT_OFF });
      Log.debug("MapRenderer3DStereo.switchToHTC3D: HTC 3D stereo enabled: " + enable + ", result: " + (Boolean) result);
      return enable && (Boolean) result;
    } catch (Exception e) {
      Log.debug("MapRenderer3DStereo.switchToHTC3D: exception while switching to/from stereo mode: " + e);
      return false;
    }
  }
  
  private static boolean switchToReal3D(SurfaceHolder holder, int width, int height) {
    final int REAL3D_TYPE_NONE = 0x00;
    final int REAL3D_TYPE_SS = 0x01;
    final int REAL3D_ORDER_LR = 0x00;

    // Create instances of Real3D and Real3DInfo classes and use setReal3DInfo method
    ClassLoader classLoader = ClassLoader.getSystemClassLoader();
    try {
      boolean enable = width > height;
      Class<?> real3DCls = classLoader.loadClass("com.lge.real3d.Real3D");
      Constructor<?> real3DConstructor = real3DCls.getConstructor(new Class<?>[] { SurfaceHolder.class });
      Object real3D = real3DConstructor.newInstance(new Object[] { holder });

      Class<?> real3DInfoCls = classLoader.loadClass("com.lge.real3d.Real3DInfo");
      Constructor<?> real3DInfoConstructor = real3DInfoCls.getConstructor(new Class<?>[] { boolean.class, int.class, int.class });
      Object real3DInfo = real3DInfoConstructor.newInstance(new Object[] { enable, enable ? REAL3D_TYPE_SS : REAL3D_TYPE_NONE, REAL3D_ORDER_LR });
      
      Method setReal3DInfoMethod = real3DCls.getMethod("setReal3DInfo", new Class<?>[] { real3DInfoCls });
      setReal3DInfoMethod.invoke(real3D, new Object[] { real3DInfo });
      Log.debug("MapRenderer3DStereo.switchToReal3D: Real 3D stereo enabled: " + enable);
      return enable;
    } catch (Exception e) {
      Log.debug("MapRenderer3DStereo.switchToReal3D: exception while switching to/from stereo mode: " + e);
      return false;
    }
  }
}
