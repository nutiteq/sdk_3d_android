package com.nutiteq.renderers;

import com.nutiteq.components.Components;
import com.nutiteq.components.Options;
import com.nutiteq.projections.EPSG3857;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderers.rendersurfaces.PlanarRenderSurface;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.renderers.rendersurfaces.SphericalRenderSurface;
import com.nutiteq.renderprojections.RenderProjection;

/**
 * Map renderer container. 
 */
public class MapRenderers {
  private final Components components;
  private MapRenderer mapRenderer;

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public MapRenderers(Components components) {
    this.components = components;
    if (!reset(components.options.getRenderProjection(), components.options.getRenderMode())) {
      reset(components.options.getRenderProjection(), Options.NORMAL_RENDERMODE);
    }
  }
  
  /**
   * Get current map renderer instance.
   * 
   * @return current map renderer
   */
  public MapRenderer getMapRenderer() {
    return mapRenderer;
  }
  
  /**
   * Get current render projection.
   * 
   * @return current map renderer
   */
  public RenderProjection getRenderProjection() {
    return mapRenderer.getRenderSurface().getRenderProjection();
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public boolean reset(int renderProj, int renderMode) {
    RenderSurface renderSurface = createRenderSurface(renderProj);

    switch (renderMode) {
    case Options.STEREO_RENDERMODE:
      if (MapRenderer3DStereo.isHTC3DSupported() || MapRenderer3DStereo.isReal3DSupported()) {
        mapRenderer = new MapRenderer3DStereo(components, renderSurface); // Stereo-screen (3D)
        return true;
      }
      return false;
    default:
      mapRenderer = new MapRenderer3D(components, renderSurface);
      return true;
    }
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public RenderSurface createRenderSurface(int renderProj) {
    Projection baseProjection = components.layers.getBaseProjection();
    if (baseProjection == null) {
      baseProjection = new EPSG3857();
    }
    return createRenderSurface(renderProj, baseProjection);
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public RenderSurface createRenderSurface(int renderProj, Projection baseProjection) {
    switch (renderProj) {
    case Options.SPHERICAL_RENDERPROJECTION:
      return new SphericalRenderSurface();
    default:
      return new PlanarRenderSurface(baseProjection, components.options);
    }
  }
  
}
