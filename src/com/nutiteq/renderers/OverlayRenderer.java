package com.nutiteq.renderers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;

import com.nutiteq.utils.GLUtils;
import com.nutiteq.utils.Utils;

/**
 * Renderer for overlays that are placed on top of view.
 */
public class OverlayRenderer {
  private FloatBuffer vertexBuffer;
  private FloatBuffer texCoordBuffer;
  private Bitmap bitmap;
  private int glTextureId = -1;
  
  public OverlayRenderer() {
    float x0 = -1, y0 = -1, x1 = 1, y1 = 1;
    final float[] verts = new float[] { x0, y0, 0, x1, y0, 0, x0, y1, 0, x1, y1, 0 };
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(verts.length * Float.SIZE / 8);
    byteBuffer.order(ByteOrder.nativeOrder());
    vertexBuffer = byteBuffer.asFloatBuffer();
    vertexBuffer.put(verts);
    vertexBuffer.position(0);
  }
  
  public void dispose(GL10 gl) {
    if (glTextureId != -1) {
      GLUtils.deleteTexture(gl, glTextureId);
      glTextureId = -1;
    }
  }

  public void draw(GL10 gl, Bitmap bitmap, float x, float y, float scale, float aspect) {
    if (this.bitmap != bitmap && glTextureId != -1) {
      GLUtils.deleteTexture(gl, glTextureId);
      glTextureId = -1;
    }
    if (glTextureId == -1) {
      int width = bitmap.getWidth();
      int height = bitmap.getHeight();
      int realWidth = Utils.upperPow2(width);
      int realHeight = Utils.upperPow2(height);

      Bitmap realBitmap = Bitmap.createBitmap(realWidth, realHeight, Config.ARGB_8888);
      realBitmap.setDensity(bitmap.getDensity());
      Canvas canvas = new Canvas(realBitmap);
      canvas.drawBitmap(bitmap, 0, 0, null);
      glTextureId = GLUtils.buildMipmaps(gl, realBitmap);
      realBitmap.recycle();

      float u0 = 0, v0 = (float) height / realHeight, u1 = (float) width / realWidth, v1 = 0;
      final float[] texCoords = new float[] { u0, v0, u1, v0, u0, v1, u1, v1 };
      ByteBuffer byteBuffer = ByteBuffer.allocateDirect(texCoords.length * Float.SIZE / 8);
      byteBuffer.order(ByteOrder.nativeOrder());
      texCoordBuffer = byteBuffer.asFloatBuffer();
      texCoordBuffer.put(texCoords);
      texCoordBuffer.position(0);
      
      this.bitmap = bitmap;
    }

    gl.glEnable(GL10.GL_TEXTURE_2D);
    gl.glBindTexture(GL10.GL_TEXTURE_2D, glTextureId);
    gl.glDisable(GL10.GL_DEPTH_TEST);
    gl.glDepthMask(false);
    gl.glAlphaFunc(GL10.GL_GREATER, 0.01f);
    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);

    gl.glMatrixMode(GL10.GL_MODELVIEW);
    gl.glPushMatrix();
    gl.glLoadIdentity();
    float bitmapScale = scale / Math.max(bitmap.getWidth(), bitmap.getHeight());
    float xScale = bitmapScale * Math.min(1 / aspect, 1) * bitmap.getWidth(), yScale = bitmapScale * Math.min(aspect, 1) * bitmap.getHeight();
    gl.glTranslatef(x * (1 - xScale), y * (1 - yScale), 0);
    gl.glScalef(xScale, yScale, 1);
    gl.glMatrixMode(GL10.GL_PROJECTION);
    gl.glPushMatrix();
    gl.glLoadIdentity();

    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
    gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, texCoordBuffer);
    gl.glColor4f(1, 1, 1, 1);
    gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);

    gl.glMatrixMode(GL10.GL_PROJECTION);
    gl.glPopMatrix();
    gl.glMatrixMode(GL10.GL_MODELVIEW);
    gl.glPopMatrix();

    gl.glDepthMask(true);
    gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
  }

}
