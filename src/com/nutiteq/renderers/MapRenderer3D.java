package com.nutiteq.renderers;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.opengl.GLSurfaceView;
import android.util.DisplayMetrics;
import android.view.MotionEvent;

import com.nutiteq.cache.TextureBitmapCache;
import com.nutiteq.cache.TextureInfoCache;
import com.nutiteq.components.Bounds;
import com.nutiteq.components.CameraState;
import com.nutiteq.components.Color;
import com.nutiteq.components.Components;
import com.nutiteq.components.Constraints;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MapTile;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.Options;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Range;
import com.nutiteq.components.TextureInfo;
import com.nutiteq.components.Vector;
import com.nutiteq.components.Vector3D;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.layers.Layer;
import com.nutiteq.layers.Layers;
import com.nutiteq.layers.TileLayer;
import com.nutiteq.license.DefaultWatermark;
import com.nutiteq.license.LicenseManager;
import com.nutiteq.license.Watermark;
import com.nutiteq.log.Log;
import com.nutiteq.projections.Projection;
import com.nutiteq.rasterlayers.RasterLayer;
import com.nutiteq.renderers.components.PickingState;
import com.nutiteq.renderers.layers.GeometryLayerRenderer;
import com.nutiteq.renderers.layers.LayerRenderer;
import com.nutiteq.renderers.layers.MarkerLayerRenderer;
import com.nutiteq.renderers.layers.NMLModelLayerRenderer;
import com.nutiteq.renderers.layers.Polygon3DLayerRenderer;
import com.nutiteq.renderers.layers.RasterLayerRenderer;
import com.nutiteq.renderers.layers.TextLayerRenderer;
import com.nutiteq.renderers.layers.VectorLayerRenderer;
import com.nutiteq.renderers.layers.VectorTileLayerRenderer;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.renderers.threads.BillBoardCullThread;
import com.nutiteq.renderers.threads.TileCullThread;
import com.nutiteq.renderers.threads.VectorCullThread;
import com.nutiteq.renderers.utils.AnimationQueue;
import com.nutiteq.renderers.utils.MapTileGenerator;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.touchhandlers.TouchHandler;
import com.nutiteq.touchhandlers.TouchHandler3D;
import com.nutiteq.ui.Label;
import com.nutiteq.ui.MapListener;
import com.nutiteq.ui.MapRenderListener;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.utils.GLUtils;
import com.nutiteq.utils.GeomUtils;
import com.nutiteq.utils.Matrix;
import com.nutiteq.utils.Utils;
import com.nutiteq.vectorlayers.BillBoardLayer;
import com.nutiteq.vectorlayers.GeometryLayer;
import com.nutiteq.vectorlayers.MarkerLayer;
import com.nutiteq.vectorlayers.NMLModelLayer;
import com.nutiteq.vectorlayers.Polygon3DLayer;
import com.nutiteq.vectorlayers.TextLayer;
import com.nutiteq.vectorlayers.VectorLayer;
import com.nutiteq.vectorlayers.VectorTileLayer;

/**
 * General implementation of map renderer interface.
 * Supports different render projections.
 */
public class MapRenderer3D implements MapRenderer {
  private static final float KINETIC_PAN_STOP_TOLERANCE_ZOOM_0 = 18 * Const.UNIT_SIZE;
  private static final float KINETIC_PAN_START_TOLERANCE_ZOOM_0 = 600 * Const.UNIT_SIZE;
  private static final float KINETIC_PAN_SLOWDOWN = 0.9f;
  private static final float KINETIC_PAN_DAMPING = 0.8f;
  private static final float KINETIC_PAN_INITIAL_FACTOR = 0.9f;
  private static final float KINETIC_PAN_MIN_THRESHOLD = 0.25f;

  private static final float KINETIC_ROTATION_STOP_TOLERANCE_ANGLE = 0.5f;
  private static final float KINETIC_ROTATION_START_TOLERANCE_ANGLE = 3;
  private static final float KINETIC_ROTATION_SLOWDOWN = 0.85f;
  private static final float KINETIC_ROTATION_DAMPING = 0.8f;
  private static final float KINETIC_ROTATION_INITIAL_FACTOR = 0.7f;

  private static final int FPS_MAX = 60;
  private static final float FPS_TEXT_SCALE = 0.15f;

  private FloatBuffer depthVertBuf;
  
  private Bitmap skyBitmap;
  private Bitmap backgroundPlaneBitmap;
  private Bitmap backgroundPlaneOverlayBitmap;
  private Bitmap backgroundImageBitmap;
  private BackgroundRenderer backgroundRenderer;

  private Watermark defaultWatermark;
  private List<OverlayRenderer> watermarkRenderers = new ArrayList<OverlayRenderer>();

  private OverlayRenderer fpsRenderer;

  private TextureInfoCache styleCache = new TextureInfoCache(Const.STYLE_TEXTURE_CACHE_SIZE);
  private TextureBitmapCache bitmapCache = new TextureBitmapCache(Const.BITMAP_TEXTURE_CACHE_SIZE);
  private Map<Layer, LayerRenderer> layerRenderers = new HashMap<Layer, LayerRenderer>();

  protected GLSurfaceView view;

  // 4x4 projection matrix
  protected final float[] projectionMatrix = new float[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0 };
  protected final float[] orthoProjectionMatrix = new float[16];
  // 4x4 modelview matrix
  protected final float[] modelviewMatrix = new float[16];
  protected double[] modelviewMatrixDbl = new double[16];
  protected final float[] localMVMatrix = new float[16];

  // Camera's position, focus point and view distance
  protected Point3D cameraPos;
  protected Point3D focusPoint;
  protected Vector3D upVector;
  protected float rotationDeg;
  protected float tiltDeg;
  protected float zoom;
  protected float zoomPow2;

  // Camera state for rendering. Keeps synchronized values of the above.
  protected CameraState cameraState;

  private volatile boolean kineticPanning;
  private float kineticPanInertia = 0;
  private Point3D kineticPanOrigin = new Point3D(); 
  private Point3D kineticPanTarget = new Point3D(); 
  private volatile boolean kineticRotation;
  private float kineticRotationInertia = 0;

  private AnimationQueue animationQueue;

  private volatile boolean detectClick;
  private volatile boolean detectLongClick;
  private final MutableMapPos clickPos2D = new MutableMapPos();
  private Point3D clickPoint;

  private float bestZoom0Distance;
  protected float width;
  protected float height;
  protected float aspect;
  protected float densityDpi = DisplayMetrics.DENSITY_HIGH;
  protected Vector focusPointOffset = null;

  protected float near;
  protected float far;

  protected volatile boolean matricesChanged;

  // Current selected element. This can be read/changed in different threads!
  protected volatile VectorElement selectedVectorElement;
  // Current selected element used in rendering thread. This is not touched by any other thread
  protected VectorElement rSelectedVectorElement;
  protected volatile Label rSelectedVectorElementLabel;
  protected volatile boolean updateLabelPos;
  protected volatile boolean reloadLabelTexture;
  protected TextureInfo labelTextureInfo;
  protected float labelTextureAlpha;
  private int labelTexture;
  private MapPos labelPos;

  protected int tileTextureCreateCount;
  protected long frameStartTime;
  protected int lastFrameTime;
  protected volatile boolean redrawPending;
  protected volatile boolean resetLayerRenderers;

  protected final Components components;
  private final PickingState pickingState = new PickingState();
  private final Layers layers;
  private final Constraints constraints;
  protected final Options options;

  protected List<MapRenderListener> mapRenderListeners = new LinkedList<MapRenderListener>();

  protected ReentrantLock generalLock = new ReentrantLock();

  protected volatile RenderSurface renderSurface;
  protected volatile RenderProjection renderProjection;
  protected final TouchHandler touchHandler;

  private VectorCullThread vectorCullThread;
  private TileCullThread tileCullThread;
  private BillBoardCullThread billBoardCullThread;

  public MapRenderer3D(Components components, RenderSurface renderSurface) {
    this.components = components;
    this.renderSurface = renderSurface;
    this.renderProjection = renderSurface.getRenderProjection();

    view = null;
    layers = components.layers;
    constraints = components.constraints;
    options = components.options;

    cameraPos = renderProjection.project(new MapPos(0, 0, 1));
    focusPoint = renderProjection.project(new MapPos(0, 0, 0));
    upVector = GeomUtils.transform(new Vector3D(0, 1, 0), renderProjection.getLocalFrameMatrix(cameraPos));

    recalculateCameraState();

    touchHandler = new TouchHandler3D(this, components);
    animationQueue = new AnimationQueue(this);
    vectorCullThread = new VectorCullThread(this, components);
    tileCullThread = new TileCullThread(this, components);
    billBoardCullThread = new BillBoardCullThread(this, components);
  }

  // Check for openGl errors
  protected static void checkGLError(GL10 gl) {
    int error = gl.glGetError();
    if (error != GL10.GL_NO_ERROR) {
      Log.debug("checkGLError: GLError 0x" + Integer.toHexString(error));
    }
  }

  @Override
  public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    setupGLState(gl);

    // Reset texture cache
    components.textureMemoryCache.onSurfaceCreated(gl);

    // Reset style and bitmap caches
    styleCache.onSurfaceCreated(gl);
    bitmapCache.onSurfaceCreated(gl);

    // Reset model layer renderers
    layerRenderers.clear();
    
    // Create new background renderer
    backgroundRenderer = new BackgroundRenderer(this, layers.getBaseProjection(), renderSurface, options);
    backgroundRenderer.setSkyBitmap(skyBitmap);
    backgroundRenderer.setBackgroundPlaneBitmap(backgroundPlaneBitmap);
    backgroundRenderer.setBackgroundPlaneOverlayBitmap(backgroundPlaneOverlayBitmap);
    backgroundRenderer.setBackgroundImageBitmap(backgroundImageBitmap);

    // Create new watermark renderer
    defaultWatermark = new DefaultWatermark();
    watermarkRenderers.clear();
    fpsRenderer = new OverlayRenderer();

    // Clear texture handles - all textures must be recreated
    labelTexture = 0;

    // Reset label state
    rSelectedVectorElementLabel = null;
    labelPos = null;
    updateLabelPos = true;
    labelTextureInfo = null;
    labelTextureAlpha = 1.0f;
    reloadLabelTexture = true;

    tileTextureCreateCount = 0;

    MapListener mapListener = options.getMapListener();
    if (mapListener != null) {
      mapListener.onSurfaceCreated(gl, config);
    }

    Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
  }

  @Override
  public void onSurfaceChanged(GL10 gl, int width, int height) {
    this.densityDpi = DisplayMetrics.DENSITY_HIGH;
    if (view != null) {
      Resources resources = view.getResources();
      if (resources != null) {
        DisplayMetrics dm = resources.getDisplayMetrics();
        if (dm != null) {
          this.densityDpi = dm.densityDpi;
        }
      }
    }
    this.width = width;
    this.height = height;
    this.aspect = (float) width / (float) height;

    Matrix.orthoM(orthoProjectionMatrix, 0, width, 0, height, 0.1f, 10);

    gl.glViewport(0, 0, width, height);

    recalculateBestZoom0Distance();
    updateZoom(zoom);

    detectClick = false;

    kineticPanning = false;
    kineticPanInertia = 0;
    kineticRotation = false;
    kineticRotationInertia = 0;

    recalculateCameraState();
    matricesChanged = true;

    MapListener mapListener = options.getMapListener();
    if (mapListener != null) {
      mapListener.onSurfaceChanged(gl, width, height);
    }

    vectorCullThread.startTasks();
    tileCullThread.startTasks();
    billBoardCullThread.startTasks();

    frameStartTime = System.currentTimeMillis();

    rendererChanged();
  }

  @Override
  public void onDrawFrame(GL10 gl) {
    redrawPending = false;

    frameStartTime = System.currentTimeMillis();

    rSelectedVectorElement = selectedVectorElement;

    synchronizeLayerRenderers(gl);

    loadTextures(gl);

    handleAnimations();

    handleKineticPanning();
    handleKineticRotation();

    detectClick(gl);

    setupRenderState(gl);

    updateLabel(gl);

    Color clearColor = options.getClearColor();
    gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
    gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

    setupGLState(gl);
    drawBackground(gl);
    drawDepth(gl);
    drawLayers(gl);
    drawLabel(gl);
    drawWatermark(gl);
    checkGLError(gl);

    lastFrameTime = (int) (System.currentTimeMillis() - frameStartTime);
    if (lastFrameTime > 200) {
      Log.debug("MapRenderer3D.onDrawFrame: Last frame time " + lastFrameTime + "ms");
    }

    captureGLView(gl);

    Thread.yield();
  }

  protected void setupGLState(GL10 gl) {
    gl.glGetError(); // this will clear pending errors
    gl.glEnable(GL10.GL_CULL_FACE);
    gl.glCullFace(GL10.GL_BACK);

    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);

    gl.glEnable(GL10.GL_TEXTURE_2D);
    gl.glTexEnvf(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE);

    gl.glEnable(GL10.GL_BLEND);
    gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);

    gl.glAlphaFunc(GL10.GL_GREATER, 0.01f);
    gl.glEnable(GL10.GL_ALPHA_TEST);

    gl.glDisable(GL10.GL_DITHER);

    gl.glDisable(GL10.GL_DEPTH_TEST);
    gl.glDepthFunc(GL10.GL_LEQUAL); // important to ensure for 2D layers to have layer order correspond to display order
    gl.glDepthMask(true);
  }

  protected void setupMapListenerGLState(GL10 gl) {
    gl.glGetError(); // this will clear pending errors
    gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
    gl.glEnable(GL10.GL_TEXTURE_2D);
    gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);

    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glDisableClientState(GL10.GL_COLOR_ARRAY);

    gl.glDisable(GL10.GL_DEPTH_TEST);
    gl.glDepthFunc(GL10.GL_LEQUAL);
    gl.glDepthMask(true);
  }

  protected void captureGLView(GL10 gl) {
    boolean wait = redrawPending || !vectorCullThread.isEmptyTaskList() || !tileCullThread.isEmptyTaskList() || !billBoardCullThread.isEmptyTaskList();
    if (!wait) {
      List<MapRenderListener> listeners = null;
      synchronized (mapRenderListeners) {
        if (!mapRenderListeners.isEmpty()) {
          listeners = mapRenderListeners;
          mapRenderListeners = new LinkedList<MapRenderListener>();
        }
      }
      if (listeners != null) {
        Bitmap mapBitmap = GLUtils.saveSurfaceBitmap(gl, 0, 0, (int) width, (int) height);
        for (MapRenderListener listener : listeners) {
          listener.onMapRendered(mapBitmap);
        }
      }
    }
  }

  protected void loadTextures(GL10 gl) {
    styleCache.createAndDeleteTextures(gl);
    bitmapCache.createAndDeleteTextures(gl);

    int createCount = components.textureMemoryCache.createAndDeleteTextures(gl);
    if (createCount > 0) {
      tileTextureCreateCount += createCount;
      requestRenderView();
    } else if (tileTextureCreateCount > 0) {
      tileTextureCreateCount = 0;
      tileCullThread.addTask(null, Const.FRUSTUM_CULL_DELAY); // some textures were added, create new cull task to find better tiles if possible
    }
    
    createCount = components.vectorTileCache.createAndDeleteTiles(gl);
    if (createCount > 0) {
      tileCullThread.addTask(null, Const.FRUSTUM_CULL_DELAY); // some tiles were added, create new cull task to find better tiles if possible
      billBoardCullThread.addTask(null, Const.TEXT_CULL_DELAY); // generate placements for new texts
    }
  }

  protected void updateLabel(GL10 gl) {
    Label oldLabel = rSelectedVectorElementLabel;
    Label label = null;
    if (rSelectedVectorElement != null) {
      label = rSelectedVectorElement.getLabel();
      if (label != null) {
        if (label != oldLabel || label.isDirty()) {
          reloadLabelTexture = true;
          if (labelPos == null) {
            updateLabelPos = true;
          }
        }
      }
    }
    rSelectedVectorElementLabel = label;

    MapPos oldLabelPos = labelPos;
    if (updateLabelPos) {
      updateLabelPos = false;
      recalculateLabelPos(rSelectedVectorElement);
    }

    if (reloadLabelTexture) {
      reloadLabelTexture = false;
      if (label != null) {
        labelTextureInfo = label.drawMarkerLabel();
        labelTextureAlpha = label.getMarkerLabelAlpha();
        loadLabelTexture(gl, labelTextureInfo.bitmap);
      }
    }

    GLSurfaceView view = getView();
    if (label != oldLabel || labelPos != oldLabelPos) {
      if (oldLabel != null && view != null) {
        oldLabel.onHide(view);
      }
      if (label != null && labelPos != null && view != null) {
        label.onShow(view, (float) labelPos.x, (float) labelPos.y);
      }
    }
  }

  protected void handleAnimations() {
    animationQueue.update();
  }

  protected void handleKineticPanning() {
    if (options.isKineticPanning() && kineticPanning) {
      generalLock.lock();
      try {
        if (kineticPanInertia * kineticPanInertia < KINETIC_PAN_STOP_TOLERANCE_ZOOM_0 / (zoomPow2 * zoomPow2)) {
          stopKineticPanning();
        } else {
          float dPos = (float) renderProjection.getDistance(kineticPanOrigin, kineticPanTarget);
          double[] transform = renderProjection.getTranslateMatrix(kineticPanOrigin, kineticPanTarget, kineticPanInertia / dPos);
          focusPoint = GeomUtils.transform(focusPoint, transform);
          cameraPos = GeomUtils.transform(cameraPos, transform);
          upVector = GeomUtils.transform(upVector, transform);
          
          applyMovementConstraints();

          matricesChanged = true;
          frustumChanged();
    
          kineticPanInertia *= KINETIC_PAN_SLOWDOWN;
        }
      } finally {
        generalLock.unlock();
      }
    }
  }

  protected void handleKineticRotation() {
    if (options.isKineticRotation() && kineticRotation) {
      generalLock.lock();
      try {
        if (kineticRotationInertia * kineticRotationInertia < KINETIC_ROTATION_STOP_TOLERANCE_ANGLE * KINETIC_ROTATION_STOP_TOLERANCE_ANGLE) {
          stopKineticRotation();
        } else {
          if (constraints.isRotatable()) {
            updateRotation(rotationDeg + kineticRotationInertia);
          }

          kineticRotationInertia *= KINETIC_ROTATION_SLOWDOWN;
        }
      } finally {
        generalLock.unlock();
      }
    }
  }

  protected void setupRenderState(GL10 gl) {
    generalLock.lock();
    try {
      if (matricesChanged) {
        matricesChanged = false;

        recalculateCameraState();
        
        recalculateContour();

        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadMatrixf(projectionMatrix, 0);

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadMatrixf(localMVMatrix, 0);

        updateLabelPos = true;
      }

      MapListener mapListener = options.getMapListener();
      if (mapListener != null) {
        mapListener.onDrawFrame(gl);
      }
    } finally {
      generalLock.unlock();
    }
  }

  protected LayerRenderer createLayerRenderer(Layer layer) {
    if (layer instanceof RasterLayer) {
      return new RasterLayerRenderer((RasterLayer) layer, renderProjection, components.textureMemoryCache, options);
    } else if (layer instanceof GeometryLayer) {
      return new GeometryLayerRenderer((GeometryLayer) layer, renderProjection, styleCache, true);
    } else if (layer instanceof MarkerLayer) {
      return new MarkerLayerRenderer((MarkerLayer) layer, renderProjection, bitmapCache);
    } else if (layer instanceof TextLayer) {
      return new TextLayerRenderer((TextLayer) layer, renderProjection);
    } else if (layer instanceof Polygon3DLayer) {
      return new Polygon3DLayerRenderer((Polygon3DLayer) layer, renderProjection);
    } else if (layer instanceof NMLModelLayer) {
      return new NMLModelLayerRenderer((NMLModelLayer) layer, renderProjection);
    } else if (layer instanceof VectorTileLayer) {
      return new VectorTileLayerRenderer((VectorTileLayer) layer, renderProjection, styleCache, bitmapCache);
    } else {
      Log.debug("MapRenderer3D: unknown layer type " + layer.getClass().getCanonicalName());
      return null;
    }
  }

  protected void synchronizeLayerRenderers(GL10 gl) {
    if (resetLayerRenderers) {
      for (Iterator<Layer> it = layerRenderers.keySet().iterator(); it.hasNext();) {
        Layer layer = it.next();
        LayerRenderer renderer = layerRenderers.get(layer);
        renderer.dispose(gl);
        it.remove();
      }
      resetLayerRenderers = false;
    }

    List<Layer> newLayers = layers.getAllLayers();
    for (Layer layer : newLayers) {
      LayerRenderer renderer = layerRenderers.get(layer);
      if (renderer == null) {
        renderer = createLayerRenderer(layer);
        if (renderer == null) {
          continue;
        }
        layerRenderers.put(layer, renderer);
      }
      try {
        renderer.synchronize(gl);
      } catch (Throwable t) {
        Log.error("MapRenderer3D: exception while synchronizing " + layer.getClass().getSimpleName() + ": " + t.getMessage());
        layerRenderers.remove(renderer);
      }
    }
    for (Iterator<Layer> it = layerRenderers.keySet().iterator(); it.hasNext();) {
      Layer layer = it.next();
      if (!newLayers.contains(layer)) {
        LayerRenderer renderer = layerRenderers.get(layer);
        renderer.dispose(gl);
        it.remove();
      }
    }
    for (LayerRenderer renderer : layerRenderers.values()) {
      if (!renderer.isSynchronized()) {
        requestRenderView();
        break;
      }
    }
  }

  protected void detectClick(GL10 gl) {
    if (detectClick || detectLongClick) {
      // Regular click (true) or long click (false)
      boolean longClick = detectLongClick;
      detectClick = false;
      detectLongClick = false;

      MapListener mapListener = options.getMapListener();

      // First check for label click
      if (rSelectedVectorElement != null && labelPos != null && labelTextureInfo != null) {
        if (clickPos2D.x >= labelPos.x - labelTextureInfo.width * 0.5f
            && clickPos2D.y >= labelPos.y
            && clickPos2D.x <= labelPos.x + labelTextureInfo.width * 0.5f
            && clickPos2D.y <= labelPos.y + labelTextureInfo.height) {
          if (mapListener != null) {
            mapListener.onLabelClickedInternal(rSelectedVectorElement, longClick);
          }
          return;
        }
      }

      // Draw all clickable layers with special colors
      pickingState.clear(gl);
      drawLayersPicking(gl);

      // Find the clicked vector element
      generalLock.lock(); // resolve may result in recursive layer drawing
      try {
        selectedVectorElement = rSelectedVectorElement = pickingState.resolveElement(gl, (int) clickPos2D.x, (int) clickPos2D.y);
      } finally {
        generalLock.unlock();
      }

      Point3D clickPoint = screenToWorld(clickPos2D.x, height - clickPos2D.y, false);

      // If no element was selected, send click event to listener
      if (rSelectedVectorElement == null) {
        if (mapListener != null) {
          if (clickPoint != null) {
            MapPos clickPosInternal = renderProjection.unproject(clickPoint);
            MapPos clickPos = layers.getBaseProjection().fromInternal(clickPosInternal);
            mapListener.onMapClickedInternal(clickPos.x, clickPos.y, longClick);
          }
        }
        requestRenderView();
        return;
      }

      // Update click point coordinates, force it to be on the element
      this.clickPoint = clickPoint = rSelectedVectorElement.calculateInternalClickPos(clickPoint);

      // Find out if label should be shown, send click event.
      // If there's no MapListener then show labels only for normal clicks
      Label label = rSelectedVectorElement.getLabel();
      boolean showLabel = !longClick;
      if (mapListener != null) {
        if (label != null) {
          showLabel = mapListener.showLabelOnVectorElementClick(rSelectedVectorElement, longClick);
        }
        if (clickPoint != null) {
          MapPos clickPosInternal = renderProjection.unproject(clickPoint);
          MapPos clickPos = layers.getBaseProjection().fromInternal(clickPosInternal);
          mapListener.onVectorElementClickedInternal(rSelectedVectorElement, clickPos.x, clickPos.y, longClick);
        }
      }

      // Show the label if possible and the MapListener allows it
      labelPos = null;
      labelTextureInfo = null;
      labelTextureAlpha = 1.0f;
      if (showLabel && label != null) {
        updateLabelPos = true;
        reloadLabelTexture = true;
      }
      requestRenderView();
    }
  }

  protected void drawBackground(GL10 gl) {
    backgroundRenderer.draw(gl, cameraState, (int) width, (int) height);
  }

  protected void drawDepth(GL10 gl) {
    if (depthVertBuf == null) {
      return;
    }

    gl.glColorMask(false, false, false, false);
    gl.glDepthMask(true);
    gl.glEnable(GL10.GL_DEPTH_TEST);

    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, depthVertBuf);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glDrawArrays(GL10.GL_TRIANGLES, 0, depthVertBuf.capacity() / 3);

    gl.glColorMask(true, true, true, true);
  }

  protected void drawLayers(GL10 gl) {
    List<Layer> allLayers = layers.getAllLayers();

    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

    // Draw 2D layers first, do not write to Z buffer. For compatibility and device-specific issues, disable depth test for now
    gl.glDepthMask(false);
    gl.glEnable(GL10.GL_DEPTH_TEST);

    for (Layer layer : allLayers) {
      LayerRenderer renderer = layerRenderers.get(layer);
      if (renderer == null) {
        continue;
      }
      if (!(layer.isVisible() && layer.isInVisibleZoomRange(cameraState.zoom))) {
        continue;
      }
      try {
        renderer.draw(gl, cameraState, LayerRenderer.Pass.BASE);
      } catch (Throwable t) {
        Log.error("MapRenderer3D: exception while drawing " + layer.getClass().getSimpleName() + ": " + t.getMessage());
        layerRenderers.remove(renderer);
      }
    }

    // Listener callback
    MapListener mapListener = options.getMapListener();
    if (mapListener != null) {
      // TODO: this code is for legacy compatibility; we should add "MapPos origin" argument to the event for better precision
      setupMapListenerGLState(gl);
      gl.glPushMatrix();
      gl.glLoadMatrixf(modelviewMatrix, 0);
      mapListener.onDrawFrameBefore3D(gl, cameraState.zoomPow2);
      gl.glPopMatrix();
      setupGLState(gl);
    }

    // Draw 3D layers, write to Z buffer
    gl.glDepthMask(true);
    gl.glEnable(GL10.GL_DEPTH_TEST);

    for (Layer layer : allLayers) {
      LayerRenderer renderer = layerRenderers.get(layer);
      if (renderer == null) {
        continue;
      }
      if (!(layer.isVisible() && layer.isInVisibleZoomRange(cameraState.zoom))) {
        continue;
      }
      try {
        renderer.draw(gl, cameraState, LayerRenderer.Pass.OVERLAY);
      } catch (Throwable t) {
        Log.error("MapRenderer3D: exception while drawing " + layer.getClass().getSimpleName() + ": " + t.getMessage());
        layerRenderers.remove(renderer);
      }
    }

    // Listener callback
    if (mapListener != null) {
      // TODO: this code is for legacy compatibility; we should add "MapPos origin" argument to the event for better precision
      setupMapListenerGLState(gl);
      gl.glPushMatrix();
      gl.glLoadMatrixf(modelviewMatrix, 0);
      mapListener.onDrawFrameAfter3D(gl, cameraState.zoomPow2);
      gl.glPopMatrix();
      setupGLState(gl);
    }

    gl.glDisable(GL10.GL_DEPTH_TEST);
  }

  protected void drawLayersPicking(GL10 gl) {
    List<Layer> allLayers = layers.getAllLayers();

    gl.glDisable(GL10.GL_TEXTURE_2D);
    gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
    gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

    // Draw 2D layers first
    gl.glDepthMask(false);
    gl.glEnable(GL10.GL_DEPTH_TEST);

    for (Layer layer : allLayers) {
      LayerRenderer renderer = layerRenderers.get(layer);
      if (renderer == null) {
        continue;
      }
      if (!(layer.isVisible() && layer.isInVisibleZoomRange(cameraState.zoom))) {
        continue;
      }
      if (renderer instanceof VectorLayerRenderer) {
        try {
          ((VectorLayerRenderer) renderer).drawPicking(gl, cameraState, pickingState, LayerRenderer.Pass.BASE);
        } catch (Throwable t) {
          Log.error("MapRenderer3D: exception while drawing (picking) " + layer.getClass().getSimpleName() + ": " + t.getMessage());
          layerRenderers.remove(renderer);
        }
      }
    }

    // Draw 3D layers
    gl.glDepthMask(true);
    gl.glEnable(GL10.GL_DEPTH_TEST);

    for (Layer layer : allLayers) {
      LayerRenderer renderer = layerRenderers.get(layer);
      if (renderer == null) {
        continue;
      }
      if (!(layer.isVisible() && layer.isInVisibleZoomRange(cameraState.zoom))) {
        continue;
      }
      if (renderer instanceof VectorLayerRenderer) {
        try {
          ((VectorLayerRenderer) renderer).drawPicking(gl, cameraState, pickingState, LayerRenderer.Pass.OVERLAY);
        } catch (Throwable t) {
          Log.error("MapRenderer3D: exception while drawing (picking) " + layer.getClass().getSimpleName() + ": " + t.getMessage());
          layerRenderers.remove(renderer);
        }
      }
    }

    gl.glDisable(GL10.GL_DEPTH_TEST);
  }

  protected void drawLabel(GL10 gl) {
    if (rSelectedVectorElement != null && labelPos != null && labelTextureInfo != null && labelTexture != 0) {
      if (rSelectedVectorElement.getInternalState().activeStyle == null) {
        return;
      }

      gl.glColor4f(labelTextureAlpha, labelTextureAlpha, labelTextureAlpha, labelTextureAlpha);
      gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);

      // Change projection to ortho
      gl.glMatrixMode(GL10.GL_PROJECTION);
      gl.glPushMatrix();
      gl.glLoadMatrixf(orthoProjectionMatrix, 0);
      gl.glMatrixMode(GL10.GL_MODELVIEW);
      gl.glPushMatrix();
      gl.glLoadIdentity();
      gl.glTranslatef(0, 0, -2);

      gl.glTranslatef((int) labelPos.x, (int) (labelPos.y + labelTextureInfo.height * 0.5f), -1);
      gl.glScalef(labelTextureInfo.width, labelTextureInfo.height, 1);
      GLUtils.drawTexturedQuad(gl, labelTexture, labelTextureInfo.texCoordBuf);

      gl.glPopMatrix();
      gl.glMatrixMode(GL10.GL_PROJECTION);
      gl.glPopMatrix();
      gl.glMatrixMode(GL10.GL_MODELVIEW);
    }
  }

  protected void drawWatermark(GL10 gl) {
    // Render license watermark
    float scale = (float) Math.sqrt(Utils.toRange(densityDpi / DisplayMetrics.DENSITY_HIGH, 0.25f, 1.0f));
    LicenseManager licenseManager = LicenseManager.getInstance();
    if (licenseManager != null) {
      for (int i = 0; i < licenseManager.getWatermarks().size(); i++) {
        Watermark watermark = licenseManager.getWatermarks().get(i);
        while (watermarkRenderers.size() <= i) {
          watermarkRenderers.add(new OverlayRenderer());
        }
        watermarkRenderers.get(i).draw(gl, watermark.getBitmap(), watermark.xPos, watermark.yPos, watermark.scale * scale, aspect);
      }
    } else {
      Watermark watermark = defaultWatermark;
      while (watermarkRenderers.size() <= 0) {
        watermarkRenderers.add(new OverlayRenderer());
      }
      watermarkRenderers.get(0).draw(gl, watermark.getBitmap(), watermark.xPos, watermark.yPos, watermark.scale * scale, aspect);
    }

    // Optionally render FPS indicator
    if (options.isFPSIndicator()) {
      long fps = (lastFrameTime > 0 ? 1000 / lastFrameTime : 999);
      String text = " " + (fps > FPS_MAX ? ">" + FPS_MAX : fps) + " ";

      Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
      paint.setTextAlign(Align.RIGHT);
      Typeface font = Typeface.create("Arial", Typeface.NORMAL);
      paint.setTypeface(font);
      paint.setTextSize(40);
      FontMetrics fontMetrics = paint.getFontMetrics();

      int textWidth = (int) paint.measureText(" 999 ");
      int textHeight = (int) (-fontMetrics.ascent + fontMetrics.descent);
      Bitmap bitmap = Bitmap.createBitmap(textWidth, textHeight, Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(bitmap);

      paint.setColor(Color.BLACK);
      paint.setStyle(android.graphics.Paint.Style.STROKE);
      paint.setStrokeWidth(2);
      canvas.drawText(text, textWidth, -fontMetrics.ascent, paint);
      paint.setColor(fps >= 30 ? Color.GREEN : Color.RED);
      paint.setStyle(android.graphics.Paint.Style.FILL);
      canvas.drawText(text, textWidth, -fontMetrics.ascent, paint);

      fpsRenderer.draw(gl, bitmap, 1, 1, FPS_TEXT_SCALE, aspect);

      bitmap.recycle();
    }
  }

  @Override
  public Point3D screenToWorld(double x, double y, boolean closest) {
    return screenToWorld(x, y, (int) width, (int) height, closest);
  }

  @Override
  public Point3D screenToWorld(double x, double y, int width, int height, boolean closest) {
    int[] viewParams = new int[] { 0, 0, width, height };
    float[] projectionMatrix = getProjectionMatrix(width, height);

    generalLock.lock();
    try {
      double[] viewMatrix = new double[16];
      Matrix.setLookAtM(viewMatrix, cameraPos.x, cameraPos.y, cameraPos.z, focusPoint.x, focusPoint.y, focusPoint.z, upVector.x, upVector.y, upVector.z);

      double[] objPos0 = new double[3];
      Matrix.unProject(x, height - y, 0, viewMatrix, 0, projectionMatrix, 0, viewParams, objPos0, 0);
      double[] objPos1 = new double[3];
      Matrix.unProject(x, height - y, 1, viewMatrix, 0, projectionMatrix, 0, viewParams, objPos1, 0);

      Point3D rayOrigin = new Point3D(objPos0[0], objPos0[1], objPos0[2]);
      Vector3D rayDir = new Vector3D(objPos1[0] - objPos0[0], objPos1[1] - objPos0[1], objPos1[2] - objPos0[2]);
      double[] ts = renderSurface.calculateIntersections(rayOrigin, rayDir, closest);
      for (double t : ts) {
        double dist = Vector3D.dotProduct(new Vector3D(cameraPos, focusPoint).getNormalized(), rayDir);
        if (!closest && (t < 0 || t * dist > far)) {
          continue;
        }
        Point3D point = new Point3D(rayOrigin.x + t * rayDir.x, rayOrigin.y + t * rayDir.y, rayOrigin.z + t * rayDir.z);
        return point;
      }
      return null;
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public MapPos worldToScreen(double x, double y, double z) {
    return worldToScreen(x, y, z, (int) width, (int) height);
  }

  @Override
  public MapPos worldToScreen(double x, double y, double z, int width, int height) {
    int[] viewParams = new int[] { 0, 0, width, height };
    float[] projectionMatrix = getProjectionMatrix(width, height);

    generalLock.lock();
    try {
      double[] viewMatrix = new double[16];
      Matrix.setLookAtM(viewMatrix, cameraPos.x, cameraPos.y, cameraPos.z, focusPoint.x, focusPoint.y, focusPoint.z, upVector.x, upVector.y, upVector.z);

      double[] screenCoords = new double[3];
      Matrix.project(x, y, z, viewMatrix, 0, projectionMatrix, 0, viewParams, 0, screenCoords, 0);
      return new MapPos(screenCoords[0], height - screenCoords[1]);
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public MapPos mapTileToInternal(MapTile mapTile, MapPos tilePos) {
    Projection baseProjection = layers.getBaseProjection();
    MapTileGenerator tileGen = new MapTileGenerator(baseProjection, baseProjection, 0, Const.MAX_SUPPORTED_ZOOM_LEVEL, renderSurface, false, options);
    Bounds tileBounds = tileGen.getRasterTileBounds(mapTile.x, mapTile.y, mapTile.zoom);
    MapPos mapPos = new MapPos(tileBounds.left + tilePos.x * tileBounds.getWidth(), tileBounds.bottom + tilePos.y * tileBounds.getHeight());
    return baseProjection.toInternal(mapPos);
  }

  @Override
  public MapTile internalToMapTile(double x, double y, int zoom, MutableMapPos tilePos) {
    Projection baseProjection = layers.getBaseProjection();
    MapTileGenerator tileGen = new MapTileGenerator(baseProjection, baseProjection, 0, Const.MAX_SUPPORTED_ZOOM_LEVEL, renderSurface, false, options);
    MapPos mapPos = baseProjection.fromInternal(x, y);
    MapTile mapTile = tileGen.findMapTile(getCameraState(), mapPos, zoom);
    if (mapTile != null && tilePos != null) {
      Bounds tileBounds = tileGen.getRasterTileBounds(mapTile.x, mapTile.y, mapTile.zoom);
      tilePos.setCoords((x - tileBounds.left) / tileBounds.getWidth(), (y - tileBounds.bottom) / tileBounds.getHeight());
    }
    return mapTile;
  }

  private void recalculateBestZoom0Distance() {
    // When the height or the tileSize changes, recalculate the optimal distance for the first tile
    generalLock.lock();
    try {
      bestZoom0Distance = renderSurface.getBestZoom0Distance(width, height, options.getTileSize());
    } finally {
      generalLock.unlock();
    }
  }

  private void recalculateAngles(boolean calcRotation, boolean calcTilt, boolean calcZoom) {
    generalLock.lock();
    try {
      Vector3D cameraVec = new Vector3D(cameraPos, focusPoint);

      float newRotationDeg = rotationDeg;
      if (calcRotation) {
        double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(cameraPos);

        // Check that frame matrix is 'precise', otherwise we could end up with NaN
        Vector3D xAxis = new Vector3D(localFrameMatrix[0], localFrameMatrix[1], localFrameMatrix[2]);
        Vector3D yAxis = new Vector3D(localFrameMatrix[4], localFrameMatrix[5], localFrameMatrix[6]);
        if (xAxis.getLength() * yAxis.getLength() > 1.0 - 1.0e-4 && xAxis.getLength() * yAxis.getLength() < 1.0 + 1.0e-4) {
          double dotX = Vector3D.dotProduct(xAxis, upVector);
          double dotY = Vector3D.dotProduct(yAxis, upVector);
          newRotationDeg = (float) Math.atan2(dotY, dotX) * Const.RAD_TO_DEG - 90;
          if (newRotationDeg < 0) {
            newRotationDeg += 360;
          }
        }
      }

      float newTiltDeg = tiltDeg;
      if (calcTilt) {
        double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(focusPoint);

        // Check that local z axis is 'precise' and not NaN, otherwise we could corrupt tilt
        Vector3D zAxis = new Vector3D(localFrameMatrix[8], localFrameMatrix[9], localFrameMatrix[10]);
        if (zAxis.getLength() > 1.0 - 1.0e-6 && zAxis.getLength() < 1.0 + 1.0e-6) {
          double dotZ = Vector3D.dotProduct(zAxis, cameraVec) / cameraVec.getLength();
          double dotY = Vector3D.dotProduct(zAxis, upVector);
          newTiltDeg = 90 - (float) Math.atan2(-dotZ, dotY) * Const.RAD_TO_DEG;
          newTiltDeg = Math.max(newTiltDeg, 0);
        }
      }

      float newZoom = zoom;
      float newZoomPow2 = zoomPow2;
      if (calcZoom) {
        double distance = cameraVec.getLength();
        newZoom = ((float) Math.log(distance / bestZoom0Distance) / Const.LOG_E_05);
        newZoom = Math.max(newZoom, 0);
        newZoomPow2 = (float) Math.pow(2, newZoom);
      }

      if (calcRotation) {
        rotationDeg = newRotationDeg;
      }
      if (calcTilt) {
        tiltDeg = newTiltDeg;
      }
      if (calcZoom) {
        zoom = newZoom;
        zoomPow2 = newZoomPow2;
      }
    } finally {
      generalLock.unlock();
    }
  }
  
  private void recalculateCameraVectors() {
    generalLock.lock();
    try {
      Vector3D cameraVec = new Vector3D(cameraPos, focusPoint);
      double distance = cameraVec.getLength();

      double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(focusPoint);
      Vector3D xAxis = new Vector3D(localFrameMatrix[0], localFrameMatrix[1], localFrameMatrix[2]);
      Vector3D yAxis = new Vector3D(localFrameMatrix[4], localFrameMatrix[5], localFrameMatrix[6]);
      Vector3D zAxis = new Vector3D(localFrameMatrix[8], localFrameMatrix[9], localFrameMatrix[10]);
      if (!(xAxis.getLength() * yAxis.getLength() > 1.0 - 1.0e-4 && xAxis.getLength() * yAxis.getLength() < 1.0 + 1.0e-4)) {
        xAxis = new Vector3D(1, 0, 0);
        yAxis = new Vector3D(0, 1, 0);
      }

      double[] rotationTransform = new double[16];
      Matrix.setRotationM(rotationTransform, -zAxis.x, -zAxis.y, -zAxis.z, rotationDeg);
      xAxis = GeomUtils.transform(xAxis, rotationTransform);
      yAxis = GeomUtils.transform(yAxis, rotationTransform);
      zAxis = GeomUtils.transform(zAxis, rotationTransform); // identity transform
      
      double[] tiltTransform = new double[16];
      Matrix.setRotationM(tiltTransform, -xAxis.x, -xAxis.y, -xAxis.z, tiltDeg);
      xAxis = GeomUtils.transform(xAxis, tiltTransform); // identity transform
      yAxis = GeomUtils.transform(yAxis, tiltTransform);
      zAxis = GeomUtils.transform(zAxis, tiltTransform);
      
      upVector = new Vector3D(yAxis);
      cameraPos = new Point3D(focusPoint.x + zAxis.x * distance, focusPoint.y + zAxis.y * distance, focusPoint.z + zAxis.z * distance);
    } finally {
      generalLock.unlock();
    }
    
  }

  private void recalculateLabelPos(VectorElement vectorElement) {
    if (vectorElement == null) {
      labelPos = null;
      return;
    }

    if (vectorElement.getInternalState().activeStyle == null) {
      labelPos = null;
      return;
    }

    LayerRenderer layerRenderer = layerRenderers.get(vectorElement.getLayer());
    if (!(layerRenderer instanceof VectorLayerRenderer)) {
      labelPos = null;
      return;
    }

    // Get label position and check that the point is not back-facing
    Point3D labelPoint = ((VectorLayerRenderer) layerRenderer).calculateElementLabelPos(vectorElement, cameraState, clickPoint);
    if (labelPoint != null) {
      Vector3D normal = renderProjection.getNormal(labelPoint);
      double dotZ = normal.x * (labelPoint.x - cameraState.cameraPos.x) + normal.y * (labelPoint.y - cameraState.cameraPos.y) + normal.z * (labelPoint.z - cameraState.cameraPos.z);
      if (dotZ >= 0) {
        labelPoint = null;
      }
    }

    generalLock.lock();
    try {
      if (labelPoint == null) {
        labelPos = null;
      } else {
        // Check if label position is between near and far plane
        int[] viewParams = new int[] { 0, 0, (int) width, (int) height };
        double[] screenCoords = new double[3];
        Matrix.project(labelPoint.x, labelPoint.y, labelPoint.z, modelviewMatrixDbl, 0, projectionMatrix, 0, viewParams, 0, screenCoords, 0);
        if (screenCoords[2] >= 0 && screenCoords[2] < 1) {
          labelPos = new MapPos(screenCoords[0], screenCoords[1]);
        } else {
          labelPos = null;
        }
      }
    } finally {
      generalLock.unlock();
    }
  }
  
  private void recalculateCameraState() {
    // Recalculate modelview matrix
    Matrix.setLookAtM(modelviewMatrixDbl, cameraPos.x, cameraPos.y, cameraPos.z, focusPoint.x, focusPoint.y, focusPoint.z, upVector.x, upVector.y, upVector.z);
    Matrix.doubleToFloatM(modelviewMatrix, 0, modelviewMatrixDbl, 0);
    Matrix.copyM(localMVMatrix, 0, modelviewMatrix, 0);
    localMVMatrix[12] = 0;
    localMVMatrix[13] = 0;
    localMVMatrix[14] = 0;

    // Recalculate near/far distance values, etc
    near = getNearPlane();
    far = getFarPlane();

    // Recalculate projection matrix
    double top = near * Const.HALF_FOV_TAN_Y;
    double bottom = -top;
    double left = bottom * aspect;
    double right = top * aspect; 
    
    Vector focusPointOffset = this.focusPointOffset;
    if (focusPointOffset != null) {
      double dx = 2 * near * Const.HALF_FOV_TAN_Y * focusPointOffset.x / height;
      double dy = 2 * near * Const.HALF_FOV_TAN_Y * focusPointOffset.y / height;

      top += dy;
      bottom += dy;
      left += dx;
      right += dx;
    }
    
    // Workaround for Android VM issue - KitKat/BuildTools 21.1.1
    //Matrix.frustumReinitializeM(projectionMatrix, left, right, bottom, top, near, far);
    float[] m = projectionMatrix;
    final double r_width = 1.0 / (right - left);
    final double r_height = 1.0 / (top - bottom);
    final double r_depth = 1.0 / (near - far);
    m[0] = (float) (2.0 * (near * r_width));
    m[5] = (float) (2.0 * (near * r_height));
    m[8] = (float) ((right + left) * r_width);
    m[9] = (float) ((top + bottom) * r_height);
    m[10] = (float) ((far + near) * r_depth);
    m[14] = (float) (2.0 * (far * near * r_depth));
    
    // Update camera state
    cameraState = new CameraState(cameraPos, focusPoint, upVector, modelviewMatrixDbl, projectionMatrix, rotationDeg, tiltDeg, zoom, near, far);
  }
  
  private void recalculateContour() {
    Point3D cameraPos = cameraState.cameraPos;
    Point3D[] contour = renderSurface.getContourVertices(cameraPos);
    if (contour.length < 1) {
      depthVertBuf = null;
      return;
    }

    double cx = 0, cy = 0, cz = 0;
    for (Point3D p : contour) {
      cx += p.x;
      cy += p.y;
      cz += p.z;
    }
    MapPos p0 = new MapPos(cx / contour.length, cy / contour.length, cz / contour.length);
    
    FloatVertexBuffer vertexBuffer = new FloatVertexBuffer();
    for (int i = 0; i < contour.length; i++) {
      Point3D p2 = contour[i];
      Point3D p1 = contour[(i + 1) % contour.length];
      vertexBuffer.add((float) (p0.x - cameraPos.x), (float) (p0.y - cameraPos.y), (float) (p0.z - cameraPos.z));
      vertexBuffer.add((float) (p1.x - cameraPos.x), (float) (p1.y - cameraPos.y), (float) (p1.z - cameraPos.z));
      vertexBuffer.add((float) (p2.x - cameraPos.x), (float) (p2.y - cameraPos.y), (float) (p2.z - cameraPos.z));
    }
    
    depthVertBuf = vertexBuffer.build(0, vertexBuffer.size());
  }

  protected void loadLabelTexture(GL10 gl, Bitmap labelBitmap) {
    GLUtils.deleteTexture(gl, labelTexture);
    labelTexture = GLUtils.buildTexture(gl, labelBitmap);

    gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
    gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

    checkGLError(gl);
  }

  @Override
  public GLSurfaceView getView() {
    return view;
  }

  @Override
  public void setView(GLSurfaceView view) {
    this.view = view;
  }

  @Override
  public void requestRenderView() {
    if (redrawPending) {
      return;
    }
    GLSurfaceView view = getView(); // make local copy - view can be changed in other threads
    if (view != null) {
      redrawPending = true;
      if (view.getHandler() != null) {
        view.requestRender();
      }
    }
  }

  @Override
  public void click(float x, float y) {
    clickPos2D.setCoords(x, height - y);
    detectClick = true;
    requestRenderView(); // click handling is really done in onDraw, so request redraw ASAP
  }

  @Override
  public void longClick(float x, float y) {
    clickPos2D.setCoords(x, height - y);
    detectLongClick = true;
    requestRenderView(); // click handling is really done in onDraw, so request redraw ASAP
  }

  @Override
  public Point3D getCameraPos() {
    return cameraPos;
  }

  @Override
  public Point3D getFocusPoint() {
    return focusPoint;
  }

  @Override
  public Vector3D getUpVector() {
    return upVector;
  }

  @Override
  public CameraState getCameraState() {
    generalLock.lock();
    try {
      return new CameraState(cameraPos, focusPoint, upVector, modelviewMatrixDbl, projectionMatrix, rotationDeg, tiltDeg, zoom, near, far);
    } finally {
      generalLock.unlock();
    }
  }
  
  @Override
  public RenderSurface getRenderSurface() {
    return renderSurface;
  }
  
  @Override
  public void setRenderSurface(RenderSurface renderSurface) {
    // Shut down background threads
    boolean started = isMappingStarted();
    if (started) {
      onStopMapping();
    }
    
    // Remember selected vector element and deselect it during switch time
    VectorElement selectedElement = getSelectedVectorElement();
    deselectVectorElement();

    // Stop panning and animations
    stopKineticPanning();
    stopKineticRotation();
    animationQueue.flush();

    // Perform switch
    generalLock.lock();
    try {
      MapPos focusPointInternal = renderProjection.unproject(focusPoint);
      MapPos cameraPosInternal = renderProjection.unproject(cameraPos);
      float zoom = getZoom();
      float tilt = getTilt();
      float rotation = getRotation();

      this.renderSurface = renderSurface;
      this.renderProjection = renderSurface.getRenderProjection();
      this.resetLayerRenderers = true;

      backgroundRenderer = new BackgroundRenderer(this, layers.getBaseProjection(), renderSurface, options);
      backgroundRenderer.setSkyBitmap(skyBitmap);
      backgroundRenderer.setBackgroundPlaneBitmap(backgroundPlaneBitmap);
      backgroundRenderer.setBackgroundPlaneOverlayBitmap(backgroundPlaneOverlayBitmap);
      backgroundRenderer.setBackgroundImageBitmap(backgroundImageBitmap);

      focusPoint = renderProjection.project(focusPointInternal);
      cameraPos = renderProjection.project(cameraPosInternal);
      updateFocusPoint(focusPoint.x, focusPoint.y, focusPoint.z, true);
      updateZoom(zoom);
      updateTilt(tilt);
      updateRotation(rotation);
      
      recalculateCameraState();
    } finally {
      generalLock.unlock();
    }

    // Update internal state of elements of all layers (may depend on renderprojection)
    for (Layer layer : layers.getAllLayers()) {
      layer.setRenderProjection(renderProjection);
    }

    // Restore selection
    selectVectorElement(selectedElement);

    // Restart
    if (started) {
      onStartMapping();
    }
  }

  @Override
  public double[] getModelviewMatrix() {
    generalLock.lock();
    try {
      return modelviewMatrixDbl.clone();
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public float[] getProjectionMatrix() {
    generalLock.lock();
    try {
      return projectionMatrix.clone();
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public double[] getModelviewProjectionMatrix() {
    generalLock.lock();
    try {
      double[] projectionMatrixDbl = new double[16];
      Matrix.floatToDoubleM(projectionMatrixDbl, 0, projectionMatrix, 0);
      double[] mvpMatrix = new double[16];
      Matrix.multiplyMM(mvpMatrix, 0, projectionMatrixDbl, 0, modelviewMatrixDbl, 0);
      return mvpMatrix;
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public float getBestZoom0Distance() {
    return bestZoom0Distance;
  }

  @Override
  public float getAspectRatio() {
    return aspect;
  }

  @Override
  public float getNearPlane() {
    return renderSurface.getNearPlane(cameraPos, focusPoint, tiltDeg);
  }

  @Override
  public float getFarPlane() {
    Vector focusPointOffset = this.focusPointOffset;
    if (focusPointOffset == null) {
      return renderSurface.getFarPlane(cameraPos, focusPoint, tiltDeg, options.getDrawDistance());
    }
    float delta = 2 * (float) Math.max(0, focusPointOffset.y) / height;
    float extraTilt = (float) Math.atan2(delta, 1) * Const.RAD_TO_DEG;
    return renderSurface.getFarPlane(cameraPos, focusPoint, tiltDeg + extraTilt, options.getDrawDistance());
  }
  
  private float[] getProjectionMatrix(int width, int height) {
    generalLock.lock();
    try {
      float near = getNearPlane();
      float far = getFarPlane();
      float aspect = (float) width / (float) height;

      double top = near * Const.HALF_FOV_TAN_Y;
      double bottom = -top;
      double left = bottom * aspect;
      double right = top * aspect;
      
      Vector focusPointOffset = this.focusPointOffset;
      if (focusPointOffset != null) {
        double dx = 2 * near * Const.HALF_FOV_TAN_Y * focusPointOffset.x / height;
        double dy = 2 * near * Const.HALF_FOV_TAN_Y * focusPointOffset.y / height;

        top += dy;
        bottom += dy;
        left += dx;
        right += dx;
      }

      float[] projectionMatrix = new float[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0 }; 

      // Workaround for Android VM issue - KitKat/BuildTools 21.1.1
      //Matrix.frustumReinitializeM(projectionMatrix, left, right, bottom, top, near, far);
      float[] m = projectionMatrix;
      final double r_width = 1.0 / (right - left);
      final double r_height = 1.0 / (top - bottom);
      final double r_depth = 1.0 / (near - far);
      m[0] = (float) (2.0 * (near * r_width));
      m[5] = (float) (2.0 * (near * r_height));
      m[8] = (float) ((right + left) * r_width);
      m[9] = (float) ((top + bottom) * r_height);
      m[10] = (float) ((far + near) * r_depth);
      m[14] = (float) (2.0 * (far * near * r_depth));

      return projectionMatrix;
    } finally {
      generalLock.unlock();
    }
  }
  
  private void applyMovementConstraints() {
    // Wrap focus point
    Point3D focusPointMoved = renderSurface.getWrappedPoint(focusPoint);
    
    // Constrain to bounds
    Bounds bounds = constraints.getMapBounds();
    Projection baseProjection = layers.getBaseProjection();
    if (bounds != null) {
      MapPos mapPosInternal = renderProjection.unproject(focusPoint);
      MapPos mapPos = baseProjection.fromInternal(mapPosInternal);
      double x = Utils.toRange(mapPos.x, bounds.left, bounds.right);
      double y = Utils.toRange(mapPos.y, bounds.bottom, bounds.top);
      MapPos mapPosInternalConstrained = baseProjection.toInternal(x, y);
      focusPointMoved = renderProjection.project(mapPosInternalConstrained);
    }

    // Update if constraints were applied
    if (!focusPointMoved.equals(focusPoint)) {
      double[] transform = renderProjection.getTranslateMatrix(focusPoint, focusPointMoved, 1);
      focusPoint = GeomUtils.transform(focusPoint, transform);
      cameraPos = GeomUtils.transform(cameraPos, transform);
      upVector = GeomUtils.transform(upVector, transform);
    }
  }
  
  @Override
  public Vector getFocusPointOffset() {
    return focusPointOffset;
  }
  
  @Override
  public void setFocusPointOffset(float x, float y) {
    generalLock.lock();
    try {
      if (x == 0 && y == 0) {
        focusPointOffset = null;
      } else {
        focusPointOffset = new Vector(-x, y);
      }
      frustumChanged();
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public void setLookAtParams(double camX, double camY, double camZ,
      double focusX, double focusY, double focusZ,
      double upX, double upY, double upZ,
      boolean calcRotation, boolean calcTilt, boolean calcZoom) {
    animationQueue.remove(AnimationQueue.MOVE_ANIMATION);
    animationQueue.remove(AnimationQueue.PAN_ANIMATION);
    if (calcRotation) {
      animationQueue.remove(AnimationQueue.ROTATE_ANIMATION);
    }
    if (calcTilt) {
      animationQueue.remove(AnimationQueue.TILT_ANIMATION);
    }
    if (calcZoom) {
      animationQueue.remove(AnimationQueue.ZOOM_ANIMATION);
    }

    Point3D focusPointTarget = renderSurface.getGroundPoint(new Point3D(focusX, focusY, focusZ));

    generalLock.lock();
    try {
      float dPos = (float) renderProjection.getDistance(focusPoint, focusPointTarget);
      dPos = Math.min(dPos, 2 * Const.UNIT_SIZE - dPos) * KINETIC_PAN_INITIAL_FACTOR;
      kineticPanInertia *= KINETIC_PAN_DAMPING;
      if (dPos > kineticPanInertia * KINETIC_PAN_MIN_THRESHOLD) {
        kineticPanInertia = dPos;
        kineticPanOrigin = focusPoint;
        kineticPanTarget = focusPointTarget;
      }
      if (kineticPanInertia * kineticPanInertia < KINETIC_PAN_START_TOLERANCE_ZOOM_0 / (zoomPow2 * zoomPow2)) {
        stopKineticPanning();
      }

      cameraPos = new Point3D(camX, camY, camZ);
      focusPoint = new Point3D(focusPointTarget);
      upVector = new Vector3D(upX, upY, upZ).getNormalized();

      float oldRotationDeg = rotationDeg;
      recalculateAngles(calcRotation, calcTilt, calcZoom);
      float dAngle = rotationDeg % 360 - oldRotationDeg % 360;
      if (dAngle > 180) {
        dAngle -= 360;
      } else if (dAngle < -180) {
        dAngle += 360;
      }
      dAngle *= KINETIC_ROTATION_INITIAL_FACTOR;
      kineticRotationInertia *= KINETIC_ROTATION_DAMPING;
      if (Math.abs(dAngle) > Math.abs(kineticRotationInertia)) {
        kineticRotationInertia = dAngle;
      }
      if (kineticRotationInertia * kineticRotationInertia < KINETIC_ROTATION_START_TOLERANCE_ANGLE * KINETIC_ROTATION_START_TOLERANCE_ANGLE) {
        stopKineticRotation();
      }

      applyMovementConstraints();

      matricesChanged = true;
      frustumChanged();
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public void fitToBoundingBox(Point3D focusPoint, Collection<Point3D> points, Rect rect, int width, int height, boolean integerZoom, boolean resetTilt, boolean resetRotation, int duration) {
    setGeneralLock(true);
    try {
      float oldBestZoom0Distance = getBestZoom0Distance();
      bestZoom0Distance = renderSurface.getBestZoom0Distance(width, height, options.getTileSize());

      Point3D oldFocusPoint = getFocusPoint();
      setFocusPoint(focusPoint.x, focusPoint.y, focusPoint.z, 0, true);

      float oldTilt = getTilt();
      float tilt = 90;
      if (resetTilt) {
        setTilt(tilt, 0);
      }

      float rotation = 0;
      float oldRotation = getRotation();
      if (resetRotation) {
        setRotation(rotation, 0);
      }

      float oldZoom = getZoom();
      float zoom = 0;
      float zoomStep = Const.MAX_SUPPORTED_ZOOM_LEVEL / 2;
      for (int i = 0; i < 24; i++) {
        setZoom(zoom + zoomStep, 0);
        boolean fit = true;
        for (Point3D point : points) {
          MapPos screenPos = worldToScreen(point.x, point.y, point.z, width, height);
          if (!rect.contains((int) screenPos.x, (int) screenPos.y)) {
            fit = false;
            break;
          }
        }
        if (fit) {
          zoom += zoomStep;
        }
        zoomStep /= 2;
      }
      if (integerZoom) {
        zoom = (float) Math.floor(zoom);
      }

      setFocusPoint(oldFocusPoint.x, oldFocusPoint.y, oldFocusPoint.z, 0, true);
      setFocusPoint(focusPoint.x, focusPoint.y, focusPoint.z, duration, true);

      if (resetTilt) {
        setTilt(oldTilt, 0);
        setTilt(tilt, duration);
      }

      if (resetRotation) {
        setRotation(oldRotation, 0);
        setRotation(rotation, duration);
      }

      setZoom(oldZoom, 0); 
      setZoom(zoom, duration);
      
      bestZoom0Distance = oldBestZoom0Distance;
    } finally {
      setGeneralLock(false);
    }
  }

  @Override
  public void setFocusPoint(double focusX, double focusY, double focusZ, int duration, boolean calcVectors) {
    stopKineticPanning();
    stopKineticRotation();
    if (duration > 0) {
      animationQueue.add(calcVectors ? AnimationQueue.MOVE_ANIMATION : AnimationQueue.PAN_ANIMATION, duration, new Point3D(focusX, focusY, focusZ));
    } else {
      animationQueue.remove(AnimationQueue.MOVE_ANIMATION);
      animationQueue.remove(AnimationQueue.PAN_ANIMATION);
      updateFocusPoint(focusX, focusY, focusZ, calcVectors);
    }
  }

  @Override
  public void updateFocusPoint(double focusX, double focusY, double focusZ, boolean calcVectors) {
    generalLock.lock();
    try {
      double[] transform = renderProjection.getTranslateMatrix(focusPoint, new Point3D(focusX, focusY, focusZ), 1);
      focusPoint = GeomUtils.transform(focusPoint, transform);
      cameraPos = GeomUtils.transform(cameraPos, transform);
      if (!calcVectors) {
        upVector = GeomUtils.transform(upVector, transform);
      } else {
        recalculateCameraVectors();
      }
      
      applyMovementConstraints();

      matricesChanged = true;
      frustumChanged();
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public float getRotation() {
    return rotationDeg;
  }

  @Override
  public void setRotation(float angle, int duration) {
    stopKineticPanning();
    stopKineticRotation();
    if (duration > 0) {
      animationQueue.add(AnimationQueue.ROTATE_ANIMATION, duration, angle);
    } else {
      animationQueue.remove(AnimationQueue.ROTATE_ANIMATION);
      updateRotation(angle);
    }
  }

  @Override
  public void updateRotation(float angle) {
    angle = (angle % 360 + 360) % 360;

    generalLock.lock();
    try {
      rotationDeg = angle;
      recalculateCameraVectors();

      matricesChanged = true;
      frustumChanged();
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public void rotate(float deltaAngle, int duration) {
    setRotation(rotationDeg + deltaAngle, duration);
  }

  @Override
  public float getTilt() {
    return 90 - tiltDeg;
  }

  @Override
  public void setTilt(float angle, int duration) {
    stopKineticPanning();
    stopKineticRotation();
    if (duration > 0) {
      animationQueue.add(AnimationQueue.TILT_ANIMATION, duration, angle);
    } else {
      animationQueue.remove(AnimationQueue.TILT_ANIMATION);
      updateTilt(angle);
    }
  }

  @Override
  public void updateTilt(float angle) {
    Range tiltRange = constraints.getTiltRange();
    angle = Utils.toRange(angle, tiltRange.min, tiltRange.max);
    
    generalLock.lock();
    try {
      tiltDeg = 90 - angle;
      recalculateCameraVectors();

      matricesChanged = true;
      frustumChanged();
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public void tilt(float deltaAngle, int duration) {
    setTilt(90 - tiltDeg + deltaAngle, duration);
  }

  @Override
  public float getZoom() {
    return zoom;
  }

  @Override
  public void setZoom(float zoom, int duration) {
    stopKineticPanning();
    stopKineticRotation();
    if (duration > 0) {
      animationQueue.add(AnimationQueue.ZOOM_ANIMATION, duration, zoom);
    } else {
      animationQueue.remove(AnimationQueue.ZOOM_ANIMATION);
      updateZoom(zoom);
    }
  }

  @Override
  public void updateZoom(float zoom) {
    Range zoomRange = constraints.getZoomRange();
    zoom = Utils.toRange(zoom, zoomRange.min, zoomRange.max);

    generalLock.lock();
    try {
      this.zoom = zoom;
      zoomPow2 = (float) Math.pow(2, zoom);
      if (bestZoom0Distance > 0) {
        Vector3D cameraVec = new Vector3D(focusPoint, cameraPos);
        double ratio = (bestZoom0Distance / Math.pow(2, zoom)) / cameraVec.getLength();
        cameraPos = new Point3D(focusPoint.x + cameraVec.x * ratio, focusPoint.y + cameraVec.y * ratio, focusPoint.z + cameraVec.z * ratio);

        matricesChanged = true;
        frustumChanged();
      }
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public void zoom(float deltaZoom, int duration) {
    setZoom(zoom + deltaZoom, duration);
  }

  @Override
  public void setViewDistance(float distance) {
    matricesChanged = true;
    frustumChanged();
  }

  @Override
  public void setSkyBitmap(Bitmap skyBitmap) {
    this.skyBitmap = skyBitmap;
    if (backgroundRenderer != null) {
      backgroundRenderer.setSkyBitmap(skyBitmap);
    }
    requestRenderView();
  }

  @Override
  public void setBackgroundPlaneBitmap(Bitmap backgroundBitmap) {
    this.backgroundPlaneBitmap = backgroundBitmap;
    if (backgroundRenderer != null) {
      backgroundRenderer.setBackgroundPlaneBitmap(backgroundPlaneBitmap);
    }
    requestRenderView();
  }

  @Override
  public void setBackgroundPlaneOverlayBitmap(Bitmap backgroundBitmap) {
    this.backgroundPlaneOverlayBitmap = backgroundBitmap;
    if (backgroundRenderer != null) {
      backgroundRenderer.setBackgroundPlaneOverlayBitmap(backgroundPlaneOverlayBitmap);
    }
    requestRenderView();
  }

  @Override
  public void setBackgroundImageBitmap(Bitmap backgroundImageBitmap) {
    this.backgroundImageBitmap = backgroundImageBitmap;
    if (backgroundRenderer != null) {
      backgroundRenderer.setBackgroundImageBitmap(backgroundImageBitmap);
    }
    requestRenderView();
  }

  @Override
  public void setGeneralLock(boolean locked) {
    if (locked) {
      generalLock.lock();
    } else {
      generalLock.unlock();
    }
  }

  private void rendererChanged() {
    for (Layer layer : layers.getAllLayers()) {
      layerChanged(layer);
    }
    
    requestRenderView();
  }

  @Override
  public void frustumChanged() {
    for (Layer layer : layers.getAllLayers()) {
      if (layer instanceof VectorLayer<?>) {
        vectorCullThread.addTask((VectorLayer<?>) layer, Const.FRUSTUM_CULL_DELAY);
      }
      if (layer instanceof TileLayer) {
        tileCullThread.addTask((TileLayer) layer, Const.FRUSTUM_CULL_DELAY);
      }
      if (layer instanceof BillBoardLayer<?> || layer instanceof VectorTileLayer) {
        billBoardCullThread.addTask(layer, Const.TEXT_CULL_DELAY);
      }
    }

    MapListener mapListener = options.getMapListener();
    if (mapListener != null) {
      mapListener.onMapMovedInternal();
    }
    
    requestRenderView(); // request redraw at once, kinetic panning may need update, etc
  }
  
  @Override
  public void layerChanged(Layer layer) {
    if (layer instanceof VectorLayer<?>) {
      // If needed, un-select element at this point - we want selection to be deterministic, so this can not be done during actual rendering time
      VectorElement vectorElement = selectedVectorElement;
      if (vectorElement != null) {
        boolean elementVisible = false;
        Layer elementLayer = vectorElement.getLayer();
        if (elementLayer != null) {
          elementVisible = elementLayer.isVisible() && elementLayer.isInVisibleZoomRange(zoom);
          if (elementLayer.getComponents() != components) {
            elementVisible = false;
          }
        }
        if (!elementVisible) {
          selectedVectorElement = null;
        }
      }
      vectorCullThread.addTask((VectorLayer<?>) layer, 0);
    }
    if (layer instanceof TileLayer) {
      tileCullThread.addTask((TileLayer) layer, 0);
    }
    if (layer instanceof BillBoardLayer<?> || layer instanceof VectorTileLayer) {
      billBoardCullThread.addTask(layer, 0);
    }
  }

  @Override
  public void layerVisibleElementsChanged(Layer layer) {
    if (selectedVectorElement != null) {
      updateLabelPos = true;
    }
    if (layer instanceof BillBoardLayer<?> || layer instanceof VectorTileLayer) {
      billBoardCullThread.addTask(layer, 0);
    }
    requestRenderView();
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    if (!isMappingStarted()) {
      return false;
    }

    VectorElement vectorElement = selectedVectorElement;
    if (vectorElement != null && labelPos != null) {
      Label label = vectorElement.getLabel();
      if (label != null) {
        if (label.onTouchEvent(event)) {
          requestRenderView();
          return true;
        }
      }
    }
    requestRenderView();
    return touchHandler.onTouchEvent(event);
  }

  @Override
  public VectorElement getSelectedVectorElement() {
    return selectedVectorElement;
  }

  @Override
  public void selectVectorElement(VectorElement vectorElement) {
    boolean elementVisible = false;
    if (vectorElement != null) {
      Layer elementLayer = vectorElement.getLayer();
      if (elementLayer != null) {
        elementVisible = elementLayer.isVisible() && elementLayer.isInVisibleZoomRange(zoom);
      }
    }
    if (!elementVisible) {
      deselectVectorElement();
      return;
    }
    selectedVectorElement = vectorElement;
    updateLabelPos = true;
    reloadLabelTexture = true;
    clickPoint = vectorElement.calculateInternalClickPos(null);
    requestRenderView();
  }

  @Override
  public void deselectVectorElement() {
    selectedVectorElement = null;
    requestRenderView();
  }

  @Override
  public void startKineticPanning() {
    kineticPanning = true;
    requestRenderView(); // we need to refresh the view until panning has finished
  }

  @Override
  public void stopKineticPanning() {
    kineticPanning = false;
    generalLock.lock();
    try {
      kineticPanInertia = 0;
    } finally {
      generalLock.unlock();
    }
  }

  @Override
  public void startKineticRotation() {
    kineticRotation = true;
    requestRenderView(); // we need to refresh the view until rotation has finished
  }

  @Override
  public void stopKineticRotation() {
    kineticRotation = false;
    generalLock.lock();
    try {
      kineticRotationInertia = 0;
    } finally {
      generalLock.unlock();
    }
  }
  
  public boolean isMappingStarted() {
    return billBoardCullThread.isAlive();
  }

  @Override
  public void onStartMapping() {
    recalculateCameraState();

    touchHandler.reset();
    touchHandler.onStartMapping();
    if (!vectorCullThread.isAlive()) {
      vectorCullThread = new VectorCullThread(this, components);
    }
    if (!tileCullThread.isAlive()) {
      tileCullThread = new TileCullThread(this, components);
    }
    if (!billBoardCullThread.isAlive()) {
      billBoardCullThread = new BillBoardCullThread(this, components);
    }

    vectorCullThread.startTasks();
    tileCullThread.startTasks();
    billBoardCullThread.startTasks();

    rendererChanged();
    requestRenderView();
  }

  @Override
  public void onStopMapping() {
    Label label = rSelectedVectorElementLabel;
    if (label != null) {
      label.onHide(view);
      rSelectedVectorElementLabel = null;
    }

    billBoardCullThread.exitThread();
    tileCullThread.exitThread();
    vectorCullThread.exitThread();

    billBoardCullThread.joinThread();
    tileCullThread.joinThread();
    vectorCullThread.joinThread();

    touchHandler.onStopMapping();
  }

  @Override
  public void captureRendering(MapRenderListener listener) {
    synchronized (mapRenderListeners) {
      mapRenderListeners.add(listener);
    }
    requestRenderView();
  }
}
