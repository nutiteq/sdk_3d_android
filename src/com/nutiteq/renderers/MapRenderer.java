package com.nutiteq.renderers;

import java.util.Collection;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.view.MotionEvent;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MapTile;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector;
import com.nutiteq.components.Vector3D;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.layers.Layer;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.ui.MapRenderListener;

/**
 * Interface for map renderers.
 */
public interface MapRenderer extends Renderer {
  
  Point3D screenToWorld(double x, double y, boolean closest);
  
  Point3D screenToWorld(double x, double y, int width, int height, boolean closest);

  MapPos worldToScreen(double x, double y, double z);

  MapPos worldToScreen(double x, double y, double z, int width, int height);

  MapPos mapTileToInternal(MapTile mapTile, MapPos tilePos);
  
  MapTile internalToMapTile(double x, double y, int zoom, MutableMapPos tilePos);

  GLSurfaceView getView();

  void setView(GLSurfaceView view);

  RenderSurface getRenderSurface();
  
  void setRenderSurface(RenderSurface renderSurface);
  
  void requestRenderView();

  void click(float x, float y);
  
  void longClick(float x, float y);

  Point3D getCameraPos();

  Point3D getFocusPoint();

  Vector3D getUpVector();
  
  CameraState getCameraState();
  
  double[] getModelviewMatrix();

  float[] getProjectionMatrix();

  double[] getModelviewProjectionMatrix();

  float getBestZoom0Distance();
  
  float getAspectRatio();

  float getNearPlane();
  
  float getFarPlane();
  
  Vector getFocusPointOffset();
  
  void setFocusPointOffset(float x, float y);
  
  void setLookAtParams(double camX, double camY, double camZ, double focusX, double focusY, double focusZ, double upX, double upY, double upZ, boolean calcRotation, boolean calcTilt, boolean calcZoom);

  void fitToBoundingBox(Point3D focusPoint, Collection<Point3D> points, Rect rect, int width, int height, boolean integerZoom, boolean resetTilt, boolean resetRotation, int duration);

  void updateFocusPoint(double focusX, double focusY, double focusZ, boolean calcVectors);
  
  void setFocusPoint(double focusX, double focusY, double focusZ, int duration, boolean calcVectors);
  
  float getRotation();
  
  void updateRotation(float angle);

  void setRotation(float angle, int duration);

  void rotate(float deltaAngle, int duration);

  float getTilt();

  void updateTilt(float angle);

  void setTilt(float angle, int duration);

  void tilt(float deltaAngle, int duration);

  float getZoom();

  void updateZoom(float zoom);

  void setZoom(float zoom, int duration);

  void zoom(float deltaZoom, int duration);

  void setViewDistance(float distance);

  void setSkyBitmap(Bitmap skyBoxBitmap);

  void setBackgroundPlaneBitmap(Bitmap backgroundBitmap);

  void setBackgroundPlaneOverlayBitmap(Bitmap backgroundBitmap);

  void setBackgroundImageBitmap(Bitmap backgroundImageBitmap);

  void setGeneralLock(boolean locked);

  void frustumChanged();
  
  void layerChanged(Layer layer);

  void layerVisibleElementsChanged(Layer layer);

  boolean onTouchEvent(MotionEvent event);

  VectorElement getSelectedVectorElement();

  void selectVectorElement(VectorElement vectorElement);

  void deselectVectorElement();

  void startKineticPanning();

  void stopKineticPanning();

  void startKineticRotation();

  void stopKineticRotation();

  void onStartMapping();

  void onStopMapping();

  void captureRendering(MapRenderListener listener);
}
