package com.nutiteq.renderers.threads;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Components;
import com.nutiteq.components.CullState;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.renderers.MapRenderer;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.tasks.CancelableThreadPool;
import com.nutiteq.utils.Matrix;
import com.nutiteq.vectorlayers.VectorLayer;

/**
 * Vector layer culling thread.
 * Calculates envelope based on render projection (in internal coordinates) and lets each vector layer to build its own list of visible vector elements.
 */
public class VectorCullThread extends Thread {
  private static final float VIEWPORT_PADDING = 1.1f; // normalized viewport relative size for culling (with some extra padding)

  private static class LayerTask {
    final VectorLayer<?> layer;
    long time;

    LayerTask(VectorLayer<?> layer, long time) {
      this.layer = layer;
      this.time = time;
    }
  }

  private MapRenderer mapRenderer;
  private CancelableThreadPool vectorTaskPool;

  private float aspect;
  private CameraState camera;
  private RenderSurface renderSurface;

  private List<Point3D> envelopeHullPoints = new ArrayList<Point3D>();
  private Envelope envelope = new Envelope(0, 0, 0, 0);
  
  private volatile boolean started;
  private volatile boolean stop;
  private List<LayerTask> layerTasks = new LinkedList<LayerTask>();

  public VectorCullThread(MapRenderer mapRenderer, Components components) {
    this.mapRenderer = mapRenderer;
    vectorTaskPool = components.vectorTaskPool;

    setPriority(Thread.MIN_PRIORITY);
    start();
  }

  public void startTasks() {
    synchronized (this) {
      started = true;
      notify();
    }
  }

  public boolean isEmptyTaskList() {
    synchronized (this) {
      return layerTasks.isEmpty();
    }
  }

  public void addTask(VectorLayer<?> layer, int delay) {
    long taskTime = System.currentTimeMillis() + delay;
    synchronized (this) {
      if (!started) {
        return;
      }
      for (LayerTask task : layerTasks) {
        if (task.layer == layer) {
          task.time = Math.min(task.time, taskTime);
          notify();
          return;
        }
      }
      layerTasks.add(new LayerTask(layer, taskTime));
      notify();
    }
  }

  @Override
  public void run() {
    List<VectorLayer<?>> layers = new ArrayList<VectorLayer<?>>();
    while (!stop) {
      long minDelay = Long.MAX_VALUE;
      long currentTime = System.currentTimeMillis();
      layers.clear();
      synchronized (this) {
        for (Iterator<LayerTask> it = layerTasks.iterator(); it.hasNext(); ) {
          LayerTask task = it.next();
          if (task.time <= currentTime + 1) { // allow task to wake up a bit before its time. In case of multiple tasks, this will batch them together
            layers.add(task.layer);
            it.remove();
          } else {
            minDelay = Math.min(minDelay, task.time - currentTime);
          }
        }
      }
      if (layers.isEmpty()) {
        synchronized (this) {
          try {
            if (stop) {
              break;
            }
            wait(minDelay == Long.MAX_VALUE ? 0 : minDelay);
          } catch (InterruptedException e) { }
        }
        continue;
      }

      if (!init()) {
        continue;
      }
      vectorTaskPool.cancelWithTags(layers);
      calculateVisibleElements(layers);
      mapRenderer.requestRenderView(); // NOTE: this is needed in case when layer becomes not visible
    }
  }

  public void exitThread() {
    synchronized (this) {
      stop = true;
      notify();
    }
  }
  
  public void joinThread() {
    try {
      join();
    } catch (InterruptedException e) {
      return;
    }

    mapRenderer = null;
    vectorTaskPool = null;
  }

  private boolean init() {
    RenderSurface oldRenderSurface = renderSurface;

    // Initialize parameters
    mapRenderer.setGeneralLock(true);
    try {
      aspect = mapRenderer.getAspectRatio();
      camera = mapRenderer.getCameraState();
      renderSurface = mapRenderer.getRenderSurface();
    }
    finally {
      mapRenderer.setGeneralLock(false);
    }
    
    // Reset internal state if renderprojection has changed
    if (renderSurface != oldRenderSurface) {
      envelopeHullPoints = new ArrayList<Point3D>();
      envelope = new Envelope(0, 0, 0, 0);
    }

    // If aspect is 0, then view parameters are invalid and skip culling
    return aspect != 0;
  }

  private Envelope calculateFrustumConvexEnvelope() {
    double[] invPerspectiveMatrix = new double[16];
    Matrix.invertM(invPerspectiveMatrix, 0, camera.modelviewProjectionMatrix, 0);
    
    // Iterate over all edges and find intersection with ground plane
    List<Point3D> hullPoints = new ArrayList<Point3D>(12);
    for (int i = 0; i < 12; i++) {
      int ax = (i >> 2) & 3;
      int as = (4-ax) >> 2;
      int bs = (5-ax) >> 2;
      int va = ((i&1) << as) | ((i&2) << bs);
      int vb = va | (1 << ax);

      // Calculate vertex 0
      double xa = (va & 1) == 0 ? VIEWPORT_PADDING : -VIEWPORT_PADDING;
      double ya = (va & 2) == 0 ? VIEWPORT_PADDING : -VIEWPORT_PADDING;
      double za = (va & 4) == 0 ? 1 : -1;
      double[] p0x = new double[] { 0, 0, 0, 0, xa, ya, za, 1 };
      Matrix.multiplyMV(p0x, 0, invPerspectiveMatrix, 0, p0x, 4);
      Point3D p0 = new Point3D(p0x[0] / p0x[3], p0x[1] / p0x[3], p0x[2] / p0x[3]);

      // Calculate vertex 1
      double xb = (vb & 1) == 0 ? VIEWPORT_PADDING : -VIEWPORT_PADDING;
      double yb = (vb & 2) == 0 ? VIEWPORT_PADDING : -VIEWPORT_PADDING;
      double zb = (vb & 4) == 0 ? 1 : -1;
      double[] p1x = new double[] { 0, 0, 0, 0, xb, yb, zb, 1 };
      Matrix.multiplyMV(p1x, 0, invPerspectiveMatrix, 0, p1x, 4);
      Point3D p1 = new Point3D(p1x[0] / p1x[3], p1x[1] / p1x[3], p1x[2] / p1x[3]);
      
      // Calculate intersection between z=0 plane and line p0<->p1
      Vector3D dp = new Vector3D(p0, p1);
      double[] ts = renderSurface.calculateIntersections(p0, dp, true);
      for (double t : ts) {
        if (t < 0 || t > 1) {
          continue;
        }
        Point3D p = new Point3D(p0.x + dp.x * t, p0.y + dp.y * t, p0.z + dp.z * t);
        hullPoints.add(p);
      }
    }
    
    // Check if hull points have changed since last time - if not, we can return old envelope.
    if (hullPoints.equals(envelopeHullPoints)) {
      return envelope;
    }

    // Calculate convex hull of generated point set
    envelope = renderSurface.getUnprojectedEnvelope(hullPoints.toArray(new Point3D[hullPoints.size()]));
    envelopeHullPoints = hullPoints;
    return envelope;
  }
  
  private void calculateVisibleElements(List<VectorLayer<?>> vectorLayers) {
    // Create culling state and pass this to all vector layers
    Envelope envelope = calculateFrustumConvexEnvelope();
    CullState cullState = new CullState(envelope, camera, renderSurface.getRenderProjection());
    for (VectorLayer<?> vectorLayer : vectorLayers) {
      if (!vectorLayer.isVisible() || !vectorLayer.isInVisibleZoomRange(camera.zoom)) {
        continue;
      }
      vectorLayer.calculateVisibleElements(cullState);
    }

  }
}
