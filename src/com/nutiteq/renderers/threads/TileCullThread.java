package com.nutiteq.renderers.threads;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Components;
import com.nutiteq.components.Options;
import com.nutiteq.layers.Layers;
import com.nutiteq.layers.TileLayer;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderers.MapRenderer;
import com.nutiteq.renderers.components.MapTileDrawData;
import com.nutiteq.renderers.components.MapTileProxy;
import com.nutiteq.renderers.components.MapTileQuadTreeNode;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.renderers.utils.MapTileGenerator;
import com.nutiteq.tasks.CancelableThreadPool;
import com.nutiteq.utils.Const;

/**
 * Tile layer culling thread.
 * Generates list of visible tiles for each tile layer. Supports different projections and render projections.
 */
public class TileCullThread extends Thread {
  private static final int MAX_TOTAL_TILE_COUNT = 64;   // Maximum number of tiles that can be generated (visible tiles are not counted against this limit)
  
  private static final Comparator<MapTileQuadTreeNode> TILE_DISTANCE_COMPARATOR = new Comparator<MapTileQuadTreeNode>() {
    @Override
    public int compare(MapTileQuadTreeNode tile1, MapTileQuadTreeNode tile2) {
      return Float.compare(tile1.distance, tile2.distance);
    }
  };
  
  private static class TileGeneratorKey {
    final Projection projection;
    final boolean preloading;
    final int minZoom;
    final int maxZoom;
    
    TileGeneratorKey(TileLayer layer) {
      this.projection = layer.getProjection();
      this.preloading = layer.getTilePreloading();
      this.minZoom = layer.getMinZoom();
      this.maxZoom = layer.getMaxZoom();
    }
    
    @Override
    public int hashCode() {
      return minZoom * Const.MAX_SUPPORTED_ZOOM_LEVEL + maxZoom;
    }
    
    @Override
    public boolean equals(Object o) {
      if (o == null) return false;
      if (this.getClass() != o.getClass()) return false;
      TileGeneratorKey other = (TileGeneratorKey) o;
      return projection.equals(other.projection) && minZoom == other.minZoom && maxZoom == other.maxZoom;
    }
  }

  private MapRenderer mapRenderer;

  private CancelableThreadPool generalTaskPool;
  private Layers layers;
  private Options options;

  private volatile boolean started;
  private volatile boolean stop;
  private volatile long cullTime = Long.MAX_VALUE;

  private CameraState camera;
  private RenderSurface renderSurface;

  private Map<MapTileProxy, MapTileDrawData> tileDrawDataMap = new HashMap<MapTileProxy, MapTileDrawData>();
  private Map<TileGeneratorKey, MapTileGenerator> projectionGeneratorMap = new HashMap<TileGeneratorKey, MapTileGenerator>();
  private Map<MapTileGenerator, List<TileLayer>> tileGeneratorLayerMap = new HashMap<MapTileGenerator, List<TileLayer>>();
  private List<TileLayer> invisibleLayers = new ArrayList<TileLayer>();
  
  public TileCullThread(MapRenderer mapRenderer, Components components) {
    this.mapRenderer = mapRenderer;

    generalTaskPool = components.rasterTaskPool;
    layers = components.layers;
    options = components.options;
    
    setPriority(Thread.MIN_PRIORITY);
    start();
  }
  
  public void startTasks() {
    synchronized (this) {
      started = true;
      notify();
    }
  }
  
  public boolean isEmptyTaskList() {
    synchronized (this) {
      return cullTime == Long.MAX_VALUE;
    }    
  }
  
  public void addTask(TileLayer layer, int delay) {
    synchronized (this) {
      if (!started) {
        return;
      }
      cullTime = Math.min(cullTime, System.currentTimeMillis() + delay);
      notify();
    }
  }

  @Override
  public void run() {
    while (!stop) {
      long wakeTime = 0;
      long currentTime = System.currentTimeMillis();
      synchronized (this) {
        if (cullTime <= currentTime + 1) { // allow task to wake up a bit before its time
          cullTime = Long.MAX_VALUE;
        } else {
          wakeTime = cullTime;
        }
      }
      if (wakeTime != 0) {
        synchronized (this) {
          try {
            if (stop) {
              break;
            }
            wait(wakeTime == Long.MAX_VALUE ? 0 : wakeTime - currentTime);
          } catch (InterruptedException e) { }
        }
        continue;
      }

      init();
      List<MapTileQuadTreeNode> tiles = calculateVisibleTiles();
      calculateTilesTextures(tiles);
      mapRenderer.requestRenderView(); // NOTE: this will force textures to be uploaded ASAP
    }
  }

  public void exitThread() {
    synchronized (this) {
      stop = true;
      notify();
    }
  }
  
  public void joinThread() {
    try {
      join();
    } catch (InterruptedException e) {
      return;
    }

    mapRenderer = null;
    generalTaskPool = null;
    layers = null;
    options = null;
    
    tileDrawDataMap.clear();
    projectionGeneratorMap.clear();
    tileGeneratorLayerMap.clear();
    invisibleLayers.clear();
  }
  
  private void init() {
    RenderSurface oldRenderSurface = renderSurface;

    // Initialize view specific parameters
    mapRenderer.setGeneralLock(true);
    try {
      camera = mapRenderer.getCameraState();
      renderSurface = mapRenderer.getRenderSurface();
    }
    finally {
      mapRenderer.setGeneralLock(false);
    }
    
    // Reset cached state if renderprojection has changed
    if (renderSurface != oldRenderSurface) {
      tileDrawDataMap.clear();
    }

    // Create map of projections and tile generators
    List<TileLayer> tileLayers = layers.getTileLayers();
    Map<TileGeneratorKey, MapTileGenerator> oldProjectionGeneratorMap = projectionGeneratorMap;
    Map<TileGeneratorKey, MapTileGenerator> newProjectionGeneratorMap = new HashMap<TileGeneratorKey, MapTileGenerator>();
    tileGeneratorLayerMap.clear();
    invisibleLayers.clear();
    for (TileLayer tileLayer : tileLayers) {
      // If layer not visible, ignore
      if (!tileLayer.isVisible() || !tileLayer.isInVisibleZoomRange(camera.zoom)) {
        invisibleLayers.add(tileLayer);
        continue;
      }

      // Try to reuse existing generator with same parameters
      TileGeneratorKey key = new TileGeneratorKey(tileLayer);
      MapTileGenerator tileGen = newProjectionGeneratorMap.get(key);
      if (tileGen == null) {
        tileGen = oldProjectionGeneratorMap.get(key);
        if (tileGen == null) {
          tileGen = new MapTileGenerator(layers.getBaseProjection(), key.projection, key.minZoom, key.maxZoom, renderSurface, key.preloading, options);
        }
        newProjectionGeneratorMap.put(key, tileGen);
      }

      List<TileLayer> layerList = tileGeneratorLayerMap.get(tileGen);
      if (layerList == null) {
        layerList = new ArrayList<TileLayer>();
        tileGeneratorLayerMap.put(tileGen, layerList);
      }
      layerList.add(tileLayer);
    }
    projectionGeneratorMap = newProjectionGeneratorMap;
  }
  
  private List<MapTileQuadTreeNode> calculateVisibleTiles() {
    // Generate lists of visible and preload tiles
    Queue<MapTileQuadTreeNode> visibleTiles = new PriorityQueue<MapTileQuadTreeNode>(128, TILE_DISTANCE_COMPARATOR);
    Queue<MapTileQuadTreeNode> preloadTiles = new PriorityQueue<MapTileQuadTreeNode>(128, TILE_DISTANCE_COMPARATOR);
    for (MapTileGenerator tileGen : projectionGeneratorMap.values()) {
      List<MapTileQuadTreeNode> projVisibleTiles = new ArrayList<MapTileQuadTreeNode>(64);
      List<MapTileQuadTreeNode> projPreloadTiles = new ArrayList<MapTileQuadTreeNode>(64);
      tileGen.generateMapTiles(camera, projVisibleTiles, projPreloadTiles);
      visibleTiles.addAll(projVisibleTiles);
      preloadTiles.addAll(projPreloadTiles);
    }

    // Create final sorted tile list
    List<MapTileQuadTreeNode> tiles = new ArrayList<MapTileQuadTreeNode>(visibleTiles.size() + preloadTiles.size() + 1);
    while (true) {
      MapTileQuadTreeNode tile = visibleTiles.poll();
      if (tile == null) {
        break;
      }
      tiles.add(tile);
    }

    while (tiles.size() < MAX_TOTAL_TILE_COUNT) {
      MapTileQuadTreeNode tile = preloadTiles.poll();
      if (tile == null) {
        break;
      }
      tiles.add(tile);
    }
    
    return tiles;
  }
    
  private void calculateTilesTextures(List<MapTileQuadTreeNode> tiles) {
    // Remove all tasks that have not started
    generalTaskPool.cancelAll();
    
    // Resolve tiles for layers
    Map<TileLayer, List<MapTileProxy>> layerTileProxyMap = new HashMap<TileLayer, List<MapTileProxy>>();
    for (MapTileQuadTreeNode tile : tiles) {
      List<TileLayer> tileLayers = tileGeneratorLayerMap.get(tile.tileGenerator);
      if (tileLayers == null) {
        continue;
      }

      for (TileLayer tileLayer : tileLayers) {
        List<MapTileProxy> tileProxies = layerTileProxyMap.get(tileLayer);
        if (tileProxies == null) {
          tileProxies = new ArrayList<MapTileProxy>(tiles.size() + 1);
          layerTileProxyMap.put(tileLayer, tileProxies);
        }
        
        // Resolve original tile into proxies
        tileLayer.resolveTile(tile, tileProxies);
      }
    }

    // Create TileDrawData for each proxy/actual draw data lists for layers
    Map<MapTileProxy, MapTileDrawData> oldTileDrawDataMap = tileDrawDataMap;
    Map<MapTileProxy, MapTileDrawData> newTileDrawDataMap = new HashMap<MapTileProxy, MapTileDrawData>();
    for (Iterator<Map.Entry<TileLayer, List<MapTileProxy>>> it = layerTileProxyMap.entrySet().iterator(); it.hasNext(); ) {
      Map.Entry<TileLayer, List<MapTileProxy>> entry = it.next();
      TileLayer tileLayer = entry.getKey();
      List<MapTileProxy> tileProxies = entry.getValue();
      
      List<MapTileDrawData> tileDrawDatas = new ArrayList<MapTileDrawData>(tileProxies.size() + 1);
      for (MapTileProxy tileProxy : tileProxies) {
        // TODO: as tileProxy contains layer-dependent fullTileId, draw datas are not reused between layers, although they should be
        MapTileDrawData tileDrawData = oldTileDrawDataMap.get(tileProxy);
        if (tileDrawData == null) {
          tileDrawData = tileProxy.mapTile.tileGenerator.createTileDrawData(tileProxy);
        }
        newTileDrawDataMap.put(tileProxy, tileDrawData);
        tileDrawDatas.add(tileDrawData);
      }
      tileLayer.setVisibleTiles(tileDrawDatas);
    }
    tileDrawDataMap = newTileDrawDataMap;

    // Update invisible layers
    for (TileLayer tileLayer : invisibleLayers) {
      tileLayer.setVisibleTiles(new ArrayList<MapTileDrawData>());
    }
  }
  
}
