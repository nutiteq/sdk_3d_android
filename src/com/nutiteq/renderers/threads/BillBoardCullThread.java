package com.nutiteq.renderers.threads;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Components;
import com.nutiteq.geometry.BillBoard;
import com.nutiteq.layers.Layer;
import com.nutiteq.layers.Layers;
import com.nutiteq.renderers.MapRenderer;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.renderers.utils.BillBoardPlacementGenerator;
import com.nutiteq.style.BillBoardStyle;
import com.nutiteq.vectorlayers.BillBoardLayer;
import com.nutiteq.vectorlayers.VectorTileLayer;

/**
 * Billboard (marker/text) culling thread.
 * Analyzes billboard layout, repositions them and hides overlapping billboard (in case overlapping is disabled by style flags).
 */
public class BillBoardCullThread extends Thread {
  private Layers layers;
  private MapRenderer mapRenderer;
  
  private CameraState camera;
  private RenderSurface renderSurface;
  
  private BillBoardPlacementGenerator placementGenerator;

  private volatile boolean started;
  private volatile boolean stop;
  private volatile long cullTime = Long.MAX_VALUE;

  public BillBoardCullThread(MapRenderer mapRenderer, Components components) {
    this.layers = components.layers;
    this.mapRenderer = mapRenderer;
    
    setPriority(Thread.MIN_PRIORITY);
    start();
  }

  public void startTasks() {
    synchronized (this) {
      started = true;
      notify();
    }
  }
  
  public boolean isEmptyTaskList() {
    synchronized (this) {
      return cullTime == Long.MAX_VALUE;
    }    
  }
  
  public void addTask(Layer layer, int delay) {
    synchronized (this) {
      if (!started) {
        return;
      }
      cullTime = Math.min(cullTime, System.currentTimeMillis() + delay);
      notify();
    }
  }

  @Override
  public void run() {
    while (!stop) {
      long wakeTime = 0;
      long currentTime = System.currentTimeMillis();
      synchronized (this) {
        if (cullTime <= currentTime + 1) { // allow task to wake up a bit before its time
          cullTime = Long.MAX_VALUE;
        } else {
          wakeTime = cullTime;
        }
      }
      if (wakeTime != 0) {
        synchronized (this) {
          try {
            if (stop) {
              break;
            }
            wait(wakeTime == Long.MAX_VALUE ? 0 : wakeTime - currentTime);
          } catch (InterruptedException e) { }
        }
        continue;
      }

      init();
      if (calculateVisibleElements()) {
        mapRenderer.requestRenderView();
      }
    }
  }

  public void exitThread() {
    synchronized (this) {
      stop = true;
      notify();
    }
  }
  
  public void joinThread() {
    try {
      join();
    } catch (InterruptedException e) {
      return;
    }

    mapRenderer = null;
    layers = null;
  }
  
  private void init() {
    RenderSurface oldRenderSurface = renderSurface;

    // Initialize parameters
    mapRenderer.setGeneralLock(true);
    try {
      camera = mapRenderer.getCameraState();
      renderSurface = mapRenderer.getRenderSurface();
    }
    finally {
      mapRenderer.setGeneralLock(false);
    }
    
    // Reset internal state if renderprojection has changed
    if (renderSurface != oldRenderSurface) {
      placementGenerator = new BillBoardPlacementGenerator(renderSurface.getRenderProjection());
    }
  }
  
  @SuppressWarnings("unchecked")
  private boolean calculateVisibleElements() {
    boolean changed = false;

    CameraState camera = mapRenderer.getCameraState();
    List<BillBoard> visibleList = new ArrayList<BillBoard>();
    for (Layer layer : layers.getAllLayers()) {
      if (!layer.isVisible()) {
        continue;
      }
      if (!layer.isInVisibleZoomRange(camera.zoom)) {
        continue;
      }

      if (layer instanceof BillBoardLayer<?>) {
        List<? extends BillBoard> visibleElements = ((BillBoardLayer<? extends BillBoard>) layer).getVisibleElements();
        if (visibleElements != null) {
          visibleList.addAll(visibleElements);
        }
      } else if (layer instanceof VectorTileLayer) {
        List<BillBoard> tileVisibleList = new ArrayList<BillBoard>();
        for (BillBoardLayer<?> billBoardLayer : ((VectorTileLayer) layer).getBillBoardLayers()) {
          List<? extends BillBoard> visibleElements = billBoardLayer.getVisibleElements();
          if (visibleElements != null) {
            tileVisibleList.addAll(visibleElements);
          }
        }
        if (calculateVisibleElements(tileVisibleList)) {
          changed = true;
        }
      }
    }
    if (calculateVisibleElements(visibleList)) {
      changed = true;
    }

    return changed;
  }
  
  private boolean calculateVisibleElements(List<? extends BillBoard> visibleList) {
    // Check if layouting is needed at all - at least one of the billboards must have style with 'allowOverlap = false' or one billboard must be invisible
    boolean checkOverlap = false;
    for (BillBoard element : visibleList) {
      if (!element.getInternalState().visible) {
        checkOverlap = true;
        break;
      }
      BillBoardStyle style = (BillBoardStyle) element.getInternalState().activeStyle;
      if (style != null && !style.allowOverlap) {
        checkOverlap = true;
        break;
      }
    }
    if (!checkOverlap) {
      return false;
    }

    // Sort elements - put higher priority elements first and then visible elements before invisible elements
    PriorityQueue<BillBoard> visibleQueue = new PriorityQueue<BillBoard>(visibleList.size() + 1, new Comparator<BillBoard>() {
      @Override
      public int compare(BillBoard element1, BillBoard element2) {
        BillBoardStyle style1 = (BillBoardStyle) element1.getInternalState().activeStyle, style2 = (BillBoardStyle) element2.getInternalState().activeStyle;
        int diff0 = (style2 != null ? (style2.allowOverlap ? 1 : 0) : 0) - (style1 != null ? (style1.allowOverlap ? 1 : 0) : 0);
        if (diff0 != 0) {
          return diff0;
        }
        int diff1 = (style2 != null ? style2.placementPriority : 0) - (style1 != null ? style1.placementPriority : 0);
        if (diff1 != 0) {
          return diff1;
        }
        int diff2 = (element2.getInternalState().visible ? 1 : 0) - (element1.getInternalState().visible ? 1 : 0);
        return diff2;
        // TODO: distance comparison
      }
    });
    for (BillBoard element : visibleList) {
      if (element.getInternalState().activeStyle != null) {
        visibleQueue.add(element);
      }
    }
    
    // Configure placement generator
    placementGenerator.setCamera(camera);
    placementGenerator.clear();
    
    // Do final layout phase - if element is already visible, try to keep it at the same position, otherwise try to find a new position
    boolean changed = false;
    while (true) {
      BillBoard element = visibleQueue.poll();
      if (element == null) {
        break;
      }
      boolean oldVisible = element.getInternalState().visible;
      if (oldVisible) {
        if (placementGenerator.add(element, true)) {
          continue;
        }
      }
      boolean newVisible = placementGenerator.fitAndAdd(element);
      if (newVisible || oldVisible) {
        changed = true;
      }
      element.getInternalState().visible = newVisible;
    }
    return changed;
  }

}
