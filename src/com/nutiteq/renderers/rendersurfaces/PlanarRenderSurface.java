package com.nutiteq.renderers.rendersurfaces;

import java.util.ArrayList;
import java.util.List;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.Options;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.PlanarRenderProjection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.GeomUtils;
import com.nutiteq.utils.TriangleMesh;
import com.nutiteq.utils.Utils;

/**
 * Flat render surface.
 */
public class PlanarRenderSurface implements RenderSurface {
  private static final int TILE_TESSELATION_MAX_ZOOM_LEVEL = 6;
  private static final double DIVIDE_THRESHOLD_PER_UNIT = 1.0 / 10; // approx number of units (depends on bounds) for each additional tesselation level when calculating envelope
  private static final double INTERNAL_DIVIDE_EPSILON = 1.0e-7;
  private static final double BOUNDS_COEFFICENT = 0.95;

  private final PlanarRenderProjection renderProjection;
  private final Projection baseProjection;
  private final Options options;

  private final GeomUtils.MapPosTransformation unprojectTransform = new GeomUtils.MapPosTransformation() {
    @Override
    public MapPos transform(MapPos src) {
      MutableMapPos dest = new MutableMapPos();
      renderProjection.unproject(src.x, src.y, src.z, dest);
      return new MapPos(dest);
    }
  };

  public PlanarRenderSurface(Projection baseProjection, Options options) {
    this.renderProjection = new PlanarRenderProjection(baseProjection, Const.EARTH_RADIUS, Const.UNIT_SIZE);
    this.baseProjection = baseProjection;
    this.options = options;
  }
  
  @Override
  public RenderProjection getRenderProjection() {
    return renderProjection;
  }

  @Override
  public Point3D getWrappedPoint(Point3D point) {
    // Convert to base projection
    MapPos mapPosInternal = renderProjection.unproject(point);
    MapPos mapPos = baseProjection.fromInternal(mapPosInternal);

    // Constrain to bounds
    Bounds bounds = baseProjection.getBounds();
    double x = Utils.toRange(mapPos.x, bounds.left, bounds.right);
    double y = Utils.toRange(mapPos.y, bounds.bottom, bounds.top);
    if (options.isSeamlessHorizontalPan()) {
      double xp = (mapPos.x - bounds.left) / bounds.getWidth();
      double x0 = Math.floor(xp);
      if (x0 != 0) {
        x = (xp - x0) * bounds.getWidth() + bounds.left;
      }
    }
    
    // Convert back
    MapPos mapPosInternalContrained = baseProjection.toInternal(x, y);
    return renderProjection.project(mapPosInternalContrained);
  }
  
  @Override
  public Point3D getGroundPoint(Point3D point) {
    return new Point3D(point.x, point.y, 0);
  }

  @Override
  public Point3D getRotationOrigin(Point3D point) {
    return point;
  }

  @Override
  public Envelope getUnprojectedEnvelope(Point3D[] points) {
    List<MapPos> mapPoints = new ArrayList<MapPos>(points.length + 4);

    // Find extreme points
    double minX = Double.MAX_VALUE, maxX = -Double.MAX_VALUE;
    double minY = Double.MAX_VALUE, maxY = -Double.MAX_VALUE;
    for (int i = 0; i < points.length; i++) {
      Point3D p = points[i];
      mapPoints.add(new MapPos(p.x, p.y)); // we will do unprojection later

      minX = Math.min(minX, p.x);
      maxX = Math.max(maxX, p.x);
      minY = Math.min(minY, p.y);
      maxY = Math.max(maxY, p.y);
    }

    // Clip against bounds (note: in case of EPSG3857, boundsX = 2 * UNIT_SIZE)
    Bounds bounds = renderProjection.getBaseProjection().getBounds();
    double boundsX = Const.UNIT_SIZE * bounds.getWidth()  / Math.min(bounds.getWidth(), bounds.getHeight());
    double boundsY = Const.UNIT_SIZE * bounds.getHeight() / Math.min(bounds.getWidth(), bounds.getHeight());

    minX = Math.max(minX, -boundsX);
    minY = Math.max(minY, -boundsY);
    maxX = Math.min(maxX,  boundsX);
    maxY = Math.min(maxY,  boundsY);

    // HACK: in order to avoid clipping implementation, detect 'close to border case' and use rectangular envelope.
    // As a result, we will get larger envelope than necessary, but at least envelope covers visible area.
    // This is based on assumption that poles and singularities of projections are at borders and never inside.
    // This 'hack' is here mostly to help transformConvexHull: otherwise there could be excessive tesselation (for example, EPSG3857 -> internal)!
    if (minX < -boundsX * BOUNDS_COEFFICENT || maxX > boundsX * BOUNDS_COEFFICENT || minY < -boundsY * BOUNDS_COEFFICENT || maxY > boundsY * BOUNDS_COEFFICENT) {
      mapPoints.clear();
      mapPoints.add(new MapPos(minX, minY));
      mapPoints.add(new MapPos(maxX, minY));
      mapPoints.add(new MapPos(maxX, maxY));
      mapPoints.add(new MapPos(minX, maxY));
    }

    MapPos[] hullPoints = GeomUtils.calculateConvexHull(mapPoints.toArray(new MapPos[mapPoints.size()]));
    double divideThreshold = Const.UNIT_SIZE * DIVIDE_THRESHOLD_PER_UNIT;
    MapPos[] unprojectedHullPoints = GeomUtils.transformConvexHull(hullPoints, unprojectTransform, divideThreshold, INTERNAL_DIVIDE_EPSILON);
    return new Envelope(unprojectedHullPoints);
  }

  @Override
  public Point3D[] getContourVertices(Point3D cameraPos) {
    // Disable contour, as it is difficult to avoid Z-fighting
    return new Point3D[0];
  }
  
  @Override
  public TriangleMesh getSurfaceTriangles() {
    TriangleMesh.Builder meshBuilder = TriangleMesh.builder(4, 4, 4, 2);
    
    float s = 32.0f;
    meshBuilder.addVertex(-Const.UNIT_SIZE * s, -Const.UNIT_SIZE * s, 0);
    meshBuilder.addVertex( Const.UNIT_SIZE * s, -Const.UNIT_SIZE * s, 0);
    meshBuilder.addVertex( Const.UNIT_SIZE * s,  Const.UNIT_SIZE * s, 0);
    meshBuilder.addVertex(-Const.UNIT_SIZE * s,  Const.UNIT_SIZE * s, 0);
    meshBuilder.addTexCoord(0, 0);
    meshBuilder.addTexCoord(0, s);
    meshBuilder.addTexCoord(s, s);
    meshBuilder.addTexCoord(s, 0);
    meshBuilder.addNormal(0, 0, 1);
    meshBuilder.addNormal(0, 0, 1);
    meshBuilder.addNormal(0, 0, 1);
    meshBuilder.addNormal(0, 0, 1);

    meshBuilder.addTriangle(0, 1, 2);
    meshBuilder.addTriangle(0, 2, 3);

    return meshBuilder.build();
  }
  
  @Override
  public float getTileSubdivisionFactor() {
    return 1;
  }
  
  @Override
  public boolean isSeamlessHorizontalPan() {
    return options.isSeamlessHorizontalPan();
  }

  @Override
  public int getTileTesselationFactor(Projection projection, int zoom, int axis) {
    if (projection.equals(baseProjection)) {
      return 1;
    }
    if (zoom >= TILE_TESSELATION_MAX_ZOOM_LEVEL) {
      return 1;
    }
    return 1 << (TILE_TESSELATION_MAX_ZOOM_LEVEL - zoom);
  }

  @Override
  public float getBestZoom0Distance(float width, float height, int tileSize) {
    float bestZoom0Distance = height * (float) Const.UNIT_SIZE / (tileSize * Const.HALF_FOV_TAN_Y);
    return bestZoom0Distance;
  }
  
  @Override
  public float getNearPlane(Point3D cameraPos, Point3D focusPoint, float tiltDeg) {
    double camZ = cameraPos.z;
    double clipNear = camZ * 0.9;
    clipNear = Math.min(clipNear, Math.max(camZ - Const.MAX_HEIGHT, Const.MIN_NEAR));
    if (Math.abs(tiltDeg - Const.HALF_FOV_ANGLE_Y) < 90) {
      // Put near plane to intersection between frustum and ground plane
      double cosAminusB = Math.cos((tiltDeg - Const.HALF_FOV_ANGLE_Y) * Const.DEG_TO_RAD);
      double cosB = Math.cos(Const.HALF_FOV_ANGLE_Y * Const.DEG_TO_RAD);
      clipNear = clipNear * cosB / cosAminusB;
    }
    clipNear = Math.min(clipNear, Const.MAX_NEAR);
    return (float) clipNear;
  }

  @Override
  public float getFarPlane(Point3D cameraPos, Point3D focusPoint, float tiltDeg, double drawDistance) {
    double camZ = cameraPos.z;
    double clipFar = camZ * drawDistance;
    if (tiltDeg + Const.HALF_FOV_ANGLE_Y < 90) {
      // Put far plane to intersection between frustum and ground plane
      double cosAplusB = Math.cos((tiltDeg + Const.HALF_FOV_ANGLE_Y) * Const.DEG_TO_RAD);
      double cosB = Math.cos(Const.HALF_FOV_ANGLE_Y * Const.DEG_TO_RAD);
      double distance = camZ * cosB / cosAplusB;
      clipFar = Math.min(clipFar, 1.1 * distance); // put far plane a bit further to avoid precision issues
    }
    return (float) clipFar;
  }
  
  @Override
  public double[] calculateIntersections(Point3D origin, Vector3D direction, boolean closest) {
    double denom = direction.z;
    if (denom == 0) {
      if (closest) {
        return new double[] { 0 };
      }
      return new double[] { };
    }

    double t = -origin.z / denom;
    return new double[] { t };
  }

}
