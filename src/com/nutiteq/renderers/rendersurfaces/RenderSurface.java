package com.nutiteq.renderers.rendersurfaces;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.TriangleMesh;

/**
 * Render surface interface.
 */
public interface RenderSurface {
  
  RenderProjection getRenderProjection();

  float getBestZoom0Distance(float width, float height, int tileSize);

  float getNearPlane(Point3D cameraPos, Point3D focusPoint, float tiltDeg);

  float getFarPlane(Point3D cameraPos, Point3D focusPoint, float tiltDeg, double drawDistance);
  
  Envelope getUnprojectedEnvelope(Point3D[] points);

  Point3D[] getContourVertices(Point3D cameraPos);
  
  TriangleMesh getSurfaceTriangles();

  Point3D getWrappedPoint(Point3D point);
  
  Point3D getGroundPoint(Point3D point);
  
  Point3D getRotationOrigin(Point3D point);

  int getTileTesselationFactor(Projection projection, int zoom, int axis);

  float getTileSubdivisionFactor();
  
  boolean isSeamlessHorizontalPan();

  double[] calculateIntersections(Point3D origin, Vector3D direction, boolean closest);
}
