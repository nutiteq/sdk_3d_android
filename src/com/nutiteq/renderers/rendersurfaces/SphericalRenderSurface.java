package com.nutiteq.renderers.rendersurfaces;

import java.util.ArrayList;
import java.util.List;

import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.MutablePoint3D;
import com.nutiteq.components.MutableVector3D;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.renderprojections.SphericalRenderProjection;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.GeomUtils;
import com.nutiteq.utils.TriangleMesh;

/**
 * Spherical render surface.
 */
public class SphericalRenderSurface implements RenderSurface {
  private static final int TILE_TESSELATION_MAX_ZOOM_LEVEL = 6;
  private static final int CONTOUR_TESSELATION_LEVELS = 10;
  private static final int SURFACE_TESSELATION_LEVELS = 64;
  private static final double ENVELOPE_DIVIDE_THRESHOLD_PER_DEGREE = 18.0;

  private final RenderProjection renderProjection;

  public SphericalRenderSurface() {
    this.renderProjection = new SphericalRenderProjection(Const.EARTH_RADIUS, Const.UNIT_SIZE);
  }
  
  @Override
  public RenderProjection getRenderProjection() {
    return renderProjection;
  }

  @Override
  public Point3D getWrappedPoint(Point3D point) {
    return point;
  }

  @Override
  public Point3D getGroundPoint(Point3D point) {
    double length = Math.sqrt(point.x * point.x + point.y * point.y + point.z * point.z);
    double scale = Const.UNIT_SIZE / length; 
    return new Point3D(point.x * scale, point.y * scale, point.z * scale);
  }

  @Override
  public Point3D getRotationOrigin(Point3D point) {
    return new Point3D(0, 0, 0);
  }

  @Override
  public Envelope getUnprojectedEnvelope(Point3D[] points) {
    List<MapPos> mapPoints = new ArrayList<MapPos>();

    // Tesselate and unproject along all points pairs
    MutableMapPos mapPos = new MutableMapPos();
    MutableMapPos mapPos0 = new MutableMapPos();
    MutableMapPos mapPos1 = new MutableMapPos();
    double minU =  Double.MAX_VALUE;
    double maxU = -Double.MAX_VALUE;
    double minV =  Double.MAX_VALUE;
    double maxV = -Double.MAX_VALUE;
    for (int i = 0; i < points.length - 1; i++) {
      Point3D p0 = points[i];
      renderProjection.unproject(p0.x, p0.y, p0.z, mapPos0);
      double cos0 = Math.cos(mapPos0.y * Math.PI / 180.0);

      for (int j = i + 1; j < points.length; j++) {
        Point3D p1 = points[j];
        renderProjection.unproject(p1.x, p1.y, p1.z, mapPos1);
        double cos1 = Math.cos(mapPos1.y * Math.PI / 180.0);

        double divideThreshold = ENVELOPE_DIVIDE_THRESHOLD_PER_DEGREE * Math.max(0.1, Math.min(cos0, cos1));
        int levels = (int) Math.ceil(renderProjection.getDistance(p0, p1) / Const.UNIT_SIZE / Math.PI * 180 / divideThreshold);

        double[] transform = renderProjection.getTranslateMatrix(p0, p1, levels > 0 ? 1.0 / levels : 0);

        Point3D p = p0;
        for (int k = 0; k <= levels; k++) {
          if (k > 0) {
            p = GeomUtils.transform(p, transform);
          }
          renderProjection.unproject(p.x, p.y, p.z, mapPos);
          mapPoints.add(new MapPos(mapPos));

          minU = Math.min(minU, mapPos.x);
          maxU = Math.max(maxU, mapPos.x);
          minV = Math.min(minV, mapPos.y);
          maxV = Math.max(maxV, mapPos.y);
        }
      }
    }

    // HACK: detect poles, enlarge envelope
    if (maxU - minU > 90) {
      if (minV < 0 && maxV > 0) {
        return new Envelope(-180, 180, -90, 90);
      }

      if (minV < 0) {
        mapPoints.add(new MapPos(-180, -90));
        mapPoints.add(new MapPos( 180, -90));
        mapPoints.add(new MapPos(-180, maxV));
        mapPoints.add(new MapPos( 180, maxV));
        minV = -90;
      }
      if (maxV > 0) {
        mapPoints.add(new MapPos(-180, minV));
        mapPoints.add(new MapPos( 180, minV));
        mapPoints.add(new MapPos(-180, 90));
        mapPoints.add(new MapPos( 180, 90));
        maxV = 90;
      }

      minU = -180;
      maxU = 180;
    }

    // If point cloud too complex, simply return rectangular bounds
    if (mapPoints.size() > 16) {
      return new Envelope(minU, maxU, minV, maxV);
    }

    // Calculate convex hull of point cloud
    MapPos[] hullPoints = GeomUtils.calculateConvexHull(mapPoints.toArray(new MapPos[mapPoints.size()]));
    return new Envelope(hullPoints);
  }

  @Override
  public Point3D[] getContourVertices(Point3D cameraPos) {
    // Contour plane equation: <normal, x> = r^2 for all x where normal == cameraPos
    Vector3D normal = new Vector3D(cameraPos.x, cameraPos.y, cameraPos.z);
    Vector3D v1 = Vector3D.crossProduct(normal, new Vector3D(1, 0, 0));
    Vector3D v2 = Vector3D.crossProduct(normal, new Vector3D(0, 1, 0));
    Vector3D v3 = Vector3D.crossProduct(normal, new Vector3D(0, 0, 1));
    if (v2.getLength() > v1.getLength()) {
      v1 = v2;
    }
    if (v3.getLength() > v1.getLength()) {
      v1 = v3;
    }

    // Use longest projected base axis as axis 1
    v1 = v1.getNormalized();

    // Use orthonal unit vector to normal, axis 1 as axis 2
    v2 = Vector3D.crossProduct(normal, v1).getNormalized();

    // Solve <normal, t * normal> = r^2 for t, use normal * t as axis 3
    double r2 = Const.UNIT_SIZE * Const.UNIT_SIZE;
    double t = r2 / Vector3D.dotProduct(normal, normal);
    v3 = new Vector3D(normal.x * t, normal.y * t, normal.z * t);

    // Calculate radius of ring at sphere/plane intersection
    double r = Math.sqrt(Math.max(0, r2 - Vector3D.dotProduct(v3, v3)));
    // Build contour vertices (clockwise)
    Point3D[] contour = new Point3D[360 / CONTOUR_TESSELATION_LEVELS];
    for (int i = 0; i < contour.length; i++) {
      float angle = -(float) i * CONTOUR_TESSELATION_LEVELS * Const.DEG_TO_RAD;
      double x1 = Math.cos(angle) * r;
      double x2 = Math.sin(angle) * r;
      Point3D p = new Point3D(v1.x * x1 + v2.x * x2 + v3.x, v1.y * x1 + v2.y * x2 + v3.y, v1.z * x1 + v2.z * x2 + v3.z);
      contour[i] = p;
    }
    return contour;
  }

  @Override
  public TriangleMesh getSurfaceTriangles() {
    int vertices = (SURFACE_TESSELATION_LEVELS + 1) * (SURFACE_TESSELATION_LEVELS + 1);
    int indices = 2 * SURFACE_TESSELATION_LEVELS * SURFACE_TESSELATION_LEVELS;
    TriangleMesh.Builder meshBuilder = TriangleMesh.builder(vertices, vertices, vertices, indices);

    // Note: we use simple longitude/latitude tesselation scheme. Recursive tetrahedra-based surface would contain fewer vertices but produces texture artifacts near poles
    MutablePoint3D point = new MutablePoint3D();
    MutableVector3D normal = new MutableVector3D();
    int tesselateU = SURFACE_TESSELATION_LEVELS;
    int tesselateV = SURFACE_TESSELATION_LEVELS;
    for (int j = 0; j <= tesselateV; j++) {
      float t = 1.0f - 1.0f * j / tesselateV;
      double v = 180.0 * j / tesselateV - 90;
      for (int i = 0; i <= tesselateU; i++) {
        float s = 2.0f * i / tesselateU;
        double u = 360.0 * i / tesselateU - 180;

        renderProjection.project(u, v, 0, point);
        renderProjection.setNormal(point.x, point.y, point.z, normal);
        meshBuilder.addVertex(point.x, point.y, point.z);
        meshBuilder.addNormal(normal.x, normal.y, normal.z);
        meshBuilder.addTexCoord(s, t);
      }
    }

    for (int j = 0; j < tesselateV; j++) {
      for (int i = 0; i < tesselateU; i++) {
        int i00 = (i + 0) + (j + 0) * (tesselateU + 1);
        int i01 = (i + 0) + (j + 1) * (tesselateU + 1);
        int i10 = (i + 1) + (j + 0) * (tesselateU + 1);
        int i11 = (i + 1) + (j + 1) * (tesselateU + 1);

        meshBuilder.addTriangle(i00, i10, i01);
        meshBuilder.addTriangle(i10, i11, i01);
      }
    }
    return meshBuilder.build();
  }

  @Override
  public boolean isSeamlessHorizontalPan() {
    return false;
  }

  @Override
  public int getTileTesselationFactor(Projection projection, int zoom, int axis) {
    if (zoom >= TILE_TESSELATION_MAX_ZOOM_LEVEL) {
      return 1;
    }
    return 1 << (TILE_TESSELATION_MAX_ZOOM_LEVEL - zoom);
  }

  @Override
  public float getTileSubdivisionFactor() {
    return 1;
  }

  @Override
  public float getBestZoom0Distance(float width, float height, int tileSize) {
    float bestZoom0Distance = (float) (height * Const.UNIT_SIZE / (tileSize * Const.HALF_FOV_TAN_Y));
    return bestZoom0Distance;
  }

  @Override
  public float getNearPlane(Point3D cameraPos, Point3D focusPoint, float tiltDeg) {
    double camDistance = new Vector3D(cameraPos, focusPoint).getLength();
    double clipNear = camDistance * 0.9;
    clipNear = Math.min(clipNear, Math.max(camDistance - Const.MAX_HEIGHT, Const.MIN_NEAR));
    if (Math.abs(tiltDeg - Const.HALF_FOV_ANGLE_Y) < 90) {
      // Put near plane to intersection between frustum and ground plane
      double cosAminusB = Math.cos((tiltDeg - Const.HALF_FOV_ANGLE_Y) * Const.DEG_TO_RAD);
      double cosB = Math.cos(Const.HALF_FOV_ANGLE_Y * Const.DEG_TO_RAD);
      clipNear = clipNear * cosB / cosAminusB / 2; // put near plane a bit closer due to curvature
    }
    clipNear = Math.min(clipNear, Const.MAX_NEAR);
    return (float) clipNear;
  }

  @Override
  public float getFarPlane(Point3D cameraPos, Point3D focusPoint, float tiltDeg, double drawDistance) {
    double camDistance = new Vector3D(cameraPos, focusPoint).getLength();
    double clipFar = camDistance * drawDistance;
    if (tiltDeg + Const.HALF_FOV_ANGLE_Y < 90) {
      // Put far plane to intersection between frustum and ground plane
      double cosAplusB = Math.cos((tiltDeg + Const.HALF_FOV_ANGLE_Y) * Const.DEG_TO_RAD);
      double cosB = Math.cos(Const.HALF_FOV_ANGLE_Y * Const.DEG_TO_RAD);
      double distance = camDistance * cosB / cosAplusB;
      clipFar = Math.min(clipFar, 1.1 * distance * Math.PI / 2); // put far plane a bit further to avoid precision issues, also be conservative due to curvature
    }
    
    double minClipFar = Const.UNIT_SIZE * (1 - Math.cos(2 * Math.PI / SURFACE_TESSELATION_LEVELS)); // max distance from sphere point to underlying tesselated surface
    return (float) Math.max(clipFar, minClipFar);
  }

  @Override
  public double[] calculateIntersections(Point3D origin, Vector3D direction, boolean closest) {
    double r2inv = 1.0 / (Const.UNIT_SIZE * Const.UNIT_SIZE);
    double a = (direction.x * direction.x + direction.y * direction.y + direction.z * direction.z) * r2inv;
    double b = (origin.x * direction.x + origin.y * direction.y + origin.z * direction.z) * 2 * r2inv;
    double c = (origin.x * origin.x + origin.y * origin.y + origin.z * origin.z) * r2inv - 1.0;

    double d = b * b - 4 * a * c;
    if (d < 0) {
      // Misses
      if (closest) {
        double t = -0.5 * b / a;
        return new double[] { t };
      }
      return new double[] { };
    } else if (d == 0) {
      // Touches only
      double t = -0.5 * b / a;
      return new double[] { t };
    }

    // General case - two intersection points
    d = Math.sqrt(d);
    double t1 = -0.5 * (b + d) / a;
    double t2 = -0.5 * (b - d) / a;

    if (t1 < t2) {
      return new double[] { t1, t2 };
    } else {
      return new double[] { t2, t1 };
    }
  }

}
