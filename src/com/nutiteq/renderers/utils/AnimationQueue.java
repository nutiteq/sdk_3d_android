package com.nutiteq.renderers.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import com.nutiteq.components.Point3D;
import com.nutiteq.log.Log;
import com.nutiteq.renderers.MapRenderer;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.GeomUtils;

/**
 * Class for synchronous animation handling.
 */
public class AnimationQueue {
  // Animation types
  public static final int MOVE_ANIMATION   = 0;
  public static final int PAN_ANIMATION    = 1;
  public static final int ROTATE_ANIMATION = 2;
  public static final int ZOOM_ANIMATION   = 3;
  public static final int TILT_ANIMATION   = 4;
  
  /**
   * Animation descriptor. Includes animation types, timing, start and end values/positions.
   */
  private class Animation {
    public final int type;
    public final long startTime;
    public long endTime;
    public final float startValue;
    public final float endValue;
    public final Point3D startPos;
    public final Point3D endPos;
    
    public Animation(int type, int duration, float endValue) {
      float startValue = 0;
      switch (type) {
      case ROTATE_ANIMATION:
        startValue = mapRenderer.getRotation();
        startValue = (startValue % 360 + 360) % 360;
        endValue = (endValue % 360 + 360) % 360;
        if (Math.abs(endValue - startValue) > 180) {
          if (endValue > startValue) {
            startValue += 360;
          } else {
            startValue -= 360;
          }
        }
        break;
      case ZOOM_ANIMATION:
        startValue = mapRenderer.getZoom();
        break;
      case TILT_ANIMATION:
        startValue = mapRenderer.getTilt();
        break;
      default:
        Log.debug("AnimationQueue: unsupported animation type " + type);
        break;
      }
      this.type = type;
      this.startTime = System.currentTimeMillis();
      this.endTime = this.startTime + duration;
      this.startValue = startValue;
      this.endValue = endValue;
      this.startPos = null;
      this.endPos = null;
    }

    public Animation(int type, int duration, Point3D endPos) {
      Point3D startPos = endPos;
      switch (type) {
      case MOVE_ANIMATION:
      case PAN_ANIMATION:
        startPos = mapRenderer.getFocusPoint();
        break;
      default:
        Log.debug("AnimationQueue: unsupported animation type " + type);
        break;
      }
      this.type = type;
      this.startTime = System.currentTimeMillis();
      this.endTime = this.startTime + duration;
      this.startValue = 0;
      this.endValue = 0;
      this.startPos = startPos;
      this.endPos = endPos;
    }
    
    public void setDuration(int duration) {
      this.endTime = this.startTime + duration;
    }
    
    public float interpolateValue(float t) {
      return startValue + (endValue - startValue) * t;
    }

    public Point3D interpolatePos(float t) {
      double[] transform = mapRenderer.getRenderSurface().getRenderProjection().getTranslateMatrix(startPos, endPos, t);
      return GeomUtils.transform(startPos, transform);
    }
    
    public void apply(float t) {
      switch (type) {
      case MOVE_ANIMATION:
      case PAN_ANIMATION:
        Point3D pos = interpolatePos(t);
        mapRenderer.updateFocusPoint(pos.x, pos.y, pos.z, type == MOVE_ANIMATION);
        break;
      case ROTATE_ANIMATION:
        mapRenderer.updateRotation(interpolateValue(t));
        break;
      case ZOOM_ANIMATION:
        mapRenderer.updateZoom(interpolateValue(t));
        break;
      case TILT_ANIMATION:
        mapRenderer.updateTilt(interpolateValue(t));
        break;
      }
    }
  }
  
  private final List<Animation> queue = new LinkedList<Animation>();
  private final Interpolator interpolator = new AccelerateDecelerateInterpolator();
  private final MapRenderer mapRenderer;
  
  public AnimationQueue(MapRenderer mapRenderer) {
    this.mapRenderer = mapRenderer;
  }
  
  public void add(int type, int duration, float value) {
    Animation newAnim = new Animation(type, duration, value);
    synchronized (queue) {
      for (ListIterator<Animation> it = queue.listIterator(); it.hasNext(); ) {
        Animation oldAnim = it.next();
        if (!isSameType(type, oldAnim.type)) {
          continue;
        }
      
        // Remove old animation of the same type and calculate new animation duration
        float dx = newAnim.endValue - newAnim.startValue;
        float dx_dt = dx / duration;
        float real_dx = value - newAnim.startValue;
        float real_dt = real_dx / dx_dt;
        newAnim.setDuration(Math.max(1, Math.min(duration * 2, (int) real_dt)));
        it.remove();
      }
      queue.add(newAnim);
    }
    mapRenderer.requestRenderView();
  }
  
  public void add(int type, int duration, Point3D pos) {
    RenderProjection renderProjection = mapRenderer.getRenderSurface().getRenderProjection();

    Animation newAnim = new Animation(type, duration, pos);
    synchronized (queue) {
      for (ListIterator<Animation> it = queue.listIterator(); it.hasNext(); ) {
        Animation oldAnim = it.next();
        if (!isSameType(type, oldAnim.type)) {
          continue;
        }

        // Remove old animation of the same type and calculate new animation duration
        float dx = (float) renderProjection.getDistance(newAnim.startPos, newAnim.endPos);
        float dx_dt = dx / duration;
        float real_dx = (float) renderProjection.getDistance(newAnim.startPos, pos);
        float real_dt = real_dx / dx_dt;
        newAnim.setDuration(Math.max(1, Math.min(duration * 2, (int) real_dt)));
        it.remove();
      }
      queue.add(newAnim);
    }
    mapRenderer.requestRenderView();
  }
  
  public void remove(int type) {
    synchronized (queue) {
      for (ListIterator<Animation> it = queue.listIterator(); it.hasNext(); ) {
        Animation oldAnim = it.next();
        if (isSameType(type, oldAnim.type)) {
          it.remove();
        }
      }
    }
  }
  
  public void update() {
    long time = System.currentTimeMillis();

    List<Animation> activeAnims;
    synchronized (queue) {
      if (queue.isEmpty()) {
        return;
      }
      activeAnims = new LinkedList<Animation>(queue);
      for (ListIterator<Animation> it = queue.listIterator(); it.hasNext(); ) {
        Animation anim = it.next();
        if (time > anim.endTime) {
          it.remove();
        }
      }
    }
    for (Animation anim : activeAnims) {
      float t = (float) (time - anim.startTime) / (anim.endTime - anim.startTime);
      t = interpolator.getInterpolation(Math.min(1.0f, t));
      anim.apply(t);
    }
    mapRenderer.requestRenderView();
  }
  
  public void flush() {
    List<Animation> activeAnims;
    synchronized (queue) {
      activeAnims = new LinkedList<Animation>(queue);
      queue.clear();
    }
    for (Animation anim : activeAnims) {
      anim.apply(1.0f);
    }
  }
  
  private boolean isSameType(int type1, int type2) {
    if (type1 == type2) {
      return true;
    }
    return (type1 == MOVE_ANIMATION || type1 == PAN_ANIMATION) && (type2 == MOVE_ANIMATION || type2 == PAN_ANIMATION);
  }
}
