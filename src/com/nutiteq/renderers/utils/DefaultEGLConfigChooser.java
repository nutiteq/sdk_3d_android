package com.nutiteq.renderers.utils;

import java.util.Arrays;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import android.opengl.GLSurfaceView.EGLConfigChooser;

import com.nutiteq.log.Log;

public class DefaultEGLConfigChooser implements EGLConfigChooser {
  /**
   * Blacklists for devices - devices that have buggy OpenGL implementations and require forcing into non-default rendering mode
   * Lists contain os.android.Build.BOARD values - http://www.glbenchmark.com/ can be used to identify them.
   * A PlayStore app called "android.os.Build" can be used to identify the device, board name is displayed on the first line.
   */
  private static final String[] EGL_MULTISAMPLING_BLACKLIST = new String[] {
    "primou", "primoc",     // HTC One V
    "saga", "spade",        // HTC Desire S
    "ace",                  // HTC Desire HD
    "magnids",              // HTC Desire SV
    "vision",               // HTC Desire Z
    "vivow",                // HTC Incredible S
    "mahimahi",             // Nexus One
    "MSM8225",              // Galaxy Win Duos (GT-i8552)
    "7x27",                 // Galaxy Trend
  };
  
  private final int stencilBits;
  
  public DefaultEGLConfigChooser(int stencilBits) {
    this.stencilBits = stencilBits;
  }

  @Override
  public EGLConfig chooseConfig(EGL10 egl, EGLDisplay eglDisplay) {
    int[][] attribTables = new int[][] {
        // 8-8-8-bit color, 4x multisampling, 24-bit z buffer. Should work on most devices.
        new int[] { EGL10.EGL_RED_SIZE, 8, EGL10.EGL_GREEN_SIZE, 8, EGL10.EGL_BLUE_SIZE, 8, EGL10.EGL_DEPTH_SIZE, 24, EGL10.EGL_STENCIL_SIZE, stencilBits, EGL10.EGL_SAMPLE_BUFFERS, 1, EGL10.EGL_SAMPLES, 4, EGL10.EGL_NONE },
        // 8-8-8-bit color, 24-bit z buffer. Should work on most devices.
        new int[] { EGL10.EGL_RED_SIZE, 8, EGL10.EGL_GREEN_SIZE, 8, EGL10.EGL_BLUE_SIZE, 8, EGL10.EGL_DEPTH_SIZE, 24, EGL10.EGL_STENCIL_SIZE, stencilBits, EGL10.EGL_NONE },
        // 5-6-5-bit color, 24-bit z buffer. Should work on most devices.
        new int[] { EGL10.EGL_RED_SIZE, 5, EGL10.EGL_GREEN_SIZE, 6, EGL10.EGL_BLUE_SIZE, 5, EGL10.EGL_DEPTH_SIZE, 24, EGL10.EGL_STENCIL_SIZE, stencilBits, EGL10.EGL_NONE },
        // 8-8-8-bit color, 16-bit z buffer. Better than 5-6-5/16 bit, should also fix problems on some obscure devices.
        new int[] { EGL10.EGL_RED_SIZE, 8, EGL10.EGL_GREEN_SIZE, 8, EGL10.EGL_BLUE_SIZE, 8, EGL10.EGL_DEPTH_SIZE, 16, EGL10.EGL_STENCIL_SIZE, stencilBits, EGL10.EGL_NONE },
        // 5-6-5-bit color, 16-bit z buffer. Fallback for original Tegra devices. 
        new int[] { EGL10.EGL_RED_SIZE, 5, EGL10.EGL_GREEN_SIZE, 6, EGL10.EGL_BLUE_SIZE, 5, EGL10.EGL_DEPTH_SIZE, 16, EGL10.EGL_STENCIL_SIZE, stencilBits, EGL10.EGL_NONE },
    };
    boolean multisamplingSupported = !Arrays.asList(EGL_MULTISAMPLING_BLACKLIST).contains(android.os.Build.BOARD);
    if (!multisamplingSupported) {
      Log.debug("DefaultEGLConfigChooser: found board in multisampling blacklist: " + android.os.Build.BOARD);
    }
    for (int i = 0; i < attribTables.length; i++) {
      boolean disableConfig = false;
      if (!multisamplingSupported) {
        for (int j = 0; j + 1 < attribTables[i].length; j++) {
          if (attribTables[i][j] == EGL10.EGL_SAMPLE_BUFFERS && attribTables[i][j + 1] > 0) {
            disableConfig = true;
          }
        }
      }
      if (disableConfig) {
        continue;
      }
      int[] numConfigs = new int[] { 0 };
      EGLConfig[] configs = new EGLConfig[1];
      if (egl.eglChooseConfig(eglDisplay, attribTables[i], configs, 1, numConfigs)) {
        if (numConfigs[0] > 0) {
          Log.debug("DefaultEGLConfigChooser: selected display configuration: " + i);
          return configs[0];
        }
      }
    }
    throw new IllegalArgumentException("eglChooseConfig failed");
  }
}
