package com.nutiteq.renderers.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.CameraState;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.MutablePoint3D;
import com.nutiteq.components.MutableVector3D;
import com.nutiteq.components.Options;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderers.components.MapTileBounds;
import com.nutiteq.renderers.components.MapTileDrawData;
import com.nutiteq.renderers.components.MapTileProxy;
import com.nutiteq.renderers.components.MapTileQuadTreeNode;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.Frustum;
import com.nutiteq.utils.LongHashMap;
import com.nutiteq.utils.TriangleMesh;
import com.nutiteq.utils.Utils;

/**
 * Class for building tiles based on projection, render projection and current view.
 */
public class MapTileGenerator {
  private static final float SUBDIVISION_THRESHOLD = Const.UNIT_SIZE * 2; // Maximum distance from camera to tile, where the tile will be subdivided
  private static final int CACHED_TILE_BOUNDS_LEVELS = 5; // How many upper levels of tile bounds to cache
  private static final int MAX_TESSELATION_LEVEL = 7; // Maximum tesselation level allowed

  private final Projection baseProjection;
  private final Projection projection;
  private final int minTileZoom;
  private final int maxTileZoom;
  private final float projectionZoomLevelBias;
  private final RenderSurface renderSurface;
  private final RenderProjection renderProjection;
  private final boolean preloading;
  private final Options options;

  private LongHashMap<MapTileBounds> tileBoundsMapCenter = new LongHashMap<MapTileBounds>();
  private LongHashMap<MapTileBounds> tileBoundsMapLeft = new LongHashMap<MapTileBounds>();
  private LongHashMap<MapTileBounds> tileBoundsMapRight = new LongHashMap<MapTileBounds>();
  private MutableMapPos tilePointInternal = new MutableMapPos();
  private MutablePoint3D tilePointWorld = new MutablePoint3D();

  private double[] modelviewProjectionMatrix;
  private Frustum frustumView;
  private Frustum frustumCull;
  private Point3D cameraPos;
  private Point3D focusPoint;
  private MutableMapPos focusPointInternal = new MutableMapPos();
  private MutableMapPos focusPointLayerProj = new MutableMapPos();
  private int targetTileZoom;

  public MapTileGenerator(Projection baseProjection, Projection projection, int minZoom, int maxZoom, RenderSurface renderSurface, boolean preloading, Options options) {
    this.baseProjection = baseProjection;
    this.projection = projection;
    this.minTileZoom = minZoom;
    this.maxTileZoom = maxZoom;
    this.renderSurface = renderSurface;
    this.renderProjection = renderSurface.getRenderProjection();
    this.preloading = preloading;
    this.options = options;
    if (projection.equals(baseProjection)) {
      this.projectionZoomLevelBias = 0;
    } else {
      MapPos projP0 = projection.toInternal(new MapPos(projection.getBounds().left, projection.getBounds().top));
      MapPos projP1 = projection.toInternal(new MapPos(projection.getBounds().right, projection.getBounds().bottom));
      MapPos baseProjP0 = baseProjection.toInternal(new MapPos(baseProjection.getBounds().left, baseProjection.getBounds().top));
      MapPos baseProjP1 = baseProjection.toInternal(new MapPos(baseProjection.getBounds().right, baseProjection.getBounds().bottom));
      double scaleX = Math.abs((projP0.x - projP1.x) / (baseProjP0.x - baseProjP1.x));
      double scaleY = Math.abs((projP0.y - projP1.y) / (baseProjP0.y - baseProjP1.y));
      this.projectionZoomLevelBias = (float) (Math.log(Math.min(scaleX, scaleY)) / Math.log(2));
    }
  }
  
  public MapTileQuadTreeNode findMapTile(CameraState camera, MapPos mapPos, int zoom) {
    setupCamera(camera);

    List<MapTileQuadTreeNode> tiles = createRootTiles(tileBoundsMapCenter, new MapPos(0, 0));
    if (renderSurface.isSeamlessHorizontalPan() && projection == baseProjection) {
      tiles.addAll(createRootTiles(tileBoundsMapLeft, new MapPos(-projection.getBounds().getWidth(), 0)));
      tiles.addAll(createRootTiles(tileBoundsMapRight, new MapPos(projection.getBounds().getWidth(), 0)));
    }

    for (MapTileQuadTreeNode tile : tiles) {
      // If no intersection with this tile, skip
      if (!(mapPos.x >= tile.minX && mapPos.x <= tile.maxX && mapPos.y >= tile.minY && mapPos.y <= tile.maxY)) {
        continue;
      }

      // Start from root and move downwards until desired zoom level is reached
      while (tile.zoom < Const.MAX_SUPPORTED_ZOOM_LEVEL) {
        if (zoom < 0) {
          double tileX = Utils.toRange(focusPointLayerProj.x, tile.minX, tile.maxX);
          double tileY = Utils.toRange(focusPointLayerProj.y, tile.minY, tile.maxY);
          projection.toInternal(tileX, tileY, 0, tilePointInternal);
          renderProjection.project(tilePointInternal.x, tilePointInternal.y, 0, tilePointWorld);

          // Subdivision calculation depends whether current tile is inside visible frustum or not - if it is, use tile distance from camera plane. Otherwise use projection distance.
          double tileZoomPow2 = Math.pow(2, tile.zoom - projectionZoomLevelBias - options.getTileZoomLevelBias());
          double tileW = tilePointWorld.x * modelviewProjectionMatrix[3] + modelviewProjectionMatrix[7] + tilePointWorld.z * modelviewProjectionMatrix[11] + camera.modelviewProjectionMatrix[15];
          double zoomDistance = tileW * tileZoomPow2;
          double subdivisionThreshold = SUBDIVISION_THRESHOLD * renderSurface.getTileSubdivisionFactor() * Math.sqrt(2.0);
          boolean subDivide = zoomDistance < subdivisionThreshold;
          if (minTileZoom > tile.zoom) {
            subDivide = true;
          } else if (maxTileZoom <= tile.zoom || targetTileZoom <= tile.zoom) {
            subDivide = false;
          }
          if (!subDivide) {
            break;
          }
        } else {
          if (zoom <= tile.zoom) {
            break;
          }
        }
        tile = new MapTileQuadTreeNode(this, tile, tile.getChildFromPoint(mapPos));
      }
      return tile;
    }
    return null;
  }
  
  public Projection getBaseProjection() {
    return baseProjection;
  }

  public Bounds getRasterTileBounds(int x, int y, int zoom) {
    Bounds bounds = projection.getBounds();
    int xCount = Math.max(1, (int) Math.round(bounds.getWidth() / bounds.getHeight())) * (1 << zoom);
    int yCount = Math.max(1, (int) Math.round(bounds.getHeight() / bounds.getWidth())) * (1 << zoom);
    
    double s0 = (double) (y + 0) / yCount;
    double s1 = (double) (y + 1) / yCount;

    double t0 = (double) (x + 0) / xCount;
    double t1 = (double) (x + 1) / xCount;

    return new Bounds(bounds.left + bounds.getWidth() * t0, bounds.bottom + bounds.getHeight() * (1 - s0), bounds.left + bounds.getWidth() * t1, bounds.bottom + bounds.getHeight() * (1 - s1));
  }

  public void generateMapTiles(CameraState camera, List<MapTileQuadTreeNode> visibleTiles, List<MapTileQuadTreeNode> preloadTiles) {
    // Calculate initial set of tiles (both visible and preload)
    setupCamera(camera);
    calculateTiles(visibleTiles, preloadTiles);

    // Calculate visible tile tesselation levels, fix cracks between adjacent tiles
    calculateTileTesselationLevels(visibleTiles);
    fixTileTesselationLevels(visibleTiles);
    propagateTileTesselationLevels(visibleTiles);
 
    // Calculate preload tile tesselation levels, do not fix cracks here
    // Note: we do not fix cracks as it may cause very high overtesselation, but once preload tiles become visible, the cracks will be fixed during next iteration
    calculateTileTesselationLevels(preloadTiles);
    propagateTileTesselationLevels(preloadTiles);
  }

  private void setupCamera(CameraState camera) {
    modelviewProjectionMatrix = camera.modelviewProjectionMatrix;
    cameraPos = camera.cameraPos;
    focusPoint = camera.focusPoint;
    targetTileZoom = (int) (camera.zoom + projectionZoomLevelBias + options.getTileZoomLevelBias() + Const.DISCRETE_ZOOM_LEVEL_BIAS);
    renderProjection.unproject(focusPoint.x, focusPoint.y, focusPoint.z, focusPointInternal);
    projection.fromInternal(focusPointInternal.x, focusPointInternal.y, 0, focusPointLayerProj);

    frustumView = new Frustum(camera.projectionMatrix, camera.modelviewMatrix);

    // If preloading is enabled culling has to performed with larger frustum
    if (!preloading) {
      frustumCull = frustumView;
    } else {
      // Look at Matrix.frustumReinitializeM for explanation
      float[] projectionMatrix = camera.projectionMatrix.clone();
      float coeff = Math.max(1, options.getHalfPreloadFOVTan() / Const.HALF_FOV_TAN_Y);
      projectionMatrix[0] /= coeff;
      projectionMatrix[5] /= coeff;
      projectionMatrix[8] /= coeff;
      projectionMatrix[9] /= coeff;
      frustumCull = new Frustum(projectionMatrix, camera.modelviewMatrix);
    }
  }
  
  private void calculateTiles(List<MapTileQuadTreeNode> visibleTiles, List<MapTileQuadTreeNode> preloadTiles) {
    List<MapTileQuadTreeNode> centerTiles = createRootTiles(tileBoundsMapCenter, new MapPos(0, 0));
    for (MapTileQuadTreeNode tile : centerTiles) {
      calculateTiles(tileBoundsMapCenter, tile, visibleTiles, preloadTiles);
    }

    if (renderSurface.isSeamlessHorizontalPan() && projection == baseProjection) {
      List<MapTileQuadTreeNode> leftTiles = createRootTiles(tileBoundsMapLeft, new MapPos(-projection.getBounds().getWidth(), 0));
      for (MapTileQuadTreeNode tile : leftTiles) {
        calculateTiles(tileBoundsMapLeft, tile, visibleTiles, preloadTiles);
      }

      List<MapTileQuadTreeNode> rightTiles = createRootTiles(tileBoundsMapRight, new MapPos(projection.getBounds().getWidth(), 0));
      for (MapTileQuadTreeNode tile : rightTiles) {
        calculateTiles(tileBoundsMapRight, tile, visibleTiles, preloadTiles);
      }
    }
  }
  
  private List<MapTileQuadTreeNode> createRootTiles(LongHashMap<MapTileBounds> boundsMap, MapPos origin) {
    List<MapTileQuadTreeNode> rootTiles = new ArrayList<MapTileQuadTreeNode>();

    Bounds bounds = projection.getBounds();
    int xCount = Math.max(1, (int) Math.round(bounds.getWidth() / bounds.getHeight()));
    int yCount = Math.max(1, (int) Math.round(bounds.getHeight() / bounds.getWidth()));
    for (int y = 0; y < yCount; y++) {
      for (int x = 0; x < xCount; x++) {        
        int rootId = x + y * xCount + xCount * yCount - 1; // for single root tile, rootId == 0, for dual root tiles, rootId = 1 or 2, ...
        Bounds rootBounds = getRasterTileBounds(x, y, 0);
        MapTileQuadTreeNode rootTile = new MapTileQuadTreeNode(this, origin, rootBounds, x, y, rootId);
        rootTile.bounds = getTileBounds(boundsMap, rootTile);
        rootTiles.add(rootTile);
      }
    }

    return rootTiles;
  }
  
  private void calculateTiles(LongHashMap<MapTileBounds> boundsMap, MapTileQuadTreeNode tile, List<MapTileQuadTreeNode> visibleTiles, List<MapTileQuadTreeNode> preloadTiles) {
    if (tile.zoom > Const.MAX_SUPPORTED_ZOOM_LEVEL) {
      return;
    }

    if (!tile.bounds.testIntersection(frustumCull, cameraPos)) {
      return;
    }

    // Find closest tile point to focus point
    double tileX = Utils.toRange(focusPointLayerProj.x, tile.minX, tile.maxX);
    double tileY = Utils.toRange(focusPointLayerProj.y, tile.minY, tile.maxY);
    projection.toInternal(tileX, tileY, 0, tilePointInternal);
    renderProjection.project(tilePointInternal.x, tilePointInternal.y, 0, tilePointWorld);

    // Subdivision calculation depends whether current tile is inside visible frustum or not - if it is, use tile distance from camera plane. Otherwise use projection distance.
    boolean visible = tile.bounds.testIntersection(frustumView, cameraPos);
    double tileZoomPow2 = Math.pow(2, tile.zoom - projectionZoomLevelBias - options.getTileZoomLevelBias());
    double tileW = tilePointWorld.x * modelviewProjectionMatrix[3] + tilePointWorld.y * modelviewProjectionMatrix[7] + tilePointWorld.z * modelviewProjectionMatrix[11] + modelviewProjectionMatrix[15];
    double zoomDistance = tileW * tileZoomPow2;
    if (!visible) {
      double dx = tilePointWorld.x - cameraPos.x;
      double dy = tilePointWorld.y - cameraPos.y;
      double dz = tilePointWorld.z - cameraPos.z;
      zoomDistance = Math.sqrt(dx * dx + dy * dy + dz * dz) * tileZoomPow2 * 2;
    }
    double subdivisionThreshold = SUBDIVISION_THRESHOLD * renderSurface.getTileSubdivisionFactor() * Math.sqrt(2.0);
    boolean subDivide = zoomDistance < subdivisionThreshold;
    if (minTileZoom > tile.zoom) {
      subDivide = true;
    } else if (maxTileZoom <= tile.zoom || targetTileZoom <= tile.zoom) {
      subDivide = false;
    }

    // Add child elements, these may be needed even when tile is not subdivided
    tile.topLeft = new MapTileQuadTreeNode(this, tile, MapTileQuadTreeNode.TOP_LEFT);
    tile.topLeft.bounds = getTileBounds(boundsMap, tile.topLeft);
    tile.topRight = new MapTileQuadTreeNode(this, tile, MapTileQuadTreeNode.TOP_RIGHT);
    tile.topRight.bounds = getTileBounds(boundsMap, tile.topRight);
    tile.bottomRight = new MapTileQuadTreeNode(this, tile, MapTileQuadTreeNode.BOTTOM_RIGHT);
    tile.bottomRight.bounds = getTileBounds(boundsMap, tile.bottomRight);
    tile.bottomLeft = new MapTileQuadTreeNode(this, tile, MapTileQuadTreeNode.BOTTOM_LEFT);
    tile.bottomLeft.bounds = getTileBounds(boundsMap, tile.bottomLeft);

    if (subDivide) {
      calculateTiles(boundsMap, tile.topLeft, visibleTiles, preloadTiles);
      calculateTiles(boundsMap, tile.topRight, visibleTiles, preloadTiles);
      calculateTiles(boundsMap, tile.bottomRight, visibleTiles, preloadTiles);
      calculateTiles(boundsMap, tile.bottomLeft, visibleTiles, preloadTiles);
    } else if (!visibleTiles.contains(tile)) {
      double dx = tilePointInternal.x - focusPointInternal.x;
      double dy = tilePointInternal.y - focusPointInternal.y;
      tile.distance = (float) Math.sqrt(dx * dx + dy * dy);
      if (visible) {
        visibleTiles.add(tile);
      } else {
        preloadTiles.add(tile);
      }
    }
  }
  
  private void calculateTileTesselationLevels(List<MapTileQuadTreeNode> tiles) {
    // Find initial zoom levels for all tiles
    for (MapTileQuadTreeNode tile : tiles) {
      tile.tesselateU = renderSurface.getTileTesselationFactor(projection, tile.zoom, 0);
      tile.tesselateV = renderSurface.getTileTesselationFactor(projection, tile.zoom, 1);
    }
  }
  
  private void fixTileTesselationLevels(List<MapTileQuadTreeNode> tiles) {
    // For all tile pairs, if there is overlap along an axis, use maximum tesselation factor to avoid cracks and T-vertices
    for (MapTileQuadTreeNode tile1 : tiles) {
      for (MapTileQuadTreeNode tile2 : tiles) {
        if (tile1.minX <= tile2.minX && tile1.maxX >= tile2.maxX) {
          int n = (int) Math.round((tile1.maxX - tile1.minX) / (tile2.maxX - tile2.minX));
          tile1.tesselateU = Math.max(tile1.tesselateU, tile2.tesselateU * n);
        }
        if (tile1.minY <= tile2.minY && tile1.maxY >= tile2.maxY) {
          int n = (int) Math.round((tile1.maxY - tile1.minY) / (tile2.maxY - tile2.minY));
          tile1.tesselateV = Math.max(tile1.tesselateV, tile2.tesselateV * n);
        }
      }
    }
    
    // Clip tesselation levels if necessary: this can introduce cracks but will avoid performance issues
    for (MapTileQuadTreeNode tile : tiles) {
      tile.tesselateU = Math.min(tile.tesselateU, 1 << MAX_TESSELATION_LEVEL);
      tile.tesselateV = Math.min(tile.tesselateV, 1 << MAX_TESSELATION_LEVEL);      
    }
  }
  
  private void propagateTileTesselationLevels(List<MapTileQuadTreeNode> tiles) {
    // Propagate calculated tesselation levels upwards (up to the root) and downwards one level
    for (MapTileQuadTreeNode tile : tiles) {
      int childTesselateU = Math.max(1, tile.tesselateU >> 1);
      int childTesselateV = Math.max(1, tile.tesselateV >> 1);
      tile.bottomLeft.tesselateU = childTesselateU;
      tile.bottomLeft.tesselateV = childTesselateV;
      tile.bottomRight.tesselateU = childTesselateU;
      tile.bottomRight.tesselateV = childTesselateV;
      tile.topLeft.tesselateU = childTesselateU;
      tile.topLeft.tesselateV = childTesselateV;
      tile.topRight.tesselateU = childTesselateU;
      tile.topRight.tesselateV = childTesselateV;

      // Note: for parent tesselation, we do not need to increase tesselation level as only slices of actual tile size are used
      while (tile.parent != null) {
        tile.parent.tesselateU = tile.tesselateU;
        tile.parent.tesselateV = tile.tesselateV;
        tile = tile.parent;
      }
    }
  }

  public MapTileDrawData createTileDrawData(MapTileProxy proxy) {
    MapTileQuadTreeNode tile = proxy.mapTile;
    int vertices = (tile.tesselateU + 1) * (tile.tesselateV + 1);
    int indices = 2 * tile.tesselateU * tile.tesselateV;
    TriangleMesh.Builder meshBuilder = TriangleMesh.builder(vertices, vertices, vertices, indices);
    RenderProjection.Transform transform = renderProjection.getTransform(projection);
    
    // Build vertex arrays
    MutablePoint3D point = new MutablePoint3D();
    MutableVector3D normal = new MutableVector3D();

    for (int j = 0; j <= tile.tesselateV; j++) {
      float t = (float) j / tile.tesselateV; 
      float v = proxy.tileY + proxy.tileSize * (1 - t);
      double y = (1 - t) * tile.minY + t * tile.maxY;
      
      for (int i = 0; i <= tile.tesselateU; i++) {
        float s = (float) i / tile.tesselateU; 
        float u = proxy.tileX + proxy.tileSize * s;
        double x = (1 - s) * tile.minX + s * tile.maxX;
        
        transform.project(x, y, 0, point);
        renderProjection.setNormal(point.x, point.y, point.z, normal);

        meshBuilder.addVertex(point.x, point.y, point.z);
        meshBuilder.addNormal(normal.x, normal.y, normal.z);
        meshBuilder.addTexCoord(u, v);
      }
    }
    
    // Build index array
    for (int j = 0; j < tile.tesselateV; j++) {
      for (int i = 0; i < tile.tesselateU; i++) {
        int i00 = (i + 0) + (j + 0) * (tile.tesselateU + 1);
        int i01 = (i + 0) + (j + 1) * (tile.tesselateU + 1);
        int i10 = (i + 1) + (j + 0) * (tile.tesselateU + 1);
        int i11 = (i + 1) + (j + 1) * (tile.tesselateU + 1);

        meshBuilder.addTriangle(i00, i10, i01);
        meshBuilder.addTriangle(i10, i11, i01);
      }
    }
    
    // Build tile
    TriangleMesh mesh = meshBuilder.build();

    FloatBuffer tileTexCoordBuf = ByteBuffer.allocateDirect(mesh.getTexCoords().length * Float.SIZE / 8).order(ByteOrder.nativeOrder()).asFloatBuffer();
    tileTexCoordBuf.put(mesh.getTexCoords());
    tileTexCoordBuf.position(0);

    ShortBuffer indexBuf = ByteBuffer.allocateDirect(mesh.getTriangleIndices().length * Short.SIZE / 8).order(ByteOrder.nativeOrder()).asShortBuffer();
    indexBuf.put(mesh.getTriangleIndices());
    indexBuf.position(0);

    return new MapTileDrawData(proxy, mesh.getVertices(), mesh.getNormals(), tileTexCoordBuf, indexBuf);
  }
  
  private MapTileBounds getTileBounds(LongHashMap<MapTileBounds> boundsMap, MapTileQuadTreeNode tile) {
    if (tile.zoom < CACHED_TILE_BOUNDS_LEVELS) {
      MapTileBounds tileBounds = boundsMap.get(tile.id);
      if (tileBounds == null) {
        tileBounds = calculateTileBounds(tile);
        boundsMap.put(tile.id, tileBounds);
      }
      return tileBounds;
    }
    return calculateTileBounds(tile);
  }

  private MapTileBounds calculateTileBounds(MapTileQuadTreeNode tile) {
    int tesselationU = renderSurface.getTileTesselationFactor(projection, tile.zoom, 0);
    int tesselationV = renderSurface.getTileTesselationFactor(projection, tile.zoom, 1);
    RenderProjection.Transform transform = renderProjection.getTransform(projection);

    // Find normal at the center of the tile
    MutablePoint3D centerPoint = new MutablePoint3D();
    MutableVector3D centerNormal = new MutableVector3D();
    transform.project(tile.centerX, tile.centerY, 0, centerPoint);
    renderProjection.setNormal(centerPoint.x, centerPoint.y, centerPoint.z, centerNormal);

    // Find bounds and maximum normal spread
    MutablePoint3D point = new MutablePoint3D();
    MutableVector3D normal = new MutableVector3D();

    double minDot = 1;
    double minX =  Double.MAX_VALUE, minY =  Double.MAX_VALUE, minZ =  Double.MAX_VALUE;
    double maxX = -Double.MAX_VALUE, maxY = -Double.MAX_VALUE, maxZ = -Double.MAX_VALUE;
    for (int j = 0; j <= tesselationV; j++) {
      double t = (double) j / tesselationV;
      double y = tile.minY + t * (tile.maxY - tile.minY);

      for (int i = 0; i <= tesselationU; i++) {
        double s = (double) i / tesselationU; 
        double x = tile.minX + s * (tile.maxX - tile.minX);

        transform.project(x, y, 0, point);
        renderProjection.setNormal(point.x, point.y, point.z, normal);
        double dot = centerNormal.x * normal.x +  centerNormal.y * normal.y + centerNormal.z * normal.z;

        minDot = Math.min(minDot, dot);
        minX = Math.min(point.x, minX); minY = Math.min(point.y, minY); minZ = Math.min(point.z, minZ);
        maxX = Math.max(point.x, maxX); maxY = Math.max(point.y, maxY); maxZ = Math.max(point.z, maxZ);
      }
    }
    float maxNormalSpread = minDot > 0.9999 ? 0 : (float) Math.acos(Utils.toRange(minDot, -1, 1));
    float safeMaxNormalSpread = Math.min(maxNormalSpread * 1.1f, (float) Math.PI); // increase max normal spread a bit due to tesselation errors
    return new MapTileBounds(minX, minY, minZ, maxX, maxY, maxZ, new Point3D(centerPoint), new Vector3D(centerNormal), safeMaxNormalSpread);
  }
}
