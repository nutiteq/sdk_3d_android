package com.nutiteq.renderers.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import android.graphics.Bitmap;

import com.nutiteq.components.TextureInfo;
import com.nutiteq.geometry.Text;
import com.nutiteq.geometry.Text.TextInfo;
import com.nutiteq.log.Log;
import com.nutiteq.style.TextStyle;
import com.nutiteq.utils.Utils;

public class TextAtlasGenerator {

  private static class TextAtlas {
    final Bitmap bitmap;
    final Map<TextInfo, TextAtlasItem> itemMap;
    
    TextAtlas(int width, int height, List<TextAtlasItem> items) {
      this.bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
      this.itemMap = new HashMap<TextInfo, TextAtlasItem>();
      for (TextAtlasItem item : items) {
        itemMap.put(item.text, item);
      }
    }
  }

  private static class TextAtlasItem {
    final int x;
    final int y;
    final TextInfo text;
    
    TextAtlasItem(int x, int y, TextInfo text) {
      this.x = x;
      this.y = y;
      this.text = text;
    }
  }
  
  public Map<Text, TextureInfo> generateMarkers(Collection<Text> texts) {
    int maxAtlasHeight = 512;
    
    // Build map: Text -> TextxtWithStyle
    Map<Text, TextInfo> textMap = new HashMap<Text, TextInfo>();
    for (Text text : texts) {
      TextInfo textInfo = new TextInfo(text.getText(), (TextStyle) text.getInternalState().activeStyle);
      textMap.put(text, textInfo);
    }
    
    // Sort texts by descending width
    List<TextInfo> textInfos = new ArrayList<TextInfo>(new HashSet<TextInfo>(textMap.values()));
    java.util.Collections.sort(textInfos, new Comparator<TextInfo>() {
      @Override
      public int compare(TextInfo text1, TextInfo text2) {
        return (int) (text2.textWidth - text1.textWidth);
      }
    });
    
    // Build atlas
    List<TextAtlas> atlases = new ArrayList<TextAtlas>();
    Map<TextInfo, TextAtlas> textAtlasMap = new HashMap<TextInfo, TextAtlas>();
    int index = 0;
    while (index < textInfos.size()) {
      int atlasHeight = 0;
      int atlasWidth = Utils.upperPow2((int) textInfos.get(index).textWidth);
      // TODO: check if atlasWidth is too large, downsample in this case
      int subAtlasX = Integer.MAX_VALUE;
      int subAtlasY = Integer.MAX_VALUE;
      List<TextAtlasItem> items = new ArrayList<TextAtlasItem>();
      while (index < textInfos.size()) {
        TextInfo textInfo = textInfos.get(index); 
        if (atlasHeight + textInfo.textHeight > maxAtlasHeight) {
          break;
        }
        if (textInfo.textWidth * 2 < atlasWidth) {
          if (Utils.upperPow2(atlasHeight) < Utils.upperPow2(atlasHeight + (int) textInfo.textHeight)) {
            break;
          }
          if (atlasHeight < subAtlasY) {
            subAtlasX = Utils.upperPow2((int) textInfo.textWidth);
            subAtlasY = atlasHeight;
          }
        }
        TextAtlasItem item = new TextAtlasItem(0, atlasHeight, textInfo);
        items.add(item);
        atlasHeight += (int) textInfo.textHeight;
        index++;
      }

      while (index < textInfos.size()) {
        int index1 = index;
        while (subAtlasX < atlasWidth && subAtlasY < atlasHeight) {
          int atlasX0 = subAtlasX;
          int atlasY0 = subAtlasY;
          subAtlasX = Integer.MAX_VALUE;
          subAtlasY = Integer.MAX_VALUE;
          int index2 = index;
          while (index < textInfos.size()) {
            TextInfo textInfo = textInfos.get(index); 
            if (atlasY0 + (int) textInfo.textHeight > atlasHeight) {
              break;
            }
            if (atlasX0 + (int) textInfo.textWidth * 2 < atlasWidth) {
              if (atlasY0 < subAtlasY) {
                subAtlasX = atlasX0 + Utils.upperPow2((int) textInfo.textWidth);
                subAtlasY = atlasY0;
              }
            }
            TextAtlasItem item = new TextAtlasItem(atlasX0, atlasY0, textInfo);
            items.add(item);
            atlasY0 += (int) textInfo.textHeight;
            index++;
          }
          if (index2 == index) {
            break;
          }
        }
        if (index1 == index) {
          break;
        }
      }
      
      if (items.isEmpty()) {
        Log.error("TextureAtlasGenerator: failed to add text " + textInfos.get(index));
        index++;
        continue;
      }
      
      atlasHeight = Utils.upperPow2(atlasHeight);
      TextAtlas atlas = new TextAtlas(atlasWidth, atlasHeight, items);
      atlases.add(atlas);
      
      for (TextAtlasItem item : items) {
        textAtlasMap.put(item.text, atlas);
      }
    }
    
    // Re-map texts to markers
    Map<Text, TextureInfo> textureMap = new HashMap<Text, TextureInfo>();
    for (Text text : texts) {
      TextInfo textInfo = textMap.get(text);
      if (textInfo == null) {
        continue;
      }
      TextAtlas atlas = textAtlasMap.get(textInfo);
      if (atlas == null) {
        continue;
      }
      TextAtlasItem item = atlas.itemMap.get(textInfo);
      if (item == null) {
        continue;
      }
      TextureInfo textureInfo = textInfo.createTextureInfo(atlas.bitmap, item.x, item.y, item.x, item.y, item.text.textWidth, item.text.textHeight);
      textureMap.put(text, textureInfo);
    }
    
    // Done
    int totalPixels = 0;
    for (TextAtlas atlas : atlases) {
      totalPixels += atlas.bitmap.getWidth() * atlas.bitmap.getHeight();
    }
    Log.debug("TextAtlasGenerator: built " + atlases.size() + " atlases, " + totalPixels + " pixels, " + textInfos.size() + " unique texts");
    return textureMap;
  }

}
