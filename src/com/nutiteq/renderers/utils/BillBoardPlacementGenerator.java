package com.nutiteq.renderers.utils;

import java.util.ArrayList;
import java.util.List;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableVector3D;
import com.nutiteq.components.Point3D;
import com.nutiteq.geometry.BillBoard;
import com.nutiteq.geometry.Line.EdgeInfo;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.BillBoardStyle;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.utils.GeomUtils;
import com.nutiteq.utils.Matrix;
import com.nutiteq.utils.Quadtree;

/**
 * Class for analyzing billboard placements, changing them and hiding based on billboard constraints.
 */
public class BillBoardPlacementGenerator {
  private static final int MAX_ITERATIONS = 5;
  private static final float MIN_DOT = 0.866f;
  private static MapPos[] SCREEN_BOUNDS = new MapPos[] { new MapPos(-1, -1), new MapPos(-1, 1), new MapPos(1, 1), new MapPos(1, -1) };

  /**
   * Placement attributes (position/orientation) for billboards. 
   */
  private static class Placement {
    final Point3D pos;
    final float rotationAngle;

    Placement(Point3D pos, float rotationAngle) {
      this.pos = pos;
      this.rotationAngle = rotationAngle;
    }
  }

  /**
   * Record for each billboard including style and tightly fitting bounding polygon.
   */
  private static class BillBoardRecord {
    BillBoard billBoard;
    BillBoardStyle style;
    MapPos[] boundingPolygon;

    BillBoardRecord(BillBoard billBoard) {
      this.billBoard = billBoard;
      this.style = (BillBoardStyle) billBoard.getInternalState().activeStyle;
    }
  }

  private final RenderProjection renderProjection;
  private final Quadtree<BillBoardRecord> quadtree = new Quadtree<BillBoardRecord>();
  private final BillBoardVertexBuilder vertexBuilder;
  private final FloatVertexBuffer vertexBuf = new FloatVertexBuffer();
  private CameraState cameraState;
  
  private float[] elementMVPVertex = new float[4];

  public BillBoardPlacementGenerator(RenderProjection renderProjection) {
    this.renderProjection = renderProjection;
    this.vertexBuilder = new BillBoardVertexBuilder(renderProjection);
  }
  
  public void setCamera(CameraState cameraState) {
    this.cameraState = cameraState;
    this.vertexBuilder.setCamera(cameraState);
  }

  public void clear() {
    quadtree.clear();
  }

  public boolean add(BillBoard element, boolean clip) {
    BillBoardRecord record = new BillBoardRecord(element);
    if (record.style == null) {
      return false;
    }
    Placement placement = new Placement(element.getInternalState().pos, element.getInternalState().rotationDeg);
    MapPos[] boundingPolygon = calculateBoundingPolygon(placement, record);
    if (boundingPolygon == null) {
      return false;
    }
    record.boundingPolygon = boundingPolygon;
    if (clip) {
      if (!GeomUtils.polygonsIntersect(SCREEN_BOUNDS, record.boundingPolygon)) {
        return false;
      }
    }
    Envelope envelope = new Envelope(record.boundingPolygon);
    List<BillBoardRecord> otherRecords = quadtree.query(envelope);
    for (BillBoardRecord otherRecord : otherRecords) {
      if (!(record.style.allowOverlap && otherRecord.style.allowOverlap)) {
        return false;
      }
    }
    quadtree.insert(envelope, record);
    return true;
  }

  public boolean fitAndAdd(BillBoard element) {
    BillBoardRecord record = new BillBoardRecord(element);
    if (record.style == null) {
      return false;
    }
    for (int iter = 0; iter < MAX_ITERATIONS; iter++) {
      Placement placement = iteratePlacement(record, iter);
      if (placement == null) {
        break;
      }
      MapPos[] boundingPolygon = calculateBoundingPolygon(placement, record);
      if (boundingPolygon == null) {
        continue;
      }
      record.boundingPolygon = boundingPolygon;
      Envelope envelope = new Envelope(record.boundingPolygon);
      List<BillBoardRecord> otherRecords = quadtree.query(envelope);
      boolean valid = true;
      for (BillBoardRecord otherRecord : otherRecords) {
        if (!(record.style.allowOverlap && otherRecord.style.allowOverlap)) {
          valid = false;
          break;
        }
      }
      if (valid) {
        element.updateInternalPlacement(placement.pos, placement.rotationAngle);
        quadtree.insert(envelope, record);
        return true;
      }
    }
    return false;
  }
  
  public boolean place(BillBoard billBoard) {
    BillBoard.BaseElement baseElement = billBoard.getBaseElement();
    if (baseElement == null) {
      return true;
    }

    Placement placement = null;
    if (baseElement instanceof BillBoard.BasePoint) {
      BillBoard.BasePoint basePoint = (BillBoard.BasePoint) baseElement;
      placement = new Placement(new Point3D(basePoint.point.x, basePoint.point.y, basePoint.point.z), billBoard.getInternalState().rotationDeg);
    }

    if (baseElement instanceof BillBoard.BaseLine) {
      BillBoard.BaseLine baseLine = (BillBoard.BaseLine) baseElement;
      MutableVector3D tangent = new MutableVector3D();
      Point3D pos = calculateEdgeMidPoint(baseLine.edges, tangent);
      double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(pos);
      double dotX = localFrameMatrix[0] * tangent.x + localFrameMatrix[1] * tangent.y + localFrameMatrix[2] * tangent.z;
      double dotY = localFrameMatrix[4] * tangent.x + localFrameMatrix[5] * tangent.y + localFrameMatrix[6] * tangent.z;
      float rotationDeg = (float) Math.atan2(dotY, dotX) * Const.RAD_TO_DEG;
      placement = new Placement(pos, rotationDeg);
    }

    if (baseElement instanceof BillBoard.BasePolygon) {
      BillBoard.BasePolygon basePolygon = (BillBoard.BasePolygon) baseElement;
      placement = new Placement(new Point3D(basePolygon.centerPoint.x, basePolygon.centerPoint.y, basePolygon.centerPoint.z), billBoard.getInternalState().rotationDeg);
    }

    if (placement == null) {
      return false;
    }

    billBoard.updateInternalPlacement(placement.pos, placement.rotationAngle);
    return true;
  }
  
  private static Point3D calculateEdgeMidPoint(List<EdgeInfo> edges, MutableVector3D tangent) {
    double lineLength = 0;
    for (EdgeInfo edge : edges) {
      lineLength += edge.length;
    }
    double linePos = 0;
    for (EdgeInfo edge : edges) {
      if (linePos + edge.length >= lineLength * 0.5) {
        tangent.x = edge.dx_du;
        tangent.y = edge.dy_du;
        tangent.z = edge.dz_du;
        double t = lineLength * 0.5 - linePos;
        return new Point3D(edge.x0 + edge.dx_du * t, edge.y0 + edge.dy_du * t, edge.z0 + edge.dz_du * t);
      }
      linePos += edge.length;
    }
    return new Point3D();
  }

  private Placement iteratePlacement(BillBoardRecord record, int iter) {
    BillBoard.BaseElement baseElement = record.billBoard.getBaseElement();
    if (baseElement == null) {
      if (iter > 0) {
        return null;
      }
      return new Placement(record.billBoard.getInternalState().pos, record.billBoard.getInternalState().rotationDeg);
    }

    if (baseElement instanceof BillBoard.BasePoint) {
      BillBoard.BasePoint basePoint = (BillBoard.BasePoint) baseElement;
      if (iter > 0) {
        return null;
      }
      return new Placement(new Point3D(basePoint.point.x, basePoint.point.y, basePoint.point.z), record.billBoard.getInternalState().rotationDeg);
    }

    if (baseElement instanceof BillBoard.BaseLine) {
      BillBoard.BaseLine baseLine = (BillBoard.BaseLine) baseElement;
      List<List<EdgeInfo>> edgesLists = null;
      if (record.style.placementClip) {
        edgesLists = clipLinesAgainstFrustum(baseLine.edges);
      } else {
        edgesLists = new ArrayList<List<EdgeInfo>>();
        edgesLists.add(baseLine.edges);
      }
      List<Placement> placements = findEdgePlacements(edgesLists, record); 
      if (iter >= placements.size()) {
        return null;
      }
      return placements.get(iter);
    }

    if (baseElement instanceof BillBoard.BasePolygon) {
      BillBoard.BasePolygon basePolygon = (BillBoard.BasePolygon) baseElement;
      if (iter > 0) {
        return null;
      }
      return new Placement(new Point3D(basePolygon.centerPoint.x, basePolygon.centerPoint.y, basePolygon.centerPoint.z), record.billBoard.getInternalState().rotationDeg);
    }

    return null;
  }

  private MapPos[] calculateBoundingPolygon(Placement placement, BillBoardRecord record) {
    double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(placement.pos);
    vertexBuf.clear();
    if (!vertexBuilder.generateElementVertices(record.billBoard, record.style, placement.pos, localFrameMatrix, placement.rotationAngle, vertexBuf, null, null)) {
      return null;
    }

    MapPos[] points = new MapPos[vertexBuf.size() / 3];
    for (int i = 0; i < vertexBuf.size() / 3; i++) {
      Matrix.multiplyMV(elementMVPVertex, 0, cameraState.projectionMatrix, 0, vertexBuf.getBuffer(), i * 3);
      if (elementMVPVertex[3] < Const.MIN_NEAR) {
        elementMVPVertex[3] = Const.MIN_NEAR;
      }
      points[i] = new MapPos(elementMVPVertex[0] / elementMVPVertex[3], elementMVPVertex[1] / elementMVPVertex[3]);
    }
    return GeomUtils.calculateConvexHull(points);
  }

  private List<List<EdgeInfo>> clipLinesAgainstFrustum(List<EdgeInfo> edges) {
    List<List<EdgeInfo>> clippedEdgesList = new ArrayList<List<EdgeInfo>>();
    List<EdgeInfo> clippedEdges = null;
    for (int i = 0; i < edges.size(); i++) {
      EdgeInfo edge = edges.get(i);
      double[] l0 = new double[] { edge.x0, edge.y0, edge.z0 };
      double[] l1 = new double[] { edge.x1, edge.y1, edge.z1 };
      if (!cameraState.frustum.clipLine(l0, l1, 0)) {
        continue;
      }
      EdgeInfo clippedEdge = new EdgeInfo(new Point3D(l0[0], l0[1], l0[2]), new Point3D(l1[0], l1[1], l1[2]), edge.normal);
      if (clippedEdges != null) {
        EdgeInfo lastClippedEdge = clippedEdges.get(clippedEdges.size() - 1);
        if (lastClippedEdge.x1 != clippedEdge.x0 || lastClippedEdge.y1 != clippedEdge.y0 || lastClippedEdge.z1 != clippedEdge.z0) {
          clippedEdgesList.add(clippedEdges);
          clippedEdges = null;
        }
      }
      if (clippedEdges == null) {
        clippedEdges = new ArrayList<EdgeInfo>();
      }
      clippedEdges.add(clippedEdge);
    }
    if (clippedEdges != null) {
      clippedEdgesList.add(clippedEdges);
    }
    return clippedEdgesList;
  }

  private List<Placement> findEdgePlacements(List<List<EdgeInfo>> edgesList, BillBoardRecord record) {
    List<Placement> placements = new ArrayList<Placement>();
    for (List<EdgeInfo> edges : edgesList) {
      int i0 = 0;
      double sectionLen = 0;
      for (int i = 0; i < edges.size(); i++) {
        EdgeInfo curEdge = edges.get(i);
        sectionLen += curEdge.length;
        float dot = -Float.MAX_VALUE;
        if (i + 1 < edges.size()) {
          EdgeInfo nextEdge = edges.get(i + 1);
          dot = curEdge.dx_du * nextEdge.dx_du + curEdge.dy_du * nextEdge.dy_du + curEdge.dz_du * nextEdge.dz_du;
        }
        if (dot < MIN_DOT) {
          double sectionPos = 0;
          for (int j = i0; j <= i; j++) {
            EdgeInfo edge = edges.get(j);
            if (sectionPos + edge.length > sectionLen * 0.5f) {
              double t = (sectionLen * 0.5f - sectionPos) / edge.length;
              Point3D pos = new Point3D(edge.x0 + (edge.x1 - edge.x0) * t, edge.y0 + (edge.y1 - edge.y0) * t, edge.z0 + (edge.z1 - edge.z0) * t);
              double[] localFrameMatrix = renderProjection.getLocalFrameMatrix(pos);
              double dotX = localFrameMatrix[0] * edge.dx_du + localFrameMatrix[1] * edge.dy_du + localFrameMatrix[2] * edge.dz_du;
              double dotY = localFrameMatrix[4] * edge.dx_du + localFrameMatrix[5] * edge.dy_du + localFrameMatrix[6] * edge.dz_du;
              float rotationDeg = (float) Math.atan2(dotY, dotX) * Const.RAD_TO_DEG;
              placements.add(new Placement(pos, rotationDeg));
              break;
            }
            sectionPos += edge.length;
          }
          i0 = i + 1;
          sectionLen = 0;
        }
      }
    }
    return placements;
  }
}
