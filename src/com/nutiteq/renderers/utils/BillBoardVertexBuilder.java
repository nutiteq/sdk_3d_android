package com.nutiteq.renderers.utils;

import java.util.List;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.TextureInfo;
import com.nutiteq.components.Vector3D;
import com.nutiteq.geometry.BillBoard;
import com.nutiteq.geometry.Line;
import com.nutiteq.geometry.Text;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.BillBoardStyle;
import com.nutiteq.style.TextStyle;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.FloatVertexBuffer;
import com.nutiteq.utils.Matrix;

/**
 * Generator for curved text placements, build vertex/texture coordinate list based on text and underlying line geometry.
 */
public class BillBoardVertexBuilder {
  private static final double MIN_DOT = 0.866; // maximum angle between line segments (roughly 30 degrees)
  private static final int MAX_SPLIT_DISTANCE = 32; // maximum number of pixels to move forward till next split

  private static final double[] QUAD_VERTICES = new double[] { -0.5f, -0.5f, 0.0f, 1.0f, 0.5f, -0.5f, 0.0f, 1.0f, 0.5f, 0.5f, 0.0f, 1.0f, -0.5f, 0.5f, 0.0f, 1.0f };
  private static final float[] QUAD_TEXCOORDS = new float[] { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f };
  
  private final RenderProjection renderProjection;
  private CameraState cameraState;
  
  private final double[] rotateMatrix  = new double[16];
  private final double[] elementMatrix = new double[16];
  private final double[] elementVertex = new double[4];
  private final double[] elementMVMatrix = new double[16];
  private final double[] elementMVVertex = new double[4 * 4];

  public BillBoardVertexBuilder(RenderProjection renderProjection) {
    this.renderProjection = renderProjection;
  }
  
  public void setCamera(CameraState cameraState) {
    this.cameraState = cameraState;
  }

  public boolean calculateElementMVMatrix(BillBoard element, BillBoardStyle elementStyle, Point3D pos, double[] localFrameMatrix, float rotationDeg, float textureWidth, float textureHeight, double[] elementMVMatrix) {
    double dotZ = localFrameMatrix[8] * (pos.x - cameraState.cameraPos.x) + localFrameMatrix[9] * (pos.y - cameraState.cameraPos.y) + localFrameMatrix[10] * (pos.z - cameraState.cameraPos.z);    
    if (dotZ >= 0) {
      return false;
    }

    boolean flip = false;
    if (elementStyle.orientation == BillBoardStyle.GROUND_ORIENTATION && elementStyle.orientationFlip) {
      float dAngle = (cameraState.rotationDeg - element.getInternalState().rotationDeg + 360) % 360;
      flip = dAngle > 90 && dAngle < 270;
      if (flip) {
        rotationDeg += 180;
      }
    }

    elementMatrix[0]  = textureWidth  / cameraState.zoomPow2;
    elementMatrix[1]  = 0;
    elementMatrix[2]  = 0;
    elementMatrix[3]  = 0;
    elementMatrix[4]  = 0;
    elementMatrix[5]  = textureHeight / cameraState.zoomPow2;
    elementMatrix[6]  = 0;
    elementMatrix[7]  = 0;
    elementMatrix[8]  = 0;
    elementMatrix[9]  = 0;
    elementMatrix[10] = 1;
    elementMatrix[11] = 0;
    elementMatrix[12] = (elementStyle.anchorX * textureWidth  * 0.5f + elementStyle.offset2DX) / cameraState.zoomPow2;
    elementMatrix[13] = ((flip ? -elementStyle.anchorY : elementStyle.anchorY) * textureHeight * 0.5f + elementStyle.offset2DY) / cameraState.zoomPow2;
    elementMatrix[14] = 0;
    elementMatrix[15] = 1;
    
    float elementAngle = -rotationDeg;
    switch (elementStyle.orientation) {
    case BillBoardStyle.CAMERA_BILLBOARD_ORIENTATION:
      if (elementAngle != 0) {
        Matrix.setRotationM(rotateMatrix, 0, 0, 1, elementAngle);
        Matrix.multiplyMM(elementMVMatrix, 0, rotateMatrix, 0, elementMatrix, 0);
        Matrix.copyM(elementMatrix, 0, elementMVMatrix, 0);
      }
      Matrix.multiplyMM(elementMVMatrix, 0, cameraState.invViewMatrix, 0, elementMatrix, 0);
      Matrix.copyM(elementMatrix, 0, elementMVMatrix, 0);
      break;
    case BillBoardStyle.GROUND_BILLBOARD_ORIENTATION:
      double dotX = localFrameMatrix[0] * cameraState.upVector.x + localFrameMatrix[1] * cameraState.upVector.y + localFrameMatrix[2] * cameraState.upVector.z;
      double dotY = localFrameMatrix[4] * cameraState.upVector.x + localFrameMatrix[5] * cameraState.upVector.y + localFrameMatrix[6] * cameraState.upVector.z;
      float angle = 90 - (float) Math.atan2(dotY, dotX) * Const.RAD_TO_DEG;
      Matrix.setRotationM(rotateMatrix, localFrameMatrix[8], localFrameMatrix[9], localFrameMatrix[10], angle + elementAngle);
      Matrix.multiplyMM(elementMVMatrix, 0, localFrameMatrix, 0, elementMatrix, 0);
      Matrix.multiplyMM(elementMatrix, 0, rotateMatrix, 0, elementMVMatrix, 0);
      break;
    case BillBoardStyle.GROUND_ORIENTATION:
      Matrix.setRotationM(rotateMatrix, 0, 0, 1, elementAngle);
      Matrix.multiplyMM(elementMVMatrix, 0, rotateMatrix, 0, elementMatrix, 0);
      Matrix.multiplyMM(elementMatrix, 0, localFrameMatrix, 0, elementMVMatrix, 0);
      break;
    }

    elementMatrix[12] += pos.x + (elementStyle.offset3DZ * localFrameMatrix[8]);
    elementMatrix[13] += pos.y + (elementStyle.offset3DZ * localFrameMatrix[9]);
    elementMatrix[14] += pos.z + (elementStyle.offset3DZ * localFrameMatrix[10]);
    
    Matrix.multiplyMM(elementMVMatrix, 0, cameraState.modelviewMatrix, 0, elementMatrix, 0);

    if (elementStyle.orientation == BillBoardStyle.CAMERA_BILLBOARD_ORIENTATION) {
      elementMVMatrix[2] = elementMVMatrix[6] = 0; // increase stability/reduce z fighting
    }
    return true;
  }

  public synchronized boolean generateElementVertices(BillBoard element, BillBoardStyle elementStyle, Point3D pos, double[] localFrameMatrix, float rotationDeg, FloatVertexBuffer vertexBuf, TextureInfo textureInfo, FloatVertexBuffer texCoordBuf) {
    double dotZ = localFrameMatrix[8] * (pos.x - cameraState.cameraPos.x) + localFrameMatrix[9] * (pos.y - cameraState.cameraPos.y) + localFrameMatrix[10] * (pos.z - cameraState.cameraPos.z);    
    if (dotZ >= 0) {
      return false;
    }
    
    if (element instanceof Text) {
      Text text = (Text) element;
      if (text.getBaseElement() instanceof BillBoard.BaseLine) {
        BillBoard.BaseLine baseLine = (BillBoard.BaseLine) text.getBaseElement();
        return generateTextLineVertices(text.getInternalState().textInfo, (TextStyle) elementStyle, pos, rotationDeg, baseLine.edges, vertexBuf, textureInfo, texCoordBuf);
      }
    }

    float textureWidth  = element.getInternalState().getTextureWidth();
    float textureHeight = element.getInternalState().getTextureHeight();
    if (textureInfo != null) {
      textureWidth  = textureInfo.width;
      textureHeight = textureInfo.height;
    }
    if (!calculateElementMVMatrix(element, elementStyle, pos, localFrameMatrix, rotationDeg, textureWidth, textureHeight, elementMVMatrix)) {
      return false;
    }

    for (int i = 0; i < 4; i++) {
      Matrix.multiplyMV(elementMVVertex, i * 4, elementMVMatrix, 0, QUAD_VERTICES, i * 4);
    }

    for (int i = 0; i < 2; i++) {
      int i0 = 0;
      int i1 = i + 1;
      int i2 = i + 2;

      vertexBuf.add((float) elementMVVertex[i0 * 4 + 0], (float) elementMVVertex[i0 * 4 + 1], (float) elementMVVertex[i0 * 4 + 2]);
      vertexBuf.add((float) elementMVVertex[i1 * 4 + 0], (float) elementMVVertex[i1 * 4 + 1], (float) elementMVVertex[i1 * 4 + 2]);
      vertexBuf.add((float) elementMVVertex[i2 * 4 + 0], (float) elementMVVertex[i2 * 4 + 1], (float) elementMVVertex[i2 * 4 + 2]);

      if (textureInfo != null) {
        float u0 = textureInfo.getTexU0(), v0 = textureInfo.getTexV0();
        float su = textureInfo.getTexSU(), sv = textureInfo.getTexSV();
        texCoordBuf.add(u0 + QUAD_TEXCOORDS[i0 * 2 + 0] * su, v0 + QUAD_TEXCOORDS[i0 * 2 + 1] * sv);
        texCoordBuf.add(u0 + QUAD_TEXCOORDS[i1 * 2 + 0] * su, v0 + QUAD_TEXCOORDS[i1 * 2 + 1] * sv);
        texCoordBuf.add(u0 + QUAD_TEXCOORDS[i2 * 2 + 0] * su, v0 + QUAD_TEXCOORDS[i2 * 2 + 1] * sv);
      }
    }
    return true;
  }
  
  private boolean generateTextLineVertices(Text.TextInfo textInfo, TextStyle textStyle, Point3D pos, float rotationDeg, List<Line.EdgeInfo> edges, FloatVertexBuffer vertexBuf, TextureInfo textureInfo, FloatVertexBuffer texCoordBuf) {
    // TODO: optimize
    int i0 = findEdgeIndex(pos, edges);
    Point3D Pprev;
    {
      Point3D Pa = pos;
      Point3D Pb = new Point3D(edges.get(i0).x0, edges.get(i0).y0, edges.get(i0).z0);
      double d0 = new Vector3D(Pa, Pb).getLength();
      double t0 = (1 - textStyle.anchorX) * textInfo.textureWidth / cameraState.zoomPow2 / 2;
      while (t0 > d0) {
        if (--i0 < 0)
          return false;
        Pa = Pb;
        Pb = new Point3D(edges.get(i0).x0, edges.get(i0).y0, edges.get(i0).z0);
        t0 -= d0;
        d0 = new Vector3D(Pa, Pb).getLength();
      }
      double t = t0 / d0;
      Pprev = new Point3D(Pa.x + (Pb.x - Pa.x) * t, Pa.y + (Pb.y - Pa.y) * t, Pa.z + (Pb.z - Pa.z) * t);
    }

    boolean flip = false;
    if (textStyle.orientationFlip) {
      float dAngle = (cameraState.rotationDeg - rotationDeg + 360) % 360;
      flip = dAngle > 90 && dAngle < 270;
      if (flip) {
        rotationDeg += 180;
      }
    }

    float texSU = (textureInfo != null ? textureInfo.getTexSU() : 1);
    float texSV = (textureInfo != null ? textureInfo.getTexSV() : 1);
    float lineWidth = textInfo.textureWidth / cameraState.zoomPow2;
    float lineHeight = textInfo.textureHeight / cameraState.zoomPow2;
    float linePos = 0;
    float uPrev = 0;
    float v0 = -textStyle.anchorY * lineHeight / 2; 
    float dv = lineHeight / 2;
    float t0 = flip ? -texSV : 0;
    float t1 = flip ? 0 : -texSV;
    
    for (int i = i0; i < edges.size(); i++) {
      Line.EdgeInfo edge = edges.get(i);
      Point3D Pnext = new Point3D(edges.get(i).x1, edges.get(i).y1, edges.get(i).z1); 
      Vector3D tangent = new Vector3D(edge.x1 - edge.x0, edge.y1 - edge.y0, edge.z1 - edge.z0).getNormalized();
      Vector3D binormal = Vector3D.crossProduct(tangent, renderProjection.getNormal(Pnext)).getNormalized();

      float uNext = 0;
      float edgeLen = (float) (new Vector3D(Pprev, Pnext).getLength()) - uPrev;
      boolean done = false;
      if (linePos + edgeLen < lineWidth) {
        if (i + 1 >= edges.size()) {
          return false;
        }
        Line.EdgeInfo nextEdge = edges.get(i + 1);
        Vector3D nextTangent = new Vector3D(nextEdge.x1 - nextEdge.x0, nextEdge.y1 - nextEdge.y0, nextEdge.z1 - nextEdge.z0).getNormalized();
        float dot = (float) Vector3D.dotProduct(tangent, nextTangent);
        if (dot < MIN_DOT) {
          return false;
        }

        float du = (float) (dot < 0.9999f ? (1 - dot) / Math.sqrt(1 - dot * dot) : 0) * lineHeight / 2;
        int n = findSplitPos(textInfo, (int) (textInfo.textWidth * (linePos + edgeLen - du) / lineWidth), flip);
        if (n > MAX_SPLIT_DISTANCE) {
          return false;
        }
        float dt = (n + 0.5f) / textInfo.textWidth * lineWidth;
        
        uNext = dt * dot + (float) Math.sqrt(Math.min(1, 1 - dot * dot)) * v0;
        edgeLen += dt - du;
      } else {
        edgeLen = lineWidth - linePos;
        done = true;
      }

      Point3D P0 = new Point3D(Pprev.x + uPrev * tangent.x + v0 * binormal.x, Pprev.y + uPrev * tangent.y + v0 * binormal.y, Pprev.z + uPrev * tangent.z + v0 * binormal.z);
      Point3D P1 = new Point3D(P0.x + edgeLen * tangent.x, P0.y + edgeLen * tangent.y, P0.z + edgeLen * tangent.z);
      float s0 = linePos / lineWidth * texSU;
      float s1 = s0 + edgeLen / lineWidth * texSU;
      if (flip) {
        s0 = texSU - s0;
        s1 = texSU - s1;
      }

      for (int y = 0; y < 2; y++) {
        float s = y * 2 - 1;
        elementVertex[0] = (P0.x + s * dv * binormal.x); elementVertex[1] = (P0.y + s * dv * binormal.y); elementVertex[2] = (P0.z + s * dv * binormal.z); elementVertex[3] = 1;
        Matrix.multiplyMV(elementMVVertex, y * 4 + 0, cameraState.modelviewMatrix, 0, elementVertex, 0);
        elementVertex[0] = (P1.x + s * dv * binormal.x); elementVertex[1] = (P1.y + s * dv * binormal.y); elementVertex[2] = (P1.z + s * dv * binormal.z); elementVertex[3] = 1;
        Matrix.multiplyMV(elementMVVertex, y * 4 + 8, cameraState.modelviewMatrix, 0, elementVertex, 0);
      }

      vertexBuf.add((float) elementMVVertex[4], (float) elementMVVertex[5], (float) elementMVVertex[6]);
      vertexBuf.add((float) elementMVVertex[12], (float) elementMVVertex[13], (float) elementMVVertex[14]);
      vertexBuf.add((float) elementMVVertex[8], (float) elementMVVertex[9], (float) elementMVVertex[10]);

      vertexBuf.add((float) elementMVVertex[4], (float) elementMVVertex[5], (float) elementMVVertex[6]);
      vertexBuf.add((float) elementMVVertex[8], (float) elementMVVertex[9], (float) elementMVVertex[10]);
      vertexBuf.add((float) elementMVVertex[0], (float) elementMVVertex[1], (float) elementMVVertex[2]);
      
      if (textureInfo != null) {
        texCoordBuf.add(s0, t1);
        texCoordBuf.add(s1, t1);
        texCoordBuf.add(s1, t0);

        texCoordBuf.add(s0, t1);
        texCoordBuf.add(s1, t0);
        texCoordBuf.add(s0, t0);
      }
      
      if (done) {
        break;
      }
    
      linePos += edgeLen;
      uPrev = uNext;
      Pprev = Pnext;
    }
    return true;
  }

  private static int findEdgeIndex(Point3D pos, List<Line.EdgeInfo> edges) {
    int edgeIndex = 0;
    float maxError = Float.MAX_VALUE;
    for (int i = 0; i < edges.size(); i++) {
      Line.EdgeInfo edge = edges.get(i);
      Vector3D tangent = new Vector3D(edge.x1 - edge.x0, edge.y1 - edge.y0, edge.z1 - edge.z0);
      Vector3D posVec = new Vector3D(pos.x - edge.x0, pos.y - edge.y0, pos.z - edge.z0);
      double t = Vector3D.dotProduct(posVec, tangent);
      if (t >= 0 && t <= tangent.getLength() * tangent.getLength()) {
        float error = 1 - (float) (t / tangent.getLength() / posVec.getLength());
        if (error < maxError) {
          edgeIndex = i;
          maxError = error;
        }
      }
    }
    return edgeIndex;
  }

  private static int findSplitPos(Text.TextInfo textInfo, int x0, boolean flip) {
    int[] splitTable = textInfo.getSplitTable();
    if (splitTable == null) {
      return 0;
    }
    int x = flip ? splitTable.length - x0 - 1 : x0;
    if (x < 0 || x >= splitTable.length) {
      return 0;
    }
    return (flip ? splitTable[x] >> 16 : splitTable[x]) & 0xffff;
  }
}
