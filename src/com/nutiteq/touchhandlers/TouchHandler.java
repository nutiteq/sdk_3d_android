package com.nutiteq.touchhandlers;

import android.view.MotionEvent;

/**
 * Interface for touch screen UI handlers.
 */
public interface TouchHandler {

  boolean onTouchEvent(MotionEvent event);

  void click(float x, float y);

  void longClick(float x, float y);

  void doubleClick(float x, float y);

  void dualClick();

  void singlePointer(float x, float y);

  void dualPointer(float x1, float y1, float x2, float y2);
  
  void reset();

  void onStartMapping();

  void onStopMapping();
}
