package com.nutiteq.touchhandlers.threads;

import com.nutiteq.components.Options;
import com.nutiteq.touchhandlers.TouchHandler;

/**
 * Background thread for detecting click types and passing this information to touch handler. 
 */
public class ClickThread extends Thread {
  private static final int LONG_CLICK_DURATION = 400;
  private static final int DUAL_CLICK_START_DURATION = 100;
  private static final int DUAL_CLICK_RELEASE_DURATION = 300;
  private static final int DOUBLE_CLICK_DURATION = 400;

  private static final int LONG_CLICK_TOLERANCE = 12;
  private static final int DUAL_CLICK_TOLERANCE = 15;

  private static final int LONG_CLICK = 1;
  private static final int DOUBLE_CLICK = 2;
  private static final int DUAL_CLICK = 3;

  private int clickMode;

  private TouchHandler touchHandler;
  private Options options;

  private long longClickExitTime;
  private long doubleClickExitTime;
  private long dualClickStartTime;
  private long dualClickExitTime;

  private int pointersDown;

  private float x1;
  private float y1;
  private float pointer1MovedSum;
  private float x2;
  private float y2;
  private float pointer2MovedSum;

  private volatile boolean chosen;
  private volatile boolean canceled;

  private volatile boolean running;

  public ClickThread(TouchHandler touchHandler, Options options) {
    super("ClickThread");
    this.touchHandler = touchHandler;
    this.options = options;
    setPriority((Thread.MAX_PRIORITY + Thread.NORM_PRIORITY) / 2);
    start();
  }

  public void init() {
    longClickExitTime = System.currentTimeMillis() + (options.isClickTypeDetection() ? LONG_CLICK_DURATION : -1);
    doubleClickExitTime = System.currentTimeMillis() + (options.isClickTypeDetection() ? DOUBLE_CLICK_DURATION : -1);
    dualClickStartTime = System.currentTimeMillis() + (options.isClickTypeDetection() ? DUAL_CLICK_START_DURATION : -1);
    dualClickExitTime = dualClickStartTime + (options.isClickTypeDetection() ? DUAL_CLICK_RELEASE_DURATION : -1);

    clickMode = LONG_CLICK;

    pointersDown = 0;

    pointer1MovedSum = 0;
    pointer2MovedSum = 0;

    chosen = false;
    canceled = false;

    running = true;
    // Thread may be waiting, wake it up
    synchronized(this){
      notify();
    }
  }

  public boolean isRunning() {
    return running;
  }

  public void pointer1Down(float x, float y) {
    pointersDown++;
    x1 = x;
    y1 = y;

    // //log.debug("DOWN1: " + pointersDown);
    if (clickMode == DOUBLE_CLICK) {
      chosen = true;
    }
  }

  public void pointer1Moved(float x, float y) {
    if (chosen) {
      return;
    }

    pointer1MovedSum += Math.abs(x - x1);
    pointer1MovedSum += Math.abs(y - y1);
    x1 = x;
    y1 = y;

    if (clickMode == LONG_CLICK) {
      if (pointer1MovedSum >= LONG_CLICK_TOLERANCE) {
        chosen = true;
        canceled = true;
      }
    } else if (clickMode == DUAL_CLICK) {
      // //log.debug("Moved: " + pointer1MovedSum);
      if (pointer1MovedSum >= DUAL_CLICK_TOLERANCE && pointersDown == 2) {
        // log.debug("Moved too much, canceled");
        chosen = true;
        canceled = true;
      }
    }
  }

  public void pointer1Up() {
    pointersDown--;

    // log.debug("UP1: " + pointersDown);
    if (clickMode == LONG_CLICK) {
      // log.debug("TOO BAD");
      clickMode = DOUBLE_CLICK;
    } else if (clickMode == DUAL_CLICK) {
      if (pointersDown == 0) {
        chosen = true;
      } else if (pointersDown == 1) {
        x1 = x2;
        y1 = y2;
      }
    }
  }

  public void pointer2Down(float x, float y) {
    pointersDown++;

    if (chosen) {
      return;
    }

    x2 = x;
    y2 = y;

    // log.debug("DOWN2: " + pointersDown);

    clickMode = DUAL_CLICK;
    if (System.currentTimeMillis() > dualClickStartTime) {
      chosen = true;
      canceled = true;
    }
  }

  public void pointer2Moved(float x, float y) {
    if (chosen) {
      return;
    }

    pointer2MovedSum += Math.abs(x - x2);
    pointer2MovedSum += Math.abs(y - y2);
    x2 = x;
    y2 = y;

    if (clickMode == DUAL_CLICK) {
      // log.debug("Moved2: " + pointer1MovedSum);
      if (pointer2MovedSum >= DUAL_CLICK_TOLERANCE && pointersDown == 2) {
        // log.debug("Moved too much 2 canceled");
        chosen = true;
        canceled = true;
      }
    }
  }

  public void pointer2Up() {
    pointersDown--;

    // log.debug("UP2: " + pointersDown);

  }

  @Override
  public void run() {
    while (true) {

      if (!running) {
        // If not running, just wait until notified
        synchronized(this){
          // This avoids race-condition when stopThread was called just before waiting on lock
          if (this.touchHandler == null){
            return;
          }
          
          try {
            wait();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          
          // If stopThread was called while waiting on lock we should return immediately
          if (this.touchHandler == null){
            return;
          }
        }
      }

      while (!chosen) {
        long time = System.currentTimeMillis();
        switch (clickMode) {
        case LONG_CLICK:
          longClick(time);
          break;
        case DOUBLE_CLICK: {
          doubleClick(time);
          break;
        }
        case DUAL_CLICK: {
          dualClick(time);
          break;
        }
        }
        Thread.yield();
      }

      // Get local copy of touch handler and check it is not null to avoid race-condition
      TouchHandler touchHandler;
      synchronized (this) {
        touchHandler = this.touchHandler;
      }
      if (touchHandler == null) {
        return;
      }

      switch (clickMode) {
      case LONG_CLICK:
        afterLongClick(touchHandler);
        break;
      case DOUBLE_CLICK: {
        afterDoubleClick(touchHandler);
        break;
      }
      case DUAL_CLICK: {
        afterDualClick(touchHandler);
        break;
      }
      }
    }
  }
  
  public void exitThread() {
    synchronized(this){
      this.touchHandler = null;
      notify();
    }
  }
  
  public void joinThread() {
    try {
      join();
    } catch (InterruptedException e) {
      return;
    }
    
    touchHandler = null;
    options = null;
  }

  private void longClick(long time) {
    if (time >= longClickExitTime) {
      chosen = true;
    }
  }

  private void afterLongClick(TouchHandler touchHandler) {
    if (!options.isClickTypeDetection()) {
      touchHandler.singlePointer(x1, y1);
      touchHandler.click(x1, y1);
      running = false;
      return;
    }

    if (canceled) {
      touchHandler.singlePointer(x1, y1);
      running = false;
      return;
    }

    touchHandler.longClick(x1, y1);
    running = false;
  }

  private void doubleClick(long time) {
    if (time >= doubleClickExitTime) {
      chosen = true;
      canceled = true;
    }
  }

  private void afterDoubleClick(TouchHandler touchHandler) {
    if (canceled) {
      touchHandler.click(x1, y1);
      running = false;
      return;
    }

    touchHandler.doubleClick(x1, y1);
    running = false;
  }

  private void dualClick(long time) {
    if (time >= dualClickExitTime) {
      // log.debug("Dual failed");
      chosen = true;
      canceled = true;
    }
  }

  private void afterDualClick(TouchHandler touchHandler) {
    if (canceled) {
      // log.debug("Regular dual pointer stuff");
      if (pointersDown == 1) {
        touchHandler.singlePointer(x1, y1);
      } else if (pointersDown >= 2) {
        touchHandler.dualPointer(x1, y1, x2, y2);
      }
      running = false;
      return;
    }

    touchHandler.dualClick();
    running = false;
  }
}
