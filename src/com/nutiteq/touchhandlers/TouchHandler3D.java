package com.nutiteq.touchhandlers;

import android.view.MotionEvent;

import com.nutiteq.components.Components;
import com.nutiteq.components.Constraints;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.components.MutableVector;
import com.nutiteq.components.Options;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Range;
import com.nutiteq.components.Vector;
import com.nutiteq.components.Vector3D;
import com.nutiteq.renderers.MapRenderer;
import com.nutiteq.renderers.rendersurfaces.RenderSurface;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.touchhandlers.threads.ClickThread;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.GeomUtils;
import com.nutiteq.utils.Matrix;
import com.nutiteq.utils.Utils;

/**
 * Specialized touch screen UI handler for 3D map renderers.
 */
public class TouchHandler3D implements TouchHandler {
  private static final int ACTION_POINTER_1_DOWN = 5;
  private static final int ACTION_POINTER_1_UP = 6;
  private static final int ACTION_POINTER_2_DOWN = 261;
  private static final int ACTION_POINTER_2_UP = 262;

  private static final int PANNING_SINGLE_POINTER = 0;
  private static final int PANNING_DUAL_POINTER_GUESSING = 1;
  private static final int PANNING_DUAL_POINTER_VIEW_ANGLE = 2;
  private static final int PANNING_DUAL_POINTER_ROTATION = 3;
  private static final int PANNING_DUAL_POINTER_SCALING = 4;
  private static final int PANNING_DUAL_POINTER_GENERAL = 5;
  private static final int PANNING_CLICK_GUESSING = 6;

  private static final float GUESSING_MAX_DELTA_Y = 360;
  private static final float GUESSING_MIN_SWIPE_LENGTH_SAME = 30;
  private static final float GUESSING_MIN_SWIPE_LENGTH_OPPOSITE = 10;
  private static final float GUESSING_SWIPE_MAX_ABS_COS = 0.707f;

  private static final float VIEW_ANGLE_PX_TO_RAD = (float) (0.2 * Math.PI / 180);
  private static final int TAP_ZOOM_DURATION = 300; // in ms

  private final MapRenderer mapRenderer;
  private RenderSurface renderSurface;
  private RenderProjection renderProjection;
  private Constraints constraints;
  private Options options;

  private MutableMapPos prevPoint = new MutableMapPos();
  private MutableMapPos prevPoint2 = new MutableMapPos();
  private MutableVector prevVector = new MutableVector();

  private MutableVector swipeVector = new MutableVector();
  private MutableVector swipeVector2 = new MutableVector();
  
  private Point3D anchorPoint;

  private Point3D prevTouchPoint;
  private Point3D prevTouchPoint2;
  
  private int panningMode = PANNING_CLICK_GUESSING;

  private boolean doKineticPanning = false;

  public ClickThread clickThread;

  public TouchHandler3D(MapRenderer mapRenderer, Components components) {
    this.mapRenderer = mapRenderer;
    this.renderSurface = mapRenderer.getRenderSurface();
    this.renderProjection = renderSurface.getRenderProjection();

    constraints = components.constraints;
    options = components.options;
    clickThread = new ClickThread(this, options);
  }
  
  @Override
  public boolean onTouchEvent(MotionEvent event) {
    Thread.currentThread().setPriority((Thread.MAX_PRIORITY + Thread.NORM_PRIORITY) / 2);
    float x1 = event.getX();
    float y1 = event.getY();
    float x2 = 0;
    float y2 = 0;

    switch (event.getAction()) {
    case MotionEvent.ACTION_DOWN:
    case ACTION_POINTER_1_DOWN:
      switch (panningMode) {
      case PANNING_CLICK_GUESSING:
        doKineticPanning = true;
        mapRenderer.stopKineticPanning();
        mapRenderer.stopKineticRotation();
        if (!clickThread.isRunning()) {
          clickThread.init();
        }
        clickThread.pointer1Down(x1, y1);
        break;
      case PANNING_SINGLE_POINTER:
        swipeVector.setCoords(0, 0, 0);
        swipeVector2.setCoords(0, 0, 0);
        prevPoint.setCoords(x1, y1, 0);
        prevPoint2.setCoords(x2, y2, 0);
        panningMode = PANNING_DUAL_POINTER_GUESSING;
        break;
      }
      break;

    case ACTION_POINTER_2_DOWN:
      x2 = TouchHandler3D.getX(event, 1);
      y2 = TouchHandler3D.getY(event, 1);
      doKineticPanning = false;
      mapRenderer.stopKineticRotation();
      switch (panningMode) {
      case PANNING_CLICK_GUESSING:
        if (clickThread.isRunning()) {
          clickThread.pointer2Down(x2, y2);
        } else {
          dualPointer(x1, y1, x2, y2);
        }
        break;
      case PANNING_SINGLE_POINTER:
        swipeVector.setCoords(0, 0, 0);
        swipeVector2.setCoords(0, 0, 0);
        prevPoint.setCoords(x1, y1, 0);
        prevPoint2.setCoords(x2, y2, 0);
        panningMode = PANNING_DUAL_POINTER_GUESSING;
        break;
      }
      break;
      
    case MotionEvent.ACTION_MOVE:
      switch (panningMode) {
      case PANNING_CLICK_GUESSING:
        if (clickThread.isRunning()) {
          clickThread.pointer1Moved(x1, y1);
        }
        break;
      case PANNING_SINGLE_POINTER:
        pointerMovedSingle(x1, y1);
        break;
      case PANNING_DUAL_POINTER_GUESSING:
        x2 = TouchHandler3D.getX(event, 1);
        y2 = TouchHandler3D.getY(event, 1);
        pointerMovedDualGuessing(x1, y1, x2, y2);
        break;
      case PANNING_DUAL_POINTER_VIEW_ANGLE:
        pointerMovedDualViewAngle(x1, y1);
        break;
      case PANNING_DUAL_POINTER_ROTATION:
        x2 = TouchHandler3D.getX(event, 1);
        y2 = TouchHandler3D.getY(event, 1);
        pointerMovedDualRotation(x1, y1, x2, y2);
        break;
      case PANNING_DUAL_POINTER_SCALING:
        x2 = TouchHandler3D.getX(event, 1);
        y2 = TouchHandler3D.getY(event, 1);
        pointerMovedDualScaling(x1, y1, x2, y2);
        break;
      case PANNING_DUAL_POINTER_GENERAL:
        x2 = TouchHandler3D.getX(event, 1);
        y2 = TouchHandler3D.getY(event, 1);
        pointerMovedDualGeneral(x1, y1, x2, y2);
        break;
      }
      break;
      
    case MotionEvent.ACTION_CANCEL:
      switch (panningMode) {
      case PANNING_CLICK_GUESSING:
        if (clickThread.isRunning()) {
          clickThread.pointer1Up();
        }
        break;
      case PANNING_SINGLE_POINTER:
        if (doKineticPanning) {
          mapRenderer.startKineticPanning();
        }
        panningMode = PANNING_CLICK_GUESSING;
        break;
      case PANNING_DUAL_POINTER_GENERAL:
        prevTouchPoint = prevTouchPoint2;
        panningMode = PANNING_SINGLE_POINTER;
        break;
      case PANNING_DUAL_POINTER_GUESSING:
      case PANNING_DUAL_POINTER_VIEW_ANGLE:
      case PANNING_DUAL_POINTER_ROTATION:
      case PANNING_DUAL_POINTER_SCALING:
        x2 = TouchHandler3D.getX(event, 1);
        y2 = TouchHandler3D.getY(event, 1);
        prevTouchPoint = screenToWorld(x2, y2, Const.PANNING_SPEED);
        panningMode = PANNING_SINGLE_POINTER;
        break;
      }
      break;
      
    case MotionEvent.ACTION_UP:
    case ACTION_POINTER_1_UP:
      switch (panningMode) {
      case PANNING_CLICK_GUESSING:
        if (clickThread.isRunning()) {
          clickThread.pointer1Up();
        }
        break;
      case PANNING_SINGLE_POINTER:
        if (doKineticPanning) {
          mapRenderer.startKineticPanning();
        }
        panningMode = PANNING_CLICK_GUESSING;
        break;
      case PANNING_DUAL_POINTER_GUESSING:
      case PANNING_DUAL_POINTER_VIEW_ANGLE:
      case PANNING_DUAL_POINTER_ROTATION:
      case PANNING_DUAL_POINTER_SCALING:
        x2 = TouchHandler3D.getX(event, 1);
        y2 = TouchHandler3D.getY(event, 1);
        prevTouchPoint = screenToWorld(x2, y2, Const.PANNING_SPEED);
        panningMode = PANNING_SINGLE_POINTER;
        mapRenderer.startKineticRotation();
        break;
      case PANNING_DUAL_POINTER_GENERAL:
        prevTouchPoint = prevTouchPoint2;
        panningMode = PANNING_SINGLE_POINTER;
        mapRenderer.startKineticRotation();
        break;
      }
      break;

    case ACTION_POINTER_2_UP:
      switch (panningMode) {
      case PANNING_CLICK_GUESSING:
        if (clickThread.isRunning()) {
          clickThread.pointer2Up();
        }
        break;
      case PANNING_DUAL_POINTER_GUESSING:
      case PANNING_DUAL_POINTER_VIEW_ANGLE:
      case PANNING_DUAL_POINTER_ROTATION:
      case PANNING_DUAL_POINTER_SCALING:
        prevTouchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
        panningMode = PANNING_SINGLE_POINTER;
        mapRenderer.startKineticRotation();
        break;
      case PANNING_DUAL_POINTER_GENERAL:
        panningMode = PANNING_SINGLE_POINTER;
        mapRenderer.startKineticRotation();
        break;
      }
      break;
    }
    return true;
  }
  
  private float calculateRotationScalingFactor(double dx1, double dy1, double dx2, double dy2) {
    // Sanity check to avoid div by zero
    if ((dx1 == 0 && dy1 == 0) || (dx2 == 0 && dy2 == 0)) {
      return 1.0f;
    }
    
    // Calculate new relative scale and cosine between movements 
    double scale = Math.abs(dx1 * dx2 + dy1 * dy2) / (dx1 * dx1 + dy1 * dy1);
    double sin = (dx2 * dy1 - dy2 * dx1) / Math.sqrt(dx1 * dx1 + dy1 * dy1) / Math.sqrt(dx2 * dx2 + dy2 * dy2);
    
    // Calculate relative factor, handle both zoom-in and zoom-out cases
    return (float) (Math.abs(sin) / Math.max(1 / scale - 1, scale - 1));
  }

  private void pointerMovedSingle(float x, float y) {
    // Get various parameters
    Point3D cameraPos;
    Point3D focusPoint;
    Vector3D upVector;
    mapRenderer.setGeneralLock(true);
    try {
      cameraPos = mapRenderer.getCameraPos();
      focusPoint = mapRenderer.getFocusPoint();
      upVector = mapRenderer.getUpVector();
    }
    finally {
      mapRenderer.setGeneralLock(false);
    }
    
    // Calculate world coordinates for touch points
    Point3D touchPoint = screenToWorld(x, y, Const.PANNING_SPEED);
    if (touchPoint != null && prevTouchPoint != null) {
      double[] translateTransform = getTranslateMatrix(touchPoint, prevTouchPoint, 1);

      // Transform camera parameters to new position
      focusPoint = GeomUtils.transform(focusPoint, translateTransform);
      cameraPos = GeomUtils.transform(cameraPos, translateTransform);
      upVector = GeomUtils.transform(upVector, translateTransform);

      mapRenderer.setLookAtParams(cameraPos.x, cameraPos.y, cameraPos.z, focusPoint.x, focusPoint.y, focusPoint.z, upVector.x, upVector.y, upVector.z, true, false, false);
    }
    
    prevTouchPoint = screenToWorld(x, y, Const.PANNING_SPEED);
  }

  private void pointerMovedDualGuessing(float x1, float y1, float x2, float y2) {
    // If the pointers' y coordinates differ too much it's the general case or rotation
    float deltaY = Math.abs(y1 - y2);
    if (deltaY > GUESSING_MAX_DELTA_Y) {
      panningMode = PANNING_DUAL_POINTER_GENERAL;
    } else {
      // Calculate swipe vectors
      double dx1 = x1 - prevPoint.x, dy1 = y1 - prevPoint.y;
      swipeVector.setCoords(swipeVector.x + dx1, swipeVector.y + dy1, swipeVector.z + Math.sqrt(dx1 * dx1 + dy1 * dy1));
      double dx2 = x2 - prevPoint2.x, dy2 = y2 - prevPoint2.y;
      swipeVector2.setCoords(swipeVector2.x + dx2, swipeVector2.y + dy2, swipeVector2.z + Math.sqrt(dx2 * dx2 + dy2 * dy2));

      // Swipes that have opposite directions can be detected earlier
      if (swipeVector.z > GUESSING_MIN_SWIPE_LENGTH_OPPOSITE || swipeVector2.z > GUESSING_MIN_SWIPE_LENGTH_OPPOSITE) {
        if (swipeVector.y * swipeVector2.y <= 0) {
          panningMode = PANNING_DUAL_POINTER_GENERAL;
        }
      }

      // If the swipes have the same direction, check their angle
      if (panningMode == PANNING_DUAL_POINTER_GUESSING
          && (swipeVector.z > GUESSING_MIN_SWIPE_LENGTH_SAME || swipeVector2.z > GUESSING_MIN_SWIPE_LENGTH_SAME)) {
        // If one of the swipes is too horizontal, it's the general case
        float cos = (float) ((swipeVector.x) / Math.sqrt(swipeVector.x * swipeVector.x + swipeVector.y * swipeVector.y));
        if (Math.abs(cos) > GUESSING_SWIPE_MAX_ABS_COS) {
          panningMode = PANNING_DUAL_POINTER_GENERAL;
        } else {
          cos = (float) ((swipeVector2.x) / Math.sqrt(swipeVector2.x * swipeVector2.x + swipeVector2.y * swipeVector2.y));
          if (Math.abs(cos) > GUESSING_SWIPE_MAX_ABS_COS) {
            panningMode = PANNING_DUAL_POINTER_GENERAL;
          } else {
            panningMode = PANNING_DUAL_POINTER_VIEW_ANGLE;
          }
        }
      }
    }
    
    // Detect rotation/scaling gesture if general panning mode is switched off
    if (panningMode == PANNING_DUAL_POINTER_GENERAL && !options.isGeneralPanningMode()) {
      float factor = calculateRotationScalingFactor(prevPoint.x - prevPoint2.x, prevPoint.y - prevPoint2.y, x2 - x1, y2 - y1);
      if (factor > 0.75f * Math.pow(2.0f, options.getZoomRotateDetectionBalance())) {
        panningMode = PANNING_DUAL_POINTER_ROTATION;
      } else {
        panningMode = PANNING_DUAL_POINTER_SCALING;
      }
    }

    // The general case requires previous coordinates for both pointers,
    // calculate them
    switch (panningMode) {
    case PANNING_DUAL_POINTER_GENERAL:
      prevTouchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
      prevTouchPoint2 = screenToWorld(x2, y2, Const.PANNING_SPEED);
      break;
    case PANNING_DUAL_POINTER_ROTATION:
    case PANNING_DUAL_POINTER_SCALING:
      prevTouchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
      prevTouchPoint2 = screenToWorld(x2, y2, Const.PANNING_SPEED);
      anchorPoint = screenToWorld((x1 + x2) * 0.5, (y1 + y2) * 0.5, Const.PANNING_SPEED);
      prevVector.setCoords(x2 - x1, y2 - y1);
      break;
    default:
      prevPoint.setCoords(x1, y1);
      prevPoint2.setCoords(x2, y2);
      break;
    }
  }

  private void pointerMovedDualViewAngle(float x, float y) {
    Point3D cameraPos;
    Point3D focusPoint;
    Vector3D upVector;
    float tiltDeg;
    mapRenderer.setGeneralLock(true);
    try {
      cameraPos = mapRenderer.getCameraPos();
      focusPoint = mapRenderer.getFocusPoint();
      upVector = mapRenderer.getUpVector();
      tiltDeg = mapRenderer.getTilt();
    }
    finally {
      mapRenderer.setGeneralLock(false);
    }

    Vector3D cameraVec = new Vector3D(cameraPos, focusPoint);
    Vector3D tiltAxis = Vector3D.crossProduct(cameraVec, upVector);

    float angle = (float) ((y - prevPoint.y) * VIEW_ANGLE_PX_TO_RAD * Const.RAD_TO_DEG);
    Range tiltRange = constraints.getTiltRange();
    angle = Utils.toRange(tiltDeg + angle, tiltRange.min, tiltRange.max) - tiltDeg;
    
    double[] tiltTransform = getTiltMatrix(focusPoint, tiltAxis, angle);
    focusPoint = GeomUtils.transform(focusPoint, tiltTransform); // identity
    cameraPos = GeomUtils.transform(cameraPos, tiltTransform);
    upVector = GeomUtils.transform(upVector, tiltTransform);

    mapRenderer.setLookAtParams(cameraPos.x, cameraPos.y, cameraPos.z, focusPoint.x, focusPoint.y, focusPoint.z, upVector.x, upVector.y, upVector.z, false, true, true);
    
    prevPoint.setCoords(x, y);
  }
  
  private void pointerMovedDualRotation(float x1, float y1, float x2, float y2) {
    // Check whether to transition to scaling mode - if movement is along the rotation axis, switch to scaling mode
    float factor = calculateRotationScalingFactor(prevVector.x, prevVector.y, x2 - x1, y2 - y1);
    if (factor < 0.25f * Math.pow(2.0f, 0.5f * options.getZoomRotateDetectionBalance())) {
      //Log.debug("pointerMovedDualRotation: switching to scaling mode: " + factor);
      panningMode = PANNING_DUAL_POINTER_SCALING;
      pointerMovedDualScaling(x1, y1, x2, y2);
      return;
    }
    
    // Get various parameters
    Point3D cameraPos;
    Point3D focusPoint;
    Vector3D upVector;
    mapRenderer.setGeneralLock(true);
    try {
      cameraPos = mapRenderer.getCameraPos();
      focusPoint = mapRenderer.getFocusPoint();
      upVector = mapRenderer.getUpVector();
    }
    finally {
      mapRenderer.setGeneralLock(false);
    }

    // Calculate world coordinates for touch points
    Point3D touchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
    Point3D touchPoint2 = screenToWorld(x2, y2, Const.PANNING_SPEED);

    // Rotate
    if (touchPoint != null && touchPoint2 != null && prevTouchPoint != null && prevTouchPoint2 != null && anchorPoint != null) {
      if (constraints.isRotatable()) {
        Vector3D touchVector = new Vector3D(touchPoint, touchPoint2);
        Vector3D prevTouchVector = new Vector3D(prevTouchPoint, prevTouchPoint2);
        double combinedLength = touchVector.getLength() * prevTouchVector.getLength();
        if (combinedLength > 0.0) {
          Vector3D axis = GeomUtils.transform(new Vector3D(0, 0, 1), renderProjection.getLocalFrameMatrix(anchorPoint));
          Vector3D cross = Vector3D.crossProduct(touchVector, prevTouchVector);
          double cos = (touchVector.x * prevTouchVector.x + touchVector.y * prevTouchVector.y + touchVector.z * prevTouchVector.z) / combinedLength;
          double angle = Math.acos(Utils.toRange(cos, -1, 1)) * -Math.signum(Vector3D.dotProduct(axis, cross));
        
          double[] rotateTransform = getRotateMatrix(anchorPoint, (float) angle * Const.RAD_TO_DEG);
          focusPoint = GeomUtils.transform(focusPoint, rotateTransform);
          cameraPos = GeomUtils.transform(cameraPos, rotateTransform);
          upVector = GeomUtils.transform(upVector, rotateTransform);

          mapRenderer.setLookAtParams(cameraPos.x, cameraPos.y, cameraPos.z, focusPoint.x, focusPoint.y, focusPoint.z, upVector.x, upVector.y, upVector.z, true, false, false);
        }
      }
    }

    prevTouchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
    prevTouchPoint2 = screenToWorld(x2, y2, Const.PANNING_SPEED);
    prevVector.setCoords(x2 - x1, y2 - y1);
  }

  private void pointerMovedDualScaling(float x1, float y1, float x2, float y2) {
    // Check whether to transition to rotation mode - if movement is tangential to scaling axis, switch to rotation mode
    float factor = calculateRotationScalingFactor(prevVector.x, prevVector.y, x2 - x1, y2 - y1);
    if (factor > 3.0f * Math.pow(2.0f, 0.5f * options.getZoomRotateDetectionBalance())) {
      //Log.debug("pointerMovedDualScaling: switching to rotation mode: " + factor);
      panningMode = PANNING_DUAL_POINTER_ROTATION;
      pointerMovedDualRotation(x1, y1, x2, y2);
      return;
    }

    // Get various parameters
    Point3D cameraPos;
    Point3D focusPoint;
    Vector3D upVector;
    float bestZoom0Distance;
    mapRenderer.setGeneralLock(true);
    try {
      cameraPos = mapRenderer.getCameraPos();
      focusPoint = mapRenderer.getFocusPoint();
      upVector = mapRenderer.getUpVector();
      bestZoom0Distance = mapRenderer.getBestZoom0Distance();
    }
    finally {
      mapRenderer.setGeneralLock(false);
    }

    // Calculate world coordinates for touch points
    Point3D touchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
    Point3D touchPoint2 = screenToWorld(x2, y2, Const.PANNING_SPEED);

    // Zoom
    if (touchPoint != null && touchPoint2 != null && prevTouchPoint != null && prevTouchPoint2 != null && anchorPoint != null) {
      double length = renderProjection.getDistance(touchPoint, touchPoint2);
      double prevLength = renderProjection.getDistance(prevTouchPoint, prevTouchPoint2);
      double cameraDistance = new Vector(cameraPos.x - focusPoint.x, cameraPos.y - focusPoint.y, cameraPos.z - focusPoint.z).getLength3D();
      if (cameraDistance > 0.0 && length > 0.0 && prevLength > 0.0) {
        double lengthRatio = prevLength / length;

        Range zoomRange = constraints.getZoomRange();
        float maxCameraDistance = bestZoom0Distance / (float) Math.pow(2, zoomRange.min);
        double newDistance = cameraDistance * lengthRatio;
        if (newDistance > maxCameraDistance) {
          lengthRatio = maxCameraDistance / cameraDistance;
        } else {
          float minCameraDistance = bestZoom0Distance / (float) Math.pow(2, zoomRange.max);
          if (newDistance < minCameraDistance) {
            lengthRatio = minCameraDistance / cameraDistance;
          }
        }

        double[] scaleTransform = getTranslateMatrix(focusPoint, anchorPoint, 1 - lengthRatio);
        focusPoint = GeomUtils.transform(focusPoint, scaleTransform);
        cameraPos = GeomUtils.transform(cameraPos, scaleTransform);
        upVector = GeomUtils.transform(upVector, scaleTransform);

        cameraPos = new Point3D(focusPoint.x + (cameraPos.x - focusPoint.x) * lengthRatio, focusPoint.y + (cameraPos.y - focusPoint.y) * lengthRatio, focusPoint.z + (cameraPos.z - focusPoint.z) * lengthRatio);
      
        mapRenderer.setLookAtParams(cameraPos.x, cameraPos.y, cameraPos.z, focusPoint.x, focusPoint.y, focusPoint.z, upVector.x, upVector.y, upVector.z, false, false, true);
      }
    }

    prevTouchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
    prevTouchPoint2 = screenToWorld(x2, y2, Const.PANNING_SPEED);
    prevVector.setCoords(x2 - x1, y2 - y1);
  }

  private void pointerMovedDualGeneral(float x1, float y1, float x2, float y2) {
    // Get various parameters
    Point3D cameraPos;
    Point3D focusPoint;
    Vector3D upVector;
    float bestZoom0Distance;
    mapRenderer.setGeneralLock(true);
    try {
      cameraPos = mapRenderer.getCameraPos();
      focusPoint = mapRenderer.getFocusPoint();
      upVector = mapRenderer.getUpVector();
      bestZoom0Distance = mapRenderer.getBestZoom0Distance();
    }
    finally {
      mapRenderer.setGeneralLock(false);
    }

    // Calculate world coordinates for touch points
    Point3D touchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
    Point3D touchPoint2 = screenToWorld(x2, y2, Const.PANNING_SPEED);

    if (touchPoint != null && touchPoint2 != null && prevTouchPoint != null && prevTouchPoint2 != null) {
      // Calculate transformation from previous to current touch point
      double[] translateTransform = getTranslateMatrix(touchPoint, prevTouchPoint, 1);
      focusPoint = GeomUtils.transform(focusPoint, translateTransform);
      cameraPos = GeomUtils.transform(cameraPos, translateTransform);
      upVector = GeomUtils.transform(upVector, translateTransform);

      // Rotate
      if (constraints.isRotatable()) {
        Vector3D touchVector = new Vector3D(touchPoint, touchPoint2);
        Vector3D prevTouchVector = new Vector3D(prevTouchPoint, prevTouchPoint2);
        double combinedLength = touchVector.getLength() * prevTouchVector.getLength();
        if (combinedLength > 0.0) {
          Vector3D axis = GeomUtils.transform(new Vector3D(0, 0, 1), renderProjection.getLocalFrameMatrix(prevTouchPoint));
          Vector3D cross = Vector3D.crossProduct(touchVector, prevTouchVector);
          double cos = (touchVector.x * prevTouchVector.x + touchVector.y * prevTouchVector.y + touchVector.z * prevTouchVector.z) / combinedLength;
          double angle = Math.acos(Utils.toRange(cos, -1, 1)) * -Math.signum(Vector3D.dotProduct(axis, cross));

          double[] rotateTransform = getRotateMatrix(prevTouchPoint, (float) angle * Const.RAD_TO_DEG);
          focusPoint = GeomUtils.transform(focusPoint, rotateTransform);
          cameraPos = GeomUtils.transform(cameraPos, rotateTransform);
          upVector = GeomUtils.transform(upVector, rotateTransform);
        }
      }

      // Scale
      double length = renderProjection.getDistance(touchPoint, touchPoint2);
      double prevLength = renderProjection.getDistance(prevTouchPoint, prevTouchPoint2);
      double cameraDistance = new Vector(cameraPos.x - focusPoint.x, cameraPos.y - focusPoint.y, cameraPos.z - focusPoint.z).getLength3D();
      if (cameraDistance > 0.0 && length > 0.0 && prevLength > 0.0) {
        double lengthRatio = prevLength / length;
        Range zoomRange = constraints.getZoomRange();
        float maxCameraDistance = bestZoom0Distance / (float) Math.pow(2, zoomRange.min);
        double newDistance = cameraDistance * lengthRatio;
        if (newDistance > maxCameraDistance) {
          lengthRatio = maxCameraDistance / cameraDistance;
        } else {
          float minCameraDistance = bestZoom0Distance / (float) Math.pow(2, zoomRange.max);
          if (newDistance < minCameraDistance) {
            lengthRatio = minCameraDistance / cameraDistance;
          }
        }

        double[] scaleTransform = getTranslateMatrix(focusPoint, prevTouchPoint, 1 - lengthRatio);
        focusPoint = GeomUtils.transform(focusPoint, scaleTransform);
        cameraPos = GeomUtils.transform(cameraPos, scaleTransform);
        upVector = GeomUtils.transform(upVector, scaleTransform);

        cameraPos = new Point3D(focusPoint.x + (cameraPos.x - focusPoint.x) * lengthRatio, focusPoint.y + (cameraPos.y - focusPoint.y) * lengthRatio, focusPoint.z + (cameraPos.z - focusPoint.z) * lengthRatio);
      }

      mapRenderer.setLookAtParams(cameraPos.x, cameraPos.y, cameraPos.z, focusPoint.x, focusPoint.y, focusPoint.z, upVector.x, upVector.y, upVector.z, true, true, true);
    }
    

    prevTouchPoint = screenToWorld(x1, y1, Const.PANNING_SPEED);
    prevTouchPoint2 = screenToWorld(x2, y2, Const.PANNING_SPEED);
  }
  
  private Point3D screenToWorld(double x, double y, float frac) {
    return mapRenderer.screenToWorld(x, y, false);
  }
  
  private double[] getTranslateMatrix(Point3D point1, Point3D point2, double t) {
    return renderProjection.getTranslateMatrix(point1, point2, t);
  }

  private double[] getRotateMatrix(Point3D point, float angle) {
    Point3D origin = renderSurface.getRotationOrigin(point);
    double[] trans1 = new double[16];
    Matrix.setTranslateM(trans1, -origin.x, -origin.y, -origin.z);
    Vector3D axis = renderProjection.getNormal(point);
    double[] rotate = new double[16];
    Matrix.setRotationM(rotate, axis.x, axis.y, axis.z, angle);
    double[] matrix = new double[16];
    Matrix.multiplyMM(matrix, 0, rotate, 0, trans1, 0);
    matrix[12] += origin.x;
    matrix[13] += origin.y;
    matrix[14] += origin.z;
    return matrix;
  }

  private double[] getTiltMatrix(Point3D point, Vector3D tiltAxis, float angle) {
    double len = tiltAxis.getLength();
    if (len == 0) {
      double[] matrix = new double[16];
      Matrix.setIdentityM(matrix, 0);
      return matrix;
    }
    double[] trans1 = new double[16];
    Matrix.setTranslateM(trans1, -point.x, -point.y, -point.z);
    double[] rotate = new double[16];
    Matrix.setRotationM(rotate, tiltAxis.x / len, tiltAxis.y / len, tiltAxis.z / len, angle);
    double[] matrix = new double[16];
    Matrix.multiplyMM(matrix, 0, rotate, 0, trans1, 0);
    matrix[12] += point.x;
    matrix[13] += point.y;
    matrix[14] += point.z;
    return matrix;
  }

  private static float getY(MotionEvent event, int pointerIndex) {
    // limit pointer index to pointerCount-1
    return event.getY(Math.min(pointerIndex,event.getPointerCount()-1));
  }

  private static float getX(MotionEvent event, int pointerIndex) {
    // limit pointer index to pointerCount-1
    return event.getX(Math.min(pointerIndex,event.getPointerCount()-1));
  }

  @Override
  public void click(float x, float y) {
    mapRenderer.click(x, y);
  }

  @Override
  public void longClick(float x, float y) {
    mapRenderer.longClick(x, y);
    singlePointer(x, y);
  }

  @Override
  public void doubleClick(float x, float y) {
    if (options.isDoubleClickZoomIn()) {
      Point3D touchPoint = mapRenderer.screenToWorld(x, y, false);
      if (touchPoint == null) {
        return;
      }
      mapRenderer.zoom(1, TAP_ZOOM_DURATION);
      mapRenderer.setFocusPoint(touchPoint.x, touchPoint.y, touchPoint.z, TAP_ZOOM_DURATION, false);
    }
  }

  @Override
  public void dualClick() {
    if (options.isDualClickZoomOut()) {
      mapRenderer.zoom(-1, TAP_ZOOM_DURATION);
    }
  }

  @Override
  public void singlePointer(float x, float y) {
    prevTouchPoint = screenToWorld(x, y, Const.PANNING_SPEED);
    panningMode = PANNING_SINGLE_POINTER;
  }

  @Override
  public void dualPointer(float x1, float y1, float x2, float y2) {
    swipeVector.setCoords(0, 0, 0);
    swipeVector2.setCoords(0, 0, 0);
    prevPoint.setCoords(x1, y1, 0);
    prevPoint2.setCoords(x2, y2, 0);
    panningMode = PANNING_DUAL_POINTER_GUESSING;
  }

  @Override
  public void reset() {
    renderSurface = mapRenderer.getRenderSurface();
    renderProjection = renderSurface.getRenderProjection();
    panningMode = PANNING_CLICK_GUESSING;
    doKineticPanning = false;
  }

  @Override
  public void onStartMapping() {
    if (!clickThread.isAlive()) {
      clickThread = new ClickThread(this, options);
    }
  }

  @Override
  public void onStopMapping() {
    clickThread.exitThread();
    clickThread.joinThread();
  }
}
