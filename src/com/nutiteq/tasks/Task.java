package com.nutiteq.tasks;

/**
 * Interface for tasks that support and need canceling.
 */
public interface Task extends Runnable {

  /**
   * Check if the task can be cancelled.
   * @return true if canceling is supported, false otherwise.
   */
  boolean isCancelable();
  
  /**
   * Cancel task before execution has started. Will be called only if isCancelable() returns true.
   */
  void cancel();
}
