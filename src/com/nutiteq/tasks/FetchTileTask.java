package com.nutiteq.tasks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.nutiteq.components.Components;
import com.nutiteq.components.MapTile;
import com.nutiteq.log.Log;

/**
 * Abstract base class for all tasks that fetch raster tiles from local file system, network, database and so on.
 */
public abstract class FetchTileTask implements Task {
  protected static final int BUFFER_SIZE = 4096;

  protected Components components;
  protected MapTile tile;

  protected long rasterTileId;

  /**
   * Default constructor.
   * 
   * @param tile
   *          tile to fetch.
   * @param components
   *          components used for mapping.
   * @param tileIdOffset
   *          tile id offset of the layer. Use RasterLayer.getTileIdOffset() for this.
   */
  public FetchTileTask(MapTile tile, Components components, long tileIdOffset) {
    this.components = components;
    this.tile = tile;
    rasterTileId = tileIdOffset + tile.id;
    components.retrievingTiles.add(rasterTileId);
  }

  protected void readStream(InputStream inputStream) {
    byte[] buffer = new byte[BUFFER_SIZE];
    ByteArrayOutputStream output = new ByteArrayOutputStream();

    try {
      int bytesRead;
      while ((bytesRead = inputStream.read(buffer)) != -1) {
        output.write(buffer, 0, bytesRead);
      }
      output.flush();

      finished(output.toByteArray());
    } catch (IOException e) {
      Log.error(getClass().getName() + ": Failed to fetch tile. " + e.getMessage());
    } finally {
      try {
        output.close();
        if (inputStream != null) {
          inputStream.close();
        }
      } catch (IOException e) {
        Log.error(getClass().getName() + ": Failed to close the stream. " + e.getMessage());
      }
    }
  }

  /**
   * Subclasses should overwrite this. After tile has been loaded, it should be always added to texture cache
   * (components.textureMemoryCache) and cleanUp() method must be called.
   */
  @Override
  public void run() {
  }

  @Override
  public boolean isCancelable() {
    return true;
  }

  @Override
  public void cancel() {
    components.retrievingTiles.remove(rasterTileId);
  }

  /**
   * Not part of public API.
   * Should be called when data is available. Add compressed bitmap to compressed memory cache and uncompressed
   * bitmap to texture memory cache.
   * 
   * @param data
   *          compressed bitmap.
   */
  protected void finished(byte[] data) {
    if (data == null) {
      Log.error(getClass().getName() + ": No data.");
    } else {
      Bitmap bitmap = null;
      try {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, opts);
        if (bitmap == null) {
          Log.error(getClass().getName() + ": Failed to decode the image.");
        }
      } catch (Throwable t) {
        Log.error(getClass().getName() + ": Exception while decoding the image. " + t.getMessage());
      }
      if (bitmap != null) {
        // Add the compressed image to compressedMemoryCache
        components.compressedMemoryCache.add(rasterTileId, data);
        // If not corrupt, add to the textureMemoryCache
        components.textureMemoryCache.add(rasterTileId, bitmap);
      }
    }
  }
  
  /**
   * Not part of public API.
   * Should be called when tile processing has finished or failed.
   */
  protected void cleanUp() {
    components.retrievingTiles.remove(rasterTileId);
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

}
