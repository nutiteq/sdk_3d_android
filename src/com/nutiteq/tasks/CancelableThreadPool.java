package com.nutiteq.tasks;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Special thread pool executor for tasks that support canceling. Also allows
 * tasks to be associated with priorities, with tasks having higher priorities
 * being executed before tasks having lower priorities.
 */
public class CancelableThreadPool {
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public interface StatusHandler {
    void onActivated();
    void onDeactivated();
  }

  private static class TaskRecord {
    public final Runnable runnable;
    public final Object tag;
    public final int priority;
    public final long sequence;

    public TaskRecord(Runnable runnable, int priority, long sequence, Object tag) {
      this.runnable = runnable;
      this.priority = priority;
      this.sequence = sequence;
      this.tag = tag;
    }
  }

  private static class Worker extends Thread {
    CancelableThreadPool threadPool;

    public Worker(CancelableThreadPool threadPool) {
      this.threadPool = threadPool;
    }

    @Override
    public void run() {
      synchronized (threadPool) {
        if (threadPool.activeWorkerCount++ == 0) {
          StatusHandler handler = threadPool.statusHandler;
          if (handler != null) {
            handler.onActivated();
          }
        }
      }
      try {
        while (true) {
          synchronized (threadPool) {
            // Check if there are any new tasks
            if (threadPool.taskRecords.size() == 0) {

              // Notify status handler if needed
              if (--threadPool.activeWorkerCount == 0) {
                StatusHandler handler = threadPool.statusHandler;
                if (handler != null) {
                  handler.onDeactivated();
                }
              }

              // Wait until notified or exit thread if interrupted
              try {
                threadPool.wait();
              } catch (InterruptedException e) {
                return;
              }
            }

            // Notify status handler if needed
            if (threadPool.activeWorkerCount++ == 0) {
              StatusHandler handler = threadPool.statusHandler;
              if (handler != null) {
                handler.onActivated();
              }
            }
          }

          // Request another task, execute it if it's not null
          while (true) {
            Runnable runnable = threadPool.getNextTask();
            if (runnable != null) {
              runnable.run();
            } else {
              break;
            }

            if (threadPool.shouldTerminateWorker(this)) {
              return;
            }

            // Wait a bit and check for interruption
            Thread.yield();
            if (Thread.interrupted()) {
              return;
            }
          }
        }
      } finally {
        synchronized (threadPool) {
          if (--threadPool.activeWorkerCount == 0) {
            StatusHandler handler = threadPool.statusHandler;
            if (handler != null) {
              handler.onDeactivated();
            }
          }
        }
      }
    }
  }

  private static final int DEFAULT_PRIORITY = 0;

  private int poolSize;

  private long taskCount;
  
  private int activeWorkerCount;

  private Set<Worker> workers = new HashSet<Worker>();
  
  private StatusHandler statusHandler;

  private PriorityQueue<TaskRecord> taskRecords = new PriorityQueue<TaskRecord>(
      1, new Comparator<TaskRecord>() {
        @Override
        public int compare(TaskRecord record1, TaskRecord record2) {
          if (record1.priority != record2.priority) {
            return record2.priority - record1.priority;
          }
          return (int) (record1.sequence - record2.sequence);
        }
      });

  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public CancelableThreadPool(int poolSize) {
    this.poolSize = poolSize;

    for (int tsj = 0; tsj < poolSize; tsj++) {
      Worker worker = new Worker(this);
      worker.setPriority(Thread.MIN_PRIORITY);
      worker.start();
      workers.add(worker);
    }
  }

  /**
   * Get the number of threads currently used in this thread pool.
   * 
   * @return current thread pool size.
   */
  public synchronized int getPoolSize() {
    return poolSize;
  }

  /**
   * Set the number of threads used in this thread pool.
   * Default is one.
   * 
   * @param poolSize
   *          new thread pool size.
   */
  public synchronized void setPoolSize(int poolSize) {
    for (int tsj = workers.size(); tsj < poolSize; tsj++) {
      Worker worker = new Worker(this);
      worker.setPriority(Thread.MIN_PRIORITY);
      worker.start();
      workers.add(worker);
    }

    this.poolSize = poolSize;
  }

  /**
   * Get number of active workers.
   * 
   * @return number of active workers
   */
  public synchronized int getActiveWorkerCount() {
    return activeWorkerCount;
  }

  /**
   * Execute task with given priority. Associate a custom tag with the task. Tasks will be queued and processed according to priorities.
   * 
   * @param runnable
   *          task to execute.
   * @param tag
   *          tag for the task. Tags can be used to identify tasks later.
   * @param priority
   *          task relative priority. Default priority is 0.
   */
  public synchronized void execute(Runnable runnable, int priority, Object tag) {
    taskRecords.add(new TaskRecord(runnable, priority, taskCount, tag));
    taskCount++;
    notify();
  }

  /**
   * Execute task with given priority. Tasks will be queued and processed according to priorities.
   * 
   * @param runnable
   *          task to execute.
   * @param priority
   *          task relative priority. Default priority is 0.
   */
  public void execute(Runnable runnable, int priority) {
    execute(runnable, priority, null);
  }

  /**
   * Execute task with default priority. Tasks will be queued and processed according to priorities.
   * 
   * @param runnable
   *          task to execute.
   */
  public void execute(Runnable runnable) {
    execute(runnable, DEFAULT_PRIORITY);
  }

  /**
   * Cancel all tasks that are queued and not yet started executing. If any queued task is
   * an instance of com.nutiteq.Task, then isCancelable() method is called first to determine
   * if the task can be cancelled. If task can be canceled, then cancel() method is called afterwards.
   */
  public synchronized void cancelAll() {
    Iterator<TaskRecord> it = taskRecords.iterator();
    while (it.hasNext()) {
      TaskRecord taskRecord = it.next();
      if (taskRecord.runnable instanceof Task) {
        Task task = (Task) taskRecord.runnable;
        if (task.isCancelable()) {
          task.cancel();
          it.remove();
        }
      }
    }
  }

  /**
   * Cancel tasks with specified tags that are queued and not yet started executing. If any queued task is
   * an instance of com.nutiteq.Task, then isCancelable() method is called first to determine
   * if the task can be cancelled. If task can be canceled, then cancel() method is called afterwards.
   * 
   * @param tags
   *          collection of tags to match against task tags. If tags are equal, tasks are cancelled.
   */
  public synchronized void cancelWithTags(Collection<? extends Object> tags) {
    Iterator<TaskRecord> it = taskRecords.iterator();
    while (it.hasNext()) {
      TaskRecord taskRecord = it.next();
      if (!tags.contains(taskRecord.tag)) {
        continue;
      }

      if (taskRecord.runnable instanceof Task) {
        Task task = (Task) taskRecord.runnable;
        if (task.isCancelable()) {
          task.cancel();
          it.remove();
        }
      }
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized void setStatusHandler(StatusHandler handler) {
    if (activeWorkerCount != 0 && statusHandler != null) {
      statusHandler.onDeactivated();
    }
    statusHandler = handler;
    if (activeWorkerCount != 0 && statusHandler != null) {
      statusHandler.onActivated();
    }
  }

  private synchronized Runnable getNextTask() {
    if (taskRecords.size() > 0) {
      return taskRecords.poll().runnable;
    }
    return null;
  }

  private synchronized boolean shouldTerminateWorker(Worker worker) {
    if (poolSize < workers.size()) {
      workers.remove(worker);
      return true;
    }
    return false;
  }
}
