package com.nutiteq.vectordatasources;

import java.util.Collection;

import com.nutiteq.components.CullState;
import com.nutiteq.components.Envelope;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.projections.Projection;

/**
 * Interface for vector element data sources.
 *
 * @param <T>
 *          element type for the data source
 */
public interface VectorDataSource<T extends VectorElement> {

  /**
   * Interface for monitoring element and data source change events.
   */
  public interface OnChangeListener {
    /**
     * Called when specific element has changed and needs to be updated.
     * 
     * @param element
     *          changed element.
     */
    void onElementChanged(VectorElement element);
    
    /**
     * Called when whole dataset has changed and needs to be updated.
     */
    void onElementsChanged();
  }

  /**
   * Get projection for this data source.
   * 
   * @return projection
   */
  Projection getProjection();
  
  /**
   * Get the tight envelope encompassing all elements of the data source. If this query is not available, null should be returned.
   * 
   * @return tight envelope for all the elements or null if this query is not available.
   */
  Envelope getDataExtent();
  
  /**
   * Load all the elements within given envelope.
   * 
   * @param cullState
   *          state for describing view parameters and conservative view envelope
   * @return collection of elements. If no elements are available, null may be returned.
   */
  Collection<T> loadElements(CullState cullState);
  
  /**
   * Call-back to inform data source that its element has changed.
   */
  void onElementChanged(VectorElement element);

  /**
   * Register listener for data source change events.
   * 
   * @param listener
   *          listener for change events.
   */
  void addOnChangeListener(OnChangeListener listener);

  /**
   * Unregister listener for data source change events.
   * 
   * @param listener
   *          previously added listener.
   */
  void removeOnChangeListener(OnChangeListener listener);
}
