package com.nutiteq.vectordatasources;

import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileData;
import com.nutiteq.projections.Projection;

/**
 * Interface for all vector tile data sources.
 */
public interface VectorTileDataSource {

  /**
   * Interface for monitoring data source change events.
   */
  public interface OnChangeListener {
    /**
     * Called when tile set has changed and needs to be updated.
     */
    void onTilesChanged();
  }
  
  /**
   * Get projection for this data source.
   * 
   * @return projection
   */
  Projection getProjection();
  
  /**
   * Get minimum zoom level supported by this data source.
   * 
   * @return minimum zoom level supported (inclusive)
   */
  int getMinZoom();
  
  /**
   * Get maximum zoom level supported by this data source.
   * 
   * @return maximum zoom level supported (inclusive)
   */
  int getMaxZoom();
  
  /**
   * Load the specified vector tile.
   * 
   * @param tile
   *          tile to load.
   * @return Tile data. If tile is not available, null may be returned.
   */
  TileData loadTile(MapTile tile);

  /**
   * Register listener for data source change events.
   * 
   * @param listener
   *          listener for change events.
   */
  void addOnChangeListener(OnChangeListener listener);

  /**
   * Unregister listener for data source change events.
   * 
   * @param listener
   *          previously added listener.
   */
  void removeOnChangeListener(OnChangeListener listener);
}
