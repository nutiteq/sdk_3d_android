package com.nutiteq.vectordatasources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import com.nutiteq.components.CullState;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MutableEnvelope;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.projections.Projection;

/**
 * Local vector data source that supports adding and removing of elements.
 * All elements are always rendered, so this is suitable only in cases when there are only a few elements.
 * 
 * @param <T>
 *          element type for the data source
 */
public class UnculledVectorDataSource<T extends VectorElement> extends AbstractVectorDataSource<T> {
  private final List<T> elements = new LinkedList<T>();

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the data source.
   */
  public UnculledVectorDataSource(Projection projection) {
    super(projection);
  }

  /**
   * Remove all elements from data source.
   */
  public void clear() {
    synchronized (this) {
      for (ListIterator<T> it = elements.listIterator(); it.hasNext(); ) {
        T element = it.next();
        it.remove();
        element.detachFromDataSource();
      }
    }

    notifyElementsChanged();
  }

  /**
   * Add a new element to data source.
   * 
   * @param element
   *          the new element to be added
   */
  public void add(T element) {
    synchronized (this) {
      element.attachToDataSource(this);
      elements.add(element);
    }

    notifyElementsChanged();
  }

  /**
   * Add collection of elements to the data source.
   * 
   * @param elements
   *          the collection of elements to be added
   */
  public void addAll(Collection<? extends T> elements) {
    synchronized (this) {
      for (T element : elements) {
        element.attachToDataSource(this);
        this.elements.add(element);
      }
    }

    notifyElementsChanged();
  }

  /**
   * Remove an existing element from the data source.
   * 
   * @param element
   *          the element to be removed
   */
  public void remove(T element) {
    synchronized (this) {
      if (elements.remove(element)) {
        element.detachFromDataSource();
      }
    }

    notifyElementsChanged();
  }

  /**
   * Remove collection of elements from the data source.
   * 
   * @param elements
   *          the collection of elements to be removed
   */
  public void removeAll(Collection<? extends T> elements) {
    Set<T> elementSet = new HashSet<T>(elements);
    synchronized (this) {
      for (ListIterator<T> it = this.elements.listIterator(); it.hasNext(); ) {
        T element = it.next();
        if (elementSet.contains(element)) {
          it.remove();
          element.detachFromDataSource();
        }
      }
    }

    notifyElementsChanged();
  }

  /**
   * Get list of all elements.
   * 
   * @return list of all elements.
   */
  public Collection<T> getAll() {
    synchronized (this) {
      return new ArrayList<T>(elements);
    }
  }

  @Override
  public Envelope getDataExtent() {
    MutableEnvelope envelope = new MutableEnvelope();

    synchronized (this) {
      for (T element : elements) {
        VectorElement.InternalState internalState = element.getInternalState();
        if (internalState != null) {
          envelope.add(projection.fromInternal(element.getInternalEnvelope()));
        }
      }
    }

    return new Envelope(envelope);
  }

  @Override
  public Collection<T> loadElements(CullState cullState) {
    return getAll();
  }
}
