package com.nutiteq.vectordatasources;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nutiteq.components.CullState;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MutableEnvelope;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.projections.Projection;
import com.nutiteq.utils.Quadtree;

/**
 * Quadtree-based local vector data source that supports adding and removing of elements.
 * As quadtree is used for element culling, only visible elements are rendered.
 * 
 * @param <T>
 *          element type for the data source
 */
public class QuadTreeVectorDataSource<T extends VectorElement> extends AbstractVectorDataSource<T> {
  private final Map<T, Envelope> elementEnvelopeMap = new HashMap<T, Envelope>();
  private final Quadtree<T> quadtree = new Quadtree<T>();

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the data source.
   */
  public QuadTreeVectorDataSource(Projection projection) {
    super(projection);
  }

  /**
   * Remove all elements from data source.
   */
  public void clear() {
    synchronized (this) {
      List<T> elements = quadtree.getAll();
      elementEnvelopeMap.clear();
      quadtree.clear();
      for (T element : elements) {
        element.detachFromDataSource();
      }
    }

    notifyElementsChanged();
  }

  /**
   * Add a new element to data source.
   * 
   * @param element
   *          the new element to be added
   * @throws IllegalArgumentException
   *          if element has already been added or element coordinates contain NaNs.
   */
  public void add(T element) {
    synchronized (this) {
      if (elementEnvelopeMap.containsKey(element)) {
        throw new IllegalArgumentException("Element already added");
      }
      element.attachToDataSource(this);
      Envelope envelope = element.getInternalEnvelope();
      quadtree.insert(envelope, element);
      elementEnvelopeMap.put(element, envelope);
    }

    notifyElementsChanged();
  }

  /**
   * Add collection of elements to the data source.
   * 
   * @param elements
   *          the collection of elements to be added
   * @throws IllegalArgumentException
   *          if one of the elements has already been added or element coordinates contain NaNs.
   */
  public void addAll(Collection<? extends T> elements) {
    synchronized (this) {
      for (T element : elements) {
        if (elementEnvelopeMap.containsKey(element)) {
          throw new IllegalArgumentException("Element already added");
        }
        element.attachToDataSource(this);
        Envelope envelope = element.getInternalEnvelope();
        quadtree.insert(envelope, element);
        elementEnvelopeMap.put(element, envelope);
      }
    }

    notifyElementsChanged();
  }

  /**
   * Remove an existing element from the data source.
   * 
   * @param element
   *          the element to be removed
   */
  public void remove(T element) {
    synchronized (this) {
      Envelope envelope = elementEnvelopeMap.get(element);
      if (envelope != null) {
        quadtree.remove(envelope, element);
        elementEnvelopeMap.remove(element);
        element.detachFromDataSource();
      }
    }

    notifyElementsChanged();
  }

  /**
   * Remove collection of elements from the data source.
   * 
   * @param elements
   *          the collection of elements to be removed
   */
  public void removeAll(Collection<? extends T> elements) {
    synchronized (this) {
      for (T element : elements) {
        Envelope envelope = elementEnvelopeMap.get(element);
        if (envelope != null) {
          quadtree.remove(element.getInternalEnvelope(), element);
          elementEnvelopeMap.remove(element);
          element.detachFromDataSource();
        }
      }
    }

    notifyElementsChanged();
  }
  
  /**
   * Get list of all elements.
   * 
   * @return list of all elements.
   */
  public Collection<T> getAll() {
    synchronized (this) {
      return quadtree.getAll();
    }
  }

  @Override
  public Envelope getDataExtent() {
    MutableEnvelope envelope = new MutableEnvelope();

    synchronized (this) {
      for (Envelope elementEnv : elementEnvelopeMap.values()) {
        envelope.add(projection.fromInternal(elementEnv));
      }
    }

    return new Envelope(envelope);
  }

  @Override
  public Collection<T> loadElements(CullState cullState) {
    synchronized (this) {
      return quadtree.query(cullState.envelope);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public void onElementChanged(VectorElement element) {
    synchronized (this) {
      Envelope oldEnvelope = elementEnvelopeMap.get(element);
      Envelope newEnvelope = element.getInternalEnvelope();
      if (!newEnvelope.equals(oldEnvelope)) {
        elementEnvelopeMap.put((T) element, newEnvelope);
        quadtree.remove(oldEnvelope, (T) element);
        quadtree.insert(newEnvelope, (T) element);
      }
    }

    notifyElementChanged((T) element);
  }
}
