package com.nutiteq.vectordatasources;

import java.util.LinkedList;
import java.util.List;

import com.nutiteq.geometry.VectorElement;
import com.nutiteq.projections.Projection;

/**
 * Abstract base class for vector data sources.
 * It is recommended to use this as a base class for all vector data sources.
 * 
 * This class provides default implementation for listener registration and other common data source methods.
 * Subclasses need to define their own implementations of getDataExtent and loadElements methods.
 * 
 * @param <T>
 *          element type for the data source
 */
public abstract class AbstractVectorDataSource<T extends VectorElement> implements VectorDataSource<T> {
  private final List<OnChangeListener> onChangeListeners = new LinkedList<OnChangeListener>();
  
  protected final Projection projection;

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the data source.
   */
  protected AbstractVectorDataSource(Projection projection) {
    this.projection = projection;
  }

  @Override
  public Projection getProjection() {
    return projection;
  }

  @SuppressWarnings("unchecked")
  @Override
  public void onElementChanged(VectorElement element) {
    notifyElementChanged((T) element);
  }

  @Override
  public void addOnChangeListener(OnChangeListener listener) {
    synchronized (onChangeListeners) {
      onChangeListeners.add(listener);
    }
  }

  @Override
  public void removeOnChangeListener(OnChangeListener listener) {
    synchronized (onChangeListeners) {
      onChangeListeners.remove(listener);
    }
  }

  /**
   * Notify listeners that specific vector element has changed.
   * This method will force the element to be reloaded from original datasource.
   * 
   * @param elementId
   *          vector element id that has changed
   */
  protected void notifyElementChanged(T element) {
    synchronized (onChangeListeners) {
      for (OnChangeListener listener : onChangeListeners) {
        listener.onElementChanged(element);
      }
    }
  }

  /**
   * Notify listeners that vector elements have changed.
   * This method will force elements to be reloaded from original datasource.
   */
  protected void notifyElementsChanged() {
    synchronized (onChangeListeners) {
      for (OnChangeListener listener : onChangeListeners) {
        listener.onElementsChanged();
      }
    }
  }
}
