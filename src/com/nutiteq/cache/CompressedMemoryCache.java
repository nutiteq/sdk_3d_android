package com.nutiteq.cache;

import com.nutiteq.utils.LinkedLongHashMap;

/**
 * Cache for bitmaps that are held in compressed form to save memory.
 */
public class CompressedMemoryCache {
  private int maxSizeInBytes;
  private int sizeInBytes;

  private LinkedLongHashMap<byte[]> cache = new LinkedLongHashMap<byte[]>() {

    @Override
    protected boolean removeEldestEntry(long tileId, byte[] compressed) {
      // If the size limit has been exceeded, delete all compressed images that
      // don't fit
      if (sizeInBytes > maxSizeInBytes) {
        sizeInBytes -= compressed.length;
        return true;
      }
      return false;
    }
  };

  /**
   * Set new cache size limit. If new size is smaller than existing size, some elements can be evicted.
   * 
   * @param maxSizeInBytes
   *          new size limit for the cache.
   */
  public synchronized void setSize(int maxSizeInBytes) {
    this.maxSizeInBytes = maxSizeInBytes;
    cache.removeEldestEntries();
  }

  /**
   * Clear the cache.
   */
  public synchronized void clear() {
    cache.clear();
    sizeInBytes = 0;
  }

  /**
   * Add compressed bitmap to the cache. Existing bitmaps can be evicted if cache size limit is exceeded.
   * 
   * @param tileId
   *          id of the tile to add.
   * @param compressed
   *          compressed tile bitmap.
   */
  public synchronized void add(long tileId, byte[] compressed) {
    if (maxSizeInBytes == 0) {
      return;
    }
    
    sizeInBytes += compressed.length;
    cache.put(tileId, compressed);
  }

  /**
   * Get existing compressed bitmap from the cache. The bitmap will be the last in the eviction queue after this call.
   * 
   * @param tileId
   *          id of the tile to get.
   * @return compressed bitmap or null if the cache does not contain it.
   */
  public synchronized byte[] get(long tileId) {
    if (maxSizeInBytes == 0) {
      return null;
    }
    
    return cache.get(tileId);
  }

  /**
   * Remove existing tile from the cache.
   * 
   * @param tileId
   *          id of the tile to remove.
   */
  public synchronized void remove(long tileId) {
    if (maxSizeInBytes == 0) {
      return;
    }
    
    byte[] compressed = cache.get(tileId);
    if (compressed == null) {
      return;
    }

    sizeInBytes -= compressed.length;
    cache.remove(tileId);
  }

  /**
   * Remove consecutive tile range from the cache.
   * 
   * @param firstTileId
   *          id of the first tile to remove (inclusive).
   * @param lastTileId
   *          id of the last tile to remove (exclusive).
   */
  public synchronized void removeRange(long firstTileId, long lastTileId) {
    if (maxSizeInBytes == 0) {
      return;
    }

    long[] tileIds = cache.keys().toArray();
    for (long tileId : tileIds) {
      if (tileId >= firstTileId && tileId < lastTileId) {
        byte[] compressed = cache.get(tileId);
        if (compressed != null) {
          sizeInBytes -= compressed.length;
          cache.remove(tileId);
        }
      }
    }
  }
}
