package com.nutiteq.cache;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.utils.LinkedLongHashMap;
import com.nutiteq.vectortile.VectorTile;

public class VectorTileCache {
  private int count;
  private int maxCount;
  private int addCount;

  private LinkedLongHashMap<VectorTile> cache = new LinkedLongHashMap<VectorTile>() {

    @Override
    protected boolean removeEldestEntry(long tileId, VectorTile tile) {
      if (count > maxCount) {
        count--;
        return true;
      }
      return false;
    }
  };

  /**
   * Set new cache size limit. If new size is smaller than existing size, some elements can be evicted.
   * 
   * @param maxCount
   *          new size limit for the cache.
   */
  public synchronized void setSize(int maxCount) {
    this.maxCount = maxCount;
    cache.removeEldestEntries();
  }

  /**
   * Clear the cache.
   */
  public synchronized void clear() {
    cache.clear();
    count = 0;
  }

  /**
   * Add tile to the cache. Existing tiles can be evicted if cache size limit is exceeded.
   * 
   * @param tileId
   *          id of the tile to add.
   * @param tile
   *          vector tile.
   */
  public synchronized void add(long tileId, VectorTile tile) {
    if (maxCount == 0) {
      return;
    }
    
    count++;
    cache.put(tileId, tile);
    addCount++;
  }

  /**
   * Get existing tile from the cache. The tile will be the last in the eviction queue after this call.
   * 
   * @param tileId
   *          id of the tile to get.
   * @return tile or null if the cache does not contain it.
   */
  public synchronized VectorTile get(long tileId) {
    if (maxCount == 0) {
      return null;
    }
    
    return cache.get(tileId);
  }

  /**
   * Get existing tile from the cache. Element eviction order will not be changed.
   * 
   * @param tileId
   *          id of the tile to get.
   * @return tile or null if the cache does not contain it.
   */
  public synchronized VectorTile getWithoutMod(long tileId) {
    if (maxCount == 0) {
      return null;
    }
    
    return cache.getWithoutMod(tileId);
  }

  /**
   * Remove existing tile from the cache.
   * 
   * @param tileId
   *          id of the tile to remove.
   */
  public synchronized void remove(long tileId) {
    if (maxCount == 0) {
      return;
    }
    
    VectorTile tile = cache.get(tileId);
    if (tile == null) {
      return;
    }

    count--;
    cache.remove(tileId);
  }

  /**
   * Remove consecutive tile range from the cache.
   * 
   * @param firstTileId
   *          id of the first tile to remove (inclusive).
   * @param lastTileId
   *          id of the last tile to remove (exclusive).
   */
  public synchronized void removeRange(long firstTileId, long lastTileId) {
    if (maxCount == 0) {
      return;
    }

    long[] tileIds = cache.keys().toArray();
    for (long tileId : tileIds) {
      if (tileId >= firstTileId && tileId < lastTileId) {
        VectorTile tile = cache.get(tileId);
        if (tile != null) {
          count--;
          cache.remove(tileId);
        }
      }
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized int createAndDeleteTiles(GL10 gl) {
    int createCount = addCount;
    addCount = 0;
    return createCount;
  }
}
