package com.nutiteq.cache;

import java.util.List;

import com.nutiteq.db.PersistentCacheDbHelper;
import com.nutiteq.log.Log;
import com.nutiteq.utils.LinkedLongHashMap;
import com.nutiteq.utils.LongMap;

/**
 * Persistent file based cache for map tiles. Cache is organized as fully associative LRU cache.
 */
public class PersistentCache {
  private int maxSizeInBytes;
  private int sizeInBytes;

  private LinkedLongHashMap<Integer> cache = new LinkedLongHashMap<Integer>() {
    
    @Override
    protected boolean removeEldestEntry(long tileId, Integer size) {
      // If the size limit has been exceeded, delete all compressed images
      // that don't fit
      if (sizeInBytes > maxSizeInBytes) {
        sizeInBytes -= size;
        try {
          dbHelper.remove(tileId);
        }
        catch (RuntimeException e) {
          Log.debug("PersistentCache: database exception: " + e.getMessage());
        }
        return true;
      }
      return false;
    }
  };

  private String path;
  private PersistentCacheDbHelper dbHelper;
  
  /**
   * Set path of the cache file to use.
   * 
   * @param path
   *          path of the cache file.
   */
  public synchronized void setPath(String path) {
    closeDb();
    this.path = path;
    reopenDb();
  }

  /**
   * Set new cache size limit. If new size is smaller than existing size, some elements can be evicted.
   * 
   * @param maxSizeInBytes
   *          new size limit for the cache.
   */
  public synchronized void setSize(int maxSizeInBytes) {  
    this.maxSizeInBytes = maxSizeInBytes;
    cache.removeEldestEntries();
  }

  /**
   * Clear the cache.
   */
  public synchronized void clear() {
    try {
      dbHelper.clear();
      cache.clear();
      sizeInBytes = 0;
    }
    catch (RuntimeException e) {
      Log.debug("PersistentCache: database exception: " + e.getMessage());
    }
  }

  /**
   * Add existing compressed raster map tile to the cache. Existing tiles can be evicted if cache size limit is exceeded.
   * 
   * @param tileId
   *          id of the tile to add.
   * @param compressed
   *          compressed bitmap for the tile. 
   */
  public synchronized void add(long tileId, byte[] compressed) {
    if (dbHelper == null || maxSizeInBytes == 0) {
      return;
    }
   
    sizeInBytes += compressed.length;
    cache.put(tileId, compressed.length);
    try {
      dbHelper.add(tileId, compressed);
    }
    catch (RuntimeException e) {
      Log.debug("PersistentCache: database exception: " + e.getMessage());
    }
  }
  
  /**
   * Test if compressed bitmap exists in the cache. This test can be much faster than actually reading the data.
   * 
   * @param tileId
   *          id of the tile to test.
   * @return true if tile exists, false otherwise.
   */
  public synchronized boolean contains(long tileId) {
    if (dbHelper == null || maxSizeInBytes == 0) {
      return false;
    }
    
    return cache.containsKey(tileId);
  }

  /**
   * Get existing compressed bitmap from the cache. The bitmap will be the last in the eviction queue after this call.
   * 
   * @param tileId
   *          id of the tile to get.
   * @return compressed bitmap or null if the cache does not contain it.
   */
  public synchronized byte[] get(long tileId) {
    if (dbHelper == null || maxSizeInBytes == 0) {
      return null;
    }
    
    cache.get(tileId);
    try {
      return dbHelper.get(tileId);
    }
    catch (RuntimeException e) {
      Log.debug("PersistentCache: database exception: " + e.getMessage());
    }
    return null;
  }

  /**
   * Remove existing tile from the cache.
   * 
   * @param tileId
   *          id of the tile to remove.
   */
  public synchronized void remove(long tileId) {
    if (dbHelper == null) {
      return;
    }
    
    Integer size = cache.get(tileId);
    if (size == null) {
      return;
    }

    sizeInBytes -= size;
    cache.remove(tileId);
    try {
      dbHelper.remove(tileId);
    }
    catch (RuntimeException e) {
      Log.debug("PersistentCache: database exception: " + e.getMessage());
    }
  }
  
  /**
   * Remove consecutive tile range from the cache.
   * 
   * @param firstTileId
   *          id of the first tile to remove (inclusive).
   * @param lastTileId
   *          id of the last tile to remove (exclusive).
   */
  public synchronized void removeRange(long firstTileId, long lastTileId) {
    if (dbHelper == null) {
      return;
    }
    
    long[] tileIds = cache.keys().toArray();
    for (long tileId : tileIds) {
      if (tileId >= firstTileId && tileId < lastTileId) {
        Integer size = cache.get(tileId);
        if (size != null) {
          sizeInBytes -= size;
          cache.remove(tileId);
        }
      }
    }
    try {
      dbHelper.removeRange(firstTileId, lastTileId);
    }
    catch (RuntimeException e) {
      Log.debug("PersistentCache: database exception: " + e.getMessage());
    }
  }
  
  /**
   * Re-open persistent cache database.
   */
  public synchronized void reopenDb() {
    if (dbHelper == null && path != null) {
      dbHelper = new PersistentCacheDbHelper(path);
      
      try {
        List<LongMap.Entry<Integer>> entries = dbHelper.loadDb();
        cache.clear();
        sizeInBytes = 0;
        for (LongMap.Entry<Integer> entry : entries) {
          sizeInBytes += entry.getValue();
          cache.put(entry.getKey(), entry.getValue());
        }
      } catch (RuntimeException e) {
        Log.error("PersistentCache: error while loading tile info from database: " + e.getMessage());
      }
    }
  }

  /**
   * Close persistent cache database. Persistent cache will be unavailable after this call until setPath is called.
   */
  public synchronized void closeDb(){
    if (dbHelper != null) {
      try {
        dbHelper.closeDb();
      } catch (RuntimeException e) {
        Log.error("PersistentCache: error while closing database: " + e.getMessage());
      }
      dbHelper = null;
    }
  }
  
}
