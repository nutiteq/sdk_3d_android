package com.nutiteq.cache;

import java.util.List;

import com.nutiteq.log.Log;
import com.nutiteq.utils.LinkedLongHashMap;

/**
 * Cache for NML binary data. Can be used to store both model, mesh and texture data.
 * 
 * @pad.exclude
 */
public class NMLPersistentCache {
  
  /**
   * Cache entry for data store.
   */
  public static class CacheEntry {
    public final long id;
    public final int size;
    public CacheEntry(long id, int size) {
      this.id = id;
      this.size = size;
    }
  }

  /**
   * Data store interface.
   */
  public static interface DataStore {
    List<CacheEntry> getSortedEntries(); // entries should be ordered from oldest to newest
    byte[] get(long id);
    void add(long id, byte[] data);
    void remove(long id);
  }

  private int maxSizeInBytes;
  private int sizeInBytes;
  private int lockCount;
  private DataStore dataStore;

  private LinkedLongHashMap<Integer> cacheMap = new LinkedLongHashMap<Integer>() {
    @Override
    protected boolean removeEldestEntry(long id, Integer size) {
      if (lockCount == 0 && sizeInBytes > maxSizeInBytes) {
        sizeInBytes -= size;
        if (dataStore != null) {
          try {
            dataStore.remove(id);
          }
          catch (RuntimeException e) {
            Log.debug("NMLPersistentCache: data store exception: " + e.getMessage());
            sizeInBytes += size;
            return false;
          }
        }
        return true;
      }
      return false;
    }
  };

  public NMLPersistentCache(int maxSizeInBytes) {
    this.maxSizeInBytes = maxSizeInBytes;
    this.sizeInBytes = 0;
    this.lockCount = 0;
    this.dataStore = null;
  }
  
  /**
   * Get cache data store.
   * 
   * @return the current data store for the cache
   */
  public DataStore getDataStore() {
    return dataStore;
  }
  
  /**
   * Set cache data store.
   * 
   * @param dataStore
   *            new data store for the cache
   */
  public synchronized void setDataStore(DataStore dataStore) {
    this.dataStore = dataStore;
    cacheMap.clear();
    sizeInBytes = 0;
    if (dataStore != null) {
      List<CacheEntry> entries = dataStore.getSortedEntries();
      for (CacheEntry entry : entries) {
        sizeInBytes += entry.size;
        cacheMap.put(entry.id, entry.size);
      }
    }
  }
  
  /**
   * Set new cache size limit. If new size is smaller than existing size, some elements can be evicted.
   * 
   * @param maxSizeInBytes
   *          new size limit for the cache.
   */
  public synchronized void setSize(int maxSizeInBytes) {  
    this.maxSizeInBytes = maxSizeInBytes;
    cacheMap.removeEldestEntries();
  }
  
  /**
   * Lock the cache. This ensures that no elements will be evicted until the cache is unlocked.
   */
  public synchronized void lock() {
    lockCount++;
  }

  /**
   * Unlock the cache. If cache size is exceeded, remove eldest elements. 
   */
  public synchronized void unlock() {
    if (lockCount <= 0) {
      throw new RuntimeException("Lock count already zero");
    }
    if (--lockCount == 0) {
      cacheMap.removeEldestEntries();
    }
  }

  /**
   * Add new element to the cache. Existing elements can be evicted if cache size limit is exceeded.
   * 
   * @param id
   *          id of the element to add.
   * @param data
   *          element data. 
   */
  public synchronized void add(long id, byte[] data) {
    if (dataStore == null || maxSizeInBytes == 0) {
      return;
    }
   
    sizeInBytes += data.length;
    cacheMap.put(id, data.length);
    try {
      dataStore.add(id, data);
    }
    catch (RuntimeException e) {
      Log.debug("NMLPersistentCache: data store exception: " + e.getMessage());
      sizeInBytes -= data.length;
    }
  }
  
  /**
   * Check if the cache contains given element.
   * 
   * @param id
   *          id of the element to check.
   * @return true if element exists and false otherwise.
   */
  public synchronized boolean contains(long id) {
    if (dataStore == null || maxSizeInBytes == 0) {
      return false;
    }
    
    return cacheMap.getWithoutMod(id) != null;
  }

  /**
   * Get existing element from the cache. The element will be the last in the eviction queue after this call.
   * 
   * @param id
   *          id of the element to get.
   * @return element data or null if the cache does not contain it.
   */
  public synchronized byte[] get(long id) {
    if (dataStore == null || maxSizeInBytes == 0) {
      return null;
    }
    
    cacheMap.get(id);
    try {
      return dataStore.get(id);
    }
    catch (RuntimeException e) {
      Log.debug("NMLPersistentCache: data store exception: " + e.getMessage());
    }
    return null;
  }

  /**
   * Remove existing element from the cache.
   * 
   * @param id
   *          id of the element to remove.
   */
  public synchronized void remove(long id) {
    if (dataStore == null) {
      return;
    }
    
    Integer size = cacheMap.get(id);
    if (size == null) {
      return;
    }

    sizeInBytes -= size;
    cacheMap.remove(id);
    try {
      dataStore.remove(id);
    }
    catch (RuntimeException e) {
      Log.debug("NMLPersistentCache: data store exception: " + e.getMessage());
      sizeInBytes += size;
    }
  }
  
}
