package com.nutiteq.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;

import com.nutiteq.utils.GLUtils;

/**
 * Rendering cache for textures. Provides automatic texture generation from on TextureInfo objects.
 * Not part of public API.
 * 
 * @pad.exclude
 */
public class TextureBitmapCache {
  private int maxSizeInBytes;
  private int sizeInBytes = 0;
  private int timeStamp = 0; // frame counter

  @SuppressWarnings("serial")
  private LinkedHashMap<Bitmap, Texture> textureMap = new LinkedHashMap<Bitmap, Texture>() {
    @Override
    protected boolean removeEldestEntry(Map.Entry<Bitmap, Texture> eldest) {
      if (sizeInBytes > maxSizeInBytes) {
        sizeInBytes -= eldest.getValue().sizeInBytes;
        evictionMap.put(eldest.getKey(), eldest.getValue());
        return true;
      }
      return false;
    }
  };

  private Map<Bitmap, Texture> evictionMap = new HashMap<Bitmap, Texture>();

  public TextureBitmapCache(int maxSizeInBytes) {
    this.maxSizeInBytes = maxSizeInBytes;
  }

  public int getTexture(GL10 gl, Bitmap bitmap) {
    Texture texture = textureMap.get(bitmap);
    if (texture == null) {
      texture = evictionMap.remove(bitmap);
      if (texture != null) {
        sizeInBytes += texture.sizeInBytes;
        textureMap.put(bitmap, texture);
      }
    }
    if (texture != null) {
      texture.timeStamp = timeStamp;
      return texture.textureId;
    }
    
    synchronized(bitmap) {
      texture = new Texture(bitmap);
      texture.textureId = GLUtils.buildMipmaps(gl, bitmap);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
      sizeInBytes += texture.sizeInBytes;
      textureMap.put(bitmap, texture);
      texture.timeStamp = timeStamp;
    }
    return texture != null ? texture.textureId : 0;
  }

  public void dispose(GL10 gl) {
    for (Texture texture : evictionMap.values()) {
      GLUtils.deleteTexture(gl, texture.textureId);
    }
    evictionMap.clear();
    for (Texture texture : textureMap.values()) {
      GLUtils.deleteTexture(gl, texture.textureId);
    }
    textureMap.clear();
    sizeInBytes = 0;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized void createAndDeleteTextures(GL10 gl) {
    // Check eviction map - if there are textures with old timestamp, delete them
    if (!evictionMap.isEmpty()) {
      Iterator<Map.Entry<Bitmap, Texture>> iterator = evictionMap.entrySet().iterator();
      while (iterator.hasNext()) {
        Texture texture = iterator.next().getValue();
        if (texture.timeStamp != timeStamp && texture.timeStamp + 1 != timeStamp) {
          GLUtils.deleteTexture(gl, texture.textureId);
          iterator.remove();
        }
      }
    }

    timeStamp++;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized void onSurfaceCreated(GL10 gl) {
    textureMap.clear();
    evictionMap.clear();
    sizeInBytes = 0;
    timeStamp = 0;
  }

  /**
   * Container class
   */
  private class Texture {
    public final int sizeInBytes;
    public int textureId;
    public int timeStamp;

    public Texture(Bitmap bitmap) {
      if (bitmap != null) {
        this.sizeInBytes = (int) (bitmap.getRowBytes() * bitmap.getHeight() * 4 * 4 / 3);
      } else {
        this.sizeInBytes = 0;
      }
    }
  }
}
