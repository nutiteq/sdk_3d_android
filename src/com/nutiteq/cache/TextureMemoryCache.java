package com.nutiteq.cache;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;

import com.nutiteq.utils.GLUtils;
import com.nutiteq.utils.LinkedLongHashMap;
import com.nutiteq.utils.LongHashMap;

/**
 * Texture memory cache for raster map tiles. Cache is organized as fully associative LRU cache.
 */
public class TextureMemoryCache {
  private static final int NEW_TEXTURES_PER_FRAME = 1;
  private static final float MIPMAP_SIZE_MULTIPLIER = 1.33f;

  private int maxSizeInBytes;
  private int sizeInBytes;
  private int timeStamp = 0; // frame counter

  private LinkedLongHashMap<Texture> cache = new LinkedLongHashMap<Texture>() {
    @Override
    protected boolean removeEldestEntry(long tileId, Texture texture) {
      // If the size limit has been exceeded, move all LRU textures that don't fit to eviction map
      if (sizeInBytes > maxSizeInBytes) {
        sizeInBytes -= texture.sizeInBytes;
        evictionMap.put(tileId, texture);
        return true;
      }
      return false;
    }

    @Override
    public String toString() {
      LinkedEntry<Texture> current = first;
      String result = "";
      while (current != null) {
        result += current.value.tileId + "; ";
        current = current.next;
      }
      return result;
    }
  };

  private LongHashMap<Texture> evictionMap = new LongHashMap<Texture>();
  private List<Texture> createQueue = new LinkedList<Texture>();

  /**
   * Set new cache size limit. If new size is smaller than existing size, some elements can be evicted.
   * 
   * @param maxSizeInBytes
   *          new size limit for the cache.
   */
  public synchronized void setSize(int maxSizeInBytes) {
    this.maxSizeInBytes = maxSizeInBytes;
    cache.removeEldestEntries();
  }

  /**
   * Clear the cache.
   */
  public synchronized void clear() {
    sizeInBytes = 0;
    for (long tileId : cache.keys().toArray()) {
      evictionMap.put(tileId, cache.get(tileId));
    }
    for (Iterator<LongHashMap.Entry<Texture>> it = evictionMap.entrySetIterator(); it.hasNext(); ) {
      Texture texture = it.next().getValue();
      texture.timeStamp = -1;
    }
    createQueue.clear();
  }
  
  /**
   * Check if cache contains given tile.
   * 
   * @param tileId
   *          id of the tile to check
   * @return true if tile is in the cache, false otherwise
   */
  public synchronized boolean contains(long tileId) {
    if (cache.containsKey(tileId)) {
      return !evictionMap.containsKey(tileId);
    }
    ListIterator<Texture> iterator = createQueue.listIterator();
    while (iterator.hasNext()) {
      Texture texture = iterator.next();
      if (texture.tileId == tileId) {
        return true;
      }
    }
    return false;
  }

  /**
   * Add raster map tile to the cache. Existing tiles can be evicted if cache size limit is exceeded.
   * 
   * @param tileId
   *          id of the tile to add.
   * @param bitmap
   *          bitmap for the tile.
   */
  public synchronized void add(long tileId, Bitmap bitmap) {
    // Try to find if texture for the same tile already exists in createQueue - then replace. Otherwise add the bitmap to createQueue
    Texture texture = new Texture(tileId, bitmap);
    ListIterator<Texture> iterator = createQueue.listIterator();
    while (iterator.hasNext()) {
      Texture otherTexture = iterator.next();
      if (otherTexture.tileId == tileId) {
        otherTexture.bitmap.recycle();
        iterator.set(texture);
        return;
      }
    }
    createQueue.add(texture);
  }

  /**
   * Remove consecutive tile range from the cache.
   * 
   * @param firstTileId
   *          id of the first tile to remove (inclusive).
   * @param lastTileId
   *          id of the last tile to remove (exclusive).
   */
  public synchronized void removeRange(long firstTileId, long lastTileId) {
    long[] tileIds = cache.keys().toArray();
    for (long tileId : tileIds) {
      if (tileId >= firstTileId && tileId < lastTileId) {
        Texture texture = cache.get(tileId);
        if (texture != null) {
          sizeInBytes -= texture.sizeInBytes;
          cache.remove(tileId);
        }
      }
    }
    ListIterator<Texture> iterator = createQueue.listIterator();
    while (iterator.hasNext()) {
      Texture texture = iterator.next();
      if (texture.tileId >= firstTileId && texture.tileId < lastTileId) {
        iterator.remove();
      }
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized int get(long tileId) {
    // Return the texture, if the tile is not cached return 0 (openGl ids start from 1)
    Texture texture = cache.get(tileId);
    if (texture == null) {
      texture = evictionMap.remove(tileId);
      if (texture == null || texture.timeStamp == -1) {
        return 0;
      }
      sizeInBytes += texture.sizeInBytes;
      cache.put(tileId, texture);
    }
    texture.timeStamp = timeStamp;
    return texture.textureId;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized int getWithoutMod(long tileId) {
    // Return the texture, if the tile is not cached return 0 (openGl ids start from 1), don't change the cache order
    Texture texture = cache.getWithoutMod(tileId);
    if (texture == null) {
      texture = evictionMap.get(tileId);
      if (texture == null || texture.timeStamp == -1) {
        return 0;
      }
    }
    texture.timeStamp = timeStamp; // questionable but gives better results in practice
    return texture.textureId;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized int createAndDeleteTextures(GL10 gl) {
    int createCount = 0;

    // Create the textures in the createQueue
    int queueSize = createQueue.size();
    if (queueSize > 0) {
      Iterator<Texture> iterator = createQueue.iterator();
      while (iterator.hasNext()) {
        Texture texture = iterator.next();
        iterator.remove();
        texture.timeStamp = timeStamp;
        texture.textureId = GLUtils.buildMipmaps(gl, texture.bitmap);

        gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
        gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

        texture.bitmap.recycle();
        texture.bitmap = null;
        
        Texture oldTexture = cache.get(texture.tileId);
        if (oldTexture != null) {
          sizeInBytes -= oldTexture.sizeInBytes;
          cache.remove(oldTexture.tileId);
          GLUtils.deleteTexture(gl, oldTexture.textureId);
        }
        
        sizeInBytes += texture.sizeInBytes;
        cache.put(texture.tileId, texture);
        createCount++;
        if (createCount >= NEW_TEXTURES_PER_FRAME) {
          break;
        }
      }
    }
    
    // Check eviction map - if there are textures with old timestamp, delete them
    if (!evictionMap.isEmpty()) {
      Iterator<LongHashMap.Entry<Texture>> iterator = evictionMap.entrySetIterator();
      while (iterator.hasNext()) {
        Texture texture = iterator.next().getValue();
        if (texture.timeStamp != timeStamp && texture.timeStamp + 1 != timeStamp) {
          GLUtils.deleteTexture(gl, texture.textureId);
          iterator.remove();
        }
      }
    }

    timeStamp++;
    return createCount;
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public synchronized void onSurfaceCreated(GL10 gl) {
    cache.clear();
    evictionMap.clear();
    createQueue.clear();
    sizeInBytes = 0;
    timeStamp = 0;
  }

  /**
   * Container class
   */
  private class Texture {
    public final long tileId;
    public volatile int textureId;
    public volatile Bitmap bitmap;
    public final int sizeInBytes;
    public volatile int timeStamp;

    public Texture(long tileId, Bitmap bitmap) {
      this.tileId = tileId;
      this.bitmap = bitmap;
      this.sizeInBytes = (int) (bitmap.getRowBytes() * bitmap.getHeight() * MIPMAP_SIZE_MULTIPLIER);
    }
  }
}
