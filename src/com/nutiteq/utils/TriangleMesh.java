package com.nutiteq.utils;

/**
 * Immutable triangle mesh.
 * @pad.exclude  
 */
public class TriangleMesh {

  /**
   * Builder for the mesh.
   */
  public static class Builder {
    private double[] vertices;
    private float[] texCoords;
    private float[] normals;
    private short[] triangleIndices;
    int vertexIndex;
    int texCoordIndex;
    int normalIndex;
    int triangleIndiceIndex;
    
    public Builder(int vertexCount, int normalCount, int texCoordCount, int triangleIndexCount) {
      vertices = new double[vertexCount * 3];
      normals = new float[normalCount * 3];
      texCoords = new float[texCoordCount * 2];
      triangleIndices = new short[triangleIndexCount * 3];
    }
    
    public void addVertex(double x, double y, double z) {
      vertices[vertexIndex++] = x;
      vertices[vertexIndex++] = y;
      vertices[vertexIndex++] = z;
    }
    
    public void addNormal(double x, double y, double z) {
      normals[normalIndex++] = (float) x;
      normals[normalIndex++] = (float) y;
      normals[normalIndex++] = (float) z;
    }

    public void addTexCoord(float u, float v) {
      texCoords[texCoordIndex++] = u;
      texCoords[texCoordIndex++] = v;
    }

    public void addTriangle(int i0, int i1, int i2) {
      triangleIndices[triangleIndiceIndex++] = (short) i0;
      triangleIndices[triangleIndiceIndex++] = (short) i1;
      triangleIndices[triangleIndiceIndex++] = (short) i2;
    }
    
    public TriangleMesh build() {
      return new TriangleMesh(vertices, texCoords, normals, triangleIndices);
    }
  }
  
  private final double[] vertices;
  private final float[] normals;
  private final float[] texCoords;
  private final short[] triangleIndices;
  
  public TriangleMesh(double[] vertices, float[] texCoords, float[] normals, short[] triangleIndices) {
    this.vertices = vertices;
    this.texCoords = texCoords;
    this.normals = normals;
    this.triangleIndices = triangleIndices;
  }
  
  public double[] getVertices() {
    return vertices;
  }
  
  public float[] getNormals() {
    return normals;
  }
  
  public float[] getTexCoords() {
    return texCoords;
  }
  
  public short[] getTriangleIndices() {
    return triangleIndices;
  }
  
  public static Builder builder(int vertexCount, int normalCount, int texCoordCount, int triangleIndexCount) {
    return new Builder(vertexCount, normalCount, texCoordCount, triangleIndexCount);
  }
  
}
