package com.nutiteq.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Container for building direct byte vertex buffers.
 * @pad.exclude
 */
public class ByteVertexBuffer {
  private static final int INITIAL_CAPACITY = 6 * 1024;

  private int size;
  private int capacity;
  private byte[] buffer;
  private ByteBuffer finalBuffer;
  
  public ByteVertexBuffer() {
    size = 0;
    capacity = 0;
    buffer = null;
    finalBuffer = null;
  }

  public void clear() {
    size = 0;
  }

  public void add(byte r, byte g, byte b) {
    if (size + 3 > capacity) {
      resize(size + 3);
    }
    buffer[size++] = r;
    buffer[size++] = g;
    buffer[size++] = b;
  }

  public void add(byte r, byte g, byte b, byte a) {
    if (size + 4 > capacity) {
      resize(size + 4);
    }
    buffer[size++] = r;
    buffer[size++] = g;
    buffer[size++] = b;
    buffer[size++] = a;
  }

  public byte[] getBuffer() {
    return buffer;
  }
  
  public int size() {
    return size;
  }
  
  public void reserve(int extra) {
    if (size + extra > capacity) {
      resize(size + extra);
    }
  }
  
  public void attach(byte[] buffer, int size) {
    this.buffer = buffer;
    this.size = size;
    this.capacity = buffer.length;
  }

  public ByteBuffer build(int offset, int count) {
    if (offset + count > size) {
      count = size - offset;
    }
    if (finalBuffer == null || finalBuffer.capacity() < count) {
      finalBuffer = ByteBuffer.allocateDirect(count);
      finalBuffer.order(ByteOrder.nativeOrder());
    }
    finalBuffer.position(0);
    if (buffer != null) {
      finalBuffer.put(buffer, offset, count);
      finalBuffer.position(0);
    }
    return finalBuffer;
  }

  protected void resize(int minCapacity) {
    int newCapacity = Math.max(INITIAL_CAPACITY, Math.max(minCapacity, capacity * 3 / 2));
    byte[] newBuffer = new byte[newCapacity];
    if (buffer != null) {
      System.arraycopy(buffer, 0, newBuffer, 0, size);
    }
    buffer = newBuffer;
    capacity = newCapacity;
  }
}
