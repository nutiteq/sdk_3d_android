package com.nutiteq.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.nutiteq.log.Log;

/**
 * IO-utilities.
 * @pad.exclude
 */
@Deprecated
public class IOUtils {
  public static byte[] readFully(final InputStream is) {
    ByteArrayOutputStream out = null;
    final byte[] buffer = new byte[1024];
    byte[] result;
    try {
      out = new ByteArrayOutputStream();
      int read;
      while ((read = is.read(buffer)) != -1) {
        out.write(buffer, 0, read);
      }
      out.flush();
      result = out.toByteArray();
    } catch (final IOException e) {
      Log.error(IOUtils.class.getName() + ": Failed to read the stream. " + e.getMessage());
      result = new byte[0];
    } finally {
      try {
        out.close();
      } catch (IOException e) {
        Log.error(IOUtils.class.getName() + ": Failed to close the stream. " + e.getMessage());
      }
    }
    return result;
  }

  public static int skip(final InputStream is, final int n, final int bufferSize) throws IOException {
    int rd = 0;
    long ch = 0;
    while (rd < n && ch >= 0) {
      final long cn = (n - rd > bufferSize) ? bufferSize : (n - rd);
      ch = is.skip(cn);

      if (ch > 0) {
        rd += ch;
      }
    }
    return rd;
  }

}
