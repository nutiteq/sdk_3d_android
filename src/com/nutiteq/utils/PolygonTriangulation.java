package com.nutiteq.utils;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUtessellator;
import javax.media.opengl.glu.GLUtessellatorCallback;
import javax.microedition.khronos.opengles.GL11;

import com.nutiteq.components.MapPos;
import com.nutiteq.log.Log;

/**
 * Helper class for triangulating polygons.
 * Uses GLU tesselation library that supports duplicate vertices and self-intersecting polygons.
 * 
 * @pad.exclude
 */
public class PolygonTriangulation {
  
  public static ArrayList<MapPos> triangulate(ArrayList<MapPos> mapPoses, List<ArrayList<MapPos>> mapPosesHoles) {
    final ArrayList<MapPos> triangles = new ArrayList<MapPos>(mapPoses.size() * 3 + 1);

    GLUtessellatorCallback tessCallback = new GLUtessellatorCallback() {
      private int type;
      private int offset;

      @Override
      public void begin(int type) {
        this.type = type;
        this.offset = triangles.size();
      }

      @Override
      public void beginData(int type, Object polygonData) {
      }

      @Override
      public void edgeFlag(boolean boundaryEdge) {
      }

      @Override
      public void edgeFlagData(boolean boundaryEdge, Object polygonData) {
      }

      @Override
      public void vertex(Object vertexData) {
        MapPos mapPos = (MapPos) vertexData;
        switch (type) {
        case GL11.GL_TRIANGLES:
          triangles.add(mapPos);
          break;
        case GL11.GL_TRIANGLE_STRIP:
          if (triangles.size() - offset >= 3) {
            int i = triangles.size() - 3;
            if ((triangles.size() - offset) % 2 == 1) {
              triangles.add(triangles.get(i + 2));
              triangles.add(triangles.get(i + 1));
            } else {
              triangles.add(triangles.get(i + 0));
              triangles.add(triangles.get(i + 2));              
            }
          }
          triangles.add(mapPos);
          break;
        case GL11.GL_TRIANGLE_FAN:
          if (triangles.size() - offset >= 3) {
            int i = triangles.size() - 3;
            triangles.add(triangles.get(offset));
            triangles.add(triangles.get(i + 2));
          }
          triangles.add(mapPos);
          break;
        }
      }

      @Override
      public void vertexData(Object vertexData, Object polygonData) {
      }

      @Override
      public void end() {
      }

      @Override
      public void endData(Object polygonData) {
      }

      @Override
      public void combine(double[] coords, Object[] data, float[] weight, Object[] outData) {
        outData[0] = new MapPos(coords[0], coords[1], coords[2]);
      }

      @Override
      public void combineData(double[] coords, Object[] data, float[] weight, Object[] outData, Object polygonData) {
      }

      @Override
      public void error(int errnum) {
        Log.error("PolygonTriangulation: error while triangulating: " + errnum);
      }

      @Override
      public void errorData(int errnum, Object polygonData) {
      }
      
    };
    
    GLUtessellator tobj = GLU.gluNewTess();
    
    GLU.gluTessCallback(tobj, GLU.GLU_TESS_VERTEX, tessCallback);
    GLU.gluTessCallback(tobj, GLU.GLU_TESS_BEGIN, tessCallback);
    GLU.gluTessCallback(tobj, GLU.GLU_TESS_END, tessCallback);
    GLU.gluTessCallback(tobj, GLU.GLU_TESS_COMBINE, tessCallback);
    GLU.gluTessCallback(tobj, GLU.GLU_TESS_ERROR, tessCallback);
    
    GLU.gluTessBeginPolygon(tobj, null);
    GLU.gluTessBeginContour(tobj);
    for (MapPos mapPos : mapPoses) {
      GLU.gluTessVertex(tobj, mapPos.toArray(), 0, mapPos);
    }
    GLU.gluTessEndContour(tobj);
    if (mapPosesHoles != null) {
      for (List<MapPos> mapPosesHole : mapPosesHoles) {
        GLU.gluTessBeginContour(tobj);
        for (MapPos mapPosHole : mapPosesHole) {
          GLU.gluTessVertex(tobj, mapPosHole.toArray(), 0, mapPosHole);
        }
        GLU.gluTessEndContour(tobj);        
      }
    }
    GLU.gluTessEndPolygon(tobj);
    
    GLU.gluDeleteTess(tobj);
    
    return triangles;
  }
}
