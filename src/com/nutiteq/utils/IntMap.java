package com.nutiteq.utils;

import java.util.Collection;
import java.util.Iterator;

/**
 * Map-like data structure with <Code>int</Code> type keys.
 * 
 * @param <V>
 *          type for map values.
 * @pad.exclude
 */
public interface IntMap<V> {

  /**
   * <p>Inner class that acts as a data structure to create a new entry in the
   * table.</p>
   */
  static class Entry<V> {
    int hash;
    V value;
    Entry<V> next;

    /**
     * <p>Create a new entry with the given values.</p>
     *
     * @param hash The code used to hash the object with
     * @param key The key used to enter this in the table
     * @param value The value for this key
     * @param next A reference to the next entry in the table
     */
    protected Entry(int hash, V value, Entry<V> next) {
      this.hash = hash;
      this.value = value;
      this.next = next;
    }

    /**
     * <p>Get key of the entry.</p>
     *
     * @return key.
     */
    public int getKey() {
      return hash;
    }

    /**
     * <p>Get value of the entry.</p>
     *
     * @return the value.
     */
    public V getValue() {
      return value;
    }
  }

  /**
   * <p>Returns the number of keys in this hashtable.</p>
   *
   * @return  the number of keys in this hashtable.
   */
  int size();

  /**
   * <p>Tests if this hashtable maps no keys to values.</p>
   *
   * @return  <code>true</code> if this hashtable maps no keys to values;
   *          <code>false</code> otherwise.
   */
  boolean isEmpty();

  /**
   * <p>Tests if some key maps into the specified value in this hashtable.
   * This operation is more expensive than the <code>containsKey</code>
   * method.</p>
   *
   * <p>Note that this method is identical in functionality to containsValue,
   * (which is part of the Map interface in the collections framework).</p>
   *
   * @param      value   a value to search for.
   * @return     <code>true</code> if and only if some key maps to the
   *             <code>value</code> argument in this hashtable as
   *             determined by the <tt>equals</tt> method;
   *             <code>false</code> otherwise.
   * @throws  NullPointerException  if the value is <code>null</code>.
   * @see        #containsKey(int)
   * @see        #containsValue(Object)
   * @see        java.util.Map
   */
  boolean contains(V value);

  /**
   * <p>Returns <code>true</code> if this HashMap maps one or more keys
   * to this value.</p>
   *
   * <p>Note that this method is identical in functionality to contains
   * (which predates the Map interface).</p>
   *
   * @param value value whose presence in this HashMap is to be tested.
   * @return boolean <code>true</code> if the value is contained
   * @see    java.util.Map
   * @since JDK1.2
   */
  boolean containsValue(V value);

  /**
   * <p>Tests if the specified object is a key in this hashtable.</p>
   *
   * @param  key  possible key.
   * @return <code>true</code> if and only if the specified object is a
   *    key in this hashtable, as determined by the <tt>equals</tt>
   *    method; <code>false</code> otherwise.
   * @see #contains(Object)
   */
  boolean containsKey(int key);

  /**
   * <p>Returns the value to which the specified key is mapped in this map.</p>
   *
   * @param   key   a key in the hashtable.
   * @return  the value to which the key is mapped in this hashtable;
   *          <code>null</code> if the key is not mapped to any value in
   *          this hashtable.
   * @see     #put(int, Object)
   */
  V get(int key);

  /**
   * <p>Maps the specified <code>key</code> to the specified
   * <code>value</code> in this hashtable. The key cannot be
   * <code>null</code>. </p>
   *
   * <p>The value can be retrieved by calling the <code>get</code> method
   * with a key that is equal to the original key.</p>
   *
   * @param key     the hashtable key.
   * @param value   the value.
   * @return the previous value of the specified key in this hashtable,
   *         or <code>null</code> if it did not have one.
   * @throws  NullPointerException  if the key is <code>null</code>.
   * @see     #get(int)
   */
  V put(int key, V value);

  /**
   * <p>Removes the key (and its corresponding value) from this
   * hashtable.</p>
   *
   * <p>This method does nothing if the key is not present in the
   * hashtable.</p>
   *
   * @param   key   the key that needs to be removed.
   * @return  the value to which the key had been mapped in this hashtable,
   *          or <code>null</code> if the key did not have a mapping.
   */
  V remove(int key);

  /**
   * <p>Clears this hashtable so that it contains no keys.</p>
   */
  void clear();

  /**
   * <p>Return collection containing all keys</p>
   *
   * @return  collection containing all keys.
   */
  IntList keys();

  /**
   * <p>Return collection containing all values</p>
   *
   * @return  collection containing all values.
   */
  Collection<V> values();

  /**
   * <p>Return new entry iterator</p>
   *
   * @return  new iterator.
   */
  Iterator<Entry<V>> entrySetIterator();
}
