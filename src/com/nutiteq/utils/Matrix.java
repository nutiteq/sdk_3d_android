/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nutiteq.utils;

/**
 * Matrix math utilities. These methods operate on OpenGL ES format matrices and
 * vectors stored in float arrays.
 * 
 * Matrices are 4 x 4 column-vector matrices stored in column-major order:
 * 
 * <pre>
 *  m[offset +  0] m[offset +  4] m[offset +  8] m[offset + 12]
 *  m[offset +  1] m[offset +  5] m[offset +  9] m[offset + 13]
 *  m[offset +  2] m[offset +  6] m[offset + 10] m[offset + 14]
 *  m[offset +  3] m[offset +  7] m[offset + 11] m[offset + 15]
 * </pre>
 * 
 * Vectors are 4 row x 1 column column-vectors stored in order:
 * 
 * <pre>
 * v[offset + 0]
 * v[offset + 1]
 * v[offset + 2]
 * v[offset + 3]
 * </pre>
 * 
 * @pad.exclude
 */
public class Matrix {

  // Scratchpad memory to skip dynamic allocations. Access should be synchronized.
  private static final double[] sScratch = new double[64];

  /**
   * Multiply two 4x4 matrices together and store the result in a third 4x4
   * matrix. In matrix notation: result = lhs x rhs. Due to the way matrix
   * multiplication works, the result matrix will have the same effect as first
   * multiplying by the rhs matrix, then multiplying by the lhs matrix. This is
   * the opposite of what you might expect.
   * 
   * The same float array may be passed for result, lhs, and/or rhs. However,
   * the result element values are undefined if the result elements overlap
   * either the lhs or rhs elements.
   * 
   * @param result
   *          The float array that holds the result.
   * @param resultOffset
   *          The offset into the result array where the result is stored.
   * @param lhs
   *          The float array that holds the left-hand-side matrix.
   * @param lhsOffset
   *          The offset into the lhs array where the lhs is stored
   * @param rhs
   *          The float array that holds the right-hand-side matrix.
   * @param rhsOffset
   *          The offset into the rhs array where the rhs is stored.
   * 
   * @throws IllegalArgumentException
   *           if result, lhs, or rhs are null, or if resultOffset + 16 >
   *           result.length or lhsOffset + 16 > lhs.length or rhsOffset + 16 >
   *           rhs.length.
   */
  public static void multiplyMM(float[] result, int resultOffset, float[] lhs, int lhsOffset, float[] rhs, int rhsOffset) {
    android.opengl.Matrix.multiplyMM(result, resultOffset, lhs, lhsOffset, rhs, rhsOffset);
  }

  /**
   * Multiply two 4x4 matrices together and store the result in a third 4x4
   * matrix. In matrix notation: result = lhs x rhs. Due to the way matrix
   * multiplication works, the result matrix will have the same effect as first
   * multiplying by the rhs matrix, then multiplying by the lhs matrix. This is
   * the opposite of what you might expect.
   * 
   * The same float array may be passed for result, lhs, and/or rhs. However,
   * the result element values are undefined if the result elements overlap
   * either the lhs or rhs elements.
   * 
   * @param result
   *          The float array that holds the result.
   * @param resultOffset
   *          The offset into the result array where the result is stored.
   * @param lhs
   *          The float array that holds the left-hand-side matrix.
   * @param lhsOffset
   *          The offset into the lhs array where the lhs is stored
   * @param rhs
   *          The float array that holds the right-hand-side matrix.
   * @param rhsOffset
   *          The offset into the rhs array where the rhs is stored.
   * 
   * @throws IllegalArgumentException
   *           if result, lhs, or rhs are null, or if resultOffset + 16 >
   *           result.length or lhsOffset + 16 > lhs.length or rhsOffset + 16 >
   *           rhs.length.
   */
  public static void multiplyMM(double[] result, int resultOffset, double[] lhs, int lhsOffset, double[] rhs, int rhsOffset) {
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        double sum = 0;
        for (int k = 0; k < 4; k++) {
          sum += lhs[i + k * 4 + lhsOffset] * rhs[k + j * 4 + rhsOffset];
        }
        result[i + j * 4 + resultOffset] = sum;
      }
    }
  }
  
  /**
   * Multiply list of 4x4 matrices together.
   * 
   * @param result
   *          The array containing result.
   * @param resultOffset
   *          offset of the result in result array.
   * @param inputs
   *          list of input matrices to concatenate.
   */ 
  public static void concatenateM(double[] result, int resultOffset, double[]... inputs) {
    if (inputs.length == 0) {
      setIdentityM(result, resultOffset);
      return;
    }
    double[] scratch = sScratch;
    synchronized (scratch) {
      copyM(scratch, 0, inputs[0], 0);
      int offset = 0; 
      for (int i = 1; i < inputs.length; i++) {
        offset = 16 - offset;
        Matrix.multiplyMM(scratch, offset, scratch, 16 - offset, inputs[i], 0);
      }
      copyM(result, resultOffset, scratch, offset);
    }
  }

  /**
   * Multiply a 4 element vector by a 4x4 matrix and store the result in a 4
   * element column vector. In matrix notation: result = lhs x rhs
   * 
   * The same float array may be passed for resultVec, lhsMat, and/or rhsVec.
   * However, the resultVec element values are undefined if the resultVec
   * elements overlap either the lhsMat or rhsVec elements.
   * 
   * @param resultVec
   *          The float array that holds the result vector.
   * @param resultVecOffset
   *          The offset into the result array where the result vector is
   *          stored.
   * @param lhsMat
   *          The float array that holds the left-hand-side matrix.
   * @param lhsMatOffset
   *          The offset into the lhs array where the lhs is stored
   * @param rhsVec
   *          The float array that holds the right-hand-side vector.
   * @param rhsVecOffset
   *          The offset into the rhs vector where the rhs vector is stored.
   * 
   * @throws IllegalArgumentException
   *           if resultVec, lhsMat, or rhsVec are null, or if resultVecOffset +
   *           4 > resultVec.length or lhsMatOffset + 16 > lhsMat.length or
   *           rhsVecOffset + 4 > rhsVec.length.
   */
  public static void multiplyMV(float[] resultVec, int resultVecOffset, float[] lhsMat, int lhsMatOffset, float[] rhsVec, int rhsVecOffset) {
    android.opengl.Matrix.multiplyMV(resultVec, resultVecOffset, lhsMat, lhsMatOffset, rhsVec, rhsVecOffset);
  }

  /**
   * Multiply a 4 element vector by a 4x4 matrix and store the result in a 4
   * element column vector. In matrix notation: result = lhs x rhs
   * 
   * The same float array may be passed for resultVec, lhsMat, and/or rhsVec.
   * However, the resultVec element values are undefined if the resultVec
   * elements overlap either the lhsMat or rhsVec elements.
   * 
   * @param resultVec
   *          The float array that holds the result vector.
   * @param resultVecOffset
   *          The offset into the result array where the result vector is
   *          stored.
   * @param lhsMat
   *          The float array that holds the left-hand-side matrix.
   * @param lhsMatOffset
   *          The offset into the lhs array where the lhs is stored
   * @param rhsVec
   *          The float array that holds the right-hand-side vector.
   * @param rhsVecOffset
   *          The offset into the rhs vector where the rhs vector is stored.
   * 
   * @throws IllegalArgumentException
   *           if resultVec, lhsMat, or rhsVec are null, or if resultVecOffset +
   *           4 > resultVec.length or lhsMatOffset + 16 > lhsMat.length or
   *           rhsVecOffset + 4 > rhsVec.length.
   */
  public static void multiplyMV(double[] resultVec, int resultVecOffset, double[] lhsMat, int lhsMatOffset, double[] rhsVec, int rhsVecOffset) {
    for (int i = 0; i < 4; i++) {
      double sum = 0;
      for (int j = 0; j < 4; j++) {
        sum += lhsMat[i + j * 4 + lhsMatOffset] * rhsVec[j + rhsVecOffset];
      }
      resultVec[i + resultVecOffset] = sum;
    }
  }

  /**
   * Transposes a 4 x 4 matrix.
   * 
   * @param mTrans
   *          the array that holds the output inverted matrix
   * @param m
   *          the input array
   * @param mOffset
   *          an offset into m where the matrix is stored.
   */
  public static void transposeM(float[] mTrans, float[] m, int mOffset) {
    for (int i = 0; i < 4; i++) {
      int mBase = i * 4 + mOffset;
      mTrans[i] = m[mBase];
      mTrans[i + 4] = m[mBase + 1];
      mTrans[i + 8] = m[mBase + 2];
      mTrans[i + 12] = m[mBase + 3];
    }
  }

  /**
   * Transposes a 4 x 4 matrix.
   * 
   * @param mTrans
   *          the array that holds the output inverted matrix
   * @param m
   *          the input array
   * @param mOffset
   *          an offset into m where the matrix is stored.
   */
  public static void transposeM(double[] mTrans, double[] m, int mOffset) {
    for (int i = 0; i < 4; i++) {
      int mBase = i * 4 + mOffset;
      mTrans[i] = m[mBase];
      mTrans[i + 4] = m[mBase + 1];
      mTrans[i + 8] = m[mBase + 2];
      mTrans[i + 12] = m[mBase + 3];
    }
  }

  /**
   * Inverts a 4 x 4 matrix.
   * 
   * @param mInv
   *          the array that holds the output inverted matrix
   * @param mInvOffset
   *          an offset into mInv where the inverted matrix is stored.
   * @param m
   *          the input array
   * @param mOffset
   *          an offset into m where the matrix is stored.
   * @return true if the matrix could be inverted, false if it could not.
   */
  public static boolean invertM(float[] mInv, int mInvOffset, float[] m, int mOffset) {
    // Invert a 4 x 4 matrix using Cramer's Rule

    // array of transpose source matrix
    float[] src = new float[16];

    // transpose matrix
    transposeM(src, m, mOffset);

    // temp array for pairs
    float[] tmp = new float[12];

    // calculate pairs for first 8 elements (cofactors)
    tmp[0] = src[10] * src[15];
    tmp[1] = src[11] * src[14];
    tmp[2] = src[9] * src[15];
    tmp[3] = src[11] * src[13];
    tmp[4] = src[9] * src[14];
    tmp[5] = src[10] * src[13];
    tmp[6] = src[8] * src[15];
    tmp[7] = src[11] * src[12];
    tmp[8] = src[8] * src[14];
    tmp[9] = src[10] * src[12];
    tmp[10] = src[8] * src[13];
    tmp[11] = src[9] * src[12];

    // Holds the destination matrix while we're building it up.
    float[] dst = new float[16];

    // calculate first 8 elements (cofactors)
    dst[0] = tmp[0] * src[5] + tmp[3] * src[6] + tmp[4] * src[7];
    dst[0] -= tmp[1] * src[5] + tmp[2] * src[6] + tmp[5] * src[7];
    dst[1] = tmp[1] * src[4] + tmp[6] * src[6] + tmp[9] * src[7];
    dst[1] -= tmp[0] * src[4] + tmp[7] * src[6] + tmp[8] * src[7];
    dst[2] = tmp[2] * src[4] + tmp[7] * src[5] + tmp[10] * src[7];
    dst[2] -= tmp[3] * src[4] + tmp[6] * src[5] + tmp[11] * src[7];
    dst[3] = tmp[5] * src[4] + tmp[8] * src[5] + tmp[11] * src[6];
    dst[3] -= tmp[4] * src[4] + tmp[9] * src[5] + tmp[10] * src[6];
    dst[4] = tmp[1] * src[1] + tmp[2] * src[2] + tmp[5] * src[3];
    dst[4] -= tmp[0] * src[1] + tmp[3] * src[2] + tmp[4] * src[3];
    dst[5] = tmp[0] * src[0] + tmp[7] * src[2] + tmp[8] * src[3];
    dst[5] -= tmp[1] * src[0] + tmp[6] * src[2] + tmp[9] * src[3];
    dst[6] = tmp[3] * src[0] + tmp[6] * src[1] + tmp[11] * src[3];
    dst[6] -= tmp[2] * src[0] + tmp[7] * src[1] + tmp[10] * src[3];
    dst[7] = tmp[4] * src[0] + tmp[9] * src[1] + tmp[10] * src[2];
    dst[7] -= tmp[5] * src[0] + tmp[8] * src[1] + tmp[11] * src[2];

    // calculate pairs for second 8 elements (cofactors)
    tmp[0] = src[2] * src[7];
    tmp[1] = src[3] * src[6];
    tmp[2] = src[1] * src[7];
    tmp[3] = src[3] * src[5];
    tmp[4] = src[1] * src[6];
    tmp[5] = src[2] * src[5];
    tmp[6] = src[0] * src[7];
    tmp[7] = src[3] * src[4];
    tmp[8] = src[0] * src[6];
    tmp[9] = src[2] * src[4];
    tmp[10] = src[0] * src[5];
    tmp[11] = src[1] * src[4];

    // calculate second 8 elements (cofactors)
    dst[8] = tmp[0] * src[13] + tmp[3] * src[14] + tmp[4] * src[15];
    dst[8] -= tmp[1] * src[13] + tmp[2] * src[14] + tmp[5] * src[15];
    dst[9] = tmp[1] * src[12] + tmp[6] * src[14] + tmp[9] * src[15];
    dst[9] -= tmp[0] * src[12] + tmp[7] * src[14] + tmp[8] * src[15];
    dst[10] = tmp[2] * src[12] + tmp[7] * src[13] + tmp[10] * src[15];
    dst[10] -= tmp[3] * src[12] + tmp[6] * src[13] + tmp[11] * src[15];
    dst[11] = tmp[5] * src[12] + tmp[8] * src[13] + tmp[11] * src[14];
    dst[11] -= tmp[4] * src[12] + tmp[9] * src[13] + tmp[10] * src[14];
    dst[12] = tmp[2] * src[10] + tmp[5] * src[11] + tmp[1] * src[9];
    dst[12] -= tmp[4] * src[11] + tmp[0] * src[9] + tmp[3] * src[10];
    dst[13] = tmp[8] * src[11] + tmp[0] * src[8] + tmp[7] * src[10];
    dst[13] -= tmp[6] * src[10] + tmp[9] * src[11] + tmp[1] * src[8];
    dst[14] = tmp[6] * src[9] + tmp[11] * src[11] + tmp[3] * src[8];
    dst[14] -= tmp[10] * src[11] + tmp[2] * src[8] + tmp[7] * src[9];
    dst[15] = tmp[10] * src[10] + tmp[4] * src[8] + tmp[9] * src[9];
    dst[15] -= tmp[8] * src[9] + tmp[11] * src[10] + tmp[5] * src[8];

    // calculate determinant
    float det = src[0] * dst[0] + src[1] * dst[1] + src[2] * dst[2] + src[3] * dst[3];

    // calculate matrix inverse
    det = 1 / det;
    for (int j = 0; j < 16; j++)
      mInv[j + mInvOffset] = dst[j] * det;

    return det != 0.0f;
  }

  /**
   * Inverts a 4 x 4 matrix.
   * 
   * @param mInv
   *          the array that holds the output inverted matrix
   * @param mInvOffset
   *          an offset into mInv where the inverted matrix is stored.
   * @param m
   *          the input array
   * @param mOffset
   *          an offset into m where the matrix is stored.
   * @return true if the matrix could be inverted, false if it could not.
   */
  public static boolean invertM(double[] mInv, int mInvOffset, double[] m, int mOffset) {
    // Invert a 4 x 4 matrix using Cramer's Rule

    // array of transpose source matrix
    double[] src = new double[16];

    // transpose matrix
    transposeM(src, m, mOffset);

    // temp array for pairs
    double[] tmp = new double[12];

    // calculate pairs for first 8 elements (cofactors)
    tmp[0] = src[10] * src[15];
    tmp[1] = src[11] * src[14];
    tmp[2] = src[9] * src[15];
    tmp[3] = src[11] * src[13];
    tmp[4] = src[9] * src[14];
    tmp[5] = src[10] * src[13];
    tmp[6] = src[8] * src[15];
    tmp[7] = src[11] * src[12];
    tmp[8] = src[8] * src[14];
    tmp[9] = src[10] * src[12];
    tmp[10] = src[8] * src[13];
    tmp[11] = src[9] * src[12];

    // Holds the destination matrix while we're building it up.
    double[] dst = new double[16];

    // calculate first 8 elements (cofactors)
    dst[0] = tmp[0] * src[5] + tmp[3] * src[6] + tmp[4] * src[7];
    dst[0] -= tmp[1] * src[5] + tmp[2] * src[6] + tmp[5] * src[7];
    dst[1] = tmp[1] * src[4] + tmp[6] * src[6] + tmp[9] * src[7];
    dst[1] -= tmp[0] * src[4] + tmp[7] * src[6] + tmp[8] * src[7];
    dst[2] = tmp[2] * src[4] + tmp[7] * src[5] + tmp[10] * src[7];
    dst[2] -= tmp[3] * src[4] + tmp[6] * src[5] + tmp[11] * src[7];
    dst[3] = tmp[5] * src[4] + tmp[8] * src[5] + tmp[11] * src[6];
    dst[3] -= tmp[4] * src[4] + tmp[9] * src[5] + tmp[10] * src[6];
    dst[4] = tmp[1] * src[1] + tmp[2] * src[2] + tmp[5] * src[3];
    dst[4] -= tmp[0] * src[1] + tmp[3] * src[2] + tmp[4] * src[3];
    dst[5] = tmp[0] * src[0] + tmp[7] * src[2] + tmp[8] * src[3];
    dst[5] -= tmp[1] * src[0] + tmp[6] * src[2] + tmp[9] * src[3];
    dst[6] = tmp[3] * src[0] + tmp[6] * src[1] + tmp[11] * src[3];
    dst[6] -= tmp[2] * src[0] + tmp[7] * src[1] + tmp[10] * src[3];
    dst[7] = tmp[4] * src[0] + tmp[9] * src[1] + tmp[10] * src[2];
    dst[7] -= tmp[5] * src[0] + tmp[8] * src[1] + tmp[11] * src[2];

    // calculate pairs for second 8 elements (cofactors)
    tmp[0] = src[2] * src[7];
    tmp[1] = src[3] * src[6];
    tmp[2] = src[1] * src[7];
    tmp[3] = src[3] * src[5];
    tmp[4] = src[1] * src[6];
    tmp[5] = src[2] * src[5];
    tmp[6] = src[0] * src[7];
    tmp[7] = src[3] * src[4];
    tmp[8] = src[0] * src[6];
    tmp[9] = src[2] * src[4];
    tmp[10] = src[0] * src[5];
    tmp[11] = src[1] * src[4];

    // calculate second 8 elements (cofactors)
    dst[8] = tmp[0] * src[13] + tmp[3] * src[14] + tmp[4] * src[15];
    dst[8] -= tmp[1] * src[13] + tmp[2] * src[14] + tmp[5] * src[15];
    dst[9] = tmp[1] * src[12] + tmp[6] * src[14] + tmp[9] * src[15];
    dst[9] -= tmp[0] * src[12] + tmp[7] * src[14] + tmp[8] * src[15];
    dst[10] = tmp[2] * src[12] + tmp[7] * src[13] + tmp[10] * src[15];
    dst[10] -= tmp[3] * src[12] + tmp[6] * src[13] + tmp[11] * src[15];
    dst[11] = tmp[5] * src[12] + tmp[8] * src[13] + tmp[11] * src[14];
    dst[11] -= tmp[4] * src[12] + tmp[9] * src[13] + tmp[10] * src[14];
    dst[12] = tmp[2] * src[10] + tmp[5] * src[11] + tmp[1] * src[9];
    dst[12] -= tmp[4] * src[11] + tmp[0] * src[9] + tmp[3] * src[10];
    dst[13] = tmp[8] * src[11] + tmp[0] * src[8] + tmp[7] * src[10];
    dst[13] -= tmp[6] * src[10] + tmp[9] * src[11] + tmp[1] * src[8];
    dst[14] = tmp[6] * src[9] + tmp[11] * src[11] + tmp[3] * src[8];
    dst[14] -= tmp[10] * src[11] + tmp[2] * src[8] + tmp[7] * src[9];
    dst[15] = tmp[10] * src[10] + tmp[4] * src[8] + tmp[9] * src[9];
    dst[15] -= tmp[8] * src[9] + tmp[11] * src[10] + tmp[5] * src[8];

    // calculate determinant
    double det = src[0] * dst[0] + src[1] * dst[1] + src[2] * dst[2] + src[3] * dst[3];

    // calculate matrix inverse
    det = 1 / det;
    for (int j = 0; j < 16; j++)
      mInv[j + mInvOffset] = dst[j] * det;

    return det != 0.0;
  }

  /**
   * Define a projection matrix in terms of six clip planes, the matrix must be
   * unaltered after the initial call to frustumM. This method is specifically
   * optimized for use in the MapRenderer and should not be used anywhere else.
   * 
   * @param m
   *          the float array that holds the perspective matrix
   * @param left
   * @param right
   * @param bottom
   * @param top
   * @param near
   * @param far
   */

  public static void frustumReinitializeM(float[] m, double left, double right, double bottom, double top, double near, double far) {
    final double r_width = 1.0 / (right - left);
    final double r_height = 1.0 / (top - bottom);
    final double r_depth = 1.0 / (near - far);
    m[0] = (float) (2.0 * (near * r_width));
    m[5] = (float) (2.0 * (near * r_height));
    m[8] = (float) ((right + left) * r_width);
    m[9] = (float) ((top + bottom) * r_height);
    m[10] = (float) ((far + near) * r_depth);
    m[14] = (float) (2.0 * (far * near * r_depth));
  }

  /**
   * Computes an orthographic projection matrix.
   * 
   * @param m
   *          returns the result
   * @param left
   * @param right
   * @param bottom
   * @param top
   * @param near
   * @param far
   */
  public static void orthoM(float[] m, double left, double right, double bottom, double top, double near, double far) {
    final double r_width = 1.0 / (right - left);
    final double r_height = 1.0 / (top - bottom);
    final double r_depth = 1.0 / (far - near);
    final double x = 2.0 * (r_width);
    final double y = 2.0 * (r_height);
    final double z = -2.0 * (r_depth);
    final double tx = -(right + left) * r_width;
    final double ty = -(top + bottom) * r_height;
    final double tz = -(far + near) * r_depth;
    m[0] = (float) x;
    m[5] = (float) y;
    m[10] = (float) z;
    m[12] = (float) tx;
    m[13] = (float) ty;
    m[14] = (float) tz;
    m[15] = 1.0f;
    m[1] = 0.0f;
    m[2] = 0.0f;
    m[3] = 0.0f;
    m[4] = 0.0f;
    m[6] = 0.0f;
    m[7] = 0.0f;
    m[8] = 0.0f;
    m[9] = 0.0f;
    m[11] = 0.0f;
  }
  
  /**
   * Create identity matrix.
   * 
   * @param rm
   *          returns the result
   */
  public static void setIdentityM(double[] rm, int resultOffset) {
    for (int i = 0; i < 16; i++) {
      rm[i + resultOffset] = (i % 4 == i / 4 ? 1 : 0);
    }
  }

  /**
   * Define a viewing transformation in terms of an eye point, a center of view,
   * and an up vector.
   * 
   * @param rm
   *          returns the result
   * @param eyeX
   *          eye point X
   * @param eyeY
   *          eye point Y
   * @param eyeZ
   *          eye point Z
   * @param centerX
   *          center of view X
   * @param centerY
   *          center of view Y
   * @param centerZ
   *          center of view Z
   * @param upX
   *          up vector X
   * @param upY
   *          up vector Y
   * @param upZ
   *          up vector Z
   */
  public static void setLookAtM(float[] rm,
      double eyeX, double eyeY, double eyeZ,
      double centerX, double centerY, double centerZ,
      double upX, double upY, double upZ) {

    // See the OpenGL GLUT documentation for gluLookAt for a description
    // of the algorithm. We implement it in a straightforward way:

    double fx = centerX - eyeX;
    double fy = centerY - eyeY;
    double fz = centerZ - eyeZ;

    // Normalize f
    double rlf = 1.0 / Math.sqrt(fx * fx + fy * fy + fz * fz);
    fx *= rlf;
    fy *= rlf;
    fz *= rlf;

    // compute s = f x up (x means "cross product")
    double sx = fy * upZ - fz * upY;
    double sy = fz * upX - fx * upZ;
    double sz = fx * upY - fy * upX;

    // and normalize s
    double rls = 1.0 / Math.sqrt(sx * sx + sy * sy + sz * sz);
    sx *= rls;
    sy *= rls;
    sz *= rls;

    // compute u = s x f
    double ux = sy * fz - sz * fy;
    double uy = sz * fx - sx * fz;
    double uz = sx * fy - sy * fx;

    rm[0] = (float) sx;
    rm[1] = (float) ux;
    rm[2] = (float) -fx;
    rm[3] = 0.0f;

    rm[4] = (float) sy;
    rm[5] = (float) uy;
    rm[6] = (float) -fy;
    rm[7] = 0.0f;

    rm[8] = (float) sz;
    rm[9] = (float) uz;
    rm[10] = (float) -fz;
    rm[11] = 0.0f;

    rm[12] = (float) (-rm[0] * eyeX - rm[4] * eyeY - rm[8] * eyeZ);
    rm[13] = (float) (-rm[1] * eyeX - rm[5] * eyeY - rm[9] * eyeZ);
    rm[14] = (float) (-rm[2] * eyeX - rm[6] * eyeY - rm[10] * eyeZ);
    rm[15] = 1.0f;
  }

  /**
   * Define a viewing transformation in terms of an eye point, a center of view,
   * and an up vector.
   * 
   * @param rm
   *          returns the result
   * @param eyeX
   *          eye point X
   * @param eyeY
   *          eye point Y
   * @param eyeZ
   *          eye point Z
   * @param centerX
   *          center of view X
   * @param centerY
   *          center of view Y
   * @param centerZ
   *          center of view Z
   * @param upX
   *          up vector X
   * @param upY
   *          up vector Y
   * @param upZ
   *          up vector Z
   */
  public static void setLookAtM(double[] rm,
      double eyeX, double eyeY, double eyeZ,
      double centerX, double centerY, double centerZ,
      double upX, double upY, double upZ) {

    // See the OpenGL GLUT documentation for gluLookAt for a description
    // of the algorithm. We implement it in a straightforward way:

    double fx = centerX - eyeX;
    double fy = centerY - eyeY;
    double fz = centerZ - eyeZ;

    // Normalize f
    double rlf = 1.0 / Math.sqrt(fx * fx + fy * fy + fz * fz);
    fx *= rlf;
    fy *= rlf;
    fz *= rlf;

    // compute s = f x up (x means "cross product")
    double sx = fy * upZ - fz * upY;
    double sy = fz * upX - fx * upZ;
    double sz = fx * upY - fy * upX;

    // and normalize s
    double rls = 1.0 / Math.sqrt(sx * sx + sy * sy + sz * sz);
    sx *= rls;
    sy *= rls;
    sz *= rls;

    // compute u = s x f
    double ux = sy * fz - sz * fy;
    double uy = sz * fx - sx * fz;
    double uz = sx * fy - sy * fx;

    rm[0] = sx;
    rm[1] = ux;
    rm[2] = -fx;
    rm[3] = 0.0;

    rm[4] = sy;
    rm[5] = uy;
    rm[6] = -fy;
    rm[7] = 0.0;

    rm[8] = sz;
    rm[9] = uz;
    rm[10] = -fz;
    rm[11] = 0.0;

    rm[12] = (-rm[0] * eyeX - rm[4] * eyeY - rm[8] * eyeZ);
    rm[13] = (-rm[1] * eyeX - rm[5] * eyeY - rm[9] * eyeZ);
    rm[14] = (-rm[2] * eyeX - rm[6] * eyeY - rm[10] * eyeZ);
    rm[15] = 1.0;
  }

  /**
   * Define a scaling transformation.
   * 
   * @param rm
   *          returns the result
   * @param sx
   *          X coordinate scaling
   * @param sy
   *          Y coordinate scaling
   * @param sz
   *          Z coordinate scaling
   */
  public static void setScaleM(double[] rm, double sx, double sy, double sz) {
    rm[0] = sx;
    rm[1] = 0;
    rm[2] = 0;
    rm[3] = 0;

    rm[4] = 0;
    rm[5] = sy;
    rm[6] = 0;
    rm[7] = 0;

    rm[8] = 0;
    rm[9] = 0;
    rm[10] = sz;
    rm[11] = 0;

    rm[12] = 0;
    rm[13] = 0;
    rm[14] = 0;
    rm[15] = 1;
  }
  
  /**
   * Define a translation transformation.
   * 
   * @param rm
   *          returns the result
   * @param x
   *          X coordinate translation
   * @param y
   *          Y coordinate translation
   * @param z
   *          Z coordinate translation
   */
  public static void setTranslateM(double[] rm, double x, double y, double z) {
    rm[0] = 1;
    rm[1] = 0;
    rm[2] = 0;
    rm[3] = 0;

    rm[4] = 0;
    rm[5] = 1;
    rm[6] = 0;
    rm[7] = 0;

    rm[8] = 0;
    rm[9] = 0;
    rm[10] = 1;
    rm[11] = 0;

    rm[12] = x;
    rm[13] = y;
    rm[14] = z;
    rm[15] = 1;
  }
  
  /**
   * Define a rotation transformation given rotation axis and rotation angle.
   * 
   * @param rm
   *          returns the result
   * @param x
   *          X coordinate of the rotation axis vector
   * @param y
   *          Y coordinate of the rotation axis vector
   * @param z
   *          Z coordinate of the rotation axis vector
   * @param angle
   *          rotation angle in degrees
   */
  public static void setRotationM(double[] rm, double x, double y, double z, double angle) {
    double sin = Math.sin(angle * Const.DEG_TO_RAD / 2);
    double cos = Math.cos(angle * Const.DEG_TO_RAD / 2);
    double q1 = x * sin;
    double q2 = y * sin;
    double q3 = z * sin;
    double q0 = cos;

    double sq_q1 = 2 * q1 * q1;
    double sq_q2 = 2 * q2 * q2;
    double sq_q3 = 2 * q3 * q3;
    double q1_q2 = 2 * q1 * q2;
    double q3_q0 = 2 * q3 * q0;
    double q1_q3 = 2 * q1 * q3;
    double q2_q0 = 2 * q2 * q0;
    double q2_q3 = 2 * q2 * q3;
    double q1_q0 = 2 * q1 * q0;

    rm[0] = 1 - sq_q2 - sq_q3;
    rm[1] = q1_q2 - q3_q0;
    rm[2] = q1_q3 + q2_q0;
    rm[3] = 0.0f;

    rm[4] = q1_q2 + q3_q0;
    rm[5] = 1 - sq_q1 - sq_q3;
    rm[6] = q2_q3 - q1_q0;
    rm[7] = 0.0f;

    rm[8] = q1_q3 - q2_q0;
    rm[9] = q2_q3 + q1_q0;
    rm[10] = 1 - sq_q1 - sq_q2;
    rm[11] = 0.0f;

    rm[12] = rm[13] = rm[14] = 0.0f;
    rm[15] = 1.0f;
  }  

  /**
   * Map object coordinates into window coordinates. gluProject transforms the
   * specified object coordinates into window coordinates using model, proj,
   * and view. The result is stored in win.
   * <p>
   * Note that you can use the OES_matrix_get extension, if present, to get
   * the current modelView and projection matrices.
   *
   * @param objX object coordinates X
   * @param objY object coordinates Y
   * @param objZ object coordinates Z
   * @param model the current modelview matrix
   * @param modelOffset the offset into the model array where the modelview
   *        matrix data starts.
   * @param project the current projection matrix
   * @param projectOffset the offset into the project array where the project
   *        matrix data starts.
   * @param view the current view, {x, y, width, height}
   * @param viewOffset the offset into the view array where the view vector
   *        data starts.
   * @param win the output vector {winX, winY, winZ}, that returns the
   *        computed window coordinates.
   * @param winOffset the offset into the win array where the win vector data
   *        starts.
   * @return A return value of true indicates success, a return value of
   *         false indicates failure.
   */
  public static boolean project(double objX, double objY, double objZ,
      double[] model, int modelOffset, float[] project, int projectOffset,
      int[] view, int viewOffset, double[] win, int winOffset) {
    double[] scratch = sScratch;
    synchronized(scratch) {
      final int M_OFFSET = 0; // 0..15
      final int V_OFFSET = 16; // 16..19
      final int V2_OFFSET = 20; // 20..23
      final int M2_OFFSET = 32; // 32..47
      Matrix.floatToDoubleM(scratch, M2_OFFSET, project, projectOffset);
      Matrix.multiplyMM(scratch, M_OFFSET, scratch, M2_OFFSET, model, modelOffset);

      scratch[V_OFFSET + 0] = objX;
      scratch[V_OFFSET + 1] = objY;
      scratch[V_OFFSET + 2] = objZ;
      scratch[V_OFFSET + 3] = 1.0;

      Matrix.multiplyMV(scratch, V2_OFFSET, scratch, M_OFFSET, scratch, V_OFFSET);

      double w = scratch[V2_OFFSET + 3];
      if (w == 0.0) {
        return false;
      }

      double rw = 1.0 / w;
      win[winOffset + 0] =  view[viewOffset + 1] + view[viewOffset + 2] * (scratch[V2_OFFSET + 0] * rw + 1.0f) * 0.5;
      win[winOffset + 1] =  view[viewOffset + 1] + view[viewOffset + 3] * (scratch[V2_OFFSET + 1] * rw + 1.0f) * 0.5;
      win[winOffset + 2] = (scratch[V2_OFFSET + 2] * rw + 1.0f) * 0.5;
    }

    return true;
  }

  /**
   * Port of gluUnProject. Unprojects a 2D screen coordinate into the model-view
   * coordinates.
   * 
   * @param winX The window point for the x value.
   * @param winY The window point for the y value.
   * @param winZ The window point for the z value. This should range between 0 and 1. 0 meaning the near clipping plane and 1 for the far.
   * @param modelViewMatrix The model-view matrix.
   * @param projectMatrix The projection matrix.
   * @param view the viewport coordinate array.
   * @param objPos the model point result.
   * @return Whether or not the unprojection was successful.
   */
  public static boolean unProject(double winX, double winY, double winZ,
      double[] modelViewMatrix, int modelOffset, float[] projectionMatrix, int projectionOffset,
      int[] viewPort, double[] objPos, int objPosOffset) {
    double[] scratch = sScratch;
    synchronized(scratch) {
      final int M_OFFSET = 0; // 0..15
      final int V_OFFSET = 16; // 16..19
      final int V2_OFFSET = 20; // 20..23
      final int M2_OFFSET = 32; // 32..47
      Matrix.floatToDoubleM(scratch, M2_OFFSET, projectionMatrix, projectionOffset);
      Matrix.multiplyMM(scratch, M_OFFSET, scratch, M2_OFFSET, modelViewMatrix, modelOffset);
      Matrix.invertM(scratch, M2_OFFSET, scratch, M_OFFSET);

      // Transformation of normalized coordinates (-1 to 1).
      scratch[V_OFFSET + 0] = (winX - viewPort[0]) / viewPort[2] * 2.0 - 1.0; 
      scratch[V_OFFSET + 1] = (winY - viewPort[1]) / viewPort[3] * 2.0 - 1.0;
      scratch[V_OFFSET + 2] = 2.0 * winZ - 1.0;
      scratch[V_OFFSET + 3] = 1.0;

      Matrix.multiplyMV(scratch, V2_OFFSET, scratch, M2_OFFSET, scratch, V_OFFSET);

      double w = scratch[V2_OFFSET + 3]; 
      if (w == 0.0) {
        return false;
      }
      
      double rw = 1.0 / w;
      objPos[objPosOffset + 0] = scratch[V2_OFFSET + 0] * rw;
      objPos[objPosOffset + 1] = scratch[V2_OFFSET + 1] * rw;
      objPos[objPosOffset + 2] = scratch[V2_OFFSET + 2] * rw;
    }

    return true;
  }  

  /**
   * Copy matrix.
   * 
   * @param result result matrix array
   * @param resultOffset offset in result array
   * @param input input matrix array
   * @param inputOffset offset in input array
   */
  public static void copyM(double[] result, int resultOffset, double[] input, int inputOffset) {
    for (int i = 0; i < 16; i++) {
      result[i + resultOffset] = input[i + inputOffset];
    }
  }

  /**
   * Copy matrix.
   * 
   * @param result result matrix array
   * @param resultOffset offset in result array
   * @param input input matrix array
   * @param inputOffset offset in input array
   */
  public static void copyM(float[] result, int resultOffset, float[] input, int inputOffset) {
    for (int i = 0; i < 16; i++) {
      result[i + resultOffset] = input[i + inputOffset];
    }
  }

  /**
   * Convert float matrix to double matrix.
   * 
   * @param result result matrix array
   * @param resultOffset offset in result array
   * @param input input matrix array
   * @param inputOffset offset in input array
   */
  public static void floatToDoubleM(double[] result, int resultOffset, float[] input, int inputOffset) {
    for (int i = 0; i < 16; i++) {
      result[i + resultOffset] = input[i + inputOffset];
    }
  }

  /**
   * Convert double matrix to float matrix.
   * 
   * @param result result matrix array
   * @param resultOffset offset in result array
   * @param input input matrix array
   * @param inputOffset offset in input array
   */
  public static void doubleToFloatM(float[] result, int resultOffset, double[] input, int inputOffset) {
    for (int i = 0; i < 16; i++) {
      result[i + resultOffset] = (float) input[i + inputOffset];
    }
  }

}
