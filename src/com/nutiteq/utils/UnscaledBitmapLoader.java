package com.nutiteq.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Build;

/**
 * Utilities for loading bitmaps without scaling. This results in better quality.
 */
public abstract class UnscaledBitmapLoader {
  private static final UnscaledBitmapLoader LOADER = Integer.parseInt(Build.VERSION.SDK) < 4 ? new Old() : new New();

  private static class Old extends UnscaledBitmapLoader {
    @Override
    public Bitmap load(Resources resources, int resId, Options options) {
      return BitmapFactory.decodeResource(resources, resId, options);
    }
  }

  private static class New extends UnscaledBitmapLoader {
    @Override
    public Bitmap load(Resources resources, int resId, Options options) {
      if (options == null)
        options = new BitmapFactory.Options();
      options.inScaled = false;
      return BitmapFactory.decodeResource(resources, resId, options);
    }
  }

  protected abstract Bitmap load(Resources resources, int resId, BitmapFactory.Options options);

  /**
   * Decode bitmap from resources.
   * 
   * @param resources
   *          resources instance to use
   * @param resId
   *          resource id to use
   * @return decoded and unscaled bitmap. Null if bitmap was not found or could not be decoded. 
   */
  public static Bitmap decodeResource(Resources resources, int resId) {
    return LOADER.load(resources, resId, null);
  }

  /**
   * Decode bitmap from resources with given options.
   * 
   * @param resources
   *          resources instance to use
   * @param resId
   *          resource id to use
   * @param options
   *          options for bitmap decoding.
   * @return decoded and unscaled bitmap. Null if bitmap was not found or could not be decoded. 
   */
  public static Bitmap decodeResource(Resources resources, int resId, BitmapFactory.Options options) {
    return LOADER.load(resources, resId, options);
  }

}
