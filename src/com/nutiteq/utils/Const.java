package com.nutiteq.utils;

import android.os.Build;

import com.nutiteq.components.Vector3D;

/**
 * Various constrants used by SDK.
 * @pad.exclude
 */
public class Const {
  // Earth radius in meters
  public static final float EARTH_RADIUS = 6378137;

  // World size. World bounds are from left -UNIT_SIZE to right +UNIT_SIZE
  // and from top +UNIT_SIZE to bottom -UNIT_SIZE
  public static final float UNIT_SIZE = 500000;

  // Number of milliseconds to delay new frustum cull when the view is changed
  public static final int FRUSTUM_CULL_DELAY = 400;

  // Number of milliseconds to delay new text cull when the view is changed
  public static final int TEXT_CULL_DELAY = 100;

  // Useful parameters for projection calculations
  public static final float DEG_TO_RAD = (float) (Math.PI / 180);
  public static final float RAD_TO_DEG = (float) (180 / Math.PI);
  public static final float LOG_E_05 = (float) Math.log(0.5f);

  // Maximum supported zoom level
  public static final int MAX_SUPPORTED_ZOOM_LEVEL = 24;
  // Fractional added to floating point zoom level before truncating.
  public static final float DISCRETE_ZOOM_LEVEL_BIAS = 0.001f;
  // Maximum range for all possible tile ids for a single raster layer
  public static final long TILE_ID_OFFSET = ((long) Math.pow(4, MAX_SUPPORTED_ZOOM_LEVEL + 1) - 1) / 3;

  // Size limit (in bytes) for style texture cache
  public static final int STYLE_TEXTURE_CACHE_SIZE = 512 * 512 * 4;

  // Size limit (in bytes) for bitmap texture cache
  public static final int BITMAP_TEXTURE_CACHE_SIZE = 512 * 512 * 4;

  // OpenGL 1.1 support for automatic mipmap generation
  public static final boolean OPENGL_11_SUPPORTED = !Build.VERSION.RELEASE.equals("1.5");

  // Sunlight direction vector used in 3d polygon lighting calculation
  public static final Vector3D LIGHT_DIR = new Vector3D(0.35f, 0.35f, -0.87f);
  // Ambient light intensity used in 3d polygon lighting calculation
  public static final float AMBIENT_FACTOR = 0.45f;

  // Vertical field of view angle
  public static final int FOV_ANGLE_Y = 70;
  public static final int HALF_FOV_ANGLE_Y = FOV_ANGLE_Y / 2;
  public static final float HALF_FOV_TAN_Y = (float) Math.tan(FOV_ANGLE_Y * DEG_TO_RAD / 2);
  public static final float HALF_FOV_COS_Y = (float) Math.cos(FOV_ANGLE_Y * DEG_TO_RAD / 2);

  // Map panning type, 0 = fast, accurate (finger stays exactly in the same
  // place), 1 = slow, inaccurate
  public static final float PANNING_SPEED = 0.45f;

  // Maximum building height, taller buildings will get clipped, if the camera
  // gets lower than the acutal building height, then it will get clipped anyway
  //for world map: Const.UNIT_SIZE / 16667. For SF map this looks usable: Const.UNIT_SIZE / 833
  public static final float MAX_HEIGHT = Const.UNIT_SIZE / 833; 
  // Minimum near plane distance, used to avoid visual artifacts on large zoom levels
  public static final float MIN_NEAR = 0.25f;
  // Maximum near plane distance
  public static final float MAX_NEAR = 5000.0f; // TODO: temp hack for demo, should create layer-specfic interface for this
}
