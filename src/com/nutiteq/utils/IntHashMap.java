package com.nutiteq.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * <p>A hash map that uses primitive ints for the key rather than objects.</p>
 *
 * <p>Note that this class is for internal optimization purposes only, and may
 * not be supported in future releases of Apache Commons Lang.  Utilities of
 * this sort may be included in future releases of Apache Commons Collections.</p>
 *
 * @author Justin Couch
 * @author Alex Chaffee (alex@apache.org)
 * @author Stephen Colebourne
 * @author Mark Tehver
 * @since 2.0
 * @version $Revision: 561230 $
 * @see java.util.HashMap
 * @pad.exclude
 */
public class IntHashMap<V> implements IntMap<V> {

  /**
   * The hash table data.
   */
  private transient Entry<V> table[];

  /**
   * The total number of entries in the hash table.
   */
  private transient int count;

  /**
   * The table is rehashed when its size exceeds this threshold.  (The
   * value of this field is (int)(capacity * loadFactor).)
   *
   * @serial
   */
  private int threshold;

  /**
   * The load factor for the hashtable.
   *
   * @serial
   */
  private float loadFactor;

  /**
   * <p>Innerclass that implements iterator interface.</p>
   */
  private class HashIterator implements Iterator<Entry<V>> {
    Entry<V> next;
    int index;
    Entry<V> current;

    HashIterator() {
      if (count > 0) { // advance to first entry
        Entry<V> t[] = table;
        while (index < t.length && (next = t[index++]) == null)
          ;
      }
    }

    public final boolean hasNext() {
      return next != null;
    }

    public Entry<V> next() {
      Entry<V> e = next;
      if (e == null)
        throw new NoSuchElementException();

      if ((next = e.next) == null) {
        Entry<V> t[] = table;
        while (index < t.length && (next = t[index++]) == null)
          ;
      }
      current = e;
      return e;
    }

    public void remove() {
      if (current == null)
        throw new IllegalStateException();
      int k = current.hash;
      current = null;
      IntHashMap.this.remove(k);
    }
  }    

  /**
   * <p>Calculate integer hash from int.<p>
   */
  protected int getHashIndex(int key) {
    return (key & 0x7FFFFFFF);  
  }

  /**
   * <p>Constructs a new, empty hashtable with a default capacity and load
   * factor, which is <code>20</code> and <code>0.75</code> respectively.</p>
   */
  public IntHashMap() {
    this(20, 0.75f);
  }

  /**
   * <p>Constructs a new, empty hashtable with the specified initial capacity
   * and default load factor, which is <code>0.75</code>.</p>
   *
   * @param  initialCapacity the initial capacity of the hashtable.
   * @throws IllegalArgumentException if the initial capacity is less
   *   than zero.
   */
  public IntHashMap(int initialCapacity) {
    this(initialCapacity, 0.75f);
  }

  /**
   * <p>Constructs a new, empty hashtable with the specified initial
   * capacity and the specified load factor.</p>
   *
   * @param initialCapacity the initial capacity of the hashtable.
   * @param loadFactor the load factor of the hashtable.
   * @throws IllegalArgumentException  if the initial capacity is less
   *             than zero, or if the load factor is nonpositive.
   */
  @SuppressWarnings("unchecked")
  public IntHashMap(int initialCapacity, float loadFactor) {
    super();
    if (initialCapacity < 0) {
      throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
    }
    if (loadFactor <= 0) {
      throw new IllegalArgumentException("Illegal Load: " + loadFactor);
    }
    if (initialCapacity == 0) {
      initialCapacity = 1;
    }

    this.loadFactor = loadFactor;
    table = new Entry[initialCapacity];
    threshold = (int) (initialCapacity * loadFactor);
  }

  /**
   * <p>Returns the number of keys in this hashtable.</p>
   *
   * @return  the number of keys in this hashtable.
   */
  public int size() {
    return count;
  }

  /**
   * <p>Tests if this hashtable maps no keys to values.</p>
   *
   * @return  <code>true</code> if this hashtable maps no keys to values;
   *          <code>false</code> otherwise.
   */
  public boolean isEmpty() {
    return count == 0;
  }

  /**
   * <p>Tests if some key maps into the specified value in this hashtable.
   * This operation is more expensive than the <code>containsKey</code>
   * method.</p>
   *
   * <p>Note that this method is identical in functionality to containsValue,
   * (which is part of the Map interface in the collections framework).</p>
   *
   * @param      value   a value to search for.
   * @return     <code>true</code> if and only if some key maps to the
   *             <code>value</code> argument in this hashtable as
   *             determined by the <tt>equals</tt> method;
   *             <code>false</code> otherwise.
   * @throws  NullPointerException  if the value is <code>null</code>.
   * @see        #containsKey(int)
   * @see        #containsValue(Object)
   * @see        java.util.Map
   */
  public boolean contains(V value) {
    if (value == null) {
      throw new NullPointerException();
    }

    Entry<V> tab[] = table;
    for (int i = tab.length; i-- > 0;) {
      for (Entry<V> e = tab[i]; e != null; e = e.next) {
        if (e.value.equals(value)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * <p>Returns <code>true</code> if this HashMap maps one or more keys
   * to this value.</p>
   *
   * <p>Note that this method is identical in functionality to contains
   * (which predates the Map interface).</p>
   *
   * @param value value whose presence in this HashMap is to be tested.
   * @return boolean <code>true</code> if the value is contained
   * @see    java.util.Map
   * @since JDK1.2
   */
  public boolean containsValue(V value) {
    return contains(value);
  }

  /**
   * <p>Tests if the specified object is a key in this hashtable.</p>
   *
   * @param  key  possible key.
   * @return <code>true</code> if and only if the specified object is a
   *    key in this hashtable, as determined by the <tt>equals</tt>
   *    method; <code>false</code> otherwise.
   * @see #contains(Object)
   */
  public boolean containsKey(int key) {
    Entry<V> tab[] = table;
    int hash = key;
    int index = getHashIndex(hash) % tab.length;
    for (Entry<V> e = tab[index]; e != null; e = e.next) {
      if (e.hash == hash) {
        return true;
      }
    }
    return false;
  }

  /**
   * <p>Returns the value to which the specified key is mapped in this map.</p>
   *
   * @param   key   a key in the hashtable.
   * @return  the value to which the key is mapped in this hashtable;
   *          <code>null</code> if the key is not mapped to any value in
   *          this hashtable.
   * @see     #put(int, Object)
   */
  public V get(int key) {
    Entry<V> tab[] = table;
    int hash = key;
    int index = getHashIndex(hash) % tab.length;
    for (Entry<V> e = tab[index]; e != null; e = e.next) {
      if (e.hash == hash) {
        return e.value;
      }
    }
    return null;
  }

  /**
   * <p>Increases the capacity of and internally reorganizes this
   * hashtable, in order to accommodate and access its entries more
   * efficiently.</p>
   *
   * <p>This method is called automatically when the number of keys
   * in the hashtable exceeds this hashtable's capacity and load
   * factor.</p>
   */
  @SuppressWarnings("unchecked")
  protected void rehash() {
    int oldCapacity = table.length;
    Entry<V> oldMap[] = table;

    int newCapacity = oldCapacity * 2 + 1;
    Entry<V> newMap[] = new Entry[newCapacity];

    threshold = (int) (newCapacity * loadFactor);
    table = newMap;

    for (int i = oldCapacity; i-- > 0;) {
      for (Entry<V> old = oldMap[i]; old != null;) {
        Entry<V> e = old;
        old = old.next;

        int index = getHashIndex(e.hash) % newCapacity;
        e.next = newMap[index];
        newMap[index] = e;
      }
    }
  }

  /**
   * <p>Maps the specified <code>key</code> to the specified
   * <code>value</code> in this hashtable. The key cannot be
   * <code>null</code>. </p>
   *
   * <p>The value can be retrieved by calling the <code>get</code> method
   * with a key that is equal to the original key.</p>
   *
   * @param key     the hashtable key.
   * @param value   the value.
   * @return the previous value of the specified key in this hashtable,
   *         or <code>null</code> if it did not have one.
   * @throws  NullPointerException  if the key is <code>null</code>.
   * @see     #get(int)
   */
  public V put(int key, V value) {
    // Makes sure the key is not already in the hashtable.
    Entry<V> tab[] = table;
    int hash = key;
    int index = getHashIndex(hash) % tab.length;
    for (Entry<V> e = tab[index]; e != null; e = e.next) {
      if (e.hash == hash) {
        V oldValue = e.value;
        e.value = value;
        return oldValue;
      }
    }

    if (count >= threshold) {
      // Rehash the table if the threshold is exceeded
      rehash();

      tab = table;
      index = getHashIndex(hash) % tab.length;
    }

    // Creates the new entry.
    Entry<V> e = new Entry<V>(hash, value, tab[index]);
    tab[index] = e;
    count++;
    return null;
  }

  /**
   * <p>Removes the key (and its corresponding value) from this
   * hashtable.</p>
   *
   * <p>This method does nothing if the key is not present in the
   * hashtable.</p>
   *
   * @param   key   the key that needs to be removed.
   * @return  the value to which the key had been mapped in this hashtable,
   *          or <code>null</code> if the key did not have a mapping.
   */
  public V remove(int key) {
    Entry<V> tab[] = table;
    int hash = key;
    int index = getHashIndex(hash) % tab.length;
    for (Entry<V> e = tab[index], prev = null; e != null; prev = e, e = e.next) {
      if (e.hash == hash) {
        if (prev != null) {
          prev.next = e.next;
        } else {
          tab[index] = e.next;
        }
        count--;
        V oldValue = e.value;
        e.value = null;
        return oldValue;
      }
    }
    return null;
  }

  /**
   * <p>Clears this hashtable so that it contains no keys.</p>
   */
  public synchronized void clear() {
    Entry<V> tab[] = table;
    for (int index = tab.length; --index >= 0;) {
      tab[index] = null;
    }
    count = 0;
  }

  /**
   * <p>Return collection containing all keys</p>
   *
   * @return  collection containing all keys.
   */
  public IntArrayList keys() {
    IntArrayList list = new IntArrayList(count + 1); 
    for (int index = table.length; --index >= 0;) {
      Entry<V> entry = table[index];
      while (entry != null) {
        list.add(entry.hash);
        entry = entry.next;
      }
    }
    return list;
  }

  /**
   * <p>Return collection containing all values</p>
   *
   * @return  collection containing all values.
   */
  public Collection<V> values() {
    List<V> list = new ArrayList<V>(count + 1); 
    for (int index = table.length; --index >= 0;) {
      Entry<V> entry = table[index];
      while (entry != null) {
        list.add(entry.value);
        entry = entry.next;
      }
    }
    return list;
  }

  /**
   * <p>Return new entry iterator</p>
   *
   * @return  new iterator.
   */
  public Iterator<Entry<V>> entrySetIterator() {
    return new HashIterator();
  }
}
