package com.nutiteq.utils;

/**
 * 3D frustum defined by 6 clipping planes.
 * 
 * Originally taken from http://www.racer.nl/reference/vfc_markmorley.htm
 * @pad.exclude
 */
public class Frustum {

  /**
   * No intersection.
   */
  public static final int MISS = 0;
  
  /**
   * Partial intersection.
   */
  public static final int PARTIAL = 1;
  
  /**
   * Full containment (frustum contains the geometric primitive)
   */
  public static final int COMPLETE = 2;

  /**
   * Frustum planes, in right, left, bottom, top, far, near order. Each plane is
   * defined by normalX, normalY, normalZ, distanceFromOrigin values
   */
  private final double[][] frustum = new double[6][4]; // right, left, bottom, top, far, near

  /**
   * Default constructor
   */
  public Frustum() {
  }
  
  /**
   * Class constructor
   * 
   * @param proj
   *          the projection matrix
   * @param modl
   *          the modelview matrix
   */
  public Frustum(float[] proj, double[] modl) {
    try {
      double[] clip = new double[16]; // used as temporary buffer only, not really part of the state

      // Combine the two matrices (multiply projection by modelview)
      clip[0] = modl[0] * proj[0] + modl[1] * proj[4] + modl[2] * proj[8] + modl[3] * proj[12];
      clip[1] = modl[0] * proj[1] + modl[1] * proj[5] + modl[2] * proj[9] + modl[3] * proj[13];
      clip[2] = modl[0] * proj[2] + modl[1] * proj[6] + modl[2] * proj[10] + modl[3] * proj[14];
      clip[3] = modl[0] * proj[3] + modl[1] * proj[7] + modl[2] * proj[11] + modl[3] * proj[15];
      clip[4] = modl[4] * proj[0] + modl[5] * proj[4] + modl[6] * proj[8] + modl[7] * proj[12];
      clip[5] = modl[4] * proj[1] + modl[5] * proj[5] + modl[6] * proj[9] + modl[7] * proj[13];
      clip[6] = modl[4] * proj[2] + modl[5] * proj[6] + modl[6] * proj[10] + modl[7] * proj[14];
      clip[7] = modl[4] * proj[3] + modl[5] * proj[7] + modl[6] * proj[11] + modl[7] * proj[15];
      clip[8] = modl[8] * proj[0] + modl[9] * proj[4] + modl[10] * proj[8] + modl[11] * proj[12];
      clip[9] = modl[8] * proj[1] + modl[9] * proj[5] + modl[10] * proj[9] + modl[11] * proj[13];
      clip[10] = modl[8] * proj[2] + modl[9] * proj[6] + modl[10] * proj[10] + modl[11] * proj[14];
      clip[11] = modl[8] * proj[3] + modl[9] * proj[7] + modl[10] * proj[11] + modl[11] * proj[15];
      clip[12] = modl[12] * proj[0] + modl[13] * proj[4] + modl[14] * proj[8] + modl[15] * proj[12];
      clip[13] = modl[12] * proj[1] + modl[13] * proj[5] + modl[14] * proj[9] + modl[15] * proj[13];
      clip[14] = modl[12] * proj[2] + modl[13] * proj[6] + modl[14] * proj[10] + modl[15] * proj[14];
      clip[15] = modl[12] * proj[3] + modl[13] * proj[7] + modl[14] * proj[11] + modl[15] * proj[15];

      // right
      frustum[0][0] = clip[3] - clip[0];
      frustum[0][1] = clip[7] - clip[4];
      frustum[0][2] = clip[11] - clip[8];
      frustum[0][3] = clip[15] - clip[12];
      // left
      frustum[1][0] = clip[3] + clip[0];
      frustum[1][1] = clip[7] + clip[4];
      frustum[1][2] = clip[11] + clip[8];
      frustum[1][3] = clip[15] + clip[12];
      // bottom
      frustum[2][0] = clip[3] + clip[1];
      frustum[2][1] = clip[7] + clip[5];
      frustum[2][2] = clip[11] + clip[9];
      frustum[2][3] = clip[15] + clip[13];
      // top
      frustum[3][0] = clip[3] - clip[1];
      frustum[3][1] = clip[7] - clip[5];
      frustum[3][2] = clip[11] - clip[9];
      frustum[3][3] = clip[15] - clip[13];
      // far
      frustum[4][0] = clip[3] - clip[2];
      frustum[4][1] = clip[7] - clip[6];
      frustum[4][2] = clip[11] - clip[10];
      frustum[4][3] = clip[15] - clip[14];
      // near
      frustum[5][0] = clip[3] + clip[2];
      frustum[5][1] = clip[7] + clip[6];
      frustum[5][2] = clip[11] + clip[10];
      frustum[5][3] = clip[15] + clip[14];
      
      // normalize
      normalize();
    } catch (Exception e) {
      StringBuilder b = new StringBuilder();
      b.append("modl.length = ").append(modl.length);
      b.append("\nproj.length = ").append(proj.length);
      b.append("\nfrustum.length = ").append(frustum.length);
      for (int i = 0; i < frustum.length; i++) {
        try {
          b.append("\n\tfrustum[ ").append(i).append(" ].length = ");
          if (frustum[i] != null) {
            b.append(frustum[i].length);
          } else {
            b.append("null");
          }
        } catch (Exception omg) {
          throw new RuntimeException("Sweet jesus what is going on here? Exception on index " + i, omg);
        }
      }
      throw new RuntimeException(b.toString(), e);
    }
  }

  /**
   * Class constructor
   * 
   * @param proj
   *          the projection matrix
   * @param modl
   *          the modelview matrix
   */
  public Frustum(float[] proj, float[] modl) {
    this(proj, toDoubleMatrix(modl));
  }
  
  /**
   * Copy constructor
   * 
   * @param f
   *          the Frustum object to be copied
   */
  public Frustum(Frustum f) {
    for (int i = 0; i < frustum.length; i++) {
      for (int j = 0; j < frustum[i].length; j++) {
        frustum[i][j] = f.frustum[i][j];
      }
    }
  }

  /**
   * Checks if the point is inside the frustum.
   * 
   * @param x
   *          x coordinate of the point
   * @param y
   *          y coordinate of the point
   * @param z
   *          z coordinate of the point
   * @return true if the point lies within the frustum
   */
  public boolean pointInside(float x, float y, float z) {
    for (int p = 0; p < 6; p++) {
      if (frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3] <= 0) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if the circle (on z=0 plane) intersects the frustum.
   * 
   * @param x
   *          x coordinate of the circle (origin)
   * @param y
   *          y coordinate of the circle (origin)
   * @param r
   *          circle radius
   * @return The intersection state between the circle and the frustum
   */
  public boolean circleIntersects(float x, float y, float r) {
    for (int p = 0; p < 6; p++) {
      double d = frustum[p][0] * x + frustum[p][1] * y + frustum[p][3];
      if (d <= -r) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if the sphere intersects the frustum.
   * 
   * @param x
   *          x coordinate of sphere (origin)
   * @param y
   *          y coordinate of sphere (origin)
   * @param z
   *          z coordinate of sphere (origin)
   * @param r
   *          sphere radius
   * @return The intersection state between the sphere and the frustum
   */
  public int sphereIntersects(float x, float y, float z, float r) {
    int c = 0;
    for (int p = 0; p < 6; p++) {
      double d = frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3];
      if (d <= -r) {
        return MISS;
      }
      if (d > r) {
        c++;
      }
    }
    return c == 6 ? COMPLETE : PARTIAL;
  }


  /**
   * Checks if the sphere intersects the frustum, and calculates the distance to
   * the near plane if so.
   * 
   * @param x
   *          x coordinate of sphere (origin)
   * @param y
   *          y coordinate of sphere (origin)
   * @param z
   *          z coordinate of sphere (origin)
   * @param r
   *          sphere radius
   * @return the distance from the camera to the sphere's center plus the
   *         radius, or 0 if the sphere does not intersect the frustum
   */
  public float sphereDistance(float x, float y, float z, float r) {
    double d = 0;
    for (int p = 0; p < 6; p++) {
      d = frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3];
      if (d <= -r) {
        return 0;
      }
    }
    return (float) (d + r);
  }

  /**
   * Checks if square (on z=0 plane) intersects the frustum.
   * 
   * @param minx
   *          square minimum x coordinate
   * @param miny
   *          square minimum y coordinate
   * @param maxx
   *          square maximum x coordinate
   * @param maxy
   *          square maximum y coordinate
   * @return the intersection state between the square and the frustum
   */
  public boolean squareIntersects(float minx, float miny, float maxx, float maxy) {
    int c;
    for (int p = 0; p < 6; p++) {
      c = 0;
      if (frustum[p][0] * minx + frustum[p][1] * miny + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * maxx + frustum[p][1] * miny + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * minx + frustum[p][1] * maxy + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * maxx + frustum[p][1] * maxy + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * minx + frustum[p][1] * miny + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * maxx + frustum[p][1] * miny + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * minx + frustum[p][1] * maxy + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * maxx + frustum[p][1] * maxy + frustum[p][3] > 0) {
        c++;
      }
      if (c == 0) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if cuboid intersects the frustum.
   * 
   * @param minx
   *          cuboid minimum x coordinate
   * @param miny
   *          cuboid minimum y coordinate
   * @param minz
   *          cuboid minimum z coordinate
   * @param maxx
   *          cuboid maximum x coordinate
   * @param maxy
   *          cuboid maximum y coordinate
   * @param maxz
   *          cuboid maximum z coordinate
   * @return the intersection state between the cuboid and the frustum
   */
  public int cuboidIntersects(double minx, double miny, double minz, double maxx, double maxy, double maxz) {
    int c;
    int c2 = 0;
    for (int p = 0; p < 6; p++) {
      c = 0;
      if (frustum[p][0] * minx + frustum[p][1] * miny + frustum[p][2] * minz + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * maxx + frustum[p][1] * miny + frustum[p][2] * minz + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * minx + frustum[p][1] * maxy + frustum[p][2] * minz + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * maxx + frustum[p][1] * maxy + frustum[p][2] * minz + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * minx + frustum[p][1] * miny + frustum[p][2] * maxz + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * maxx + frustum[p][1] * miny + frustum[p][2] * maxz + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * minx + frustum[p][1] * maxy + frustum[p][2] * maxz + frustum[p][3] > 0) {
        c++;
      }
      if (frustum[p][0] * maxx + frustum[p][1] * maxy + frustum[p][2] * maxz + frustum[p][3] > 0) {
        c++;
      }
      if (c == 0) {
        return MISS;
      }
      if (c == 8) {
        c2++;
      }
    }
    return c2 == 6 ? COMPLETE : PARTIAL;
  }
  
  /**
   * Checks/clips line against the frustum planes.
   * 
   * @param l0
   *          first endpoint coordinates. After calling will contain clipped coordinates.
   * @param l1
   *          second endpoint coordinates. After calling will contain clipped coordinates.
   * @param size
   *          line size (width). This parameter will enlarge the frustum accordingly. 
   * @return true if line is inside frustum, false otherwise.
   */
  public boolean clipLine(double[] l0, double[] l1, float size) {
    for (int i = 0; i < frustum.length; i++) {
      double d0 = frustum[i][3];
      double d1 = frustum[i][3];
      for (int j = 0; j < 3; j++) {
        d0 += frustum[i][j] * l0[j];
        d1 += frustum[i][j] * l1[j];
      }
      if (d0 <= -size && d1 <= -size)
        return false;
      if (d0 < -size) {
        double d = (-size - d0) / (d1 - d0);
        for (int j = 0; j < 3; j++) {
          l0[j] = l0[j] + d * (l1[j] - l0[j]);
        }
      } else if (d1 < -size) {
        double d = (-size - d1) / (d0 - d1);
        for (int j = 0; j < 3; j++) {
          l1[j] = l1[j] + d * (l0[j] - l1[j]);
        }
      }
    }
    return true;
  }

  private void normalize() {
    double t = 1.0 / Math.sqrt(frustum[0][0] * frustum[0][0] + frustum[0][1] * frustum[0][1] + frustum[0][2] * frustum[0][2]);
    frustum[0][0] *= t;
    frustum[0][1] *= t;
    frustum[0][2] *= t;
    frustum[0][3] *= t;

    t = 1.0 / Math.sqrt(frustum[1][0] * frustum[1][0] + frustum[1][1] * frustum[1][1] + frustum[1][2] * frustum[1][2]);
    frustum[1][0] *= t;
    frustum[1][1] *= t;
    frustum[1][2] *= t;
    frustum[1][3] *= t;

    t = 1.0 / Math.sqrt(frustum[2][0] * frustum[2][0] + frustum[2][1] * frustum[2][1] + frustum[2][2] * frustum[2][2]);
    frustum[2][0] *= t;
    frustum[2][1] *= t;
    frustum[2][2] *= t;
    frustum[2][3] *= t;

    t = 1.0 / Math.sqrt(frustum[3][0] * frustum[3][0] + frustum[3][1] * frustum[3][1] + frustum[3][2] * frustum[3][2]);
    frustum[3][0] *= t;
    frustum[3][1] *= t;
    frustum[3][2] *= t;
    frustum[3][3] *= t;

    t = 1.0 / Math.sqrt(frustum[4][0] * frustum[4][0] + frustum[4][1] * frustum[4][1] + frustum[4][2] * frustum[4][2]);
    frustum[4][0] *= t;
    frustum[4][1] *= t;
    frustum[4][2] *= t;
    frustum[4][3] *= t;

    t = 1.0 / Math.sqrt(frustum[5][0] * frustum[5][0] + frustum[5][1] * frustum[5][1] + frustum[5][2] * frustum[5][2]);
    frustum[5][0] *= t;
    frustum[5][1] *= t;
    frustum[5][2] *= t;
    frustum[5][3] *= t;
  }
  
  private static double[] toDoubleMatrix(float[] fltMatrix) {
    double[] dblMatrix = new double[fltMatrix.length];
    for (int i = 0; i < fltMatrix.length; i++) {
      dblMatrix[i] = fltMatrix[i];
    }
    return dblMatrix;
  }

}
