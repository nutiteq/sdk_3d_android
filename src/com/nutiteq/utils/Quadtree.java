package com.nutiteq.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.nutiteq.components.Envelope;

/**
 * Quadtree spatial data structure for efficient geometry indexing.
 *
 * @param <T>
 *          geometry type to store in quadtree
 * @pad.exclude
 */
public class Quadtree<T> {

  private static final int MAX_DEPTH = 20;

  private static class Record<T> {
    final Envelope envelope;
    final T object;

    Record(Envelope envelope, T object) {
      this.envelope = envelope;
      this.object = object;
    }
  }

  private static class Node<T> {
    Envelope envelope;
    List<Record<T>> records;
    Node<T>[] children; // topleft, topright, bottomleft, bottomright
    double centerX, centerY; // only defined if children != null. Once set, becomes immutable

    Node(Envelope env) {
      envelope = env;
      records = new LinkedList<Record<T>>();
      children = null;
      centerX = centerY = Double.NaN;
    }
  }

  private Node<T> root;
  private int count;

  public Quadtree() {
    this.root = null;
    this.count = 0;
  }

  public void clear() {
    this.root = null;
    this.count = 0;
  }
  
  public int count() {
    return count;
  }

  public boolean insert(Envelope envelope, T object) {
    if (Double.isNaN(envelope.getMinX()) || Double.isNaN(envelope.getMaxX()) ||
        Double.isNaN(envelope.getMinY()) || Double.isNaN(envelope.getMaxY())) {
      return false;
    }
    if (root == null) {
      root = new Node<T>(envelope);
    }
    insertToNode(root, envelope, object, 0);
    return true;
  }

  @SuppressWarnings("unchecked")
  private Node<T> insertToNode(Node<T> node, Envelope envelope, T object, int depth) {
    node.envelope = addEnvelopes(node.envelope, envelope);

    // Find child index (do this only in case when current node is not empty)
    int index = -1;
    if (!(node.records.isEmpty() && node.children == null)) {
      index = getChildIndex(node, envelope);
    }

    // If this was not possible (or depth limit has exceeded) add object to current node
    if (index < 0 || depth >= MAX_DEPTH) {
      node.records.add(new Record<T>(envelope, object));
      count++;
      return node;
    }

    // Create children and redistribute node records among children if children do not exist yet
    if (node.children == null) {
      node.centerX = (node.envelope.getMinX() + node.envelope.getMaxX()) * 0.5;
      node.centerY = (node.envelope.getMinY() + node.envelope.getMaxY()) * 0.5;
      if (Double.isInfinite(node.centerX) || Double.isNaN(node.centerX)) {
        node.centerX = 0;
      }
      if (Double.isInfinite(node.centerY) || Double.isNaN(node.centerY)) {
        node.centerY = 0;
      }
      node.children = new Node[4];
      for (ListIterator<Record<T>> it = node.records.listIterator(); it.hasNext(); ) {
        Record<T> record = it.next();
        int index2 = getChildIndex(node, record.envelope);
        if (index2 < 0) {
          continue;
        }
        if (node.children[index2] == null) {
          node.children[index2] = new Node<T>(record.envelope);
        }
        node.children[index2].envelope = addEnvelopes(node.children[index2].envelope, record.envelope);
        node.children[index2].records.add(record);
        it.remove();
      }
    }

    // Recurse to children
    if (node.children[index] == null) {
      node.children[index] = new Node<T>(envelope);
    }
    return insertToNode(node.children[index], envelope, object, depth + 1);
  }

  private Envelope addEnvelopes(Envelope envelope1, Envelope envelope2) {
    if (envelope1.contains(envelope2)) {
      return envelope1;
    }
    if (envelope2.contains(envelope1)) {
      return envelope2;
    }

    double minX = Math.min(envelope1.minX, envelope2.minX);
    double maxX = Math.max(envelope1.maxX, envelope2.maxX);
    double minY = Math.min(envelope1.minY, envelope2.minY);
    double maxY = Math.max(envelope1.maxY, envelope2.maxY);
    return new Envelope(minX, maxX, minY, maxY);
  }

  private int getChildIndex(Node<T> node, Envelope envelope) {
    for (int index = 0; index < 4; index++) {
      if ((index & 1) != 0) {
        if (envelope.minX < node.centerX)
          continue;
      } else {
        if (envelope.maxX > node.centerX)
          continue;
      }
      if ((index & 2) != 0) {
        if (envelope.minY < node.centerY)
          continue;
      } else {
        if (envelope.maxY > node.centerY)
          continue;
      }
      return index;
    }
    return -1;
  }

  public boolean remove(T object) {
    int count = this.count;
    root = removeFromNode(root, null, object);
    return count != this.count;
  }

  public boolean remove(Envelope envelope, T object) {
    int count = this.count;
    root = removeFromNode(root, envelope, object);
    return count != this.count;
  }

  private Node<T> removeFromNode(Node<T> node, Envelope envelope, T object) {
    // Check if we need to proceed
    if (node == null) {
      return node;
    }
    if (envelope != null && !node.envelope.intersects(envelope)) {
      return node;
    }

    // Remove object from current node
    for (ListIterator<Record<T>> it = node.records.listIterator(); it.hasNext(); ) {
      Record<T> record = it.next();
      if (record.object == object) {
        it.remove();
        count--;
      }
    }

    // Recurse and prune
    if (node.children != null) {
      boolean empty = true;
      for (int i = 0; i < node.children.length; i++) {
        node.children[i] = removeFromNode(node.children[i], envelope, object);
        if (node.children[i] != null) {
          empty = false;
        }
      }
      if (empty) {
        node.children = null;
      }
    }
    return node.records.isEmpty() && node.children == null ? null : node;
  }

  public List<T> query(Envelope envelope) {
    List<T> results = new ArrayList<T>();
    queryNode(root, envelope, results);
    return results;
  }

  private void queryNode(Node<T> node, Envelope envelope, List<T> results) {
    // Check if this node intersects with given envelope
    if (node == null) {
      return;
    }
    if (!node.envelope.intersects(envelope)) {
      return;
    }

    // Test for intersection of current node records
    for (ListIterator<Record<T>> it = node.records.listIterator(); it.hasNext(); ) {
      Record<T> record = it.next();
      if (record.envelope.intersects(envelope)) {
        results.add(record.object);
      }
    }

    // Recurse
    if (node.children != null) {
      for (int i = 0; i < node.children.length; i++) {
        queryNode(node.children[i], envelope, results);
      }
    }
  }
  
  public List<T> getAll() {
    List<T> list = new ArrayList<T>();
    getAllFromNode(root, list);
    return list;
  }
  
  private void getAllFromNode(Node<T> node, List<T> list) {
    if (node == null) {
      return;
    }
    
    // All objects under this node
    for (ListIterator<Record<T>> it = node.records.listIterator(); it.hasNext(); ) {
      Record<T> record = it.next();
      list.add(record.object);
    }

    // Recurse
    if (node.children != null) {
      for (int i = 0; i < node.children.length; i++) {
        getAllFromNode(node.children[i], list);
      }
    }
  }

}
