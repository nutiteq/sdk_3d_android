package com.nutiteq.utils;

import java.util.Collection;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Various internal utilities for the SDK.
 * @pad.exclude
 */
public class Utils {

  // Input must be power of 2
  public static int log2(int x) {
    for (int i = 0; i < 32; i++) {
      if ((1 << i) == x) {
        return i;
      }
    }
    throw new IllegalArgumentException("Do not know the log2 from " + x);
  }

  // Round v up to the next highest power of 2
  public static int upperPow2(int v) {
    if (v <= 0) {
      return 1;
    }
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    return v;
  }

  public static int unsigned(final byte b) {
    return (b < 0) ? (b + 256) : b;
  }

  public static float toRange(float value, float min, float max) {
    if (value <= min) {
      return min;
    }
  
    if (value >= max) {
      return max;
    }
  
    return value;
  }

  public static double toRange(double value, double min, double max) {
    if (value <= min) {
      return min;
    }
  
    if (value >= max) {
      return max;
    }
  
    return value;
  }
  
  public static float lerp(float val1, float val2, float t) {
    return val1 * (1 - t) + val2 * t;
  }

  public static double lerp(double val1, double val2, double t) {
    return val1 * (1 - t) + val2 * t;
  }

  public static String[] split(final String string, final String splitBy) {
    final Vector<String> tokens = new Vector<String>();
    final int tokenLength = splitBy.length();

    int tokenStart = 0;
    int splitIndex;
    while ((splitIndex = string.indexOf(splitBy, tokenStart)) != -1) {
      tokens.addElement(string.substring(tokenStart, splitIndex));
      tokenStart = splitIndex + tokenLength;
    }

    tokens.addElement(string.substring(tokenStart));

    final String[] result = new String[tokens.size()];
    tokens.copyInto(result);
    return result;
  }

  @Deprecated
  public static String[] split(final String s, final char c, final boolean dblquotes, final int max) {
    int j = 0;
    final Vector<String> vector = new Vector<String>();

    // add first max-1 components
    int num = 0;
    int i = 0;
    String ss = null;
    int k1;
    int k2;
    for (i = 0; num != max - 1; i = j + 1) {
      k1 = -1;
      k2 = -1;
      j = s.indexOf(c, i);
      if (dblquotes) {
        // should have k1=0
        k1 = s.indexOf('"', i);
        // quote found and before delimiter
        if (k1 >= 0 && k1 < j) {
          // next quote
          k2 = s.indexOf('"', k1 + 1);
          if (k2 >= 0) {
            // recompute next delimiter - should have j=k2+1
            j = s.indexOf(c, k2 + 1);
          }
        }
      }
      if (j >= 0) {
        if (dblquotes && k1 >= 0 && k2 >= 0) {
          ss = s.substring(k1 + 1, k2);
        } else {
          ss = s.substring(i, j);
        }
        vector.addElement(ss);
        num++;
      } else {
        if (dblquotes && k1 >= 0 && k2 >= 0) {
          ss = s.substring(k1 + 1, k2);
        } else {
          ss = s.substring(i);
        }
        vector.addElement(ss);
        num++;
        break;
      }
    }

    // add the max-th component
    k1 = -1;
    k2 = -1;
    if (max != 0 && j >= 0) {
      if (dblquotes) {
        k1 = s.indexOf('"', i);
        // quote found and before delimiter
        if (k1 >= 0) {
          // next quote
          k2 = s.indexOf('"', k1 + 1);
        }
      }
      if (dblquotes && k1 >= 0 && k2 >= 0) {
        ss = s.substring(k1 + 1, k2);
      } else {
        ss = s.substring(i);
      }
      vector.addElement(ss);
      num++;
    }

    // convert to array
    final String as[] = new String[num];
    vector.copyInto(as);

    // return the array
    return as;
  }
  
  public static String join(Collection<String> strings, char separator) {
    StringBuilder sb = new StringBuilder();
    int n = 0;
    for (String str : strings) {
      if (n++ > 0) {
        sb.append(separator);
      }
      sb.append(str);
    }
    return sb.toString();
  }

  public static byte[] hexStringToByteArray(String s) {
    int len = s.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
      data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
    }
    return data;
  }

  @Deprecated
  public static String prepareForParameters(final String url) {
    if (url.endsWith("?") || url.endsWith("&")) {
        return url;
    }

    if (url.indexOf("?") > 0) {
        return url + "&";
    } else {
        return url + "?";
    }
  }

  /**
   * URL encode arbitrary string.
   * 
   * @param s
   *          string to url-encode
   * @return url-encoded string
   */
  public static String urlEncode(final String s) {
    final String HEX_TABLE = "0123456789ABCDEF";

    if (s == null) {
      return null;
    }

    byte[] b = null;

    try {
      b = s.getBytes("utf-8");
    } catch (final Exception ex) {
      try {
        b = s.getBytes("UTF8");
      } catch (final Exception ex2) {
        b = s.getBytes();
      }
    }

    final StringBuffer result = new StringBuffer();

    for (int i = 0; i < b.length; i++) {
      if ((b[i] >= 'A' && b[i] <= 'Z') || (b[i] >= 'a' && b[i] <= 'z') || (b[i] >= '0' && b[i] <= '9')) {
        result.append((char) b[i]);
      } else {
        result.append('%');
        result.append(HEX_TABLE.charAt(((b[i] + 256) >> 4) & 0x0F));
        result.append(HEX_TABLE.charAt(b[i] & 0x0F));
      }
    }

    return result.toString();
  }
  
  /**
   * Decode string encoded using Base64 method.
   * 
   * @param s String to encode
   * @return decoded binary array
   */
  public static byte[] base64Decode(final String s) {
    final byte[] DECODE_TABLE = {
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54,
        55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4,
        5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
        24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34,
        35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51
    };
    final byte PAD = '=';
    final int MASK_8BITS = 0xff;

    byte[] inBuffer = s.getBytes();
    byte[] buffer = new byte[s.length() * 2];
    int inPos = 0, pos = 0;
    int x = 0, modulus = 0;
    boolean eof = false;
    for (int i = 0; i < s.length(); i++) {
      byte b = inBuffer[inPos++];
      if (b == PAD) {
        // We're done.
        eof = true;
        break;
      } else {
        if (b >= 0 && b < DECODE_TABLE.length) {
          int result = DECODE_TABLE[b];
          if (result >= 0) {
            modulus = (++modulus) % 4;
            x = (x << 6) + result;
            if (modulus == 0) {
              buffer[pos++] = (byte) ((x >> 16) & MASK_8BITS);
              buffer[pos++] = (byte) ((x >> 8) & MASK_8BITS);
              buffer[pos++] = (byte) (x & MASK_8BITS);
            }
          }
        }
      }
    }

    // Two forms of EOF as far as base64 decoder is concerned: actual
    // EOF (-1) and first time '=' character is encountered in stream.
    // This approach makes the '=' padding characters completely optional.
    if (eof && modulus != 0) {
      x = x << 6;
      switch (modulus) {
      case 2 :
        x = x << 6;
        buffer[pos++] = (byte) ((x >> 16) & MASK_8BITS);
        break;
      case 3 :
        buffer[pos++] = (byte) ((x >> 16) & MASK_8BITS);
        buffer[pos++] = (byte) ((x >> 8) & MASK_8BITS);
        break;
      }
    }
    byte[] outBuffer = new byte[pos];
    System.arraycopy(buffer, 0, outBuffer, 0, pos);
    return outBuffer;
  }

  /**
   * Encode binary data using Base64 method.
   * 
   * @param in data to encode
   * @param lineLength maximum line length in output (or -1 if unlimited)
   * @return encoded base64 string
   */
  public static String base64Encode(byte[] in, int lineLength) {
    final char[] ENCODE_TABLE = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
    };
    final char PAD = '=';
    final int BYTES_PER_UNENCODED_BLOCK = 3;
    final int BYTES_PER_ENCODED_BLOCK = 4;
    final int MASK_6BITS = 0x3f;

    int inPos = 0;
    int inAvail = in.length;
    int ibitWorkArea = 0;
    int modulus = 0;
    int currentLinePos = 0;
    StringBuilder str = new StringBuilder();

    for (int i = 0; i < inAvail; i++) {
      modulus = (modulus+1) % BYTES_PER_UNENCODED_BLOCK;
      int b = in[inPos++];
      if (b < 0) {
        b += 256;
      }
      ibitWorkArea = (ibitWorkArea << 8) + b; //  BITS_PER_BYTE
      if (0 == modulus) { // 3 bytes = 24 bits = 4 * 6 bits to extract
        str.append(ENCODE_TABLE[(ibitWorkArea >> 18) & MASK_6BITS]);
        str.append(ENCODE_TABLE[(ibitWorkArea >> 12) & MASK_6BITS]);
        str.append(ENCODE_TABLE[(ibitWorkArea >> 6) & MASK_6BITS]);
        str.append(ENCODE_TABLE[ibitWorkArea & MASK_6BITS]);
        currentLinePos += BYTES_PER_ENCODED_BLOCK;
        if (lineLength > 0 && lineLength <= currentLinePos) {
          str.append('\n');
          currentLinePos = 0;
        }
      }
    }

    if (0 == modulus && lineLength == 0) {
      return str.toString(); // no leftovers to process and not using chunking
    }
    switch (modulus) { // 0-2
    case 0 : // nothing to do here
      break;
    case 1 : // 8 bits = 6 + 2
      // top 6 bits:
      str.append(ENCODE_TABLE[(ibitWorkArea >> 2) & MASK_6BITS]);
      // remaining 2:
      str.append(ENCODE_TABLE[(ibitWorkArea << 4) & MASK_6BITS]);
      // URL-SAFE skips the padding to further reduce size.
      str.append(PAD);
      str.append(PAD);
      break;
    case 2 : // 16 bits = 6 + 6 + 4
      str.append(ENCODE_TABLE[(ibitWorkArea >> 10) & MASK_6BITS]);
      str.append(ENCODE_TABLE[(ibitWorkArea >> 4) & MASK_6BITS]);
      str.append(ENCODE_TABLE[(ibitWorkArea << 2) & MASK_6BITS]);
      // URL-SAFE skips the padding to further reduce size.
      str.append(PAD);
      break;
    default:
      throw new IllegalStateException("Impossible modulus "+modulus);
    }
    return str.toString();
  }

  /**
   * Find and replace tags of the form {tag} in text. If text contains tags with unspecified replacements, these tags are left as-is.
   * 
   * @param message template message containing the tags
   * @param tags map of tags and corresponding replacement values
   * @return message with all tags replaced
   */
  public static String replaceTags(String message, Map<String, String> tags) {
    Pattern p = Pattern.compile("\\{(\\w+)\\}");
    Matcher m = p.matcher(message);
    boolean result = m.find();
    if (result) {
      StringBuffer sb = new StringBuffer();
      do {
        String tag = m.group(1);
        m.appendReplacement(sb, tags.containsKey(tag) ? tags.get(tag) : "{" + tag + "}");
        result = m.find();
      } while (result);
      m.appendTail(sb);
      message = sb.toString();
    }
    return message;
  }
}
