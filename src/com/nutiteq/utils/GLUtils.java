package com.nutiteq.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;

/**
 * OpenGL-related utilites.
 * @pad.exclude
 */
public class GLUtils {
  private static final int MAX_VERTEXBUFFER_SIZE = 65535;
  private static final float[] QUAD_VERTICES = { -0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f, -0.5f, 0.5f, 0.0f, 0.5f, 0.5f, 0.0f };
  private static final FloatBuffer QUAD_VERTEX_BUF;
  
  static {
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(QUAD_VERTICES.length * Float.SIZE / 8);
    byteBuffer.order(ByteOrder.nativeOrder());
    QUAD_VERTEX_BUF = byteBuffer.asFloatBuffer();
    QUAD_VERTEX_BUF.put(QUAD_VERTICES);
    QUAD_VERTEX_BUF.position(0);
  }
  
  public static void drawQuad(GL10 gl) {
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, QUAD_VERTEX_BUF);
    gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, QUAD_VERTICES.length / 3);
  }
  
  public static void drawTexturedQuad(GL10 gl, int texture, FloatBuffer texCoordBuf) {
    gl.glEnable(GL10.GL_TEXTURE_2D);
    gl.glBindTexture(GL10.GL_TEXTURE_2D, texture);
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, QUAD_VERTEX_BUF);
    gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, texCoordBuf);
    gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, QUAD_VERTICES.length / 3);
  }

  public static int buildTexture(GL10 gl, Bitmap bitmap) {
    int[] ids = new int[1];
    gl.glGenTextures(1, ids, 0);
    int texture = ids[0];
  
    gl.glBindTexture(GL10.GL_TEXTURE_2D, texture);
  
    gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
    gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
  
    android.opengl.GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
  
    return texture;
  }

  public static int buildMipmaps(GL10 gl, Bitmap bitmap) {
    // Safety check - test if mipmaps can be created 
    if (bitmap.getWidth() < 2 || bitmap.getHeight() < 2 || (bitmap.getWidth() & (bitmap.getWidth() - 1)) != 0 || (bitmap.getHeight() & (bitmap.getHeight() - 1)) != 0) {
      return buildTexture(gl, bitmap);
    }

    int[] ids = new int[1];
    gl.glGenTextures(1, ids, 0);
    int texture = ids[0];

    gl.glBindTexture(GL10.GL_TEXTURE_2D, texture);

    gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR_MIPMAP_LINEAR);
    gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

    //  jaakl: this is commented out, otherwise HTC Desire had black overlay tiles (Geomatic issue)
    // TODO jaakl: find more optimal way to fix HTC Desire specific issue, so other OGL11 devices would run faster
    //    if (Const.OPENGL_11_SUPPORTED) {
    //      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR_MIPMAP_LINEAR);
    //      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
    //
    //      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL11.GL_GENERATE_MIPMAP, GL10.GL_TRUE);
    //      GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
    //
    //    } else {
    // This ensures that the original and mipmap bitmaps have the same pixel format
    bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), false);

    int level = 0;
    int height = bitmap.getHeight();
    int width = bitmap.getWidth();
    while (height >= 1 || width >= 1) {
      android.opengl.GLUtils.texImage2D(GL10.GL_TEXTURE_2D, level, bitmap, 0);

      if (height == 1 && width == 1) {
        break;
      }

      height = Math.max(height / 2, 1);
      width  = Math.max(width  / 2, 1);
      Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, width, height, true);

      if (level > 0) {
        bitmap.recycle();
      }
      bitmap = bitmap2;
      level++;
    }
    if (level > 0) {
      bitmap.recycle();
    }
    //  }

    return texture;
  }

  public static void deleteTexture(GL10 gl, int texture) {
    if (texture != 0) {
      int[] ids = new int[1];
      ids[0] = texture;
      gl.glDeleteTextures(ids.length, ids, 0);
    }
  }
  
  public static Bitmap saveSurfaceBitmap(GL10 gl, int x, int y, int w, int h) {
    int b[]=new int[w*(y+h)];
    int bt[]=new int[w*h];
    IntBuffer ib=IntBuffer.wrap(b);
    ib.position(0);
    gl.glReadPixels(x, 0, w, y+h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, ib);

    for(int i=0, k=0; i<h; i++, k++) {
      for(int j=0; j<w; j++) {
        int pix=b[i*w+j];
        int pb=(pix>>16)&0xff;
        int pr=(pix<<16)&0x00ff0000;
        int pix1=(pix&0xff00ff00) | pr | pb;
        bt[(h-k-1)*w+j]=pix1;
      }
    }

    Bitmap sb=Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);
    return sb;
  }

  public static int getMaxVertexBufferSize(GL10 gl) {
    return MAX_VERTEXBUFFER_SIZE;
  }
}
