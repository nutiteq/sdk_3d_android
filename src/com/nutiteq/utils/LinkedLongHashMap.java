package com.nutiteq.utils;

/**
 * LinkedHashMap class specialized for <Code>long</Code> keys.
 *
 * @param <V>
 *          value type for the map
 * @pad.exclude          
 */
public class LinkedLongHashMap<V> {
  protected LongHashMap<LinkedEntry<V>> hashMap = new LongHashMap<LinkedEntry<V>>(100);
  protected LinkedEntry<V> first;
  protected LinkedEntry<V> last;
  protected int size;

  public void clear() {
    hashMap.clear();
    size = 0;
    first = null;
    last = null;
  }
  
  public boolean containsKey(long key) {
    return hashMap.containsKey(key);
  }
  
  public LongArrayList keys() {
    return hashMap.keys();
  }

  public void put(long key, V value) {
    LinkedEntry<V> e = new LinkedEntry<V>(key, value);
    addFirst(e);
    hashMap.put(key, e);
    while (removeEldestEntry(last.key, last.value)) {
      hashMap.remove(last.key);
      removeFromList(last);

      if (last == null) {
        break;
      }
    }
  }

  public void removeEldestEntries() {
    if (last == null) {
      return;
    }

    while (removeEldestEntry(last.key, last.value)) {
      hashMap.remove(last.key);
      removeFromList(last);

      if (last == null) {
        break;
      }
    }
  }

  public V get(long key) {
    LinkedEntry<V> entry = (LinkedEntry<V>) hashMap.get(key);
    if (entry != null) {
      removeFromList(entry);
      addFirst(entry);
      return entry.value;
    }
    return null;
  }

  public V getWithoutMod(long key) {
    LinkedEntry<V> entry = (LinkedEntry<V>) hashMap.get(key);
    return entry != null ? entry.value : null;
  }

  public void remove(long key) {
    LinkedEntry<V> e = (LinkedEntry<V>) hashMap.get(key);
    if (e != null) {
      hashMap.remove(key);
      removeFromList(e);
    }
  }

  protected void removeFromList(LinkedEntry<V> e) {
    size--;
    if (size == 0) {
      first = last = null;
    } else {
      if (e == first) {
        first = e.next;
        e.next.previous = null;
      } else if (e == last) {
        last = e.previous;
        e.previous.next = null;
      } else {
        e.next.previous = e.previous;
        e.previous.next = e.next;
      }
    }
  }

  protected void addFirst(LinkedEntry<V> e) {
    if (size == 0) {
      first = last = e;
    } else {
      e.next = first;
      first.previous = e;
      first = e;
    }
    size++;
  }

  protected boolean removeEldestEntry(long key, V value) {
    return false;
  }

  protected class LinkedEntry<K> {
    public LinkedEntry<K> next;
    public LinkedEntry<K> previous;
    public long key;
    public K value;

    public LinkedEntry(long key, K value) {
      this.key = key;
      this.value = value;
    }
  }

}
