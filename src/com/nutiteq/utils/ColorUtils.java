package com.nutiteq.utils;

/**
 * Utilities for converting colors between different representations.
 * @pad.exclude
 */
public class ColorUtils {
  private static final float RB5_TO_RB8 = 255.0f / 31.0f;
  private static final float G6_TO_G8 = 255.0f / 63.0f;

  // Encode integer id as RGB(A) color (4 floats)
  public static float[] encodeIntAsFloatColor(int val) {
    float[] color = new float[4];
    return encodeIntAsFloatColor(val, color);
  }

  // Encode integer id as RGB(A) color (4 floats)
  public static float[] encodeIntAsFloatColor(int val, float[] color) {
    color[3] = 1.0f;
    color[2] = Math.round((val & 0x1F) * RB5_TO_RB8) / 255.0f;
    color[1] = Math.round(((val >> 5) & 0x3F) * G6_TO_G8) / 255.0f;
    color[0] = Math.round(((val >> 11) & 0x1F) * RB5_TO_RB8) / 255.0f;
    return color;
  }

  // Encode integer id as RGB(A) color (4 bytes)
  public static byte[] encodeIntAsColor(int val) {
    byte[] color = new byte[4];
    return encodeIntAsColor(val, color);
  }

  // Encode integer id as RGB(A) color (4 bytes)
  public static byte[] encodeIntAsColor(int val, byte[] color) {
    color[3] = (byte) 255;
    color[2] = (byte) Math.round((val & 0x1F) * RB5_TO_RB8);
    color[1] = (byte) Math.round(((val >> 5) & 0x3F) * G6_TO_G8);
    color[0] = (byte) Math.round(((val >> 11) & 0x1F) * RB5_TO_RB8);
    return color;
  }

  // Decode integer id from encoded RGB(A) color
  public static int decodeIntFromFloatColor(float[] color) {
    int val = 0;
    val |= Math.round((color[0] * 255.0f) / RB5_TO_RB8);
    val <<= 6;
    val |= Math.round((color[1] * 255.0f) / G6_TO_G8);
    val <<= 5;
    val |= Math.round((color[2] * 255.0f) / RB5_TO_RB8);
    return val;
  }

  // Decode integer id from encoded RGB(A) color
  public static int decodeIntFromColor(byte[] color) {
    int val = 0;
    val |= Math.round((color[0] & 0xFF) / RB5_TO_RB8);
    val <<= 6;
    val |= Math.round((color[1] & 0xFF) / G6_TO_G8);
    val <<= 5;
    val |= Math.round((color[2] & 0xFF) / RB5_TO_RB8);
    return val;
  }

}
