package com.nutiteq.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Container for building direct floating point vertex buffers.
 * @pad.exclude
 */
public class FloatVertexBuffer {
  private static final int INITIAL_CAPACITY = 256 * 3;

  private int size;
  private int capacity;
  private float[] buffer;
  private int finalBufferSize;
  private FloatBuffer finalBuffer;
  
  public FloatVertexBuffer() {
    size = 0;
    capacity = 0;
    buffer = null;
    finalBufferSize = -1;
    finalBuffer = null;
  }

  public void clear() {
    finalBufferSize = -1;
    size = 0;
  }

  public void add(float x, float y) {
    if (size + 2 > capacity) {
      resize(size + 2);
    }
    buffer[size++] = x;
    buffer[size++] = y;
  }

  public void add(float x, float y, float z) {
    if (size + 3 > capacity) {
      resize(size + 3);
    }
    buffer[size++] = x;
    buffer[size++] = y;
    buffer[size++] = z;
  }
  
  public void add(float x, float y, float z, float w) {
    if (size + 4 > capacity) {
      resize(size + 4);
    }
    buffer[size++] = x;
    buffer[size++] = y;
    buffer[size++] = z;
    buffer[size++] = w;
  }
  
  public float[] getBuffer() {
    return buffer;
  }
  
  public int size() {
    return size;
  }
  
  public int capacity() {
    return capacity;
  }
  
  public void reserve(int size) {
    if (size > capacity) {
      resize(size);
    }
  }
  
  public void attach(float[] buffer, int size) {
    this.buffer = buffer;
    this.size = size;
    this.capacity = buffer.length;
    this.finalBufferSize = -1;
  }

  public FloatBuffer build(int offset, int count) {
    if (offset == 0 && count == finalBufferSize) {
      return finalBuffer;
    }
    if (offset + count > size) {
      count = size - offset;
    }
    if (finalBuffer == null || finalBuffer.capacity() < count) {
      ByteBuffer byteBuffer = ByteBuffer.allocateDirect(count * Float.SIZE / 8);
      byteBuffer.order(ByteOrder.nativeOrder());
      finalBuffer = byteBuffer.asFloatBuffer();
    }
    finalBuffer.position(0);
    if (buffer != null) {
      finalBuffer.put(buffer, offset, count);
      finalBuffer.position(0);
      finalBufferSize = (offset == 0 ? count : -1);
    }
    return finalBuffer;
  }

  protected void resize(int minCapacity) {
    int newCapacity = Math.max(INITIAL_CAPACITY, Math.max(minCapacity, capacity * 3 / 2));
    float[] newBuffer = new float[newCapacity];
    if (buffer != null) {
      System.arraycopy(buffer, 0, newBuffer, 0, size);
    }
    buffer = newBuffer;
    capacity = newCapacity;
  }
}
