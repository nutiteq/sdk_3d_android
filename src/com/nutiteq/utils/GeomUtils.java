package com.nutiteq.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.nutiteq.components.MapPos;
import com.nutiteq.components.Point3D;
import com.nutiteq.components.Vector3D;

/**
 * Utilities for line distance, polygon containment and orientation testing. 
 * @pad.exclude
 */
public class GeomUtils {

  public static interface MapPosTransformation {
    MapPos transform(MapPos src);
  }

  public static MapPos nearestPointOnLine(MapPos a, MapPos b, MapPos p) {
    double apx = p.x - a.x;
    double apy = p.y - a.y;
    double abx = b.x - a.x;
    double aby = b.y - a.y;

    double ab2 = abx * abx + aby * aby;
    double ap_ab = apx * abx + apy * aby;
    double t = Utils.toRange(ap_ab / ab2, 0, 1);

    return new MapPos(a.x + abx * t, a.y + aby * t);
  }

  public static Point3D nearestPointOnLine(Point3D a, Point3D b, Point3D p) {
    double apx = p.x - a.x;
    double apy = p.y - a.y;
    double apz = p.z - a.z;
    double abx = b.x - a.x;
    double aby = b.y - a.y;
    double abz = b.z - a.z;

    double ab2 = abx * abx + aby * aby + abz * abz;
    double ap_ab = apx * abx + apy * aby + apz * abz;
    double t = Utils.toRange(ap_ab / ab2, 0, 1);

    return new Point3D(a.x + abx * t, a.y + aby * t, a.z + abz * t);
  }

  public static boolean isPolygonClockwise(MapPos[] verts) {
    for (int i = 0; i < verts.length; i++) {
      MapPos v1 = verts[(i + verts.length - 1) % verts.length];
      MapPos v2 = verts[i];
      MapPos v3 = verts[(i + 1) % verts.length];
      double d = (v2.x - v1.x) * (v3.y - v2.y) - (v2.y - v1.y) * (v3.x - v2.x);
      if (d < 0) {
        return true;
      } else if (d > 0) {
        return false;
      }
    }
    return true;
  }

  public static boolean pointInsidePolygon(MapPos[] poly, double x, double y) {
    float c = isPolygonClockwise(poly) ? -1 : 1;
    for (int i = 0; i < poly.length; i++) {
      MapPos v1 = poly[i];
      MapPos v2 = poly[(i + 1) % poly.length];
      double d = (v2.x - v1.x) * (y - v1.y) - (v2.y - v1.y) * (x - v1.x);
      if (c * d < 0) {
        return false;
      }
    }
    return true;
  }

  public static boolean polygonsIntersect(MapPos[] poly1, MapPos[] poly2) {
    return testPointsInsidePolygonEdges(poly1, poly2) && testPointsInsidePolygonEdges(poly2, poly1);
  }

  private static boolean testPointsInsidePolygonEdges(MapPos[] poly, MapPos[] verts) {
    float c = isPolygonClockwise(poly) ? -1 : 1;
    for (int i = 0; i < poly.length; i++) {
      MapPos v1 = poly[i];
      MapPos v2 = poly[(i + 1) % poly.length];
      boolean inside = false;
      for (int j = 0; j < verts.length; j++) {
        MapPos v3 = verts[j];
        double d = (v2.x - v1.x) * (v3.y - v1.y) - (v2.y - v1.y) * (v3.x - v1.x);
        if (c * d >= 0) {
          inside = true;
          break;
        }
      }
      if (!inside) {
        return false;
      }
    }
    return true;
  }
  
  public static MapPos calculateCentroid(MapPos[] mapPoses) {
    double cx = 0, cy = 0, ar = 0;
    for (int i = 0; i < mapPoses.length; i++) {
      MapPos p0 = mapPoses[i];
      MapPos p1 = mapPoses[(i + 1) % mapPoses.length];
      double a = p0.x * p1.y - p1.x * p0.y;
      cx += (p0.x + p1.x) * a;
      cy += (p0.y + p1.y) * a;
      ar += a;
    }
    return new MapPos(cx / 3 / ar, cy / 3 / ar);
  }

  public static MapPos[] calculateConvexHull(MapPos[] points) {
    int n = points.length, k = 0;
    MapPos[] P = points.clone();
    MapPos[] H = new MapPos[n * 2];

    // Sort points lexicographically
    java.util.Arrays.sort(P, new Comparator<MapPos>() {
      @Override
      public int compare(MapPos a, MapPos b) {
        int cx = Double.compare(a.x, b.x);
        if (cx != 0) {
          return cx;
        }
        return Double.compare(a.y, b.y);
      }
    });

    // Build lower hull
    for (int i = 0; i < n; i++) {
      while (k >= 2) {
        double x1 = H[k-1].x - H[k-2].x, y1 = H[k-1].y - H[k-2].y;
        double x2 = P[i].x - H[k-2].x, y2 = P[i].y - H[k-2].y;
        if (x1 * y2 - x2 * y1 < 0) {
          break;
        }
        k--;
      }
      H[k++] = P[i];
    }

    // Build upper hull
    for (int i = n-2, t = k+1; i >= 0; i--) {
      while (k >= t) {
        double x1 = H[k-1].x - H[k-2].x, y1 = H[k-1].y - H[k-2].y;
        double x2 = P[i].x - H[k-2].x, y2 = P[i].y - H[k-2].y;
        if (x1 * y2 - x2 * y1 < 0) {
          break;
        }
        k--;
      }
      H[k++] = P[i];
    }

    // Remove consecutive duplicates
    for (int i = 0; i < k; ) {
      int j = (i + k - 1) % k;
      if (H[i].equals(H[j])) {
        for (j = i + 1; j < k; j++) {
          H[j - 1] = H[j];
        }
        k--;
      } else {
        i++;
      }
    }

    // Create result list
    MapPos[] convexHull = new MapPos[k];
    System.arraycopy(H, 0, convexHull, 0, k);
    return convexHull;
  }

  public static MapPos[] transformConvexHull(MapPos[] mapPoses, MapPosTransformation transform, double divideThreshold, double epsilon) {
    final int MAX_DEPTH = 8;
    
    // Transform all points
    MapPos[] mapPosesTrans = new MapPos[mapPoses.length];
    for (int i = 0; i < mapPoses.length; i++) {
      mapPosesTrans[i] = transform.transform(mapPoses[i]);
    }
    
    // Find extreme points, test for infinities
    double minX = Double.MAX_VALUE, maxX = -Double.MAX_VALUE;
    double minY = Double.MAX_VALUE, maxY = -Double.MAX_VALUE;
    for (MapPos p : mapPosesTrans) {
      minX = Math.min(minX, p.x);
      maxX = Math.max(maxX, p.x);
      minY = Math.min(minY, p.y);
      maxY = Math.max(maxY, p.y);
    }
    if (Double.isInfinite(minX) || Double.isInfinite(maxX) || Double.isInfinite(minY) || Double.isInfinite(maxY)) {
      return new MapPos[] { new MapPos(minX, minY), new MapPos(maxX, minY), new MapPos(maxX, maxY), new MapPos(minX, maxY) };
    }

    // Determine ordering
    double sign = isPolygonClockwise(mapPosesTrans) ? 1 : -1;

    // Start subdividing original polygon edges and add transformed points to point list
    List<MapPos> points = new ArrayList<MapPos>(mapPosesTrans.length * 2 + 1);
    for (int i = 0; i < mapPoses.length; i++) {
      int i0 = i;
      int i1 = (i + 1) % mapPoses.length;

      MapPos p0 = mapPoses[i0];
      MapPos p1 = mapPoses[i1];
      MapPos p0t = mapPosesTrans[i0];
      MapPos p1t = mapPosesTrans[i1];

      points.add(p0t);

      double dx = p1.x - p0.x;
      double dy = p1.y - p0.y;
      double len = Math.sqrt(dx * dx + dy * dy);
      int levels = (int) Math.ceil(len / divideThreshold);
      for (int k = 0; k < levels; k++) {
        double t = (double) k / (levels + 1);
        transformConvexHullEdge(transform, sign, epsilon, p0.x + t * dx, p0.y + t * dy, dx / (levels + 1), dy / (levels + 1), p0t, p1t, points, MAX_DEPTH);
      }
    }
    
    // Calculate convex hull of resulting point set (which could be concave!)
    return calculateConvexHull(points.toArray(new MapPos[points.size()]));
  }

  private static void transformConvexHullEdge(MapPosTransformation transform, double sign, double epsilon, double x0, double y0, double dx, double dy, MapPos p0t, MapPos p1t, List<MapPos> points, int depth) {
    final double DIST_COEFF = 1.2;

    if (depth <= 0) {
      return;
    }

    // If points are very close together, ignore
    double dxt = p1t.x - p0t.x;
    double dyt = p1t.y - p0t.y;
    double len = Math.sqrt(dxt * dxt + dyt * dyt);
    if (len < epsilon) {
      return;
    }

    // Test if transformed points lies on the correct side
    MapPos pt = transform.transform(new MapPos(x0 + dx, y0 + dy));
    double dist = sign * (dxt * (pt.y - p0t.y) - dyt * (pt.x - p0t.x)) / len;
    if (dist > epsilon) {
      // Move transformed point a bit further away to avoid overtesselation
      MapPos pline = nearestPointOnLine(p0t, p1t, pt);
      pt = new MapPos(pline.x + (pt.x - pline.x) * DIST_COEFF, pline.y + (pt.y - pline.y) * DIST_COEFF);

      // Retry with subdivided edges
      transformConvexHullEdge(transform, sign, epsilon * 2, x0, y0, dx * 0.5, dy * 0.5, p0t, pt, points, depth - 1);
      points.add(pt);
      transformConvexHullEdge(transform, sign, epsilon * 2, x0 + dx, y0 + dy, dx * 0.5, dy * 0.5, pt, p1t, points, depth - 1);
    }
  }

  public static Point3D transform(Point3D pos, double[] matrix) {
    double[] coords = new double[] { pos.x, pos.y, pos.z, 1, 0, 0, 0, 0 };
    Matrix.multiplyMV(coords, 4, matrix, 0, coords, 0);
    return new Point3D(coords[4] / coords[7], coords[5] / coords[7], coords[6] / coords[7]);
  }

  public static Vector3D transform(Vector3D vec, double[] matrix) {
    double[] coords = new double[] { vec.x, vec.y, vec.z, 0, 0, 0, 0, 0 };
    Matrix.multiplyMV(coords, 4, matrix, 0, coords, 0);
    return new Vector3D(coords[4], coords[5], coords[6]);
  }

}
