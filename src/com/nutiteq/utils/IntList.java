package com.nutiteq.utils;

/**
 * A list of <Code>int</Code> elements.
 * @pad.exclude
 */
public interface IntList {

  /**
   * Returns the number of elements in this list.
   * 
   * @return the number of elements in this list
   */
  int size();

  /**
   * Get element at specified at index.
   * 
   * @param index element index
   * @return element at specified index
   * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= size())
   */
  int get(int index);

  /**
   * Test if list contains given element.
   * 
   * @param value element to test for.
   * @return true if the list contains the element, false otherwise.
   */
  boolean contains(int value);

  /**
   * Find index of first occurence of the given element.
   *   
   * @param value element to find
   * @return index of first occurence of the given element or -1 if the element is not found.
   */
  int indexOf(int value);

  /**
   * Find index of last occurence of the given element.
   *   
   * @param value element to find
   * @return index of last occueance of the given element or -1 if the element is not found.
   */
  int lastIndexOf(int value);

  /**
   * Set element value at given index.
   * 
   * @param index index of the element
   * @param value value to set
   * @return previous element value
   * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= size())
   */
  int set(int index, int value);

  /**
   * Add element to the end of the list.
   * 
   * @param value element value
   */
  boolean add(int value);

  /**
   * Add element at specified index to the list.
   * 
   * @param index index for the element
   * @param value element value
   */
  void add(int index, int value);

  /**
   * Remove all elements from the list.
   */
  void clear();

  /**
   * Remove element with specified index from the list.
   * 
   * @param value value to remove
   * @return removed element
   * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= size())
   */
  int removeAt(int index);

  /**
   * Remove specified element from the list. Only the first instance of element is removed if multiple instances exist.
   * 
   * @param value value to remove
   * @return true if specified element was found and removed, false otherwise.
   */
  boolean remove(int value);

  /**
   * Convert list to array.
   * 
   * @return array of list elements.
   */
  int[] toArray();
}
