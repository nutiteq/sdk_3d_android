package com.nutiteq.nmlpackage;

import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

public class GLMaterial {

  // Material parameter that can be either constant color or texture
  private class ColorOrTexture {
    public String textureId = null;
    public GLTexture texture = null;
    public float[] color = null;

    public ColorOrTexture(NMLPackage.ColorOrTexture colorOrTexture, Map<String, GLTexture> textureMap) {
      if (colorOrTexture != null) {
        String id = colorOrTexture.getTextureId();
        if (id != null) {
          textureId = id;
          texture = textureMap.get(id);
        }
        NMLPackage.ColorRGBA c = colorOrTexture.getColor();
        if (c != null) {
          color = new float[] { c.getR(), c.getG(), c.getB(), c.getA() };
        }
      }
    }
  }

  // Material attributes
  private NMLPackage.Material.Type mType;
  private NMLPackage.Material.Culling mCulling;
  private boolean mTranslucent;
  private ColorOrTexture mDiffuse = null;
  // TODO: other attributes

  public GLMaterial(NMLPackage.Material material, Map<String, GLTexture> textureMap) {
    mType = material.getType();
    mCulling = material.getCulling();
    mDiffuse = new ColorOrTexture(material.getDiffuse(), textureMap);
    mTranslucent = material.hasTranslucent() ? material.getTranslucent() : false;
    // TODO: use other attributes
  }
  
  public NMLPackage.Material.Type getType() {
    return mType;
  }

  public void replaceTexture(String textureId, GLTexture glTexture) {
    if (mDiffuse.textureId.equals(textureId)) {
      mDiffuse.texture = glTexture;
    }
    // TODO: other attributes
  }

  public void bind(GL10 gl) {
    if (mTranslucent) {
      gl.glDepthMask(false);
    }

    if (mCulling == NMLPackage.Material.Culling.NONE) {
      gl.glDisable(GL10.GL_CULL_FACE);
    } else {
      gl.glEnable(GL10.GL_CULL_FACE);
      gl.glCullFace(mCulling == NMLPackage.Material.Culling.FRONT ? GL10.GL_FRONT : GL10.GL_BACK);
    }

    if (mDiffuse.texture != null && mDiffuse.texture.isBindable()) {
      gl.glColor4f(1, 1, 1, 1);
      gl.glEnable(GL10.GL_TEXTURE_2D);
      mDiffuse.texture.bind(gl);
    } else if (mDiffuse.color != null) {
      gl.glColor4f(mDiffuse.color[0], mDiffuse.color[1], mDiffuse.color[2], mDiffuse.color[3]);
      gl.glDisable(GL10.GL_TEXTURE_2D);
    }
    // TODO: use texture combiners for all material attributes
  }

  public void unbind(GL10 gl) {
    if (mTranslucent) {
      gl.glDepthMask(true);
    }

    if (mCulling != NMLPackage.Material.Culling.NONE) {
      gl.glDisable(GL10.GL_CULL_FACE);
    }

    if (mDiffuse.texture != null && mDiffuse.texture.isBindable()) {
      mDiffuse.texture.unbind(gl);
    } else if (mDiffuse.color != null) {
    }
  }
}
