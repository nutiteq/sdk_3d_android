package com.nutiteq.nmlpackage;

import java.util.ArrayList;
import java.util.List;

public class GLMesh {
  private float[] mMinBounds = new float[3];
  private float[] mMaxBounds = new float[3];
  GLSubmesh[] mSubmeshes;

  public GLMesh(NMLPackage.Mesh mesh) {
    // Get bounds
    NMLPackage.Vector3 minBounds = mesh.getBounds().getMin(), maxBounds = mesh.getBounds().getMax();
    mMinBounds = new float[] { minBounds.getX(), minBounds.getY(), minBounds.getZ() };
    mMaxBounds = new float[] { maxBounds.getX(), maxBounds.getY(), maxBounds.getZ() };

    // Create submeshes
    List<GLSubmesh> submeshList = new ArrayList<GLSubmesh>();
    for (NMLPackage.Submesh submesh : mesh.getSubmeshesList()) {
      GLSubmesh glSubmesh = new GLSubmesh(submesh);
      submeshList.add(glSubmesh);
    }
    mSubmeshes = submeshList.toArray(new GLSubmesh[submeshList.size()]);
  }

  public GLMesh(GLMesh glMesh, NMLPackage.MeshOp meshOp) {
    // Get bounds
    NMLPackage.Vector3 minBounds = meshOp.getBounds().getMin(), maxBounds = meshOp.getBounds().getMax();
    mMinBounds = new float[] { minBounds.getX(), minBounds.getY(), minBounds.getZ() };
    mMaxBounds = new float[] { maxBounds.getX(), maxBounds.getY(), maxBounds.getZ() };

    // Create submeshes
    List<GLSubmesh> submeshList = new ArrayList<GLSubmesh>();
    for (NMLPackage.SubmeshOpList submeshOpList : meshOp.getSubmeshOpListsList()) {
      GLSubmesh glSubmesh = new GLSubmesh(glMesh, submeshOpList);
      submeshList.add(glSubmesh);
    }
    mSubmeshes = submeshList.toArray(new GLSubmesh[submeshList.size()]);
  }

  public float[] getMinBounds() {
    return mMinBounds.clone();
  }

  public float[] getMaxBounds() {
    return mMaxBounds.clone();
  }

  public GLSubmesh[] getSubmeshes() {
    return mSubmeshes;
  }

  public int getTotalGeometrySize() {
    if (mSubmeshes == null)
      return 0;
    int size = 0;
    for (GLSubmesh submesh : mSubmeshes) {
      size += submesh.getTotalGeometrySize();
    }
    return size;
  }

}
