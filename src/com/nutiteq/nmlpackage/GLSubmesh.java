package com.nutiteq.nmlpackage;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import com.google.protobuf.ByteString;
import com.nutiteq.utils.ColorUtils;

public class GLSubmesh {
  private static final int MAX_BATCH_SIZE = 65535;

  private int mGLType = -1;
  private int[] mVertexCounts;
  private String mMaterialId;

  private FloatBuffer mPositionBuffer;
  private FloatBuffer mNormalBuffer;
  private FloatBuffer mUVBuffer;
  private ByteBuffer mColorBuffer;
  private ByteBuffer mVertexIdBuffer;
  
  private static ByteBuffer allocateDirectByteBuffer(int size) {
    ByteBuffer dest = null;
    try {
      dest = ByteBuffer.allocateDirect(size);
    }
    catch (OutOfMemoryError error) {
      System.gc(); // try to collect garbage and then retry
      dest = ByteBuffer.allocateDirect(size);
    }
    return dest.order(ByteOrder.nativeOrder());
  }
  
  private FloatBuffer allocateDirectFloatBuffer(int size) {
    return allocateDirectByteBuffer(size * Float.SIZE / 8).asFloatBuffer();
  }
  
  private static ByteBuffer finalizeBuffer(ByteBuffer buffer) {
    if (buffer.position() == 0) {
      return null;
    }
    buffer.position(0);
    return buffer;
  }
  
  private static FloatBuffer finalizeBuffer(FloatBuffer buffer) {
    if (buffer.position() == 0) {
      return null;
    }
    buffer.position(0);
    return buffer;
  }
  
  private static byte[] getBufferSlice(ByteBuffer src, int offset, int count) {
    src.position(offset);
    byte[] data = new byte[count];
    src.get(data, 0, count);
    src.position(0);
    return data;
  }

  private static float[] getBufferSlice(FloatBuffer src, int offset, int count) {
    src.position(offset);
    float[] data = new float[count];
    src.get(data, 0, count);
    src.position(0);
    return data;
  }

  // Convert ProtoBuf big-endian floating point buffer to native
  // direct-allocated buffer that can be used as a GL vertex array
  private static FloatBuffer convertToDirectFloatBuffer(ByteString bstr) {
    if (bstr == null)
      return null;
    if (bstr.size() == 0)
      return null;
    FloatBuffer src = bstr.asReadOnlyByteBuffer().order(ByteOrder.LITTLE_ENDIAN).asFloatBuffer();
    FloatBuffer dest = allocateDirectByteBuffer(bstr.size()).asFloatBuffer();
    dest.put(src);
    dest.position(0);
    return dest;
  }

  private static ByteBuffer convertToDirectByteBuffer(ByteString bstr) {
    if (bstr == null)
      return null;
    if (bstr.size() == 0)
      return null;
    ByteBuffer src = bstr.asReadOnlyByteBuffer().order(ByteOrder.LITTLE_ENDIAN);
    ByteBuffer dest = allocateDirectByteBuffer(bstr.size());
    dest.put(src);
    dest.position(0);
    return dest;
  }
  
  private static int convertType(NMLPackage.Submesh.Type type) {
    int glType = -1;
    switch (type) {
    case POINTS:
      glType = GL10.GL_POINTS;
      break;
    case LINES:
      glType = GL10.GL_LINES;
      break;
    case LINE_STRIPS:
      glType = GL10.GL_LINE_STRIP;
      break;
    case TRIANGLES:
      glType = GL10.GL_TRIANGLES;
      break;
    case TRIANGLE_STRIPS:
      glType = GL10.GL_TRIANGLE_STRIP;
      break;
    case TRIANGLE_FANS:
      glType = GL10.GL_TRIANGLE_FAN;
      break;
    default:
      throw new RuntimeException("Unsupported submesh type");
    }
    return glType;
  }

  public enum DrawMode {
    DRAW_NORMAL, DRAW_VERTEX_IDS, DRAW_CONST
  };

  public GLSubmesh(NMLPackage.Submesh submesh) {
    mGLType = convertType(submesh.getType());

    // Copy basic attributes
    int totalVertexCount = 0;
    mVertexCounts = new int[submesh.getVertexCountsCount()];
    for (int i = 0; i < submesh.getVertexCountsCount(); i++) {
      mVertexCounts[i] = submesh.getVertexCounts(i);
      totalVertexCount += mVertexCounts[i];
    }
    mMaterialId = submesh.getMaterialId();

    // Create vertex buffers
    mPositionBuffer = convertToDirectFloatBuffer(submesh.getPositions());
    mNormalBuffer = convertToDirectFloatBuffer(submesh.getNormals());
    mUVBuffer = convertToDirectFloatBuffer(submesh.getUvs());
    mColorBuffer = convertToDirectByteBuffer(submesh.getColors());
    mVertexIdBuffer = allocateDirectByteBuffer(totalVertexCount * 4 * Byte.SIZE / 8);
    for (int i = 0; i < submesh.getVertexIdsCount(); i++) {
      int count = (int) (submesh.getVertexIds(i) >> 32);
      int id = (int) (submesh.getVertexIds(i) & (((long) 1 << 32) - 1));
      byte[] color = ColorUtils.encodeIntAsColor(id);
      byte[] vertexIdBuffer = new byte[4 * count]; // it is much faster to fill array on Java side and then call put method only once 
      for (int j = 0; j < vertexIdBuffer.length; j++) {
        vertexIdBuffer[j] = color[j & 3];
      }
      mVertexIdBuffer.put(vertexIdBuffer);
    }
    mVertexIdBuffer.position(0);
  }
  
  public GLSubmesh(GLMesh glMesh, NMLPackage.SubmeshOpList submeshOpList) {
    mGLType = convertType(submeshOpList.getType());
    mMaterialId = submeshOpList.getMaterialId();
    
    int vertexCount = 0;
    for (NMLPackage.SubmeshOp submeshOp : submeshOpList.getSubmeshOpsList()) {
      vertexCount += submeshOp.getCount();
    }
    mVertexCounts = new int[] { vertexCount };
    
    mPositionBuffer = allocateDirectFloatBuffer(vertexCount * 3);
    mNormalBuffer = allocateDirectFloatBuffer(vertexCount * 3);
    mColorBuffer = allocateDirectByteBuffer(vertexCount * 4);
    mVertexIdBuffer = allocateDirectByteBuffer(vertexCount * 4);
    mUVBuffer = allocateDirectFloatBuffer(vertexCount * 2);
    for (NMLPackage.SubmeshOp submeshOp : submeshOpList.getSubmeshOpsList()) {
      GLSubmesh glSubmesh = glMesh.getSubmeshes()[submeshOp.getSubmeshIdx()];
      synchronized (glSubmesh) {
        mPositionBuffer.put(getBufferSlice(glSubmesh.mPositionBuffer, submeshOp.getOffset() * 3, submeshOp.getCount() * 3));
        
        if (glSubmesh.mNormalBuffer != null) {
          mNormalBuffer.put(getBufferSlice(glSubmesh.mNormalBuffer, submeshOp.getOffset() * 3, submeshOp.getCount() * 3));
        }
        if (glSubmesh.mColorBuffer != null) {
          mColorBuffer.put(getBufferSlice(glSubmesh.mColorBuffer, submeshOp.getOffset() * 4, submeshOp.getCount() * 4));
        }
        if (glSubmesh.mVertexIdBuffer != null) {
          mVertexIdBuffer.put(getBufferSlice(glSubmesh.mVertexIdBuffer, submeshOp.getOffset() * 4, submeshOp.getCount() * 4));
        }
        
        if (glSubmesh.mUVBuffer != null) {
          float[] data = getBufferSlice(glSubmesh.mUVBuffer, submeshOp.getOffset() * 2, submeshOp.getCount() * 2);
          for (int i = 0; i + 1 < data.length; i += 2) {
            data[i + 0] = data[i + 0] * submeshOp.getTexUScale() + submeshOp.getTexUTrans();
            data[i + 1] = data[i + 1] * submeshOp.getTexVScale() + submeshOp.getTexVTrans();
          }
          mUVBuffer.put(data);
        }
      }
    }
    mPositionBuffer.position(0);
    mNormalBuffer = finalizeBuffer(mNormalBuffer);
    mColorBuffer = finalizeBuffer(mColorBuffer);
    mVertexIdBuffer = finalizeBuffer(mVertexIdBuffer);
    mUVBuffer = finalizeBuffer(mUVBuffer);
  }

  public synchronized void draw(GL10 gl, DrawMode drawMode) {
    if (mVertexCounts == null)
      return;

    // Enable vertex buffers
    if (mPositionBuffer != null) {
      gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
      gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mPositionBuffer);
    } else {
      gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }
    if (mNormalBuffer != null) {
      gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
      gl.glNormalPointer(GL10.GL_FLOAT, 0, mNormalBuffer);
    } else {
      gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);
    }
    if (mUVBuffer != null) {
      gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
      gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mUVBuffer);
    } else {
      gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    }
    if (drawMode == DrawMode.DRAW_NORMAL && mColorBuffer != null) {
      gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
      gl.glColorPointer(4, GL10.GL_UNSIGNED_BYTE, 0, mColorBuffer);
    } else if (drawMode == DrawMode.DRAW_VERTEX_IDS && mVertexIdBuffer != null) {
      gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
      gl.glColorPointer(4, GL10.GL_UNSIGNED_BYTE, 0, mVertexIdBuffer);
    } else {
      gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
    }

    // Draw primitives
    int idx = 0;
    for (int count : mVertexCounts) {
      for (int offset = 0; offset < count; offset += MAX_BATCH_SIZE) {
        int batch = Math.min(MAX_BATCH_SIZE, count - offset);
        gl.glDrawArrays(mGLType, idx + offset, batch);
      }
      idx += count;
    }
  }

  public String getMaterialId() {
    return mMaterialId;
  }

  public synchronized int getTotalGeometrySize() {
    int size = 0;
    if (mPositionBuffer != null) {
      size += mPositionBuffer.capacity() * Float.SIZE / 8;
    }
    if (mNormalBuffer != null) {
      size += mNormalBuffer.capacity() * Float.SIZE / 8;
    }
    if (mUVBuffer != null) {
      size += mUVBuffer.capacity() * Float.SIZE / 8;
    }
    if (mColorBuffer != null) {
      size += mColorBuffer.capacity() * Byte.SIZE / 8;
    }
    if (mVertexIdBuffer != null) {
      size += mVertexIdBuffer.capacity() * Byte.SIZE / 8;
    }
    return size;
  }

}
