package com.nutiteq.nmlpackage;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.microedition.khronos.opengles.GL10;

import android.annotation.TargetApi;
import android.opengl.ETC1Util;
import android.opengl.GLES10;

import com.google.protobuf.ByteString;

@TargetApi(8)
public class GLTexture {
  private NMLPackage.Texture mNMLTexture = null;
  private int mGLTextureId = -1;
  private int mSize = 0;

  private int getSamplerWrapMode(NMLPackage.Sampler.WrapMode wrapMode) {
    switch (wrapMode) {
    case CLAMP:
      return GL10.GL_CLAMP_TO_EDGE;
    default:
      return GL10.GL_REPEAT; // ignore MIRROR, etc
    }
  }

  private void updateSampler(GL10 gl, NMLPackage.Sampler sampler, boolean complete) {
    switch (sampler.getFilter()) {
    case NEAREST:
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST_MIPMAP_NEAREST);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_NEAREST);
      break;
    case BILINEAR:
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
      break;
    case TRILINEAR:
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, complete ? GL10.GL_LINEAR_MIPMAP_LINEAR : GL10.GL_LINEAR);
      gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
      break;
    }
    gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, getSamplerWrapMode(sampler.getWrapS()));
    gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, getSamplerWrapMode(sampler.getWrapT()));
  }

  private void updateMipLevel(GL10 gl, int level, NMLPackage.Texture texture) {
    int glFormat = -1;
    ByteString glTextureData = texture.getMipmaps(level);
    switch (texture.getFormat()) {
    case LUMINANCE8:
      glFormat = GL10.GL_LUMINANCE;
      break;
    case RGB8:
      glFormat = GL10.GL_RGB;
      break;
    case RGBA8:
      glFormat = GL10.GL_RGBA;
      break;
    case ETC1:
      glFormat = GL10.GL_RGB;
      try {
        ETC1Util.ETC1Texture etc1Tex = ETC1Util.createTexture(glTextureData.newInput()); 
        ETC1Util.loadTexture(GLES10.GL_TEXTURE_2D, level, 0, GLES10.GL_RGB, GLES10.GL_UNSIGNED_SHORT_5_6_5, etc1Tex);
      }
      catch (IOException ex) {
        throw new RuntimeException("Failed to load ETC1 texture");
      }
      glTextureData = null;
      break;
    default:
      throw new RuntimeException("Unsupported texture format (PVRTC?)");
    }
    if (glTextureData != null) {
      ByteBuffer bb = ByteBuffer.allocateDirect(glTextureData.size());
      bb.order(ByteOrder.nativeOrder());
      bb.put(glTextureData.asReadOnlyByteBuffer());
      bb.position(0);
      int width = Math.max(1, texture.getWidth() >> level);
      int height = Math.max(1, texture.getHeight() >> level);
      if (glTextureData.size() > 0) {
        gl.glTexImage2D(GL10.GL_TEXTURE_2D, level, glFormat, width, height, 0, glFormat, GL10.GL_UNSIGNED_BYTE, bb);
      }
    }
  }
  
  private void updateMipMaps(GL10 gl, NMLPackage.Texture texture) {
    mSize = 0;
    for (int i = 0; i < texture.getMipmapsCount(); i++) {
      updateMipLevel(gl, i, texture);
      mSize += texture.getMipmaps(i).size();
    }
  }
  
  public GLTexture(NMLPackage.Texture texture) {
    mNMLTexture = texture;
  }

  public void build(GL10 gl) {
    if (mGLTextureId == -1) {
      int[] ids = new int[1];
      gl.glGenTextures(1, ids, 0);
      mGLTextureId = ids[0];
    }
    gl.glBindTexture(GL10.GL_TEXTURE_2D, mGLTextureId);
    updateMipMaps(gl, mNMLTexture);
    boolean complete = false;
    if (mNMLTexture.getMipmapsCount() > 1) {
      complete = 2 * Math.max(mNMLTexture.getWidth(), mNMLTexture.getHeight()) == 1 << mNMLTexture.getMipmapsCount();
    }
    updateSampler(gl, mNMLTexture.getSampler(), complete);
  }

  public void dispose(GL10 gl) {
    if (mGLTextureId != -1) {
      int[] ids = new int[] { mGLTextureId };
      gl.glDeleteTextures(1, ids, 0);
    }
    mNMLTexture = null;
    mGLTextureId = -1;
    mSize = 0;
  }

  public boolean isBindable() {
    return mGLTextureId != -1;
  }
  
  public void bind(GL10 gl) {
    if (mGLTextureId != -1) {
      gl.glBindTexture(GL10.GL_TEXTURE_2D, mGLTextureId);
    }
  }

  public void unbind(GL10 gl) {
  }

  public int getTotalTextureSize() {
    return mSize;
  }

}
