package com.nutiteq.nmlpackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

public class GLModel {
  private float[] mMinBounds = new float[3];
  private float[] mMaxBounds = new float[3];
  private Map<String, GLMesh> mMeshMap = new HashMap<String, GLMesh>();
  private Map<String, GLTexture> mTextureMap = new HashMap<String, GLTexture>();
  GLMeshInstance[] mMeshInstances;

  public GLModel(NMLPackage.Model model) {
    // Update bounds
    NMLPackage.Vector3 minBounds = model.getBounds().getMin(), maxBounds = model.getBounds().getMax();
    mMinBounds = new float[] { minBounds.getX(), minBounds.getY(), minBounds.getZ() };
    mMaxBounds = new float[] { maxBounds.getX(), maxBounds.getY(), maxBounds.getZ() };

    // Build map from texture ids to GL textures
    for (NMLPackage.Texture texture : model.getTexturesList()) {
      GLTexture glTexture = new GLTexture(texture);
      mTextureMap.put(texture.getId(), glTexture);
    }

    // Build map from meshes to GL mesh objects
    for (NMLPackage.Mesh mesh : model.getMeshesList()) {
      GLMesh glMesh = new GLMesh(mesh);
      mMeshMap.put(mesh.getId(), glMesh);
    }

    // Create mesh instances
    List<GLMeshInstance> meshInstanceList = new ArrayList<GLMeshInstance>();
    for (NMLPackage.MeshInstance meshInstance : model.getMeshInstancesList()) {
      GLMeshInstance glMeshInstance = new GLMeshInstance(meshInstance, mMeshMap, mTextureMap);
      meshInstanceList.add(glMeshInstance);
    }
    mMeshInstances = meshInstanceList.toArray(new GLMeshInstance[meshInstanceList.size()]);
  }

  public void replaceMesh(String id, GLMesh glMesh) {
    if (mMeshMap.get(id) == glMesh)
      return;
    mMeshMap.put(id, glMesh);
    if (mMeshInstances == null)
      return;
    for (GLMeshInstance meshInstance : mMeshInstances) {
      meshInstance.replaceMesh(id, glMesh);
    }
  }

  public void replaceTexture(String id, GLTexture glTexture) {
    if (mTextureMap.get(id) == glTexture)
      return;
    mTextureMap.put(id, glTexture);
    if (mMeshInstances == null)
      return;
    for (GLMeshInstance meshInstance : mMeshInstances) {
      meshInstance.replaceTexture(id, glTexture);
    }
  }
  
  public void draw(GL10 gl, GLSubmesh.DrawMode mode) {
    if (mMeshInstances == null)
      return;
    for (GLMeshInstance meshInstance : mMeshInstances) {
      meshInstance.draw(gl, mode);
    }
  }

  public float[] getMinBounds() {
    return mMinBounds.clone();
  }

  public float[] getMaxBounds() {
    return mMaxBounds.clone();
  }

  public int getTotalGeometrySize() {
    int size = 0;
    for (GLMesh mesh : mMeshMap.values()) {
      size += mesh.getTotalGeometrySize();
    }
    return size;
  }

  public int getTotalTextureSize() {
    int size = 0;
    for (GLTexture texture : mTextureMap.values()) {
      size += texture.getTotalTextureSize();
    }
    return size;
  }
}
