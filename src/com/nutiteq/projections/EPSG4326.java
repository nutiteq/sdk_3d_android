package com.nutiteq.projections;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;

/**
 * EPSG:4326 is WGS84, unprojected.
 * Z-coordinate denotes height in meters.
 */
public class EPSG4326 extends Projection {
  private static final Bounds BOUNDS = new Bounds(-180, 90, 180, -90);

  /**
   * Default constructor.
   */
  public EPSG4326() {
  }

  @Override
  public Bounds getBounds() {
    return BOUNDS;
  }

  @Override
  public MapPos fromWgs84(double lon, double lat) {
    /*
    double x = lon % 360;
    if (x > 180) {
      x -= 360;
    } else if (x < -180) {
      x += 360;
    }
    */
    double x = lon;
    double y = lat;
    return new MapPos(x, y);
  }

  @Override
  public MapPos toWgs84(double x, double y) {
    /*
    double lon = x % 360;
    if (lon > 180) {
      lon -= 360;
    } else if (lon < -180) {
      lon += 360;
    }
    */
    double lon = x;
    if (lon < -180) {
      lon = -180;
    } else if (lon > 180) {
      lon = 180;
    }
    double lat = y;
    return new MapPos(lon, lat);
  }

  @Override
  public void fromInternal(double u, double v, double h, MutableMapPos pos) {
    pos.setCoords(u, v, h);
  }
  
  @Override
  public Envelope fromInternal(Envelope envInternal) {
    return envInternal;
  }

  @Override
  public void toInternal(double x, double y, double z, MutableMapPos posInternal) {
    posInternal.setCoords(x, y, z);
  }

  @Override
  public Envelope toInternal(Envelope env) {
    return env;
  }

  @Override
  public double[] getInternalFrameMatrix(double u, double v, double h) {
    double scale = 1 / Math.cos(v * (Math.PI / 180));

    double[] transform = new double[16];
    transform[0]  = scale;
    transform[5]  = 1.0;
    transform[10] = 1.0 / 360.0;
    transform[15] = 1;
    return transform;
  }

  @Override
  public String name() {
    return "EPSG:4326";   
  }

  @Override
  public int hashCode() {
    return name().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    return true;
  }
}
