package com.nutiteq.projections;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;
import com.nutiteq.utils.GeomUtils;

/**
 * EPSG:3857 is a Spherical Mercator projection coordinate system popularized by web services such as Google and later OpenStreetMap.
 * Z-coordinate denotes height, in meters.
 */
public class EPSG3857 extends Projection {
  private static final double SCALE = Math.PI * 6378137;
  private static final Bounds BOUNDS = new Bounds(-SCALE, SCALE, SCALE, -SCALE);
  private static final double DIVIDE_THRESHOLD = 2 * SCALE / 10; // approx number of meters for each additional tesselation level when calculating envelope
  private static final double INTERNAL_DIVIDE_EPSILON = 1.0e-8; // smaller errors in envelope calculations are ignored

  private final GeomUtils.MapPosTransformation toInternalTransform = new GeomUtils.MapPosTransformation() {
    @Override
    public MapPos transform(MapPos src) {
      return toInternal(src.x, src.y);
    }    
  };

  private final GeomUtils.MapPosTransformation fromInternalTransform = new GeomUtils.MapPosTransformation() {
    @Override
    public MapPos transform(MapPos src) {
      return fromInternal(src.x, src.y);
    }    
  };

  /**
   * Default constructor.
   */
  public EPSG3857() {
  }

  @Override
  public Bounds getBounds() {
    return BOUNDS;
  }

  @Override
  public MapPos fromWgs84(double lon, double lat) {
    /*
    double x = (lon % 360) / 180 * SCALE;
    if (x > SCALE) {
      x -= 2 * SCALE;
    } else if (x < -SCALE) {
      x += 2 * SCALE;
    }
    */
    double x = lon * (SCALE / 180);
    double y = Math.log(Math.max(0, Math.tan(lat * (Math.PI / 2 / 180) + Math.PI / 4))) * (SCALE / Math.PI);
    return new MapPos(x, y);
  }

  @Override
  public MapPos toWgs84(double x, double y) {
    /*
    double lon = (x / SCALE * 180) % 360;
    if (lon > 180) {
      lon -= 360;
    } else if (lon < -180) {
      lon += 360;
    }
    */
    double lon = x * (180 / SCALE);
    if (lon < -180) {
      lon = -180;
    } else if (lon > 180) {
      lon = 180;
    }
    double lat = (180 * 2 / Math.PI) * (Math.atan(Math.exp(y * (Math.PI / SCALE))) - Math.PI / 4);
    return new MapPos(lon, lat);
  }

  @Override
  public void fromInternal(double u, double v, double h, MutableMapPos pos) {
    double x = u * (SCALE / 180);
    double y = Math.log(Math.max(0, Math.tan(v * (Math.PI / 2 / 180) + Math.PI / 4))) * (SCALE / Math.PI);
    double z = h;
    pos.setCoords(x, y, z);
  }

  @Override
  public Envelope fromInternal(Envelope envInternal) {
    // Note: we do not need to tesselate when converting from WGS84 as dy(v1)/dv > dy(v2)/dv if v1 > v2 and u transformation is linear transformation
    MapPos[] hullPoints = GeomUtils.transformConvexHull(envInternal.getConvexHull(), fromInternalTransform, Double.POSITIVE_INFINITY, 0);
    return new Envelope(hullPoints);
  }

  @Override
  public void toInternal(double x, double y, double z, MutableMapPos pos) {
    double u = x * (180 / SCALE);
    double v = (180 * 2 / Math.PI) * (Math.atan(Math.exp(y * (Math.PI / SCALE))) - Math.PI / 4);
    double h = z;
    pos.setCoords(u, v, h);
  }

  @Override
  public Envelope toInternal(Envelope env) {
    // Test envelope - if points are very close to pole, use bounding box of the envelope.
    // Otherwise very fine tesselation of the envelope would be required.
    if (env.minY < -SCALE * 0.9 || env.maxY > SCALE * 0.9) {
      MutableMapPos minPos = new MutableMapPos();
      MutableMapPos maxPos = new MutableMapPos();
      toInternal(env.minX, env.minY, 0, minPos);
      toInternal(env.maxX, env.maxY, 0, maxPos);
      return new Envelope(minPos.x, maxPos.x, minPos.y, maxPos.y);
    }

    // Use general convex hull transformation algorithm
    MapPos[] hullPoints = GeomUtils.transformConvexHull(env.getConvexHull(), toInternalTransform, DIVIDE_THRESHOLD, INTERNAL_DIVIDE_EPSILON);
    return new Envelope(hullPoints);
  }

  @Override
  public double[] getInternalFrameMatrix(double u, double v, double h) {
    double scale = 1 / Math.cos(v * (Math.PI / 180));

    double[] transform = new double[16];
    transform[0]  = scale * (360.0 / SCALE / 2);
    transform[5]  = scale * (360.0 / SCALE / 2);
    transform[10] = scale;
    transform[15] = 1;
    return transform;
  }

  @Override
  public String name() {
    return "EPSG:3857";	  
  }
  
  @Override
  public int hashCode() {
    return name().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    return true;
  }
}
