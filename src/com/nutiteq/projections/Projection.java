package com.nutiteq.projections;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableMapPos;

/**
 * Abstract base class for all projections.
 */
public abstract class Projection {

  /**
   * Get bounds of this projection.
   * 
   * @return projection bounds specified when projection was constructed.
   */
  public abstract Bounds getBounds();

  /**
   * Transform point represented in WGS84 coordinates to coordinate system of current projection.
   * 
   * @param lon
   *        longitude in WGS84 system (-180..180)
   * @param lat
   *        latitude in WGS84 system (-90..90)
   * @return point coordinates in the coordinate system of current projection 
   */
  public abstract MapPos fromWgs84(double lon, double lat);

  /**
   * Transform point in coordinate system of this projection to WGS84 coordinate system.
   * 
   * @param x
   *        first coordinate
   * @param y
   *        second coordinate
   * @return point coordinates in WGS84 coordinate system
   */
  public abstract MapPos toWgs84(double x, double y);

  /**
   * Transform point from internal coordinates to coordinate system of current projection.
   * 
   * @param u
   *        first coordinate (-180..180)
   * @param v
   *        second coordinate (-90..90)
   * @param h
   *        third coordinate
   * @param pos
   *        transformed point
   *        
   * @pad.exclude
   */
  public abstract void fromInternal(double u, double v, double h, MutableMapPos pos);
  
  /**
   * Transform envelope from internal coordinates to coordinate system of current projection.
   * 
   * @param envInternal
   *        envelope in internal coordinate system
   * @return transformed envelope
   *        
   * @pad.exclude
   */
  public abstract Envelope fromInternal(Envelope envInternal);
  
  /**
   * Transform point from internal coordinates to coordinate system of current projection.
   * 
   * @param posInternal
   *        point in internal coordinates
   * @return transformed point
   *        
   * @pad.exclude
   */
  public MapPos fromInternal(MapPos posInternal) {
    MutableMapPos pos = new MutableMapPos();
    fromInternal(posInternal.x, posInternal.y, posInternal.z, pos);
    return new MapPos(pos);
  }

  /**
   * Transform point from internal coordinates to coordinate system of current projection.
   * 
   * @param u
   *        first coordinate (-180..180)
   * @param v
   *        second coordinate (-90..90)
   * @return transformed point
   *        
   * @pad.exclude
   */
  public MapPos fromInternal(double u, double v) {
    MutableMapPos pos = new MutableMapPos();
    fromInternal(u, v, 0, pos);
    return new MapPos(pos.x, pos.y);
  }

  /**
   * Transform point in coordinate system of this projection to internal coordinate system.
   * 
   * @param x
   *        first coordinate
   * @param y
   *        second coordinate
   * @param z
   *        third coordinate (height)
   * @param posInternal
   *        transformed point
   * 
   * @pad.exclude
   */
  public abstract void toInternal(double x, double y, double z, MutableMapPos posInternal);

  /**
   * Transform envelope in coordinate system of this projection to internal coordinate system.
   * 
   * @param env
   *        source envelope
   * @return envelope in internal coordinate system
   * 
   * @pad.exclude
   */
  public abstract Envelope toInternal(Envelope env);

  /**
   * Transform point in coordinate system of this projection to internal coordinate system.
   * 
   * @param pos
   *        point in coordinate system of this projection
   * @return transformed point in internal coordinates
   * 
   * @pad.exclude
   */
  public MapPos toInternal(MapPos pos) {
    MutableMapPos posInternal = new MutableMapPos();
    toInternal(pos.x, pos.y, pos.z, posInternal);
    return new MapPos(posInternal);
  }
  
  /**
   * Transform point in coordinate system of this projection to internal coordinate system.
   * 
   * @param x
   *        first coordinate
   * @param y
   *        second coordinate
   * @param z
   *        third coordinate (height)
   * @param wgsPos
   *        transformed point
   * 
   * @pad.exclude
   */
  public MapPos toInternal(double x, double y) {
    MutableMapPos posInternal = new MutableMapPos();
    toInternal(x, y, 0, posInternal);
    return new MapPos(posInternal.x, posInternal.y);
  }
  
  /**
   * Get internal transformation matrix for given point.
   * 
   * @param u
   *        first coordinate
   * @param v
   *        second coordinate
   * @param h
   *        third coordinate (height)
   * @return 4x4 transformation matrix encoded in row-major format
   * 
   * @pad.exclude
   */
  public abstract double[] getInternalFrameMatrix(double u, double v, double h);

  /**
   * Get name of the projection, usually "EPSG:xxxx".
   * Should be redefined by each actual projection!
   * 
   * @return name of the projection
   */
  public abstract String name();
}
