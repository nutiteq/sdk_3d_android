package com.nutiteq.log;

/**
 * Log handler used by library. Supports insertion of custom handlers. By
 * default all logging is disabled. All disabled log levels will be removed by
 * obfuscator and also the code required for log message will be gone.
 */
public class Log {
  private static final String DEFAULT_TAG = "3dLib";
  private static String tag = DEFAULT_TAG;
  private static boolean showError;
  private static boolean showInfo;
  private static boolean showDebug;
  private static boolean showWarning;

  /**
   * Set tag for logging. Tag can be used for filtering log messages.
   * 
   * @param tag
   *          tag for the log.
   */
  public static void setTag(String tag) {
    Log.tag = tag;
  }

  /**
   * Log an error.
   * 
   * @param message
   *          message to log.
   */
  public static void error(String message) {
    if (showError) {
      android.util.Log.e(tag, message);
    }
  }

  /**
   * Log a warning, a possible problem
   * 
   * @param message
   *          message to log.
   */
  public static void warning(String message) {
    if (showWarning) {
      android.util.Log.w(tag, message);
    }
    
  }

  
  /**
   * Log an info message.
   * 
   * @param message
   *          message to log.
   */
  public static void info(String message) {
    if (showInfo) {
      android.util.Log.i(tag, message);
    }
  }

  /**
   * Log a debug message.
   * 
   * @param message
   *          message to log.
   */
  public static void debug(final String message) {
    if (showDebug) {
      android.util.Log.d(tag, message);
    }
  }

  /**
   * Enable or disable error logging.
   * 
   * @param showError
   *          flag specifying whether to enable or disable error logging.
   */
  public static void setShowError(boolean showError) {
    Log.showError = showError;
  }

  /**
   * Enable or disable logging of info messages.
   * 
   * @param showInfo
   *          flag specifying whether to enable or disable logging of info messages.
   */
  public static void setShowInfo(boolean showInfo) {
    Log.showInfo = showInfo;
  }

  /**
   * Enable or disable logging of debug messages.
   * 
   * @param showDebug
   *          flag specifying whether to enable or disable logging of debug messages.
   */
  public static void setShowWarning(boolean showDebug) {
    Log.showDebug = showDebug;
  }
  
  /**
   * Enable or disable logging of debug messages.
   * 
   * @param showDebug
   *          flag specifying whether to enable or disable logging of debug messages.
   */
  public static void setShowDebug(boolean showDebug) {
    Log.showDebug = showDebug;
  }

  /**
   * Enable logging of error, info and debug messages.
   */
  public static void enableAll() {
    showError = true;
    showInfo = true;
    showDebug = true;
    showWarning = true;
  }

}
