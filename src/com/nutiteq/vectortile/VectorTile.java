package com.nutiteq.vectortile;

import java.util.ArrayList;
import java.util.List;

import com.nutiteq.components.MapTile;
import com.nutiteq.vectorlayers.BillBoardLayer;
import com.nutiteq.vectorlayers.GeometryLayer;
import com.nutiteq.vectorlayers.VectorLayer;

/**
 * Vector tile container.
 * 
 * @pad.exclude
 */
public class VectorTile {
  private final MapTile mapTile;
  private final List<GeometryLayer> geometryLayers;
  private final List<BillBoardLayer<?>> billBoardLayers;

  /**
   * Default constructor.
   * 
   * @param mapTile
   *          tile coordinates
   * @param vectorLayers
   *          list of layers for this tile
   */
  public VectorTile(MapTile mapTile, List<VectorLayer<?>> vectorLayers) {
    this.mapTile = mapTile;
    this.geometryLayers = new ArrayList<GeometryLayer>();
    this.billBoardLayers = new ArrayList<BillBoardLayer<?>>();
    for (VectorLayer<?> layer : vectorLayers) {
      if (layer instanceof GeometryLayer) {
        geometryLayers.add((GeometryLayer) layer);
      } else if (layer instanceof BillBoardLayer<?>) {
        billBoardLayers.add((BillBoardLayer<?>) layer);
      }
    }
  }

  /**
   * Get current map tile coordinates.
   * 
   * @return map tile coordinates
   */
  public MapTile getMapTile() {
    return mapTile;
  }

  /**
   * Get list of geometry layers for this tile.
   * 
   * @return list of geometry layers
   */
  public List<GeometryLayer> getGeometryLayers() {
    return geometryLayers;
  }

  /**
   * Get list of billboard layers for this tile.
   * 
   * @return list of billboard layers
   */
  public List<BillBoardLayer<?>> getBillBoardLayers() {
    return billBoardLayers;
  }

}
