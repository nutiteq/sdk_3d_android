package com.nutiteq.vectortile;

import com.nutiteq.components.Bounds;
import com.nutiteq.components.MapTile;
import com.nutiteq.components.TileData;

/**
 * Reader class for vector tiles.
 */
public interface VectorTileReader {
  /**
   * Get minimum zoom level supported by the reader.
   * 
   * @return minimum zoom level
   */
  int getMinZoom();

  /**
   * Get maximum zoom level supported by the reader.
   * 
   * @return maximum zoom level
   */
  int getMaxZoom();
  
  /**
   * Create tile for tile coordinates and tile data.
   * 
   * @param tile
   *          source tile coordinates
   * @param tileData
   *          source tile data 
   * @param targetTile
   *          target tile coordinates
   * @param targetBounds
   *          target tile bounds (in data source projection coordinates)
   * @return target vector tile
   */
  VectorTile createTile(MapTile tile, TileData tileData, MapTile targetTile, Bounds targetBounds);
}
