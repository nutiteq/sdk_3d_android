package com.nutiteq.license;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Typeface;

import com.nutiteq.components.Color;

/**
 * License watermark bitmap description.
 * @pad.exclude
 */
public class Watermark {
  private Bitmap bitmap;
  private final String text;
  public final float xPos;
  public final float yPos;
  public final float scale;

  public Watermark(Bitmap bitmap, float xPos, float yPos, float scale) {
    this.bitmap = bitmap;
    this.text = null;
    this.xPos = xPos;
    this.yPos = yPos;
    this.scale = scale;
  }

  public Watermark(String text, float xPos, float yPos, float scale) {
    this.bitmap = null;
    this.text = text;
    this.xPos = xPos;
    this.yPos = yPos;
    this.scale = scale;
  }
  
  public Bitmap getBitmap() {
    if (bitmap != null) {
      return bitmap;
    }

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    paint.setTextAlign(Align.RIGHT);
    Typeface font = Typeface.create("Arial", Typeface.NORMAL);
    paint.setTypeface(font);
    paint.setTextSize(30);
    FontMetrics fontMetrics = paint.getFontMetrics();

    int textWidth = (int) paint.measureText(text);
    int textHeight = (int) (-fontMetrics.ascent + fontMetrics.descent);
    Bitmap bitmap = Bitmap.createBitmap(textWidth, textHeight, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);

    paint.setColor(Color.WHITE);
    paint.setAlpha(128);
    paint.setStyle(android.graphics.Paint.Style.STROKE);
    paint.setStrokeWidth(4);
    canvas.drawText(text, textWidth, -fontMetrics.ascent, paint);
    paint.setColor(Color.BLACK);
    paint.setAlpha(128);
    paint.setStyle(android.graphics.Paint.Style.FILL);
    canvas.drawText(text, textWidth, -fontMetrics.ascent, paint);
    this.bitmap = bitmap;
    return bitmap;
  }
}
