package com.nutiteq.license;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.nutiteq.utils.Utils;

/**
 * Helper class for reading license files and checking license properties.
 * @pad.exclude
 */
public class LicenseManager {
  private final static String ENCRYPT_PREFIX = "X";
  
  private static LicenseManager instance = null;
  private List<Watermark> watermarks = new ArrayList<Watermark>();
  private Properties properties = new Properties();
  
  public LicenseManager() {
  }

  public synchronized void clear() {
    watermarks = new ArrayList<Watermark>();
    properties.clear();
  }

  public synchronized boolean load(String signedLicense, String publicKeyString) {
    try {
      PublicKey publicKey = KeyFactory.getInstance("DSA").generatePublic(new X509EncodedKeySpec(Utils.base64Decode(publicKeyString)));

      Signature signature = Signature.getInstance("SHA1withDSA");
      signature.initVerify(publicKey);

      BufferedReader reader = new BufferedReader(new StringReader(signedLicense));
      String signatureString = "";
      while (reader.ready()) {
        String line = reader.readLine();
        if (line == null || line.equals("")) {
          break;
        }
        signatureString += line;
      }
      String propertiesString = "";
      while (reader.ready()) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }
        signature.update(line.getBytes(), 0, line.getBytes().length);
        propertiesString += line + "\n";
      }

      byte[] signatureData = Utils.base64Decode(signatureString);
      if (!signature.verify(signatureData)) {
        return false;
      }
      properties.clear();
      properties.load(new ByteArrayInputStream(propertiesString.getBytes("UTF-8")));
      return true;
    } catch (Exception ex) {
      return false;
    }
  }

  public synchronized void setProperty(String encodedProperty, String value) {
    try {
      String property = new String(Utils.base64Decode(encodedProperty), "UTF-8");
      if (value == null) {
        properties.remove(property);
      } else {
        properties.setProperty(property, value);
      }
    }
    catch (Exception e) {
      return;
    }
  }

  public synchronized String getProperty(String encodedProperty) {
    try {
      String property = new String(Utils.base64Decode(encodedProperty), "UTF-8");
      return properties.getProperty(property).trim();
    }
    catch (Exception e) {
      return null;
    }
  }

  public void setWatermark(Watermark watermark) {
    List<Watermark> watermarks = new ArrayList<Watermark>();
    if (watermark != null) {
      watermarks.add(watermark);
    }
    this.watermarks = watermarks;
  }

  public void addWatermark(Watermark watermark) {
    List<Watermark> watermarks = new ArrayList<Watermark>(this.watermarks);
    if (!watermarks.contains(watermark)) {
      watermarks.add(watermark);
    }
    this.watermarks = watermarks;
  }

  public void removeWatermark(Watermark watermark) {
    List<Watermark> watermarks = new ArrayList<Watermark>(this.watermarks);
    if (watermarks.contains(watermark)) {
      watermarks.remove(watermark);
    }
    this.watermarks = watermarks;
  }

  public List<Watermark> getWatermarks() {
    return watermarks;
  }

  public static void setInstance(LicenseManager instance) {
    LicenseManager.instance = instance;
  }

  public static LicenseManager getInstance() {
    return instance;
  }

  public static String encryptLicense(String license) {
    try {
      return ENCRYPT_PREFIX + Utils.base64Encode(license.getBytes("UTF-8"), -1); 
    } catch (UnsupportedEncodingException e) {
      return null;
    }
  }

  public static String decryptLicense(String license) throws UnsupportedEncodingException {
    if (!license.startsWith(ENCRYPT_PREFIX)) {
      throw new UnsupportedEncodingException();
    }
    return new String(Utils.base64Decode(license.substring(ENCRYPT_PREFIX.length())), "UTF-8");
  }

  public static String signLicense(String license, KeySpec privateKeySpec, String publicKeyString) throws IOException, GeneralSecurityException {
    PrivateKey privateKey = KeyFactory.getInstance("DSA", "SUN").generatePrivate(privateKeySpec);
    PublicKey publicKey = KeyFactory.getInstance("DSA").generatePublic(new X509EncodedKeySpec(Utils.base64Decode(publicKeyString)));

    Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
    signature.initSign(privateKey);

    Signature signature2 = Signature.getInstance("SHA1withDSA");
    signature2.initVerify(publicKey);

    BufferedReader reader = new BufferedReader(new StringReader(license));
    String propertiesString = "";
    while (reader.ready()) {
      String line = reader.readLine();
      if (line == null) {
        break;
      }
      signature.update(line.getBytes(), 0, line.getBytes().length);
      signature2.update(line.getBytes(), 0, line.getBytes().length);
      propertiesString += line + "\n";
    }

    byte[] signatureData = signature.sign();
    if (!signature2.verify(signatureData)) {
      throw new RuntimeException("Signature consistency failure, perhaps public and private key mismatch?");
    }
    String signatureString = Utils.base64Encode(signatureData, -1);
    return signatureString + "\n\n" + propertiesString;
  }
}
