package com.nutiteq.license;

/**
 * Watermark for OpenStreetMap licenses.
 * @pad.exclude
 */
public class OSMTextWatermark extends Watermark {
  private final static String TEXT = "\u00a9 OpenStreetMap";
  private final static float SCALE = 0.4f;
  private final static float XPOS = 0.975f;
  private final static float YPOS = -1.0f;
  
  public OSMTextWatermark() {
    super(TEXT, XPOS, YPOS, SCALE);
  }
}
