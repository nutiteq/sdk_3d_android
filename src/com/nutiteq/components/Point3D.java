package com.nutiteq.components;

/**
 * Point in 3D space.
 */
public class Point3D {
  /**
   * x coordinate of the point.
   */
  public final double x;

  /**
   * y coordinate of the point.
   */
  public final double y;

  /**
   * z coordinate of the point.
   */
  public final double z;

  /**
   * Default constructor. Place point at origin.
   */
  public Point3D() {
    this(0, 0, 0);
  }

  /**
   * Construct point from 3 coordinates.
   * 
   * @param x
   *          x coordinate.
   * @param y
   *          y coordinate.
   * @param z
   *          z coordinate.
   */
  public Point3D(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  /**
   * Construct point from coordinate array. Array must contain 3 coordinates.
   *  
   * @param components
   *          array containing x, y, z coordinates.
   */
  public Point3D(double[] components) {
    this.x = components[0];
    this.y = components[1];
    this.z = components[2];
  }

  /**
   * Construct point by copying existing point.
   * 
   * @param point
   *          point to copy.
   */
  public Point3D(Point3D point) {
    this.x = point.x;
    this.y = point.y;
    this.z = point.z;
  }

  /**
   * Construct point by copying existing mutable point.
   * 
   * @param point
   *          point to copy.
   */
  public Point3D(MutablePoint3D point) {
    this.x = point.x;
    this.y = point.y;
    this.z = point.z;
  }

  /**
   * Get array containing 3 coordinates.
   * 
   * @return array containing x, y, z coordinates.
   */
  public double[] toArray() {
    return new double[] { x, y, z };
  }

  @Override
  public String toString() {
    return "Point3D [x=" + x + ", y=" + y + ", z=" + z +"]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    Point3D other = (Point3D) o;
    return x == other.x && y == other.y && z == other.z;
  }

}
