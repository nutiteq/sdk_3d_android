package com.nutiteq.components;

import com.nutiteq.utils.GeomUtils;

/**
 * Bounding area on the map. Can be defined by by horizontal and vertical bounds or by convex bounding polygon.
 * This class is intended for conservative object area estimation and fast intersection testing.
 */
public class Envelope {

  /**
   * Minimum value for x coordinate.
   */
  public final double minX;

  /**
   * Minimum value for y coordinate.
   */
  public final double minY;

  /**
   * Maximum value for x coordinate.
   */
  public final double maxX;

  /**
   * Maximum value for y coordinate.
   */
  public final double maxY;
  
  /**
   * Convex hull for tight fit. Can be null!
   */
  private final MapPos[] convexHull;
  
  /**
   * Construct envelope by copying existing envelope.
   * 
   * @param env
   *          envelope to copy.
   */
  public Envelope(Envelope env) {
    minX = env.minX;
    minY = env.minY;
    maxX = env.maxX;
    maxY = env.maxY;
    convexHull = env.convexHull;
  }
  
  /**
   * Construct envelope by copying existing envelope.
   * 
   * @param env
   *          envelope to copy.
   */
  public Envelope(MutableEnvelope env) {
    minX = env.minX;
    minY = env.minY;
    maxX = env.maxX;
    maxY = env.maxY;
    convexHull = null;
  }
  
  /**
   * Construct empty envelope from a single point.
   * 
   * @param x
   *          x coordinate of the point.
   * @param y
   *          y coordinate of the point.
   */
  public Envelope(double x, double y) {
    this.minX = x;
    this.minY = y;
    this.maxX = x;
    this.maxY = y;
    this.convexHull = null;
  }

  /**
   * Construct envelope from coordinate ranges.
   * 
   * @param minX
   *          minimum x coordinate.
   * @param maxX
   *          maximum x coordinate.
   * @param minY
   *          minimum y coordinate.
   * @param maxY
   *          maximum y coordinate.
   */
  public Envelope(double minX, double maxX, double minY, double maxY) {
    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;
    this.convexHull = null;
  }
  
  /**
   * Construct envelope from convex hull.
   * 
   * @param convexHull
   *          points defining convex hull of the envelope.
   */
  public Envelope(MapPos[] convexHull) {
    double minX = Double.MAX_VALUE, maxX = -Double.MAX_VALUE;
    double minY = Double.MAX_VALUE, maxY = -Double.MAX_VALUE;
    for (MapPos p : convexHull) {
      minX = Math.min(minX, p.x);
      maxX = Math.max(maxX, p.x);
      minY = Math.min(minY, p.y);
      maxY = Math.max(maxY, p.y);
    }
    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;
    if (Double.isInfinite(minX) || Double.isInfinite(maxX) || Double.isInfinite(minY) || Double.isInfinite(maxY)) { // polygon-based containment tests will not work in this case, use bounding box only
      this.convexHull = null;
    } else {
      this.convexHull = convexHull.clone();
    }
  }

  /**
   * Get minimum x coordinate.
   * 
   * @return minimum x coordinate of the envelope.
   */
  public double getMinX() {
    return minX;
  }

  /**
   * Get minimum y coordinate.
   * 
   * @return minimum y coordinate of the envelope.
   */
  public double getMinY() {
    return minY;
  }

  /**
   * Get maximum x coordinate.
   * 
   * @return maximum x coordinate of the envelope.
   */
  public double getMaxX() {
    return maxX;
  }

  /**
   * Get maximum y coordinate.
   * 
   * @return maximum y coordinate of the envelope.
   */
  public double getMaxY() {
    return maxY;
  }

  /**
   * Get width of the envelope
   * 
   * @return width of the envelope.
   */
  public double getWidth(){
    return maxX - minX;
  }
  
  /**
   * Get height of the envelope
   * 
   * @return height of the envelope.
   */
  public double getHeight(){
    return maxY - minY;
  }
  
  /**
   * Get convex hull points.
   * 
   * @return array of convex hull points. This array should not be changed!
   */
  public MapPos[] getConvexHull() {
    if (convexHull == null) {
      MapPos[] convexHull = new MapPos[] {
          new MapPos(minX, minY),
          new MapPos(minX, maxY),
          new MapPos(maxX, maxY),
          new MapPos(maxX, minY)
      };
      return convexHull;
    }
    return convexHull;
  }

  /**
   * Test if this envelope contains the given point.
   * 
   * @param mapPos
   *          the point to test.
   * @return true when the point is contained within this envelope. false otherwise.
   */
  public boolean contains(MapPos mapPos) {
    if (!(minX <= mapPos.x && minY <= mapPos.y && maxX >= mapPos.x && maxY >= mapPos.y)) {
      return false;
    }
    if (convexHull == null) {
      return true;
    }
    if (!GeomUtils.pointInsidePolygon(convexHull, mapPos.x, mapPos.y)) {
      return false;
    }
    return true;
  }
  
  /**
   * Test if this envelope fully contains the given envelope.
   * 
   * @param env
   *          the envelope to test.
   * @return true when the other envelope is fully contained within this envelope. false otherwise.
   */
  public boolean contains(Envelope env) {
    if (!(minX <= env.minX && minY <= env.minY && maxX >= env.maxX && maxY >= env.maxY)) {
      return false;
    }
    if (Double.isInfinite(env.minX) || Double.isInfinite(env.maxX) || Double.isInfinite(env.minY) || Double.isInfinite(env.maxY)) {
      return false;
    }
    if (convexHull == null) {
      return true;
    }
    for (MapPos p : env.getConvexHull()) {
      if (!GeomUtils.pointInsidePolygon(convexHull, p.x, p.y)) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Test if this envelope fully covers the given envelope. Equivalent to contains() method.
   * 
   * @param env
   *          the envelope to test.
   * @return true when the other envelope is fully contained within this envelope. false otherwise.
   */
  public boolean covers(Envelope env) {
    return env.contains(this);
  }
  
  /**
   * Test if this envelope partly intersects the given envelope.
   * 
   * @param env
   *          the envelope to test.
   * @return true when the other envelope intersects this envelope. false otherwise.
   */
  public boolean intersects(Envelope env) {
    if (env.maxX < minX || env.minX > maxX || env.maxY < minY || env.minY > maxY) {
      return false;
    }
    if (convexHull == null && env.convexHull == null) {
      return true;
    }
    if (Double.isInfinite(minX) || Double.isInfinite(maxX) || Double.isInfinite(minY) || Double.isInfinite(maxY)) {
      return true;
    }
    if (Double.isInfinite(env.minX) || Double.isInfinite(env.maxX) || Double.isInfinite(env.minY) || Double.isInfinite(env.maxY)) {
      return true;
    }
    return GeomUtils.polygonsIntersect(getConvexHull(), env.getConvexHull());
  }

  @Override
  public String toString() {
    if (convexHull == null) {
      return "Envelope [minX=" + minX + ", minY=" + minY + ", maxX=" + maxX + ", maxY=" + maxY + "]";
    } else {
      String coords = "";
      for (MapPos p : convexHull) {
        coords += (coords.length() == 0 ? "" : ",") + p;
      }
      return "Envelope [" + coords + "]";
    }
  }
  
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    Envelope other = (Envelope) o;
    if (!(minX == other.minX && minY == other.minY && maxX == other.maxX && maxY == other.maxY)) {
      return false;
    }
    if (convexHull == null && other.convexHull == null) {
      return true;
    }
    return java.util.Arrays.equals(getConvexHull(), other.getConvexHull());
  }
}
