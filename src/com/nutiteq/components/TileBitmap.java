package com.nutiteq.components;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

/**
 * Wrapper for raster tile bitmaps.
 */
public class TileBitmap {
  private static BitmapFactory.Options BITMAP_FACTORY_OPTIONS;
  
  static {
    BITMAP_FACTORY_OPTIONS = new BitmapFactory.Options();
    BITMAP_FACTORY_OPTIONS.inScaled = false;
  }

  private final Bitmap bitmap;
  private byte[] compressed;

  /**
   * Default constructor. Construct tile bitmap from uncompressed bitmap.
   * 
   * @param bitmap
   *          source bitmap for the tile.
   */
  public TileBitmap(Bitmap bitmap) {
    this.bitmap = bitmap;
  }
  
  /**
   * Default constructor. Construct tile bitmap from compressed bitmap.
   * 
   * @param compressed
   *          compressed image as byte array.
   */
  public TileBitmap(byte[] compressed) {
    this.compressed = compressed;
    this.bitmap = BitmapFactory.decodeByteArray(compressed, 0, compressed.length, BITMAP_FACTORY_OPTIONS);
  }
 
  /**
   * Get the source bitmap.
   * 
   * @return source bitmap.
   */
  public Bitmap getBitmap() {
    return bitmap;
  }
  
  /**
   * Get bitmap as compressed byte array. The byte array can be decompressed using BitmapFactory.
   * 
   * @return compressed bitmap as byte array.
   */
  public synchronized byte[] getCompressed() {
    if (compressed == null) {
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      bitmap.compress(CompressFormat.PNG, 100, os);
      compressed = os.toByteArray();
    }
    return compressed;
  }
}
