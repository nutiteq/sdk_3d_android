package com.nutiteq.components;

import com.nutiteq.cache.CompressedMemoryCache;
import com.nutiteq.cache.PersistentCache;
import com.nutiteq.cache.TextureMemoryCache;
import com.nutiteq.cache.VectorTileCache;
import com.nutiteq.layers.Layers;
import com.nutiteq.renderers.MapRenderers;
import com.nutiteq.tasks.CancelableThreadPool;
import com.nutiteq.utils.LongHashMap;

/**
 * Components is a container for internal state of the map view. It contains layers, caches, etc.
 * This state should be retained when activity is recreated within application.
 */
public class Components {
  
  /**
   * Synchronized container for keeping tiles that are being processed.
   * @pad.exclude
   */
  public static class RetrievingTileSet {
    private static Object tileObject = new Object();
    private LongHashMap<Object> retrievingTiles = new LongHashMap<Object>();

    public synchronized void add(long tileId) {
      retrievingTiles.put(tileId, tileObject);
    }

    public synchronized void remove(long tileId) {
      retrievingTiles.remove(tileId);
    }

    public synchronized boolean contains(long tileId) {
      return retrievingTiles.containsKey(tileId);
    }
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public final RetrievingTileSet retrievingTiles = new RetrievingTileSet();

  /**
   * Texture memory cache is used to store raster map tiles.
   */
  public final TextureMemoryCache textureMemoryCache = new TextureMemoryCache();
  
  /**
   * Compressed memory cache is used to store raster map tiles in compressed form.
   */
  public final CompressedMemoryCache compressedMemoryCache = new CompressedMemoryCache();

  /**
   * Persistent cache is used to persistently store raster map tiles, so that tiles can be loaded quickly when application is restarted.
   */
  public final PersistentCache persistentCache = new PersistentCache();
  
  /**
   * Vector tile cache
   */
  public final VectorTileCache vectorTileCache = new VectorTileCache();

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public final CancelableThreadPool rasterTaskPool = new CancelableThreadPool(1);

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public final CancelableThreadPool vectorTaskPool = new CancelableThreadPool(1);

  /**
   * List of map layers.
   */
  public final Layers layers = new Layers(this);

  /**
   * Constraints for map manipulations.
   */
  public final Constraints constraints = new Constraints(this);

  /**
   * Options for map rendering, resource managements, etc.
   */
  public final Options options = new Options(this);

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public final MapRenderers mapRenderers = new MapRenderers(this);
}
