package com.nutiteq.components;

/**
 * Vector 3D space.
 * This class is deprecated and has been superseded by Vector3D.
 */
public class Vector {
  /**
   * x coordinate of the vector.
   */
  public final double x;

  /**
   * x coordinate of the vector.
   */
  public final double y;

  /**
   * z coordinate of the vector.
   */
  public final double z;
  
  /**
   * Constructor for null vector.
   */
  public Vector() {
    this(0, 0, 0);
  }
  
  /**
   * Construct vector from 2 coordinates.
   * 
   * @param x
   *          x coordinate of the vector.
   * @param y
   *          y coordinate of the vector.
   */
  public Vector(double x, double y) {
    this(x, y, 0);
  }
  
  /**
   * Construct vector from 3 coordinates.
   * 
   * @param x
   *          x coordinate of the vector.
   * @param y
   *          y coordinate of the vector.
   * @param z
   *          z coordinate of the vector.
   */
  public Vector(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  /**
   * Construct vector from coordinate array. Array must contain 3 coordinates.
   *  
   * @param components
   *          array containing x, y, z coordinates.
   */
  public Vector(double[] components) {
    this.x = components[0];
    this.y = components[1];
    this.z = components[2];
  }

  /**
   * Construct vector by copying existing vector.
   *  
   * @param vec
   *          existing vector to copy.
   */
  public Vector(Vector vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }

  /**
   * Construct vector by copying existing vector.
   *  
   * @param vec
   *          existing vector to copy.
   */
  public Vector(MutableVector vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }

  /**
   * Get length of the vector when projected to 2D plane (by dropping z coordinate).
   * 
   * @return length of the xy projection.
   */
  public double getLength2D() {
    return Math.sqrt(x * x + y * y);
  }

  /**
   * Get length of the vector.
   * 
   * @return length of the vector.
   */
  public double getLength3D() {
    return Math.sqrt(x * x + y * y + z * z);
  }
  
  /**
   * Get a normalized version of this vector that's been projected to xy plane (by dropping z coordinate).
   * 
   * @return normalized version of the vector projected to xy plane
   */
  public Vector getNormalized2D() {
    double length = getLength2D();
    return new Vector(x / length, y / length);
  }
  
  /**
   * Get the normalized version of this vector.
   * 
   * @return normalized vector
   */
  public Vector getNormalized3D() {
    double length = getLength3D();
    return new Vector(x / length, y / length, z / length);
  }
  
  /**
   * Get the distance between this and another point projected to a 2D plane (by dropping z coordinate).
   * @param vector
   *          the point from which the distance is measured
   * @return xy projection of the distance between two points
   */
  public double getDistanceFromPoint2D(Vector vector) {
    double deltaX = vector.x - x;
    double deltaY = vector.y - y;
    return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
  }
  
  /**
   * Get the distance between this and another point.
   * @param vector
   *          the point from which the distance is measured
   * @return the distance between two points
   */
  public double getDistanceFromPoint3D(Vector vector) {
    double deltaX = vector.x - x;
    double deltaY = vector.y - y;
    double deltaZ = vector.z - z;
    return Math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
  }
  
  /**
   * Get the distance between this point and a line defined by two other points.   * @param p1
   *        the first point that defines the line
   * @param p2
   *        the second point that defines the line
   * @return the distance between this point and the line
   */
  public double getDistanceFromLine2D(Vector p1, Vector p2) {
    double length = p1.getDistanceFromPoint2D(p2);
    return Math.abs((x - p1.x) * (p2.y - p1.y) - (y - p1.y) * (p2.x - p1.x)) / length;
  }
  
  /**
   * Get the point that lies between this and another point, projected to a 2D plane (by dropping z coordinate).
   * @param vector
   *         the other point from which the between point is calculated
   * @param distance
   *         the distance between this and the between point
   * @return the middle point
   */
  public Vector getPointBetween2D(Vector vector, float distance) {
    return new Vector(x + (vector.x - x) * distance, y + (vector.y - y) * distance);
  }
  
  /**
   * Get the point that lies between this and another point.
   * @param vector
   *         the other point from which the middle point is calculated
   * @param distance
   *         the distance between this and the between point
   * @return the middle point
   */
  public Vector getPointBetween3D(Vector vector, float distance) {
    return new Vector(x + (vector.x - x) * distance, y + (vector.y - y) * distance, z + (vector.z - z) * distance);
  }
  
  /**
   * Get a vector that's equal to this vector minus the input vector, projected to a 2D plane (by dropping z coordinate).
   * @param vector
   *         the vector to be subtracted from the original vector
   * @return the subtracting result vector
   */
  public Vector getSubtracted2D(Vector vector) {
    return new Vector(x - vector.x, y - vector.y);
  }
  
  /**
  * Get a vector that's equal to this vector minus the input vector
  * @param vector
  *         the vector to be subtracted from the original vector
  * @return the subtracting result vector
  */
  public Vector getSubtracted3D(Vector vector) {
    return new Vector(x - vector.x, y - vector.y, z - vector.z);
  }
  
  /**
   * Calculate dot product between two vectors.
   * @param a
   *        first vector
   * @param b
   *        second vector
   * @return dot product between the vectors
   */
  public static double dotProduct(Vector a, Vector b) {
    return a.x * b.x + a.y * b.y + a.z * b.z;
  }
 
  /**
   * Calculate cross product between two vectors.
   * @param a
   *        first vector
   * @param b
   *        second vector
   * @return dot product between the vectors
   */
  public static Vector crossProduct(Vector a, Vector b) {
    double x = a.y * b.z - a.z * b.y; 
    double y = a.z * b.x - a.x * b.z; 
    double z = a.x * b.y - a.y * b.x;
    return new Vector(x, y, z);
  }
 
  /**
   * Get vector components as array containing 3 elements.
   * 
   * @return array containing x, y, z coordinates.
   */
  public double[] toArray() {
    return new double[] { x, y, z };
  }

  @Override
  public String toString() {
    return "Vector [x=" + x + ", y=" + y + ", z=" + z +"]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    Vector other = (Vector) o;
    return x == other.x && y == other.y && z == other.z;
  }

}