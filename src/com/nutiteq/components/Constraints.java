package com.nutiteq.components;

import com.nutiteq.utils.Const;
import com.nutiteq.utils.Utils;

/**
 * Specification for various map manipulations constraints. These constraints will be used by both, the MapView
 * map manipulation API, and the user interface.
 */
public class Constraints {

  /**
   * The default zoom range.
   */
  public static final Range DEFAULT_ZOOM_RANGE = new Range(0, Const.MAX_SUPPORTED_ZOOM_LEVEL);

  /**
   * The default tilt range.
   */
  public static final Range DEFAULT_TILT_RANGE = new Range(30, 90);
  
  /**
   * The default bounds.
   */
  public static final Bounds DEFAULT_BOUNDS = null;

  private Range zoomRange = DEFAULT_ZOOM_RANGE;

  private Range tiltRange = DEFAULT_TILT_RANGE;

  private Bounds bounds = null;

  private boolean rotatable = true;

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public Constraints(Components components) {
  }

  /**
   * Gets the current tilt range. The minimum and maximum values are in degrees.
   * 
   * @return the current tilt range
   */
  public Range getTiltRange() {
    return tiltRange;
  }

  /**
   * Sets the new tilt range. The minimum tilt range is 30 degrees and the maximum is 90 degrees. Values that are out of
   * range are capped. In order to make map non-tiltable, range 90,90 should be used.
   * 
   * @param tiltRange
   *          the new tilt range
   */
  public void setTiltRange(Range tiltRange) {
    float min = Utils.toRange(tiltRange.min, DEFAULT_TILT_RANGE.min, DEFAULT_TILT_RANGE.max);
    float max = Utils.toRange(tiltRange.max, DEFAULT_TILT_RANGE.min, DEFAULT_TILT_RANGE.max);
    this.tiltRange = new Range(min, max);
  }

  /**
   * Gets the current zoom range.
   * 
   * @return the current zoom range
   */
  public Range getZoomRange() {
    return zoomRange;
  }

  /**
   * Sets the new zoom range. The minimum zoom value is 0 and the maximum is 24. Values that are out of range are
   * capped.
   * 
   * @param zoomRange
   *          the new zoom range
   */
  public void setZoomRange(Range zoomRange) {
    float min = Utils.toRange(zoomRange.min, DEFAULT_ZOOM_RANGE.min, DEFAULT_ZOOM_RANGE.max);
    float max = Utils.toRange(zoomRange.max, DEFAULT_ZOOM_RANGE.min, DEFAULT_ZOOM_RANGE.max);
    this.zoomRange = new Range(min, max);
  }

  /**
   * Gets the current map bounds. Bounds left, top, right and bottom values are in the current base map's projection.
   * 
   * @return the current map bounds. Can be null when bounds are not defined.
   */
  public Bounds getMapBounds() {
    return bounds;
  }

  /**
   * Sets new map bounds. Bounds left, top, right and bottom values are in the current base map's projection. If the
   * bounds are larger than the world size, they will be capped to world bounds.
   * 
   * @param bounds
   *          the new map bounds (can be null)
   */
  public void setMapBounds(Bounds bounds) {
    this.bounds = bounds;
  }

  /**
   * Test if rotation is activated.
   * 
   * @return can rotate map view with dual-touch gesture
   */
  public boolean isRotatable() {
    return this.rotatable;
  }

  /**
   * Set map view rotation on/off. Default is on.
   * 
   * @param rotatable
   *          flag to turn rotation on/off. 
   */
  public void setRotatable(boolean rotatable) {
    this.rotatable = rotatable;
  }

}
