package com.nutiteq.components;


/**
 * Container class that defines rectangle on the map using left, top, right and bottom coordinates.
 * For valid bounds, left should be less than right and bottom should be less than top.
 * Valid ranges for bounds depend on the projection used.
 * 
 * @author Nutiteq
 * 
 */
public class Bounds {
  /**
   * Left coordinate of the bounds.
   */
  public final double left;

  /**
   * Right coordinate of the bounds.
   */
  public final double right;
  /**
   * Top coordinate of the bounds.
   */
  public final double top;
  /**
   * Bottom coordinate of the bounds.
   */
  public final double bottom;

  /**
   * Class constructor, that uses 4 separate parameters for defining the rectangle.
   * 
   * @param left
   *          the left value
   * @param top
   *          the top value
   * @param right
   *          the right value
   * @param bottom
   *          the bottom value
   */
  public Bounds(double left, double top, double right, double bottom) {
    this.left = left;
    this.right = right;
    this.top = top;
    this.bottom = bottom;
  }
  
  /**
   * Return width of the bounds
   * 
   * @return width of the bounds.
   */
  public double getWidth() {
    return right - left;
  }

  /**
   * Return height of the bounds
   * 
   * @return height of the bounds.
   */
  public double getHeight() {
    return top - bottom;
  }

  @Override
  public String toString() {
    return "Bounds [left=" + left + ", top=" + top + ", right=" + right + ", bottom=" + bottom + "]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    Bounds other = (Bounds) o;
    return left == other.left && right == other.right && top == other.top && bottom == other.bottom;
  }
}
