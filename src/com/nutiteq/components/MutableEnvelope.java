package com.nutiteq.components;

/**
 * Rectangular area on the map. Defined by horizontal and vertical bounds.
 * This class is intended for conservative object area estimation and fast intersection testing.
 * This class is mutable version of Envelope class.
 */
public class MutableEnvelope {
  /**
   * Minimum value for x coordinate.
   */
  public double minX;

  /**
   * Minimum value for y coordinate.
   */
  public double minY;

  /**
   * Maximum value for x coordinate.
   */
  public double maxX;
  
  /**
   * Maximum value for y coordinate.
   */
  public double maxY;
  
  /**
   * Construct empty envelope.
   */
  public MutableEnvelope() {
    minX = minY =  Double.MAX_VALUE;
    maxX = maxY = -Double.MAX_VALUE;
  }

  /**
   * Construct envelope by copying existing envelope.
   * 
   * @param env
   *          envelope to copy.
   */
  public MutableEnvelope(Envelope env) {
    minX = env.minX;
    minY = env.minY;
    maxX = env.maxX;
    maxY = env.maxY;
  }
  
  /**
   * Construct envelope by copying existing envelope.
   * 
   * @param env
   *          envelope to copy.
   */
  public MutableEnvelope(MutableEnvelope env) {
    minX = env.minX;
    minY = env.minY;
    maxX = env.maxX;
    maxY = env.maxY;
  }
  
  /**
   * Construct empty envelope from a single point.
   * 
   * @param x
   *          x coordinate of the point.
   * @param y
   *          y coordinate of the point.
   */
  public MutableEnvelope(double x, double y) {
    this.minX = x;
    this.minY = y;
    this.maxX = x;
    this.maxY = y;
  }

  /**
   * Construct envelope from coordinate ranges.
   * 
   * @param minX
   *          minimum x coordinate.
   * @param maxX
   *          maximum x coordinate.
   * @param minY
   *          minimum y coordinate.
   * @param maxY
   *          maximum y coordinate.
   */
  public MutableEnvelope(double minX, double maxX, double minY, double maxY) {
    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;
  }
  
  /**
   * Add single point to the envelope. If point is outside the envelope, envelope will be enlarged to contain the point.
   * 
   * @param x
   *          x coordinate of the point.
   * @param y
   *          y coordinate of the point.
   */
  public void add(double x, double y) {
    minX = Math.min(minX, x);
    minY = Math.min(minY, y);
    maxX = Math.max(maxX, x);
    maxY = Math.max(maxY, y);
  }
  
  /**
   * Add single point to the envelope. If point is outside the envelope, envelope will be enlarged to contain the point.
   * 
   * @param mapPos
   *          the point to add.
   */
  public void add(MapPos mapPos) {
    minX = Math.min(minX, mapPos.x);
    minY = Math.min(minY, mapPos.y);
    maxX = Math.max(maxX, mapPos.x);
    maxY = Math.max(maxY, mapPos.y);
  }
  
  /**
   * Add another envelope to the envelope. Current envelope will be will be enlarged to contain the given envelope.
   * 
   * @param env
   *          envelope to add.
   */
  public void add(Envelope env) {
    minX = Math.min(minX, env.minX);
    minY = Math.min(minY, env.minY);
    maxX = Math.max(maxX, env.maxX);
    maxY = Math.max(maxY, env.maxY);
  }
  
  /**
   * Add another envelope to the envelope. Current envelope will be will be enlarged to contain the given envelope.
   * 
   * @param env
   *          envelope to add.
   */
  public void add(MutableEnvelope env) {
    minX = Math.min(minX, env.minX);
    minY = Math.min(minY, env.minY);
    maxX = Math.max(maxX, env.maxX);
    maxY = Math.max(maxY, env.maxY);
  }
  
  /**
   * Get minimum x coordinate.
   * 
   * @return minimum x coordinate of the envelope.
   */
  public double getMinX() {
    return minX;
  }

  /**
   * Get minimum y coordinate.
   * 
   * @return minimum y coordinate of the envelope.
   */
  public double getMinY() {
    return minY;
  }

  /**
   * Get maximum x coordinate.
   * 
   * @return maximum x coordinate of the envelope.
   */
  public double getMaxX() {
    return maxX;
  }

  /**
   * Get maximum y coordinate.
   * 
   * @return maximum y coordinate of the envelope.
   */
  public double getMaxY() {
    return maxY;
  }

  /**
   * Get width of the envelope
   * 
   * @return width of the envelope.
   */
  public double getWidth(){
    return maxX - minX;
  }
  
  /**
   * Get height of the envelope
   * 
   * @return height of the envelope.
   */
  public double getHeight(){
    return maxY - minY;
  }

  /**
   * Test if this envelope fully contains the given envelope.
   * 
   * @param env
   *          the envelope to test.
   * @return true when the other envelope is fully contained within this envelope. false otherwise.
   */
  public boolean contains(MutableEnvelope env) {
    return minX <= env.minX && minY <= env.minY && maxX >= env.maxX && maxY >= env.maxY;
  }
  
  /**
   * Test if this envelope fully covers the given envelope. Equivalent to contains() method.
   * 
   * @param env
   *          the envelope to test.
   * @return true when the other envelope is fully contained within this envelope. false otherwise.
   */
  public boolean covers(MutableEnvelope env) {
    return env.contains(this);
  }
  
  /**
   * Test if this envelope partly intersects the given envelope.
   * 
   * @param env
   *          the envelope to test.
   * @return true when the other envelope intersects this envelope. false otherwise.
   */
  public boolean intersects(MutableEnvelope env) {
    if (env.maxX < minX || env.minX > maxX)
      return false;
    if (env.maxY < minY || env.minY > maxY)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    return "MutableEnvelope [minX=" + minX + ", minY=" + minY + ", maxX=" + maxX + ", maxY=" + maxY + "]";
  }
  
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MutableEnvelope other = (MutableEnvelope) o;
    return minX == other.minX && minY == other.minY && maxX == other.maxX && maxY == other.maxY;
  }
}
