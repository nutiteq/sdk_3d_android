package com.nutiteq.components;

/**
 * Vector in 3D space.
 */
public class Vector3D {
  /**
   * x coordinate of the vector.
   */
  public final double x;

  /**
   * x coordinate of the vector.
   */
  public final double y;

  /**
   * z coordinate of the vector.
   */
  public final double z;
  
  /**
   * Constructor for null vector.
   */
  public Vector3D() {
    this(0, 0, 0);
  }
  
  /**
   * Construct vector from 3 coordinates.
   * 
   * @param x
   *          x coordinate of the vector.
   * @param y
   *          y coordinate of the vector.
   * @param z
   *          z coordinate of the vector.
   */
  public Vector3D(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  /**
   * Construct vector from coordinate array. Array must contain 3 coordinates.
   *  
   * @param components
   *          array containing x, y, z coordinates.
   */
  public Vector3D(double[] components) {
    this.x = components[0];
    this.y = components[1];
    this.z = components[2];
  }

  /**
   * Construct vector by copying existing vector.
   *  
   * @param vec
   *          existing vector to copy.
   */
  public Vector3D(Vector3D vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }

  /**
   * Construct vector by copying existing mutable vector.
   *  
   * @param vec
   *          existing vector to copy.
   */
  public Vector3D(MutableVector3D vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }

  /**
   * Construct vector specified by 2 points.
   *  
   * @param point1
   *          vector origin.
   * @param point2
   *          vector target.
   */
  public Vector3D(Point3D point1, Point3D point2) {
    this.x = point2.x - point1.x;
    this.y = point2.y - point1.y;
    this.z = point2.z - point1.z;
  }

  /**
   * Get length of the vector.
   * 
   * @return length of the vector.
   */
  public double getLength() {
    return Math.sqrt(x * x + y * y + z * z);
  }
  
  /**
   * Get the normalized version of this vector.
   * 
   * @return normalized vector
   */
  public Vector3D getNormalized() {
    double length = getLength();
    return new Vector3D(x / length, y / length, z / length);
  }
  
  /**
   * Get the distance between this and another point.
   * 
   * @param point
   *          the point from which the distance is measured
   * @return the distance between two points
   */
  public double getDistanceFromPoint(Point3D point) {
    double deltaX = point.x - x;
    double deltaY = point.y - y;
    double deltaZ = point.z - z;
    return Math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
  }
  
  /**
   * Calculate dot product between two vectors.
   * @param a
   *        first vector
   * @param b
   *        second vector
   * @return dot product between the vectors
   */
  public static double dotProduct(Vector3D a, Vector3D b) {
    return a.x * b.x + a.y * b.y + a.z * b.z;
  }
 
  /**
   * Calculate cross product between two vectors.
   * @param a
   *        first vector
   * @param b
   *        second vector
   * @return dot product between the vectors
   */
  public static Vector3D crossProduct(Vector3D a, Vector3D b) {
    double x = a.y * b.z - a.z * b.y; 
    double y = a.z * b.x - a.x * b.z; 
    double z = a.x * b.y - a.y * b.x;
    return new Vector3D(x, y, z);
  }
 
  /**
   * Get vector components as array containing 3 elements.
   * 
   * @return array containing x, y, z coordinates.
   */
  public double[] toArray() {
    return new double[] { x, y, z };
  }

  @Override
  public String toString() {
    return "Vector3D [x=" + x + ", y=" + y + ", z=" + z +"]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    Vector3D other = (Vector3D) o;
    return x == other.x && y == other.y && z == other.z;
  }

}
