package com.nutiteq.components;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.graphics.Bitmap;

import com.nutiteq.tasks.CancelableThreadPool;
import com.nutiteq.ui.MapListener;
import com.nutiteq.ui.MapTouchListener;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.Utils;

/**
 * Options for cache management, rendering and map manipulation.  
 */
public class Options {
  private static final float MIN_PRELOAD_FOV = 90;
  private static final float MAX_PRELOAD_FOV = 160;

  private static final int DEFAULT_TEXTURE_CACHE_SIZE = 15 * 1024 * 1024;
  private static final int DEFAULT_COMPRESSED_CACHE_SIZE = 5 * 1024 * 1024;
  private static final int PERSISTENT_CACHE_SIZE = 50 * 1024 * 1024;

  /**
   * Default preloading field of view.
   */
  public static final float DEFAULT_PRELOAD_FOV = 130;

  /**
   * Default background plane color.
   */
  public static final int DEFAULT_BACKGROUND_PLANE_COLOR = 0xFFB2B2B2;
  /**
   * Default clear color.
   */
  public static final int DEFAULT_CLEAR_COLOR = 0xFFFFFFFF;

  /**
   * Default draw distance.
   */
  public static final float DEFAULT_DRAW_DISTANCE = 8.0f;

  /**
   * Default sky draw offset.
   */
  public static final float DEFAULT_SKY_OFFSET = 4.86f;

  /**
   * Draw nothing.
   */
  public static final int DRAW_NOTHING = 0;

  /**
   * Draw color only.
   */
  public static final int DRAW_COLOR = 1;

  /**
   * Draw bitmap. The bitmap will be scaled and rotated according to camera parameters.
   */
  public static final int DRAW_BITMAP = 2;

  /**
   * Draw bitmap as backdrop. The bitmap will fill entire background and will not be rotated or scaled depending on camera parameters.
   */
  public static final int DRAW_BACKDROP_BITMAP = 3;

  /**
   * Default tile size
   */
  public static final int DEFAULT_TILE_SIZE = 256;
  
  /**
   * Normal renderer
   */
  public static final int NORMAL_RENDERMODE = 0;

  /**
   * Stereo renderer
   */
  public static final int STEREO_RENDERMODE = 1;
  
  /**
   * Planar render projection
   */
  public static final int PLANAR_RENDERPROJECTION = 0;
  
  /**
   * Spherical render projection
   */
  public static final int SPHERICAL_RENDERPROJECTION = 1;

  
  private volatile boolean preloading;
  private volatile float preloadFOV;
  private volatile float halfPreloadFOVTan;

  private volatile int skyDrawMode = DRAW_NOTHING;
  private volatile float skyOffset = DEFAULT_SKY_OFFSET;

  private volatile int backgroundPlaneDrawMode = DRAW_NOTHING;
  private volatile Color backgroundPlaneColor = new Color(DEFAULT_BACKGROUND_PLANE_COLOR);

  private volatile int backgroundPlaneOverlayDrawMode = DRAW_NOTHING;
  private volatile Color backgroundPlaneOverlayColor = new Color(DEFAULT_BACKGROUND_PLANE_COLOR);
  private volatile Bounds backgroundPlaneOverlayBounds = null;

  private volatile int backgroundImageDrawMode = DRAW_NOTHING;

  private volatile Color clearColor = new Color(DEFAULT_CLEAR_COLOR);

  private volatile float drawDistance = DEFAULT_DRAW_DISTANCE;

  private volatile boolean tileFading;

  private volatile boolean seamlessHorizontalPan;

  private volatile boolean doubleClickZoomIn;
  private volatile boolean dualClickZoomOut;

  private volatile boolean generalPanningMode = true;
  private volatile boolean clickTypeDetection = true;
  private volatile float zoomRotateDetectionBalance = 0.0f;
  
  private volatile int tileSize = DEFAULT_TILE_SIZE;
  private volatile float tileZoomLevelBias = 0;

  private volatile boolean kineticPanning;
  private volatile boolean kineticRotation;
  
  private volatile boolean fpsIndicator;

  private volatile int renderMode = NORMAL_RENDERMODE;
  private volatile int renderProjection = PLANAR_RENDERPROJECTION;
  
  private volatile float stereoModeStrength = 1.0f;

  private final Components components;

  private MapListener mapListener;
  
  private List<MapTouchListener> mapTouchListeners = new LinkedList<MapTouchListener>();
  
  private static class StatusHandler implements CancelableThreadPool.StatusHandler {
    WeakReference<MapListener> mapListener;
    
    StatusHandler(MapListener mapListener) {
      this.mapListener = new WeakReference<MapListener>(mapListener);
    }

    @Override
    public void onActivated() {
      MapListener mapListener = this.mapListener.get();
      if (mapListener != null) {
        mapListener.onBackgroundTaskStartedInternal();
      }
    }

    @Override
    public void onDeactivated() {
      MapListener mapListener = this.mapListener.get();
      if (mapListener != null) {
        mapListener.onBackgroundTaskFinishedInternal();
      }
    }
  }

  /**
   * Default constructor.
   */
  public Options(Components components) {
    this.components = components;
    setPreloadFOV(DEFAULT_PRELOAD_FOV);
    components.textureMemoryCache.setSize(DEFAULT_TEXTURE_CACHE_SIZE);
    components.compressedMemoryCache.setSize(DEFAULT_COMPRESSED_CACHE_SIZE);
    components.persistentCache.setSize(PERSISTENT_CACHE_SIZE);
  }

  /**
   * Checks the state of preloading.
   * 
   * @return true if preloading is enabled
   */
  public boolean isPreloading() {
    return preloading;
  }

  /**
   * Sets the state of preloading, default is disabled. Preloading allows the downloading of tiles that are not
   * currently visible on screen, but are adjacent to ones that are. This means that the user can pan the map without
   * immediately noticing any missing tiles.
   * <p>
   * <p>
   * Enabling this option might introduce a small performance hit on slower devices. It should also be noted that this
   * will considerably increase network traffic if used with online maps.
   * <p>
   * <p>
   * The exact area for which the tiles should be preloaded for can be defined using the setPreloadFOV method.
   * 
   * @param preloading
   *          new preloading value
   */
  public void setPreloading(boolean preloading) {
    this.preloading = preloading;
  }

  /**
   * Gets the current preloading field of view value in degrees.
   * 
   * @return the current preloading field of view in degrees
   */
  public float getPreloadFOV() {
    return preloadFOV;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public float getHalfPreloadFOVTan() {
    return halfPreloadFOVTan;
  }

  /**
   * Sets the current preloading field of view in degrees. If preloading is enabled, this value will determine the size
   * of the area for which the tiles should be downloaded. Minimum value is 90 degrees, maximum value is 160 degrees.
   * It's worth noting that the camera's vertical field of view is 90 degrees.
   * 
   * @param preloadFOV
   *          the new preload field of view in degrees
   */
  public void setPreloadFOV(float preloadFOV) {
    preloadFOV = Utils.toRange(preloadFOV, MIN_PRELOAD_FOV, MAX_PRELOAD_FOV);
    this.preloadFOV = preloadFOV;
    halfPreloadFOVTan = (float) Math.tan(preloadFOV * Const.DEG_TO_RAD / 2);
  }

  /**
   * Gets the current sky draw mode. Possible values: DRAW_NOTHING, DRAW_BITMAP.
   * 
   * @return the current sky draw mode.
   */
  public int getSkyDrawMode() {
    return skyDrawMode;
  }

  /**
   * Sets a new background plane draw mode. Possible values:
   * <p>
   * <p>
   * DRAW_NOTHING - sky does't get drawn. Areas that are not covered by map tiles or background plane will be the
   * colored by the color set by setClearColor.
   * <p>
   * <p>
   * DRAW_COLOR - this will default to DRAW_NOTHING. If solid color sky is desired, setClearColor should be used
   * instead.
   * <p>
   * <p>
   * DRAW_BITMAP - draw the sky behind the map tiles and background plane. The bitmap provided will be wrapped around
   * the world so that the first 25% are shown in the north, the next 25% in the east, the next 25% in the south and the
   * last 25% in the west. The sky can be raised or lowered using the setSkyOffset method. The bitmap can be defined
   * with setSkyBitmap. Enabling this might introduce a performance hit on slower devices.
   * 
   * @param skyDrawMode
   *          the new background draw mode
   */
  public void setSkyDrawMode(int skyDrawMode) {
    if (skyDrawMode == DRAW_COLOR) {
      skyDrawMode = DRAW_NOTHING;
    }
    this.skyDrawMode = skyDrawMode;
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Sets a new sky bitmap. See setSkyDrawMode for details. The width and height of the bitmap must be power of two (for
   * example: 256 * 256 or 128 * 512). In the most common use case the width of the bitmap is 4 times the height (width
   * == 4 * height), but this is not a requirement.
   * 
   * @param skyBitmap
   *          the new sky bitmap
   */
  public void setSkyBitmap(Bitmap skyBitmap) {
    components.mapRenderers.getMapRenderer().setSkyBitmap(skyBitmap);
  }

  /**
   * Gets the current sky offset.
   * 
   * @return the current sky offset
   */
  public float getSkyOffset() {
    return skyOffset;
  }

  /**
   * Sets a new sky offset value. This can be used for raising or lowering the sky, to match the desired part of the sky
   * bitmap to the horizon. The default value is configured so that the lower part of the sky bitmap matches the horizon
   * on the default draw distance. Positive values raise the sky, negative values lower it.
   * 
   * @param skyOffset
   *          the new sky offset
   */
  public void setSkyOffset(float skyOffset) {
    this.skyOffset = skyOffset;
  }

  /**
   * Gets the current background plane draw mode. Possible values: DRAW_NOTHING, DRAW_COLOR, DRAW_BITMAP.
   * 
   * @return the current background plane draw mode.
   */
  public int getBackgroundPlaneDrawMode() {
    return backgroundPlaneDrawMode;
  }

  /**
   * Sets a new background plane draw mode. Possible values:
   * <p>
   * <p>
   * DRAW_NOTHING - background plane doesn't get drawn. Areas that are not covered by map tiles will be colored by the
   * color set by setClearColor.
   * <p>
   * <p>
   * DRAW_COLOR - draws the background plane behind the map tiles. The color itself can be defined with
   * setBackgroundPlaneColor. Background plane bitmap is ignored.
   * <p>
   * <p>
   * DRAW_BITMAP - draw the background plane behind the map tiles. The bitmap is repeated over the visible area and gets
   * scaled with each zoom level. The bitmap can be defined with setBackgroundPlaneBitmap. Background plane color is
   * ignored.
   * 
   * @param backgroundPlaneDrawMode
   *          the new background plane draw mode
   */
  public void setBackgroundPlaneDrawMode(int backgroundPlaneDrawMode) {
    this.backgroundPlaneDrawMode = backgroundPlaneDrawMode;
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Gets the current background plane color.
   * 
   * @return the current background plane color
   */
  public Color getBackgroundPlaneColor() {
    return backgroundPlaneColor;
  }

  /**
   * Sets a new background plane color. See setBackgroundPlaneDrawMode for details.
   * 
   * @param backgroundPlaneColor
   *          the new background plane color
   */
  public void setBackgroundPlaneColor(int backgroundPlaneColor) {
    this.backgroundPlaneColor = new Color(backgroundPlaneColor);
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Sets a new background plane bitmap. See setBackgroundDrawMode for details. The width and height of the bitmap must
   * be power of two (for example: 256 * 256 or 128 * 512). It's also preferred if the bitmap was square (width ==
   * height), but this is not a requirement.
   * 
   * @param backgroundPlaneBitmap
   *          the new background plane bitmap.
   */
  public void setBackgroundPlaneBitmap(Bitmap backgroundPlaneBitmap) {
    components.mapRenderers.getMapRenderer().setBackgroundPlaneBitmap(backgroundPlaneBitmap);
  }
  
  /**
   * Gets the current background plane overlay draw mode. Possible values: DRAW_NOTHING, DRAW_COLOR, DRAW_BITMAP.
   * 
   * @return the current background plane draw mode.
   */
  public int getBackgroundPlaneOverlayDrawMode() {
    return backgroundPlaneOverlayDrawMode;
  }

  /**
   * Sets a new background plane overlay draw mode. Possible values:
   * <p>
   * <p>
   * DRAW_NOTHING - background plane doesn't get drawn. Areas that are not covered by map tiles will be colored by the
   * color set by setClearColor.
   * <p>
   * <p>
   * DRAW_COLOR - draws the background plane behind the map tiles. The color itself can be defined with
   * setBackgroundPlaneColor. Background plane bitmap is ignored.
   * <p>
   * <p>
   * DRAW_BITMAP - draw the background plane behind the map tiles. The bitmap is repeated over the visible area and gets
   * scaled with each zoom level. The bitmap can be defined with setBackgroundPlaneBitmap. Background plane color is
   * ignored.
   * 
   * @param backgroundPlaneOverlayDrawMode
   *          the new background plane draw mode
   */
  public void setBackgroundPlaneOverlayDrawMode(int backgroundPlaneOverlayDrawMode) {
    this.backgroundPlaneOverlayDrawMode = backgroundPlaneOverlayDrawMode;
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Get bounds for background plane overlay. Bounds are specified in base layer coordinates.
   *
   * @return bounds for overlay.
   */
  public Bounds getBackgroundPlaneOverlayBounds() {
    return backgroundPlaneOverlayBounds;
  }

  /**
   * Set bounds for background plane overlay. Bounds are specified in base layer coordinates.
   *
   * @param bounds
   *          bounds for overlay.
   */
  public void setBackgroundPlaneOverlayBounds(Bounds bounds) {
    this.backgroundPlaneOverlayBounds = bounds;
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Gets the current background plane overlay color.
   * 
   * @return the current background plane overlay color
   */
  public Color getBackgroundPlaneOverlayColor() {
    return backgroundPlaneOverlayColor;
  }

  /**
   * Sets a new background plane overlay color. See setBackgroundPlaneOverlayDrawMode for details.
   * 
   * @param backgroundPlaneOverlayColor
   *          the new background plane overlay color
   */
  public void setBackgroundPlaneOverlayColor(int backgroundPlaneOverlayColor) {
    this.backgroundPlaneOverlayColor = new Color(backgroundPlaneOverlayColor);
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Sets a new background plane overlay bitmap. See setBackgroundOverlayDrawMode for details. The width and height of the bitmap must
   * be power of two (for example: 256 * 256 or 128 * 512). It's also preferred if the bitmap was square (width ==
   * height), but this is not a requirement.
   * 
   * @param backgroundPlaneOverlayBitmap
   *          the new background plane bitmap.
   */
  public void setBackgroundPlaneOverlayBitmap(Bitmap backgroundPlaneOverlayBitmap) {
    components.mapRenderers.getMapRenderer().setBackgroundPlaneOverlayBitmap(backgroundPlaneOverlayBitmap);
  }
  
  /**
   * Gets the current background image draw mode. Possible values: DRAW_NOTHING, DRAW_BITMAP, DRAW_BACKDROP_BITMAP.
   * 
   * @return the current background image draw mode.
   */
  public int getBackgroundImageDrawMode() {
    return backgroundImageDrawMode;
  }

  /**
   * Sets a new background image draw mode. Possible values:
   * <p>
   * <p>
   * DRAW_NOTHING - background image doesn't get drawn. Areas that are not covered by map tiles or the background plane
   * will be colored by the color set by setClearColor.
   * <p>
   * <p>
   * DRAW_BITMAP - draw the background image behind the map tiles. The bitmap is drawn the same way as the first level
   * of the map, regardless of the camera position. The bitmap can be defined with setBackgroundImageBitmap.
   * <p>
   * <p>
   * DRAW_BACKDROP_BITMAP - draw static background image facing the camera. The bitmap can be defined with setBackgroundImageBitmap.
   * 
   * @param backgroundImageDrawMode
   *          the new background image draw mode
   */
  public void setBackgroundImageDrawMode(int backgroundImageDrawMode) {
    this.backgroundImageDrawMode = backgroundImageDrawMode;
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Sets a new background Image bitmap. See setBackgroundDrawMode for details. The width and height of the bitmap must
   * be power of two (for example: 256 * 256 or 128 * 512). It's also preferred if the bitmap was square (width ==
   * height), but this is not a requirement.
   * 
   * @param backgroundImageBitmap
   *          the new background Image bitmap.
   */
  public void setBackgroundImageBitmap(Bitmap backgroundImageBitmap) {
    components.mapRenderers.getMapRenderer().setBackgroundImageBitmap(backgroundImageBitmap);
  }

  /**
   * Gets the current clear color.
   * 
   * @return the current clear color
   */
  public Color getClearColor() {
    return clearColor;
  }

  /**
   * Sets the clear color. Clear color is the first component that gets drawn. This color will visible in all empty
   * areas of the screen. See setSkyDrawMode and setBackgroundPlaneDrawMode for details.
   * 
   * @param clearColor
   *          the new clear color
   */
  public void setClearColor(int clearColor) {
    this.clearColor = new Color(clearColor);
  }

  /**
   * Gets the current draw distance
   * 
   * @return the current draw distance
   */
  public float getDrawDistance() {
    return drawDistance;
  }

  /**
   * Sets a new draw distance. The higher the draw distance the more tiles can be seen, if the map is tilted. Changing
   * the draw distance will cause the horizon to move, which means that the if the sky is set to DRAW_BITMAP, the
   * horizon may not match up anymore. This can be corrected using setSkyOffset.
   * <p>
   * <p>
   * Increasing this value will decrease performance and increase network traffic, if online map is used.
   * 
   * @param drawDistance
   */
  public void setDrawDistance(float drawDistance) {
    this.drawDistance = drawDistance;
  }

  /**
   * Checks the state of tile fading.
   * 
   * @return true if tile fading is enabled
   */
  public boolean isTileFading() {
    return tileFading;
  }

  /**
   * Sets the tile fade parameter, default is disabled. If enabled new tiles will fade in smoothly, instead of appearing
   * out of thing air, at the cost of a performance hit. NOT IMPLEMENTED YET.
   * 
   * @param tileFading
   *          the new state of tile fading
   */
  public void setTileFading(boolean tileFading) {
    this.tileFading = tileFading;
  }

  /**
   * Checks the state of seamless horizontal panning.
   * 
   * @return true if seamless horizontal panning is enabled
   */
  public boolean isSeamlessHorizontalPan() {
    return seamlessHorizontalPan;
  }

  /**
   * Sets the state of seamless horizontal panning, default is disabled. If enabled, the user can scroll seamlessly from
   * the left side of the map to the right, and the other way around..
   * 
   * @param seemlessHorizontalPan
   *          the new state of seamless horizontal panning
   */
  public void setSeamlessHorizontalPan(boolean seemlessHorizontalPan) {
    this.seamlessHorizontalPan = seemlessHorizontalPan;
    components.mapRenderers.getMapRenderer().requestRenderView();
  }

  /**
   * Checks the state of zooming in on double click.
   * 
   * @return true if zooming in on double click is enabled
   */
  public boolean isDoubleClickZoomIn() {
    return doubleClickZoomIn;
  }

  /**
   * Set the state of zooming in on double click.
   * 
   * @param doubleClickZoomIn
   *          the new state of zooming in on double click
   */
  public void setDoubleClickZoomIn(boolean doubleClickZoomIn) {
    this.doubleClickZoomIn = doubleClickZoomIn;
  }

  /**
   * Checks the state of zooming out on dual click.
   * 
   * @return true if zooming out on dual click is enabled
   */
  public boolean isDualClickZoomOut() {
    return dualClickZoomOut;
  }

  /**
   * Set the state of zooming out on dual click.
   * 
   * @param dualClickZoomOut
   *          the new state of zooming out on dual click
   */
  public void setDualClickZoomOut(boolean dualClickZoomOut) {
    this.dualClickZoomOut = dualClickZoomOut;
  }

  /**
   * Checks the state of general panning mode.
   * 
   * @return true if general panning mode is enabled
   */
  public boolean isGeneralPanningMode() {
    return generalPanningMode;
  }

  /**
   * Set the state of general panning mode.
   * In general panning mode, points under both fingers are moved to keep them in sync with finger movements.
   * If this mode is switched off, map component tries to detect whether finger movement corresponds
   * to rotation or scaling gesture. This provides smoother experience but less control. 
   * 
   * @param generalPanningMode
   *          the new state of general panning mode
   */
  public void setGeneralPanningMode(boolean generalPanningMode) {
    this.generalPanningMode = generalPanningMode;
  }
  
  /**
   * Get the balance between zoom and rotation detection. Default is 0.0f, values greater than this favor zooming, values less favor rotation.
   * This flag assumes that general panning mode is disabled.
   * 
   * @return balance factor.
   */
  public float getZoomRotateDetectionBalance() {
    return zoomRotateDetectionBalance;
  }
  
  /**
   * Set the balance between zoom and rotation detection. Default is 0.0f, values greater than this favor zooming, values less favor rotation.
   * This flag assumes that general panning mode is disabled.
   * 
   * @param balance balance between zooming and rotation
   */
  public void setZoomRotateDetectionBalance(float balance) {
    this.zoomRotateDetectionBalance = balance;
  }
  
  /**
   * Checks the state of click type detection.
   * 
   * @return true if click type detection is enabled. false otherwise.
   */
  public boolean isClickTypeDetection() {
    return clickTypeDetection;
  }

  /**
   * Set the state for click type detection. If enabled (enabled by default), clicks
   * are categorized as normal clicks, double clicks, long clicks and dual clicks.
   * This resolving take about 400ms, so for applications that do not require such resolving,
   * it can be turned off.
   * 
   * @param clickTypeDetection
   *          the new state for click type detection
   */
  public void setClickTypeDetection(boolean clickTypeDetection) {
    this.clickTypeDetection = clickTypeDetection;
  }
  
  /**
   * Checks the state of FPS indicator.
   * 
   * @return true if FPS indicator is visible, false otherwise.
   */
  public boolean isFPSIndicator() {
    return fpsIndicator;
  }
  
  /**
   * Enable/disable FPS indicator. FPS indicator displays current frame rate as overlay on top of the view.
   * This provides rough estimate of the rendering performance and layer complexity.
   * Note: in order this to be meaningful, continuous rendermode should be used!
   * 
   * @param fpsIndicator
   *          true to display the indicator, false to hide.
   */
  public void setFPSIndicator(boolean fpsIndicator) {
    this.fpsIndicator = fpsIndicator;
  }

  /**
   * Gets the map listener.
   * 
   * @return the current map listener
   */
  public MapListener getMapListener() {
    return mapListener;
  }

  /**
   * Sets a new map listener.
   * 
   * @param mapListener
   *          the new map listener
   */
  public void setMapListener(final MapListener mapListener) {
    if (this.mapListener != mapListener) {
      this.mapListener = mapListener;
      CancelableThreadPool.StatusHandler handler = new StatusHandler(mapListener);
      this.components.rasterTaskPool.setStatusHandler(handler);
      this.components.vectorTaskPool.setStatusHandler(handler);
    }
  }

  /**
   * Get list of active map touch listeners.
   * 
   * @return list of map touch listener (if present) or null if no listeners are attached. The list is a unique copy of the actual list.
   */
  public List<MapTouchListener> getMapTouchListeners() {
    synchronized (mapTouchListeners) {
      if (mapTouchListeners.isEmpty()) {
        return null;
      }
      return new ArrayList<MapTouchListener>(mapTouchListeners);
    }
  }
  
  /**
   * Add map touch listener. Listener will be added as the first listener (potentially overriding others).
   * 
   * @param listener touch listener to add.
   */
  public void addMapTouchListener(MapTouchListener listener) {
    synchronized (mapTouchListeners) {
      mapTouchListeners.add(0, listener);
    }
  }

  /**
   * Remove map touch listener.
   * 
   * @param listener touch listener to remove.
   */
  public void removeMapTouchListener(MapTouchListener listener) {
    synchronized (mapTouchListeners) {
      mapTouchListeners.remove(listener);
    }
  }

  /**
   * Sets a new size limit for the texture memory cache in bytes. Texture cache is the primary storage for raster data,
   * all tiles contained within the texture cache are stored as uncompressed openGL textures and can immediately be
   * drawn to the screen. If the texture cache is not big enough to store all the visible and preloaded tiles, artifacts
   * start to show (such as disappearing tiles). The bigger the screen and the more raster layers there are, the bigger
   * the texture cache should be. Example: typical hdpi device (480x800) has up to 4x5 256-pixel tiles visible, 
   * depending on zoom, rotation and tilting. For 20 tiles x 256x256 pixels x 16 bits/pixel = 20 MB
   * is minimum. NB! Preloading = true setting doubles the requirement, and each raster overlay also requires extra 20MB.
   * Larger screens (tablets) need also extra.
   * 
   * @param textureMemoryCacheSize
   *          the new texture cache size in bytes
   */
  public void setTextureMemoryCacheSize(int textureMemoryCacheSize) {
    components.textureMemoryCache.setSize(textureMemoryCacheSize);
  }

  /**
   * Sets a new size limit for the compressed memory cache. Compressed cache is an optional secondary storage for raster
   * data, all tiles contained within the compressed cache are stored as either JPGs or PNGs in the memory.
   * 
   * 
   * @param compressedMemoryCacheSize
   *          the new compressed cache size
   */
  public void setCompressedMemoryCacheSize(int compressedMemoryCacheSize) {
    components.compressedMemoryCache.setSize(compressedMemoryCacheSize);
  }
  
  /**
   * Sets a new size limit for the persistent cache. Persistent cache is an optional tertiary storage for raster
   * data, all tiles contained within the persistent cache are stored as either JPGs or PNGs in a database in the user specified directory.
   * 
   * 
   * @param persistentCacheSize
   *          the new persistent cache size, in Bytes
   */
  public void setPersistentCacheSize(int persistentCacheSize) {
    components.persistentCache.setSize(persistentCacheSize);
  }
  
  /**
   * Sets a new database directory for the persistent cache. Persistent cache won't work until a path is specified.
   * 
   * 
   * @param persistentCachePath
   *          the new persistent cache database path
   */
  public void setPersistentCachePath(String persistentCachePath) {
    components.persistentCache.setPath(persistentCachePath);
  }

  /**
   * Sets a new size limit for the vector tile memory cache. 
   * The limit is specified as number of tiles, not bytes.
   * 
   * 
   * @param vectorTileCacheSize
   *          the new tile cache size (number of tiles)
   */
  public void setVectorTileCacheSize(int vectorTileCacheSize) {
    components.vectorTileCache.setSize(vectorTileCacheSize);
  }
  
  /**
   * Checks the state of kinetic panning.
   * 
   * @return true if kinetic panning is enabled
   */
  public boolean isKineticPanning() {
    return kineticPanning;
  }

  /**
   * Sets the state of kinetic panning. Default is disabled. Kinetic panning allows the map to move automatically using
   * the inertia of the last swipe, after the user has finished interacting with the touch screen.
   * 
   * @param kineticPanning new value of the kinetic panning flag.
   */
  public void setKineticPanning(boolean kineticPanning) {
    this.kineticPanning = kineticPanning;
  }

  /**
   * Checks the state of kinetic rotation.
   * 
   * @return true if kinetic rotation is enabled
   */
  public boolean isKineticRotation() {
    return kineticRotation;
  }

  /**
   * Sets the state of kinetic rotation. Default is disabled. Kinetic rotation allows the map to rotate automatically using
   * the inertia of the last rotation gesture.
   * 
   * @param kineticRotation new state of the kinetic rotation flag
   */
  public void setKineticRotation(boolean kineticRotation) {
    this.kineticRotation = kineticRotation;
  }

  /**
   * Gets the current tile size in pixels, it will be used when drawing the tiles.
   *
   * @return current tile size. Default is 256.
   */
  public int getTileSize() {
    return tileSize;
  }

  /**
   * Sets the new tile size in pixels, it will be used when drawing the tiles.
   * 
   * @param tileSize
   *          the new tile size in pixels
   */
  public void setTileSize(int tileSize) {
    this.tileSize = tileSize;
  }
  
  /**
   * Get the current zoom level bias for tiles.
   * 
   * @return current tile zoom level bias. Default is 0. 
   */
  public float getTileZoomLevelBias() {
    return tileZoomLevelBias;
  }
  
  /**
   * Set the current zoom level bias for tiles. Zoom level bias of 1 means that tiles with double resolution are used, -1 means that tiles with half resolution are used.
   * 
   * @param tileZoomLevelBias
   *          the new zoom level bias for tiles 
   */
  public void setTileZoomLevelBias(float tileZoomLevelBias) {
    if (this.tileZoomLevelBias != tileZoomLevelBias) {
      this.tileZoomLevelBias = tileZoomLevelBias;
      components.mapRenderers.getMapRenderer().frustumChanged();
    }
  }

  /**
   * Get renderer mode.
   * 
   * @return either NORMAL_RENDERMODE or STEREO_RENDERMODE
   * @pad.exclude
   */
  public int getRenderMode() {
    return renderMode;
  }
  
  /**
   * Set renderer mode. This function must be called BEFORE setting any rendering options.
   * 
   * @param renderMode
   *          either NORMAL_RENDERMODE or STEREO_RENDERMODE
   * @pad.exclude
   */
  public boolean setRenderMode(int renderMode) {
    if (renderMode != this.renderMode) {
      if (!components.mapRenderers.reset(renderProjection, renderMode)) {
        return false;
      }
      this.renderMode = renderMode;
    }
    return true;
  }
  
  /**
   * Get render projection.
   * 
   * @return either PLANAR_RENDERPROJECTION or SPHERICAL_RENDERPROJECTION
   */
  public int getRenderProjection() {
    return renderProjection;
  }
  
  /**
   * Set render projection. All view parameters are translated for new projection.
   * 
   * @param renderProjection
   *          either PLANAR_RENDERPROJECTION or SPHERICAL_RENDERPROJECTION
   */
  public void setRenderProjection(int renderProjection) {
    if (renderProjection != this.renderProjection) {
      this.renderProjection = renderProjection;
      components.mapRenderers.getMapRenderer().setRenderSurface(components.mapRenderers.createRenderSurface(renderProjection));
    }
  }

  /**
   * Get stereo effect relative strength.
   * 
   * @return relative strength. 1.0 is default.
   * @pad.exclude
   */
  public float getStereoModeStrength() {
    return stereoModeStrength;
  }

  /**
   * Set stereo effect relative strength.  Only works when renderer is set to stereo mode.
   * 
   * @param strength
   *          relative strength. 1.0 is default.
   * @pad.exclude
   */
  public void setStereoModeStrength(float strength) {
    this.stereoModeStrength = strength;
    components.mapRenderers.getMapRenderer().requestRenderView();
  }
  
  /**
   * Get the number of threads currently used in the raster task pool.
   * 
   * @return current raster task pool size.
   */
  public int getRasterTaskPoolSize() {
    return components.rasterTaskPool.getPoolSize();
  }

  /**
   * Set the number of threads used in the raster task pool. More threads means more tiles are downloaded in parallel.
   * Default is one.
   * 
   * @param poolSize
   *          new raster task pool size.
   */
  public void setRasterTaskPoolSize(int poolSize) {
    components.rasterTaskPool.setPoolSize(poolSize);
  }
  
}
