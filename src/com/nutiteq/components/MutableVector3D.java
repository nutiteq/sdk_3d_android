package com.nutiteq.components;

/**
 * Vector in 3D space.
 * This is mutable version of the Vector3D class.
 * It is intended for output parameters only and contains minimum number of operations.
 */
public class MutableVector3D {
  /**
   * x coordinate of the vector.
   */
  public double x;

  /**
   * y coordinate of the vector.
   */
  public double y;

  /**
   * z coordinate of the vector.
   */
  public double z;
  
  /**
   * Constructor for null vector.
   */
  public MutableVector3D() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }
  
  /**
   * Construct vector from 3 coordinates.
   * 
   * @param x
   *          x coordinate of the vector.
   * @param y
   *          y coordinate of the vector.
   * @param z
   *          z coordinate of the vector.
   */
  public MutableVector3D(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
   
  /**
   * Get vector components as array containing 3 elements.
   * 
   * @return array containing x, y, z coordinates.
   */
  public double[] toArray() {
    return new double[] { x, y, z };
  }

  @Override
  public String toString() {
    return "MutableVector3D [x=" + x + ", y=" + y + ", z=" + z +"]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MutableVector3D other = (MutableVector3D) o;
    return x == other.x && y == other.y && z == other.z;
  }

}
