package com.nutiteq.components;

/**
 * Map tile description for raster layers.
 */
public class MapTile {

  /**
   * x coordinate of the tile.
   */
  public final int x;
  
  /**
   * y coordinate of the tile.
   */
  public final int y;
  
  /*
   * Tile zoom level, starting from 0. At zoom level, there are 1x1 tiles. At level 1 there are 2x2 tiles, and so on.
   */
  public final int zoom;
  
  /**
   * Internal tile id.
   */
  public final long id;
  
  /**
   * Default constructor.
   * @param x
   *          tile x coordinate
   * @param y
   *          tile y coordinate
   * @param zoom
   *          tile zoom level
   * @param id
   *          tile id
   */
  public MapTile(int x, int y, int zoom, long id) {
    this.x = x;
    this.y = y;
    this.zoom = zoom;
    this.id = id;
  }

  @Override
  public String toString() {
    return "MapTile [x=" + x + ", y=" + y + ", zoom=" + zoom + "]";
  }

  @Override
  public int hashCode() {
    return (int) (id * 997) ^ (x *  199) ^ (y * 23) ^ zoom;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MapTile other = (MapTile) o;
    return x == other.x && y == other.y && zoom == other.zoom && id == other.id;
  }
}
