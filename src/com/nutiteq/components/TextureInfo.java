package com.nutiteq.components;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.nutiteq.utils.Const;
import com.nutiteq.utils.Utils;

/**
 * This class is used for drawing bitmaps as OpenGL textures.
 */
public class TextureInfo {
  /**
   * Clamp-to-edge wrap mode.
   */
  public static final int CLAMP = 0;

  /**
   * Repeat wrap mode.
   */
  public static final int REPEAT = 1;
  
  /**
   * @pad.exclude
   */
  public final Bitmap bitmap;

  /**
   * @pad.exclude
   */
  public final float[] texCoords;

  /**
   * @pad.exclude
   */
  public final FloatBuffer texCoordBuf;

  /**
   * @pad.exclude
   */
  public final float width;

  /**
   * @pad.exclude
   */
  public final float height;
  
  /**
   * pad.exclude
   */
  public final int wrapS;

  /**
   * pad.exclude
   */
  public final int wrapT;

  /**
   * Default constructor.
   * 
   * @param bitmap
   *          bitmap to embed. Bitmap dimensions must be power of 2!
   * @param texCoords
   *          texture coordinates for bitmap. Should contain 4 pairs of uv coordinates.
   * @param width
   *          bitmap screen width.
   * @param height
   *          bitmap screen height.
   */
  public TextureInfo(Bitmap bitmap, float[] texCoords, float width, float height) {
    this(bitmap, texCoords, width, height, CLAMP, CLAMP);
  }

  /**
   * Specialized constructor for the case when wrapping modes need to be explicitly set.
   * 
   * @param bitmap
   *          bitmap to embed. Bitmap dimensions must be power of 2!
   * @param texCoords
   *          texture coordinates for bitmap. Should contain 4 pairs of uv coordinates.
   * @param width
   *          bitmap screen width.
   * @param height
   *          bitmap screen height.
   * @param wrapS
   *          wrap mode for S or X texture coordinate. Either REPEAT or CLAMP.
   * @param wrapT
   *          wrap mode for T or Y texture coordinate. Either REPEAT or CLAMP.
   */
  public TextureInfo(Bitmap bitmap, float[] texCoords, float width, float height, int wrapS, int wrapT) {
    this.bitmap = bitmap;
    this.width = width;
    this.height = height;
    this.wrapS = wrapS;
    this.wrapT = wrapT;
    this.texCoords = texCoords.clone();

    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(texCoords.length * Float.SIZE / 8);
    byteBuffer.order(ByteOrder.nativeOrder());
    texCoordBuf = byteBuffer.asFloatBuffer();
    texCoordBuf.put(texCoords);
    texCoordBuf.position(0);
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public float getTexU0() {
    return texCoords[0];
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public float getTexV0() {
    return texCoords[1];
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public float getTexU1() {
    return texCoords[6];
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public float getTexV1() {
    return texCoords[7];
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public float getTexSU() {
    return texCoords[6] - texCoords[0];
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public float getTexSV() {
    return texCoords[7] - texCoords[1];
  }

  /**
   * Not part of public API.
   * Factory method for creating texture info from bitmap and relative size.
   * Bitmap will be internally resized to POT, so the resulting bitmap can be repeated.
   * @pad.exclude
   */
  public static TextureInfo createScaled(Bitmap bitmap, float size) {
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    int realWidth = Utils.upperPow2(width);
    int realHeight = Utils.upperPow2(height);
  
    float tx = 1.0f;
    float ty = 1.0f;
  
    float[] texCoords = new float[] { 0.0f, ty, tx, ty, 0.0f, 0.0f, tx, 0.0f };
  
    Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, realWidth, realHeight, true);
    Bitmap realBitmap = Bitmap.createBitmap(realWidth, realHeight, Config.ARGB_8888);
    realBitmap.setDensity(bitmap.getDensity());
    Canvas canvas = new Canvas(realBitmap);
    canvas.drawBitmap(bitmap2, 0, 0, null);
  
    return new TextureInfo(realBitmap, texCoords, size * Const.UNIT_SIZE, size * height / width * Const.UNIT_SIZE, REPEAT, REPEAT);
  }

  /**
   * Not part of public API.
   * Factory method for creating texture info from bitmap and relative size.
   * @pad.exclude
   */
  public static TextureInfo createFromSize(Bitmap bitmap, Rect rect, float size) {
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    int realWidth = Utils.upperPow2(width);
    int realHeight = Utils.upperPow2(height);
  
    float tx0 = (rect == null ? 0.0f : (float) rect.left / realWidth);
    float ty0 = (rect == null ? 0.0f : (float) rect.top / realHeight);
    float tx1 = (rect == null ? (float) width / realWidth : (float) rect.right / realWidth);
    float ty1 = (rect == null ? (float) height / realHeight : (float) rect.bottom / realHeight);
  
    float[] texCoords = new float[] { tx0, ty1, tx1, ty1, tx0, ty0, tx1, ty0 };
  
    Bitmap realBitmap = Bitmap.createBitmap(realWidth, realHeight, Config.ARGB_8888);
    realBitmap.setDensity(bitmap.getDensity());
    Canvas canvas = new Canvas(realBitmap);
    canvas.drawBitmap(bitmap, 0, 0, null);
  
    return new TextureInfo(realBitmap, texCoords, size * Const.UNIT_SIZE, size * height / width * Const.UNIT_SIZE, CLAMP, CLAMP);
  }

  /**
   * Not part of public API.
   * Factory method for creating texture info from bitmap and relative size and stretching factor.
   * Bitmap will be internally resized to POT along Y axis, so the resulting bitmap can be repeated along V coordinate.
   * @pad.exclude
   */
  public static TextureInfo createFromSizeAndStretch(Bitmap bitmap, float size, float stretch) {
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    int realWidth = Utils.upperPow2(width);
    int realHeight = Utils.upperPow2(height);
  
    float tx = (float) width / realWidth;
    float ty = 1.0f / stretch * width / height;
  
    float[] texCoords = new float[] { 0.0f, ty, tx, ty, 0.0f, 0.0f, tx, 0.0f };
  
    Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, width, realHeight, true);
    Bitmap realBitmap = Bitmap.createBitmap(realWidth, realHeight, Config.ARGB_8888);
    realBitmap.setDensity(bitmap.getDensity());
    Canvas canvas = new Canvas(realBitmap);
    canvas.drawBitmap(bitmap2, 0, 0, null);
  
    return new TextureInfo(realBitmap, texCoords, size * Const.UNIT_SIZE, size * height / width * Const.UNIT_SIZE, CLAMP, REPEAT);
  }

}
