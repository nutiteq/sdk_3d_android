package com.nutiteq.components;

/**
 * Wrapper for vector tile data.
 */
public class TileData {
  private final byte[] data;

  /**
   * Default constructor.
   * 
   * @param data
   *          data as byte array.
   */
  public TileData(byte[] data) {
    this.data = data;
  }
 
  /**
   * Get tile data.
   * 
   * @return data as byte array.
   */
  public byte[] getData() {
    return data;
  }
}
