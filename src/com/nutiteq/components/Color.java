package com.nutiteq.components;

/**
 * Color, represented in RGB space, with additional alpha component.
 */
public class Color {
  /**
   * 32-bit color value for black color.
   */
  public static final int BLACK = 0xFF000000;

  /**
   * 32-bit color value for red color.
   */
  public static final int RED = 0xFFFF0000;

  /**
   * 32-bit color value for green color.
   */
  public static final int GREEN = 0xFF00FF00;

  /**
   * 32-bit color value for blue color.
   */
  public static final int BLUE = 0xFF0000FF;

  /**
   * 32-bit color value for white color.
   */
  public static final int WHITE = 0xFFFFFFFF;

  /**
   * Red component, 0..1
   */
  public final float r;

  /**
   * Green component, 0..1
   */
  public final float g;

  /**
   * Blue component, 0..1
   */
  public final float b;

  /**
   * Alpha component, 0..1
   */
  public final float a;
  
  /**
   * Color encoded in 32-bits as ARGB (8 bits per component).
   */
  public final int colorInt;

  /**
   * Default constructor.
   * 
   * @param color
   *          color encoded in 32-bits as ARGB.
   */
  public Color(int color) {
    this.colorInt = color;
    a = ((color >> 24) & 0xFF) / 255f;
    r = ((color >> 16) & 0xFF) / 255f;
    g = ((color >> 8) & 0xFF) / 255f;
    b = (color & 0xFF) / 255f;
  }

  /**
   * Construct color from red, green and blue components.
   * 
   * @param r
   *          red component of the color. Must be between 0 and 255.
   * @param g
   *          green component of the color. Must be between 0 and 255.
   * @param b
   *          blue component of the color. Must be between 0 and 255.
   */
  public Color(int r, int g, int b) {
    this(argb(0xFF, r, g, b));
  }
  
  /**
   * Construct color from red, green and blue and alpha components.
   * Note: alpha is interpreted as non-premultiplied transparency value, 
   * thus to make half-transparent green, use argb(128, 0, 255, 0). 
   * 
   * @param r
   *          red component of the color. Must be between 0 and 255.
   * @param g
   *          green component of the color. Must be between 0 and 255.
   * @param b
   *          blue component of the color. Must be between 0 and 255.
   * @param a
   *          alpha component of the color. Must be between 0 and 255.
   */
  public Color(int r, int g, int b, int a) {
    this(argb(a, r, g, b));
  }
  
  /**
   * Calculate color value from ARGB (Alpha, Red, Green, Blue) components.
   * Note: alpha is interpreted as non-premultiplied transparency value, 
   * thus to make half-transparent green, use argb(128, 0, 255, 0). 
   * 
   * @param a
   *          alpha component (0..255)
   * @param r
   *          red component (0..255)
   * @param g
   *          green component (0..255)
   * @param b
   *          blue component (0..255)
   * @return color value
   */
  public static int argb(int a, int r, int g, int b) {
    return ((a & 0xFF) << 24) | ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | ((b & 0xFF) << 0);
  }

  /**
   * Get red component of the color.
   * 
   * @return value of red component (0..255)
   */
  public int getRed() {
    return (int) (r * 255f);
  }

  /**
   * Get green component of the color.
   * 
   * @return value of green component (0..255)
   */
  public int getGreen() {
    return (int) (g * 255f);
  }

  /**
   * Get blue component of the color.
   * 
   * @return value of blue component (0..255)
   */
  public int getBlue() {
    return (int) (b * 255f);
  }
  
  /**
   * Get alpha component of the color.
   * 
   * @return value of alpha component (0..255)
   */
  public int getAlpha() {
    return (int) (a * 255f);
  }
  
  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    Color other = (Color) o;
    return colorInt == other.colorInt;
  }

  @Override
  public int hashCode() {
    return colorInt;
  }

  @Override
  public String toString() {
    return "Color [r=" + r + ", g=" + g + ", b=" + b + ", a=" + a + "]";
  }
}
