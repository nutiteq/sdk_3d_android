package com.nutiteq.components;

/**
 * Vector in 3D space. This is mutable version of the Vector class.
 * This class is deprecated and has been superseded by Vector3D.
 */
public class MutableVector {
  /**
   * x coordinate of the vector.
   */
  public double x;

  /**
   * y coordinate of the vector.
   */
  public double y;

  /**
   * z coordinate of the vector.
   */
  public double z;
  
  /**
   * Constructor for null vector.
   */
  public MutableVector() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }
  
  /**
   * Construct vector from 3 coordinates.
   * 
   * @param x
   *          x coordinate of the vector.
   * @param y
   *          y coordinate of the vector.
   * @param z
   *          z coordinate of the vector.
   */
  public MutableVector(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  /**
   * Construct vector from coordinate array. Array must contain 3 coordinates.
   *  
   * @param components
   *          array containing x, y, z coordinates.
   */
  public MutableVector(double[] components) {
    this.x = components[0];
    this.y = components[1];
    this.z = components[2];
  }

  /**
   * Construct vector by copying existing vector.
   *  
   * @param vec
   *          existing vector to copy.
   */
  public MutableVector(Vector vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }

  /**
   * Construct vector by copying existing vector.
   *  
   * @param vec
   *          existing vector to copy.
   */
  public MutableVector(MutableVector vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }

  /**
   * Set vector x, y coordinate. Do not change z coordinate.
   * 
   * @param x
   *          new x coordinate.
   * @param y
   *          new y coordinate.
   */
  public void setCoords(double x, double y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Set vector x, y, z coordinates.
   * 
   * @param x
   *          new x coordinate.
   * @param y
   *          new y coordinate.
   * @param z
   *          new z coordinate.
   */
  public void setCoords(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  /**
   * Copy vector coordinate from another vector.
   * 
   * @param vec
   *          another vector to copy.
   */
  public void setCoords(Vector vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }

  /**
   * Copy vector coordinate from another vector.
   * 
   * @param vec
   *          another vector to copy.
   */
  public void setCoords(MutableVector vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }
 
  /**
   * Get vector components as array containing 3 elements.
   * 
   * @return array containing x, y, z coordinates.
   */
  public double[] toArray() {
    return new double[] { x, y, z };
  }

  @Override
  public String toString() {
    return "MutableVector [x=" + x + ", y=" + y + ", z=" + z +"]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MutableVector other = (MutableVector) o;
    return x == other.x && y == other.y && z == other.z;
  }

}
