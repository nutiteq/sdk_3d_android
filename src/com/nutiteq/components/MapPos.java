package com.nutiteq.components;

/**
 * Map position defined using three coordinates. X and y coordinates denote positions on the map,
 * while z coordinate is height from the ground plane. Actual units for x, y and z depend on map projection.
 * For example, in EPSG:4326 x is used for latitude and y is used for longitude.
 */
public class MapPos {
  /**
   * x coordinate of the position.
   */
  public final double x;

  /**
   * y coordinate of the position.
   */
  public final double y;

  /**
   * z coordinate of the position.
   */
  public final double z;

  /**
   * Construct map position from 2 coordinates. The z coordinate will be 0.
   *  
   * @param x
   *          x coordinate.
   * @param y
   *          y coordinate.
   */
  public MapPos(double x, double y) {
    this.x = x;
    this.y = y;
    this.z = 0;
  }

  /**
   * Construct map position from 3 coordinates.
   * 
   * @param x
   *          x coordinate.
   * @param y
   *          y coordinate.
   * @param z
   *          z coordinate.
   */
  public MapPos(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  /**
   * Construct map position from coordinate array. Array must contain 3 coordinates.
   *  
   * @param components
   *          array containing x, y, z coordinates.
   */
  public MapPos(double[] components) {
    this.x = components[0];
    this.y = components[1];
    this.z = components[2];
  }

  /**
   * Construct map position by copying existing position.
   * 
   * @param mapPos
   *          position to copy.
   */
  public MapPos(MapPos mapPos) {
    this.x = mapPos.x;
    this.y = mapPos.y;
    this.z = mapPos.z;
  }

  /**
   * Construct map position by copying existing position.
   * 
   * @param mapPos
   *          position to copy.
   */
  public MapPos(MutableMapPos mapPos) {
    this.x = mapPos.x;
    this.y = mapPos.y;
    this.z = mapPos.z;
  }

  /**
   * Get array containing 3 coordinates.
   * 
   * @return array containing x, y, z coordinates.
   */
  public double[] toArray() {
    return new double[] { x, y, z };
  }

  @Override
  public String toString() {
    return "MapPos [x=" + x + ", y=" + y + ", z=" + z +"]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MapPos other = (MapPos) o;
    return x == other.x && y == other.y && z == other.z;
  }

}
