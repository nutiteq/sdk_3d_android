package com.nutiteq.components;

/**
 * A container class that defines a range using minimum and maximum values.
 * 
 * @author Nutiteq
 * 
 */
public class Range {
  /**
   * Range minimum value.
   */
  public final float min;

  /**
   * Range maximum value.
   */
  public final float max;

  /**
   * Class constructor, that uses minimum and maximum values to defined the range.
   * 
   * @param min
   *          range minimum.
   * @param max
   *          range maximum.
   */
  public Range(float min, float max) {
    this.min = min;
    this.max = max;
  }

  /**
   * Return length of the range.
   * 
   * @return length of the range.
   */
  public float length() {
    return max - min;
  }

  @Override
  public String toString() {
    return "Range [min=" + min + ", max=" + max + "]";
  }
  
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    Range other = (Range) o;
    return min == other.min && max == other.max;
  }
}
