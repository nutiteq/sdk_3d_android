package com.nutiteq.components;

import com.nutiteq.utils.Frustum;
import com.nutiteq.utils.Matrix;

/**
 * Camera state for the current view.
 */
public class CameraState {
  /**
   * Origin point of the camera.
   */
  public final Point3D cameraPos;
  
  /**
   * Focus point of the camera.
   */
  public final Point3D focusPoint;
  
  /**
   * Normalized up vector for the camera.
   */
  public final Vector3D upVector;
  
  /**
   * Camera frustum.
   */
  public final Frustum frustum;
  
  /**
   * Modelview matrix for the camera.
   */
  public final double[] modelviewMatrix;
  
  /**
   * Inverse of the modelview matrix without translation component.
   */
  public final double[] invViewMatrix;
  
  /**
   * Projection matrix for the camera.
   */
  public final float[] projectionMatrix;
  
  /**
   * Concatenated projection and modelview matrices.
   */
  public final double[] modelviewProjectionMatrix;
  
  /**
   * Camera rotation in degrees.
   */
  public final float rotationDeg;
  
  /**
   * Camera tilt in degrees.
   */
  public final float tiltDeg;
  
  /**
   * Camera zoom level.
   */
  public final float zoom;
  
  /**
   * Camera zoom level as power of 2.
   */
  public final float zoomPow2;
  
  /**
   * Near plane distance
   */
  public final float nearPlane;

  /**
   * Near plane distance
   */
  public final float farPlane;

  /**
   * Default constructor
   * 
   * @param cameraPos
   *          camera origin
   * @param upVector
   *          camera up vector
   * @param modelviewMatrix
   *          modelview matrix
   * @param projectionMatrix
   *          projection matrix
   * @param rotationDeg
   *          rotation in degrees
   * @param tiltDeg
   *          tilt in degrees
   * @param zoom
   *          camera zoom level
   * @param nearPlane
   *          near plane distance
   * @param farPlane
   *          far plane distance
   */
  public CameraState(Point3D cameraPos, Point3D focusPoint, Vector3D upVector, double[] modelviewMatrix, float[] projectionMatrix, float rotationDeg, float tiltDeg, float zoom, float nearPlane, float farPlane) {
    this.cameraPos = cameraPos;
    this.focusPoint = focusPoint;
    this.upVector = upVector;
    this.frustum = new Frustum(projectionMatrix, modelviewMatrix);
    this.modelviewMatrix = modelviewMatrix.clone();
    this.projectionMatrix = projectionMatrix.clone();
    double[] projectionMatrixDbl = new double[16];
    Matrix.floatToDoubleM(projectionMatrixDbl, 0, projectionMatrix, 0);
    this.modelviewProjectionMatrix = new double[16];
    Matrix.multiplyMM(this.modelviewProjectionMatrix, 0, projectionMatrixDbl, 0, modelviewMatrix, 0);
    this.invViewMatrix = new double[16];
    Matrix.invertM(invViewMatrix, 0, modelviewMatrix, 0);
    invViewMatrix[12] = invViewMatrix[13] = invViewMatrix[14] = 0;
    this.rotationDeg = rotationDeg;
    this.tiltDeg = tiltDeg;
    this.zoom = zoom;
    this.zoomPow2 = (float) Math.pow(2, zoom);
    this.nearPlane = nearPlane;
    this.farPlane = farPlane;
  }
}
