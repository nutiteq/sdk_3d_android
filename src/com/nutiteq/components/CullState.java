package com.nutiteq.components;

import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.utils.Const;

/**
 * View-specific state determined by the culling process. This includes convex envelope of the visible area, zoom level, camera state. 
 */
public class CullState {
  /**
   * Envelope for the visible area in internal coordinates.
   */
  public final Envelope envelope;
  
  /**
   * Discrete zoom level for the view. Note: this is calculated from camera state
   */
  public final int zoom;
  
  /**
   * Camera state for the view
   */
  public final CameraState camera;
  
  /**
   * Current render projection
   */
  public RenderProjection renderProjection;

  /**
   * Default constructor
   * 
   * @param envelope
   *          envelope for the visible area.
   * @param camera
   *          camera parameters for the view.
   * @param renderProjection
   *          current rendering projection
   */
  public CullState(Envelope envelope, CameraState camera, RenderProjection renderProjection) {
    this.envelope = envelope;
    this.zoom = (int) (camera.zoom + Const.DISCRETE_ZOOM_LEVEL_BIAS);
    this.camera = camera;
    this.renderProjection = renderProjection;
  }
}
