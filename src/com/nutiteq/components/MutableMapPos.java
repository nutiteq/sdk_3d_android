package com.nutiteq.components;

/**
 * Map position defined using three coordinates. X and y coordinates denote positions on the map,
 * while z coordinate is height from the ground plane. Actual units for x, y and z depend on map projection.
 * For example, in EPSG:4326 x is used for latitude and y is used for longitude.
 * This is mutable version of MapPos class.
 */
public class MutableMapPos {
  /**
   * x coordinate of the position.
   */
  public double x;

  /**
   * y coordinate of the position.
   */
  public double y;

  /**
   * z coordinate of the position.
   */
  public double z;

  /**
   * Default constructor. Set all coordinates to 0.
   */
  public MutableMapPos() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }

  /**
   * Construct map position from 2 coordinates. The z coordinate will be 0.
   *  
   * @param x
   *          x coordinate.
   * @param y
   *          y coordinate.
   */
  public MutableMapPos(double x, double y) {
    this.x = x;
    this.y = y;
    this.z = 0;
  }

  /**
   * Construct map position from 3 coordinates.
   * 
   * @param x
   *          x coordinate.
   * @param y
   *          y coordinate.
   * @param z
   *          z coordinate.
   */
  public MutableMapPos(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  /**
   * Construct map position from coordinate array. Array must contain 3 coordinates.
   *  
   * @param components
   *          array containing x, y, z coordinates.
   */
  public MutableMapPos(double[] components) {
    this.x = components[0];
    this.y = components[1];
    this.z = components[2];
  }

  /**
   * Construct map position by copying existing position.
   * 
   * @param mapPos
   *          position to copy.
   */
  public MutableMapPos(MutableMapPos mapPos) {
    this.x = mapPos.x;
    this.y = mapPos.y;
    this.z = mapPos.z;
  }

  /**
   * Construct map position by copying existing position.
   * 
   * @param mapPos
   *          position to copy.
   */
  public MutableMapPos(MapPos mapPos) {
    this.x = mapPos.x;
    this.y = mapPos.y;
    this.z = mapPos.z;
  }

  /**
   * Set x, y coordinates of the position. z coordinate will be left unchanged.
   * 
   * @param x
   *          new x coordinate of the position.
   * @param y
   *          new y coordinate of the position.
   */
  public void setCoords(double x, double y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Set x, y, z coordinates of the position.
   * 
   * @param x
   *          new x coordinate of the position.
   * @param y
   *          new y coordinate of the position.
   * @param z
   *          new z coordinate of the position.
   */
  public void setCoords(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  /**
   * Copy coordinates from another map position.
   * 
   * @param mapPos
   *          the map position to copy.
   */
  public void setCoords(MapPos mapPos) {
    this.x = mapPos.x;
    this.y = mapPos.y;
    this.z = mapPos.z;
  }

  /**
   * Copy coordinates from another map position.
   * 
   * @param mapPos
   *          the map position to copy.
   */
  public void setCoords(MutableMapPos mapPos) {
    this.x = mapPos.x;
    this.y = mapPos.y;
    this.z = mapPos.z;
  }

  /**
   * Get array containing 3 coordinates.
   * 
   * @return array containing x, y, z coordinates.
   */
  public double[] toArray() {
    return new double[] { x, y, z };
  }

  @Override
  public String toString() {
    return "MutableMapPos [x=" + x + ", y=" + y + ", z=" + z +"]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MutableMapPos other = (MutableMapPos) o;
    return x == other.x && y == other.y && z == other.z;
  }

}
