package com.nutiteq.components;

/**
 * Mutable 3D point.
 * Contains almost no operations, used for output arguments.
 */
public class MutablePoint3D {
  /**
   * x coordinate of the position.
   */
  public double x;

  /**
   * y coordinate of the position.
   */
  public double y;

  /**
   * z coordinate of the position.
   */
  public double z;

  /**
   * Default constructor. Set all coordinates to 0.
   */
  public MutablePoint3D() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }

  /**
   * Construct point from 3 coordinates.
   * 
   * @param x
   *          x coordinate.
   * @param y
   *          y coordinate.
   * @param z
   *          z coordinate.
   */
  public MutablePoint3D(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  /**
   * Get array containing 3 coordinates.
   * 
   * @return array containing x, y, z coordinates.
   */
  public double[] toArray() {
    return new double[] { x, y, z };
  }

  @Override
  public String toString() {
    return "MutablePoint3D [x=" + x + ", y=" + y + ", z=" + z +"]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    MutablePoint3D other = (MutablePoint3D) o;
    return x == other.x && y == other.y && z == other.z;
  }

}
