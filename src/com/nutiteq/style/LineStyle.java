package com.nutiteq.style;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

import com.nutiteq.components.Color;
import com.nutiteq.components.TextureInfo;
import com.nutiteq.utils.Const;

/**
 * Style for lines. Includes line color, texture, width and other attributes.
 */
public class LineStyle extends Style {
  /**
   * Disabled line join mode. This is fastest and recommended for narrow lines. 
   */
  public static final int NO_LINEJOIN = 0;

  /**
   * Bevel line join mode. Does not work for lines that are wide but very short. 
   */
  public static final int BEVEL_LINEJOIN = 1;

  /**
   * Round line join mode. Does not work for lines that are wide but very short. 
   */
  public static final int ROUND_LINEJOIN = 2;
  
  /**
   * Butt line cap mode - line ends abruptly at specified position. This is fastest and recommended for narrow lines. 
   */
  public static final int BUTT_LINECAP = 0;
  
  /**
   * Square line cap mode - provides square cap at line end points.
   */
  public static final int SQUARE_LINECAP = 1;

  /**
   * Round line cap mode - provides round cap at line end points.
   */
  public static final int ROUND_LINECAP = 2;
  
  /**
   * @pad.exclude
   */
  public final TextureInfo textureInfo;

  /**
   * @pad.exclude
   */
  public final PointStyle pointStyle;

  /**
   * @pad.exclude
   */
  public final float pickingWidth;
  
  /**
   * @pad.exclude
   */
  public final int lineJoinMode; // NO_LINEJOIN, ...
  
  /**
   * @pad.exclude
   */
  public final int lineCapMode; // BUTT_LINECAP, ...

  /**
   * Default constructor.
   * 
   * @param builder
   *          style builder.
   */
  public LineStyle(Builder<?> builder) {
    super(builder);
    this.textureInfo = TextureInfo.createFromSizeAndStretch(builder.bitmap, builder.width, builder.stretch);
    this.pointStyle = builder.pointStyle;
    this.pickingWidth = (builder.pickingWidth != null ? builder.pickingWidth : builder.width * 5) * Const.UNIT_SIZE;
    this.lineJoinMode = builder.lineJoinMode;
    this.lineCapMode = builder.lineCapMode;
  }

  /**
   * Style builder class for LineStyle.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> extends Style.Builder<T> {
    private static final int DEFAULT_COLOR = Color.WHITE;
    private static final float DEFAULT_WIDTH = 0.1f;
    private static final int DEFAULT_BITMAP_SIZE = 16;
    private static Bitmap DEFAULT_BITMAP;
    static {
      DEFAULT_BITMAP = Bitmap.createBitmap(16, 1, Config.ARGB_8888);
      for (int i = 0; i < DEFAULT_BITMAP_SIZE; i++) {
        float x = 2 * (i + 0.5f) / DEFAULT_BITMAP_SIZE - 1;
        int c = (int) (255.0f * Math.max(0.0f, Math.min(1.0f, 1.5f - 1.75f * Math.abs(x * x)))); 
        DEFAULT_BITMAP.setPixel(i, 0, Color.argb(c, 255, 255, 255));
      }
    }
    
    protected Bitmap bitmap = DEFAULT_BITMAP;
    protected float width = DEFAULT_WIDTH;
    protected Float pickingWidth = null;
    protected float stretch = 1;
    protected int lineJoinMode = BEVEL_LINEJOIN;
    protected int lineCapMode = BUTT_LINECAP;
    
    protected PointStyle pointStyle;

    /**
     * Default constructor.
     */
    public Builder() {
      color = DEFAULT_COLOR;
    }

    /**
     * Set bitmap for line texture.
     * 
     * @param bitmap
     *          bitmap to use as a texture. 16x1 bitmap gives usually good results. 
     * @return builder itself.
     */
    public T setBitmap(Bitmap bitmap) {
      this.bitmap = bitmap;
      return self();
    }

    /**
     * Set line width.
     * 
     * @param width
     *          relative line width. Default value is 0.1f.
     * @return builder itself.
     */
    public T setWidth(float width) {
      this.width = width;
      return self();
    }
    
    /**
     * Set line width for picking (for detecting clicks on lines). Picking width should be larger than normal width in most cases.
     * 
     * @param width
     *          relative picking line width. Default value is 5x line width.
     * @return builder itself.
     */
    public T setPickingWidth(float width) {
      this.pickingWidth = width;
      return self();
    }
    
    /**
     * Set relative stretching coefficient. Texture on the line will be stretched according to this along the length of the line.
     * This is is important only for lines that have MxN textures where N>1.
     * 
     * @param stretch
     *          relative stretching coefficient. Default is 1.0f.
     * @return builder itself.
     */
    public T setStretch(float stretch) {
      this.stretch = stretch;
      return self();
    }
    
    /**
     * Set style for line end points.
     * 
     * @param pointStyle
     *          style to use for end points. If null (which is default), no end points will be drawn.
     * @return builder itself.
     */
    public T setPointStyle(PointStyle pointStyle) {
      this.pointStyle = pointStyle;
      return self();
    }

    /**
     * Set the mode for joining line end points.
     * 
     * @param lineJoinMode
     *          mode to use for joining line end points (NO_LINEJOIN, BEVEL_LINEJOIN, ROUND_LINEJOIN). Default is BEVEL_LINEJOIN.
     * @return builder itself.
     */
    public T setLineJoinMode(int lineJoinMode) {
      this.lineJoinMode = lineJoinMode;
      return self();
    }

    /**
     * Set the mode for line caps (end points).
     * 
     * @param lineCapMode
     *          drawing style for line end points (BUTT_LINECAP, SQUARE_LINECAP, ROUND_LINECAP). Default is BUTT_LINECAP.
     * @return builder itself.
     */
    public T setLineCapMode(int lineCapMode) {
      this.lineCapMode = lineCapMode;
      return self();
    }

    /**
     * Build new instance of LineStyle.
     * 
     * @return new instance of LineStyle.
     */
    public LineStyle build() {
      return new LineStyle(this);
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
