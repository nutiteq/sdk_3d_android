package com.nutiteq.style;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

import com.nutiteq.components.Color;
import com.nutiteq.components.TextureInfo;
import com.nutiteq.utils.Const;

/**
 * Style for points. Includes color, texture, size and other attributes.
 */
public class PointStyle extends Style {
  /**
   * @pad.exclude
   */
  public final TextureInfo textureInfo;

  /**
   * @pad.exclude
   */
  public final float pickingSize;

  /**
   * Default constructor.
   * 
   * @param builder
   *          style builder.
   */
  public PointStyle(Builder<?> builder) {
    super(builder);
    this.textureInfo = TextureInfo.createFromSize(builder.bitmap, null, builder.size);
    this.pickingSize = (builder.pickingSize != null ? builder.pickingSize : builder.size) * Const.UNIT_SIZE;
  }

  /**
   * Style builder class for PointStyle.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> extends Style.Builder<T> {
    private static final int DEFAULT_COLOR = Color.WHITE;
    private static final float DEFAULT_SIZE = 0.2f;
    private static final int DEFAULT_BITMAP_SIZE = 16;
    private static Bitmap DEFAULT_BITMAP;
    static {
      DEFAULT_BITMAP = Bitmap.createBitmap(DEFAULT_BITMAP_SIZE, DEFAULT_BITMAP_SIZE, Config.ARGB_8888);
      for (int i = 0; i < DEFAULT_BITMAP_SIZE; i++) {
        for (int j = 0; j < DEFAULT_BITMAP_SIZE; j++) {
          float x = 2 * (i + 0.5f) / DEFAULT_BITMAP_SIZE - 1;
          float y = 2 * (j + 0.5f) / DEFAULT_BITMAP_SIZE - 1;
          int c = 0;
          if (x * x + y * y < 1) {
            c = (int) (255.0f * Math.max(0.0f, Math.min(1.0f, 1.5f - 1.75f * Math.abs(x * x + y * y))));
          }
          DEFAULT_BITMAP.setPixel(i, j, Color.argb(c, 255, 255, 255));
        }
      }
    }

    private Bitmap bitmap = DEFAULT_BITMAP;
    private float size = DEFAULT_SIZE;
    private Float pickingSize = null;

    /**
     * Default constructor.
     */
    public Builder() {
      color = DEFAULT_COLOR;
    }

    /**
     * Set bitmap for point texture.
     * 
     * @param bitmap
     *          bitmap to use.
     * @return builder instance.
     */
    public T setBitmap(Bitmap bitmap) {
      this.bitmap = bitmap;
      return self();
    }

    /**
     * Set point size.
     * 
     * @param size
     *          relative point size. Default value is 0.2f.
     * @return builder itself.
     */
    public T setSize(float size) {
      this.size = size;
      return self();
    }

    /**
     * Set point size for picking (for detecting clicks on points). Picking size should be at least as large as size in most cases.
     * 
     * @param size
     *          relative picking size. Default value is equal to size.
     * @return builder itself.
     */
    public T setPickingSize(float size) {
      this.pickingSize = size;
      return self();
    }

    /**
     * Build new instance of PointStyle.
     * 
     * @return new instance of PointStyle.
     */
    public PointStyle build() {
      return new PointStyle(this);
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
