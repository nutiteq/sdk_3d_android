package com.nutiteq.style;

import com.nutiteq.components.Color;

/**
 * Style for 3D polygons. Includes color attribute.
 */
public class Polygon3DStyle extends Style {

  /**
   * Default constructor.
   * 
   * @param builder
   *          style builder.
   */
  public Polygon3DStyle(Builder<?> builder) {
    super(builder);
  }

  /**
   * Style builder class for Polygon3DStyle.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> extends Style.Builder<T> {
    private static final int DEFAULT_COLOR = Color.WHITE;

    /**
     * Default constructor.
     */
    public Builder() {
      color = DEFAULT_COLOR;
    }

    /**
     * Build new instance of Polygon3DStyle.
     * 
     * @return new instance of Polygon3DStyle.
     */
    public Polygon3DStyle build() {
      return new Polygon3DStyle(this);
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
