package com.nutiteq.style;

import android.graphics.Paint.Align;
import android.graphics.Typeface;

import com.nutiteq.components.Color;

/**
 * Style for vector element labels. Includes attributes for overall shape, text colors, fonts and sizes.
 */
public class LabelStyle {
  /**
   * @pad.exclude
   */
  public final int edgePadding;

  /**
   * @pad.exclude
   */
  public final int linePadding;

  /**
   * @pad.exclude
   */
  public final int roundedOutside;

  /**
   * @pad.exclude
   */
  public final int roundedInside;

  /**
   * @pad.exclude
   */
  public final int borderSize;

  /**
   * @pad.exclude
   */
  public final int triangleSize;

  /**
   * @pad.exclude
   */
  public final float titleSize;

  /**
   * @pad.exclude
   */
  public final float descriptionSize;

  /**
   * @pad.exclude
   */
  public final Typeface titleFont;

  /**
   * @pad.exclude
   */
  public final Typeface descriptionFont;
  
  /**
   * @pad.exclude
   */
  public final Align titleAlign;

  /**
   * @pad.exclude
   */
  public final Align descriptionAlign;

  /**
   * @pad.exclude
   */
  public final int descriptionWrapWidth;
  
  /**
   * @pad.exclude
   */
  public final int backgroundColor;

  /**
   * @pad.exclude
   */
  public final int borderColor;

  /**
   * @pad.exclude
   */
  public final int titleColor;

  /**
   * @pad.exclude
   */
  public final int descriptionColor;
  
  /**
   * @pad.exclude
   */
  public final float alpha;

  /**
   * Default constructor.
   * 
   * @param builder
   *          builder instance.
   */
  protected LabelStyle(Builder<?> builder) {
    this.edgePadding = builder.edgePadding;
    this.linePadding = builder.linePadding;

    this.roundedOutside = builder.borderRadius;
    this.roundedInside = Math.max(0, builder.borderRadius - builder.borderSize);
    this.borderSize = builder.borderSize;
    this.triangleSize = builder.triangleSize;

    this.titleSize = builder.titleSize;
    this.descriptionSize = builder.descriptionSize;
    this.titleFont = builder.titleFont;
    this.descriptionFont = builder.descriptionFont;
    this.titleAlign = builder.titleAlign;
    this.descriptionAlign = builder.descriptionAlign;
    this.titleColor = builder.titleColor;
    this.descriptionColor = builder.descriptionColor;
    this.descriptionWrapWidth = builder.descriptionWrapWidth;
    
    this.backgroundColor = builder.backgroundColor;
    this.borderColor = builder.borderColor;
    this.alpha = builder.alpha;
  }

  /**
   * Style builder class.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> {
    private static final int EDGE_PADDING = 12;
    private static final int LINE_PADDING = 6;

    private static final int BORDER_RADIUS = 15;
    private static final int BORDER_SIZE = 2;
    private static final int TRIANGLE_SIZE = 15;

    private static final float TITLE_SIZE = 19;
    private static final float DESCRIPTION_SIZE = 16;
    private static final Typeface TITLE_FONT = Typeface.create("Arial", Typeface.BOLD);
    private static final Typeface DESCRIPTION_FONT = Typeface.create("Arial", Typeface.NORMAL);
    private static final Align TITLE_ALIGN = Align.CENTER;
    private static final Align DESCRIPTION_ALIGN = Align.CENTER;
    
    private static final int BACKGROUND_COLOR = Color.WHITE;
    private static final int BORDER_COLOR = Color.BLACK;
    private static final int TEXT_COLOR = Color.BLACK;

    private int edgePadding = EDGE_PADDING;
    private int linePadding = LINE_PADDING;

    private int borderRadius = BORDER_RADIUS;
    private int borderSize = BORDER_SIZE;
    private int triangleSize = TRIANGLE_SIZE;

    private float titleSize = TITLE_SIZE;
    private float descriptionSize = DESCRIPTION_SIZE;
    private Typeface titleFont = TITLE_FONT;
    private Typeface descriptionFont = DESCRIPTION_FONT;
    private Align titleAlign = TITLE_ALIGN;
    private Align descriptionAlign = DESCRIPTION_ALIGN;
    private int descriptionWrapWidth = -1;
    
    private int backgroundColor = BACKGROUND_COLOR;
    private int borderColor = BORDER_COLOR;
    private int titleColor = TEXT_COLOR;
    private int descriptionColor = TEXT_COLOR;
    private float alpha = 1.0f;

    /**
     * Set edge padding for text in pixels.
     * 
     * @param padding
     *          padding in pixels.
     * @return builder instance.
     */
    public T setEdgePadding(int padding) {
      this.edgePadding = padding;
      return self();
    }

    /**
     * Set line padding for text in pixels.
     * 
     * @param padding
     *          padding in pixels.
     * @return builder instance.
     */
    public T setLinePadding(int padding) {
      this.linePadding = padding;
      return self();
    }

    /**
     * Set border radius in pixels.
     * 
     * @param radius
     *          radius in pixels.
     * @return builder instance.
     */
    public T setBorderRadius(int radius) {
      this.borderRadius = radius;
      return self();
    }

    /**
     * Set border width in pixels.
     * 
     * @param width
     *          width in pixels.
     * @return builder instance.
     */
    public T setBorderWidth(int width) {
      this.borderSize = width;
      return self();
    }

    /**
     * Set label tip size in pixels.
     * 
     * @param size
     *          size in pixels.
     * @return builder instance.
     */
    public T setTipSize(int size) {
      this.triangleSize = size;
      return self();
    }

    /**
     * Set label title font and size.
     * 
     * @param font
     *          font for the title.
     * @param size
     *          size of the font.
     * @return builder instance.
     */
    public T setTitleFont(Typeface font, float size) {
      this.titleFont = font; 
      this.titleSize = size;
      return self();
    }

    /**
     * Set label description font and size.
     * 
     * @param font
     *          font for the description text.
     * @param size
     *          size of the font.
     * @return builder instance.
     */
    public T setDescriptionFont(Typeface font, float size) {
      this.descriptionFont = font; 
      this.descriptionSize = size;
      return self();
    }
    
    /**
     * Set label title alignment.
     * 
     * @param align
     *          title alignment. Default is ALIGN.CENTER.
     * @return builder instance.
     */
    public T setTitleAlign(Align align) {
      this.titleAlign = align;
      return self();
    }

    /**
     * Set label description alignment.
     * 
     * @param align
     *          description alignment. Default is ALIGN.CENTER.
     * @return builder instance.
     */
    public T setDescriptionAlign(Align align) {
      this.descriptionAlign = align;
      return self();
    }
    
    /**
     * Set description wrapping width.
     * 
     * @param wrapWidth
     *          description wrapping width. Default is -1, which disables wrapping.
     * @return builder instance.
     */
    public T setDescriptionWrapWidth(int wrapWidth) {
      this.descriptionWrapWidth = wrapWidth;
      return self();
    }

    /**
     * Set title text color of the label.
     * 
     * @param color
     *          color encoded as 32-bit ARGB.
     * @return builder instance.
     */
    public T setTitleColor(int color) {
      this.titleColor = color;
      return self();
    }

    /**
     * Set description text color of the label.
     * 
     * @param color
     *          color encoded as 32-bit ARGB.
     * @return builder instance.
     */
    public T setDescriptionColor(int color) {
      this.descriptionColor = color;
      return self();
    }

    /**
     * Set background color of the label.
     * 
     * @param color
     *          color encoded as 32-bit ARGB.
     * @return builder instance.
     */
    public T setBackgroundColor(int color) {
      this.backgroundColor = color;
      return self();
    }
    
    /**
     * Set label alpha. This alpha value applies to whole label and can be used to make the label partly transparent.
     * 
     * @param alpha
     *          alpha value between 0.0f and 1.0f, 0.0f makes label transparent and 1.0f opaque. Default is 1.0f.
     */
    public T setAlpha(float alpha) {
      this.alpha = alpha;
      return self();
    }

    /**
     * Set border color of the label.
     * 
     * @param color
     *          color encoded as 32-bit ARGB.
     * @return builder instance.
     */
    public T setBorderColor(int color) {
      this.borderColor = color;
      return self();
    }

    /**
     * Build new instance of LabelStyle.
     * 
     * @return new instance of LabelStyle.
     */
    public LabelStyle build() {
      return new LabelStyle(this);
    }

    @SuppressWarnings("unchecked")
    protected T self() {
      return (T) this;
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
