package com.nutiteq.style;

import com.nutiteq.utils.Const;

/**
 * Abstract base class for styles of billboard-type object (texts, markers).
 */
public abstract class BillBoardStyle extends Style {
  /**
   * use center as element anchor.
   */
  public static final float CENTER = 0;

  /**
   * use leftmost point as element anchor. 
   */
  public static final float LEFT = 1;
  
  /**
   * use rightmost point as element anchor. 
   */
  public static final float RIGHT = -1;

  /**
   * use topmost point as element anchor. 
   */
  public static final float TOP = -1;

  /**
   * use bottom point as element anchor. 
   */
  public static final float BOTTOM = 1;

  /**
   * Element is always facing the camera.
   */
  public static final int CAMERA_BILLBOARD_ORIENTATION = 0;

  /**
   * Element is always on the ground but rotated accordingly to face the camera.
   */
  public static final int GROUND_BILLBOARD_ORIENTATION = 1;
  
  /**
   * Element is always on the ground.
   */
  public static final int GROUND_ORIENTATION = 2;

  /**
   * @pad.exclude
   */
  public final float anchorX;

  /**
   * @pad.exclude
   */
  public final float anchorY;

  /**
   * @pad.exclude
   */
  public final float labelAnchorX;

  /**
   * @pad.exclude
   */
  public final float labelAnchorY;

  /**
   * @pad.exclude
   */
  public final float offset2DX;

  /**
   * @pad.exclude
   */
  public final float offset2DY;
  
  /**
   * @pad.exclude
   */
  public final float offset3DZ;

  /**
   * @pad.exclude
   */
  public final boolean allowOverlap;

  /**
   * @pad.exclude
   */
  public final int placementPriority;

  /**
   * @pad.exclude
   */
  public final boolean placementClip;

  /**
   * @pad.exclude
   */
  public final int orientation;
  
  /**
   * @pad.exclude
   */
  public final boolean orientationFlip;

  /**
   * Default constructor.
   * 
   * @param builder
   *          style builder.
   */
  public BillBoardStyle(Builder<?> builder) {
    super(builder);
    this.anchorX = builder.anchorX;
    this.anchorY = builder.anchorY;
    this.labelAnchorX = builder.labelAnchorX;
    this.labelAnchorY = builder.labelAnchorY;
    this.offset2DX = builder.offset2DX * Const.UNIT_SIZE;
    this.offset2DY = builder.offset2DY * Const.UNIT_SIZE;
    this.offset3DZ = builder.offset3DZ;
    this.allowOverlap = builder.allowOverlap;
    this.placementPriority = builder.placementPriority;
    this.placementClip = builder.placementClip;
    this.orientation = builder.orientation;
    this.orientationFlip = builder.orientationFlip;
  }

  /**
   * Style builder class for BillBoardStyle.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> extends Style.Builder<T> {
    private float anchorX = CENTER;
    private float anchorY = BOTTOM;
    private float labelAnchorX = CENTER;
    private float labelAnchorY = TOP;
    private float offset2DX;
    private float offset2DY;
    private float offset3DZ;
    private boolean allowOverlap = true;
    private int placementPriority = 0;
    private boolean placementClip = true;
    private int orientation = CAMERA_BILLBOARD_ORIENTATION;
    protected boolean orientationFlip = false;

    /**
     * Set horizontal anchor point.
     * 
     * @param anchorX
     *          value between -1 (right) and 1 (left). Can also be BillBoardStyle.LEFT, BillBoardStyle.CENTER or BillBoardStyle.RIGHT 
     * @return builder itself.
     */
    public T setAnchorX(float anchorX) {
      this.anchorX = anchorX;
      return self();
    }

    /**
     * Set vertical anchor point.
     * 
     * @param anchorY
     *          value between -1 (top) and 1 (bottom). Can also be BillBoardStyle.TOP, BillBoardStyle.CENTER or BillBoardStyle.BOTTOM 
     * @return builder itself.
     */
    public T setAnchorY(float anchorY) {
      this.anchorY = anchorY;
      return self();
    }

    /**
     * Set horizontal anchor point for billboard label.
     * 
     * @param anchorX
     *          value between -1 (right) and 1 (left). Can also be BillBoardStyle.LEFT, BillBoardStyle.CENTER or BillBoardStyle.RIGHT 
     * @return builder itself.
     */
    public T setLabelAnchorX(float anchorX) {
      this.labelAnchorX = anchorX;
      return self();
    }

    /**
     * Set vertical anchor point for billboard label.
     * 
     * @param anchorY
     *          value between -1 (top) and 1 (bottom). Can also be BillBoardStyle.TOP, BillBoardStyle.CENTER or BillBoardStyle.BOTTOM 
     * @return builder itself.
     */
    public T setLabelAnchorY(float anchorY) {
      this.labelAnchorY = anchorY;
      return self();
    }

    /**
     * Set horizontal offset for the billboard.
     * 
     * @param offset2DX
     *          horizontal offset in pixels.
     * @return builder itself.
     */
    public T setOffset2DX(float offset2DX) {
      this.offset2DX = offset2DX;
      return self();
    }

    /**
     * Set vertical offset for the billboard.
     * 
     * @param offset2DY
     *          vertical offset in pixels.
     * @return builder itself.
     */
    public T setOffset2DY(float offset2DY) {
      this.offset2DY = offset2DY;
      return self();
    }

    /**
     * Set Z offset for the billboard.
     * 
     * @param offset3DZ
     *          Z offset.
     * @return builder itself.
     */
    public T setOffset3DZ(float offset3DZ) {
      this.offset3DZ = offset3DZ;
      return self();
    }

    /**
     * Set the flag controlling whether this element may overlap other billboard or not. Default is true.
     * 
     * @param allowOverlap
     *          new overlap flag value.
     * @return builder instance.
     */
    public T setAllowOverlap(boolean allowOverlap) {
      this.allowOverlap = allowOverlap;
      return self();
    }

    /**
     * Set the billboard placement priority. If overlapping is not allowed and multiple billboards collide, billboards with higher priority
     * are placed first. Default priority is 0.
     * 
     * @param placementPriority
     *          new placement priority.
     * @return builder instance.
     */
    public T setPlacementPriority(int placementPriority) {
      this.placementPriority = placementPriority;
      return self();
    }

    /**
     * Set the billboard placement clipping flag. If set, billboard positions are updated when they move out of the screen. Default is true.
     * 
     * @param placementClip
     *          new placement clip flag value.
     * @return builder instance.
     */
    public T setPlacementClip(boolean placementClip) {
      this.placementClip = placementClip;
      return self();
    }

    /**
     * Set the element orientation mode. Either CAMERA_BILLBOARD_ORIENTATION, GROUND_BILLBOARD_ORIENTATION or GROUND_ORIENTATION.
     * Default is CAMERA_BILLBOARD_ORIENTATION. 
     * 
     * @param orientation
     *          new orientation mode.
     * @return builder instance.
     */
    public T setOrientation(int orientation) {
      this.orientation = orientation;
      return self();
    }

    /**
     * Set orientation flip flag (assumes GROUND_ORIENTATION orientation). Mostly usable for texts, if billboard is drawn 'upside down', then this flag will help correct it.
     * Default is false for markers, true for texts. 
     * 
     * @param orientationFlip
     *          new orientation flip flag.
     * @return builder instance.
     */
    public T setOrientationFlip(boolean orientationFlip) {
      this.orientationFlip = orientationFlip;
      return self();
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
