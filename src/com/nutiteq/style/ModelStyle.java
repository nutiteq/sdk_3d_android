package com.nutiteq.style;

/**
 * Style class for 3D models.
 */
public class ModelStyle extends Style {

  /**
   * Default constructor.
   * 
   * @param builder
   *          builder instance.
   */
  public ModelStyle(Builder<?> builder) {
    super(builder);
  }

  /**
   * Style builder class for ModelStyle.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> extends Style.Builder<T> {

    /**
     * Unsupported for this style. Throws exception.
     */
    public T setColor(int color) {
      throw new UnsupportedOperationException();
    }

    /**
     * Build new instance of ModelStyle.
     * 
     * @return new instance of ModelStyle.
     */
    public ModelStyle build() {
      return new ModelStyle(this);
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
