package com.nutiteq.style;

import android.graphics.Bitmap;

import com.nutiteq.components.Color;
import com.nutiteq.components.TextureInfo;

/**
 * Style for polygons. Includes color and line style.
 */
public class PolygonStyle extends Style {
  /**
   * @pad.exclude
   */
  public final LineStyle lineStyle;
  
  /**
   * @pad.exclude
   */
  public final TextureInfo patternTextureInfo;
  
  /**
   * Default constructor.
   * 
   * @param builder
   *          style builder.
   */
  public PolygonStyle(Builder<?> builder) {
    super(builder);
    this.lineStyle = builder.lineStyle;
    if (builder.patternBitmap != null) {
      this.patternTextureInfo = TextureInfo.createScaled(builder.patternBitmap, builder.patternScale);
    } else {
      this.patternTextureInfo = null;
    }
  }

  /**
   * Style builder class for PolygonStyle.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> extends Style.Builder<T> {
    private static final int DEFAULT_COLOR = Color.WHITE;
    
    protected LineStyle lineStyle;
    protected Bitmap patternBitmap;
    protected float patternScale = 1;

    /**
     * Default constructor.
     */
    public Builder() {
      color = DEFAULT_COLOR;
    }

    /**
     * Set style for polygon edges (lines).
     * 
     * @param lineStyle
     *          style to use for edges. If null (which is default), no edges will be drawn.
     * @return builder itself.
     */
    public T setLineStyle(LineStyle lineStyle) {
      this.lineStyle = lineStyle;
      return self();
    }
    
    /**
     * Set polygon pattern
     * 
     * @param patternBitmap
     *          pattern bitmap
     * @param patternScale
     *          pattern scale
     */
    public T setPattern(Bitmap patternBitmap, float patternScale) {
      this.patternBitmap = patternBitmap;
      this.patternScale = patternScale;
      return self();
    }

    /**
     * Build new instance of PolygonStyle.
     * 
     * @return new instance of PolygonStyle.
     */
    public PolygonStyle build() {
      return new PolygonStyle(this);
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
