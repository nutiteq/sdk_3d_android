package com.nutiteq.style;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.nutiteq.components.Color;
import com.nutiteq.components.TextureInfo;

/**
 * Style for markers. Includes bitmap, color and size attributes.
 */
public class MarkerStyle extends BillBoardStyle {
  /**
   * @pad.exclude
   */
  public final TextureInfo textureInfo;

  /**
   * @pad.exclude
   */
  public final TextureInfo pickingTextureInfo;

  /**
   * Default constructor.
   * 
   * @param builder
   *          style builder.
   */
  public MarkerStyle(Builder<?> builder) {
    super(builder);
    this.textureInfo = TextureInfo.createFromSize(builder.bitmap, builder.bitmapRect, builder.size);
    this.pickingTextureInfo = (builder.pickingBitmap != null ? TextureInfo.createFromSize(createPickingBitmap(builder.pickingBitmap), builder.pickingBitmapRect, builder.pickingSize != null ? builder.pickingSize : builder.size) : null);
  }
  
  protected Bitmap createPickingBitmap(Bitmap source) {
    Bitmap target = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
    int[] pixels = new int[target.getWidth() * target.getHeight()];
    source.getPixels(pixels, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
    for (int i = 0; i < pixels.length; i++) {
      if (((pixels[i] >> 24) & 0xff) > 127) {
        pixels[i] = 0xffffffff;
      } else {
        pixels[i] = 0;
      }
    }
    target.setPixels(pixels, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
    return target;
  }

  /**
   * Style builder class for MarkerStyle.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> extends BillBoardStyle.Builder<T> {
    private static final int DEFAULT_COLOR = Color.WHITE;
    private static final byte[] DEFAULT_BITMAP_DATA = new byte[0];
    private static final Bitmap DEFAULT_BITMAP;
    private static final Bitmap DEFAULT_PICKING_BITMAP;
    private static final float DEFAULT_SIZE = 0.5f;
    
    static {
      DEFAULT_BITMAP = BitmapFactory.decodeByteArray(DEFAULT_BITMAP_DATA, 0, DEFAULT_BITMAP_DATA.length);
      DEFAULT_PICKING_BITMAP = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
      DEFAULT_PICKING_BITMAP.setPixel(0, 0, 0xffffffff);
    }

    protected Bitmap bitmap = DEFAULT_BITMAP;
    protected Rect bitmapRect = null;
    protected float size = DEFAULT_SIZE;
    protected Bitmap pickingBitmap = DEFAULT_PICKING_BITMAP;
    protected Rect pickingBitmapRect = null; 
    protected Float pickingSize = null;

    /**
     * Default constructor.
     */
    public Builder() {
      color = DEFAULT_COLOR;
    }
    
    /**
     * Set marker bitmap.
     * 
     * @param bitmap
     *          marker bitmap
     * @return builder instance.
     */
    public T setBitmap(Bitmap bitmap) {
      this.bitmap = bitmap;
      this.bitmapRect = null;
      return self();
    }
    
    /**
     * Set marker bitmap. Bitmap is specified as a rectangle from a larger atlas.
     * 
     * @param bitmap
     *          atlas bitmap
     * @param rect
     *          rectangle within atlas bitmap to use for this style
     * @return builder instance.
     */
    public T setBitmap(Bitmap bitmap, Rect rect) {
      this.bitmap = bitmap;
      this.bitmapRect = rect;
      return self();
    }
    

    /**
     * Set marker bitmap for picking.
     * Note: only alpha channel of the bitmap is used, pixels with alpha < 0.5 are considered fully transparent and pixels with >= 0.5 are considered fully opaque.
     * Usually normal marker bitmap can be used as a picking bitmap. By default, fully opaque bitmap is used for picking.
     * 
     * @param pickingBitmap
     *          marker bitmap for picking
     * @return builder instance.
     */
    public T setPickingBitmap(Bitmap pickingBitmap) {
      this.pickingBitmap = pickingBitmap;
      this.pickingBitmapRect = null;
      return self();
    }

    /**
     * Set marker bitmap for picking. Bitmap is specified as a rectangle from a larger atlas.
     * Note: only alpha channel of the bitmap is used, pixels with alpha < 0.5 are considered fully transparent and pixels with >= 0.5 are considered fully opaque.
     * Usually normal marker bitmap can be used as a picking bitmap. By default, fully opaque bitmap is used for picking.
     * 
     * @param pickingBitmap
     *          atlas bitmap
     * @param rect
     *          rectangle within atlas bitmap to use for this style
     * @return builder instance.
     */
    public T setPickingBitmap(Bitmap pickingBitmap, Rect rect) {
      this.pickingBitmap = pickingBitmap;
      this.pickingBitmapRect = rect;
      return self();
    }

    /**
     * Set marker size.
     * 
     * @param size
     *          relative marker size. Default is 0.5f.
     * @return builder instance.
     */
    public T setSize(float size) {
      this.size = size;
      return self();
    }

    /**
     * Set marker size for picking (for detecting clicks on markers). Picking size should be at least as large as size in most cases.
     * 
     * @param size
     *          relative picking size. Default value is equal to size.
     * @return builder itself.
     */
    public T setPickingSize(float size) {
      this.pickingSize = size;
      return self();
    }

    /**
     * Build new instance of MarkerStyle.
     * 
     * @return new instance of MarkerStyle.
     */
    public MarkerStyle build() {
      return new MarkerStyle(this);
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
