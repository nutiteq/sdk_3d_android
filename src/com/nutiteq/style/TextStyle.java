package com.nutiteq.style;

import android.graphics.Bitmap;
import android.graphics.Typeface;

import com.nutiteq.components.Color;

/**
 * Style for texts. Includes text font, color and size attributes.
 */
public class TextStyle extends BillBoardStyle {
  /**
   * @pad.exclude
   */
  public final Typeface font;

  /**
   * @pad.exclude
   */
  public final int size;
  
  /**
   * @pad.exclude
   */
  public final Color strokeColor;
  
  /**
   * @pad.exclude
   */
  public final float strokeWidth;
  
  /**
   * @pad.exclude
   */
  public final Bitmap backgroundBitmap;
  
  /**
   * @pad.exclude
   */
  public final int backgroundBitmapPaddingLeft;
  
  /**
   * @pad.exclude
   */
  public final int backgroundBitmapPaddingRight;
  
  /**
   * @pad.exclude
   */
  public final int backgroundBitmapPaddingTop;
  
  /**
   * @pad.exclude
   */
  public final int backgroundBitmapPaddingBottom;
  
  /**
   * @pad.exclude
   */
  public final Bitmap iconBitmap;
  
  /**
   * @pad.exclude
   */
  public final float iconAnchorX;
  
  /**
   * @pad.exclude
   */
  public final float iconAnchorY;
  
  /**
   * @pad.exclude
   */
  public final boolean pickingMode;
  
  /**
   * Default constructor.
   * 
   * @param builder
   *          style builder.
   */
  public TextStyle(Builder<?> builder) {
    super(builder);
    this.size = builder.size;
    this.font = builder.font;
    this.strokeColor = new Color(builder.strokeColor);
    this.strokeWidth = builder.strokeWidth;
    this.backgroundBitmap = builder.backgroundBitmap;
    this.backgroundBitmapPaddingLeft = builder.backgroundBitmapPaddingLeft;
    this.backgroundBitmapPaddingRight = builder.backgroundBitmapPaddingRight;
    this.backgroundBitmapPaddingTop = builder.backgroundBitmapPaddingTop;
    this.backgroundBitmapPaddingBottom = builder.backgroundBitmapPaddingBottom;
    this.iconBitmap = builder.iconBitmap;
    this.iconAnchorX = builder.iconAnchorX;
    this.iconAnchorY = builder.iconAnchorY;
    this.pickingMode = builder.pickingMode;
  }

  /**
   * Style builder class for TextStyle.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> extends BillBoardStyle.Builder<T> {
    private static final int DEFAULT_COLOR = Color.BLACK;
    private static final int DEFAULT_STROKE_COLOR = Color.WHITE;
    private static final float DEFAULT_STROKE_WIDTH = 4;
    private static final Typeface DEFAULT_FONT = Typeface.create("Arial", Typeface.NORMAL);
    private static final int DEFAULT_SIZE = 32;

    protected int strokeColor;
    protected float strokeWidth = DEFAULT_STROKE_WIDTH;
    protected Typeface font = DEFAULT_FONT;
    protected int size = DEFAULT_SIZE;
    protected Bitmap backgroundBitmap = null;
    protected int backgroundBitmapPaddingLeft = 0;
    protected int backgroundBitmapPaddingTop = 0;
    protected int backgroundBitmapPaddingRight = 0;
    protected int backgroundBitmapPaddingBottom = 0;
    protected Bitmap iconBitmap = null;
    protected float iconAnchorX = CENTER;
    protected float iconAnchorY = CENTER;
    protected boolean pickingMode = false;

    /**
     * Default constructor.
     */
    public Builder() {
      color = DEFAULT_COLOR;
      strokeColor = DEFAULT_STROKE_COLOR;
      orientationFlip = true;
    }

    /**
     * Set text font.
     * 
     * @param font
     *          typeface to use. Default is Arial/normal.
     * @return builder instance.
     */
    public T setFont(Typeface font) {
      this.font = font;
      return self();
    }

    /**
     * Set text size.
     * 
     * @param size
     *          relative text size. Default is 32.
     * @return builder instance.
     */
    public T setSize(int size) {
      this.size = size;
      return self();
    }

    /**
     * Set text stroke color.
     * 
     * @param strokeColor
     *          stroke color. Default is white.
     * @return builder instance.
     */
    public T setStrokeColor(int strokeColor) {
      this.strokeColor = strokeColor;
      return self();
    }

    /**
     * Set text stroke width.
     * 
     * @param strokeWidth
     *          stroke width. Default is 4.
     * @return builder instance.
     */
    public T setStrokeWidth(float strokeWidth) {
      this.strokeWidth = strokeWidth;
      return self();
    }
    
    /**
     * Set text background bitmap. Default is none (transparent background).
     * 
     * @param backgroundBitmap
     *          text background bitmap
     * @return builder instance
     */
    public T setBackgroundBitmap(Bitmap backgroundBitmap) {
      this.backgroundBitmap = backgroundBitmap;
      return self();
    }
    
    /**
     * Set text background bitmap padding. Default is (0, 0, 0, 0) (or no padding).
     * 
     * @param left
     *          left padding in pixels
     * @param top
     *          top padding in pixels
     * @param right
     *          right padding in pixels
     * @param bottom
     *          bottom padding in pixels
     * @return builder instance
     */
    public T setBackgroundBitmapPadding(int left, int top, int right, int bottom) {
      this.backgroundBitmapPaddingLeft = left;
      this.backgroundBitmapPaddingTop = top;
      this.backgroundBitmapPaddingRight = right;
      this.backgroundBitmapPaddingBottom = bottom;
      return self();
    }
    
    /**
     * Set text icon bitmap. Default is none.
     * 
     * @param iconBitmap
     *          icon bitmap for the text
     * @param anchorX
     *          either CENTER, LEFT, RIGHT.
     * @param anchorY
     *          either CENTER, TOP, BOTTOM.
     * @return builder instance
     */
    public T setIconBitmap(Bitmap iconBitmap, float anchorX, float anchorY) {
      this.iconBitmap = iconBitmap;
      this.iconAnchorX = anchorX;
      this.iconAnchorY = anchorY;
      return self();
    }
    
    /**
     * Set text picking mode flag. Default is false. If set to true, text can be clicked on and will receive listener events.
     * 
     * @param pickingMode
     *          picking mode flag
     * @return builder instance
     */
    public T setPickingMode(boolean pickingMode) {
      this.pickingMode = pickingMode;
      return self();
    }

    /**
     * Build new instance of TextStyle.
     * 
     * @return new instance of TextStyle.
     */
    public TextStyle build() {
      return new TextStyle(this);
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }

}
