package com.nutiteq.style;

import java.util.ArrayList;
import java.util.List;

/**
 * A class for defining mapping between zoom levels and styles.
 *
 * @param <T>
 *          style class.
 */
public class StyleSet<T extends Style> {
  
  /**
   * Tuple containing zoom level and corresponding style.
   */
  public static class ZoomStyle<T> {
    public final int zoom;
    public T style;

    public ZoomStyle(int zoom, T style) {
      this.zoom = zoom;
      this.style = style;
    }
    
    @Override
    public int hashCode() {
      return zoom;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object o) {
      if (o == null) return false;
      if (this.getClass() != o.getClass()) return false;
      ZoomStyle<T> other = (ZoomStyle<T>) o;
      return zoom == other.zoom && style.equals(other.style);
    }
}

  private List<ZoomStyle<T>> zoomStyles = new ArrayList<ZoomStyle<T>>();

  /**
   * Default constructor. Creates null style for zoom level 0.
   */
  public StyleSet() {
    ZoomStyle<T> zoomStyle = new ZoomStyle<T>(0, null);
    zoomStyles = java.util.Collections.unmodifiableList(java.util.Collections.singletonList(zoomStyle));
  }

  /**
   * Constructor for single style. The style will be used for all zoom levels.
   * 
   * @param style
   *          style for all zoom levels.
   */
  public StyleSet(T style) {
    ZoomStyle<T> zoomStyle = new ZoomStyle<T>(0, style);
    zoomStyles = java.util.Collections.unmodifiableList(java.util.Collections.singletonList(zoomStyle));
  }

  /**
   * Get list of zoom styles.
   * 
   * @return list of zoom styles. The list is immutable.
   */
  public List<ZoomStyle<T>> getZoomStyles() {
    return zoomStyles;
  }

  /**
   * Update style for specific zoom level.
   * 
   * @param zoom
   *          zoom level for the style.
   * @param style
   *          style to use for this zoom level.
   */
  public void setZoomStyle(int zoom, T style) {
    for (int tsj = 0; tsj < zoomStyles.size(); tsj++) {
      ZoomStyle<T> oldZoomStyle = zoomStyles.get(tsj);
      if (zoom < oldZoomStyle.zoom) {
        List<ZoomStyle<T>> newZoomStyles = new ArrayList<ZoomStyle<T>>(zoomStyles.size() + 1);
        newZoomStyles.addAll(zoomStyles);
        newZoomStyles.add(tsj, new ZoomStyle<T>(zoom, style));
        zoomStyles = java.util.Collections.unmodifiableList(newZoomStyles);
        return;
      } else if (zoom == oldZoomStyle.zoom) {
        oldZoomStyle.style = style;
        return;
      }
    }
    List<ZoomStyle<T>> newZoomStyles = new ArrayList<ZoomStyle<T>>(zoomStyles.size() + 1);
    newZoomStyles.addAll(zoomStyles);
    newZoomStyles.add(new ZoomStyle<T>(zoom, style));
    zoomStyles = java.util.Collections.unmodifiableList(newZoomStyles);
  }
  
  /**
   * Get style for given zoom level.
   * 
   * @param zoom
   *          zoom level.
   * @return matching style. If style is not defined for matching zoom level, smaller zoom levels are tried until match is found.
   */
  public T getZoomStyle(int zoom) {
    int size = zoomStyles.size();
    for (int tsj = 0; tsj < size; tsj++) {
      ZoomStyle<T> zoomStyle = zoomStyles.get(tsj);
      if (zoom >= zoomStyle.zoom) {
        if (tsj + 1 < size && zoom >= zoomStyles.get(tsj + 1).zoom) {
          continue;
        }
        return zoomStyle.style;
      }
    }
    return null;
  }

  /**
   * Get first valid zoom style from the style set.
   * 
   * @return first zoom style. If the style set does not contain valid zoom styles, returns null.
   */
  public T getFirstZoomStyle() {
    for (ZoomStyle<T> zoomStyle : zoomStyles) {
      if (zoomStyle != null && zoomStyle.style != null) {
        return zoomStyle.style;
      }
    }
    return null;
  }
  
  /**
   * Get first valid zoom level from the style set.
   * 
   * @return first defined zoom level. If the style set does not contain valid zoom levels, returns -1.
   */
  public int getFirstNonNullZoomStyleZoom() {
    for (ZoomStyle<T> zoomStyle : zoomStyles) {
      if (zoomStyle != null && zoomStyle.style != null) {
        return zoomStyle.zoom;
      }
    }
    return -1;
  }
  
  @Override
  public int hashCode() {
    return zoomStyles.size();
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) return false;
    if (this.getClass() != o.getClass()) return false;
    StyleSet<?> other = (StyleSet<?>) o;
    return zoomStyles.equals(other.zoomStyles);
  }
}
