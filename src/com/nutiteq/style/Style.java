package com.nutiteq.style;

import com.nutiteq.components.Color;

/**
 * Base class for styles of all vector elements.
 */
public class Style {
  /**
   * @pad.exclude
   */
  public final Color color;

  /**
   * Default constructor.
   * 
   * @param builder
   *          builder instance.
   */
  protected Style(Builder<?> builder) {
    this.color = new Color(builder.color);
  }

  /**
   * Style builder class.
   *
   * @param <T>
   *          actual builder type.
   */
  public static class Builder<T extends Builder<T>> {
    protected int color;

    /**
     * Set color of vector element.
     * 
     * @param color
     *          color encoded as 32-bit ARGB.
     * @return builder instance.
     */
    public T setColor(int color) {
      this.color = color;
      return self();
    }

    @SuppressWarnings("unchecked")
    protected T self() {
      return (T) this;
    }
  }

  /**
   * Create new builder instance for the style.
   * 
   * @return builder instance for the style.
   */
  @SuppressWarnings("rawtypes")
  public static Builder<?> builder() {
    return new Builder();
  }
}
