package com.nutiteq.vectorlayers;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Queue;
import java.util.zip.GZIPInputStream;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;

import com.google.protobuf.InvalidProtocolBufferException;
import com.nutiteq.cache.NMLPersistentCache;
import com.nutiteq.components.CullState;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.geometry.NMLModel;
import com.nutiteq.log.Log;
import com.nutiteq.nmlpackage.NMLPackage;
import com.nutiteq.projections.Projection;
import com.nutiteq.style.ModelStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.tasks.CancelableThreadPool;
import com.nutiteq.tasks.Task;
import com.nutiteq.utils.IntHashMap;
import com.nutiteq.utils.LongArrayList;
import com.nutiteq.utils.LongHashMap;
/**
 * Layer for 3D models that are read streamed from server and cached locally.
 * This layer is still in highly experimental stage and changes will not be backward compatible.
 */
public class NMLModelOnlineLayer extends NMLModelDbLayer {
  private static final String NML_HTTP_CONTENT_TYPE = "application/vnd.nutiteq.nml";
  private static final int MESH_FETCH_CHUNKSIZE = 16; // max number of meshes to request as one chunk 
  private static final int TEXTURE_FETCH_CHUNKSIZE = 8; // max number of textures to request as one chunk

  /**
   * Task for downloading meshes.
   */
  private abstract class DownloadTask implements Task {
    @Override
    public boolean isCancelable() {
      return true;
    }
  }

  /**
   * Sqlite data store interface for NML objects.
   */
  private static class NMLSqliteDataStore implements NMLPersistentCache.DataStore {
    private final String table;
    private final SQLiteDatabase db;

    private NMLSqliteDataStore(SQLiteDatabase db, String table) {
      this.db = db;
      this.table = table;
    }

    @Override
    public List<NMLPersistentCache.CacheEntry> getSortedEntries() {
      synchronized(db) {
        List<NMLPersistentCache.CacheEntry> entries = new ArrayList<NMLPersistentCache.CacheEntry>();
        Cursor cursor = db.rawQuery("SELECT id, LENGTH(data) FROM " + table + " ORDER BY timestamp ASC", null);
        if (cursor == null) {
          return null;
        }
        while (cursor.moveToNext()) {
          long id = cursor.getLong(0);
          int length = cursor.getInt(1);
          entries.add(new NMLPersistentCache.CacheEntry(id, length));
        }
        cursor.close();
        return entries;
      }
    }

    @Override
    public byte[] get(long id) {
      synchronized(db) {
        Cursor cursor = db.rawQuery("SELECT data FROM " + table + " WHERE id=?", new String[] { Long.toString(id) });
        if (cursor == null) {
          return null;
        }
        byte[] compressed = null;
        if (cursor.moveToNext()) {
          compressed = cursor.getBlob(0);
          db.execSQL("UPDATE " + table + " SET timestamp=? WHERE id=?", new String[] { Long.toString(System.currentTimeMillis()), Long.toString(id) });
        }
        cursor.close();
        return compressed;
      }
    }

    @Override
    public void add(long id, byte[] data) {
      synchronized(db) {
        SQLiteStatement stmt = db.compileStatement("INSERT OR REPLACE INTO " + table + "(id, data, timestamp) VALUES(?, ?, ?)");
        stmt.bindLong(1, id);
        stmt.bindBlob(2, data);
        stmt.bindLong(3, System.currentTimeMillis());
        stmt.execute();
      }
    }

    @Override
    public void remove(long id) {
      synchronized(db) {
        db.execSQL("DELETE FROM " + table + " WHERE id=?", new String[] { Long.toString(id) });
      }
    }

    public static NMLSqliteDataStore create(SQLiteDatabase db, String table) {
      synchronized(db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + table + "(id INTEGER NOT NULL PRIMARY KEY, data BYTEA NOT NULL, timestamp INTEGER)");
      }
      return new NMLSqliteDataStore(db, table);
    }
  };

  private final String baseUrl;
  private final CancelableThreadPool downloadPool = new CancelableThreadPool(1);
  private SQLiteDatabase persistentCacheDB;

  private final LongHashMap<Object> modelLODTreeDownloadSet = new LongHashMap<Object>();
  private final NMLPersistentCache modelLODTreePersistentCache = new NMLPersistentCache(0);

  private final LongArrayList meshFetchList = new LongArrayList();
  private final LongHashMap<Object> meshDownloadSet = new LongHashMap<Object>();
  private final NMLPersistentCache meshPersistentCache = new NMLPersistentCache(0);

  private final LongArrayList textureFetchList = new LongArrayList();
  private final LongHashMap<Object> textureDownloadSet = new LongHashMap<Object>();
  private final NMLPersistentCache texturePersistentCache = new NMLPersistentCache(0);

  /**
   * Main constructor.
   *  
   * @param proj
   *            projection for this layer. MUST BE instance of EPSG3857!
   * @param baseUrl
   *            base URL for the service.
   * @param styleSet
   *            styleSet to use for database models.
   * @throws IOException 
   *            if database does not exist of can not be opened
   * @throws UnsupportedOperationException
   *            if projection is not EPSG3857
   */
  public NMLModelOnlineLayer(Projection proj, String baseUrl, StyleSet<ModelStyle> styleSet) {
    super(proj, (SQLiteDatabase) null, styleSet);
    this.baseUrl = baseUrl;
  }
  
  /**
   * Set full path to persistent cache file.
   * 
   * @param persistentCachePath
   *            full path to the persistent cache file.
   */
  public synchronized void setPersistentCachePath(String persistentCachePath) {
    if (persistentCacheDB != null) {
      persistentCacheDB.close();
    }
    File pathFile = new File(persistentCachePath);
    pathFile.getParentFile().mkdirs();
    persistentCacheDB = SQLiteDatabase.openOrCreateDatabase(persistentCachePath, null);
    modelLODTreePersistentCache.setDataStore(NMLSqliteDataStore.create(persistentCacheDB, "ModelLODTrees"));
    meshPersistentCache.setDataStore(NMLSqliteDataStore.create(persistentCacheDB, "Meshes"));
    texturePersistentCache.setDataStore(NMLSqliteDataStore.create(persistentCacheDB, "Textures"));
  }

  /**
   * Set size limit for persistent cache.
   * 
   * @param maxSizeInBytes
   *            maximum cache size in bytes. Default is 0.
   */
  public synchronized void setPersistentCacheSize(int maxSizeInBytes) {
    modelLODTreePersistentCache.setSize(maxSizeInBytes * 1 / 6);
    meshPersistentCache.setSize(maxSizeInBytes * 2 / 6);
    texturePersistentCache.setSize(maxSizeInBytes * 3 / 6);
  }

  @Override
  protected synchronized void loadModels(CullState state) {
    if (persistentCacheDB == null) {
      Log.error("NMLModelOnlineLayer: persistent cache file (or size) not set, this is REQUIRED for this layer!");
      return;
    }
    meshPersistentCache.lock();
    texturePersistentCache.lock();
    try {
      super.loadModels(state);
    }
    finally {
      meshPersistentCache.unlock();
      texturePersistentCache.unlock();
    }
  }

  @Override
  protected synchronized void refineDrawLists() {
    for (ModelLODTree modelLODTree : modelLODTreeMap.values()) {
      HashMap<ModelLODTreeNode, ArrayList<ModelLODTreeNode>> childrenMap = new HashMap<ModelLODTreeNode, ArrayList<ModelLODTreeNode>>();
      for (ModelLODTreeNode node : modelLODTree.modelMap.keySet()) {
        while (node.parentNode != null) {
          if (!childrenMap.containsKey(node.parentNode)) {
            childrenMap.put(node.parentNode, new ArrayList<ModelLODTreeNode>());
          }
          childrenMap.get(node.parentNode).add(node);
          node = node.parentNode;
        }
      }
      
      Queue<ModelLODTreeNode> queue = new LinkedList<ModelLODTreeNode>();
      for (ListIterator<ModelLODTreeNode> it = modelLODTree.drawList.listIterator(); it.hasNext(); ) {
        // Check if data is available
        ModelLODTreeNode node = it.next();
        if (isDataAvailable(node.meshBindings, node.textureBindings)) {
          continue;
        }
        
        // No, must remove this node
        it.remove();

        // Check if we have some children available. Note: size constraints are not checked here anymore
        int childCount = 0;
        ArrayList<ModelLODTreeNode> childNodeList = childrenMap.get(node);
        if (childNodeList != null) {
          queue.addAll(childNodeList);
        }
        while (!queue.isEmpty()) {
          ModelLODTreeNode childNode = queue.poll();
          if (isDataAvailable(childNode.meshBindings, childNode.textureBindings)) {
            it.add(childNode);
            childCount++;
            continue;
          }
          childNodeList = childrenMap.get(childNode);
          if (childNodeList != null) {
            queue.addAll(childNodeList);
          }
        }
        if (childCount > 0) {
          prefetchData(node.meshBindings, node.textureBindings);
          continue;
        }
        
        // Find closest parent that has data available. Ignore size checks
        while (node.parentNode != null) {
          if (isDataAvailable(node.parentNode.meshBindings, node.parentNode.textureBindings)) {
            break;
          }
          node = node.parentNode;
        }
        if (node.parentNode != null) {
          it.add(node.parentNode);
        }
        prefetchData(node.meshBindings, node.textureBindings);
      }
    }
    
    // Remove nodes that have parents listed
    for (ModelLODTree modelLODTree : modelLODTreeMap.values()) {
      HashSet<ModelLODTreeNode> nodeSet = new HashSet<ModelLODTreeNode>(modelLODTree.drawList);
      for (ListIterator<ModelLODTreeNode> it = modelLODTree.drawList.listIterator(); it.hasNext(); ) {
        ModelLODTreeNode parentNode = it.next().parentNode;
        while (parentNode != null) {
          if (nodeSet.contains(parentNode)) {
            it.remove();
            break;
          }
          parentNode = parentNode.parentNode;
        }
      }
    }
    
    // Actually load the data
    fetchData();
  }

  private boolean isDataAvailable(List<MeshBinding> meshBindings, List<TextureBinding> textureBindings) {
    for (MeshBinding binding : meshBindings) {
      long id = binding.meshId;
      if (!meshMap.containsKey(id)) {
        if (!meshPersistentCache.contains(id)) {
          return false;
        }
      }
    }
    for (TextureBinding binding : textureBindings) {
      long id = binding.textureId;
      if (!textureMap.containsKey(id)) {
        if (!texturePersistentCache.contains(id)) {
          return false;
        }
      }
    }
    return true;
  }

  private void prefetchData(List<MeshBinding> meshBindings, List<TextureBinding> textureBindings) {
    for (MeshBinding binding : meshBindings) {
      long id = binding.meshId;
      if (!meshMap.containsKey(id)) {
        if (!meshPersistentCache.contains(id)) {
          if (!meshDownloadSet.containsKey(id)) {
            meshDownloadSet.put(id, null);
            meshFetchList.add(id);
          }
        }
      }
    }
    for (TextureBinding binding : textureBindings) {
      long id = binding.textureId;
      if (!textureMap.containsKey(id)) {
        if (!texturePersistentCache.contains(id)) {
          if (!textureDownloadSet.containsKey(id)) {
            textureDownloadSet.put(id, null);
            textureFetchList.add(id);
          }
        }
      }
    }
  }

  private void fetchData() {
    downloadPool.cancelAll();

    for (int start = 0; start < meshFetchList.size(); ) {
      int len = Math.min(meshFetchList.size() - start, MESH_FETCH_CHUNKSIZE); 
      final LongArrayList ids = new LongArrayList(len);
      for (int i = 0; i < len; i++) {
        ids.add(meshFetchList.get(start + i));
      }
      downloadPool.execute(new DownloadTask() {
        @Override
        public void run() {
          downloadMeshes(ids);
        }

        @Override
        public void cancel() {
          cancelMeshDownload(ids);
        }
      });
      start += len;
    }
    meshFetchList.clear();

    for (int start = 0; start < textureFetchList.size(); ) {
      int len = Math.min(textureFetchList.size() - start, TEXTURE_FETCH_CHUNKSIZE); 
      final LongArrayList ids = new LongArrayList(len);
      for (int i = 0; i < len; i++) {
        ids.add(textureFetchList.get(start + i));
      }
      downloadPool.execute(new DownloadTask() {
        @Override
        public void run() {
          downloadTextures(ids);
        }

        @Override
        public void cancel() {
          cancelTextureDownload(ids);
        }
      });
      start += len;
    }
    textureFetchList.clear();
  }
  
  @Override
  protected Envelope fetchDataExtent() {
    Uri.Builder uri = Uri.parse(baseUrl).buildUpon();
    uri.appendQueryParameter("q", "DataExtent");
    Log.debug("NMLModelOnlineLayer: url " + uri.build().toString());

    try {
      HttpURLConnection conn = (HttpURLConnection) new URL(uri.build().toString()).openConnection();
      if (!checkConnection(conn)) {
        return null;
      }
      DataInputStream connData = new DataInputStream(conn.getInputStream());
      int length = connData.readInt();
      if (length == 0) {
        return null; // bounds not available
      }
      byte[] compressedData = new byte[length];
      connData.readFully(compressedData);
      DataInputStream data = new DataInputStream(new GZIPInputStream(new ByteArrayInputStream(compressedData)));
      double mapBoundsX0 = data.readDouble();
      double mapBoundsX1 = data.readDouble();
      double mapBoundsY0 = data.readDouble();
      double mapBoundsY1 = data.readDouble();
      return new Envelope(mapBoundsX0, mapBoundsX1, mapBoundsY0, mapBoundsY1);
    } catch (IOException e) {
      Log.error("NMLModelOnlineLayer: Failed to fetch data extent! " + e.getMessage());
    }
    return null;
  }

  @Override
  protected List<MapTile> fetchMapTiles(MapPos envelopeMin, MapPos envelopeMax) {
    List<MapTile> mapTiles = new ArrayList<MapTile>(); 

    String envelopeX0 = Double.toString(envelopeMin.x), envelopeY0 = Double.toString(envelopeMin.y);
    String envelopeX1 = Double.toString(envelopeMax.x), envelopeY1 = Double.toString(envelopeMax.y);
    String width = Double.toString(projection.getBounds().getWidth());

    Uri.Builder uri = Uri.parse(baseUrl).buildUpon();
    uri.appendQueryParameter("q", "MapTiles");
    uri.appendQueryParameter("mapbounds_x0", envelopeX0);
    uri.appendQueryParameter("mapbounds_x1", envelopeX1);
    uri.appendQueryParameter("mapbounds_y0", envelopeY0);
    uri.appendQueryParameter("mapbounds_y1", envelopeY1);
    uri.appendQueryParameter("width", width);
    Log.debug("NMLModelOnlineLayer: url " + uri.build().toString());

    try {
      HttpURLConnection conn = (HttpURLConnection) new URL(uri.build().toString()).openConnection();
      if (!checkConnection(conn)) {
        return mapTiles;
      }
      DataInputStream connData = new DataInputStream(conn.getInputStream());
      int length = connData.readInt();
      byte[] compressedData = new byte[length];
      connData.readFully(compressedData);
      DataInputStream data = new DataInputStream(new GZIPInputStream(new ByteArrayInputStream(compressedData)));

      while (true) {
        long id = data.readLong();
        if (id == -1) {
          break;
        }
        long lodTreeId = data.readLong();
        double mapPosX = data.readDouble();
        double mapPosY = data.readDouble();
        double mapPosZ = data.readDouble();
        MapPos mapPos = new MapPos(mapPosX, mapPosY, mapPosZ);
        MapTile mapTile = new MapTile(id, mapPos, lodTreeId);
        mapTiles.add(mapTile);
      }
    } catch (IOException e) {
      Log.error("NMLModelOnlineLayer: Failed to fetch map tile! " + e.getMessage());
    }
    return mapTiles;
  }

  private synchronized byte[] getCachedModelLODTree(MapTile mapTile) {
    final long id = mapTile.modelLODTreeId;
    byte[] compressedData = modelLODTreePersistentCache.get(id);
    if (compressedData != null) {
      return compressedData;
    }
    if (!modelLODTreeDownloadSet.containsKey(id)) {
      modelLODTreeDownloadSet.put(id, null);
      downloadPool.execute(new DownloadTask() {
        @Override
        public void run() {
          downloadModelLODTree(id);
        }

        @Override
        public void cancel() {
          cancelModelLODTreeDownload(id);
        }
      });
    }
    return null;
  }

  @SuppressLint("UseSparseArrays")
  @Override
  protected ModelLODTree fetchModelLODTree(MapTile mapTile) {
    NMLPackage.ModelLODTree nmlModelLODTree = null;
    Map<Integer, NMLModel.Proxy> proxyMap = new HashMap<Integer, NMLModel.Proxy>();
    IntHashMap<List<MeshBinding>> nodeMeshBindingMap = new IntHashMap<List<MeshBinding>>();
    IntHashMap<List<TextureBinding>> nodeTextureBindingMap = new IntHashMap<List<TextureBinding>>();
    try {
      byte[] compressedData = getCachedModelLODTree(mapTile);
      if (compressedData == null) {
        return null;
      }
      ByteArrayInputStream is = new ByteArrayInputStream(compressedData);
      GZIPInputStream gzis = new GZIPInputStream(is);
      DataInputStream data = new DataInputStream(gzis);

      // Model LOD tree
      int length = data.readInt();
      byte[] binary = new byte[length];
      data.readFully(binary);
      try {
        nmlModelLODTree = NMLPackage.ModelLODTree.parseFrom(binary);
      }
      catch (InvalidProtocolBufferException e) {
        Log.error("NMLModelOnlineLayer: Failed to load LOD tree! " + e.getMessage());
        return null;
      }

      // Model info
      while (true) {
        int modelId = data.readInt();
        if (modelId == -1) {
          break;
        }
        String folder = data.readUTF();
        double mapPosX = data.readDouble();
        double mapPosY = data.readDouble();
        double mapPosZ = data.readDouble();
        MapPos mapPos = new MapPos(mapPosX, mapPosY, mapPosZ);
        Map<String, String> userData = new HashMap<String, String>();
        userData.put("folder", folder);
        NMLModel.Proxy proxy = new NMLModel.Proxy(mapPos, createLabel(userData), styleSet, userData);
        proxy.attachToLayer(this);
        proxyMap.put(modelId, proxy);
      }

      // Mesh bindings
      while (true) {
        int nodeId = data.readInt();
        if (nodeId == -1) {
          break;
        }
        String localId = data.readUTF();
        long meshId = data.readLong();

        MeshBinding binding = new MeshBinding(meshId, localId, null);
        if (nodeMeshBindingMap.get(nodeId) == null) {
          nodeMeshBindingMap.put(nodeId, new ArrayList<MeshBinding>());
        }
        nodeMeshBindingMap.get(nodeId).add(binding);
      }

      // Texture bindings
      while (true) {
        int nodeId = data.readInt();
        if (nodeId == -1) {
          break;
        }
        String localId = data.readUTF();
        long textureId = data.readLong();
        int level = data.readInt();

        TextureBinding binding = new TextureBinding(textureId, level, localId);
        if (nodeTextureBindingMap.get(nodeId) == null) {
          nodeTextureBindingMap.put(nodeId, new ArrayList<TextureBinding>());
        }
        nodeTextureBindingMap.get(nodeId).add(binding);
      }
    }
    catch (IOException e) {
      Log.error("NMLModelOnlineLayer: Exception while fetching LOD tree! " + e.getMessage());
      return null;
    }
    return new ModelLODTree(mapTile.modelLODTreeId, mapTile.mapPos, projection, nmlModelLODTree, nodeMeshBindingMap, nodeTextureBindingMap, proxyMap);
  }

  private void downloadModelLODTree(long id) {
    Uri.Builder uri = Uri.parse(baseUrl).buildUpon();
    uri.appendQueryParameter("q", "ModelLODTree");
    uri.appendQueryParameter("id", Long.toString(id));
    Log.debug("NMLModelOnlineLayer: fetchModelLODTree url " + uri.build().toString());

    try {
      HttpURLConnection conn = (HttpURLConnection) new URL(uri.build().toString()).openConnection();
      if (!checkConnection(conn)) {
        return;
      }
      DataInputStream data = new DataInputStream(conn.getInputStream());
      int length = data.readInt();
      byte[] compressedData = new byte[length];
      data.readFully(compressedData);
      synchronized(this) {
        modelLODTreePersistentCache.add(id, compressedData);
        modelLODTreeDownloadSet.remove(id);
      }
    }
    catch (IOException e) {
      Log.error("NMLModelOnlineLayer: Failed to download LOD tree! " + e.getMessage());
    }

    updateVisibleElements();
  }
  
  protected synchronized void cancelModelLODTreeDownload(long id) {
    modelLODTreeDownloadSet.remove(id);
  }
  
  @Override
  protected synchronized LongHashMap<NMLModel.Mesh> fetchMeshes(LongArrayList ids) {
    LongHashMap<NMLModel.Mesh> meshMap = new LongHashMap<NMLModel.Mesh>();
    for (int i = 0; i < ids.size(); i++) {
      long id = ids.get(i);
      byte[] compressedData = meshPersistentCache.get(id);
      if (compressedData == null) {
        Log.error("NMLModelOnlineLayer: Missing compressed mesh!");
        continue;
      }
      ByteArrayInputStream is = new ByteArrayInputStream(compressedData);
      try {
        GZIPInputStream gzis = new GZIPInputStream(is); 
        NMLPackage.Mesh nmlMesh = NMLPackage.Mesh.parseFrom(gzis);
        NMLModel.Mesh mesh = new NMLModel.Mesh(id, nmlMesh);
        meshMap.put(id, mesh);
      }
      catch (IOException e) {
        Log.error("NMLModelOnlineLayer: Failed to load mesh! " + e.getMessage());
        break;
      }
    }
    return meshMap;
  }

  private void downloadMeshes(LongArrayList ids) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < ids.size(); i++) {
      if (i > 0) {
        sb.append(",");
      }
      sb.append(Long.toString(ids.get(i)));
    }
    Uri.Builder uri = Uri.parse(baseUrl).buildUpon();
    uri.appendQueryParameter("q", "Meshes");
    uri.appendQueryParameter("ids", sb.toString());
    Log.debug("NMLModelOnlineLayer: fetchMeshes url " + uri.build().toString());

    try {
      HttpURLConnection conn = (HttpURLConnection) new URL(uri.build().toString()).openConnection();
      if (!checkConnection(conn)) {
        return;
      }
      DataInputStream data = new DataInputStream(conn.getInputStream());
      while (true) {
        long id = data.readLong();
        if (id == -1) {
          break;
        }
        int length = data.readInt();
        byte[] compressedData = new byte[length];
        data.readFully(compressedData);
        synchronized(this) {
          meshPersistentCache.add(id, compressedData);
          meshDownloadSet.remove(id);
        }
      }
    } catch (IOException e) {
      Log.error("NMLModelOnlineLayer: Failed to download meshes! " + e.getMessage());
    }

    // NOTE: no need to cull in almost all cases, textures will be also needed
  }

  private synchronized void cancelMeshDownload(LongArrayList ids) {
    for (int i = 0; i < ids.size(); i++) {
      meshDownloadSet.remove(ids.get(i));
    }
  }
  
  @Override
  protected synchronized LongHashMap<NMLModel.Texture> fetchTextures(LongArrayList ids) {
    LongHashMap<NMLModel.Texture> textureMap = new LongHashMap<NMLModel.Texture>();
    for (int i = 0; i < ids.size(); i++) {
      long id = ids.get(i);
      byte[] compressedData = texturePersistentCache.get(id);
      if (compressedData == null) {
        Log.error("NMLModelOnlineLayer: Missing compressed texture!");
        continue;
      }
      ByteArrayInputStream is = new ByteArrayInputStream(compressedData);
      try {
        GZIPInputStream gzis = new GZIPInputStream(is); 
        NMLPackage.Texture nmlTexture = NMLPackage.Texture.parseFrom(gzis);
        NMLModel.Texture texture = new NMLModel.Texture(id, nmlTexture);
        textureMap.put(id, texture);
      }
      catch (IOException e) {
        Log.error("NMLModelOnlineLayer: Failed to load texture! " + e.getMessage());
        break;
      }
    }
    return textureMap;
  }

  private void downloadTextures(LongArrayList ids) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < ids.size(); i++) {
      if (i > 0) {
        sb.append(",");
      }
      sb.append(Long.toString(ids.get(i)));
    }
    Uri.Builder uri = Uri.parse(baseUrl).buildUpon();
    uri.appendQueryParameter("q", "Textures");
    uri.appendQueryParameter("ids", sb.toString());
    Log.debug("NMLModelOnlineLayer: fetchTextures url " + uri.build().toString());

    try {
      HttpURLConnection conn = (HttpURLConnection) new URL(uri.build().toString()).openConnection();
      if (!checkConnection(conn)) {
        return;
      }
      DataInputStream data = new DataInputStream(conn.getInputStream());
      while (true) {
        long id = data.readLong();
        if (id == -1) {
          break;
        }
        int length = data.readInt();
        byte[] compressedData = new byte[length];
        data.readFully(compressedData);
        synchronized(this) {
          texturePersistentCache.add(id, compressedData);
          textureDownloadSet.remove(id);
        }
      }
    } catch (IOException e) {
      Log.error("NMLModelOnlineLayer: Failed to download textures! " + e.getMessage());
    }

    updateVisibleElements();
  }

  private synchronized void cancelTextureDownload(LongArrayList ids) {
    for (int i = 0; i < ids.size(); i++) {
      textureDownloadSet.remove(ids.get(i));
    }
  }

  private boolean checkConnection(HttpURLConnection conn) {
    String contentType = conn.getContentType();
    if (contentType == null || !contentType.equals(NML_HTTP_CONTENT_TYPE)) {
      Log.error("NMLModelOnlineLayer: illegal HTTP content type");
      return false;
    }
    return true;
  }
}
