package com.nutiteq.vectorlayers;

import com.nutiteq.geometry.Text;
import com.nutiteq.projections.Projection;
import com.nutiteq.vectordatasources.VectorDataSource;

/**
 * Layer for displaying texts.
 */
public class TextLayer extends BillBoardLayer<Text> {

  private boolean textFading = false;

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the layer.
   */
  public TextLayer(Projection projection) {
    super(projection);
  }

  /**
   * Constructor with explicit data source.
   * 
   * @param dataSource
   *          data source for the layer.
   */
  public TextLayer(VectorDataSource<Text> dataSource) {
    super(dataSource);
  }

  /**
   * Get text fading state for this layer. By default
   * fading is disabled for all layers.
   * 
   * @return fading flag value
   */
  public boolean isTextFading() {
    return textFading;
  }

  /**
   * Enable or disable text fading for this layer. By default
   * fading is disabled for all layers.
   * 
   * @param
   *      textFading fading flag value
   */
  public void setTextFading(boolean textFading) {
    this.textFading = textFading;
  }

}
