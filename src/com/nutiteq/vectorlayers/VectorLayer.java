package com.nutiteq.vectorlayers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import com.nutiteq.components.CameraState;
import com.nutiteq.components.Components;
import com.nutiteq.components.CullState;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MutableEnvelope;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.layers.Layer;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderers.MapRenderer;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.tasks.Task;
import com.nutiteq.utils.Const;
import com.nutiteq.vectordatasources.QuadTreeVectorDataSource;
import com.nutiteq.vectordatasources.VectorDataSource;

/**
 * The abstract layer class, inherited by all layers that contain some sort of
 * vector data. Contains methods for adding and removing new vector data.
 * 
 * @param <T>
 *          vector element type for this layer.
 */
public abstract class VectorLayer<T extends VectorElement> extends Layer {
  
  /**
   * Task for querying currently visible elements based on envelope/zoom.
   */
  private class VectorCalcVisibleTask implements Task {
    private final CullState cullState;

    public VectorCalcVisibleTask(CullState cullState) {
      this.cullState = cullState;
    }

    @Override
    public void run() {
      // Load visible elements
      List<T> visibleElements = loadVisibleElements(cullState);

      // Update styles
      for (T element : visibleElements) {
        element.setActiveStyle(cullState.zoom);
      }

      // Set visible list
      setVisibleElements(visibleElements);
    }

    @Override
    public boolean isCancelable() {
      return true;
    }

    @Override
    public void cancel() {
    }
  }
  
  /**
   * Listener for data source 'on change' events.
   */
  protected class DataSourceChangeListener implements VectorDataSource.OnChangeListener {
    @Override
    public void onElementChanged(VectorElement element) {
      updateVisibleElements();
    }

    @Override
    public void onElementsChanged() {
      updateVisibleElements();
    }
  }

  private volatile List<T> visibleElementsList;
  private volatile AtomicLong visibleElementsTimeStamp = new AtomicLong();
  private volatile Set<T> dataSourceElements = new HashSet<T>();
  private Boolean calculateVisibleElementsOverridden = null;

  protected final VectorDataSource<T> dataSource;
  protected DataSourceChangeListener dataSourceListener = null;
  protected final QuadTreeVectorDataSource<T> internalDataSource;
  protected DataSourceChangeListener internalDataSourceListener = null;

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the layer.
   */
  protected VectorLayer(Projection projection) {
    super(projection);
    this.dataSource = null;
    this.internalDataSource = new QuadTreeVectorDataSource<T>(projection);
  }

  /**
   * Constructor with explicit data source.
   * 
   * @param dataSource
   *          data source for the layer.
   */
  protected VectorLayer(VectorDataSource<T> dataSource) {
    super(dataSource.getProjection());
    this.dataSource = dataSource;
    this.internalDataSource = new QuadTreeVectorDataSource<T>(projection);
  }

  /**
   * Remove all elements from layer.
   * Note: only locally added elements are removed, data source is not changed.
   */
  public void clear() {
    Collection<T> elements = internalDataSource.getAll();
    internalDataSource.clear();
    for (T element : elements) {
      element.detachFromLayer();
    }
  }

  /**
   * Add a new element to layer.
   * Note: element is only added locally and not to the data source.
   * 
   * @param element
   *          the new element to be added
   * @throws IllegalArgumentException
   *          if element has already been added or element coordinates contain NaNs
   */
  public void add(T element) {
    List<T> elements = new ArrayList<T>();
    elements.add(element);
    addAll(elements);
  }

  /**
   * Add collection of elements to the layer.
   * Note: elements are only added locally and not to the data source.
   * 
   * @param elements
   *          the collection of elements to be added
   * @throws IllegalArgumentException
   *          if one of the element has already been added or element coordinates contain NaNs
   */
  public void addAll(Collection<? extends T> elements) {
    for (T element : elements) {
      element.attachToLayer(this);
    }
    internalDataSource.addAll(elements);
  }

  /**
   * Remove an existing element from the layer.
   * Note: only locally added elements can be removed, data source is not changed.
   * 
   * @param element
   *          the element to be removed
   */
  public void remove(T element) {
    List<T> elements = new ArrayList<T>();
    elements.add(element);
    removeAll(elements);
  }

  /**
   * Remove collection of elements from the layer.
   * Note: only locally added elements can be removed, data source is not changed.
   * 
   * @param elements
   *          the collection of elements to be removed
   */
  public void removeAll(Collection<? extends T> elements) {
    internalDataSource.removeAll(elements);
    for (T element : elements) {
      element.detachFromLayer();
    }
  }

  /**
   * Get all elements added to the layer via add/addAll methods.
   * Note: this does NOT include attached data source elements! 
   * 
   * @return collection of all elements.
   */
  public Collection<T> getAll() {
    return internalDataSource.getAll();
  }

  /**
   * Called when an existing element is changed.
   * 
   * @param element
   *          the changed element
   * @pad.exclude
   */
  public void onElementChanged(VectorElement element) {
    if (renderProjection != null) {
      element.calculateInternalState();
      visibleElementsTimeStamp.incrementAndGet();

      Components components = getComponents();
      if (components != null) {
        components.mapRenderers.getMapRenderer().requestRenderView();
      }
    }
  }
  
  /**
   * Get the extent of all elements in the layer. Extent includes all added elements plus all elements from the data source (if specified).
   * 
   * @return tight envelope containing all layer elements
   */
  public Envelope getDataExtent() {
    MutableEnvelope envelope = new MutableEnvelope();

    Envelope internalDataSourceEnvelope = internalDataSource.getDataExtent();
    if (internalDataSourceEnvelope != null) {
      envelope.add(internalDataSourceEnvelope);
    }

    if (dataSource != null) {
      Envelope dataSourceEnvelope = dataSource.getDataExtent();
      if (dataSourceEnvelope == null) {
        return null;
      }
      envelope.add(dataSourceEnvelope);
    }

    return new Envelope(envelope);
  }

  /**
   * Get layer datasource.
   * 
   * @return active data source or null if datasource is not set.
   */
  public VectorDataSource<T> getDataSource() {
    return dataSource;
  }

  @Override
  public void setRenderProjection(RenderProjection renderProjection) {
    if (renderProjection != this.renderProjection) {
      this.renderProjection = renderProjection;
      setVisibleElements(null);

      if (renderProjection != null) {
        synchronized (dataSourceElements) {
          for (T element : dataSourceElements) {
            element.calculateInternalState();
          }
        }

        Collection<T> elements = internalDataSource.getAll();
        for (T element : elements) {
          element.calculateInternalState();
        }
        internalDataSource.clear();
        internalDataSource.addAll(elements);
      }

      updateVisibleElements();
    }
  }

  @Override
  public void setVisible(boolean visible) {
    if (this.visible != visible) {
      this.visible = visible;
      updateVisibleElements();
    }
  }
  
  /**
   * Not part of public API.
   *
   * @pad.exclude
   */
  public long getVisibleElementsTimeStamp() {
    return visibleElementsTimeStamp.get();
  }

  /**
   * Get list of visible element. This method is intended for subclasses and
   * renderers.
   * 
   * @return list of visible elements. Can be null.
   */
  public List<T> getVisibleElements() {
    return visibleElementsList;
  }

  /**
   * Set visible elements list. This method is intended for subclasses. 
   * It should be called at the end of calculateVisibleElements(). Note: after
   * calling this method, caller MUST NOT modify the list anymore.
   * 
   * @param visibleElements
   *          list of visible elements.
   */
  public void setVisibleElements(List<T> visibleElements) {
    this.visibleElementsList = visibleElements;
    this.visibleElementsTimeStamp.incrementAndGet();

    Components components = getComponents();
    if (components != null) {
      components.mapRenderers.getMapRenderer().layerVisibleElementsChanged(this);
    }
  }

  /**
   * Not part of public API.
   */
  protected void executeVisibilityCalculationTask(Runnable runnable) {
    Components components = getComponents();
    if (components != null) {
      components.vectorTaskPool.execute(runnable, 0, this);
    }
  }
  
  /**
   * Not part of public API.
   */
  protected Task createCullTask(CullState cullState) {
    return new VectorCalcVisibleTask(cullState);
  }

  /**
   * This method is intended for culling task. Note: as this function is called
   * from a different thread, all data accessed here should be synchronized.
   * 
   * @pad.exclude
   */
  public void calculateVisibleElements(CullState cullState) {
    // Determine if calculateVisibleElements(Envelope, int) is overridden or not. Important for backward-compatibility
    if (calculateVisibleElementsOverridden == null) {
      try {
        calculateVisibleElementsOverridden = getClass().getMethod("calculateVisibleElements", Envelope.class, int.class).getDeclaringClass() != VectorLayer.class;
      } catch (SecurityException e) {
        calculateVisibleElementsOverridden = false;
      } catch (NoSuchMethodException e) {
        calculateVisibleElementsOverridden = false;
      }
    }

    if (!calculateVisibleElementsOverridden) {
      executeVisibilityCalculationTask(createCullTask(cullState));
    } else {
      calculateVisibleElements(cullState.envelope, cullState.zoom);
    }
  }

  /**
   * This method is intended for culling task. Note: as this function is called
   * from a different thread, all data accessed here should be synchronized.
   */
  public void calculateVisibleElements(Envelope envelope, int zoom) {
  }

  /**
   * Not part of public API.
   * Request the update of visible elements list.
   */
  public void updateVisibleElements() {
    Components components = getComponents();
    if (components != null) {
      components.mapRenderers.getMapRenderer().layerChanged(this);
    }
  }
  
  /**
   * Not part of public API.
   * Load visible elements from data source and attach to layer.
   */
  @SuppressWarnings("unchecked")
  protected List<T> loadVisibleElements(CullState cullState) {
    Components components = getComponents();

    List<T> visibleElements = new ArrayList<T>();
    visibleElements.addAll(internalDataSource.loadElements(cullState));

    if (components != null) {
      VectorElement element = components.mapRenderers.getMapRenderer().getSelectedVectorElement();
      if (element != null) {
        if (element.getLayer() == this && element.getDataSource() == internalDataSource) {
          if (!visibleElements.contains(element)) {
            visibleElements.add((T) element);
          }
        }
      }
    }

    if (dataSource != null) {
      Collection<T> elements = dataSource.loadElements(cullState);
      if (elements == null) {
        elements = new ArrayList<T>();
      }

      synchronized (dataSourceElements) {
        Set<T> visibleDataSourceElements = new HashSet<T>(elements.size() + 1);
        for (T element : elements) {
          if (!dataSourceElements.contains(element)) {
            element.attachToLayer(VectorLayer.this);
          }
          visibleDataSourceElements.add(element);
        }

        if (components != null) {
          VectorElement element = components.mapRenderers.getMapRenderer().getSelectedVectorElement();
          if (element != null) {
            if (element.getLayer() == this && element.getDataSource() == dataSource) {
              if (!visibleDataSourceElements.contains(element)) {
                visibleDataSourceElements.add((T) element);
              }
            }
          }
        }

        for (T element : dataSourceElements) {
          if (!visibleDataSourceElements.contains(element)) {
            element.detachFromLayer();
          }
        }

        visibleElements.addAll(visibleDataSourceElements);
        dataSourceElements = visibleDataSourceElements;
      }
    }

    return visibleElements;
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  public int getCurrentZoomLevel() {
    Components components = getComponents();
    if (components != null) {
      MapRenderer mapRenderer = components.mapRenderers.getMapRenderer();

      mapRenderer.setGeneralLock(true);
      try {
        CameraState camera = mapRenderer.getCameraState();
        return (int) (camera.zoom + Const.DISCRETE_ZOOM_LEVEL_BIAS);
      }
      finally {
        mapRenderer.setGeneralLock(false);
      }
    }
    return 0;
  }
  
  /**
   * Not part of public API.
   * @pad.exclude
   */
  public void onStartMapping() {
    visibleElementsList = null;
    visibleElementsTimeStamp.incrementAndGet();
  }

  /**
   * Not part of public API
   * @pad.exclude
   */
  public void onStopMapping() {
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  protected void registerDataSourceListeners() {
    if (dataSource != null) {
      dataSourceListener = new DataSourceChangeListener();
      dataSource.addOnChangeListener(dataSourceListener);
    }
    internalDataSourceListener = new DataSourceChangeListener();
    internalDataSource.addOnChangeListener(internalDataSourceListener);
  }

  /**
   * Not part of public API.
   * @pad.exclude
   */
  protected void unregisterDataSourceListeners() {
    internalDataSource.removeOnChangeListener(internalDataSourceListener);
    internalDataSourceListener = null;
    if (dataSource != null) {
      dataSource.removeOnChangeListener(dataSourceListener);
      dataSourceListener = null;
    }
  }

  @Override
  public synchronized void setComponents(Components components) {
    super.setComponents(components);
    if (components != null && internalDataSourceListener == null) {
      registerDataSourceListeners();
    } else if (components == null && internalDataSourceListener != null) {
      unregisterDataSourceListeners();
    }
  }
}
