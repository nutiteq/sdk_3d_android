package com.nutiteq.vectorlayers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import com.nutiteq.cache.CompressedMemoryCache;
import com.nutiteq.cache.VectorTileCache;
import com.nutiteq.components.Bounds;
import com.nutiteq.components.Components;
import com.nutiteq.components.TileData;
import com.nutiteq.geometry.BillBoard;
import com.nutiteq.geometry.Marker;
import com.nutiteq.geometry.Text;
import com.nutiteq.layers.TileLayer;
import com.nutiteq.log.Log;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderers.components.MapTileDrawData;
import com.nutiteq.renderers.components.MapTileProxy;
import com.nutiteq.renderers.components.MapTileQuadTreeNode;
import com.nutiteq.tasks.Task;
import com.nutiteq.utils.Const;
import com.nutiteq.utils.LongHashMap;
import com.nutiteq.utils.LongMap;
import com.nutiteq.vectordatasources.VectorTileDataSource;
import com.nutiteq.vectortile.VectorTile;
import com.nutiteq.vectortile.VectorTileReader;

public class VectorTileLayer extends TileLayer {
  private static final int CACHE_FETCH_TASK_PRIORITY = Integer.MAX_VALUE; // Priority for cache fetch tasks
  private static final int CREATE_TILE_TASK_PRIORITY = Integer.MAX_VALUE;

  /**
   * Task for fetching tiles from persistent cache.
   */
  private class CacheFetchTileTask implements Task {
    private final MapTileQuadTreeNode targetTile;
    private final MapTileQuadTreeNode tile;
    private final Components components;

    public CacheFetchTileTask(MapTileQuadTreeNode targetTile, MapTileQuadTreeNode tile, Components components) {
      this.targetTile = targetTile;
      this.tile = tile;
      this.components = components;
      components.retrievingTiles.add(tileIdOffset + targetTile.id);
    }

    @Override
    public void run() {
      byte[] compressed = components.persistentCache.get(tileIdOffset + tile.id);
      if (compressed != null) {
        TileData tileData = new TileData(compressed);
        VectorTile vectorTile = tileReader.createTile(tile, tileData, targetTile, getTargetTileBounds(tile, targetTile));
        if (vectorTile == null) {
          Log.error("DataSourceFetchTask: could not create tile");
        } else {
          byte[] data = tileData.getData();
          if (memoryCaching) {
            components.compressedMemoryCache.add(tileIdOffset + tile.id, data);
          }
          synchronized (vectorTileMap) {
            vectorTileMap.put(tile, vectorTile);
          }
          components.vectorTileCache.add(tileIdOffset + targetTile.id, vectorTile);
        }
        components.retrievingTiles.remove(tileIdOffset + targetTile.id);
        components.mapRenderers.getMapRenderer().requestRenderView();
      } else {
        components.retrievingTiles.remove(tileIdOffset + targetTile.id);
        if (getComponents() != null) {
          fetchTile(targetTile, tile);
        }
      }
    }

    @Override
    public boolean isCancelable() {
      return true;
    }

    @Override
    public void cancel() {
      components.retrievingTiles.remove(tileIdOffset + targetTile.id);
    }
  }

  /**
   * Task for fetching tiles from data source.
   */
  private class LoadTileTask implements Task {
    private final MapTileQuadTreeNode targetTile;
    private final MapTileQuadTreeNode tile;
    private final TileData tileData;
    private final Components components;

    public LoadTileTask(MapTileQuadTreeNode targetTile, MapTileQuadTreeNode tile, TileData tileData, Components components) {
      this.targetTile = targetTile;
      this.tile = tile;
      this.tileData = tileData;
      this.components = components;
      components.retrievingTiles.add(tileIdOffset + targetTile.id);
    }

    @Override
    public void run() {
      try {
        MapTileQuadTreeNode tile = this.tile;
        TileData tileData = this.tileData;
        if (tileData == null) {
          tileData = dataSource.loadTile(tile);
        }
        if (tileData == null) {
          Log.error("LoadTileTask: no tile data");
        } else {
          VectorTile vectorTile = tileReader.createTile(tile, tileData, targetTile, getTargetTileBounds(tile, targetTile));
          if (vectorTile == null) {
            Log.error("LoadTileTask: could not create tile");
          } else {
            byte[] data = tileData.getData();
            if (persistentCaching) {
              components.persistentCache.add(tileIdOffset + tile.id, data);
            }
            if (memoryCaching) {
              components.compressedMemoryCache.add(tileIdOffset + tile.id, data);
            }
            synchronized (vectorTileMap) {
              vectorTileMap.put(tile, vectorTile);
            }
            components.vectorTileCache.add(tileIdOffset + targetTile.id, vectorTile);
          }
        }
      } catch (Exception e) {
        Log.error("LoadTileTask: failed to fetch tile: " + e.getMessage());
      }
      components.retrievingTiles.remove(tileIdOffset + targetTile.id);
      components.mapRenderers.getMapRenderer().requestRenderView();
    }

    @Override
    public boolean isCancelable() {
      return true;
    }

    @Override
    public void cancel() {
      components.retrievingTiles.remove(tileIdOffset + targetTile.id);
    }
  }

  /**
   * Listener for data source 'on change' events.
   */
  protected class DataSourceChangeListener implements VectorTileDataSource.OnChangeListener {
    @Override
    public void onTilesChanged() {
      long firstTileId = tileIdOffset;
      long lastTileId = tileIdOffset + Const.TILE_ID_OFFSET;
      Components components = getComponents();
      if (components != null) {
        // Stop all pending tasks
        while (true) {
          Thread.yield();
          
          // Check that there are no pending tasks: we have to wait until last one has finished
          synchronized (components.rasterTaskPool) {
            components.rasterTaskPool.cancelAll();
            if (components.rasterTaskPool.getActiveWorkerCount() > 0) {
              continue;
            }
        
            // Remove tile ranges from caches and recalculate visible tiles
            if (persistentCaching) {
              components.persistentCache.removeRange(firstTileId, lastTileId);
            }
            if (memoryCaching) {
              components.compressedMemoryCache.removeRange(firstTileId, lastTileId);
            }
            components.vectorTileCache.removeRange(firstTileId, lastTileId);
          }
          updateVisibleTiles();
          break;
        }
      }
    }
  }

  protected final VectorTileDataSource dataSource;
  protected final VectorTileReader tileReader;
  protected DataSourceChangeListener dataSourceListener = null;

  private final Map<MapTileQuadTreeNode, VectorTile> vectorTileMap = new HashMap<MapTileQuadTreeNode, VectorTile>();
  private volatile List<MapTileDrawData> visibleTiles;
  private volatile List<BillBoardLayer<?>> billBoardLayers = new ArrayList<BillBoardLayer<?>>();
  private volatile AtomicLong visibleTilesTimeStamp = new AtomicLong();

  public VectorTileLayer(VectorTileDataSource dataSource, VectorTileReader tileReader, int id) {
    super(dataSource.getProjection(), Math.max(dataSource.getMinZoom(), tileReader.getMinZoom()), tileReader.getMaxZoom(), id);
    this.dataSource = dataSource;
    this.tileReader = tileReader;
  }

  protected VectorTileLayer(Projection projection, int minZoom, int maxZoom, int id) {
    super(projection, minZoom, maxZoom, id);
    this.dataSource = null;
    this.tileReader = null;
  }

  @Override
  public boolean getTilePreloading() {
    return false;
  }

  public List<BillBoardLayer<?>> getBillBoardLayers() {
    return billBoardLayers;
  }

  @Override
  public long getVisibleTilesTimeStamp() {
    return visibleTilesTimeStamp.get();
  }

  @Override
  public List<MapTileDrawData> getVisibleTiles() {
    return visibleTiles;
  }

  public VectorTile getVisibleVectorTile(MapTileQuadTreeNode tile) {
    synchronized (vectorTileMap) {
      return vectorTileMap.get(tile);
    }
  }

  @Override
  public void setVisibleTiles(List<MapTileDrawData> visibleTiles) {
    this.visibleTiles = visibleTiles;
    Set<MapTileQuadTreeNode> visibleTileSet = new HashSet<MapTileQuadTreeNode>();
    if (visibleTiles != null) {
      for (MapTileDrawData tileDrawData : visibleTiles) {
        visibleTileSet.add(tileDrawData.proxy.mapTile);
      }
    }
    synchronized (vectorTileMap) {
      for (Iterator<Map.Entry<MapTileQuadTreeNode, VectorTile>> it = vectorTileMap.entrySet().iterator(); it.hasNext(); ) {
        Map.Entry<MapTileQuadTreeNode, VectorTile> entry = it.next();
        if (!visibleTileSet.contains(entry.getKey())) {
          it.remove();
        }
      }
    }
    generateVisibleTexts();
    visibleTilesTimeStamp.incrementAndGet();

    Components components = getComponents();
    if (components != null) {
      components.mapRenderers.getMapRenderer().layerVisibleElementsChanged(this);
    }
  }

  @Override
  public boolean resolveTile(MapTileQuadTreeNode targetTile, List<MapTileProxy> proxies) {
    Components components = getComponents();
    if (components == null) {
      return false;
    }

    MapTileQuadTreeNode tile = targetTile;
    while (tile.zoom > dataSource.getMaxZoom()) {
      tile = tile.parent;
    }

    VectorTileCache vectorTileCache = components.vectorTileCache;
    CompressedMemoryCache compressedMemoryCache = components.compressedMemoryCache;

    {
      long fullTileId = tileIdOffset + targetTile.id;
      VectorTile vectorTile = vectorTileCache.get(fullTileId);
      if (vectorTile != null) {
        synchronized (vectorTileMap) {
          vectorTileMap.put(targetTile, vectorTile);
        }
        proxies.add(new MapTileProxy(fullTileId, targetTile));
        return true;
      }
    }

    MapTileProxy proxy = null;

    // Lookup tile children
    for (MapTileQuadTreeNode parentTile = targetTile.parent; parentTile != null; parentTile = parentTile.parent) {
      long fullTileId = tileIdOffset + parentTile.id;
      VectorTile vectorTile = vectorTileCache.getWithoutMod(fullTileId);
      if (vectorTile != null) {
        synchronized (vectorTileMap) {
          vectorTileMap.put(targetTile, vectorTile);
        }
        proxy = new MapTileProxy(fullTileId, targetTile);
        break;
      }
    }
    if (proxy != null) {
      proxies.add(proxy);
    } else {
      // Lookup children, but only single level
      for (int i = 0; i < 4; i++) {
        MapTileQuadTreeNode childTile = targetTile.getChild(i);
        long fullTileId = tileIdOffset + childTile.id;
        VectorTile vectorTile = vectorTileCache.getWithoutMod(fullTileId); // Note: use withoutMod-version for now, reduces cache trashing
        if (vectorTile != null) {
          synchronized (vectorTileMap) {
            vectorTileMap.put(childTile, vectorTile);
          }
          proxies.add(new MapTileProxy(fullTileId, childTile));
        }
      }
    }

    // Check the compressed memory cache
    if (!components.retrievingTiles.contains(tileIdOffset + targetTile.id)) {
      byte[] compressed = null;
      if (isMemoryCaching()) {
        compressed = compressedMemoryCache.get(tileIdOffset + tile.id);
      }
      if (compressed == null) {
        // If persistent caching is not enabled for the layer or tile is not in the cache, do tile fetch directly.
        if (isPersistentCaching() && components.persistentCache.contains(tileIdOffset + tile.id)) {
          components.rasterTaskPool.execute(new CacheFetchTileTask(targetTile, tile, components), CACHE_FETCH_TASK_PRIORITY);
        } else {
          fetchTile(targetTile, tile);
        }
      } else {
        components.rasterTaskPool.execute(new LoadTileTask(targetTile, tile, new TileData(compressed), components), CREATE_TILE_TASK_PRIORITY);
      }
    }
    return true;
  }

  public void fetchTile(MapTileQuadTreeNode targetTile, MapTileQuadTreeNode tile) {
    if (dataSource == null) {
      throw new IllegalArgumentException("VectorTileLayer: data source not set!");
    }
    int tileZoom = tile.zoom;
    if (tileZoom < minZoom || tileZoom > maxZoom) {
      return;
    }
    Components components = getComponents();
    if (components == null) {
      return;
    }
    executeFetchTask(new LoadTileTask(targetTile, tile, null, components));
  }
  
  protected Bounds getTargetTileBounds(MapTileQuadTreeNode tile, MapTileQuadTreeNode targetTile) {
    float tileX = 0, tileY = 0;
    float tileSize = 1;
    while (true) {
      if (targetTile == tile) {
        break;
      }
      tileX = (tileX + MapTileQuadTreeNode.getChildX(0, targetTile.nodeType)) * 0.5f;
      tileY = (tileY + MapTileQuadTreeNode.getChildY(0, targetTile.nodeType)) * 0.5f;
      tileSize = tileSize * 0.5f;
      targetTile = targetTile.parent;
    }
    return new Bounds(tileX, tileY + tileSize, tileX + tileSize, tileY);
  }

  protected void executeFetchTask(Runnable runnable) {
    Components components = getComponents();
    if (components != null) {
      components.rasterTaskPool.execute(runnable, fetchPriority); // TODO: separate pool? or rename?
    }
  }

  protected synchronized void generateVisibleTexts() {
    List<MapTileDrawData> visibleTiles = this.visibleTiles;
    if (visibleTiles == null) {
      billBoardLayers = new ArrayList<BillBoardLayer<?>>();
      return;
    }

    LongMap<Text> textMap = new LongHashMap<Text>();
    LongMap<Marker> markerMap = new LongHashMap<Marker>();
    
    // Collect visible elements
    LongMap<Text> visibleTextMap = new LongHashMap<Text>();
    LongMap<Marker> visibleMarkerMap = new LongHashMap<Marker>();
    for (BillBoardLayer<?> layer : billBoardLayers) {
      for (BillBoard billBoard : layer.getVisibleElements()) {
        if (billBoard.getId() == -1) {
          continue;
        }
        
        if (billBoard instanceof Marker) {
          visibleMarkerMap.put(billBoard.getId(), (Marker) billBoard);
        } else if (billBoard instanceof Text) {
          visibleTextMap.put(billBoard.getId(), (Text) billBoard);
        }
      }
    }
    
    // Pass 1: previously visible elements
    for (MapTileDrawData drawData : visibleTiles) {
      VectorTile tile = vectorTileMap.get(drawData.proxy.mapTile);
      if (tile == null) {
        continue;
      }

      for (BillBoardLayer<?> layer : tile.getBillBoardLayers()) {
        for (BillBoard billBoard : layer.getVisibleElements()) {
          if (billBoard.getId() == -1) {
            continue;
          }

          if (billBoard instanceof Marker) {
            Marker marker = (Marker) billBoard;
            Marker visibleMarker = visibleMarkerMap.get(billBoard.getId()); 
            if (visibleMarker == null) {
              continue;
            }
            if (markerMap.containsKey(billBoard.getId())) {
              continue;
            }
            visibleMarker.setStyleSet(marker.getStyleSet());
            visibleMarker.setActiveStyle(0);
            markerMap.put(billBoard.getId(), visibleMarker);
          } else if (billBoard instanceof Text) {
            Text text = (Text) billBoard;
            Text visibleText = visibleTextMap.get(billBoard.getId());
            if (visibleText == null) {
              continue;
            }
            if (textMap.containsKey(billBoard.getId())) {
              continue;
            }
            visibleText.setStyleSet(text.getStyleSet());
            visibleText.setActiveStyle(0);
            textMap.put(billBoard.getId(), visibleText);
          }
        }
      }
    }
    
    // Pass 2: other elements
    for (MapTileDrawData drawData : visibleTiles) {
      VectorTile tile = vectorTileMap.get(drawData.proxy.mapTile);
      if (tile == null) {
        continue;
      }

      for (BillBoardLayer<?> layer : tile.getBillBoardLayers()) {
        for (BillBoard billBoard : layer.getVisibleElements()) {
          if (billBoard.getId() == -1) {
            continue;
          }

          if (billBoard instanceof Marker) {
            if (markerMap.containsKey(billBoard.getId())) {
              continue;
            }
            markerMap.put(billBoard.getId(), (Marker) billBoard);
          } else if (billBoard instanceof Text) {
            if (textMap.containsKey(billBoard.getId())) {
              continue;
            }
            textMap.put(billBoard.getId(), (Text) billBoard);
          }
        }
      }
    }

    // Rebuild billboard layers
    if (billBoardLayers.isEmpty()) {
      List<BillBoardLayer<?>> layers = new ArrayList<BillBoardLayer<?>>();
      
      TextLayer textLayer = new TextLayer(getProjection());
      textLayer.setZOrdered(false);
      textLayer.setTextFading(true);
      layers.add(textLayer);
      
      MarkerLayer markerLayer = new MarkerLayer(getProjection());
      markerLayer.setZOrdered(false);
      layers.add(markerLayer);
      
      billBoardLayers = layers;
    }
    
    TextLayer textLayer = (TextLayer) billBoardLayers.get(0);
    textLayer.setVisibleElements(new ArrayList<Text>(textMap.values()));
    
    MarkerLayer markerLayer = (MarkerLayer) billBoardLayers.get(1);
    markerLayer.setVisibleElements(new ArrayList<Marker>(markerMap.values()));
  }

  /**
   * Not part of public API.
   * 
   * @pad.exclude
   */
  protected void registerDataSourceListener() {
    dataSourceListener = new DataSourceChangeListener();
    dataSource.addOnChangeListener(dataSourceListener);
  }

  /**
   * Not part of public API.
   * 
   * @pad.exclude
   */
  protected void unregisterDataSourceListener() {
    dataSource.removeOnChangeListener(dataSourceListener);
    dataSourceListener = null;
  }

  @Override
  public synchronized void setComponents(Components components) {
    super.setComponents(components);
    if (dataSource != null) {
      if (components != null && dataSourceListener == null) {
        registerDataSourceListener();
      } else if (components == null && dataSourceListener != null) {
        unregisterDataSourceListener();
      }
    }
  }
}
