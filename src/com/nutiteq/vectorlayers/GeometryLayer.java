package com.nutiteq.vectorlayers;

import com.nutiteq.geometry.Geometry;
import com.nutiteq.projections.Projection;
import com.nutiteq.vectordatasources.VectorDataSource;

/**
 * Layer for all geometric elements (points, lines, polygons).
 */
public class GeometryLayer extends VectorLayer<Geometry> {

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the layer.
   */
  public GeometryLayer(Projection projection) {
    super(projection);
  }

  /**
   * Constructor with explicit data source.
   * 
   * @param dataSource
   *          data source for the layer.
   */
  public GeometryLayer(VectorDataSource<Geometry> dataSource) {
    super(dataSource);
  }
}
