package com.nutiteq.vectorlayers;

import java.util.LinkedList;
import java.util.List;

import android.view.MotionEvent;

import com.nutiteq.components.Components;
import com.nutiteq.geometry.DynamicMarker;
import com.nutiteq.geometry.Marker;
import com.nutiteq.projections.Projection;
import com.nutiteq.ui.MapTouchListener;
import com.nutiteq.vectordatasources.VectorDataSource;

/**
 * Layer for static and dynamic markers.
 */
public class MarkerLayer extends BillBoardLayer<Marker> {
  
  protected class DynamicMarkerTouchListener implements MapTouchListener {
    @Override
    public boolean onTouchEvent(MotionEvent event) {
      if (event.getPointerCount() > 1) {
        return false;
      }
      List<DynamicMarker> visibleDynamicMarkers = MarkerLayer.this.visibleDynamicMarkers;
      if (visibleDynamicMarkers != null) {
        for (DynamicMarker dynamicMarker : visibleDynamicMarkers) {
          if (!dynamicMarker.isVisible()) {
            continue;
          }
          if (dynamicMarker.onDragStart(event.getX(), event.getY())) {
            return true;
          }
        }
      }
      return false;
    }
  }
  
  protected List<DynamicMarker> visibleDynamicMarkers = null;
  protected DynamicMarkerTouchListener dynamicMarkerTouchListener = null;

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the layer.
   */
  public MarkerLayer(Projection projection) {
    super(projection);
  }

  /**
   * Constructor with explicit data source.
   * 
   * @param dataSource
   *          data source for the layer.
   */
  public MarkerLayer(VectorDataSource<Marker> dataSource) {
    super(dataSource);
  }
  
  @Override
  public void setVisibleElements(List<Marker> visibleElements) {
    List<DynamicMarker> visibleDynamicMarkers = null;
    if (visibleElements != null) {
      for (Marker marker : visibleElements) {
        if (marker instanceof DynamicMarker) {
          if (visibleDynamicMarkers == null) {
            visibleDynamicMarkers = new LinkedList<DynamicMarker>();
          }
          visibleDynamicMarkers.add((DynamicMarker) marker);
        }
      }
    }
    this.visibleDynamicMarkers = visibleDynamicMarkers;
    super.setVisibleElements(visibleElements);
  }

  @Override
  public synchronized void setComponents(Components components) {
    Components oldComponents = getComponents();
    if (oldComponents != null && dynamicMarkerTouchListener != null) {
      oldComponents.options.removeMapTouchListener(dynamicMarkerTouchListener);
      dynamicMarkerTouchListener = null;
    }
    super.setComponents(components);
    if (components != null && dynamicMarkerTouchListener == null) {
      dynamicMarkerTouchListener = new DynamicMarkerTouchListener();
      components.options.addMapTouchListener(dynamicMarkerTouchListener);
    }
  }
}
