package com.nutiteq.vectorlayers;

import com.nutiteq.components.Components;
import com.nutiteq.geometry.BillBoard;
import com.nutiteq.projections.Projection;
import com.nutiteq.vectordatasources.VectorDataSource;

/**
 * Layer for all billboard elements (markers, texts).
 * This class can not be directly instantiated, use derived TextLayer or MarkerLayer instead.
 */
public class BillBoardLayer<T extends BillBoard> extends VectorLayer<T> {

  private boolean zOrdered = true;

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the layer.
   */
  protected BillBoardLayer(Projection projection) {
    super(projection);
  }

  /**
   * Constructor for explicit data source.
   * 
   * @param dataSource
   *          data source for the layer.
   */
  protected BillBoardLayer(VectorDataSource<T> dataSource) {
    super(dataSource);
  }

  /**
   * Get the Z ordering state for this layer.
   * 
   * @return current Z ordering state. True if enabled (default).          
   */
  public boolean isZOrdered() {
    return zOrdered;
  }

  /**
   * Set the Z ordering state for this layer.
   * When enabled, elements are ordered based on distance from the camera (using camera relative Z-coordinate) and layer ordering is not preserved. This forces the layer to be in "3D" mode.
   * When disabled, elements are ordered as normal elements (points, lines, polygons), this is the preferred mode for normal 2D views as overlapping elements cause less visual artifacts.
   * 
   * @param zOrdered
   *        new Z ordering state. Default is enabled (true).          
   */
  public void setZOrdered(boolean zOrdered) {
    this.zOrdered = zOrdered;

    Components components = getComponents();
    if (components != null) {
      components.mapRenderers.getMapRenderer().requestRenderView();
    }
  }

}
