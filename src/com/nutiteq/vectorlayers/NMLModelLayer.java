package com.nutiteq.vectorlayers;

import com.nutiteq.geometry.NMLModel;
import com.nutiteq.projections.Projection;
import com.nutiteq.vectordatasources.VectorDataSource;

/**
 * Layer for 3D models.
 */
public class NMLModelLayer extends VectorLayer<NMLModel> {
  
  private float alphaTestRef = 0; 

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the layer.
   */
  public NMLModelLayer(Projection projection) {
    super(projection);
  }

  /**
   * Constructor with explicit data source.
   * 
   * @param dataSource
   *          data source for the layer.
   */
  public NMLModelLayer(VectorDataSource<NMLModel> dataSource) {
    super(dataSource);
  }
  
  /**
   * Get current alpha test reference value.
   * 
   * @return current alpha test reference value
   */
  public float getAlphaTestRef() {
    return alphaTestRef;
  }

  /**
   * Set alpha test reference value. If alpha of a renderer pixel is lower or equal than this, pixel is considered fully transparent.
   *
   * @param alphaTestRef
   *          reference value for alpha test. Default is 0.
   */
  public void setAlphaTestRef(float alphaTestRef) {
    this.alphaTestRef = alphaTestRef;
  }
}
