package com.nutiteq.vectorlayers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.protobuf.InvalidProtocolBufferException;
import com.nutiteq.components.CullState;
import com.nutiteq.components.Envelope;
import com.nutiteq.components.MapPos;
import com.nutiteq.components.MutableEnvelope;
import com.nutiteq.geometry.NMLModel;
import com.nutiteq.log.Log;
import com.nutiteq.nmlpackage.NMLPackage;
import com.nutiteq.projections.EPSG3857;
import com.nutiteq.projections.Projection;
import com.nutiteq.renderprojections.RenderProjection;
import com.nutiteq.style.ModelStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.tasks.Task;
import com.nutiteq.ui.DefaultLabel;
import com.nutiteq.ui.Label;
import com.nutiteq.utils.Frustum;
import com.nutiteq.utils.IntHashMap;
import com.nutiteq.utils.LinkedLongHashMap;
import com.nutiteq.utils.LongArrayList;
import com.nutiteq.utils.LongHashMap;
import com.nutiteq.utils.Matrix;

/**
 * Layer for 3D models that are read from local database file.
 * This layer user Nutiteq proprietary database file (extension .nmldb) and is still in experimental stage. Further changes may break backward compatibility.
 */
public class NMLModelDbLayer extends NMLModelLayer {
  private static final int MODELLODTREE_CACHE_SIZE = 16; // max number of LOD trees cached in memory. LOD tree parsing is quite heavy, so having a small LRU cache gives smoother user experience
  private static final float NEAR_PLANE_DISTANCE = 0.25f; // this is somewhat related to camera's near plane but should be higher, some node bounding boxes may cross near plane and these will be forced to this value
  private static final int BLOB_CHUNK_SIZE = 512 * 1024; // size of each blob chunk
  
  /**
   * Internal visibility calculation task. This is not strictly necessary (general VectorCalcVisibleTask would also work),
   * but custom implementation is more optimal by releasing cached bitmaps of hidden texts.
   */
  protected class NMLModelCalcVisibleTask implements Task {
    final CullState state;
    
    NMLModelCalcVisibleTask(CullState state) {
      this.state = state;
    }
    
    @Override
    public void run() {
      loadModels(state);
    }

    @Override
    public boolean isCancelable() {
      return true;
    }

    @Override
    public void cancel() {
    }
  }
  
  /**
   * Map tile
   */
  protected static class MapTile {
    public final long id;
    public final MapPos mapPos;
    public final long modelLODTreeId;
    
    public MapTile(long id, MapPos mapPos, long modelLODTreeId) {
      this.id = id;
      this.mapPos = mapPos;
      this.modelLODTreeId = modelLODTreeId;
    }
  }

  /**
   * Association between global mesh id and local id.
   */
  protected static class MeshBinding {
    public final long meshId;
    public final String localId;
    public final NMLPackage.MeshOp meshOp;
    
    public MeshBinding(long meshId, String localId, NMLPackage.MeshOp meshOp) {
      this.meshId = meshId;
      this.localId = localId;
      this.meshOp = meshOp;
    }
  }
  
  /**
   * Association between global texture id/level and local id.
   */
  protected static class TextureBinding {
    public final long textureId;
    public final int level;
    public final String localId;
    
    public TextureBinding(long textureId, int level, String localId) {
      this.textureId = textureId;
      this.level = level;
      this.localId = localId;
    }
  }
  
  /**
   * Level-Of-Detail tree for model
   */
  protected class ModelLODTree {
    public final long id;
    public final MapPos mapPos;
    public final ModelLODTreeNode rootNode;
    public final Map<Integer, NMLModel.Proxy> proxyMap;
    public Map<ModelLODTreeNode, NMLModel> modelMap = new HashMap<ModelLODTreeNode, NMLModel>();
    public List<ModelLODTreeNode> drawList = new ArrayList<ModelLODTreeNode>();
    
    public ModelLODTree(long id, MapPos mapPos, Projection projection, NMLPackage.ModelLODTree nmlModelLODTree, IntHashMap<List<MeshBinding>> nodeMeshBindingMap, IntHashMap<List<TextureBinding>> nodeTextureBindingMap, Map<Integer, NMLModel.Proxy> proxyMap) {
      this.id = id;
      this.mapPos = mapPos;
      if (nmlModelLODTree.getNodesCount() > 0) {
        rootNode = new ModelLODTreeNode(this, null, nmlModelLODTree, nmlModelLODTree.getNodes(0), nodeMeshBindingMap, nodeTextureBindingMap);
      } else {
        rootNode = null;
      }
      this.proxyMap = proxyMap;
    }
  }

  /**
   * Node for LOD tree, with mesh and texture bindings.
   */
  protected class ModelLODTreeNode {
    public final int id;
    public final ModelLODTree modelLODTree;
    public final ModelLODTreeNode parentNode;
    public final ModelLODTreeNode[] childNodes;
    public final NMLPackage.Model nmlModel;
    public final List<MeshBinding> meshBindings;
    public final List<TextureBinding> textureBindings;
    public final int modelFootprint;
    public final double[] modelBounds;
    public final double[] nodeCorners;
    public float screenSize;
    
    public ModelLODTreeNode(ModelLODTree modelLODTree, ModelLODTreeNode parentNode, NMLPackage.ModelLODTree nmlModelLODTree, NMLPackage.ModelLODTreeNode nmlModelLODTreeNode, IntHashMap<List<MeshBinding>> nodeMeshBindingMap, IntHashMap<List<TextureBinding>> nodeTextureBindingMap) {
      this.id = nmlModelLODTreeNode.getId();
      this.modelLODTree = modelLODTree;
      this.parentNode = parentNode;
      this.childNodes = new ModelLODTreeNode[nmlModelLODTreeNode.getChildrenIdsCount()];
      for (int i = 0; i < nmlModelLODTreeNode.getChildrenIdsCount(); i++) {
        int childId = nmlModelLODTreeNode.getChildrenIds(i);
        this.childNodes[i] = new ModelLODTreeNode(modelLODTree, this, nmlModelLODTree, nmlModelLODTree.getNodes(childId), nodeMeshBindingMap, nodeTextureBindingMap);
      }
      this.nmlModel = nmlModelLODTreeNode.getModel();
      List<MeshBinding> meshBindings = nodeMeshBindingMap.get(nmlModelLODTreeNode.getId());
      this.meshBindings = (meshBindings != null ? meshBindings : new ArrayList<MeshBinding>());
      List<TextureBinding> textureBindings = nodeTextureBindingMap.get(nmlModelLODTreeNode.getId());
      this.textureBindings = (textureBindings != null ? textureBindings : new ArrayList<TextureBinding>());
      this.modelFootprint = nmlModelLODTreeNode.getModel().getMeshFootprint() + nmlModelLODTreeNode.getModel().getTextureFootprint();
      this.modelBounds = calculateBounds(nmlModelLODTreeNode.getModel().getBounds(), modelLODTree.mapPos, projection, getRenderProjection());
      this.nodeCorners = calculateCorners(nmlModelLODTreeNode.getBounds(), modelLODTree.mapPos, projection, getRenderProjection());
    }
  }

  /**
   * Comparator class for nodes using projected screen size
   */
  private static class ModelLODTreeNodeScreenSizeComparator implements Comparator<ModelLODTreeNode> {
    public int compare(ModelLODTreeNode node1, ModelLODTreeNode node2) {
      return -Float.compare(node1.screenSize, node2.screenSize); // put bigger nodes in front of smaller nodes
    }
  }

  private final SQLiteDatabase db;
  private final boolean dbMeshOpFieldExists;
  private int minZoom;
  private int maxMemorySize = 20 * 1024 * 1024;
  private float lodResolutionFactor = 1;
  private Envelope calculateVisibleElementsEnvelope;
  protected StyleSet<ModelStyle> styleSet;
  protected Envelope mapTileEnvelope = new Envelope(0, 0, 0, 0);
  protected List<MapTile> mapTileList = new ArrayList<MapTile>(); 
  protected LongHashMap<ModelLODTree> modelLODTreeMap = new LongHashMap<ModelLODTree>();
  protected LongHashMap<NMLModel.Mesh> meshMap = new LongHashMap<NMLModel.Mesh>();
  protected LongHashMap<NMLModel.Texture> textureMap = new LongHashMap<NMLModel.Texture>();
  protected LinkedLongHashMap<ModelLODTree> modelLODTreeCache = new LinkedLongHashMap<ModelLODTree>() {
    @Override
    protected boolean removeEldestEntry(long id, ModelLODTree modelLODTree) {
      return this.size > MODELLODTREE_CACHE_SIZE;
    }
  };
  
  private static ModelLODTreeNodeScreenSizeComparator screenSizeComparator = new ModelLODTreeNodeScreenSizeComparator();
  
  /**
   * Main constructor.
   *  
   * @param proj
   *            projection for this layer. MUST BE instance of EPSG3857!
   * @param dbPath
   *            local file name for sqlite database containing output from kmz2sqlite utility.
   *            If the database does not exist or can not be opened in read-only mode, SQLiteException will be thrown.
   * @param styleSet
   *            styleSet to use for database models.
   * @throws IOException 
   *            if database does not exist of can not be opened
   * @throws UnsupportedOperationException
   *            if projection is not EPSG3857
   */
  public NMLModelDbLayer(Projection proj, String dbPath, StyleSet<ModelStyle> styleSet) throws IOException {
    super(proj);
    if (!(proj instanceof EPSG3857)) {
      throw new UnsupportedOperationException("Projection must be EPSG3857");
    }
    this.styleSet = styleSet;
    this.minZoom = styleSet.getFirstNonNullZoomStyleZoom();
    try {
      this.db = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
      this.dbMeshOpFieldExists = checkDbMeshOpField(this.db);
    } catch (Exception e) {
      throw new IOException("Can not open " + dbPath + ", " + e.getMessage());
    }
  }
  
  /**
   * Constructor for derived classes that use custom data sources.
   *  
   * @param proj
   *            projection for this layer. MUST BE instance of EPSG3857!
   * @param styleSet
   *            styleSet to use for database models.
   * @throws UnsupportedOperationException
   *            if projection is not EPSG3857
   */
  protected NMLModelDbLayer(Projection proj, SQLiteDatabase db, StyleSet<ModelStyle> styleSet) {
    super(proj);
    if (!(proj instanceof EPSG3857)) {
      throw new UnsupportedOperationException("Projection must be EPSG3857");
    }
    this.styleSet = styleSet;
    this.minZoom = styleSet.getFirstNonNullZoomStyleZoom();
    this.db = db;
    this.dbMeshOpFieldExists = checkDbMeshOpField(this.db);
  }
  
  private static boolean checkDbMeshOpField(SQLiteDatabase db) {
    if (db == null) {
      return false;
    }
    try {
      boolean found = false;
      Cursor cursor = db.rawQuery("PRAGMA table_info(ModelLODTreeNodeMeshes)", null);
      if (cursor.moveToFirst()) {
        do {
          String field = cursor.getString(1);
          if (field.equals("nmlmeshop")) {
            found = true;
          }
        } while (cursor.moveToNext());
      }
      cursor.close();

      if (found) {
        Log.info("NMLModelDbLayer: mesh compression supported");
      } else {
        Log.info("NMLModelDbLayer: mesh compression NOT supported");
      }
      return found;
    } catch (Throwable e) {
      Log.info("NMLModelDbLayer: exception while querying metainfo");
      return false;
    }
  }
  
  /**
   * Set upper limit for layer memory usage.
   * 
   * @param maxMemorySize
   *            estimated maximum memory footprint for any given view in bytes.
   *            If this limit is reached, models are shown using lower LODs.
   *            Suggested value depends on data, but for many data sets 20MB (20 * 1024 * 1024) is a good starting point.
   */
  public void setMemoryLimit(int maxMemorySize) {
    this.maxMemorySize = maxMemorySize;
  }

  /**
   * Set Level-Of-Detail resolution factor.
   * 
   * @param lodResolutionFactor
   *            relative resolution factor for the layer. 1.0f is the default value, use larger values to make models more detailed
   *            and lower values to make them less detailed. Note that system really uses discrete levels of detail, so LOD changes
   *            are not continuous and smooth. Larger resolution factors can also cause shimmering, require more memory and take
   *            more time to process.
   */
  public void setLODResolutionFactor(float lodResolutionFactor) {
    this.lodResolutionFactor = Math.max(0.0f, lodResolutionFactor);
  }

  /**
   * Unsupported for this layer. Throws exception.
   */
  @Override
  public void clear() {
    throw new UnsupportedOperationException();
  }

  /**
   * Unsupported for this layer. Throws exception.
   */
  @Override
  public void add(NMLModel element) {
    throw new UnsupportedOperationException();
  }

  /**
   * Unsupported for this layer. Throws exception.
   */
  @Override
  public void addAll(Collection<? extends NMLModel> elements) {
    throw new UnsupportedOperationException();
  }

  /**
   * Unsupported for this layer. Throws exception.
   */
  @Override
  public void remove(NMLModel element) {
    throw new UnsupportedOperationException();
  }
  
  /**
   * Unsupported for this layer. Throws exception.
   */
  @Override
  public void removeAll(Collection<? extends NMLModel> elements) {
    throw new UnsupportedOperationException();
  }
  
  /**
   * Unsupported for this layer. Throws exception.
   */
  @Override
  public Collection<NMLModel> getAll() {
    throw new UnsupportedOperationException();
  }
  
  @Override
  public synchronized void setRenderProjection(RenderProjection renderProjection) {
    mapTileEnvelope = new Envelope(0, 0, 0, 0);
    mapTileList.clear();
    modelLODTreeMap.clear();
    modelLODTreeCache.clear();
    super.setRenderProjection(renderProjection);
  }

  @Override
  public Envelope getDataExtent() {
    return fetchDataExtent();
  }

  @Override
  public void calculateVisibleElements(CullState state) {
    if (state.zoom < minZoom) {
      clearModels();
      return;
    }
    calculateVisibleElementsEnvelope = state.envelope;
    executeVisibilityCalculationTask(new NMLModelCalcVisibleTask(state));
  }

  /**
   * Can be used by subclasses to provide custom label for models.
   * 
   * @param userData
   *          model metadata for customizing the label
   * @return label for the element. Can be null.
   */
  protected Label createLabel(Map<String, String> userData) {
    String folder = userData.get("folder");
    if (folder != null) {
      return new DefaultLabel(folder);
    }
    return null;
  }
  
  protected synchronized void clearModels() {
    modelLODTreeMap.clear();
    meshMap.clear();
    textureMap.clear();
    setVisibleElements(null);
  }
  
  protected synchronized void loadModels(CullState state) {
    calculateVisibleElementsEnvelope = state.envelope;
    long t0 = System.currentTimeMillis();
    
    // Load LOD trees
    loadModelLODTrees(state.envelope);
    long t1 = System.currentTimeMillis();
    Log.debug("NMLModelDbLayer: time for loading LOD trees " + (t1 - t0) + "ms");

    // Build LOD tree draw lists
    createDrawLists(state);
    refineDrawLists();
    long t2 = System.currentTimeMillis();
    Log.debug("NMLModelDbLayer: time for calculating LOD nodes " + (t2 - t1) + "ms");
    
    // Process all LOD trees. This is time consuming, so update visibile elements list if enough time has passed
    loadData(state);
    long t3 = System.currentTimeMillis();
    Log.debug("NMLModelDbLayer: time for loading new data " + (t3 - t2) + "ms");

    // Update visible elements
    List<NMLModel> newVisibleElements = new ArrayList<NMLModel>(modelLODTreeMap.size() + 1);
    for (ModelLODTree modelLODTree : modelLODTreeMap.values()) {
      newVisibleElements.addAll(modelLODTree.modelMap.values());
    }
    if (state.envelope.equals(calculateVisibleElementsEnvelope)) {
      setVisibleElements(newVisibleElements);
    }
  }
  
  protected void createDrawLists(CullState state) {
    double[] modelviewTransform = state.camera.modelviewMatrix;
    double[] projectionTransform = new double[16];
    Matrix.floatToDoubleM(projectionTransform, 0, state.camera.projectionMatrix, 0);
    double[] mvpTransform = new double[16];
    Matrix.multiplyMM(mvpTransform, 0, projectionTransform, 0, modelviewTransform, 0);
    Frustum frustum = new Frustum(state.camera.projectionMatrix, state.camera.modelviewMatrix);
    
    // Create initial node queue from roots
    PriorityQueue<ModelLODTreeNode> initialQueue = new PriorityQueue<ModelLODTreeNode>(modelLODTreeMap.size() + 1, screenSizeComparator); 
    for (ModelLODTree modelLODTree : modelLODTreeMap.values()) {
      modelLODTree.drawList.clear();
      ModelLODTreeNode rootNode = modelLODTree.rootNode; 
      if (rootNode == null) {
        continue;
      }
       
      rootNode.screenSize = calculateProjectedScreenSize(rootNode.nodeCorners, mvpTransform);
      initialQueue.add(rootNode);
    }
    
    // Create new queue by taking root nodes from initial queue until size limits are exceeded
    int totalSize = 0;
    PriorityQueue<ModelLODTreeNode> queue = new PriorityQueue<ModelLODTreeNode>(initialQueue.size() * 8 + 1, screenSizeComparator); 
    while (!initialQueue.isEmpty()) {
      ModelLODTreeNode node = initialQueue.poll();

      // If node is invisible, simply drop it and continue
      double[] bounds = node.modelBounds;
      if (frustum.cuboidIntersects(bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5]) == Frustum.MISS) {
        continue;
      }

      // Test if this node can be added or we have already exceeded max memory footprint
      if (totalSize + node.modelFootprint <= maxMemorySize) {
        queue.add(node);
        totalSize += node.modelFootprint;
      }
    }

    // Create actual draw list by opening bigger nodes (as seen from viewpoint) first and maximum memory footprint is not exceeded
    List<ModelLODTreeNode> childList = new ArrayList<ModelLODTreeNode>(8);
    while (!queue.isEmpty()) {
      ModelLODTreeNode node = queue.poll();
      
      // Decide whether to subdivide this node - this depends on node screen size estimation and whether we can stay within memory size constraints after subdividing
      if (node.screenSize * lodResolutionFactor > 2 && node.childNodes.length > 0) {
        childList.clear();
        int childListTotalSize = totalSize - node.modelFootprint; 
        for (ModelLODTreeNode childNode : node.childNodes) {
          // If child node is visible, add it to child list
          double[] bounds = childNode.modelBounds;
          if (frustum.cuboidIntersects(bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5]) != Frustum.MISS) {
            childList.add(childNode);
            childListTotalSize += childNode.modelFootprint;
          }
        }
        if (childListTotalSize <= maxMemorySize) {
          for (int i = 0; i < childList.size(); i++) {
            ModelLODTreeNode childNode = childList.get(i);
            childNode.screenSize = calculateProjectedScreenSize(childNode.nodeCorners, mvpTransform);
            queue.add(childNode);
          }
          totalSize = childListTotalSize;
          continue;
        }
      }

      // Done with this node, add to draw list
      node.modelLODTree.drawList.add(node);
    }
  }

  protected void refineDrawLists() {
    // We have all data available, no need to refine
  }
  
  protected void loadData(CullState state) {
    LongHashMap<NMLModel.Mesh> newMeshMap = new LongHashMap<NMLModel.Mesh>();
    LongHashMap<NMLModel.Texture> newTextureMap = new LongHashMap<NMLModel.Texture>();
    for (ModelLODTree modelLODTree : modelLODTreeMap.values()) {
      // Start loading models and textures
      HashMap<ModelLODTreeNode, NMLModel> newModelMap = new HashMap<ModelLODTreeNode, NMLModel>();
      for (ModelLODTreeNode node : modelLODTree.drawList) {
        NMLModel model = modelLODTree.modelMap.get(node);
        if (model == null) {
          LongArrayList parentIds = new LongArrayList();
          for (ModelLODTreeNode parentNode = node.parentNode; parentNode != null; parentNode = parentNode.parentNode) {
            parentIds.add(encodeModelIdIndex(modelLODTree.id, parentNode.id));
          }
          model = new NMLModel(modelLODTree.mapPos, null, styleSet, node.nmlModel, modelLODTree.proxyMap, encodeModelIdIndex(modelLODTree.id, node.id), parentIds.toArray(), null); 
          model.attachToLayer(this);
        }

        // Create meshes
        if (node.meshBindings != null) {
          loadMeshes(model, meshMap, newMeshMap, node.meshBindings);
        }

        // Create textures
        if (node.textureBindings != null) {
          loadTextures(model, textureMap, newTextureMap, node.textureBindings);
        }

        // Set style
        model.setActiveStyle(state.zoom);
        newModelMap.put(node, model);
      }
      modelLODTree.drawList.clear();
      modelLODTree.modelMap = newModelMap;
    }
    meshMap = newMeshMap;
    textureMap = newTextureMap;
  }
  
  protected Envelope fetchDataExtent() {
    MutableEnvelope envelope = new MutableEnvelope();
    Cursor cursor = db.rawQuery("SELECT MIN(mapbounds_x0), MIN(mapbounds_y0), MAX(mapbounds_x1), MAX(mapbounds_y1) FROM MapTiles", null);
    if (cursor.moveToFirst()) {
      envelope.minX = cursor.getDouble(0);
      envelope.minY = cursor.getDouble(1);
      envelope.maxX = cursor.getDouble(2);
      envelope.maxY = cursor.getDouble(3);
    }
    cursor.close();
    return new Envelope(envelope);
  }

  protected List<MapTile> fetchMapTiles(MapPos envelopeMin, MapPos envelopeMax) {
    // Handle also degenerate cases (+- 180 crossing) 
    String envelopeX0 = Double.toString(envelopeMin.x), envelopeY0 = Double.toString(envelopeMin.y);
    String envelopeX1 = Double.toString(envelopeMax.x), envelopeY1 = Double.toString(envelopeMax.y);
    String width = Double.toString(projection.getBounds().getWidth());

    List<MapTile> mapTiles = new ArrayList<MapTile>();
    Cursor cursor = db.rawQuery("SELECT id, modellodtree_id, mappos_x, mappos_y, groundheight FROM MapTiles WHERE ((mapbounds_x1>=? AND mapbounds_x0<=?) OR (mapbounds_x1>=?+"+width+" AND mapbounds_x0<=?+"+width+") OR (mapbounds_x1>=?-"+width+" AND mapbounds_x0<=?-"+width+")) AND (mapbounds_y1>=? AND mapbounds_y0<=?)", new String[] { envelopeX0, envelopeX1, envelopeX0, envelopeX1, envelopeX0, envelopeX1, envelopeY0, envelopeY1 } );
    if (cursor.moveToFirst()) {
      do {
        long id = cursor.getLong(0);
        long lodTreeId = cursor.getLong(1);
        double mapPosX = cursor.getDouble(2);
        double mapPosY = cursor.getDouble(3);
        double mapPosZ = cursor.getDouble(4);
        MapPos mapPos = new MapPos(mapPosX, mapPosY, mapPosZ);
        MapTile mapTile = new MapTile(id, mapPos, lodTreeId);
        mapTiles.add(mapTile);
      } while (cursor.moveToNext());
    }
    cursor.close();
    return mapTiles;
  }
  
  @SuppressLint("UseSparseArrays")
  protected ModelLODTree fetchModelLODTree(MapTile mapTile) {
    // Load model LOD tree
    NMLPackage.ModelLODTree nmlModelLODTree = null;
    Cursor cursor = db.rawQuery("SELECT id, nmlmodellodtree FROM ModelLODTrees WHERE id=?", new String[] { Long.toString(mapTile.modelLODTreeId) });
    if (cursor.moveToFirst()) {
      try {
        byte[] blob = cursor.getBlob(1);
        nmlModelLODTree = NMLPackage.ModelLODTree.parseFrom(blob);
      } catch (InvalidProtocolBufferException e) {
        Log.error("NMLModelDbLayer: Failed to load model LOD tree! " + e.getMessage());
      }
    }
    cursor.close();
    if (nmlModelLODTree == null) {
      return null;
    }

    // Load model info
    Map<Integer, NMLModel.Proxy> proxyMap = new HashMap<Integer, NMLModel.Proxy>();
    cursor = db.rawQuery("SELECT * FROM ModelInfo WHERE modellodtree_id=?", new String[] { Long.toString(mapTile.modelLODTreeId) });
    if (cursor.moveToFirst()) {
      do {
        int modelId = cursor.getInt(cursor.getColumnIndex("model_id"));
        double mapPosX = cursor.getFloat(cursor.getColumnIndex("mappos_x"));
        double mapPosY = cursor.getFloat(cursor.getColumnIndex("mappos_y"));
        double mapPosZ = cursor.getFloat(cursor.getColumnIndex("groundheight"));
        MapPos proxyMapPos = new MapPos(mapPosX, mapPosY, mapPosZ);
        
        Map<String, String> userData = new HashMap<String, String>();
        for (String columnName : cursor.getColumnNames()) {
          if (columnName.equals("modellodtree_id") || columnName.equals("global_id") || columnName.equals("model_id") || columnName.equals("mappos_x") || columnName.equals("mappos_y") || columnName.equals("groundheight")) {
            continue;
          }
          userData.put(columnName, cursor.getString(cursor.getColumnIndex(columnName)));
        }

        Label label = createLabel(userData);
        NMLModel.Proxy proxy = new NMLModel.Proxy(proxyMapPos, label, styleSet, userData);
        proxy.attachToLayer(this);
        proxyMap.put(modelId, proxy);
      } while (cursor.moveToNext());
    }
    cursor.close();
    
    // Load mesh mappings
    IntHashMap<List<MeshBinding>> nodeMeshBindingMap = new IntHashMap<List<MeshBinding>>();
    if (dbMeshOpFieldExists) {
      cursor = db.rawQuery("SELECT node_id, local_id, mesh_id, nmlmeshop FROM ModelLODTreeNodeMeshes WHERE modellodtree_id=?", new String[] { Long.toString(mapTile.modelLODTreeId) });
    } else {
      cursor = db.rawQuery("SELECT node_id, local_id, mesh_id FROM ModelLODTreeNodeMeshes WHERE modellodtree_id=?", new String[] { Long.toString(mapTile.modelLODTreeId) });
    }
    if (cursor.moveToFirst()) {
      do {
        int nodeId = cursor.getInt(0);
        String localId = cursor.getString(1);
        long meshId = cursor.getLong(2);
        NMLPackage.MeshOp meshOp = null;
        if (dbMeshOpFieldExists) {
          try {
            byte[] blob = cursor.getBlob(3);
            if (blob != null) {
              meshOp = NMLPackage.MeshOp.parseFrom(blob);
            }
          } catch (InvalidProtocolBufferException e) {
            Log.error("NMLModelDbLayer: Failed to load texture! " + e.getMessage());
          }
        }
        MeshBinding binding = new MeshBinding(meshId, localId, meshOp);
        if (nodeMeshBindingMap.get(nodeId) == null) {
          nodeMeshBindingMap.put(nodeId, new ArrayList<MeshBinding>());
        }
        nodeMeshBindingMap.get(nodeId).add(binding);
      } while (cursor.moveToNext());
    }
    cursor.close();

    // Load texture mappings
    IntHashMap<List<TextureBinding>> nodeTextureBindingMap = new IntHashMap<List<TextureBinding>>();
    cursor = db.rawQuery("SELECT node_id, local_id, texture_id, level FROM ModelLODTreeNodeTextures WHERE modellodtree_id=?", new String[] { Long.toString(mapTile.modelLODTreeId) });
    if (cursor.moveToFirst()) {
      do {
        int nodeId = cursor.getInt(0);
        String localId = cursor.getString(1);
        long textureId = cursor.getLong(2);
        int level = cursor.getInt(3);
        TextureBinding binding = new TextureBinding(textureId, level, localId);
        if (nodeTextureBindingMap.get(nodeId) == null) {
          nodeTextureBindingMap.put(nodeId, new ArrayList<TextureBinding>());
        }
        nodeTextureBindingMap.get(nodeId).add(binding);
      } while (cursor.moveToNext());
    }
    cursor.close();

    // Build LOD tree
    return new ModelLODTree(mapTile.modelLODTreeId, mapTile.mapPos, projection, nmlModelLODTree, nodeMeshBindingMap, nodeTextureBindingMap, proxyMap);
  }

  protected void loadModelLODTrees(Envelope envelope) {
    // If envelope has changed, fetch new list of map tiles
    if (!mapTileEnvelope.equals(envelope)) {
      Envelope envelopeProj = projection.fromInternal(envelope);
      mapTileList = fetchMapTiles(new MapPos(envelopeProj.minX, envelopeProj.minY), new MapPos(envelopeProj.maxX, envelopeProj.maxY));
      mapTileEnvelope = envelope;
    }
    
    // Rebuild new map of model LOD trees
    LongHashMap<ModelLODTree> newModelLODTreeMap = new LongHashMap<ModelLODTree>();
    for (MapTile mapTile : mapTileList) {
      ModelLODTree modelLODTree = modelLODTreeCache.get(mapTile.modelLODTreeId);
      if (modelLODTree == null) {
        modelLODTree = modelLODTreeMap.get(mapTile.modelLODTreeId); 
        if (modelLODTree == null) {
          modelLODTree = fetchModelLODTree(mapTile);
        }
        if (modelLODTree != null) {
          modelLODTreeCache.put(mapTile.modelLODTreeId, modelLODTree);
        }
      }
      if (modelLODTree != null) {
        newModelLODTreeMap.put(mapTile.modelLODTreeId, modelLODTree);
      }
    }
    modelLODTreeMap = newModelLODTreeMap;
  }
  
  protected LongHashMap<NMLModel.Mesh> fetchMeshes(LongArrayList ids) {
    LongHashMap<NMLModel.Mesh> meshMap = new LongHashMap<NMLModel.Mesh>();
    for (int i = 0; i < ids.size(); i++) {
      long id = ids.get(i);
      Cursor cursor = db.rawQuery("SELECT ROWID, SUBSTR(nmlmesh, 1, ?) FROM Meshes WHERE id=?", new String[] { Integer.toString(BLOB_CHUNK_SIZE), Long.toString(id) });
      if (cursor.moveToFirst()) {
        try {
          byte[] blob = cursor.getBlob(1);
          if (blob.length == BLOB_CHUNK_SIZE) {
            blob = readBlob("Meshes", "nmlmesh", cursor.getLong(0));
          }
          NMLPackage.Mesh nmlMesh = NMLPackage.Mesh.parseFrom(blob);
          NMLModel.Mesh mesh = new NMLModel.Mesh(id, nmlMesh);
          meshMap.put(id, mesh);
        }
        catch (InvalidProtocolBufferException e) {
          Log.error("NMLModelDbLayer: Failed to load mesh! " + e.getMessage());
        }
      }
      cursor.close();
    }
    return meshMap;
  }
  
  protected void loadMeshes(NMLModel model, LongHashMap<NMLModel.Mesh> oldMeshMap, LongHashMap<NMLModel.Mesh> newMeshMap, List<MeshBinding> meshBindings) {
    LongHashMap<MeshBinding> fetchMap = new LongHashMap<MeshBinding>(); 
    for (int i = 0; i < meshBindings.size(); i++) {
      MeshBinding binding = meshBindings.get(i); 
      long id = binding.meshId;
      NMLModel.Mesh mesh = oldMeshMap.get(id);
      if (mesh == null) {
        fetchMap.put(id, binding);
        continue;
      }
      model.setMesh(binding.localId, mesh, binding.meshOp);
      newMeshMap.put(id, mesh);
    }
    
    if (fetchMap.size() > 0) {
      LongHashMap<NMLModel.Mesh> fetchedMeshMap = fetchMeshes(fetchMap.keys());
      for (Iterator<LongHashMap.Entry<NMLModel.Mesh>> it = fetchedMeshMap.entrySetIterator(); it.hasNext(); ) {
        LongHashMap.Entry<NMLModel.Mesh> entry = it.next();
        long id = entry.getKey();
        NMLModel.Mesh mesh = entry.getValue();
        MeshBinding binding = fetchMap.get(id);
        if (binding == null) {
          continue;
        }
        model.setMesh(binding.localId, mesh, binding.meshOp);
        newMeshMap.put(id, mesh);
      }
    }
  }
  
  protected LongHashMap<NMLModel.Texture> fetchTextures(LongArrayList ids) {
    LongHashMap<NMLModel.Texture> textureMap = new LongHashMap<NMLModel.Texture>();
    for (int i = 0; i < ids.size(); i++) {
      long id = ids.get(i);
      Cursor cursor = db.rawQuery("SELECT ROWID, SUBSTR(nmltexture, 1, ?) FROM Textures WHERE id=? AND level=?", new String[] { Integer.toString(BLOB_CHUNK_SIZE), Long.toString(id), Integer.toString(0) });
      if (cursor.moveToFirst()) {
        try {
          byte[] blob = cursor.getBlob(1);
          if (blob.length == BLOB_CHUNK_SIZE) {
            blob = readBlob("Textures", "nmltexture", cursor.getLong(0));
          }
          NMLPackage.Texture nmlTexture = NMLPackage.Texture.parseFrom(blob);
          NMLModel.Texture texture = new NMLModel.Texture(id, nmlTexture);
          textureMap.put(id, texture);
        }
        catch (InvalidProtocolBufferException e) {
          Log.error("NMLModelDbLayer: Failed to load texture! " + e.getMessage());
        }
      }
      cursor.close();
    }
    return textureMap;
  }

  protected void loadTextures(NMLModel model, LongHashMap<NMLModel.Texture> oldTextureMap, LongHashMap<NMLModel.Texture> newTextureMap, List<TextureBinding> textureBindings) {
    LongHashMap<TextureBinding> fetchMap = new LongHashMap<TextureBinding>(); 
    for (int i = 0; i < textureBindings.size(); i++) {
      TextureBinding binding = textureBindings.get(i);
      long id = binding.textureId; // ignore level for now
      NMLModel.Texture texture = oldTextureMap.get(id);
      if (texture == null) {
        fetchMap.put(id, binding);
        continue;
      }
      model.setTexture(binding.localId, texture);
      newTextureMap.put(id, texture);
    }
    
    if (fetchMap.size() > 0) {
      LongHashMap<NMLModel.Texture> fetchedTextureMap = fetchTextures(fetchMap.keys());
      for (Iterator<LongHashMap.Entry<NMLModel.Texture>> it = fetchedTextureMap.entrySetIterator(); it.hasNext(); ) {
        LongHashMap.Entry<NMLModel.Texture> entry = it.next();
        long id = entry.getKey();
        NMLModel.Texture texture = entry.getValue();
        TextureBinding binding = fetchMap.get(id);
        if (binding == null) {
          continue;
        }
        model.setTexture(binding.localId, texture);
        newTextureMap.put(id, texture);
      }
    }
  }
  
  private byte[] readBlob(String table, String field, long rowid) {
    List<byte[]> chunks = new ArrayList<byte[]>(16);
    int blobSize = 0;
    for (int offset = 0; true; offset += BLOB_CHUNK_SIZE) {
      boolean lastChunk = true;
      Cursor cursor = db.rawQuery("SELECT SUBSTR(" + field + ", ?, ?) FROM " + table + " WHERE ROWID=?", new String[] { Integer.toString(offset + 1), Integer.toString(BLOB_CHUNK_SIZE), Long.toString(rowid) });
      if (cursor.moveToFirst()) {
        byte[] chunk = cursor.getBlob(0);
        chunks.add(chunk);
        blobSize += chunk.length;
        lastChunk = chunk.length < BLOB_CHUNK_SIZE;
      }
      cursor.close();
      if (lastChunk) {
        break;
      }
    }
    Log.debug("NMLModelDbLayer: reading large blob from " + table + ", size " + blobSize); 

    byte[] blob = new byte[blobSize];
    int offset = 0;
    for (byte[] chunk : chunks) {
      System.arraycopy(chunk, 0, blob, offset, chunk.length);
      offset += chunk.length;
    }
    return blob;
  }

  private static double[] calculateBounds(NMLPackage.Bounds3 bounds, MapPos mapPos, Projection projection, RenderProjection renderProjection) {
    double[] corners = calculateCorners(bounds, mapPos, projection, renderProjection);
    double[] transBounds = new double[] { Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, -Double.MAX_VALUE, -Double.MAX_VALUE, -Double.MAX_VALUE };
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 3; j++) {
        transBounds[j + 0] = Math.min(transBounds[j + 0], corners[i * 3 + j]);
        transBounds[j + 3] = Math.max(transBounds[j + 3], corners[i * 3 + j]);
      }
    }
    return transBounds;
  }
  
  private static double[] calculateCorners(NMLPackage.Bounds3 bounds, MapPos mapPos, Projection projection, RenderProjection renderProjection) {
    MapPos mapPosInternal = projection.toInternal(mapPos);
    double[] matrix = renderProjection.getGlobalFrameMatrix(mapPosInternal, true); 
    double[] point = new double[] { 0, 0, 0, 1 };
    double[] transPoint = new double[4];
    double[] transCorners = new double[3 * 8];
    for (int i = 0; i < 8; i++) {
      point[0] = (i & 1) == 0 ? bounds.getMin().getX() : bounds.getMax().getX();
      point[1] = (i & 2) == 0 ? bounds.getMin().getY() : bounds.getMax().getY();
      point[2] = (i & 4) == 0 ? bounds.getMin().getZ() : bounds.getMax().getZ();
      
      Matrix.multiplyMV(transPoint, 0, matrix, 0, point, 0);
      
      transCorners[i * 3 + 0] = transPoint[0];
      transCorners[i * 3 + 1] = transPoint[1];
      transCorners[i * 3 + 2] = transPoint[2];
    }
    return transCorners;
  }

  private static float calculateProjectedScreenSize(double[] corners, double[] mvpTransform) {
    float minX = Float.MAX_VALUE, maxX = -Float.MAX_VALUE;
    float minY = Float.MAX_VALUE, maxY = -Float.MAX_VALUE;
    double[] point = new double[] { 0, 0, 0, 1 };
    double[] projPoint = new double[4];
    for (int i = 0; i < 8; i++) {
      point[0] = corners[i * 3 + 0];
      point[1] = corners[i * 3 + 1];
      point[2] = corners[i * 3 + 2];

      Matrix.multiplyMV(projPoint, 0, mvpTransform, 0, point, 0);
      if (projPoint[3] < NEAR_PLANE_DISTANCE) {
        projPoint[3] = NEAR_PLANE_DISTANCE; // clip to near plane
      }

      minX = Math.min(minX, (float) (projPoint[0] / projPoint[3]));
      maxX = Math.max(maxX, (float) (projPoint[0] / projPoint[3]));
      minY = Math.min(minY, (float) (projPoint[1] / projPoint[3]));
      maxY = Math.max(maxY, (float) (projPoint[1] / projPoint[3]));
    }
    return Math.max(maxX - minX, maxY - minY);
  }
  
  protected static long encodeModelIdIndex(long modelId, int index) {
    return (modelId << 32) + index;
  }
  
}
