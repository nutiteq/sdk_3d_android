package com.nutiteq.vectorlayers;

import com.nutiteq.geometry.Polygon3D;
import com.nutiteq.projections.Projection;
import com.nutiteq.vectordatasources.VectorDataSource;

/**
 * Layer for 3D polygons.
 */
public class Polygon3DLayer extends VectorLayer<Polygon3D> {

  /**
   * Default constructor.
   * 
   * @param projection
   *          projection for the layer.
   */
  public Polygon3DLayer(Projection projection) {
    super(projection);
  }

  /**
   * Constructor with explicit data source.
   * 
   * @param dataSource
   *          data source for the layer.
   */
  public Polygon3DLayer(VectorDataSource<Polygon3D> dataSource) {
    super(dataSource);
  }
}
