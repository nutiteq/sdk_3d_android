BUILDING library JAR:
1. Download source:
   git clone git@bitbucket.org:nutiteq/sdk_3d_android.git
 
2. go to project folder, configure local project with command:
   android update project --path .

3. Then run build.xml target "build-jar". This produces library and javadoc packages to dist/ folder
